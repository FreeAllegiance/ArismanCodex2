SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss17-SS17.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pilon1-cam_int1.1-0 ROOT ; 
       pilon1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       capdock_pylon_F-inf_light1.1-0 ROOT ; 
       capdock_pylon_F-inf_light2.1-0 ROOT ; 
       capdock_pylon_F-inf_light3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 66     
       capdock_pylon_F-mat1.1-0 ; 
       capdock_pylon_F-mat10.1-0 ; 
       capdock_pylon_F-mat11.1-0 ; 
       capdock_pylon_F-mat12.1-0 ; 
       capdock_pylon_F-mat13.1-0 ; 
       capdock_pylon_F-mat14.1-0 ; 
       capdock_pylon_F-mat15.1-0 ; 
       capdock_pylon_F-mat16.1-0 ; 
       capdock_pylon_F-mat2.1-0 ; 
       capdock_pylon_F-mat27.1-0 ; 
       capdock_pylon_F-mat28.1-0 ; 
       capdock_pylon_F-mat29.1-0 ; 
       capdock_pylon_F-mat3.1-0 ; 
       capdock_pylon_F-mat30.1-0 ; 
       capdock_pylon_F-mat31.1-0 ; 
       capdock_pylon_F-mat32.1-0 ; 
       capdock_pylon_F-mat33.1-0 ; 
       capdock_pylon_F-mat34.1-0 ; 
       capdock_pylon_F-mat35.1-0 ; 
       capdock_pylon_F-mat36.1-0 ; 
       capdock_pylon_F-mat37.1-0 ; 
       capdock_pylon_F-mat38.1-0 ; 
       capdock_pylon_F-mat39.1-0 ; 
       capdock_pylon_F-mat4.1-0 ; 
       capdock_pylon_F-mat40.1-0 ; 
       capdock_pylon_F-mat41.1-0 ; 
       capdock_pylon_F-mat42.1-0 ; 
       capdock_pylon_F-mat43.1-0 ; 
       capdock_pylon_F-mat44.1-0 ; 
       capdock_pylon_F-mat45.1-0 ; 
       capdock_pylon_F-mat46.1-0 ; 
       capdock_pylon_F-mat47.1-0 ; 
       capdock_pylon_F-mat48.1-0 ; 
       capdock_pylon_F-mat49.1-0 ; 
       capdock_pylon_F-mat5.1-0 ; 
       capdock_pylon_F-mat50.1-0 ; 
       capdock_pylon_F-mat51.1-0 ; 
       capdock_pylon_F-mat52.1-0 ; 
       capdock_pylon_F-mat53.1-0 ; 
       capdock_pylon_F-mat54.1-0 ; 
       capdock_pylon_F-mat55.1-0 ; 
       capdock_pylon_F-mat56.1-0 ; 
       capdock_pylon_F-mat57.1-0 ; 
       capdock_pylon_F-mat58.1-0 ; 
       capdock_pylon_F-mat59.1-0 ; 
       capdock_pylon_F-mat6.1-0 ; 
       capdock_pylon_F-mat60.1-0 ; 
       capdock_pylon_F-mat61.1-0 ; 
       capdock_pylon_F-mat62.1-0 ; 
       capdock_pylon_F-mat63.1-0 ; 
       capdock_pylon_F-mat64.1-0 ; 
       capdock_pylon_F-mat65.1-0 ; 
       capdock_pylon_F-mat66.1-0 ; 
       capdock_pylon_F-mat67.1-0 ; 
       capdock_pylon_F-mat68.1-0 ; 
       capdock_pylon_F-mat69.1-0 ; 
       capdock_pylon_F-mat7.1-0 ; 
       capdock_pylon_F-mat70.1-0 ; 
       capdock_pylon_F-mat71.1-0 ; 
       capdock_pylon_F-mat72.1-0 ; 
       capdock_pylon_F-mat73.1-0 ; 
       capdock_pylon_F-mat74.1-0 ; 
       capdock_pylon_F-mat75.1-0 ; 
       capdock_pylon_F-mat76.1-0 ; 
       capdock_pylon_F-mat8.1-0 ; 
       capdock_pylon_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       ss17-bfuselg1.1-0 ; 
       ss17-bfuselg2.1-0 ; 
       ss17-bfuselg3.1-0 ; 
       ss17-doccon.1-0 ; 
       ss17-fuselg1.1-0 ; 
       ss17-fuselg2.1-0 ; 
       ss17-fuselg3.1-0 ; 
       ss17-lfuselg.1-0 ; 
       ss17-lfuselg1.1-0 ; 
       ss17-lfuselg2.1-0 ; 
       ss17-lfuselg3.1-0 ; 
       ss17-mfuselg1.1-0 ; 
       ss17-mfuselg2.1-0 ; 
       ss17-rfuselg.1-0 ; 
       ss17-rfuselg1.1-0 ; 
       ss17-rfuselg2.1-0 ; 
       ss17-rfuselg3.1-0 ; 
       ss17-SS1.1-0 ; 
       ss17-SS17.1-0 ROOT ; 
       ss17-SS18.1-0 ; 
       ss17-SS19.1-0 ; 
       ss17-SS20.1-0 ; 
       ss17-SS21.1-0 ; 
       ss17-SS22.1-0 ; 
       ss17-tfuselg1.1-0 ; 
       ss17-tfuselg2.1-0 ; 
       ss17-tfuselg3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss17/PICTURES/ss17 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss17-capdock_pylon_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 58     
       capdock_pylon_F-t2d1.1-0 ; 
       capdock_pylon_F-t2d10.1-0 ; 
       capdock_pylon_F-t2d11.1-0 ; 
       capdock_pylon_F-t2d12.1-0 ; 
       capdock_pylon_F-t2d13.1-0 ; 
       capdock_pylon_F-t2d14.1-0 ; 
       capdock_pylon_F-t2d15.1-0 ; 
       capdock_pylon_F-t2d2.1-0 ; 
       capdock_pylon_F-t2d26.1-0 ; 
       capdock_pylon_F-t2d27.1-0 ; 
       capdock_pylon_F-t2d28.1-0 ; 
       capdock_pylon_F-t2d29.1-0 ; 
       capdock_pylon_F-t2d3.1-0 ; 
       capdock_pylon_F-t2d30.1-0 ; 
       capdock_pylon_F-t2d31.1-0 ; 
       capdock_pylon_F-t2d32.1-0 ; 
       capdock_pylon_F-t2d33.1-0 ; 
       capdock_pylon_F-t2d34.1-0 ; 
       capdock_pylon_F-t2d35.1-0 ; 
       capdock_pylon_F-t2d36.1-0 ; 
       capdock_pylon_F-t2d37.1-0 ; 
       capdock_pylon_F-t2d38.1-0 ; 
       capdock_pylon_F-t2d39.1-0 ; 
       capdock_pylon_F-t2d4.1-0 ; 
       capdock_pylon_F-t2d40.1-0 ; 
       capdock_pylon_F-t2d41.1-0 ; 
       capdock_pylon_F-t2d42.1-0 ; 
       capdock_pylon_F-t2d43.1-0 ; 
       capdock_pylon_F-t2d44.1-0 ; 
       capdock_pylon_F-t2d45.1-0 ; 
       capdock_pylon_F-t2d46.1-0 ; 
       capdock_pylon_F-t2d47.1-0 ; 
       capdock_pylon_F-t2d48.1-0 ; 
       capdock_pylon_F-t2d49.1-0 ; 
       capdock_pylon_F-t2d5.1-0 ; 
       capdock_pylon_F-t2d50.1-0 ; 
       capdock_pylon_F-t2d51.1-0 ; 
       capdock_pylon_F-t2d52.1-0 ; 
       capdock_pylon_F-t2d53.1-0 ; 
       capdock_pylon_F-t2d54.1-0 ; 
       capdock_pylon_F-t2d55.1-0 ; 
       capdock_pylon_F-t2d56.1-0 ; 
       capdock_pylon_F-t2d57.1-0 ; 
       capdock_pylon_F-t2d58.1-0 ; 
       capdock_pylon_F-t2d59.1-0 ; 
       capdock_pylon_F-t2d6.1-0 ; 
       capdock_pylon_F-t2d60.1-0 ; 
       capdock_pylon_F-t2d61.1-0 ; 
       capdock_pylon_F-t2d62.1-0 ; 
       capdock_pylon_F-t2d63.1-0 ; 
       capdock_pylon_F-t2d64.1-0 ; 
       capdock_pylon_F-t2d65.1-0 ; 
       capdock_pylon_F-t2d66.1-0 ; 
       capdock_pylon_F-t2d67.1-0 ; 
       capdock_pylon_F-t2d68.1-0 ; 
       capdock_pylon_F-t2d7.1-0 ; 
       capdock_pylon_F-t2d8.1-0 ; 
       capdock_pylon_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       17 4 110 ; 
       19 4 110 ; 
       20 14 110 ; 
       21 8 110 ; 
       22 24 110 ; 
       23 0 110 ; 
       0 6 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 7 110 ; 
       4 18 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 6 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 4 110 ; 
       12 11 110 ; 
       13 4 110 ; 
       14 6 110 ; 
       15 14 110 ; 
       16 15 110 ; 
       24 6 110 ; 
       25 24 110 ; 
       26 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       17 58 300 ; 
       18 0 300 ; 
       19 59 300 ; 
       20 60 300 ; 
       21 61 300 ; 
       22 62 300 ; 
       23 63 300 ; 
       0 31 300 ; 
       0 32 300 ; 
       0 33 300 ; 
       0 35 300 ; 
       1 36 300 ; 
       1 37 300 ; 
       1 38 300 ; 
       2 39 300 ; 
       2 40 300 ; 
       2 41 300 ; 
       4 8 300 ; 
       4 12 300 ; 
       4 23 300 ; 
       4 34 300 ; 
       5 48 300 ; 
       5 49 300 ; 
       5 50 300 ; 
       7 42 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       7 46 300 ; 
       7 47 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       10 56 300 ; 
       10 64 300 ; 
       10 65 300 ; 
       11 51 300 ; 
       11 57 300 ; 
       12 52 300 ; 
       12 53 300 ; 
       12 54 300 ; 
       12 55 300 ; 
       13 45 300 ; 
       14 20 300 ; 
       14 21 300 ; 
       14 22 300 ; 
       14 24 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 27 300 ; 
       16 28 300 ; 
       16 29 300 ; 
       16 30 300 ; 
       24 9 300 ; 
       24 10 300 ; 
       24 11 300 ; 
       24 13 300 ; 
       25 14 300 ; 
       25 15 300 ; 
       25 16 300 ; 
       26 17 300 ; 
       26 18 300 ; 
       26 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       18 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 57 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 0 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 7 401 ; 
       13 11 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 12 401 ; 
       24 22 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 29 401 ; 
       31 30 401 ; 
       32 31 401 ; 
       33 32 401 ; 
       34 23 401 ; 
       35 33 401 ; 
       36 35 401 ; 
       37 36 401 ; 
       38 37 401 ; 
       39 38 401 ; 
       40 39 401 ; 
       41 40 401 ; 
       42 41 401 ; 
       43 42 401 ; 
       44 43 401 ; 
       45 34 401 ; 
       46 44 401 ; 
       47 46 401 ; 
       48 47 401 ; 
       49 48 401 ; 
       50 49 401 ; 
       51 50 401 ; 
       53 51 401 ; 
       54 52 401 ; 
       55 53 401 ; 
       56 45 401 ; 
       57 54 401 ; 
       64 55 401 ; 
       65 56 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       17 SCHEM 30 -4 0 MPRFLG 0 ; 
       18 SCHEM 17.5 0 0 SRT 1 1 1 0 1.192484e-008 -8.727015e-008 8.196537e-010 0.009322004 3.314018e-005 MPRFLG 0 ; 
       19 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 20 -10 0 MPRFLG 0 ; 
       21 SCHEM 25 -10 0 MPRFLG 0 ; 
       22 SCHEM 15 -10 0 MPRFLG 0 ; 
       23 SCHEM 10 -10 0 MPRFLG 0 ; 
       0 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 MPRFLG 0 ; 
       12 SCHEM 5 -6 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       24 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       26 SCHEM 12.5 -12 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 34 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 80 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
