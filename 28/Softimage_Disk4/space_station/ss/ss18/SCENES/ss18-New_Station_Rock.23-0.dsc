SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       New_Station_Rock-cam_int1.22-0 ROOT ; 
       New_Station_Rock-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       New_Station_Rock-inf_light1.21-0 ROOT ; 
       New_Station_Rock-inf_light2.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       New_Station_Rock-mat1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       New_Station_Rock-Hi_patch_asteriod.14-0 ROOT ; 
       New_Station_Rock-spline2.6-0 ROOT ; 
       New_Station_Rock-spline3.6-0 ROOT ; 
       New_Station_Rock1-spline1.1-0 ; 
       New_Station_Rock1-spline1_1.1-0 ; 
       New_Station_Rock1-spline1_2.1-0 ; 
       New_Station_Rock1-spline1_3.1-0 ; 
       New_Station_Rock1-spline1_4.6-0 ROOT ; 
       New_Station_Rock1-spline1_5.1-0 ; 
       New_Station_Rock1-spline1_6.1-0 ; 
       New_Station_Rock1-spline1_7.1-0 ; 
       New_Station_Rock1-spline1_8.1-0 ; 
       New_Station_Rock1-spline1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/pete_data2/space_station/ss/ss18/PICTURES/MoonMap_Bump ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/MoonMap_Sides_bump ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-New_Station_Rock.22-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       New_Station_Rock-Main_Bump1.10-0 ; 
       New_Station_Rock-rendermap1.8-0 ; 
       New_Station_Rock-side_bump1.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
       0 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 0.3291198 0.586 0 0 0 -15.38457 -0.2050935 0.9819939 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 32.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 35 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
