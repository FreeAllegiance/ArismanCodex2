SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.127-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       done_text-mat270.3-0 ; 
       done_text-mat271.3-0 ; 
       Rotate-mat322.1-0 ; 
       Rotate-mat323.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       Rotate-cube1.3-0 ; 
       Rotate-cube10.1-0 ; 
       Rotate-cube11.1-0 ; 
       Rotate-cube2.2-0 ; 
       Rotate-cube6.1-0 ; 
       Rotate-cube6_1.2-0 ; 
       Rotate-cube6_2.1-0 ; 
       Rotate-cube7.1-0 ; 
       Rotate-cube8.1-0 ; 
       Rotate-cube9.1-0 ; 
       Rotate-cyl1.1-0 ; 
       Rotate-fuselg1.1-0 ; 
       Rotate-null1.2-0 ; 
       Rotate-null2.1-0 ; 
       Rotate-null3.1-0 ; 
       Rotate-null5.8-0 ROOT ; 
       Rotate-null6.1-0 ; 
       Rotate-null7.1-0 ; 
       Rotate-null8.1-0 ; 
       Rotate-nurbs6_1.38-0 ; 
       Rotate-plaza_lights1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       add_frames_and_strobes-t2d271.6-0 ; 
       add_frames_and_strobes-t2d272.6-0 ; 
       add_frames_and_strobes-t2d274.6-0 ; 
       add_frames_and_strobes-t2d275.6-0 ; 
       add_frames_and_strobes-t2d276.6-0 ; 
       add_frames_and_strobes-t2d277.6-0 ; 
       add_frames_and_strobes-t2d278.13-0 ; 
       add_frames_and_strobes-t2d279.13-0 ; 
       add_frames_and_strobes-t2d7.13-0 ; 
       done_text-t2d196.9-0 ; 
       done_text-t2d197.9-0 ; 
       done_text-t2d198.9-0 ; 
       done_text-t2d199.9-0 ; 
       done_text-t2d200.9-0 ; 
       done_text-t2d201.9-0 ; 
       done_text-t2d261.9-0 ; 
       done_text-t2d262.10-0 ; 
       done_text-t2d263.10-0 ; 
       Rotate-t2d280.4-0 ; 
       Rotate-t2d281.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 19 110 ; 
       1 19 110 ; 
       2 19 110 ; 
       3 19 110 ; 
       4 19 110 ; 
       5 19 110 ; 
       6 19 110 ; 
       7 19 110 ; 
       8 19 110 ; 
       9 19 110 ; 
       10 3 110 ; 
       11 0 110 ; 
       12 15 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 12 110 ; 
       20 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 16 300 ; 
       1 19 300 ; 
       2 20 300 ; 
       3 18 300 ; 
       4 4 300 ; 
       5 3 300 ; 
       6 5 300 ; 
       7 6 300 ; 
       8 7 300 ; 
       9 8 300 ; 
       10 17 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       19 2 300 ; 
       19 1 300 ; 
       19 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 15 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 6 401 ; 
       2 8 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 20 -6 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 25 -6 0 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 30 -4 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 35 -2 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 40 -2 0 MPRFLG 0 ; 
       19 SCHEM 15 -4 0 MPRFLG 0 ; 
       20 SCHEM 27.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 78 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 76.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 77 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 28 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 75.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
