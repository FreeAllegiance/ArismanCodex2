SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.27-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       done_text-light1.25-0 ROOT ; 
       done_text-light2.25-0 ROOT ; 
       done_text-light3.25-0 ROOT ; 
       done_text-light4.25-0 ROOT ; 
       done_text-light5.25-0 ROOT ; 
       done_text-light6.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       add_garages-bay_1.1-0 ; 
       add_garages-bay_one1.2-0 ; 
       add_garages-default5.3-0 ; 
       add_garages-mat272.1-0 ; 
       add_garages-mat273.1-0 ; 
       add_garages-mat274.1-0 ; 
       add_garages-mat275.1-0 ; 
       add_garages-mat276.1-0 ; 
       add_garages-mat277.1-0 ; 
       add_garages-mat278.1-0 ; 
       add_garages-mat279.1-0 ; 
       add_garages-mat280.1-0 ; 
       add_garages-mat281.1-0 ; 
       add_garages-mat283.1-0 ; 
       add_garages-mat284.1-0 ; 
       add_garages-mat285.1-0 ; 
       add_garages-mat286.1-0 ; 
       done_text-mat204.1-0 ; 
       done_text-mat205.1-0 ; 
       done_text-mat206.1-0 ; 
       done_text-mat207.1-0 ; 
       done_text-mat208.1-0 ; 
       done_text-mat209.1-0 ; 
       done_text-mat210.1-0 ; 
       done_text-mat269.1-0 ; 
       done_text-mat270.1-0 ; 
       done_text-mat271.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       add_garages-cube10.1-0 ROOT ; 
       add_garages1-cube10.1-0 ROOT ; 
       redo3-cube1.3-0 ; 
       redo3-cube2.2-0 ; 
       redo3-cube5.1-0 ; 
       redo3-cube6.1-0 ; 
       redo3-cube6_1.2-0 ; 
       redo3-cube6_2.1-0 ; 
       redo3-cube7.1-0 ; 
       redo3-cube8.1-0 ; 
       redo3-cube9.1-0 ; 
       redo3-cyl1.1-0 ; 
       redo3-fuselg1.1-0 ; 
       redo3-fuselg4.1-0 ; 
       redo3-nurbs6_1.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss18/PICTURES/ss18 ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-add_garages.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_garages-t2d264.1-0 ; 
       add_garages-t2d265.1-0 ; 
       add_garages-t2d266.1-0 ; 
       add_garages-t2d267.1-0 ; 
       add_garages-t2d268.1-0 ; 
       add_garages-t2d269.1-0 ; 
       add_garages-t2d270.1-0 ; 
       add_garages-t2d271.1-0 ; 
       add_garages-t2d272.1-0 ; 
       add_garages-t2d274.1-0 ; 
       add_garages-t2d275.1-0 ; 
       add_garages-t2d276.1-0 ; 
       add_garages-t2d277.1-0 ; 
       add_garages-t2d278.6-0 ; 
       add_garages-t2d279.4-0 ; 
       add_garages-t2d7.7-0 ; 
       done_text-t2d196.3-0 ; 
       done_text-t2d197.3-0 ; 
       done_text-t2d198.3-0 ; 
       done_text-t2d199.3-0 ; 
       done_text-t2d200.3-0 ; 
       done_text-t2d201.3-0 ; 
       done_text-t2d261.3-0 ; 
       done_text-t2d262.4-0 ; 
       done_text-t2d263.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 14 110 ; 
       10 14 110 ; 
       7 14 110 ; 
       8 14 110 ; 
       5 14 110 ; 
       9 14 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 14 110 ; 
       11 3 110 ; 
       12 2 110 ; 
       13 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 11 300 ; 
       10 16 300 ; 
       7 13 300 ; 
       8 14 300 ; 
       5 12 300 ; 
       9 15 300 ; 
       2 24 300 ; 
       3 26 300 ; 
       4 3 300 ; 
       11 25 300 ; 
       12 17 300 ; 
       12 18 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       13 4 300 ; 
       13 5 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       13 9 300 ; 
       13 10 300 ; 
       14 2 300 ; 
       14 1 300 ; 
       14 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 22 400 ; 
       4 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       16 12 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       1 13 401 ; 
       0 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 65 -2 0 MPRFLG 0 ; 
       7 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 60 -2 0 MPRFLG 0 ; 
       5 SCHEM 55 -2 0 MPRFLG 0 ; 
       9 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 67.5 0 0 DISPLAY 0 0 SRT 0.5128641 0.248 0.248 0 0 0 -24.96878 -0.4648999 9.926764 MPRFLG 0 ; 
       1 SCHEM 70 0 0 DISPLAY 0 0 SRT 0.5128641 0.248 0.248 0 0 0 22.08499 0.1717586 10.65286 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 40 -2 0 MPRFLG 0 ; 
       11 SCHEM 25 -4 0 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 MPRFLG 0 ; 
       13 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 37.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 36.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
