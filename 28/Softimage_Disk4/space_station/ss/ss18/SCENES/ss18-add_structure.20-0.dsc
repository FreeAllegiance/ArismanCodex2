SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.20-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       done_text-light1.18-0 ROOT ; 
       done_text-light2.18-0 ROOT ; 
       done_text-light3.18-0 ROOT ; 
       done_text-light4.18-0 ROOT ; 
       done_text-light5.18-0 ROOT ; 
       done_text-light6.18-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       add_structure-default5.2-0 ; 
       add_structure-mat272.1-0 ; 
       add_structure-mat273.1-0 ; 
       add_structure-mat274.1-0 ; 
       add_structure-mat275.1-0 ; 
       add_structure-mat276.1-0 ; 
       add_structure-mat277.1-0 ; 
       add_structure-mat278.1-0 ; 
       add_structure-mat279.1-0 ; 
       add_structure-mat280.2-0 ; 
       add_structure-mat281.1-0 ; 
       add_structure-mat283.1-0 ; 
       add_structure-mat284.1-0 ; 
       add_structure-mat285.1-0 ; 
       add_structure-mat286.1-0 ; 
       done_text-mat204.1-0 ; 
       done_text-mat205.1-0 ; 
       done_text-mat206.1-0 ; 
       done_text-mat207.1-0 ; 
       done_text-mat208.1-0 ; 
       done_text-mat209.1-0 ; 
       done_text-mat210.1-0 ; 
       done_text-mat269.1-0 ; 
       done_text-mat270.1-0 ; 
       done_text-mat271.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       redo3-cube1.3-0 ; 
       redo3-cube2.2-0 ; 
       redo3-cube5.1-0 ; 
       redo3-cube6.1-0 ; 
       redo3-cube6_1.2-0 ; 
       redo3-cube6_2.1-0 ; 
       redo3-cube7.1-0 ; 
       redo3-cube8.1-0 ; 
       redo3-cube9.1-0 ; 
       redo3-cyl1.1-0 ; 
       redo3-fuselg1.1-0 ; 
       redo3-fuselg4.1-0 ; 
       redo3-nurbs6_1.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss18/PICTURES/ss18 ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-add_structure.20-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       add_structure-t2d264.2-0 ; 
       add_structure-t2d265.2-0 ; 
       add_structure-t2d266.2-0 ; 
       add_structure-t2d267.2-0 ; 
       add_structure-t2d268.2-0 ; 
       add_structure-t2d269.2-0 ; 
       add_structure-t2d270.2-0 ; 
       add_structure-t2d271.2-0 ; 
       add_structure-t2d272.3-0 ; 
       add_structure-t2d274.2-0 ; 
       add_structure-t2d275.3-0 ; 
       add_structure-t2d276.2-0 ; 
       add_structure-t2d277.2-0 ; 
       add_structure-t2d7.5-0 ; 
       done_text-t2d196.3-0 ; 
       done_text-t2d197.3-0 ; 
       done_text-t2d198.3-0 ; 
       done_text-t2d199.3-0 ; 
       done_text-t2d200.3-0 ; 
       done_text-t2d201.3-0 ; 
       done_text-t2d261.3-0 ; 
       done_text-t2d262.4-0 ; 
       done_text-t2d263.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 12 110 ; 
       8 12 110 ; 
       5 12 110 ; 
       6 12 110 ; 
       3 12 110 ; 
       7 12 110 ; 
       0 12 110 ; 
       1 12 110 ; 
       2 12 110 ; 
       9 1 110 ; 
       10 0 110 ; 
       11 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 9 300 ; 
       8 14 300 ; 
       5 11 300 ; 
       6 12 300 ; 
       3 10 300 ; 
       7 13 300 ; 
       0 22 300 ; 
       1 24 300 ; 
       2 1 300 ; 
       9 23 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       12 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 20 400 ; 
       2 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       14 12 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 65 -2 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 60 -2 0 MPRFLG 0 ; 
       3 SCHEM 55 -2 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 40 -2 0 MPRFLG 0 ; 
       9 SCHEM 25 -4 0 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 35 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 35 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 33.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
