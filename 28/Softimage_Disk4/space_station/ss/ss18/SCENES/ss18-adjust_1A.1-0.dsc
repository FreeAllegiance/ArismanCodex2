SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.42-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       add_frames_and_strobes-bay_1.2-0 ; 
       add_frames_and_strobes-bay_one1.2-0 ; 
       add_frames_and_strobes-default5.2-0 ; 
       add_frames_and_strobes-mat272.2-0 ; 
       add_frames_and_strobes-mat273.2-0 ; 
       add_frames_and_strobes-mat274.2-0 ; 
       add_frames_and_strobes-mat275.2-0 ; 
       add_frames_and_strobes-mat276.2-0 ; 
       add_frames_and_strobes-mat277.2-0 ; 
       add_frames_and_strobes-mat278.2-0 ; 
       add_frames_and_strobes-mat279.2-0 ; 
       add_frames_and_strobes-mat280.2-0 ; 
       add_frames_and_strobes-mat281.2-0 ; 
       add_frames_and_strobes-mat283.2-0 ; 
       add_frames_and_strobes-mat284.2-0 ; 
       add_frames_and_strobes-mat285.2-0 ; 
       add_frames_and_strobes-mat286.2-0 ; 
       done_text-mat204.2-0 ; 
       done_text-mat205.2-0 ; 
       done_text-mat206.2-0 ; 
       done_text-mat207.2-0 ; 
       done_text-mat208.2-0 ; 
       done_text-mat209.2-0 ; 
       done_text-mat210.2-0 ; 
       done_text-mat269.2-0 ; 
       done_text-mat270.2-0 ; 
       done_text-mat271.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 59     
       redo3-bay1.1-0 ; 
       redo3-bay2.1-0 ; 
       redo3-beam_lights.1-0 ; 
       redo3-cube1.3-0 ; 
       redo3-cube2.2-0 ; 
       redo3-cube5.1-0 ; 
       redo3-cube6.1-0 ; 
       redo3-cube6_1.2-0 ; 
       redo3-cube6_2.1-0 ; 
       redo3-cube7.1-0 ; 
       redo3-cube8.1-0 ; 
       redo3-cube9.1-0 ; 
       redo3-cyl1.1-0 ; 
       redo3-fuselg1.1-0 ; 
       redo3-fuselg4.1-0 ; 
       redo3-garage1A.1-0 ; 
       redo3-garage1B.1-0 ; 
       redo3-garage1C.1-0 ; 
       redo3-garage1D.1-0 ; 
       redo3-garage1E.1-0 ; 
       redo3-garage2A.1-0 ; 
       redo3-garage2B.1-0 ; 
       redo3-garage2C.1-0 ; 
       redo3-garage2D.1-0 ; 
       redo3-garage2E.1-0 ; 
       redo3-LAUNCH1.1-0 ; 
       redo3-nurbs6_1.22-0 ROOT ; 
       redo3-plaza_lights.1-0 ; 
       redo3-plaza_lights1.1-0 ; 
       redo3-SS_1.1-0 ; 
       redo3-SS_13.1-0 ; 
       redo3-SS_14.1-0 ; 
       redo3-SS_15.1-0 ; 
       redo3-SS_16.1-0 ; 
       redo3-SS_17.1-0 ; 
       redo3-SS_18.1-0 ; 
       redo3-SS_19.1-0 ; 
       redo3-SS_2.1-0 ; 
       redo3-SS_20.1-0 ; 
       redo3-SS_21.1-0 ; 
       redo3-SS_22.1-0 ; 
       redo3-SS_23.1-0 ; 
       redo3-SS_24.1-0 ; 
       redo3-SS_25.1-0 ; 
       redo3-SS_26.1-0 ; 
       redo3-SS_27.1-0 ; 
       redo3-SS_28.1-0 ; 
       redo3-SS_29.1-0 ; 
       redo3-SS_30.1-0 ; 
       redo3-SS_31.1-0 ; 
       redo3-SS_32.1-0 ; 
       redo3-SS_33.1-0 ; 
       redo3-SS_34.1-0 ; 
       redo3-SS_35.1-0 ; 
       redo3-SS_36.1-0 ; 
       redo3-SS_37.1-0 ; 
       redo3-SS_38.1-0 ; 
       redo3-SS_39.1-0 ; 
       redo3-SS_40.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss18/PICTURES/ss18 ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-adjust_1A.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames_and_strobes-t2d264.2-0 ; 
       add_frames_and_strobes-t2d265.2-0 ; 
       add_frames_and_strobes-t2d266.2-0 ; 
       add_frames_and_strobes-t2d267.2-0 ; 
       add_frames_and_strobes-t2d268.2-0 ; 
       add_frames_and_strobes-t2d269.2-0 ; 
       add_frames_and_strobes-t2d270.2-0 ; 
       add_frames_and_strobes-t2d271.2-0 ; 
       add_frames_and_strobes-t2d272.2-0 ; 
       add_frames_and_strobes-t2d274.2-0 ; 
       add_frames_and_strobes-t2d275.2-0 ; 
       add_frames_and_strobes-t2d276.2-0 ; 
       add_frames_and_strobes-t2d277.2-0 ; 
       add_frames_and_strobes-t2d278.2-0 ; 
       add_frames_and_strobes-t2d279.2-0 ; 
       add_frames_and_strobes-t2d7.2-0 ; 
       done_text-t2d196.5-0 ; 
       done_text-t2d197.5-0 ; 
       done_text-t2d198.5-0 ; 
       done_text-t2d199.5-0 ; 
       done_text-t2d200.5-0 ; 
       done_text-t2d201.5-0 ; 
       done_text-t2d261.5-0 ; 
       done_text-t2d262.6-0 ; 
       done_text-t2d263.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 26 110 ; 
       1 26 110 ; 
       2 26 110 ; 
       3 26 110 ; 
       4 26 110 ; 
       5 26 110 ; 
       6 26 110 ; 
       7 26 110 ; 
       8 26 110 ; 
       9 26 110 ; 
       10 26 110 ; 
       11 26 110 ; 
       12 4 110 ; 
       13 3 110 ; 
       14 5 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 26 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       29 27 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 27 110 ; 
       33 27 110 ; 
       34 27 110 ; 
       35 27 110 ; 
       36 27 110 ; 
       37 27 110 ; 
       38 27 110 ; 
       39 27 110 ; 
       40 27 110 ; 
       41 28 110 ; 
       42 28 110 ; 
       43 28 110 ; 
       44 28 110 ; 
       45 28 110 ; 
       46 28 110 ; 
       47 28 110 ; 
       48 28 110 ; 
       49 28 110 ; 
       50 28 110 ; 
       51 28 110 ; 
       52 28 110 ; 
       53 2 110 ; 
       54 2 110 ; 
       55 2 110 ; 
       56 2 110 ; 
       57 2 110 ; 
       58 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 24 300 ; 
       4 26 300 ; 
       5 3 300 ; 
       6 12 300 ; 
       7 11 300 ; 
       8 13 300 ; 
       9 14 300 ; 
       10 15 300 ; 
       11 16 300 ; 
       12 25 300 ; 
       13 17 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       13 22 300 ; 
       13 23 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       14 6 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       26 2 300 ; 
       26 1 300 ; 
       26 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 22 400 ; 
       5 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 13 401 ; 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 72.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 85 -2 0 MPRFLG 0 ; 
       2 SCHEM 161.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 40 -2 0 MPRFLG 0 ; 
       6 SCHEM 55 -2 0 MPRFLG 0 ; 
       7 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 60 -2 0 MPRFLG 0 ; 
       10 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 65 -2 0 MPRFLG 0 ; 
       12 SCHEM 25 -4 0 MPRFLG 0 ; 
       13 SCHEM 10 -4 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 67.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 70 -4 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 75 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 77.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 80 -4 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       21 SCHEM 82.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 85 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 90 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 92.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 88.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 108.75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 138.75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 95 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 112.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 102.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 105 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 107.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 110 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 100 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 115 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 97.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 117.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 120 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 122.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 142.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 132.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 135 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 137.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 140 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 130 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 145 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 147.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 150 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 152.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 127.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 125 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 155 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 157.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 160 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 162.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 165 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 167.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 91.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 93.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 88.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 93.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 91.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 87.5 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
