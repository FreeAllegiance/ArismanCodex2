SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       asteroid_chain-cam_int1.8-0 ROOT ; 
       asteroid_chain-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 8     
       asteroid_chain-inf_light1.8-0 ROOT ; 
       asteroid_chain-inf_light2.8-0 ROOT ; 
       asteroid_chain-spot1.1-0 ; 
       asteroid_chain-spot1_int.4-0 ROOT ; 
       asteroid_chain-spot2.1-0 ; 
       asteroid_chain-spot2_int.4-0 ROOT ; 
       asteroid_chain-spot3.1-0 ; 
       asteroid_chain-spot3_int.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       asteroid_chain-default2.3-0 ; 
       asteroid_chain-default4.3-0 ; 
       asteroid_chain-default5.3-0 ; 
       asteroid_chain-default6.5-0 ; 
       asteroid_chain-default7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       asteroid_chain-grid6.6-0 ROOT ; 
       asteroid_chain-grid7.6-0 ROOT ; 
       asteroid_chain-nurbs6.6-0 ROOT ; 
       new_model1-nurbs6.12-0 ROOT ; 
       new_model2-nurbs6.1-0 ROOT ; 
       redo2-nurbs6.7-0 ROOT ; 
       redo3-nurbs6.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       E:/pete_data2/space_station/ss/ss18/PICTURES/MoonMap ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/hollow ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/inside_craters ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/main_craters ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/main_shell ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/minibump ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/rendermap ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/rendermap_light ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-asteroid_chain.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       asteroid_chain-cr.3-0 ; 
       asteroid_chain-crater1.3-0 ; 
       asteroid_chain-crater2.3-0 ; 
       asteroid_chain-hollow1.5-0 ; 
       asteroid_chain-inside_craters.5-0 ; 
       asteroid_chain-rendermap3.3-0 ; 
       asteroid_chain-rendermap4.5-0 ; 
       asteroid_chain-rendermap5.1-0 ; 
       asteroid_chain-t2d10.3-0 ; 
       asteroid_chain-t2d11.3-0 ; 
       asteroid_chain-t2d12.3-0 ; 
       asteroid_chain-t2d13.5-0 ; 
       asteroid_chain-t2d14.5-0 ; 
       asteroid_chain-t2d15.5-0 ; 
       asteroid_chain-t2d6.3-0 ; 
       asteroid_chain-t2d7.3-0 ; 
       asteroid_chain-t2d8.3-0 ; 
       asteroid_chain-t2d9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       asteroid_chain-rock1.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       3 3 300 ; 
       5 1 300 ; 
       6 2 300 ; 
       4 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 0 400 ; 
       2 1 400 ; 
       2 2 400 ; 
       2 16 400 ; 
       2 17 400 ; 
       2 8 400 ; 
       2 9 400 ; 
       2 10 400 ; 
       3 11 400 ; 
       3 12 400 ; 
       3 13 400 ; 
       3 4 400 ; 
       3 3 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       3 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
       4 5 2110 ; 
       6 7 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       1 14 401 ; 
       2 15 401 ; 
       3 6 401 ; 
       4 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 60 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 65 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 6.239145 0 MPRFLG 0 ; 
       1 SCHEM 31.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -7.361294 0 MPRFLG 0 ; 
       2 SCHEM 16.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 45 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 3.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 35 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 35 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 8.75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 13.75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 50 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 23.75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 40 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 45 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 33.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 18.75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 47.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
