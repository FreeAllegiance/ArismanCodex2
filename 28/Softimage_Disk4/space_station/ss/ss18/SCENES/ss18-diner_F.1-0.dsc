SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss18-ss18.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       diner_sPTL-cam_int1.1-0 ROOT ; 
       diner_sPTL-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       diner_sPTL-spot10.1-0 ; 
       diner_sPTL-spot10_int.1-0 ROOT ; 
       diner_sPTL-spot11.1-0 ; 
       diner_sPTL-spot11_int.1-0 ROOT ; 
       diner_sPTL-spot9.1-0 ; 
       diner_sPTL-spot9_int.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 75     
       diner_F-nose_white-center.1-1.1-0 ; 
       diner_F-nose_white-center.1-10.1-0 ; 
       diner_F-nose_white-center.1-11.1-0 ; 
       diner_F-nose_white-center.1-12.1-0 ; 
       diner_F-nose_white-center.1-13.1-0 ; 
       diner_F-nose_white-center.1-14.1-0 ; 
       diner_F-nose_white-center.1-2.1-0 ; 
       diner_F-nose_white-center.1-3.1-0 ; 
       diner_F-nose_white-center.1-6.1-0 ; 
       diner_F-nose_white-center.1-7.1-0 ; 
       diner_F-nose_white-center.1-8.1-0 ; 
       diner_F-nose_white-center.1-9.1-0 ; 
       diner_sPTL-default1.1-0 ; 
       diner_sPTL-default2.1-0 ; 
       diner_sPTL-mat1.1-0 ; 
       diner_sPTL-mat10.1-0 ; 
       diner_sPTL-mat11.1-0 ; 
       diner_sPTL-mat12.1-0 ; 
       diner_sPTL-mat13.1-0 ; 
       diner_sPTL-mat14.1-0 ; 
       diner_sPTL-mat15.1-0 ; 
       diner_sPTL-mat16.1-0 ; 
       diner_sPTL-mat17.1-0 ; 
       diner_sPTL-mat18.1-0 ; 
       diner_sPTL-mat19.1-0 ; 
       diner_sPTL-mat2.1-0 ; 
       diner_sPTL-mat20.1-0 ; 
       diner_sPTL-mat21.1-0 ; 
       diner_sPTL-mat22.1-0 ; 
       diner_sPTL-mat23.1-0 ; 
       diner_sPTL-mat24.1-0 ; 
       diner_sPTL-mat25.1-0 ; 
       diner_sPTL-mat26.1-0 ; 
       diner_sPTL-mat27.1-0 ; 
       diner_sPTL-mat28.1-0 ; 
       diner_sPTL-mat29.1-0 ; 
       diner_sPTL-mat3.1-0 ; 
       diner_sPTL-mat30.1-0 ; 
       diner_sPTL-mat31.1-0 ; 
       diner_sPTL-mat32.1-0 ; 
       diner_sPTL-mat33.1-0 ; 
       diner_sPTL-mat34.1-0 ; 
       diner_sPTL-mat35.1-0 ; 
       diner_sPTL-mat36.1-0 ; 
       diner_sPTL-mat37.1-0 ; 
       diner_sPTL-mat38.1-0 ; 
       diner_sPTL-mat39.1-0 ; 
       diner_sPTL-mat4.1-0 ; 
       diner_sPTL-mat40.1-0 ; 
       diner_sPTL-mat41.1-0 ; 
       diner_sPTL-mat42.1-0 ; 
       diner_sPTL-mat43.1-0 ; 
       diner_sPTL-mat44.1-0 ; 
       diner_sPTL-mat45.1-0 ; 
       diner_sPTL-mat46.1-0 ; 
       diner_sPTL-mat47.1-0 ; 
       diner_sPTL-mat48.1-0 ; 
       diner_sPTL-mat5.1-0 ; 
       diner_sPTL-mat50.1-0 ; 
       diner_sPTL-mat51.1-0 ; 
       diner_sPTL-mat52.1-0 ; 
       diner_sPTL-mat53.1-0 ; 
       diner_sPTL-mat54.1-0 ; 
       diner_sPTL-mat55.1-0 ; 
       diner_sPTL-mat56.1-0 ; 
       diner_sPTL-mat57.1-0 ; 
       diner_sPTL-mat58.1-0 ; 
       diner_sPTL-mat59.1-0 ; 
       diner_sPTL-mat6.1-0 ; 
       diner_sPTL-mat60.1-0 ; 
       diner_sPTL-mat61.1-0 ; 
       diner_sPTL-mat7.1-0 ; 
       diner_sPTL-mat8.1-0 ; 
       diner_sPTL-mat9.1-0 ; 
       diner_sPTL-z.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       ss18-bdoccon.1-0 ; 
       ss18-fuselg1.1-0 ; 
       ss18-fuselg2.1-0 ; 
       ss18-lndpad1.1-0 ; 
       ss18-lndpad2.1-0 ; 
       ss18-lndpad3.1-0 ; 
       ss18-lndpad4.1-0 ; 
       ss18-platfm.1-0 ; 
       ss18-portal0.1-0 ; 
       ss18-portal1.1-0 ; 
       ss18-portal2.1-0 ; 
       ss18-portal3.1-0 ; 
       ss18-portal4.1-0 ; 
       ss18-signzz0.1-0 ; 
       ss18-signzz1.1-0 ; 
       ss18-signzz2.1-0 ; 
       ss18-signzz3.1-0 ; 
       ss18-signzz4.1-0 ; 
       ss18-signzz5.1-0 ; 
       ss18-signzz6.1-0 ; 
       ss18-signzz7.1-0 ; 
       ss18-signzz8.1-0 ; 
       ss18-ss18.1-0 ROOT ; 
       ss18-SSp0.1-0 ; 
       ss18-SSp1.1-0 ; 
       ss18-SSp2.1-0 ; 
       ss18-SSp3.1-0 ; 
       ss18-SSp4.1-0 ; 
       ss18-SSp5.1-0 ; 
       ss18-SSp6.1-0 ; 
       ss18-SSp7.1-0 ; 
       ss18-SSp8.1-0 ; 
       ss18-SSt10.1-0 ; 
       ss18-SSt11.1-0 ; 
       ss18-SSt12.1-0 ; 
       ss18-SSt9.1-0 ; 
       ss18-tdoccon.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss18/PICTURES/ss18 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-diner_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 57     
       diner_sPTL-t2d1.1-0 ; 
       diner_sPTL-t2d10.1-0 ; 
       diner_sPTL-t2d11.1-0 ; 
       diner_sPTL-t2d12.1-0 ; 
       diner_sPTL-t2d13.1-0 ; 
       diner_sPTL-t2d14.1-0 ; 
       diner_sPTL-t2d15.1-0 ; 
       diner_sPTL-t2d16.1-0 ; 
       diner_sPTL-t2d17.1-0 ; 
       diner_sPTL-t2d18.1-0 ; 
       diner_sPTL-t2d19.1-0 ; 
       diner_sPTL-t2d2.1-0 ; 
       diner_sPTL-t2d20.1-0 ; 
       diner_sPTL-t2d21.1-0 ; 
       diner_sPTL-t2d22.1-0 ; 
       diner_sPTL-t2d23.1-0 ; 
       diner_sPTL-t2d24.1-0 ; 
       diner_sPTL-t2d25.1-0 ; 
       diner_sPTL-t2d26.1-0 ; 
       diner_sPTL-t2d27.1-0 ; 
       diner_sPTL-t2d28.1-0 ; 
       diner_sPTL-t2d29.1-0 ; 
       diner_sPTL-t2d3.1-0 ; 
       diner_sPTL-t2d30.1-0 ; 
       diner_sPTL-t2d31.1-0 ; 
       diner_sPTL-t2d32.1-0 ; 
       diner_sPTL-t2d33.1-0 ; 
       diner_sPTL-t2d34.1-0 ; 
       diner_sPTL-t2d35.1-0 ; 
       diner_sPTL-t2d36.1-0 ; 
       diner_sPTL-t2d37.1-0 ; 
       diner_sPTL-t2d38.1-0 ; 
       diner_sPTL-t2d39.1-0 ; 
       diner_sPTL-t2d4.1-0 ; 
       diner_sPTL-t2d40.1-0 ; 
       diner_sPTL-t2d41.1-0 ; 
       diner_sPTL-t2d42.1-0 ; 
       diner_sPTL-t2d43.1-0 ; 
       diner_sPTL-t2d44.1-0 ; 
       diner_sPTL-t2d45.1-0 ; 
       diner_sPTL-t2d46.1-0 ; 
       diner_sPTL-t2d47.1-0 ; 
       diner_sPTL-t2d48.1-0 ; 
       diner_sPTL-t2d49.1-0 ; 
       diner_sPTL-t2d5.1-0 ; 
       diner_sPTL-t2d50.1-0 ; 
       diner_sPTL-t2d51.1-0 ; 
       diner_sPTL-t2d52.1-0 ; 
       diner_sPTL-t2d53.1-0 ; 
       diner_sPTL-t2d54.1-0 ; 
       diner_sPTL-t2d55.1-0 ; 
       diner_sPTL-t2d56.1-0 ; 
       diner_sPTL-t2d6.1-0 ; 
       diner_sPTL-t2d7.1-0 ; 
       diner_sPTL-t2d8.1-0 ; 
       diner_sPTL-t2d9.1-0 ; 
       diner_sPTL-zt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       23 2 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 23 110 ; 
       29 23 110 ; 
       30 23 110 ; 
       31 23 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       0 2 110 ; 
       1 22 110 ; 
       2 22 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       7 22 110 ; 
       8 22 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       13 22 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 13 110 ; 
       19 13 110 ; 
       20 13 110 ; 
       21 13 110 ; 
       36 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       24 0 300 ; 
       25 10 300 ; 
       26 9 300 ; 
       27 8 300 ; 
       28 11 300 ; 
       29 1 300 ; 
       30 7 300 ; 
       31 6 300 ; 
       32 3 300 ; 
       33 4 300 ; 
       34 5 300 ; 
       35 2 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       1 30 300 ; 
       1 31 300 ; 
       1 32 300 ; 
       1 33 300 ; 
       1 34 300 ; 
       1 35 300 ; 
       1 37 300 ; 
       1 38 300 ; 
       1 39 300 ; 
       1 40 300 ; 
       1 41 300 ; 
       1 42 300 ; 
       1 43 300 ; 
       1 53 300 ; 
       1 58 300 ; 
       1 59 300 ; 
       1 60 300 ; 
       1 61 300 ; 
       1 62 300 ; 
       1 67 300 ; 
       2 14 300 ; 
       2 25 300 ; 
       2 70 300 ; 
       7 36 300 ; 
       7 47 300 ; 
       7 57 300 ; 
       7 68 300 ; 
       7 71 300 ; 
       7 72 300 ; 
       7 73 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       7 19 300 ; 
       7 63 300 ; 
       7 64 300 ; 
       7 65 300 ; 
       7 66 300 ; 
       7 69 300 ; 
       9 54 300 ; 
       10 74 300 ; 
       11 56 300 ; 
       12 55 300 ; 
       14 52 300 ; 
       15 51 300 ; 
       16 50 300 ; 
       17 49 300 ; 
       18 48 300 ; 
       19 46 300 ; 
       20 45 300 ; 
       21 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       22 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
       2 3 2110 ; 
       4 5 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       15 52 401 ; 
       16 53 401 ; 
       17 54 401 ; 
       18 55 401 ; 
       19 1 401 ; 
       20 2 401 ; 
       21 3 401 ; 
       22 4 401 ; 
       23 5 401 ; 
       24 6 401 ; 
       25 56 401 ; 
       26 7 401 ; 
       27 8 401 ; 
       28 9 401 ; 
       29 10 401 ; 
       30 12 401 ; 
       31 13 401 ; 
       32 14 401 ; 
       33 15 401 ; 
       34 16 401 ; 
       37 17 401 ; 
       38 18 401 ; 
       39 19 401 ; 
       40 20 401 ; 
       41 21 401 ; 
       42 23 401 ; 
       43 24 401 ; 
       44 25 401 ; 
       45 26 401 ; 
       46 27 401 ; 
       47 0 401 ; 
       48 28 401 ; 
       49 29 401 ; 
       50 30 401 ; 
       51 31 401 ; 
       52 32 401 ; 
       53 34 401 ; 
       54 35 401 ; 
       55 36 401 ; 
       56 37 401 ; 
       58 39 401 ; 
       59 40 401 ; 
       60 41 401 ; 
       61 42 401 ; 
       62 43 401 ; 
       63 45 401 ; 
       64 46 401 ; 
       65 47 401 ; 
       66 48 401 ; 
       67 49 401 ; 
       68 11 401 ; 
       69 50 401 ; 
       70 51 401 ; 
       71 22 401 ; 
       72 33 401 ; 
       73 44 401 ; 
       74 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 -0.5 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 19 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -2.5 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 19 -2.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -4.5 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 19 -4.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       23 SCHEM 13 -7.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 16.5 -14.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 16.5 -12.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 16.5 -10.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 16.5 -8.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 16.5 -6.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 16.5 -4.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 16.5 -2.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 16.5 -0.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 13.01306 -29.5653 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 13 -31.5131 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 13 -33.5 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 13 -27.5 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 13 -16.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 9.5 -30.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 9.5 -8.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 13 -18.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 13 -20.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 13 -22.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 13 -24.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 9.5 -21.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 9.5 -55.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 13 -52.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 13 -54.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 13 -56.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 13 -58.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 9.5 -43.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 13 -50.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 13 -48.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 13 -46.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 13 -44.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 13 -42.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 13 -40.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 13 -38.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 13 -36.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 6 -29.5 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 -8.289865 0 MPRFLG 0 ; 
       36 SCHEM 13 -25.5 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -14.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -27.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.51306 -29.8153 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -31.7631 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -33.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -8.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -10.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 20 -12.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -6.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -36.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -38.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -40.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -42.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -44.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -46.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 16.5 -48.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -50.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 16.5 -52.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 16.5 -58.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 16.5 -56.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 13 -23.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 13 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 16.5 -54.75 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 20 -36.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 20 -38.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 20 -40.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 20 -42.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 20 -44.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 20 -46.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 20 -48.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 20 -50.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 20 -52.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 20 -58.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 20 -56.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 20 -54.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -23.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 16.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9.5 1.25 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 19 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
