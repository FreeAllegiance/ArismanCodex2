SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.103-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 61     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat272.3-0 ; 
       add_frames_and_strobes-mat273.3-0 ; 
       add_frames_and_strobes-mat274.3-0 ; 
       add_frames_and_strobes-mat275.3-0 ; 
       add_frames_and_strobes-mat276.3-0 ; 
       add_frames_and_strobes-mat277.3-0 ; 
       add_frames_and_strobes-mat278.3-0 ; 
       add_frames_and_strobes-mat279.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       done_text-mat270.3-0 ; 
       done_text-mat271.3-0 ; 
       edit_strobe_materials-mat287.2-0 ; 
       edit_strobe_materials-mat288.2-0 ; 
       edit_strobe_materials-mat289.2-0 ; 
       edit_strobe_materials-mat290.2-0 ; 
       edit_strobe_materials-mat291.2-0 ; 
       edit_strobe_materials-mat292.2-0 ; 
       edit_strobe_materials-mat293.2-0 ; 
       edit_strobe_materials-mat294.2-0 ; 
       edit_strobe_materials-mat295.2-0 ; 
       edit_strobe_materials-mat296.2-0 ; 
       edit_strobe_materials-mat297.2-0 ; 
       edit_strobe_materials-mat298.2-0 ; 
       edit_strobe_materials-mat299.3-0 ; 
       edit_strobe_materials-mat300.2-0 ; 
       edit_strobe_materials-mat301.2-0 ; 
       edit_strobe_materials-mat302.2-0 ; 
       edit_strobe_materials-mat303.2-0 ; 
       edit_strobe_materials-mat304.2-0 ; 
       edit_strobe_materials-mat305.2-0 ; 
       edit_strobe_materials-mat306.2-0 ; 
       edit_strobe_materials-mat307.2-0 ; 
       edit_strobe_materials-mat308.2-0 ; 
       edit_strobe_materials-mat309.2-0 ; 
       edit_strobe_materials-mat310.2-0 ; 
       edit_strobe_materials-mat311.2-0 ; 
       edit_strobe_materials-mat312.2-0 ; 
       edit_strobe_materials-mat313.2-0 ; 
       edit_strobe_materials-mat314.2-0 ; 
       edit_strobe_materials-mat315.2-0 ; 
       edit_strobe_materials-mat316.2-0 ; 
       edit_strobe_materials-mat317.2-0 ; 
       edit_strobe_materials-mat318.2-0 ; 
       edit_strobe_materials-mat319.2-0 ; 
       move_launch_null-mat321.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 83     
       move_launch_null-bay1.1-0 ; 
       move_launch_null-bay2.1-0 ; 
       move_launch_null-bay3.1-0 ; 
       move_launch_null-beam_lights.1-0 ; 
       move_launch_null-cube1.3-0 ; 
       move_launch_null-cube2.2-0 ; 
       move_launch_null-cube5.1-0 ; 
       move_launch_null-cube6.1-0 ; 
       move_launch_null-cube6_1.2-0 ; 
       move_launch_null-cube6_2.1-0 ; 
       move_launch_null-cube7.1-0 ; 
       move_launch_null-cube8.1-0 ; 
       move_launch_null-cube9.1-0 ; 
       move_launch_null-cyl1.1-0 ; 
       move_launch_null-fuselg1.1-0 ; 
       move_launch_null-fuselg4.1-0 ; 
       move_launch_null-garage1A.1-0 ; 
       move_launch_null-garage1B.1-0 ; 
       move_launch_null-garage1C.1-0 ; 
       move_launch_null-garage1D.1-0 ; 
       move_launch_null-garage1E.1-0 ; 
       move_launch_null-garage2A.1-0 ; 
       move_launch_null-garage2B.1-0 ; 
       move_launch_null-garage2C.1-0 ; 
       move_launch_null-garage2D.1-0 ; 
       move_launch_null-garage2E.1-0 ; 
       move_launch_null-garage3A.1-0 ; 
       move_launch_null-garage3B.1-0 ; 
       move_launch_null-garage3C.1-0 ; 
       move_launch_null-garage3D.1-0 ; 
       move_launch_null-garage3E.1-0 ; 
       move_launch_null-garage4A.1-0 ; 
       move_launch_null-garage5B.1-0 ; 
       move_launch_null-garage5C.1-0 ; 
       move_launch_null-garage5D.1-0 ; 
       move_launch_null-garage5E.1-0 ; 
       move_launch_null-launch1.1-0 ; 
       move_launch_null-null1.2-0 ; 
       move_launch_null-null1_1.1-0 ; 
       move_launch_null-null2.1-0 ; 
       move_launch_null-null3.1-0 ; 
       move_launch_null-null5.1-0 ROOT ; 
       move_launch_null-nurbs6_1.38-0 ; 
       move_launch_null-plaza_lights.1-0 ; 
       move_launch_null-plaza_lights1.1-0 ; 
       move_launch_null-SS_1.1-0 ; 
       move_launch_null-SS_13.1-0 ; 
       move_launch_null-SS_14.1-0 ; 
       move_launch_null-SS_15.1-0 ; 
       move_launch_null-SS_16.1-0 ; 
       move_launch_null-SS_17.1-0 ; 
       move_launch_null-SS_18.1-0 ; 
       move_launch_null-SS_19.1-0 ; 
       move_launch_null-SS_2.1-0 ; 
       move_launch_null-SS_20.1-0 ; 
       move_launch_null-SS_21.1-0 ; 
       move_launch_null-SS_22.1-0 ; 
       move_launch_null-SS_23.1-0 ; 
       move_launch_null-SS_24.1-0 ; 
       move_launch_null-SS_25.1-0 ; 
       move_launch_null-SS_26.1-0 ; 
       move_launch_null-SS_27.1-0 ; 
       move_launch_null-SS_28.1-0 ; 
       move_launch_null-SS_29.1-0 ; 
       move_launch_null-SS_30.1-0 ; 
       move_launch_null-SS_31.1-0 ; 
       move_launch_null-SS_32.1-0 ; 
       move_launch_null-SS_33.1-0 ; 
       move_launch_null-SS_34.1-0 ; 
       move_launch_null-SS_35.1-0 ; 
       move_launch_null-SS_36.1-0 ; 
       move_launch_null-SS_37.1-0 ; 
       move_launch_null-SS_38.1-0 ; 
       move_launch_null-SS_39.1-0 ; 
       move_launch_null-SS_40.1-0 ; 
       move_launch_null-SS_41.1-0 ; 
       move_launch_null-SS_42.1-0 ; 
       move_launch_null-SS_43.1-0 ; 
       move_launch_null-SS_44.1-0 ; 
       move_launch_null-SS_45.1-0 ; 
       move_launch_null-SS_46.1-0 ; 
       move_launch_null-SS_47.1-0 ; 
       move_launch_null-SS_48.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss18/PICTURES/cwbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss18/PICTURES/ss18 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-move_launch_null.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames_and_strobes-t2d264.4-0 ; 
       add_frames_and_strobes-t2d265.4-0 ; 
       add_frames_and_strobes-t2d266.4-0 ; 
       add_frames_and_strobes-t2d267.4-0 ; 
       add_frames_and_strobes-t2d268.4-0 ; 
       add_frames_and_strobes-t2d269.4-0 ; 
       add_frames_and_strobes-t2d270.4-0 ; 
       add_frames_and_strobes-t2d271.4-0 ; 
       add_frames_and_strobes-t2d272.4-0 ; 
       add_frames_and_strobes-t2d274.4-0 ; 
       add_frames_and_strobes-t2d275.4-0 ; 
       add_frames_and_strobes-t2d276.4-0 ; 
       add_frames_and_strobes-t2d277.4-0 ; 
       add_frames_and_strobes-t2d278.11-0 ; 
       add_frames_and_strobes-t2d279.11-0 ; 
       add_frames_and_strobes-t2d7.11-0 ; 
       done_text-t2d196.7-0 ; 
       done_text-t2d197.7-0 ; 
       done_text-t2d198.7-0 ; 
       done_text-t2d199.7-0 ; 
       done_text-t2d200.7-0 ; 
       done_text-t2d201.7-0 ; 
       done_text-t2d261.7-0 ; 
       done_text-t2d262.8-0 ; 
       done_text-t2d263.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 42 110 ; 
       1 42 110 ; 
       3 42 110 ; 
       4 42 110 ; 
       5 42 110 ; 
       6 42 110 ; 
       7 42 110 ; 
       8 42 110 ; 
       9 42 110 ; 
       10 42 110 ; 
       11 42 110 ; 
       12 42 110 ; 
       13 5 110 ; 
       14 4 110 ; 
       15 6 110 ; 
       31 38 110 ; 
       16 0 110 ; 
       26 2 110 ; 
       32 38 110 ; 
       17 0 110 ; 
       27 2 110 ; 
       33 38 110 ; 
       18 0 110 ; 
       28 2 110 ; 
       34 38 110 ; 
       19 0 110 ; 
       29 2 110 ; 
       35 38 110 ; 
       20 0 110 ; 
       30 2 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       36 42 110 ; 
       37 41 110 ; 
       38 37 110 ; 
       39 37 110 ; 
       40 37 110 ; 
       2 37 110 ; 
       42 37 110 ; 
       43 42 110 ; 
       44 42 110 ; 
       45 43 110 ; 
       46 43 110 ; 
       47 43 110 ; 
       48 43 110 ; 
       49 43 110 ; 
       50 43 110 ; 
       51 43 110 ; 
       52 43 110 ; 
       53 43 110 ; 
       54 43 110 ; 
       55 43 110 ; 
       56 43 110 ; 
       57 44 110 ; 
       58 44 110 ; 
       59 44 110 ; 
       60 44 110 ; 
       61 44 110 ; 
       62 44 110 ; 
       63 44 110 ; 
       64 44 110 ; 
       65 44 110 ; 
       66 44 110 ; 
       67 44 110 ; 
       68 44 110 ; 
       69 3 110 ; 
       70 3 110 ; 
       71 3 110 ; 
       72 3 110 ; 
       73 3 110 ; 
       74 3 110 ; 
       75 39 110 ; 
       76 39 110 ; 
       77 39 110 ; 
       78 39 110 ; 
       79 40 110 ; 
       80 40 110 ; 
       81 40 110 ; 
       82 40 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 53 300 ; 
       4 24 300 ; 
       5 26 300 ; 
       6 3 300 ; 
       7 12 300 ; 
       8 11 300 ; 
       9 13 300 ; 
       10 14 300 ; 
       11 15 300 ; 
       12 16 300 ; 
       13 25 300 ; 
       14 17 300 ; 
       14 18 300 ; 
       14 19 300 ; 
       14 20 300 ; 
       14 21 300 ; 
       14 22 300 ; 
       14 23 300 ; 
       15 4 300 ; 
       15 5 300 ; 
       15 6 300 ; 
       15 7 300 ; 
       15 8 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       42 2 300 ; 
       42 1 300 ; 
       42 0 300 ; 
       43 27 300 ; 
       44 40 300 ; 
       45 39 300 ; 
       46 28 300 ; 
       47 29 300 ; 
       48 30 300 ; 
       49 31 300 ; 
       50 32 300 ; 
       51 33 300 ; 
       52 34 300 ; 
       53 38 300 ; 
       54 35 300 ; 
       55 36 300 ; 
       56 37 300 ; 
       57 41 300 ; 
       58 42 300 ; 
       59 43 300 ; 
       60 44 300 ; 
       61 45 300 ; 
       62 46 300 ; 
       63 47 300 ; 
       64 48 300 ; 
       65 49 300 ; 
       66 50 300 ; 
       67 51 300 ; 
       68 52 300 ; 
       69 59 300 ; 
       70 58 300 ; 
       71 57 300 ; 
       72 56 300 ; 
       73 55 300 ; 
       74 54 300 ; 
       75 60 300 ; 
       76 60 300 ; 
       77 60 300 ; 
       78 60 300 ; 
       79 60 300 ; 
       80 60 300 ; 
       81 60 300 ; 
       82 60 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 22 400 ; 
       6 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 13 401 ; 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 94.8566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 107.3566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 189.8566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 34.8566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 48.6066 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 62.3566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 77.3566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 74.8566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 79.8566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 82.3566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 84.8566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 87.3566 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 47.3566 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 125.0928 4.935291 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 32.3566 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 59.8566 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 231.4803 -4.624751 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 89.8566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 228.9582 -0.03333783 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 236.4803 -4.624751 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 92.3566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 233.9582 -0.03333783 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 233.9803 -4.624751 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 94.8566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 231.4582 -0.03333783 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 241.4803 -4.624751 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 97.3566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 238.9582 -0.03333783 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 238.9803 -4.624751 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 99.8566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 236.4582 -0.03333783 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 102.3566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 104.8566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 107.3566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 109.8566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 112.3566 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 114.8566 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 124.8566 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 237.3788 -1.923336 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 211.1066 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 221.1066 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 234.8566 2.668077 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 114.8566 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 132.3566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 164.8566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 117.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 134.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 124.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 127.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 129.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 132.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 122.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 137.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 119.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 139.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 142.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 144.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 167.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 157.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 159.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 162.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       61 SCHEM 164.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       62 SCHEM 154.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       63 SCHEM 169.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       64 SCHEM 172.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       65 SCHEM 174.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       66 SCHEM 177.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       67 SCHEM 152.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       68 SCHEM 149.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       69 SCHEM 182.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       70 SCHEM 184.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       71 SCHEM 187.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       72 SCHEM 189.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       73 SCHEM 192.3566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       74 SCHEM 194.8566 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       75 SCHEM 207.3566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       76 SCHEM 209.8566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       77 SCHEM 212.3566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       78 SCHEM 214.8566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       79 SCHEM 217.3566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       80 SCHEM 219.8566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       81 SCHEM 222.3566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       82 SCHEM 224.8566 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 202.3566 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 199.8566 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 198.8566 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 72.3566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 52.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 57.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 62.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 64.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 74.8566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 77.3566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 79.8566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 82.3566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 84.8566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 87.3566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 27.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 32.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 37.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 44.8566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 47.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49.8566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 147.3566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 134.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 124.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 127.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 129.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 132.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 122.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 137.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 139.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 142.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 144.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 119.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 117.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 179.8566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 167.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 157.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 159.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 162.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 164.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 154.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 169.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 172.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 174.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 177.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 152.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 149.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 194.8103 -9.513936 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 194.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 192.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 189.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 187.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 184.8566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 182.3566 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 207.3566 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 69.8566 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 52.3566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54.8566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 57.3566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59.8566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 62.3566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 64.8566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 74.8566 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 77.3566 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 79.8566 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 82.3566 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 84.8566 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 87.3566 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 199.8566 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 202.3566 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 197.8566 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24.8566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 27.3566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 29.8566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 32.3566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34.8566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 37.3566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 42.3566 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 47.3566 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49.8566 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
