SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.98-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 61     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat272.3-0 ; 
       add_frames_and_strobes-mat273.3-0 ; 
       add_frames_and_strobes-mat274.3-0 ; 
       add_frames_and_strobes-mat275.3-0 ; 
       add_frames_and_strobes-mat276.3-0 ; 
       add_frames_and_strobes-mat277.3-0 ; 
       add_frames_and_strobes-mat278.3-0 ; 
       add_frames_and_strobes-mat279.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       done_text-mat270.3-0 ; 
       done_text-mat271.3-0 ; 
       edit_strobe_materials-mat287.2-0 ; 
       edit_strobe_materials-mat288.2-0 ; 
       edit_strobe_materials-mat289.2-0 ; 
       edit_strobe_materials-mat290.2-0 ; 
       edit_strobe_materials-mat291.2-0 ; 
       edit_strobe_materials-mat292.2-0 ; 
       edit_strobe_materials-mat293.2-0 ; 
       edit_strobe_materials-mat294.2-0 ; 
       edit_strobe_materials-mat295.2-0 ; 
       edit_strobe_materials-mat296.2-0 ; 
       edit_strobe_materials-mat297.2-0 ; 
       edit_strobe_materials-mat298.2-0 ; 
       edit_strobe_materials-mat299.3-0 ; 
       edit_strobe_materials-mat300.2-0 ; 
       edit_strobe_materials-mat301.2-0 ; 
       edit_strobe_materials-mat302.2-0 ; 
       edit_strobe_materials-mat303.2-0 ; 
       edit_strobe_materials-mat304.2-0 ; 
       edit_strobe_materials-mat305.2-0 ; 
       edit_strobe_materials-mat306.2-0 ; 
       edit_strobe_materials-mat307.2-0 ; 
       edit_strobe_materials-mat308.2-0 ; 
       edit_strobe_materials-mat309.2-0 ; 
       edit_strobe_materials-mat310.2-0 ; 
       edit_strobe_materials-mat311.2-0 ; 
       edit_strobe_materials-mat312.2-0 ; 
       edit_strobe_materials-mat313.2-0 ; 
       edit_strobe_materials-mat314.2-0 ; 
       edit_strobe_materials-mat315.2-0 ; 
       edit_strobe_materials-mat316.2-0 ; 
       edit_strobe_materials-mat317.2-0 ; 
       edit_strobe_materials-mat318.2-0 ; 
       edit_strobe_materials-mat319.2-0 ; 
       move_nulls_out-mat321.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 70     
       move_nulls_out-bay1.1-0 ; 
       move_nulls_out-bay2.1-0 ; 
       move_nulls_out-beam_lights.1-0 ; 
       move_nulls_out-cube1.3-0 ; 
       move_nulls_out-cube2.2-0 ; 
       move_nulls_out-cube5.1-0 ; 
       move_nulls_out-cube6.1-0 ; 
       move_nulls_out-cube6_1.2-0 ; 
       move_nulls_out-cube6_2.1-0 ; 
       move_nulls_out-cube7.1-0 ; 
       move_nulls_out-cube8.1-0 ; 
       move_nulls_out-cube9.1-0 ; 
       move_nulls_out-cyl1.1-0 ; 
       move_nulls_out-fuselg1.1-0 ; 
       move_nulls_out-fuselg4.1-0 ; 
       move_nulls_out-garage1A.1-0 ; 
       move_nulls_out-garage1B.1-0 ; 
       move_nulls_out-garage1C.1-0 ; 
       move_nulls_out-garage1D.1-0 ; 
       move_nulls_out-garage1E.1-0 ; 
       move_nulls_out-garage2A.1-0 ; 
       move_nulls_out-garage2B.1-0 ; 
       move_nulls_out-garage2C.1-0 ; 
       move_nulls_out-garage2D.1-0 ; 
       move_nulls_out-garage2E.1-0 ; 
       move_nulls_out-LAUNCH1.1-0 ; 
       move_nulls_out-null1.3-0 ROOT ; 
       move_nulls_out-null2.1-0 ; 
       move_nulls_out-null3.1-0 ; 
       move_nulls_out-nurbs6_1.38-0 ; 
       move_nulls_out-plaza_lights.1-0 ; 
       move_nulls_out-plaza_lights1.1-0 ; 
       move_nulls_out-SS_1.1-0 ; 
       move_nulls_out-SS_13.1-0 ; 
       move_nulls_out-SS_14.1-0 ; 
       move_nulls_out-SS_15.1-0 ; 
       move_nulls_out-SS_16.1-0 ; 
       move_nulls_out-SS_17.1-0 ; 
       move_nulls_out-SS_18.1-0 ; 
       move_nulls_out-SS_19.1-0 ; 
       move_nulls_out-SS_2.1-0 ; 
       move_nulls_out-SS_20.1-0 ; 
       move_nulls_out-SS_21.1-0 ; 
       move_nulls_out-SS_22.1-0 ; 
       move_nulls_out-SS_23.1-0 ; 
       move_nulls_out-SS_24.1-0 ; 
       move_nulls_out-SS_25.1-0 ; 
       move_nulls_out-SS_26.1-0 ; 
       move_nulls_out-SS_27.1-0 ; 
       move_nulls_out-SS_28.1-0 ; 
       move_nulls_out-SS_29.1-0 ; 
       move_nulls_out-SS_30.1-0 ; 
       move_nulls_out-SS_31.1-0 ; 
       move_nulls_out-SS_32.1-0 ; 
       move_nulls_out-SS_33.1-0 ; 
       move_nulls_out-SS_34.1-0 ; 
       move_nulls_out-SS_35.1-0 ; 
       move_nulls_out-SS_36.1-0 ; 
       move_nulls_out-SS_37.1-0 ; 
       move_nulls_out-SS_38.1-0 ; 
       move_nulls_out-SS_39.1-0 ; 
       move_nulls_out-SS_40.1-0 ; 
       move_nulls_out-SS_41.1-0 ; 
       move_nulls_out-SS_42.1-0 ; 
       move_nulls_out-SS_43.1-0 ; 
       move_nulls_out-SS_44.1-0 ; 
       move_nulls_out-SS_45.1-0 ; 
       move_nulls_out-SS_46.1-0 ; 
       move_nulls_out-SS_47.1-0 ; 
       move_nulls_out-SS_48.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss18/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/ss18/PICTURES/ss18 ; 
       E:/Pete_Data2/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-move_nulls_out.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames_and_strobes-t2d264.4-0 ; 
       add_frames_and_strobes-t2d265.4-0 ; 
       add_frames_and_strobes-t2d266.4-0 ; 
       add_frames_and_strobes-t2d267.4-0 ; 
       add_frames_and_strobes-t2d268.4-0 ; 
       add_frames_and_strobes-t2d269.4-0 ; 
       add_frames_and_strobes-t2d270.4-0 ; 
       add_frames_and_strobes-t2d271.4-0 ; 
       add_frames_and_strobes-t2d272.4-0 ; 
       add_frames_and_strobes-t2d274.4-0 ; 
       add_frames_and_strobes-t2d275.4-0 ; 
       add_frames_and_strobes-t2d276.4-0 ; 
       add_frames_and_strobes-t2d277.4-0 ; 
       add_frames_and_strobes-t2d278.11-0 ; 
       add_frames_and_strobes-t2d279.11-0 ; 
       add_frames_and_strobes-t2d7.11-0 ; 
       done_text-t2d196.7-0 ; 
       done_text-t2d197.7-0 ; 
       done_text-t2d198.7-0 ; 
       done_text-t2d199.7-0 ; 
       done_text-t2d200.7-0 ; 
       done_text-t2d201.7-0 ; 
       done_text-t2d261.7-0 ; 
       done_text-t2d262.8-0 ; 
       done_text-t2d263.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 29 110 ; 
       1 29 110 ; 
       2 29 110 ; 
       3 29 110 ; 
       4 29 110 ; 
       5 29 110 ; 
       6 29 110 ; 
       7 29 110 ; 
       8 29 110 ; 
       9 29 110 ; 
       10 29 110 ; 
       11 29 110 ; 
       12 4 110 ; 
       13 3 110 ; 
       14 5 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 29 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       30 29 110 ; 
       31 29 110 ; 
       32 30 110 ; 
       33 30 110 ; 
       34 30 110 ; 
       35 30 110 ; 
       36 30 110 ; 
       37 30 110 ; 
       38 30 110 ; 
       39 30 110 ; 
       40 30 110 ; 
       41 30 110 ; 
       42 30 110 ; 
       43 30 110 ; 
       44 31 110 ; 
       45 31 110 ; 
       46 31 110 ; 
       47 31 110 ; 
       48 31 110 ; 
       49 31 110 ; 
       50 31 110 ; 
       51 31 110 ; 
       52 31 110 ; 
       53 31 110 ; 
       54 31 110 ; 
       55 31 110 ; 
       56 2 110 ; 
       57 2 110 ; 
       58 2 110 ; 
       59 2 110 ; 
       60 2 110 ; 
       61 2 110 ; 
       62 27 110 ; 
       63 27 110 ; 
       64 27 110 ; 
       65 27 110 ; 
       66 28 110 ; 
       67 28 110 ; 
       68 28 110 ; 
       69 28 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 53 300 ; 
       3 24 300 ; 
       4 26 300 ; 
       5 3 300 ; 
       6 12 300 ; 
       7 11 300 ; 
       8 13 300 ; 
       9 14 300 ; 
       10 15 300 ; 
       11 16 300 ; 
       12 25 300 ; 
       13 17 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       13 22 300 ; 
       13 23 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       14 6 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       29 2 300 ; 
       29 1 300 ; 
       29 0 300 ; 
       30 27 300 ; 
       31 40 300 ; 
       32 39 300 ; 
       33 28 300 ; 
       34 29 300 ; 
       35 30 300 ; 
       36 31 300 ; 
       37 32 300 ; 
       38 33 300 ; 
       39 34 300 ; 
       40 38 300 ; 
       41 35 300 ; 
       42 36 300 ; 
       43 37 300 ; 
       44 41 300 ; 
       45 42 300 ; 
       46 43 300 ; 
       47 44 300 ; 
       48 45 300 ; 
       49 46 300 ; 
       50 47 300 ; 
       51 48 300 ; 
       52 49 300 ; 
       53 50 300 ; 
       54 51 300 ; 
       55 52 300 ; 
       56 59 300 ; 
       57 58 300 ; 
       58 57 300 ; 
       59 56 300 ; 
       60 55 300 ; 
       61 54 300 ; 
       62 60 300 ; 
       63 60 300 ; 
       64 60 300 ; 
       65 60 300 ; 
       66 60 300 ; 
       67 60 300 ; 
       68 60 300 ; 
       69 60 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 22 400 ; 
       5 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 13 401 ; 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 85 -4 0 MPRFLG 0 ; 
       2 SCHEM 167.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 40 -4 0 MPRFLG 0 ; 
       6 SCHEM 55 -4 0 MPRFLG 0 ; 
       7 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 60 -4 0 MPRFLG 0 ; 
       10 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 65 -4 0 MPRFLG 0 ; 
       12 SCHEM 25 -6 0 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 67.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 70 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 75 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 77.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 80 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 82.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 85 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 90 -6 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       25 SCHEM 92.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 102.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 188.75 -2 0 MPRFLG 0 ; 
       28 SCHEM 198.75 -2 0 MPRFLG 0 ; 
       29 SCHEM 92.5 -2 0 MPRFLG 0 ; 
       30 SCHEM 110 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 142.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 95 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 112.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 102.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 105 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 107.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 110 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 100 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 115 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 97.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 117.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 120 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 122.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 145 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 135 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 137.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 140 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 142.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 132.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 147.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 150 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 152.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 155 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 130 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 127.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 160 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 162.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 165 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 167.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       60 SCHEM 170 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       61 SCHEM 172.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 185 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 187.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 190 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 192.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 195 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       67 SCHEM 197.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       68 SCHEM 200 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       69 SCHEM 202.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 176.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 172.4537 -9.513936 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 185 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 175.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
