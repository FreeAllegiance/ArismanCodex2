SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       new_model-cam_int1.8-0 ROOT ; 
       new_model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       new_model-inf_light1.8-0 ROOT ; 
       new_model-inf_light2.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       new_model-default2.2-0 ; 
       new_model-default4.1-0 ; 
       new_model-default5.1-0 ; 
       new_model-default6.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       new_model-grid6.1-0 ROOT ; 
       new_model-grid7.1-0 ROOT ; 
       new_model-nurbs6.3-0 ROOT ; 
       new_model1-nurbs6.4-0 ROOT ; 
       redo2-nurbs6.2-0 ROOT ; 
       redo3-nurbs6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 8     
       E:/pete_data2/space_station/ss/ss18/PICTURES/MoonMap ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/hollow ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/inside_craters ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/main_craters ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/main_shell ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/minibump ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/rendermap ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-new_model.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       new_model-cr.2-0 ; 
       new_model-crater1.2-0 ; 
       new_model-crater2.2-0 ; 
       new_model-hollow1.1-0 ; 
       new_model-inside_craters.1-0 ; 
       new_model-rendermap3.2-0 ; 
       new_model-rendermap4.4-0 ; 
       new_model-t2d10.1-0 ; 
       new_model-t2d11.1-0 ; 
       new_model-t2d12.1-0 ; 
       new_model-t2d13.4-0 ; 
       new_model-t2d14.4-0 ; 
       new_model-t2d15.3-0 ; 
       new_model-t2d6.2-0 ; 
       new_model-t2d7.1-0 ; 
       new_model-t2d8.1-0 ; 
       new_model-t2d9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       new_model-rock1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       3 3 300 ; 
       2 0 300 ; 
       4 1 300 ; 
       5 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 10 400 ; 
       3 11 400 ; 
       3 12 400 ; 
       3 4 400 ; 
       3 3 400 ; 
       2 0 400 ; 
       2 1 400 ; 
       2 2 400 ; 
       2 15 400 ; 
       2 16 400 ; 
       2 7 400 ; 
       2 8 400 ; 
       2 9 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       3 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       2 14 401 ; 
       1 13 401 ; 
       3 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 45 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 16.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 31.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -7.361294 0 MPRFLG 0 ; 
       0 SCHEM 28.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 6.239145 0 MPRFLG 0 ; 
       4 SCHEM 3.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 35 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 35 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       5 SCHEM 6.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 8.75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 33.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 13.75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 18.75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 23.75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 26.25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 40 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 45 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 50 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 47.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
