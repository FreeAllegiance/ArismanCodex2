SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.86-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat272.3-0 ; 
       add_frames_and_strobes-mat273.3-0 ; 
       add_frames_and_strobes-mat274.3-0 ; 
       add_frames_and_strobes-mat275.3-0 ; 
       add_frames_and_strobes-mat276.3-0 ; 
       add_frames_and_strobes-mat277.3-0 ; 
       add_frames_and_strobes-mat278.3-0 ; 
       add_frames_and_strobes-mat279.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       done_text-mat270.3-0 ; 
       done_text-mat271.3-0 ; 
       edit_strobe_materials-mat287.2-0 ; 
       edit_strobe_materials-mat288.2-0 ; 
       edit_strobe_materials-mat289.2-0 ; 
       edit_strobe_materials-mat290.2-0 ; 
       edit_strobe_materials-mat291.2-0 ; 
       edit_strobe_materials-mat292.2-0 ; 
       edit_strobe_materials-mat293.2-0 ; 
       edit_strobe_materials-mat294.2-0 ; 
       edit_strobe_materials-mat295.2-0 ; 
       edit_strobe_materials-mat296.2-0 ; 
       edit_strobe_materials-mat297.2-0 ; 
       edit_strobe_materials-mat298.2-0 ; 
       edit_strobe_materials-mat299.3-0 ; 
       edit_strobe_materials-mat300.2-0 ; 
       edit_strobe_materials-mat301.2-0 ; 
       edit_strobe_materials-mat302.2-0 ; 
       edit_strobe_materials-mat303.2-0 ; 
       edit_strobe_materials-mat304.2-0 ; 
       edit_strobe_materials-mat305.2-0 ; 
       edit_strobe_materials-mat306.2-0 ; 
       edit_strobe_materials-mat307.2-0 ; 
       edit_strobe_materials-mat308.2-0 ; 
       edit_strobe_materials-mat309.2-0 ; 
       edit_strobe_materials-mat310.2-0 ; 
       edit_strobe_materials-mat311.2-0 ; 
       edit_strobe_materials-mat312.2-0 ; 
       edit_strobe_materials-mat313.2-0 ; 
       edit_strobe_materials-mat314.2-0 ; 
       edit_strobe_materials-mat315.2-0 ; 
       edit_strobe_materials-mat316.2-0 ; 
       edit_strobe_materials-mat317.2-0 ; 
       edit_strobe_materials-mat318.2-0 ; 
       edit_strobe_materials-mat319.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       recheck_nulls-bay1.1-0 ; 
       recheck_nulls-bay2.1-0 ; 
       recheck_nulls-beam_lights.1-0 ; 
       recheck_nulls-cube1.3-0 ; 
       recheck_nulls-cube2.2-0 ; 
       recheck_nulls-cube5.1-0 ; 
       recheck_nulls-cube6.1-0 ; 
       recheck_nulls-cube6_1.2-0 ; 
       recheck_nulls-cube6_2.1-0 ; 
       recheck_nulls-cube7.1-0 ; 
       recheck_nulls-cube8.1-0 ; 
       recheck_nulls-cube9.1-0 ; 
       recheck_nulls-cyl1.1-0 ; 
       recheck_nulls-fuselg1.1-0 ; 
       recheck_nulls-fuselg4.1-0 ; 
       recheck_nulls-garage1A.1-0 ; 
       recheck_nulls-garage1B.1-0 ; 
       recheck_nulls-garage1C.1-0 ; 
       recheck_nulls-garage1D.1-0 ; 
       recheck_nulls-garage1E.1-0 ; 
       recheck_nulls-garage2A.1-0 ; 
       recheck_nulls-garage2B.1-0 ; 
       recheck_nulls-garage2C.1-0 ; 
       recheck_nulls-garage2D.1-0 ; 
       recheck_nulls-garage2E.1-0 ; 
       recheck_nulls-LAUNCH1.1-0 ; 
       recheck_nulls-null1.3-0 ROOT ; 
       recheck_nulls-nurbs6_1.38-0 ; 
       recheck_nulls-plaza_lights.1-0 ; 
       recheck_nulls-plaza_lights1.1-0 ; 
       recheck_nulls-SS_1.1-0 ; 
       recheck_nulls-SS_13.1-0 ; 
       recheck_nulls-SS_14.1-0 ; 
       recheck_nulls-SS_15.1-0 ; 
       recheck_nulls-SS_16.1-0 ; 
       recheck_nulls-SS_17.1-0 ; 
       recheck_nulls-SS_18.1-0 ; 
       recheck_nulls-SS_19.1-0 ; 
       recheck_nulls-SS_2.1-0 ; 
       recheck_nulls-SS_20.1-0 ; 
       recheck_nulls-SS_21.1-0 ; 
       recheck_nulls-SS_22.1-0 ; 
       recheck_nulls-SS_23.1-0 ; 
       recheck_nulls-SS_24.1-0 ; 
       recheck_nulls-SS_25.1-0 ; 
       recheck_nulls-SS_26.1-0 ; 
       recheck_nulls-SS_27.1-0 ; 
       recheck_nulls-SS_28.1-0 ; 
       recheck_nulls-SS_29.1-0 ; 
       recheck_nulls-SS_30.1-0 ; 
       recheck_nulls-SS_31.1-0 ; 
       recheck_nulls-SS_32.1-0 ; 
       recheck_nulls-SS_33.1-0 ; 
       recheck_nulls-SS_34.1-0 ; 
       recheck_nulls-SS_35.1-0 ; 
       recheck_nulls-SS_36.1-0 ; 
       recheck_nulls-SS_37.1-0 ; 
       recheck_nulls-SS_38.1-0 ; 
       recheck_nulls-SS_39.1-0 ; 
       recheck_nulls-SS_40.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss18/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/ss18/PICTURES/ss18 ; 
       E:/Pete_Data2/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-recheck_nulls.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames_and_strobes-t2d264.4-0 ; 
       add_frames_and_strobes-t2d265.4-0 ; 
       add_frames_and_strobes-t2d266.4-0 ; 
       add_frames_and_strobes-t2d267.4-0 ; 
       add_frames_and_strobes-t2d268.4-0 ; 
       add_frames_and_strobes-t2d269.4-0 ; 
       add_frames_and_strobes-t2d270.4-0 ; 
       add_frames_and_strobes-t2d271.4-0 ; 
       add_frames_and_strobes-t2d272.4-0 ; 
       add_frames_and_strobes-t2d274.4-0 ; 
       add_frames_and_strobes-t2d275.4-0 ; 
       add_frames_and_strobes-t2d276.4-0 ; 
       add_frames_and_strobes-t2d277.4-0 ; 
       add_frames_and_strobes-t2d278.11-0 ; 
       add_frames_and_strobes-t2d279.11-0 ; 
       add_frames_and_strobes-t2d7.11-0 ; 
       done_text-t2d196.7-0 ; 
       done_text-t2d197.7-0 ; 
       done_text-t2d198.7-0 ; 
       done_text-t2d199.7-0 ; 
       done_text-t2d200.7-0 ; 
       done_text-t2d201.7-0 ; 
       done_text-t2d261.7-0 ; 
       done_text-t2d262.8-0 ; 
       done_text-t2d263.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 27 110 ; 
       1 27 110 ; 
       2 27 110 ; 
       3 27 110 ; 
       4 27 110 ; 
       5 27 110 ; 
       6 27 110 ; 
       7 27 110 ; 
       8 27 110 ; 
       9 27 110 ; 
       10 27 110 ; 
       11 27 110 ; 
       12 4 110 ; 
       13 3 110 ; 
       14 5 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 27 110 ; 
       27 26 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       30 28 110 ; 
       31 28 110 ; 
       32 28 110 ; 
       33 28 110 ; 
       34 28 110 ; 
       35 28 110 ; 
       36 28 110 ; 
       37 28 110 ; 
       38 28 110 ; 
       39 28 110 ; 
       40 28 110 ; 
       41 28 110 ; 
       42 29 110 ; 
       43 29 110 ; 
       44 29 110 ; 
       45 29 110 ; 
       46 29 110 ; 
       47 29 110 ; 
       48 29 110 ; 
       49 29 110 ; 
       50 29 110 ; 
       51 29 110 ; 
       52 29 110 ; 
       53 29 110 ; 
       54 2 110 ; 
       55 2 110 ; 
       56 2 110 ; 
       57 2 110 ; 
       58 2 110 ; 
       59 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 53 300 ; 
       3 24 300 ; 
       4 26 300 ; 
       5 3 300 ; 
       6 12 300 ; 
       7 11 300 ; 
       8 13 300 ; 
       9 14 300 ; 
       10 15 300 ; 
       11 16 300 ; 
       12 25 300 ; 
       13 17 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       13 22 300 ; 
       13 23 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       14 6 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       27 2 300 ; 
       27 1 300 ; 
       27 0 300 ; 
       28 27 300 ; 
       29 40 300 ; 
       30 39 300 ; 
       31 28 300 ; 
       32 29 300 ; 
       33 30 300 ; 
       34 31 300 ; 
       35 32 300 ; 
       36 33 300 ; 
       37 34 300 ; 
       38 38 300 ; 
       39 35 300 ; 
       40 36 300 ; 
       41 37 300 ; 
       42 41 300 ; 
       43 42 300 ; 
       44 43 300 ; 
       45 44 300 ; 
       46 45 300 ; 
       47 46 300 ; 
       48 47 300 ; 
       49 48 300 ; 
       50 49 300 ; 
       51 50 300 ; 
       52 51 300 ; 
       53 52 300 ; 
       54 59 300 ; 
       55 58 300 ; 
       56 57 300 ; 
       57 56 300 ; 
       58 55 300 ; 
       59 54 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 22 400 ; 
       5 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 13 401 ; 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -4 0 MPRFLG 0 ; 
       1 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 118.75 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 5 -6 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 30 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 35 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -6 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       21 SCHEM 40 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 42.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 45 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 47.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 50 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 63.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 63.75 -2 0 MPRFLG 0 ; 
       28 SCHEM 66.25 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 96.25 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 52.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 70 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 60 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 62.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 65 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 67.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 57.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 72.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 55 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 75 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 77.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 80 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 100 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 90 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 92.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 95 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 97.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 87.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 102.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 105 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 107.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 110 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 85 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 82.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 112.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 115 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 117.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 120 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 122.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 125 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 126.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 126.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 147.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 111.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 99 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 89 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 91.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 94 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 96.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 86.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 101.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 104 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 106.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 109 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 123.7037 -9.513936 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 124 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 119 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 116.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 114 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 111.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 126.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 126.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 146.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
