SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       asteroid_chain-cam_int1.9-0 ROOT ; 
       asteroid_chain-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 8     
       asteroid_chain-inf_light1.9-0 ROOT ; 
       asteroid_chain-inf_light2.9-0 ROOT ; 
       asteroid_chain-spot1.1-0 ; 
       asteroid_chain-spot1_int.5-0 ROOT ; 
       asteroid_chain-spot2.1-0 ; 
       asteroid_chain-spot2_int.5-0 ROOT ; 
       asteroid_chain-spot3.1-0 ; 
       asteroid_chain-spot3_int.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       asteroid_chain-default6.5-0 ; 
       asteroid_chain-default7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       new_model1-nurbs6.13-0 ROOT ; 
       new_model2-nurbs6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       E:/pete_data2/space_station/ss/ss18/PICTURES/MoonMap ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/hollow ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/inside_craters ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/main_craters ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/main_shell ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/rendermap ; 
       E:/pete_data2/space_station/ss/ss18/PICTURES/rendermap_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-reduce.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       asteroid_chain-hollow1.5-0 ; 
       asteroid_chain-inside_craters.5-0 ; 
       asteroid_chain-rendermap4.5-0 ; 
       asteroid_chain-rendermap5.1-0 ; 
       asteroid_chain-t2d13.5-0 ; 
       asteroid_chain-t2d14.5-0 ; 
       asteroid_chain-t2d15.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       asteroid_chain-rock1.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 4 400 ; 
       0 5 400 ; 
       0 6 400 ; 
       0 1 400 ; 
       0 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
       4 5 2110 ; 
       6 7 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 20 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
