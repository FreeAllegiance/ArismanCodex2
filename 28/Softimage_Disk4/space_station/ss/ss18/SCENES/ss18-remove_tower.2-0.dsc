SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.129-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       done_text-mat270.3-0 ; 
       done_text-mat271.3-0 ; 
       edit_strobe_materials-mat301.3-0 ; 
       edit_strobe_materials-mat314.3-0 ; 
       remove_tower-mat325.1-0 ; 
       remove_tower-mat334.1-0 ; 
       Rotate-mat321.2-0 ; 
       Rotate-mat322.1-0 ; 
       Rotate-mat323.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 83     
       Rotate-capgarage1A.1-0 ; 
       Rotate-capgarage1B.1-0 ; 
       Rotate-capgarage1C.1-0 ; 
       Rotate-capgarage1D.1-0 ; 
       Rotate-capgarage1E.1-0 ; 
       Rotate-cube1.3-0 ; 
       Rotate-cube10.1-0 ; 
       Rotate-cube11.1-0 ; 
       Rotate-cube2.2-0 ; 
       Rotate-cube6.1-0 ; 
       Rotate-cube6_1.2-0 ; 
       Rotate-cube6_2.1-0 ; 
       Rotate-cube7.1-0 ; 
       Rotate-cube8.1-0 ; 
       Rotate-cube9.1-0 ; 
       Rotate-cyl1.1-0 ; 
       Rotate-fuselg1.1-0 ; 
       Rotate-garage2A.1-0 ; 
       Rotate-garage2B.1-0 ; 
       Rotate-garage2C.1-0 ; 
       Rotate-garage2D.1-0 ; 
       Rotate-garage2E.1-0 ; 
       Rotate-garage3A.1-0 ; 
       Rotate-garage3B.1-0 ; 
       Rotate-garage3C.1-0 ; 
       Rotate-garage3D.1-0 ; 
       Rotate-garage3E.1-0 ; 
       Rotate-launch1.1-0 ; 
       Rotate-null1.2-0 ; 
       Rotate-null2.1-0 ; 
       Rotate-null3.1-0 ; 
       Rotate-null5.9-0 ROOT ; 
       Rotate-null6.1-0 ; 
       Rotate-null7.1-0 ; 
       Rotate-null8.1-0 ; 
       Rotate-nurbs6_1.38-0 ; 
       Rotate-plaza_lights1.1-0 ; 
       Rotate-SS_23.1-0 ; 
       Rotate-SS_24.1-0 ; 
       Rotate-SS_25.1-0 ; 
       Rotate-SS_26.1-0 ; 
       Rotate-SS_27.1-0 ; 
       Rotate-SS_28.1-0 ; 
       Rotate-SS_29.1-0 ; 
       Rotate-SS_30.1-0 ; 
       Rotate-SS_31.1-0 ; 
       Rotate-SS_32.1-0 ; 
       Rotate-SS_33.1-0 ; 
       Rotate-SS_34.1-0 ; 
       Rotate-SS_36.1-0 ; 
       Rotate-SS_37.1-0 ; 
       Rotate-SS_38.1-0 ; 
       Rotate-SS_39.1-0 ; 
       Rotate-SS_40.1-0 ; 
       Rotate-SS_41.1-0 ; 
       Rotate-SS_42.1-0 ; 
       Rotate-SS_43.1-0 ; 
       Rotate-SS_44.1-0 ; 
       Rotate-SS_45.1-0 ; 
       Rotate-SS_46.1-0 ; 
       Rotate-SS_47.1-0 ; 
       Rotate-SS_48.1-0 ; 
       Rotate-SS_50.1-0 ; 
       Rotate-SS_51.1-0 ; 
       Rotate-SS_52.1-0 ; 
       Rotate-SS_53.1-0 ; 
       Rotate-SS_54.1-0 ; 
       Rotate-SS_55.1-0 ; 
       Rotate-SS_56.1-0 ; 
       Rotate-SS_57.1-0 ; 
       Rotate-SS_58.1-0 ; 
       Rotate-SS_59.1-0 ; 
       Rotate-SS_61.1-0 ; 
       Rotate-SS_62.1-0 ; 
       Rotate-SS_63.1-0 ; 
       Rotate-SS_64.1-0 ; 
       Rotate-SS_65.1-0 ; 
       Rotate-SS_66.1-0 ; 
       Rotate-SS_67.1-0 ; 
       Rotate-SS_68.1-0 ; 
       Rotate-SS_69.1-0 ; 
       Rotate-SS_70.1-0 ; 
       Rotate-SS_71.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-remove_tower.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       add_frames_and_strobes-t2d271.7-0 ; 
       add_frames_and_strobes-t2d272.7-0 ; 
       add_frames_and_strobes-t2d274.7-0 ; 
       add_frames_and_strobes-t2d275.7-0 ; 
       add_frames_and_strobes-t2d276.7-0 ; 
       add_frames_and_strobes-t2d277.7-0 ; 
       add_frames_and_strobes-t2d278.14-0 ; 
       add_frames_and_strobes-t2d279.14-0 ; 
       add_frames_and_strobes-t2d7.14-0 ; 
       done_text-t2d196.10-0 ; 
       done_text-t2d197.10-0 ; 
       done_text-t2d198.10-0 ; 
       done_text-t2d199.10-0 ; 
       done_text-t2d200.10-0 ; 
       done_text-t2d201.10-0 ; 
       done_text-t2d261.10-0 ; 
       done_text-t2d262.11-0 ; 
       done_text-t2d263.11-0 ; 
       Rotate-t2d280.5-0 ; 
       Rotate-t2d281.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 31 110 ; 
       1 31 110 ; 
       2 31 110 ; 
       3 31 110 ; 
       4 31 110 ; 
       5 35 110 ; 
       6 35 110 ; 
       7 35 110 ; 
       8 35 110 ; 
       9 35 110 ; 
       10 35 110 ; 
       11 35 110 ; 
       12 35 110 ; 
       13 35 110 ; 
       14 35 110 ; 
       15 8 110 ; 
       16 5 110 ; 
       17 31 110 ; 
       18 31 110 ; 
       19 31 110 ; 
       20 31 110 ; 
       21 31 110 ; 
       22 31 110 ; 
       23 31 110 ; 
       24 31 110 ; 
       25 31 110 ; 
       26 31 110 ; 
       27 31 110 ; 
       28 31 110 ; 
       29 28 110 ; 
       30 28 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 31 110 ; 
       35 28 110 ; 
       36 35 110 ; 
       37 36 110 ; 
       38 36 110 ; 
       39 36 110 ; 
       40 36 110 ; 
       41 36 110 ; 
       42 36 110 ; 
       43 36 110 ; 
       44 36 110 ; 
       45 36 110 ; 
       46 36 110 ; 
       47 36 110 ; 
       48 36 110 ; 
       49 32 110 ; 
       50 32 110 ; 
       51 33 110 ; 
       52 32 110 ; 
       53 32 110 ; 
       54 29 110 ; 
       55 29 110 ; 
       56 29 110 ; 
       57 29 110 ; 
       58 30 110 ; 
       59 30 110 ; 
       60 30 110 ; 
       61 30 110 ; 
       62 33 110 ; 
       63 33 110 ; 
       64 33 110 ; 
       65 33 110 ; 
       66 33 110 ; 
       67 33 110 ; 
       68 33 110 ; 
       69 33 110 ; 
       70 33 110 ; 
       71 33 110 ; 
       72 34 110 ; 
       73 34 110 ; 
       74 34 110 ; 
       75 34 110 ; 
       76 34 110 ; 
       77 34 110 ; 
       78 34 110 ; 
       79 34 110 ; 
       80 34 110 ; 
       81 34 110 ; 
       82 34 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 16 300 ; 
       6 24 300 ; 
       7 25 300 ; 
       8 18 300 ; 
       9 4 300 ; 
       10 3 300 ; 
       11 5 300 ; 
       12 6 300 ; 
       13 7 300 ; 
       14 8 300 ; 
       15 17 300 ; 
       16 9 300 ; 
       16 10 300 ; 
       16 11 300 ; 
       16 12 300 ; 
       16 13 300 ; 
       16 14 300 ; 
       16 15 300 ; 
       35 2 300 ; 
       35 1 300 ; 
       35 0 300 ; 
       37 19 300 ; 
       38 19 300 ; 
       39 19 300 ; 
       40 19 300 ; 
       41 19 300 ; 
       42 19 300 ; 
       43 19 300 ; 
       44 19 300 ; 
       45 19 300 ; 
       46 19 300 ; 
       47 19 300 ; 
       48 19 300 ; 
       49 20 300 ; 
       50 20 300 ; 
       51 21 300 ; 
       52 20 300 ; 
       53 20 300 ; 
       54 23 300 ; 
       55 23 300 ; 
       56 23 300 ; 
       57 23 300 ; 
       58 23 300 ; 
       59 23 300 ; 
       60 23 300 ; 
       61 23 300 ; 
       62 21 300 ; 
       63 21 300 ; 
       64 21 300 ; 
       65 21 300 ; 
       66 21 300 ; 
       67 21 300 ; 
       68 21 300 ; 
       69 21 300 ; 
       70 21 300 ; 
       71 21 300 ; 
       72 22 300 ; 
       73 22 300 ; 
       74 22 300 ; 
       75 22 300 ; 
       76 22 300 ; 
       77 22 300 ; 
       78 22 300 ; 
       79 22 300 ; 
       80 22 300 ; 
       81 22 300 ; 
       82 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 15 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 6 401 ; 
       2 8 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 55 -6 0 MPRFLG 0 ; 
       8 SCHEM 45 -6 0 MPRFLG 0 ; 
       9 SCHEM 50 -6 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 60 -6 0 MPRFLG 0 ; 
       13 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 65 -6 0 MPRFLG 0 ; 
       15 SCHEM 45 -8 0 MPRFLG 0 ; 
       16 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 35 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 40 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 2.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 78.75 -2 0 MPRFLG 0 ; 
       29 SCHEM 101.25 -4 0 MPRFLG 0 ; 
       30 SCHEM 111.25 -4 0 MPRFLG 0 ; 
       31 SCHEM 91.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       32 SCHEM 121.25 -2 0 MPRFLG 0 ; 
       33 SCHEM 140 -2 0 MPRFLG 0 ; 
       34 SCHEM 167.5 -2 0 MPRFLG 0 ; 
       35 SCHEM 68.75 -4 0 MPRFLG 0 ; 
       36 SCHEM 81.25 -6 0 MPRFLG 0 ; 
       37 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 130 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 122.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 125 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 97.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 100 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 102.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 105 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 107.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 110 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 112.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 115 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 127.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 132.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 135 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 137.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 140 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 142.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 145 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 147.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       70 SCHEM 150 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       71 SCHEM 152.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       72 SCHEM 157.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 155 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 160 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 162.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       76 SCHEM 165 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       77 SCHEM 167.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       78 SCHEM 170 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       79 SCHEM 172.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       80 SCHEM 175 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       81 SCHEM 177.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       82 SCHEM 180 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 96.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 131.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 130.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 84 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 124 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 139.7896 -6.815282 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 156.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 96.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 130.75 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 95.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 129.25 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
