SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.119-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat272.3-0 ; 
       add_frames_and_strobes-mat273.3-0 ; 
       add_frames_and_strobes-mat274.3-0 ; 
       add_frames_and_strobes-mat275.3-0 ; 
       add_frames_and_strobes-mat276.3-0 ; 
       add_frames_and_strobes-mat277.3-0 ; 
       add_frames_and_strobes-mat278.3-0 ; 
       add_frames_and_strobes-mat279.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       done_text-mat270.3-0 ; 
       done_text-mat271.3-0 ; 
       edit_strobe_materials-mat288.2-0 ; 
       edit_strobe_materials-mat289.2-0 ; 
       edit_strobe_materials-mat290.2-0 ; 
       edit_strobe_materials-mat291.2-0 ; 
       edit_strobe_materials-mat292.2-0 ; 
       edit_strobe_materials-mat293.2-0 ; 
       edit_strobe_materials-mat294.2-0 ; 
       edit_strobe_materials-mat295.2-0 ; 
       edit_strobe_materials-mat296.2-0 ; 
       edit_strobe_materials-mat297.2-0 ; 
       edit_strobe_materials-mat298.2-0 ; 
       edit_strobe_materials-mat299.3-0 ; 
       edit_strobe_materials-mat301.2-0 ; 
       edit_strobe_materials-mat302.2-0 ; 
       edit_strobe_materials-mat303.2-0 ; 
       edit_strobe_materials-mat304.2-0 ; 
       edit_strobe_materials-mat305.2-0 ; 
       edit_strobe_materials-mat306.2-0 ; 
       edit_strobe_materials-mat307.2-0 ; 
       edit_strobe_materials-mat308.2-0 ; 
       edit_strobe_materials-mat309.2-0 ; 
       edit_strobe_materials-mat310.2-0 ; 
       edit_strobe_materials-mat311.2-0 ; 
       edit_strobe_materials-mat312.2-0 ; 
       edit_strobe_materials-mat314.3-0 ; 
       edit_strobe_materials-mat316.3-0 ; 
       Rotate-mat321.2-0 ; 
       Rotate-mat322.1-0 ; 
       Rotate-mat323.1-0 ; 
       Rotate-mat324.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 80     
       Rotate-beam_lights.1-0 ; 
       Rotate-capgarage1A.1-0 ; 
       Rotate-capgarage1B.1-0 ; 
       Rotate-capgarage1C.1-0 ; 
       Rotate-capgarage1D.1-0 ; 
       Rotate-capgarage1E.1-0 ; 
       Rotate-cube1.3-0 ; 
       Rotate-cube10.1-0 ; 
       Rotate-cube11.1-0 ; 
       Rotate-cube2.2-0 ; 
       Rotate-cube5.1-0 ; 
       Rotate-cube6.1-0 ; 
       Rotate-cube6_1.2-0 ; 
       Rotate-cube6_2.1-0 ; 
       Rotate-cube7.1-0 ; 
       Rotate-cube8.1-0 ; 
       Rotate-cube9.1-0 ; 
       Rotate-cyl1.1-0 ; 
       Rotate-fuselg1.1-0 ; 
       Rotate-fuselg4.1-0 ; 
       Rotate-garage2A.1-0 ; 
       Rotate-garage2B.1-0 ; 
       Rotate-garage2C.1-0 ; 
       Rotate-garage2D.1-0 ; 
       Rotate-garage2E.1-0 ; 
       Rotate-garage3A.1-0 ; 
       Rotate-garage3B.1-0 ; 
       Rotate-garage3C.1-0 ; 
       Rotate-garage3D.1-0 ; 
       Rotate-garage3E.1-0 ; 
       Rotate-launch1.1-0 ; 
       Rotate-null1.2-0 ; 
       Rotate-null2.1-0 ; 
       Rotate-null3.1-0 ; 
       Rotate-null5.3-0 ROOT ; 
       Rotate-null6.1-0 ; 
       Rotate-null7.1-0 ; 
       Rotate-nurbs6_1.38-0 ; 
       Rotate-plaza_lights.1-0 ; 
       Rotate-plaza_lights1.1-0 ; 
       Rotate-SS_1.1-0 ; 
       Rotate-SS_13.1-0 ; 
       Rotate-SS_14.1-0 ; 
       Rotate-SS_15.1-0 ; 
       Rotate-SS_16.1-0 ; 
       Rotate-SS_17.1-0 ; 
       Rotate-SS_18.1-0 ; 
       Rotate-SS_19.1-0 ; 
       Rotate-SS_2.1-0 ; 
       Rotate-SS_20.1-0 ; 
       Rotate-SS_21.1-0 ; 
       Rotate-SS_22.1-0 ; 
       Rotate-SS_23.1-0 ; 
       Rotate-SS_24.1-0 ; 
       Rotate-SS_25.1-0 ; 
       Rotate-SS_26.1-0 ; 
       Rotate-SS_27.1-0 ; 
       Rotate-SS_28.1-0 ; 
       Rotate-SS_29.1-0 ; 
       Rotate-SS_30.1-0 ; 
       Rotate-SS_31.1-0 ; 
       Rotate-SS_32.1-0 ; 
       Rotate-SS_33.1-0 ; 
       Rotate-SS_34.1-0 ; 
       Rotate-SS_35.1-0 ; 
       Rotate-SS_36.1-0 ; 
       Rotate-SS_37.1-0 ; 
       Rotate-SS_38.1-0 ; 
       Rotate-SS_39.1-0 ; 
       Rotate-SS_40.1-0 ; 
       Rotate-SS_41.1-0 ; 
       Rotate-SS_42.1-0 ; 
       Rotate-SS_43.1-0 ; 
       Rotate-SS_44.1-0 ; 
       Rotate-SS_45.1-0 ; 
       Rotate-SS_46.1-0 ; 
       Rotate-SS_47.1-0 ; 
       Rotate-SS_48.1-0 ; 
       Rotate-SS_49.1-0 ; 
       Rotate-SS_50.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-rotate.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       add_frames_and_strobes-t2d264.4-0 ; 
       add_frames_and_strobes-t2d265.4-0 ; 
       add_frames_and_strobes-t2d266.4-0 ; 
       add_frames_and_strobes-t2d267.4-0 ; 
       add_frames_and_strobes-t2d268.4-0 ; 
       add_frames_and_strobes-t2d269.4-0 ; 
       add_frames_and_strobes-t2d270.4-0 ; 
       add_frames_and_strobes-t2d271.4-0 ; 
       add_frames_and_strobes-t2d272.4-0 ; 
       add_frames_and_strobes-t2d274.4-0 ; 
       add_frames_and_strobes-t2d275.4-0 ; 
       add_frames_and_strobes-t2d276.4-0 ; 
       add_frames_and_strobes-t2d277.4-0 ; 
       add_frames_and_strobes-t2d278.11-0 ; 
       add_frames_and_strobes-t2d279.11-0 ; 
       add_frames_and_strobes-t2d7.11-0 ; 
       done_text-t2d196.7-0 ; 
       done_text-t2d197.7-0 ; 
       done_text-t2d198.7-0 ; 
       done_text-t2d199.7-0 ; 
       done_text-t2d200.7-0 ; 
       done_text-t2d201.7-0 ; 
       done_text-t2d261.7-0 ; 
       done_text-t2d262.8-0 ; 
       done_text-t2d263.8-0 ; 
       Rotate-t2d280.2-0 ; 
       Rotate-t2d281.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 34 110 ; 
       1 34 110 ; 
       2 34 110 ; 
       3 34 110 ; 
       4 34 110 ; 
       5 34 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       6 37 110 ; 
       7 37 110 ; 
       8 37 110 ; 
       9 37 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       12 37 110 ; 
       13 37 110 ; 
       14 37 110 ; 
       15 37 110 ; 
       16 37 110 ; 
       17 9 110 ; 
       18 6 110 ; 
       19 10 110 ; 
       20 34 110 ; 
       21 34 110 ; 
       22 34 110 ; 
       23 34 110 ; 
       24 34 110 ; 
       25 34 110 ; 
       26 34 110 ; 
       27 34 110 ; 
       28 34 110 ; 
       29 34 110 ; 
       30 34 110 ; 
       31 34 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       37 31 110 ; 
       38 37 110 ; 
       39 37 110 ; 
       40 38 110 ; 
       41 38 110 ; 
       42 38 110 ; 
       43 38 110 ; 
       44 38 110 ; 
       45 38 110 ; 
       46 38 110 ; 
       47 38 110 ; 
       48 38 110 ; 
       49 38 110 ; 
       50 38 110 ; 
       51 38 110 ; 
       52 39 110 ; 
       53 39 110 ; 
       54 39 110 ; 
       55 39 110 ; 
       56 39 110 ; 
       57 39 110 ; 
       58 39 110 ; 
       59 39 110 ; 
       60 39 110 ; 
       61 39 110 ; 
       62 39 110 ; 
       63 39 110 ; 
       64 0 110 ; 
       65 35 110 ; 
       66 35 110 ; 
       67 36 110 ; 
       68 35 110 ; 
       69 35 110 ; 
       70 32 110 ; 
       71 32 110 ; 
       72 32 110 ; 
       73 32 110 ; 
       74 33 110 ; 
       75 33 110 ; 
       76 33 110 ; 
       77 33 110 ; 
       78 0 110 ; 
       79 36 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 24 300 ; 
       7 54 300 ; 
       8 55 300 ; 
       9 26 300 ; 
       10 3 300 ; 
       11 12 300 ; 
       12 11 300 ; 
       13 13 300 ; 
       14 14 300 ; 
       15 15 300 ; 
       16 16 300 ; 
       17 25 300 ; 
       18 17 300 ; 
       18 18 300 ; 
       18 19 300 ; 
       18 20 300 ; 
       18 21 300 ; 
       18 22 300 ; 
       18 23 300 ; 
       19 4 300 ; 
       19 5 300 ; 
       19 6 300 ; 
       19 7 300 ; 
       19 8 300 ; 
       19 9 300 ; 
       19 10 300 ; 
       37 2 300 ; 
       37 1 300 ; 
       37 0 300 ; 
       40 38 300 ; 
       41 27 300 ; 
       42 28 300 ; 
       43 29 300 ; 
       44 30 300 ; 
       45 31 300 ; 
       46 32 300 ; 
       47 33 300 ; 
       48 37 300 ; 
       49 34 300 ; 
       50 35 300 ; 
       51 36 300 ; 
       52 39 300 ; 
       53 40 300 ; 
       54 41 300 ; 
       55 42 300 ; 
       56 43 300 ; 
       57 44 300 ; 
       58 45 300 ; 
       59 46 300 ; 
       60 47 300 ; 
       61 48 300 ; 
       62 49 300 ; 
       63 50 300 ; 
       64 56 300 ; 
       65 51 300 ; 
       66 51 300 ; 
       67 52 300 ; 
       68 51 300 ; 
       69 51 300 ; 
       70 53 300 ; 
       71 53 300 ; 
       72 53 300 ; 
       73 53 300 ; 
       74 53 300 ; 
       75 53 300 ; 
       76 53 300 ; 
       77 53 300 ; 
       78 56 300 ; 
       79 52 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 22 400 ; 
       10 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 13 401 ; 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       54 25 401 ; 
       55 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 216.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 203.75 -2 0 MPRFLG 0 ; 
       36 SCHEM 211.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 97.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 100 -6 0 MPRFLG 0 ; 
       9 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       10 SCHEM 80 -6 0 MPRFLG 0 ; 
       11 SCHEM 95 -6 0 MPRFLG 0 ; 
       12 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 105 -6 0 MPRFLG 0 ; 
       15 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 110 -6 0 MPRFLG 0 ; 
       17 SCHEM 65 -8 0 MPRFLG 0 ; 
       18 SCHEM 50 -8 0 MPRFLG 0 ; 
       19 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 35 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 40 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 2.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 120 -2 0 MPRFLG 0 ; 
       32 SCHEM 183.75 -4 0 MPRFLG 0 ; 
       33 SCHEM 193.75 -4 0 MPRFLG 0 ; 
       34 SCHEM 110 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       37 SCHEM 110 -4 0 MPRFLG 0 ; 
       38 SCHEM 126.25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 156.25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 112.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 130 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 120 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 122.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 125 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 127.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 117.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 132.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 115 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 135 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 137.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 140 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 160 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 150 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 152.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 155 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 157.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 147.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 162.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 165 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       60 SCHEM 167.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       61 SCHEM 170 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 145 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 142.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 217.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 200 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 202.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 212.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 205 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 180 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       71 SCHEM 182.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       72 SCHEM 185 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       73 SCHEM 187.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       74 SCHEM 190 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       75 SCHEM 192.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       76 SCHEM 195 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       77 SCHEM 197.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       78 SCHEM 215 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       79 SCHEM 210 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 175.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 173 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 171.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 117.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 132.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 160 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 150 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 155 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 157.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 147.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 162.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 165 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 167.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 170 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 206.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 212.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 179 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 172 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 174.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 170.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
