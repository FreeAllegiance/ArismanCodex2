SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.121-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat272.3-0 ; 
       add_frames_and_strobes-mat273.3-0 ; 
       add_frames_and_strobes-mat274.3-0 ; 
       add_frames_and_strobes-mat275.3-0 ; 
       add_frames_and_strobes-mat276.3-0 ; 
       add_frames_and_strobes-mat277.3-0 ; 
       add_frames_and_strobes-mat278.3-0 ; 
       add_frames_and_strobes-mat279.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       done_text-mat270.3-0 ; 
       done_text-mat271.3-0 ; 
       edit_strobe_materials-mat288.3-0 ; 
       edit_strobe_materials-mat301.3-0 ; 
       edit_strobe_materials-mat314.3-0 ; 
       Rotate-mat321.2-0 ; 
       Rotate-mat322.1-0 ; 
       Rotate-mat323.1-0 ; 
       rotate-mat325.1-0 ; 
       rotate-mat334.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 98     
       Rotate-capgarage1A.1-0 ; 
       Rotate-capgarage1B.1-0 ; 
       Rotate-capgarage1C.1-0 ; 
       Rotate-capgarage1D.1-0 ; 
       Rotate-capgarage1E.1-0 ; 
       Rotate-cube1.3-0 ; 
       Rotate-cube10.1-0 ; 
       Rotate-cube11.1-0 ; 
       Rotate-cube2.2-0 ; 
       Rotate-cube5.1-0 ; 
       Rotate-cube6.1-0 ; 
       Rotate-cube6_1.2-0 ; 
       Rotate-cube6_2.1-0 ; 
       Rotate-cube7.1-0 ; 
       Rotate-cube8.1-0 ; 
       Rotate-cube9.1-0 ; 
       Rotate-cyl1.1-0 ; 
       Rotate-fuselg1.1-0 ; 
       Rotate-fuselg4.1-0 ; 
       Rotate-garage2A.1-0 ; 
       Rotate-garage2B.1-0 ; 
       Rotate-garage2C.1-0 ; 
       Rotate-garage2D.1-0 ; 
       Rotate-garage2E.1-0 ; 
       Rotate-garage3A.1-0 ; 
       Rotate-garage3B.1-0 ; 
       Rotate-garage3C.1-0 ; 
       Rotate-garage3D.1-0 ; 
       Rotate-garage3E.1-0 ; 
       Rotate-launch1.1-0 ; 
       Rotate-null1.2-0 ; 
       Rotate-null2.1-0 ; 
       Rotate-null3.1-0 ; 
       Rotate-null5.5-0 ROOT ; 
       Rotate-null6.1-0 ; 
       Rotate-null7.1-0 ; 
       Rotate-null8.1-0 ; 
       Rotate-nurbs6_1.38-0 ; 
       Rotate-plaza_lights.1-0 ; 
       Rotate-plaza_lights1.1-0 ; 
       Rotate-SS_1.1-0 ; 
       Rotate-SS_13.1-0 ; 
       Rotate-SS_14.1-0 ; 
       Rotate-SS_15.1-0 ; 
       Rotate-SS_16.1-0 ; 
       Rotate-SS_17.1-0 ; 
       Rotate-SS_18.1-0 ; 
       Rotate-SS_19.1-0 ; 
       Rotate-SS_2.1-0 ; 
       Rotate-SS_20.1-0 ; 
       Rotate-SS_21.1-0 ; 
       Rotate-SS_22.1-0 ; 
       Rotate-SS_23.1-0 ; 
       Rotate-SS_24.1-0 ; 
       Rotate-SS_25.1-0 ; 
       Rotate-SS_26.1-0 ; 
       Rotate-SS_27.1-0 ; 
       Rotate-SS_28.1-0 ; 
       Rotate-SS_29.1-0 ; 
       Rotate-SS_30.1-0 ; 
       Rotate-SS_31.1-0 ; 
       Rotate-SS_32.1-0 ; 
       Rotate-SS_33.1-0 ; 
       Rotate-SS_34.1-0 ; 
       Rotate-SS_36.1-0 ; 
       Rotate-SS_37.1-0 ; 
       Rotate-SS_38.1-0 ; 
       Rotate-SS_39.1-0 ; 
       Rotate-SS_40.1-0 ; 
       Rotate-SS_41.1-0 ; 
       Rotate-SS_42.1-0 ; 
       Rotate-SS_43.1-0 ; 
       Rotate-SS_44.1-0 ; 
       Rotate-SS_45.1-0 ; 
       Rotate-SS_46.1-0 ; 
       Rotate-SS_47.1-0 ; 
       Rotate-SS_48.1-0 ; 
       Rotate-SS_50.1-0 ; 
       Rotate-SS_51.1-0 ; 
       Rotate-SS_52.1-0 ; 
       Rotate-SS_53.1-0 ; 
       Rotate-SS_54.1-0 ; 
       Rotate-SS_55.1-0 ; 
       Rotate-SS_56.1-0 ; 
       Rotate-SS_57.1-0 ; 
       Rotate-SS_58.1-0 ; 
       Rotate-SS_59.1-0 ; 
       Rotate-SS_61.1-0 ; 
       Rotate-SS_62.1-0 ; 
       Rotate-SS_63.1-0 ; 
       Rotate-SS_64.1-0 ; 
       Rotate-SS_65.1-0 ; 
       Rotate-SS_66.1-0 ; 
       Rotate-SS_67.1-0 ; 
       Rotate-SS_68.1-0 ; 
       Rotate-SS_69.1-0 ; 
       Rotate-SS_70.1-0 ; 
       Rotate-SS_71.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-rotate.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       add_frames_and_strobes-t2d264.5-0 ; 
       add_frames_and_strobes-t2d265.5-0 ; 
       add_frames_and_strobes-t2d266.5-0 ; 
       add_frames_and_strobes-t2d267.5-0 ; 
       add_frames_and_strobes-t2d268.5-0 ; 
       add_frames_and_strobes-t2d269.5-0 ; 
       add_frames_and_strobes-t2d270.5-0 ; 
       add_frames_and_strobes-t2d271.5-0 ; 
       add_frames_and_strobes-t2d272.5-0 ; 
       add_frames_and_strobes-t2d274.5-0 ; 
       add_frames_and_strobes-t2d275.5-0 ; 
       add_frames_and_strobes-t2d276.5-0 ; 
       add_frames_and_strobes-t2d277.5-0 ; 
       add_frames_and_strobes-t2d278.12-0 ; 
       add_frames_and_strobes-t2d279.12-0 ; 
       add_frames_and_strobes-t2d7.12-0 ; 
       done_text-t2d196.8-0 ; 
       done_text-t2d197.8-0 ; 
       done_text-t2d198.8-0 ; 
       done_text-t2d199.8-0 ; 
       done_text-t2d200.8-0 ; 
       done_text-t2d201.8-0 ; 
       done_text-t2d261.8-0 ; 
       done_text-t2d262.9-0 ; 
       done_text-t2d263.9-0 ; 
       Rotate-t2d280.3-0 ; 
       Rotate-t2d281.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 33 110 ; 
       1 33 110 ; 
       2 33 110 ; 
       3 33 110 ; 
       4 33 110 ; 
       34 33 110 ; 
       35 33 110 ; 
       78 35 110 ; 
       79 35 110 ; 
       80 35 110 ; 
       5 37 110 ; 
       6 37 110 ; 
       7 37 110 ; 
       8 37 110 ; 
       9 37 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       12 37 110 ; 
       13 37 110 ; 
       14 37 110 ; 
       81 35 110 ; 
       82 35 110 ; 
       83 35 110 ; 
       15 37 110 ; 
       84 35 110 ; 
       16 8 110 ; 
       17 5 110 ; 
       18 9 110 ; 
       19 33 110 ; 
       20 33 110 ; 
       21 33 110 ; 
       22 33 110 ; 
       23 33 110 ; 
       24 33 110 ; 
       25 33 110 ; 
       26 33 110 ; 
       27 33 110 ; 
       28 33 110 ; 
       85 35 110 ; 
       29 33 110 ; 
       86 35 110 ; 
       30 33 110 ; 
       31 30 110 ; 
       32 30 110 ; 
       36 33 110 ; 
       37 30 110 ; 
       38 37 110 ; 
       39 37 110 ; 
       87 36 110 ; 
       88 36 110 ; 
       40 38 110 ; 
       89 36 110 ; 
       41 38 110 ; 
       42 38 110 ; 
       43 38 110 ; 
       44 38 110 ; 
       45 38 110 ; 
       46 38 110 ; 
       90 36 110 ; 
       91 36 110 ; 
       47 38 110 ; 
       48 38 110 ; 
       49 38 110 ; 
       92 36 110 ; 
       93 36 110 ; 
       94 36 110 ; 
       95 36 110 ; 
       50 38 110 ; 
       51 38 110 ; 
       96 36 110 ; 
       52 39 110 ; 
       53 39 110 ; 
       54 39 110 ; 
       55 39 110 ; 
       56 39 110 ; 
       57 39 110 ; 
       58 39 110 ; 
       59 39 110 ; 
       60 39 110 ; 
       61 39 110 ; 
       97 36 110 ; 
       62 39 110 ; 
       63 39 110 ; 
       64 34 110 ; 
       65 34 110 ; 
       66 35 110 ; 
       67 34 110 ; 
       68 34 110 ; 
       69 31 110 ; 
       70 31 110 ; 
       71 31 110 ; 
       72 31 110 ; 
       73 32 110 ; 
       74 32 110 ; 
       75 32 110 ; 
       76 32 110 ; 
       77 35 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       78 33 300 ; 
       79 33 300 ; 
       80 33 300 ; 
       5 24 300 ; 
       6 31 300 ; 
       7 32 300 ; 
       8 26 300 ; 
       9 3 300 ; 
       10 12 300 ; 
       11 11 300 ; 
       12 13 300 ; 
       13 14 300 ; 
       14 15 300 ; 
       81 33 300 ; 
       82 33 300 ; 
       83 33 300 ; 
       15 16 300 ; 
       84 33 300 ; 
       16 25 300 ; 
       17 17 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       17 20 300 ; 
       17 21 300 ; 
       17 22 300 ; 
       17 23 300 ; 
       18 4 300 ; 
       18 5 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       18 10 300 ; 
       85 33 300 ; 
       86 33 300 ; 
       37 2 300 ; 
       37 1 300 ; 
       37 0 300 ; 
       87 34 300 ; 
       88 34 300 ; 
       40 27 300 ; 
       89 34 300 ; 
       41 27 300 ; 
       42 27 300 ; 
       43 27 300 ; 
       44 27 300 ; 
       45 27 300 ; 
       46 27 300 ; 
       90 34 300 ; 
       91 34 300 ; 
       47 27 300 ; 
       48 27 300 ; 
       49 27 300 ; 
       92 34 300 ; 
       93 34 300 ; 
       94 34 300 ; 
       95 34 300 ; 
       50 27 300 ; 
       51 27 300 ; 
       96 34 300 ; 
       52 28 300 ; 
       53 28 300 ; 
       54 28 300 ; 
       55 28 300 ; 
       56 28 300 ; 
       57 28 300 ; 
       58 28 300 ; 
       59 28 300 ; 
       60 28 300 ; 
       61 28 300 ; 
       97 34 300 ; 
       62 28 300 ; 
       63 28 300 ; 
       64 29 300 ; 
       65 29 300 ; 
       66 33 300 ; 
       67 29 300 ; 
       68 29 300 ; 
       69 30 300 ; 
       70 30 300 ; 
       71 30 300 ; 
       72 30 300 ; 
       73 30 300 ; 
       74 30 300 ; 
       75 30 300 ; 
       76 30 300 ; 
       77 33 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 22 400 ; 
       9 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 13 401 ; 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       31 25 401 ; 
       32 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 203.75 -2 0 MPRFLG 0 ; 
       35 SCHEM 222.5 -2 0 MPRFLG 0 ; 
       78 SCHEM 215 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       79 SCHEM 217.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       80 SCHEM 220 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       5 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 97.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 100 -6 0 MPRFLG 0 ; 
       8 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 80 -6 0 MPRFLG 0 ; 
       10 SCHEM 95 -6 0 MPRFLG 0 ; 
       11 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 105 -6 0 MPRFLG 0 ; 
       14 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       81 SCHEM 222.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       82 SCHEM 225 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       83 SCHEM 227.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       15 SCHEM 110 -6 0 MPRFLG 0 ; 
       84 SCHEM 230 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       16 SCHEM 65 -8 0 MPRFLG 0 ; 
       17 SCHEM 50 -8 0 MPRFLG 0 ; 
       18 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 32.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 35 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 40 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       85 SCHEM 232.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 2.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       86 SCHEM 235 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 120 -2 0 MPRFLG 0 ; 
       31 SCHEM 183.75 -4 0 MPRFLG 0 ; 
       32 SCHEM 193.75 -4 0 MPRFLG 0 ; 
       33 SCHEM 132.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       36 SCHEM 250 -2 0 MPRFLG 0 ; 
       37 SCHEM 110 -4 0 MPRFLG 0 ; 
       38 SCHEM 126.25 -6 0 MPRFLG 0 ; 
       39 SCHEM 156.25 -6 0 MPRFLG 0 ; 
       87 SCHEM 240 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       88 SCHEM 237.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 242.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 245 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       91 SCHEM 247.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 250 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       93 SCHEM 252.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       94 SCHEM 255 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       95 SCHEM 257.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       96 SCHEM 260 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       97 SCHEM 262.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 200 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 202.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 212.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 205 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 180 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       70 SCHEM 182.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       71 SCHEM 185 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       72 SCHEM 187.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       73 SCHEM 190 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       74 SCHEM 192.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       75 SCHEM 195 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       76 SCHEM 197.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       77 SCHEM 210 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 175.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 173 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 171.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 222.2896 -6.815282 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 160 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 206.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 179 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 240 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 172 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 174.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 170.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
