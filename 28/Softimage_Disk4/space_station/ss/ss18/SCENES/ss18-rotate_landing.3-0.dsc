SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.110-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 61     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat272.3-0 ; 
       add_frames_and_strobes-mat273.3-0 ; 
       add_frames_and_strobes-mat274.3-0 ; 
       add_frames_and_strobes-mat275.3-0 ; 
       add_frames_and_strobes-mat276.3-0 ; 
       add_frames_and_strobes-mat277.3-0 ; 
       add_frames_and_strobes-mat278.3-0 ; 
       add_frames_and_strobes-mat279.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       done_text-mat270.3-0 ; 
       done_text-mat271.3-0 ; 
       edit_strobe_materials-mat287.2-0 ; 
       edit_strobe_materials-mat288.2-0 ; 
       edit_strobe_materials-mat289.2-0 ; 
       edit_strobe_materials-mat290.2-0 ; 
       edit_strobe_materials-mat291.2-0 ; 
       edit_strobe_materials-mat292.2-0 ; 
       edit_strobe_materials-mat293.2-0 ; 
       edit_strobe_materials-mat294.2-0 ; 
       edit_strobe_materials-mat295.2-0 ; 
       edit_strobe_materials-mat296.2-0 ; 
       edit_strobe_materials-mat297.2-0 ; 
       edit_strobe_materials-mat298.2-0 ; 
       edit_strobe_materials-mat299.3-0 ; 
       edit_strobe_materials-mat300.2-0 ; 
       edit_strobe_materials-mat301.2-0 ; 
       edit_strobe_materials-mat302.2-0 ; 
       edit_strobe_materials-mat303.2-0 ; 
       edit_strobe_materials-mat304.2-0 ; 
       edit_strobe_materials-mat305.2-0 ; 
       edit_strobe_materials-mat306.2-0 ; 
       edit_strobe_materials-mat307.2-0 ; 
       edit_strobe_materials-mat308.2-0 ; 
       edit_strobe_materials-mat309.2-0 ; 
       edit_strobe_materials-mat310.2-0 ; 
       edit_strobe_materials-mat311.2-0 ; 
       edit_strobe_materials-mat312.2-0 ; 
       edit_strobe_materials-mat313.2-0 ; 
       edit_strobe_materials-mat314.2-0 ; 
       edit_strobe_materials-mat315.2-0 ; 
       edit_strobe_materials-mat316.2-0 ; 
       edit_strobe_materials-mat317.2-0 ; 
       edit_strobe_materials-mat318.2-0 ; 
       edit_strobe_materials-mat319.2-0 ; 
       rotate_landing-mat321.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 79     
       rotate_landing-beam_lights.1-0 ; 
       rotate_landing-capgarage1A.1-0 ; 
       rotate_landing-capgarage1B.1-0 ; 
       rotate_landing-capgarage1C.1-0 ; 
       rotate_landing-capgarage1D.1-0 ; 
       rotate_landing-capgarage1E.1-0 ; 
       rotate_landing-capgarage2A.1-0 ; 
       rotate_landing-capgarage2B.1-0 ; 
       rotate_landing-capgarage2C.1-0 ; 
       rotate_landing-capgarage2D.1-0 ; 
       rotate_landing-capgarage2E.1-0 ; 
       rotate_landing-cube1.3-0 ; 
       rotate_landing-cube2.2-0 ; 
       rotate_landing-cube5.1-0 ; 
       rotate_landing-cube6.1-0 ; 
       rotate_landing-cube6_1.2-0 ; 
       rotate_landing-cube6_2.1-0 ; 
       rotate_landing-cube7.1-0 ; 
       rotate_landing-cube8.1-0 ; 
       rotate_landing-cube9.1-0 ; 
       rotate_landing-cyl1.1-0 ; 
       rotate_landing-fuselg1.1-0 ; 
       rotate_landing-fuselg4.1-0 ; 
       rotate_landing-garage3A.1-0 ; 
       rotate_landing-garage3B.1-0 ; 
       rotate_landing-garage3C.1-0 ; 
       rotate_landing-garage3D.1-0 ; 
       rotate_landing-garage3E.1-0 ; 
       rotate_landing-garage4A.1-0 ; 
       rotate_landing-garage4B.1-0 ; 
       rotate_landing-garage4C.1-0 ; 
       rotate_landing-garage4D.1-0 ; 
       rotate_landing-garage4E.1-0 ; 
       rotate_landing-launch1.1-0 ; 
       rotate_landing-null1.2-0 ; 
       rotate_landing-null2.1-0 ; 
       rotate_landing-null3.1-0 ; 
       rotate_landing-null5.3-0 ROOT ; 
       rotate_landing-nurbs6_1.38-0 ; 
       rotate_landing-plaza_lights.1-0 ; 
       rotate_landing-plaza_lights1.1-0 ; 
       rotate_landing-SS_1.1-0 ; 
       rotate_landing-SS_13.1-0 ; 
       rotate_landing-SS_14.1-0 ; 
       rotate_landing-SS_15.1-0 ; 
       rotate_landing-SS_16.1-0 ; 
       rotate_landing-SS_17.1-0 ; 
       rotate_landing-SS_18.1-0 ; 
       rotate_landing-SS_19.1-0 ; 
       rotate_landing-SS_2.1-0 ; 
       rotate_landing-SS_20.1-0 ; 
       rotate_landing-SS_21.1-0 ; 
       rotate_landing-SS_22.1-0 ; 
       rotate_landing-SS_23.1-0 ; 
       rotate_landing-SS_24.1-0 ; 
       rotate_landing-SS_25.1-0 ; 
       rotate_landing-SS_26.1-0 ; 
       rotate_landing-SS_27.1-0 ; 
       rotate_landing-SS_28.1-0 ; 
       rotate_landing-SS_29.1-0 ; 
       rotate_landing-SS_30.1-0 ; 
       rotate_landing-SS_31.1-0 ; 
       rotate_landing-SS_32.1-0 ; 
       rotate_landing-SS_33.1-0 ; 
       rotate_landing-SS_34.1-0 ; 
       rotate_landing-SS_35.1-0 ; 
       rotate_landing-SS_36.1-0 ; 
       rotate_landing-SS_37.1-0 ; 
       rotate_landing-SS_38.1-0 ; 
       rotate_landing-SS_39.1-0 ; 
       rotate_landing-SS_40.1-0 ; 
       rotate_landing-SS_41.1-0 ; 
       rotate_landing-SS_42.1-0 ; 
       rotate_landing-SS_43.1-0 ; 
       rotate_landing-SS_44.1-0 ; 
       rotate_landing-SS_45.1-0 ; 
       rotate_landing-SS_46.1-0 ; 
       rotate_landing-SS_47.1-0 ; 
       rotate_landing-SS_48.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-rotate_landing.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames_and_strobes-t2d264.4-0 ; 
       add_frames_and_strobes-t2d265.4-0 ; 
       add_frames_and_strobes-t2d266.4-0 ; 
       add_frames_and_strobes-t2d267.4-0 ; 
       add_frames_and_strobes-t2d268.4-0 ; 
       add_frames_and_strobes-t2d269.4-0 ; 
       add_frames_and_strobes-t2d270.4-0 ; 
       add_frames_and_strobes-t2d271.4-0 ; 
       add_frames_and_strobes-t2d272.4-0 ; 
       add_frames_and_strobes-t2d274.4-0 ; 
       add_frames_and_strobes-t2d275.4-0 ; 
       add_frames_and_strobes-t2d276.4-0 ; 
       add_frames_and_strobes-t2d277.4-0 ; 
       add_frames_and_strobes-t2d278.11-0 ; 
       add_frames_and_strobes-t2d279.11-0 ; 
       add_frames_and_strobes-t2d7.11-0 ; 
       done_text-t2d196.7-0 ; 
       done_text-t2d197.7-0 ; 
       done_text-t2d198.7-0 ; 
       done_text-t2d199.7-0 ; 
       done_text-t2d200.7-0 ; 
       done_text-t2d201.7-0 ; 
       done_text-t2d261.7-0 ; 
       done_text-t2d262.8-0 ; 
       done_text-t2d263.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 38 110 ; 
       11 38 110 ; 
       12 38 110 ; 
       13 38 110 ; 
       14 38 110 ; 
       15 38 110 ; 
       16 38 110 ; 
       17 38 110 ; 
       18 38 110 ; 
       19 38 110 ; 
       20 12 110 ; 
       21 11 110 ; 
       22 13 110 ; 
       23 37 110 ; 
       24 37 110 ; 
       25 37 110 ; 
       26 37 110 ; 
       27 37 110 ; 
       28 37 110 ; 
       29 37 110 ; 
       30 37 110 ; 
       31 37 110 ; 
       32 37 110 ; 
       1 37 110 ; 
       2 37 110 ; 
       3 37 110 ; 
       4 37 110 ; 
       5 37 110 ; 
       6 37 110 ; 
       7 37 110 ; 
       8 37 110 ; 
       9 37 110 ; 
       10 37 110 ; 
       33 37 110 ; 
       34 37 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       38 34 110 ; 
       39 38 110 ; 
       40 38 110 ; 
       41 39 110 ; 
       42 39 110 ; 
       43 39 110 ; 
       44 39 110 ; 
       45 39 110 ; 
       46 39 110 ; 
       47 39 110 ; 
       48 39 110 ; 
       49 39 110 ; 
       50 39 110 ; 
       51 39 110 ; 
       52 39 110 ; 
       53 40 110 ; 
       54 40 110 ; 
       55 40 110 ; 
       56 40 110 ; 
       57 40 110 ; 
       58 40 110 ; 
       59 40 110 ; 
       60 40 110 ; 
       61 40 110 ; 
       62 40 110 ; 
       63 40 110 ; 
       64 40 110 ; 
       65 0 110 ; 
       66 0 110 ; 
       67 0 110 ; 
       68 0 110 ; 
       69 0 110 ; 
       70 0 110 ; 
       71 35 110 ; 
       72 35 110 ; 
       73 35 110 ; 
       74 35 110 ; 
       75 36 110 ; 
       76 36 110 ; 
       77 36 110 ; 
       78 36 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 53 300 ; 
       11 24 300 ; 
       12 26 300 ; 
       13 3 300 ; 
       14 12 300 ; 
       15 11 300 ; 
       16 13 300 ; 
       17 14 300 ; 
       18 15 300 ; 
       19 16 300 ; 
       20 25 300 ; 
       21 17 300 ; 
       21 18 300 ; 
       21 19 300 ; 
       21 20 300 ; 
       21 21 300 ; 
       21 22 300 ; 
       21 23 300 ; 
       22 4 300 ; 
       22 5 300 ; 
       22 6 300 ; 
       22 7 300 ; 
       22 8 300 ; 
       22 9 300 ; 
       22 10 300 ; 
       38 2 300 ; 
       38 1 300 ; 
       38 0 300 ; 
       39 27 300 ; 
       40 40 300 ; 
       41 39 300 ; 
       42 28 300 ; 
       43 29 300 ; 
       44 30 300 ; 
       45 31 300 ; 
       46 32 300 ; 
       47 33 300 ; 
       48 34 300 ; 
       49 38 300 ; 
       50 35 300 ; 
       51 36 300 ; 
       52 37 300 ; 
       53 41 300 ; 
       54 42 300 ; 
       55 43 300 ; 
       56 44 300 ; 
       57 45 300 ; 
       58 46 300 ; 
       59 47 300 ; 
       60 48 300 ; 
       61 49 300 ; 
       62 50 300 ; 
       63 51 300 ; 
       64 52 300 ; 
       65 59 300 ; 
       66 58 300 ; 
       67 57 300 ; 
       68 56 300 ; 
       69 55 300 ; 
       70 54 300 ; 
       71 60 300 ; 
       72 60 300 ; 
       73 60 300 ; 
       74 60 300 ; 
       75 60 300 ; 
       76 60 300 ; 
       77 60 300 ; 
       78 60 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       11 22 400 ; 
       13 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 13 401 ; 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 143.75 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 55 -6 0 MPRFLG 0 ; 
       12 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 60 -6 0 MPRFLG 0 ; 
       14 SCHEM 65 -6 0 MPRFLG 0 ; 
       15 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 70 -6 0 MPRFLG 0 ; 
       18 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 75 -6 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 55 -8 0 MPRFLG 0 ; 
       22 SCHEM 60 -8 0 MPRFLG 0 ; 
       23 SCHEM 5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 10 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 12.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 35 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 40 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 45 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 50 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 52.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 2.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 112.5 -2 0 MPRFLG 0 ; 
       35 SCHEM 156.25 -4 0 MPRFLG 0 ; 
       36 SCHEM 166.25 -4 0 MPRFLG 0 ; 
       37 SCHEM 86.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       38 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 91.25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 121.25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 77.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 95 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 85 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 87.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 90 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 92.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 82.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 97.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 80 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 100 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 102.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 105 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 125 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 115 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 117.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 120 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 122.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 112.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 127.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       60 SCHEM 130 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       61 SCHEM 132.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 135 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 110 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 107.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 137.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 140 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       67 SCHEM 142.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       68 SCHEM 145 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       69 SCHEM 147.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       70 SCHEM 150 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       71 SCHEM 152.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       72 SCHEM 155 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 157.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 160 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 162.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       76 SCHEM 165 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       77 SCHEM 167.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       78 SCHEM 170 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 151.0747 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 151.0747 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 161.8567 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.07468 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 58.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 58.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 58.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 58.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 58.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 58.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 58.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 61.07468 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 63.57468 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 66.07468 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 68.57468 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 71.07468 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 73.57468 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 53.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 53.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 53.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 53.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 53.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 53.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 53.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 56.07468 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 56.07468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 58.57468 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 106.0747 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 93.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 83.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 86.07468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 88.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 91.07468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 81.07468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 96.07468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 98.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 101.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 103.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 78.57468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 76.07468 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 136.0747 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 123.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 113.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 116.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 118.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 121.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 111.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 126.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 128.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 131.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 133.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 108.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 106.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 137.8104 -11.51394 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 148.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 146.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 143.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 141.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 138.5747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 136.0747 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 151.0747 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.07468 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 58.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 58.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 58.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 58.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 58.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 58.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 61.07468 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 63.57468 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 66.07468 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 68.57468 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 71.07468 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 73.57468 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 151.0747 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 151.0747 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 160.8567 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 53.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 53.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 53.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 53.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 53.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 53.57468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 56.07468 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 56.07468 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 58.57468 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
