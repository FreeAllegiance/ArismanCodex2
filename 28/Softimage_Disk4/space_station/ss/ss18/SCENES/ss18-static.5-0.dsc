SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.68-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat272.3-0 ; 
       add_frames_and_strobes-mat273.3-0 ; 
       add_frames_and_strobes-mat274.3-0 ; 
       add_frames_and_strobes-mat275.3-0 ; 
       add_frames_and_strobes-mat276.3-0 ; 
       add_frames_and_strobes-mat277.3-0 ; 
       add_frames_and_strobes-mat278.3-0 ; 
       add_frames_and_strobes-mat279.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       done_text-mat270.3-0 ; 
       done_text-mat271.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       static-cube1.3-0 ; 
       static-cube2.2-0 ; 
       static-cube5.1-0 ; 
       static-cube6.1-0 ; 
       static-cube6_1.2-0 ; 
       static-cube6_2.1-0 ; 
       static-cube7.1-0 ; 
       static-cube8.1-0 ; 
       static-cube9.1-0 ; 
       static-cyl1.1-0 ; 
       static-fuselg1.1-0 ; 
       static-fuselg4.1-0 ; 
       static-null1.1-0 ROOT ; 
       static-nurbs6_1.38-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss18/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/ss18/PICTURES/ss18 ; 
       E:/Pete_Data2/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames_and_strobes-t2d264.4-0 ; 
       add_frames_and_strobes-t2d265.4-0 ; 
       add_frames_and_strobes-t2d266.4-0 ; 
       add_frames_and_strobes-t2d267.4-0 ; 
       add_frames_and_strobes-t2d268.4-0 ; 
       add_frames_and_strobes-t2d269.4-0 ; 
       add_frames_and_strobes-t2d270.4-0 ; 
       add_frames_and_strobes-t2d271.4-0 ; 
       add_frames_and_strobes-t2d272.4-0 ; 
       add_frames_and_strobes-t2d274.4-0 ; 
       add_frames_and_strobes-t2d275.4-0 ; 
       add_frames_and_strobes-t2d276.4-0 ; 
       add_frames_and_strobes-t2d277.4-0 ; 
       add_frames_and_strobes-t2d278.11-0 ; 
       add_frames_and_strobes-t2d279.11-0 ; 
       add_frames_and_strobes-t2d7.11-0 ; 
       done_text-t2d196.7-0 ; 
       done_text-t2d197.7-0 ; 
       done_text-t2d198.7-0 ; 
       done_text-t2d199.7-0 ; 
       done_text-t2d200.7-0 ; 
       done_text-t2d201.7-0 ; 
       done_text-t2d261.7-0 ; 
       done_text-t2d262.8-0 ; 
       done_text-t2d263.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 1 110 ; 
       10 0 110 ; 
       11 2 110 ; 
       13 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 24 300 ; 
       1 26 300 ; 
       2 3 300 ; 
       3 12 300 ; 
       4 11 300 ; 
       5 13 300 ; 
       6 14 300 ; 
       7 15 300 ; 
       8 16 300 ; 
       9 25 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       10 23 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       13 2 300 ; 
       13 1 300 ; 
       13 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 22 400 ; 
       2 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 13 401 ; 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 20 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 96.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 95.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
