SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.124-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       add_frames_and_strobes-bay_1.3-0 ; 
       add_frames_and_strobes-bay_one1.3-0 ; 
       add_frames_and_strobes-default5.3-0 ; 
       add_frames_and_strobes-mat272.3-0 ; 
       add_frames_and_strobes-mat273.3-0 ; 
       add_frames_and_strobes-mat274.3-0 ; 
       add_frames_and_strobes-mat275.3-0 ; 
       add_frames_and_strobes-mat276.3-0 ; 
       add_frames_and_strobes-mat277.3-0 ; 
       add_frames_and_strobes-mat278.3-0 ; 
       add_frames_and_strobes-mat279.3-0 ; 
       add_frames_and_strobes-mat280.3-0 ; 
       add_frames_and_strobes-mat281.3-0 ; 
       add_frames_and_strobes-mat283.3-0 ; 
       add_frames_and_strobes-mat284.3-0 ; 
       add_frames_and_strobes-mat285.3-0 ; 
       add_frames_and_strobes-mat286.3-0 ; 
       done_text-mat204.3-0 ; 
       done_text-mat205.3-0 ; 
       done_text-mat206.3-0 ; 
       done_text-mat207.3-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat269.3-0 ; 
       Rotate-mat322.1-0 ; 
       Rotate-mat323.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       Rotate-cube1.3-0 ; 
       Rotate-cube10.1-0 ; 
       Rotate-cube11.1-0 ; 
       Rotate-cube5.1-0 ; 
       Rotate-cube6.1-0 ; 
       Rotate-cube6_1.2-0 ; 
       Rotate-cube6_2.1-0 ; 
       Rotate-cube7.1-0 ; 
       Rotate-cube8.1-0 ; 
       Rotate-cube9.1-0 ; 
       Rotate-fuselg1.1-0 ; 
       Rotate-fuselg4.1-0 ; 
       Rotate-null1.2-0 ; 
       Rotate-null5.6-0 ROOT ; 
       Rotate-nurbs6_1.38-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss18/PICTURES/ss18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss18-static.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames_and_strobes-t2d264.5-0 ; 
       add_frames_and_strobes-t2d265.5-0 ; 
       add_frames_and_strobes-t2d266.5-0 ; 
       add_frames_and_strobes-t2d267.5-0 ; 
       add_frames_and_strobes-t2d268.5-0 ; 
       add_frames_and_strobes-t2d269.5-0 ; 
       add_frames_and_strobes-t2d270.5-0 ; 
       add_frames_and_strobes-t2d271.5-0 ; 
       add_frames_and_strobes-t2d272.5-0 ; 
       add_frames_and_strobes-t2d274.5-0 ; 
       add_frames_and_strobes-t2d275.5-0 ; 
       add_frames_and_strobes-t2d276.5-0 ; 
       add_frames_and_strobes-t2d277.5-0 ; 
       add_frames_and_strobes-t2d278.12-0 ; 
       add_frames_and_strobes-t2d279.12-0 ; 
       add_frames_and_strobes-t2d7.12-0 ; 
       done_text-t2d196.8-0 ; 
       done_text-t2d197.8-0 ; 
       done_text-t2d198.8-0 ; 
       done_text-t2d199.8-0 ; 
       done_text-t2d200.8-0 ; 
       done_text-t2d201.8-0 ; 
       done_text-t2d261.8-0 ; 
       Rotate-t2d280.3-0 ; 
       Rotate-t2d281.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 14 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 14 110 ; 
       5 14 110 ; 
       6 14 110 ; 
       7 14 110 ; 
       8 14 110 ; 
       9 14 110 ; 
       10 0 110 ; 
       11 3 110 ; 
       12 13 110 ; 
       14 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 24 300 ; 
       1 25 300 ; 
       2 26 300 ; 
       3 3 300 ; 
       4 12 300 ; 
       5 11 300 ; 
       6 13 300 ; 
       7 14 300 ; 
       8 15 300 ; 
       9 16 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       10 23 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       14 2 300 ; 
       14 1 300 ; 
       14 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 22 400 ; 
       3 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 13 401 ; 
       2 15 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 20 -6 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 25 -6 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 MPRFLG 0 ; 
       12 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       13 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 13.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 76.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 75.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 75.75 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 25.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 74.25 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
