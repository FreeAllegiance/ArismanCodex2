SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss20-ss20.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       hub_trailer_F-cam_int1.1-0 ROOT ; 
       hub_trailer_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       hub_trailer_F-light13.1-0 ROOT ; 
       hub_trailer_F-light14.1-0 ROOT ; 
       hub_trailer_F-light15.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       hub_trailer_F-default7.1-0 ; 
       hub_trailer_F-mat1.1-0 ; 
       hub_trailer_F-mat10.1-0 ; 
       hub_trailer_F-mat11.1-0 ; 
       hub_trailer_F-mat12.1-0 ; 
       hub_trailer_F-mat13.1-0 ; 
       hub_trailer_F-mat14.1-0 ; 
       hub_trailer_F-mat2.1-0 ; 
       hub_trailer_F-mat23.1-0 ; 
       hub_trailer_F-mat25.1-0 ; 
       hub_trailer_F-mat26.1-0 ; 
       hub_trailer_F-mat27.1-0 ; 
       hub_trailer_F-mat28.1-0 ; 
       hub_trailer_F-mat29.1-0 ; 
       hub_trailer_F-mat3.1-0 ; 
       hub_trailer_F-mat30.1-0 ; 
       hub_trailer_F-mat31.1-0 ; 
       hub_trailer_F-mat32.1-0 ; 
       hub_trailer_F-mat33.1-0 ; 
       hub_trailer_F-mat38.1-0 ; 
       hub_trailer_F-mat4.1-0 ; 
       hub_trailer_F-mat52.1-0 ; 
       hub_trailer_F-mat53.1-0 ; 
       hub_trailer_F-mat54.1-0 ; 
       hub_trailer_F-mat55.1-0 ; 
       hub_trailer_F-mat56.1-0 ; 
       hub_trailer_F-mat57.1-0 ; 
       hub_trailer_F-mat58.1-0 ; 
       hub_trailer_F-mat59.1-0 ; 
       hub_trailer_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       ss20-arfuselg.1-0 ; 
       ss20-doccon.1-0 ; 
       ss20-fuselg.1-0 ; 
       ss20-lafuselg.1-0 ; 
       ss20-lantenn.1-0 ; 
       ss20-lwingz.1-0 ; 
       ss20-rantenn.1-0 ; 
       ss20-rbfuselg.1-0 ; 
       ss20-rfuselg.1-0 ; 
       ss20-rwingz.1-0 ; 
       ss20-ss20.1-0 ROOT ; 
       ss20-SSl.1-0 ; 
       ss20-SSr.1-0 ; 
       ss20-SSt.1-0 ; 
       ss20-tafuselg0.1-0 ; 
       ss20-tafuselg1.1-0 ; 
       ss20-tantenn.1-0 ; 
       ss20-turatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss20/PICTURES/ss20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss20-hub_trailer_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       hub_trailer_F-t2d1.1-0 ; 
       hub_trailer_F-t2d10.1-0 ; 
       hub_trailer_F-t2d11.1-0 ; 
       hub_trailer_F-t2d19.1-0 ; 
       hub_trailer_F-t2d2.1-0 ; 
       hub_trailer_F-t2d21.1-0 ; 
       hub_trailer_F-t2d22.1-0 ; 
       hub_trailer_F-t2d23.1-0 ; 
       hub_trailer_F-t2d24.1-0 ; 
       hub_trailer_F-t2d25.1-0 ; 
       hub_trailer_F-t2d26.1-0 ; 
       hub_trailer_F-t2d27.1-0 ; 
       hub_trailer_F-t2d28.1-0 ; 
       hub_trailer_F-t2d34.1-0 ; 
       hub_trailer_F-t2d35.1-0 ; 
       hub_trailer_F-t2d36.1-0 ; 
       hub_trailer_F-t2d37.1-0 ; 
       hub_trailer_F-t2d38.1-0 ; 
       hub_trailer_F-t2d39.1-0 ; 
       hub_trailer_F-t2d7.1-0 ; 
       hub_trailer_F-t2d8.1-0 ; 
       hub_trailer_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 4 110 ; 
       12 6 110 ; 
       13 16 110 ; 
       0 2 110 ; 
       1 2 110 ; 
       2 10 110 ; 
       3 2 110 ; 
       4 8 110 ; 
       5 2 110 ; 
       6 8 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       14 2 110 ; 
       15 14 110 ; 
       16 8 110 ; 
       17 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       11 28 300 ; 
       12 27 300 ; 
       13 26 300 ; 
       0 13 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       2 19 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       4 4 300 ; 
       5 1 300 ; 
       5 7 300 ; 
       6 5 300 ; 
       7 20 300 ; 
       8 29 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       9 14 300 ; 
       15 0 300 ; 
       15 8 300 ; 
       16 6 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       10 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 19 401 ; 
       3 20 401 ; 
       4 21 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 0 401 ; 
       8 3 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       14 4 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       20 18 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       24 16 401 ; 
       25 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 5 -8 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 MPRFLG 0 ; 
       6 SCHEM 5 -6 0 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 29 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 2 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
