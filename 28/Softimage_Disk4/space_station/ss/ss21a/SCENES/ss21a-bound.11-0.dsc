SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.58-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       bound-bound1.1-0 ; 
       bound-bound10.1-0 ; 
       bound-bound11.1-0 ; 
       bound-bound2.1-0 ; 
       bound-bound3.1-0 ; 
       bound-bound4.1-0 ; 
       bound-bound5.1-0 ; 
       bound-bound6.1-0 ; 
       bound-bound7.1-0 ; 
       bound-bound8.1-0 ; 
       bound-bound9.1-0 ; 
       bound-root.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss21a-bound.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 11 110 ; 
       9 11 110 ; 
       0 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       8 11 110 ; 
       6 11 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       7 11 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 MPRFLG 0 ; 
       11 SCHEM 15 0 0 SRT 1 1 1 0 0 0 8.28805 10.32402 7.412924 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
