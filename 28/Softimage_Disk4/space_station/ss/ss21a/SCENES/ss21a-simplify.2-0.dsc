SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       simplify-null1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.5-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 136     
       mass_carg_con_F-mat110.1-0 ; 
       mass_carg_con_F-mat111.1-0 ; 
       mass_carg_con_F-mat112.1-0 ; 
       mass_carg_con_F-mat120.1-0 ; 
       mass_carg_con_F-mat124.1-0 ; 
       mass_carg_con_F-mat125.1-0 ; 
       mass_carg_con_F-mat126.1-0 ; 
       mass_carg_con_F-mat131.1-0 ; 
       mass_carg_con_F-mat135.1-0 ; 
       mass_carg_con_F-mat136.1-0 ; 
       mass_carg_con_F-mat137.1-0 ; 
       mass_carg_con_F-mat218.1-0 ; 
       mass_carg_con_F-mat219.1-0 ; 
       mass_carg_con_F-mat220.1-0 ; 
       mass_carg_con_F-mat221.1-0 ; 
       mass_carg_con_F-mat222.1-0 ; 
       mass_carg_con_F-mat66.1-0 ; 
       mass_carg_con_F-mat92.1-0 ; 
       mass_carg_con_F-nose_white-center12.1-0 ; 
       mass_carg_con_F-nose_white-center19.1-0 ; 
       mass_carg_con_F-nose_white-center21.1-0 ; 
       mass_carg_con_F-nose_white-center22.1-0 ; 
       mass_carg_con_F-nose_white-center6.1-0 ; 
       mass_carg_con_F-nose_white-center8.1-0 ; 
       simplify-mat1.1-0 ; 
       simplify-mat10.1-0 ; 
       simplify-mat11.1-0 ; 
       simplify-mat12.1-0 ; 
       simplify-mat13.1-0 ; 
       simplify-mat14.1-0 ; 
       simplify-mat15.1-0 ; 
       simplify-mat16.1-0 ; 
       simplify-mat17.1-0 ; 
       simplify-mat18.1-0 ; 
       simplify-mat19.1-0 ; 
       simplify-mat2.1-0 ; 
       simplify-mat20.1-0 ; 
       simplify-mat21.1-0 ; 
       simplify-mat22.1-0 ; 
       simplify-mat23.1-0 ; 
       simplify-mat24.1-0 ; 
       simplify-mat25.1-0 ; 
       simplify-mat26.1-0 ; 
       simplify-mat27.1-0 ; 
       simplify-mat28.1-0 ; 
       simplify-mat29.1-0 ; 
       simplify-mat3.1-0 ; 
       simplify-mat30.1-0 ; 
       simplify-mat31.1-0 ; 
       simplify-mat32.1-0 ; 
       simplify-mat33.1-0 ; 
       simplify-mat34.1-0 ; 
       simplify-mat35.1-0 ; 
       simplify-mat36.1-0 ; 
       simplify-mat39.1-0 ; 
       simplify-mat4.1-0 ; 
       simplify-mat40.1-0 ; 
       simplify-mat41.1-0 ; 
       simplify-mat42.1-0 ; 
       simplify-mat43.1-0 ; 
       simplify-mat44.1-0 ; 
       simplify-mat45.1-0 ; 
       simplify-mat46.1-0 ; 
       simplify-mat47.1-0 ; 
       simplify-mat48.1-0 ; 
       simplify-mat49.1-0 ; 
       simplify-mat5.1-0 ; 
       simplify-mat50.1-0 ; 
       simplify-mat51.1-0 ; 
       simplify-mat52.1-0 ; 
       simplify-mat53.1-0 ; 
       simplify-mat54.1-0 ; 
       simplify-mat55.1-0 ; 
       simplify-mat56.1-0 ; 
       simplify-mat57.1-0 ; 
       simplify-mat58.1-0 ; 
       simplify-mat59.1-0 ; 
       simplify-mat6.1-0 ; 
       simplify-mat60.1-0 ; 
       simplify-mat61.1-0 ; 
       simplify-mat62.1-0 ; 
       simplify-mat63.1-0 ; 
       simplify-mat64.1-0 ; 
       simplify-mat65.1-0 ; 
       simplify-mat66.1-0 ; 
       simplify-mat67.1-0 ; 
       simplify-mat7.1-0 ; 
       simplify-mat74.1-0 ; 
       simplify-mat75.1-0 ; 
       simplify-mat76.1-0 ; 
       simplify-mat77.1-0 ; 
       simplify-mat78.1-0 ; 
       simplify-mat79.1-0 ; 
       simplify-mat8.1-0 ; 
       simplify-mat80.1-0 ; 
       simplify-mat85.1-0 ; 
       simplify-mat9.1-0 ; 
       simplify-nose_white-center.1-1.1-0 ; 
       simplify-nose_white-center.1-14.1-0 ; 
       simplify-nose_white-center.1-15.1-0 ; 
       simplify-nose_white-center.1-16.1-0 ; 
       simplify-nose_white-center.1-17.1-0 ; 
       simplify-nose_white-center.1-18.1-0 ; 
       simplify-port_red-left.1-0.1-0 ; 
       simplify-port_red-left.1-1.1-0 ; 
       simplify-starbord_green-right.1-0.1-0 ; 
       simplify-starbord_green-right.1-0_1.1-0 ; 
       simplify-starbord_green-right.1-0_2.1-0 ; 
       simplify-starbord_green-right.1-0_3.1-0 ; 
       simplify-starbord_green-right.1-0_4.1-0 ; 
       simplify-starbord_green-right.1-0_5.1-0 ; 
       simplify-starbord_green-right.1-0_6.1-0 ; 
       simplify-starbord_green-right.1-1.1-0 ; 
       simplify-starbord_green-right.1-10.1-0 ; 
       simplify-starbord_green-right.1-11.1-0 ; 
       simplify-starbord_green-right.1-12.1-0 ; 
       simplify-starbord_green-right.1-13.1-0 ; 
       simplify-starbord_green-right.1-14.1-0 ; 
       simplify-starbord_green-right.1-15.1-0 ; 
       simplify-starbord_green-right.1-16.1-0 ; 
       simplify-starbord_green-right.1-17.1-0 ; 
       simplify-starbord_green-right.1-18.1-0 ; 
       simplify-starbord_green-right.1-19.1-0 ; 
       simplify-starbord_green-right.1-2.1-0 ; 
       simplify-starbord_green-right.1-20.1-0 ; 
       simplify-starbord_green-right.1-21.1-0 ; 
       simplify-starbord_green-right.1-22.1-0 ; 
       simplify-starbord_green-right.1-23.1-0 ; 
       simplify-starbord_green-right.1-24.1-0 ; 
       simplify-starbord_green-right.1-3.1-0 ; 
       simplify-starbord_green-right.1-4.1-0 ; 
       simplify-starbord_green-right.1-5.1-0 ; 
       simplify-starbord_green-right.1-6.1-0 ; 
       simplify-starbord_green-right.1-7.1-0 ; 
       simplify-starbord_green-right.1-8.1-0 ; 
       simplify-starbord_green-right.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 95     
       simplify-aantenn1.1-0 ; 
       simplify-aantenn2.1-0 ; 
       simplify-aantenn3.1-0 ; 
       simplify-blndpad0.1-0 ; 
       simplify-blndpad1.1-0 ; 
       simplify-blndpad2.1-0 ; 
       simplify-blndpad3.1-0 ; 
       simplify-blndpad4.1-0 ; 
       simplify-corrdr.1-0 ; 
       simplify-crgatt.1-0 ; 
       simplify-docbay.2-0 ; 
       simplify-doccon1.1-0 ; 
       simplify-doccon2.1-0 ; 
       simplify-fantenn0.1-0 ; 
       simplify-fantenn1.1-0 ; 
       simplify-fantenn2.1-0 ; 
       simplify-fantenn3.1-0 ; 
       simplify-fantenn4.1-0 ; 
       simplify-fantenn5.1-0 ; 
       simplify-ffuselg.1-0 ; 
       simplify-fuselg.1-0 ; 
       simplify-fuselg1.1-0 ; 
       simplify-fuselg2.1-0 ; 
       simplify-fuselg3.1-0 ; 
       simplify-hubcon1.1-0 ; 
       simplify-hubcon2.1-0 ; 
       simplify-null1.2-0 ROOT ; 
       simplify-platfm2.1-0 ; 
       simplify-platfm3.1-0 ; 
       simplify-ss21a.2-0 ; 
       simplify-SSa1.1-0 ; 
       simplify-SSa2.1-0 ; 
       simplify-SSa3.1-0 ; 
       simplify-SSa4.1-0 ; 
       simplify-SSa5.1-0 ; 
       simplify-SSaa1.1-0 ; 
       simplify-SSc1.1-0 ; 
       simplify-SSc2.1-0 ; 
       simplify-SSc3.1-0 ; 
       simplify-SSc4.1-0 ; 
       simplify-SSc5.1-0 ; 
       simplify-SSc6.1-0 ; 
       simplify-SSfb0.1-0 ; 
       simplify-SSfb1.1-0 ; 
       simplify-SSfb2.1-0 ; 
       simplify-SSg0.1-0 ; 
       simplify-SSg1.1-0 ; 
       simplify-SSg2.1-0 ; 
       simplify-SSg3.1-0 ; 
       simplify-SSg5.1-0 ; 
       simplify-SSg6.1-0 ; 
       simplify-SSg7.1-0 ; 
       simplify-SSgb0.1-0 ; 
       simplify-SSgb1.1-0 ; 
       simplify-SSgb2.1-0 ; 
       simplify-SSp1.2-0 ; 
       simplify-SSp2.1-0 ; 
       simplify-SSp3.1-0 ; 
       simplify-SSp4.1-0 ; 
       simplify-SSp5.1-0 ; 
       simplify-SSp6.1-0 ; 
       simplify-SSp7.1-0 ; 
       simplify-SSp8.1-0 ; 
       simplify-SSta0.1-0 ; 
       simplify-SSta1.3-0 ; 
       simplify-SSta2.1-0 ; 
       simplify-SSta3.1-0 ; 
       simplify-SSta4.1-0 ; 
       simplify-SSta5.1-0 ; 
       simplify-SStb0.1-0 ; 
       simplify-SStb1.1-0 ; 
       simplify-SStb2.1-0 ; 
       simplify-SStb3.1-0 ; 
       simplify-SStb4.1-0 ; 
       simplify-SStb5.1-0 ; 
       simplify-SStc0.1-0 ; 
       simplify-SStc1.1-0 ; 
       simplify-SStc2.1-0 ; 
       simplify-SStc3.1-0 ; 
       simplify-SStc4.1-0 ; 
       simplify-SStc5.1-0 ; 
       simplify-tcontwr.3-0 ; 
       simplify-tlndpad1.1-0 ; 
       simplify-tlndpad2.1-0 ; 
       simplify-tlndpad3.1-0 ; 
       simplify-turatt0.1-0 ; 
       simplify-turatt1.1-0 ; 
       simplify-turatt2.1-0 ; 
       simplify-turatt3.1-0 ; 
       simplify-turatt4.1-0 ; 
       simplify-turatt5.1-0 ; 
       simplify-utl28a.1-0 ; 
       simplify-wepemt0.1-0 ; 
       simplify-wepemt1.1-0 ; 
       simplify-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss21a/PICTURES/ss21a ; 
       E:/Pete_Data2/space_station/ss/ss21a/PICTURES/utl14a ; 
       E:/Pete_Data2/space_station/ss/ss21a/PICTURES/utl14b ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss21a-simplify.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 86     
       mass_carg_con_F-t2d105.1-0 ; 
       mass_carg_con_F-t2d106.1-0 ; 
       mass_carg_con_F-t2d107.1-0 ; 
       mass_carg_con_F-t2d119.1-0 ; 
       mass_carg_con_F-t2d120.1-0 ; 
       mass_carg_con_F-t2d121.1-0 ; 
       mass_carg_con_F-t2d129.1-0 ; 
       mass_carg_con_F-t2d130.1-0 ; 
       mass_carg_con_F-t2d131.1-0 ; 
       mass_carg_con_F-t2d205.1-0 ; 
       mass_carg_con_F-t2d206.1-0 ; 
       mass_carg_con_F-t2d207.1-0 ; 
       mass_carg_con_F-t2d208.1-0 ; 
       mass_carg_con_F-t2d209.1-0 ; 
       simplify-t2d1.1-0 ; 
       simplify-t2d10.1-0 ; 
       simplify-t2d11.1-0 ; 
       simplify-t2d12.1-0 ; 
       simplify-t2d13.1-0 ; 
       simplify-t2d14.1-0 ; 
       simplify-t2d15.1-0 ; 
       simplify-t2d16.1-0 ; 
       simplify-t2d17.1-0 ; 
       simplify-t2d18.1-0 ; 
       simplify-t2d19.1-0 ; 
       simplify-t2d2.1-0 ; 
       simplify-t2d20.1-0 ; 
       simplify-t2d21.1-0 ; 
       simplify-t2d22.1-0 ; 
       simplify-t2d23.1-0 ; 
       simplify-t2d24.1-0 ; 
       simplify-t2d25.1-0 ; 
       simplify-t2d26.1-0 ; 
       simplify-t2d27.1-0 ; 
       simplify-t2d28.1-0 ; 
       simplify-t2d29.1-0 ; 
       simplify-t2d3.1-0 ; 
       simplify-t2d30.1-0 ; 
       simplify-t2d31.1-0 ; 
       simplify-t2d32.1-0 ; 
       simplify-t2d33.1-0 ; 
       simplify-t2d34.1-0 ; 
       simplify-t2d35.1-0 ; 
       simplify-t2d38.1-0 ; 
       simplify-t2d39.1-0 ; 
       simplify-t2d4.1-0 ; 
       simplify-t2d40.1-0 ; 
       simplify-t2d41.1-0 ; 
       simplify-t2d42.1-0 ; 
       simplify-t2d43.1-0 ; 
       simplify-t2d44.1-0 ; 
       simplify-t2d45.1-0 ; 
       simplify-t2d46.1-0 ; 
       simplify-t2d47.1-0 ; 
       simplify-t2d48.1-0 ; 
       simplify-t2d49.1-0 ; 
       simplify-t2d5.1-0 ; 
       simplify-t2d50.1-0 ; 
       simplify-t2d51.1-0 ; 
       simplify-t2d52.1-0 ; 
       simplify-t2d53.1-0 ; 
       simplify-t2d54.1-0 ; 
       simplify-t2d55.1-0 ; 
       simplify-t2d56.1-0 ; 
       simplify-t2d57.1-0 ; 
       simplify-t2d58.1-0 ; 
       simplify-t2d59.1-0 ; 
       simplify-t2d6.1-0 ; 
       simplify-t2d60.1-0 ; 
       simplify-t2d61.1-0 ; 
       simplify-t2d62.1-0 ; 
       simplify-t2d63.1-0 ; 
       simplify-t2d64.1-0 ; 
       simplify-t2d65.1-0 ; 
       simplify-t2d66.1-0 ; 
       simplify-t2d7.1-0 ; 
       simplify-t2d73.1-0 ; 
       simplify-t2d74.1-0 ; 
       simplify-t2d75.1-0 ; 
       simplify-t2d76.1-0 ; 
       simplify-t2d77.1-0 ; 
       simplify-t2d78.1-0 ; 
       simplify-t2d79.1-0 ; 
       simplify-t2d8.1-0 ; 
       simplify-t2d82.1-0 ; 
       simplify-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 81 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 10 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 81 110 ; 
       10 20 110 ; 
       11 24 110 ; 
       12 25 110 ; 
       13 27 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 13 110 ; 
       20 29 110 ; 
       24 20 110 ; 
       25 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 26 110 ; 
       30 14 110 ; 
       31 15 110 ; 
       32 16 110 ; 
       33 17 110 ; 
       34 18 110 ; 
       35 1 110 ; 
       42 27 110 ; 
       43 42 110 ; 
       44 42 110 ; 
       45 10 110 ; 
       46 45 110 ; 
       47 45 110 ; 
       48 45 110 ; 
       49 45 110 ; 
       50 45 110 ; 
       51 45 110 ; 
       52 10 110 ; 
       53 52 110 ; 
       54 52 110 ; 
       55 20 110 ; 
       56 20 110 ; 
       57 20 110 ; 
       58 20 110 ; 
       59 20 110 ; 
       60 20 110 ; 
       61 20 110 ; 
       62 20 110 ; 
       63 10 110 ; 
       64 63 110 ; 
       65 63 110 ; 
       66 63 110 ; 
       67 63 110 ; 
       68 63 110 ; 
       69 10 110 ; 
       70 69 110 ; 
       71 69 110 ; 
       72 69 110 ; 
       73 69 110 ; 
       74 69 110 ; 
       75 10 110 ; 
       76 75 110 ; 
       77 75 110 ; 
       78 75 110 ; 
       79 75 110 ; 
       80 75 110 ; 
       81 20 110 ; 
       82 3 110 ; 
       83 3 110 ; 
       84 3 110 ; 
       85 20 110 ; 
       86 85 110 ; 
       87 85 110 ; 
       88 85 110 ; 
       89 85 110 ; 
       90 85 110 ; 
       92 27 110 ; 
       93 92 110 ; 
       94 92 110 ; 
       9 19 110 ; 
       19 91 110 ; 
       21 91 110 ; 
       22 91 110 ; 
       23 91 110 ; 
       36 21 110 ; 
       37 21 110 ; 
       38 23 110 ; 
       39 23 110 ; 
       40 22 110 ; 
       41 22 110 ; 
       91 28 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 24 300 ; 
       0 84 300 ; 
       0 85 300 ; 
       1 24 300 ; 
       1 82 300 ; 
       1 83 300 ; 
       2 24 300 ; 
       2 80 300 ; 
       2 81 300 ; 
       8 24 300 ; 
       8 76 300 ; 
       8 78 300 ; 
       8 79 300 ; 
       10 24 300 ; 
       10 35 300 ; 
       10 46 300 ; 
       10 55 300 ; 
       10 66 300 ; 
       10 77 300 ; 
       10 86 300 ; 
       10 93 300 ; 
       10 96 300 ; 
       10 25 300 ; 
       10 26 300 ; 
       10 27 300 ; 
       10 28 300 ; 
       10 29 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       10 94 300 ; 
       14 24 300 ; 
       14 54 300 ; 
       14 56 300 ; 
       15 24 300 ; 
       15 57 300 ; 
       15 58 300 ; 
       16 24 300 ; 
       16 59 300 ; 
       16 60 300 ; 
       17 24 300 ; 
       17 63 300 ; 
       17 64 300 ; 
       18 24 300 ; 
       18 61 300 ; 
       18 62 300 ; 
       20 24 300 ; 
       20 32 300 ; 
       20 33 300 ; 
       20 34 300 ; 
       20 36 300 ; 
       20 37 300 ; 
       20 38 300 ; 
       20 39 300 ; 
       20 40 300 ; 
       20 41 300 ; 
       20 42 300 ; 
       20 43 300 ; 
       20 44 300 ; 
       20 45 300 ; 
       20 47 300 ; 
       20 48 300 ; 
       20 95 300 ; 
       24 24 300 ; 
       24 68 300 ; 
       24 69 300 ; 
       24 70 300 ; 
       25 24 300 ; 
       25 65 300 ; 
       25 67 300 ; 
       25 71 300 ; 
       27 24 300 ; 
       27 49 300 ; 
       27 50 300 ; 
       27 51 300 ; 
       27 52 300 ; 
       27 53 300 ; 
       28 24 300 ; 
       28 87 300 ; 
       28 88 300 ; 
       28 89 300 ; 
       28 90 300 ; 
       28 91 300 ; 
       28 92 300 ; 
       29 24 300 ; 
       30 97 300 ; 
       31 101 300 ; 
       32 100 300 ; 
       33 102 300 ; 
       34 99 300 ; 
       35 98 300 ; 
       43 128 300 ; 
       44 104 300 ; 
       46 120 300 ; 
       47 121 300 ; 
       48 122 300 ; 
       49 124 300 ; 
       50 125 300 ; 
       51 126 300 ; 
       53 127 300 ; 
       54 103 300 ; 
       55 105 300 ; 
       56 112 300 ; 
       57 123 300 ; 
       58 129 300 ; 
       59 130 300 ; 
       60 131 300 ; 
       61 132 300 ; 
       62 133 300 ; 
       64 134 300 ; 
       65 106 300 ; 
       66 135 300 ; 
       67 113 300 ; 
       68 109 300 ; 
       70 114 300 ; 
       71 115 300 ; 
       72 107 300 ; 
       73 116 300 ; 
       74 110 300 ; 
       76 117 300 ; 
       77 108 300 ; 
       78 118 300 ; 
       79 119 300 ; 
       80 111 300 ; 
       81 24 300 ; 
       81 72 300 ; 
       81 73 300 ; 
       81 74 300 ; 
       81 75 300 ; 
       19 17 300 ; 
       19 11 300 ; 
       19 12 300 ; 
       21 7 300 ; 
       21 8 300 ; 
       21 9 300 ; 
       21 10 300 ; 
       21 13 300 ; 
       22 16 300 ; 
       22 0 300 ; 
       22 1 300 ; 
       22 2 300 ; 
       22 14 300 ; 
       23 3 300 ; 
       23 4 300 ; 
       23 5 300 ; 
       23 6 300 ; 
       23 15 300 ; 
       36 23 300 ; 
       37 18 300 ; 
       38 22 300 ; 
       39 19 300 ; 
       40 20 300 ; 
       41 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       26 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       25 85 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       28 17 401 ; 
       29 18 401 ; 
       30 19 401 ; 
       31 20 401 ; 
       32 21 401 ; 
       33 22 401 ; 
       34 23 401 ; 
       35 14 401 ; 
       36 24 401 ; 
       37 26 401 ; 
       38 27 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 31 401 ; 
       42 30 401 ; 
       43 32 401 ; 
       44 33 401 ; 
       45 34 401 ; 
       46 25 401 ; 
       47 35 401 ; 
       48 37 401 ; 
       49 38 401 ; 
       50 39 401 ; 
       51 40 401 ; 
       52 41 401 ; 
       53 42 401 ; 
       54 43 401 ; 
       55 36 401 ; 
       56 44 401 ; 
       57 46 401 ; 
       58 47 401 ; 
       59 48 401 ; 
       60 49 401 ; 
       61 50 401 ; 
       62 51 401 ; 
       63 52 401 ; 
       64 53 401 ; 
       65 54 401 ; 
       66 45 401 ; 
       67 55 401 ; 
       68 57 401 ; 
       69 58 401 ; 
       70 59 401 ; 
       71 60 401 ; 
       72 61 401 ; 
       73 62 401 ; 
       74 63 401 ; 
       75 64 401 ; 
       76 65 401 ; 
       77 56 401 ; 
       78 66 401 ; 
       79 68 401 ; 
       80 69 401 ; 
       81 70 401 ; 
       82 71 401 ; 
       83 72 401 ; 
       84 73 401 ; 
       85 74 401 ; 
       86 67 401 ; 
       87 76 401 ; 
       88 77 401 ; 
       89 78 401 ; 
       90 79 401 ; 
       91 80 401 ; 
       92 81 401 ; 
       93 75 401 ; 
       94 82 401 ; 
       95 84 401 ; 
       96 83 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 88.75 -12 0 MPRFLG 0 ; 
       1 SCHEM 88.75 -14 0 MPRFLG 0 ; 
       2 SCHEM 87.5 -16 0 MPRFLG 0 ; 
       26 SCHEM 78.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 50 -12 0 MPRFLG 0 ; 
       4 SCHEM 52.5 -14 0 MPRFLG 0 ; 
       5 SCHEM 50 -14 0 MPRFLG 0 ; 
       6 SCHEM 45 -14 0 MPRFLG 0 ; 
       7 SCHEM 47.5 -14 0 MPRFLG 0 ; 
       8 SCHEM 85 -12 0 MPRFLG 0 ; 
       10 SCHEM 36.25 -10 0 MPRFLG 0 ; 
       11 SCHEM 115 -12 0 MPRFLG 0 ; 
       12 SCHEM 117.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 142.5 -12 0 MPRFLG 0 ; 
       14 SCHEM 147.5 -14 0 MPRFLG 0 ; 
       15 SCHEM 137.5 -14 0 MPRFLG 0 ; 
       16 SCHEM 140 -14 0 MPRFLG 0 ; 
       17 SCHEM 142.5 -14 0 MPRFLG 0 ; 
       18 SCHEM 145 -14 0 MPRFLG 0 ; 
       20 SCHEM 78.75 -8 0 MPRFLG 0 ; 
       24 SCHEM 115 -10 0 MPRFLG 0 ; 
       25 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       27 SCHEM 147.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 127.5 -10 0 MPRFLG 0 ; 
       29 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       30 SCHEM 147.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 137.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 140 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 142.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 145 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 90 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 156.25 -12 0 MPRFLG 0 ; 
       43 SCHEM 157.5 -14 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 155 -14 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 66.25 -12 0 MPRFLG 0 ; 
       46 SCHEM 72.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 60 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 70 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 65 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 67.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 62.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 1.25 -12 0 MPRFLG 0 ; 
       53 SCHEM 2.5 -14 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 0 -14 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 100 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       56 SCHEM 75 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       57 SCHEM 77.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       58 SCHEM 80 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       59 SCHEM 82.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       60 SCHEM 92.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       61 SCHEM 95 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       62 SCHEM 97.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       63 SCHEM 10 -12 0 MPRFLG 0 ; 
       64 SCHEM 15 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       65 SCHEM 5 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       66 SCHEM 7.5 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       67 SCHEM 10 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       68 SCHEM 12.5 -14 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       70 SCHEM 27.5 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       71 SCHEM 17.5 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       72 SCHEM 20 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       73 SCHEM 22.5 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       74 SCHEM 25 -14 0 WIRECOL 2 7 MPRFLG 0 ; 
       75 SCHEM 35 -12 0 MPRFLG 0 ; 
       76 SCHEM 40 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       77 SCHEM 30 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       78 SCHEM 32.5 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       79 SCHEM 35 -14 0 WIRECOL 6 7 MPRFLG 0 ; 
       80 SCHEM 37.5 -14 0 WIRECOL 2 7 MPRFLG 0 ; 
       81 SCHEM 87.5 -10 0 MPRFLG 0 ; 
       82 SCHEM 42.5 -14 0 MPRFLG 0 ; 
       83 SCHEM 55 -14 0 MPRFLG 0 ; 
       84 SCHEM 57.5 -14 0 MPRFLG 0 ; 
       85 SCHEM 107.5 -10 0 MPRFLG 0 ; 
       86 SCHEM 112.5 -12 0 MPRFLG 0 ; 
       87 SCHEM 102.5 -12 0 MPRFLG 0 ; 
       88 SCHEM 105 -12 0 MPRFLG 0 ; 
       89 SCHEM 107.5 -12 0 MPRFLG 0 ; 
       90 SCHEM 110 -12 0 MPRFLG 0 ; 
       92 SCHEM 151.25 -12 0 MPRFLG 0 ; 
       93 SCHEM 152.5 -14 0 MPRFLG 0 ; 
       94 SCHEM 150 -14 0 MPRFLG 0 ; 
       9 SCHEM 120 -16 0 MPRFLG 0 ; 
       19 SCHEM 120 -14 0 DISPLAY 1 2 MPRFLG 0 ; 
       21 SCHEM 128.75 -14 0 MPRFLG 0 ; 
       22 SCHEM 133.75 -14 0 MPRFLG 0 ; 
       23 SCHEM 123.75 -14 0 MPRFLG 0 ; 
       36 SCHEM 127.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 130 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 122.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 125 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 132.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 135 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       91 SCHEM 127.5 -12 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       24 SCHEM 55.38971 13.3762 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24.56708 38.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24.56708 40.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24.56708 42.98859 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24.56708 44.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24.56708 52.98859 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24.56708 50.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24.56708 48.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24.56708 56.95851 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24.56708 58.95851 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 121.287 117.9285 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 121.3171 115.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 121.3171 113.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 121.9807 111.7733 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 121.3153 110.4233 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 99.86169 154.3591 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 24.56708 62.95851 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 100.8125 157.0269 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 90.31711 141.814 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 90.82956 144.8902 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 93.79076 130.0361 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 92.91959 133.061 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 97.81711 118.3607 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 98.14166 120.7536 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 95.23842 107.588 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 95.23842 108.9048 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 101.6471 43.72787 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 24.47458 61.57127 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 119 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 116.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 98.67371 30.51694 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 99.38989 28.45619 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 101.635 40.27418 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 71.83112 3.604996 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 91.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 71.81708 -1.041504 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 71.81708 -3.041504 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 58.81706 63.95848 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 58.81706 61.95848 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 58.81706 59.95848 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 40.75467 20.2262 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 40.25928 22.44826 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 51.61641 13.61949 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 51.61641 15.61949 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 62.56708 78.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 62.56708 80.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 24.56708 64.95851 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 101.3171 144.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 101.3171 142.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 101.3171 140.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 101.3171 138.9886 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 101.3171 136.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 101.3171 134.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 24.56708 46.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       95 SCHEM 159 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       96 SCHEM 24.56708 54.9585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       97 SCHEM 146.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       98 SCHEM 89 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       99 SCHEM 144 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       100 SCHEM 139 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       101 SCHEM 136.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       102 SCHEM 141.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       103 SCHEM -1 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       104 SCHEM 154 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       105 SCHEM 99 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       106 SCHEM 4 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       107 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       108 SCHEM 29 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       109 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       110 SCHEM 24 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       111 SCHEM 36.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       112 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       113 SCHEM 9 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       114 SCHEM 26.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       115 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       116 SCHEM 21.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       117 SCHEM 39 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       118 SCHEM 31.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       119 SCHEM 34 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       120 SCHEM 71.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       121 SCHEM 59 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       122 SCHEM 69 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       123 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       124 SCHEM 64 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       125 SCHEM 66.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       126 SCHEM 61.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       127 SCHEM 1.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       128 SCHEM 156.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       129 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       130 SCHEM 81.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       131 SCHEM 91.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       132 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       133 SCHEM 96.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       134 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       135 SCHEM 6.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 136.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 136.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 136.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 126.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 126.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 126.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 126.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 131.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 131.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 131.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 131.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 121.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 121.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 131.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 136.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 126.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 136.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 121.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 129 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 124 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 131.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 134 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 121.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 126.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       14 SCHEM 10.27658 -65.37582 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 10.33667 13.84309 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 10.06627 4.77449 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 10.12636 -4.955109 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24.56708 50.98859 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24.56708 48.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24.56708 46.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 10.03622 -75.40581 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9.825928 -96.30708 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 121.287 115.9285 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 121.3171 113.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 121.3171 111.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 121.9807 109.7733 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 121.3153 108.4233 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 99.86169 152.3591 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 100.8125 155.0269 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 11.16209 -89.1425 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 90.31711 139.814 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 90.82956 142.8902 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 93.79076 128.0361 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 92.91959 131.061 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 97.81711 116.3607 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 98.14166 118.7536 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 95.23842 105.588 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 95.23842 106.9048 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 88.66652 -40.8416 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 119 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 74 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 111.3261 -179.8954 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 89.12735 -17.7563 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 99.38989 26.45619 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 101.635 38.27418 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 71.83112 1.604996 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 91.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 71.81708 -3.041504 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 71.81708 -5.041504 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 58.81706 61.95848 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 58.81706 59.95848 0 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 10.18643 -105.3758 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 58.81706 57.95848 0 WIRECOL 10 7 MPRFLG 0 ; 
       69 SCHEM 40.75467 18.2262 0 WIRECOL 10 7 MPRFLG 0 ; 
       70 SCHEM 40.25928 20.44826 0 WIRECOL 10 7 MPRFLG 0 ; 
       71 SCHEM 62.23764 -33.4221 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       72 SCHEM 51.61641 13.61949 0 WIRECOL 10 7 MPRFLG 0 ; 
       73 SCHEM 62.56708 76.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       74 SCHEM 62.56708 78.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       75 SCHEM 69.00635 -187.8053 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       76 SCHEM 97.31628 -91.19553 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       77 SCHEM 98.30774 -81.91663 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       78 SCHEM 101.3171 138.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       79 SCHEM 101.3171 136.9886 0 WIRECOL 10 7 MPRFLG 0 ; 
       80 SCHEM 101.3171 134.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       81 SCHEM 101.3171 132.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       82 SCHEM 24.56708 44.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       83 SCHEM 24.56708 52.9585 0 WIRECOL 10 7 MPRFLG 0 ; 
       84 SCHEM 159 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       85 SCHEM 9.525467 23.87309 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 136.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 136.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 136.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 126.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 126.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 126.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 131.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 131.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 131.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 121.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 121.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 131.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 136.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 126.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 159 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
