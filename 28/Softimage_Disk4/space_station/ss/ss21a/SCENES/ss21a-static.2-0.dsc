SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.21-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 64     
       mass_carg_con_F-mat120.2-0 ; 
       mass_carg_con_F-mat124.2-0 ; 
       mass_carg_con_F-mat218.1-0 ; 
       mass_carg_con_F-mat219.1-0 ; 
       mass_carg_con_F-mat92.1-0 ; 
       re_text-mat1.2-0 ; 
       re_text-mat10.1-0 ; 
       re_text-mat11.1-0 ; 
       re_text-mat12.1-0 ; 
       re_text-mat13.1-0 ; 
       re_text-mat14.1-0 ; 
       re_text-mat15.1-0 ; 
       re_text-mat16.1-0 ; 
       re_text-mat17.1-0 ; 
       re_text-mat18.1-0 ; 
       re_text-mat19.1-0 ; 
       re_text-mat2.1-0 ; 
       re_text-mat20.1-0 ; 
       re_text-mat21.1-0 ; 
       re_text-mat22.1-0 ; 
       re_text-mat222.1-0 ; 
       re_text-mat223.1-0 ; 
       re_text-mat224.1-0 ; 
       re_text-mat225.1-0 ; 
       re_text-mat23.1-0 ; 
       re_text-mat24.1-0 ; 
       re_text-mat25.1-0 ; 
       re_text-mat26.1-0 ; 
       re_text-mat27.1-0 ; 
       re_text-mat28.1-0 ; 
       re_text-mat29.1-0 ; 
       re_text-mat3.1-0 ; 
       re_text-mat30.1-0 ; 
       re_text-mat31.1-0 ; 
       re_text-mat32.2-0 ; 
       re_text-mat33.2-0 ; 
       re_text-mat34.2-0 ; 
       re_text-mat4.1-0 ; 
       re_text-mat49.1-0 ; 
       re_text-mat5.1-0 ; 
       re_text-mat50.1-0 ; 
       re_text-mat51.1-0 ; 
       re_text-mat52.1-0 ; 
       re_text-mat53.1-0 ; 
       re_text-mat54.1-0 ; 
       re_text-mat55.1-0 ; 
       re_text-mat56.1-0 ; 
       re_text-mat57.1-0 ; 
       re_text-mat58.1-0 ; 
       re_text-mat59.1-0 ; 
       re_text-mat6.1-0 ; 
       re_text-mat60.1-0 ; 
       re_text-mat61.1-0 ; 
       re_text-mat7.1-0 ; 
       re_text-mat74.1-0 ; 
       re_text-mat75.1-0 ; 
       re_text-mat76.1-0 ; 
       re_text-mat77.1-0 ; 
       re_text-mat78.1-0 ; 
       re_text-mat79.1-0 ; 
       re_text-mat8.1-0 ; 
       re_text-mat80.1-0 ; 
       re_text-mat85.1-0 ; 
       re_text-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       re_text-corrdr.1-0 ; 
       re_text-docbay.2-0 ; 
       re_text-ffuselg.1-0 ; 
       re_text-fuselg.1-0 ; 
       re_text-fuselg3.1-0 ; 
       re_text-fuselg4.1-0 ; 
       re_text-fuselg5.1-0 ; 
       re_text-hubcon1.1-0 ; 
       re_text-hubcon2.1-0 ; 
       re_text-platfm2.1-0 ; 
       re_text-platfm3.1-0 ; 
       re_text-ss21a.10-0 ROOT ; 
       re_text-tcontwr.3-0 ; 
       re_text-utl28a.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss21a/PICTURES/ss21a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss21a-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 59     
       mass_carg_con_F-t2d205.3-0 ; 
       mass_carg_con_F-t2d206.3-0 ; 
       re_text-t2d1.4-0 ; 
       re_text-t2d10.4-0 ; 
       re_text-t2d11.4-0 ; 
       re_text-t2d12.4-0 ; 
       re_text-t2d13.4-0 ; 
       re_text-t2d14.4-0 ; 
       re_text-t2d15.4-0 ; 
       re_text-t2d16.5-0 ; 
       re_text-t2d17.5-0 ; 
       re_text-t2d18.5-0 ; 
       re_text-t2d19.5-0 ; 
       re_text-t2d2.4-0 ; 
       re_text-t2d20.5-0 ; 
       re_text-t2d208.2-0 ; 
       re_text-t2d209.2-0 ; 
       re_text-t2d21.5-0 ; 
       re_text-t2d210.2-0 ; 
       re_text-t2d22.5-0 ; 
       re_text-t2d23.5-0 ; 
       re_text-t2d24.5-0 ; 
       re_text-t2d25.5-0 ; 
       re_text-t2d26.5-0 ; 
       re_text-t2d27.5-0 ; 
       re_text-t2d28.5-0 ; 
       re_text-t2d29.5-0 ; 
       re_text-t2d3.4-0 ; 
       re_text-t2d30.5-0 ; 
       re_text-t2d31.3-0 ; 
       re_text-t2d32.3-0 ; 
       re_text-t2d33.3-0 ; 
       re_text-t2d4.4-0 ; 
       re_text-t2d48.3-0 ; 
       re_text-t2d49.3-0 ; 
       re_text-t2d5.4-0 ; 
       re_text-t2d50.2-0 ; 
       re_text-t2d51.2-0 ; 
       re_text-t2d52.2-0 ; 
       re_text-t2d53.3-0 ; 
       re_text-t2d54.2-0 ; 
       re_text-t2d55.2-0 ; 
       re_text-t2d56.2-0 ; 
       re_text-t2d57.2-0 ; 
       re_text-t2d58.2-0 ; 
       re_text-t2d59.2-0 ; 
       re_text-t2d6.4-0 ; 
       re_text-t2d60.2-0 ; 
       re_text-t2d7.4-0 ; 
       re_text-t2d73.3-0 ; 
       re_text-t2d74.3-0 ; 
       re_text-t2d75.3-0 ; 
       re_text-t2d76.3-0 ; 
       re_text-t2d77.3-0 ; 
       re_text-t2d78.3-0 ; 
       re_text-t2d79.4-0 ; 
       re_text-t2d8.4-0 ; 
       re_text-t2d82.5-0 ; 
       re_text-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 3 110 ; 
       2 13 110 ; 
       3 11 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       12 3 110 ; 
       13 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       0 49 300 ; 
       0 51 300 ; 
       0 52 300 ; 
       1 5 300 ; 
       1 16 300 ; 
       1 31 300 ; 
       1 37 300 ; 
       1 39 300 ; 
       1 50 300 ; 
       1 53 300 ; 
       1 60 300 ; 
       1 63 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 61 300 ; 
       2 4 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       3 5 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       3 62 300 ; 
       4 0 300 ; 
       4 1 300 ; 
       5 20 300 ; 
       5 21 300 ; 
       6 22 300 ; 
       6 23 300 ; 
       7 5 300 ; 
       7 41 300 ; 
       7 42 300 ; 
       7 43 300 ; 
       8 5 300 ; 
       8 38 300 ; 
       8 40 300 ; 
       8 44 300 ; 
       9 5 300 ; 
       9 34 300 ; 
       9 35 300 ; 
       9 36 300 ; 
       10 5 300 ; 
       10 54 300 ; 
       10 55 300 ; 
       10 56 300 ; 
       10 57 300 ; 
       10 58 300 ; 
       10 59 300 ; 
       11 5 300 ; 
       12 5 300 ; 
       12 45 300 ; 
       12 46 300 ; 
       12 47 300 ; 
       12 48 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 15 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       6 58 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 2 401 ; 
       17 12 401 ; 
       18 14 401 ; 
       19 17 401 ; 
       21 16 401 ; 
       23 18 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 22 401 ; 
       27 21 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 13 401 ; 
       32 26 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 27 401 ; 
       38 33 401 ; 
       39 32 401 ; 
       40 34 401 ; 
       41 36 401 ; 
       42 37 401 ; 
       43 38 401 ; 
       44 39 401 ; 
       45 40 401 ; 
       46 41 401 ; 
       47 42 401 ; 
       48 43 401 ; 
       49 44 401 ; 
       50 35 401 ; 
       51 45 401 ; 
       52 47 401 ; 
       53 46 401 ; 
       54 49 401 ; 
       55 50 401 ; 
       56 51 401 ; 
       57 52 401 ; 
       58 53 401 ; 
       59 54 401 ; 
       60 48 401 ; 
       61 55 401 ; 
       62 57 401 ; 
       63 56 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 -8 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 20 -8 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 12.5 0 0 SRT 1 1 1 1.570796 0 -3.141593 0 0 0 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 16.25 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 33.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 27.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 38.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 35.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 39.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 21.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 39.8223 -4.265444 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 47.57761 -7.11677 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 43.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 23.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 20.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 5.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 7.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 17 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 14 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 15.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 1.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 3 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 4.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 29.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 47.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 40.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 41.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 43.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 44.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 46.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 36.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 26.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 23.75 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 32.75 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.75 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29.75 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 38.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 35.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 39.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 20.75 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 22.25 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 39.8223 -6.265444 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 47.57761 -9.11677 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 43.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19.25 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 8.25 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 5.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 4.25 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 6.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 7.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 17 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 15.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 3 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 28.25 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 4.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 0.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 46.75 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 39.25 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 41.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 43.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 44.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 46.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 36.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 26.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 31.25 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
