SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       corphq3-cam_int1.36-0 ROOT ; 
       corphq3-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 74     
       PRE_FREEZE-cnostant3.1-0 ; 
       PRE_FREEZE-grage_int1.1-0 ; 
       Tarus_Station-default103.2-0 ; 
       Tarus_Station-default111.2-0 ; 
       Tarus_Station-default112.2-0 ; 
       Tarus_Station-default113.2-0 ; 
       Tarus_Station-default114.2-0 ; 
       Tarus_Station-default33.2-0 ; 
       Tarus_Station-default57.2-0 ; 
       Tarus_Station-default87.2-0 ; 
       Tarus_Station-default95.2-0 ; 
       Tarus_Station-mat10.2-0 ; 
       Tarus_Station-mat11.2-0 ; 
       Tarus_Station-mat12.2-0 ; 
       Tarus_Station-mat13.2-0 ; 
       Tarus_Station-mat131.2-0 ; 
       Tarus_Station-mat132.2-0 ; 
       Tarus_Station-mat133.2-0 ; 
       Tarus_Station-mat134.2-0 ; 
       Tarus_Station-mat135.2-0 ; 
       Tarus_Station-mat136.2-0 ; 
       Tarus_Station-mat137.2-0 ; 
       Tarus_Station-mat138.2-0 ; 
       Tarus_Station-mat139.2-0 ; 
       Tarus_Station-mat14.2-0 ; 
       Tarus_Station-mat140.2-0 ; 
       Tarus_Station-mat141.2-0 ; 
       Tarus_Station-mat142.2-0 ; 
       Tarus_Station-mat143.2-0 ; 
       Tarus_Station-mat144.2-0 ; 
       Tarus_Station-mat145.2-0 ; 
       Tarus_Station-mat146.2-0 ; 
       Tarus_Station-mat147.2-0 ; 
       Tarus_Station-mat148.2-0 ; 
       Tarus_Station-mat149.2-0 ; 
       Tarus_Station-mat15.2-0 ; 
       Tarus_Station-mat150.2-0 ; 
       Tarus_Station-mat151.2-0 ; 
       Tarus_Station-mat17.2-0 ; 
       Tarus_Station-mat25.2-0 ; 
       Tarus_Station-mat26.2-0 ; 
       Tarus_Station-mat3.2-0 ; 
       Tarus_Station-mat4.2-0 ; 
       Tarus_Station-mat42.2-0 ; 
       Tarus_Station-mat43.2-0 ; 
       Tarus_Station-mat44.2-0 ; 
       Tarus_Station-mat45.2-0 ; 
       Tarus_Station-mat46.2-0 ; 
       Tarus_Station-mat47.2-0 ; 
       Tarus_Station-mat48.2-0 ; 
       Tarus_Station-mat49.2-0 ; 
       Tarus_Station-mat5.2-0 ; 
       Tarus_Station-mat50.2-0 ; 
       Tarus_Station-mat51.2-0 ; 
       Tarus_Station-mat52.2-0 ; 
       Tarus_Station-mat53.2-0 ; 
       Tarus_Station-mat54.2-0 ; 
       Tarus_Station-mat55.2-0 ; 
       Tarus_Station-mat56.2-0 ; 
       Tarus_Station-mat57.2-0 ; 
       Tarus_Station-mat58.2-0 ; 
       Tarus_Station-mat59.2-0 ; 
       Tarus_Station-mat6.2-0 ; 
       Tarus_Station-mat60.2-0 ; 
       Tarus_Station-mat61.2-0 ; 
       Tarus_Station-mat62.2-0 ; 
       Tarus_Station-mat63.2-0 ; 
       Tarus_Station-mat7.2-0 ; 
       Tarus_Station-mat8.2-0 ; 
       Tarus_Station-mat81.2-0 ; 
       Tarus_Station-mat82.2-0 ; 
       Tarus_Station-mat83.2-0 ; 
       Tarus_Station-mat84.2-0 ; 
       Tarus_Station-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       ss23-bfuselg.1-0 ; 
       ss23-corrdr0.1-0 ; 
       ss23-corrdr1.1-0 ; 
       ss23-corrdr10.1-0 ; 
       ss23-corrdr11.1-0 ; 
       ss23-corrdr2.1-0 ; 
       ss23-corrdr3.1-0 ; 
       ss23-corrdr4.1-0 ; 
       ss23-corrdr6.1-0 ; 
       ss23-corrdr9.1-0 ; 
       ss23-mfuselg.4-0 ; 
       ss23-null1.1-0 ; 
       ss23-ss23.27-0 ROOT ; 
       ss23-tarus.1-0 ; 
       ss23-tfuselg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss23/PICTURES/bgrnd57 ; 
       E:/pete_data2/space_station/ss/ss23/PICTURES/ss23 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       NEW_SS23-SATATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 65     
       PRE_FREEZE-t2d108.3-0 ; 
       PRE_FREEZE-t2d109.3-0 ; 
       Tarus_Station-t2d1.4-0 ; 
       Tarus_Station-t2d10.4-0 ; 
       Tarus_Station-t2d100.4-0 ; 
       Tarus_Station-t2d101.4-0 ; 
       Tarus_Station-t2d102.4-0 ; 
       Tarus_Station-t2d11.4-0 ; 
       Tarus_Station-t2d12.5-0 ; 
       Tarus_Station-t2d13.6-0 ; 
       Tarus_Station-t2d2.4-0 ; 
       Tarus_Station-t2d21.5-0 ; 
       Tarus_Station-t2d22.6-0 ; 
       Tarus_Station-t2d3.4-0 ; 
       Tarus_Station-t2d38.4-0 ; 
       Tarus_Station-t2d39.4-0 ; 
       Tarus_Station-t2d4.4-0 ; 
       Tarus_Station-t2d40.4-0 ; 
       Tarus_Station-t2d41.4-0 ; 
       Tarus_Station-t2d42.4-0 ; 
       Tarus_Station-t2d43.4-0 ; 
       Tarus_Station-t2d44.4-0 ; 
       Tarus_Station-t2d45.4-0 ; 
       Tarus_Station-t2d46.4-0 ; 
       Tarus_Station-t2d47.4-0 ; 
       Tarus_Station-t2d48.4-0 ; 
       Tarus_Station-t2d49.4-0 ; 
       Tarus_Station-t2d5.4-0 ; 
       Tarus_Station-t2d50.4-0 ; 
       Tarus_Station-t2d51.4-0 ; 
       Tarus_Station-t2d52.4-0 ; 
       Tarus_Station-t2d53.4-0 ; 
       Tarus_Station-t2d54.4-0 ; 
       Tarus_Station-t2d55.4-0 ; 
       Tarus_Station-t2d56.4-0 ; 
       Tarus_Station-t2d57.4-0 ; 
       Tarus_Station-t2d58.4-0 ; 
       Tarus_Station-t2d59.4-0 ; 
       Tarus_Station-t2d6.4-0 ; 
       Tarus_Station-t2d7.4-0 ; 
       Tarus_Station-t2d77.4-0 ; 
       Tarus_Station-t2d78.4-0 ; 
       Tarus_Station-t2d79.4-0 ; 
       Tarus_Station-t2d8.4-0 ; 
       Tarus_Station-t2d80.4-0 ; 
       Tarus_Station-t2d81.4-0 ; 
       Tarus_Station-t2d82.4-0 ; 
       Tarus_Station-t2d83.4-0 ; 
       Tarus_Station-t2d84.4-0 ; 
       Tarus_Station-t2d85.4-0 ; 
       Tarus_Station-t2d86.4-0 ; 
       Tarus_Station-t2d87.4-0 ; 
       Tarus_Station-t2d88.4-0 ; 
       Tarus_Station-t2d89.4-0 ; 
       Tarus_Station-t2d9.4-0 ; 
       Tarus_Station-t2d90.4-0 ; 
       Tarus_Station-t2d91.4-0 ; 
       Tarus_Station-t2d92.4-0 ; 
       Tarus_Station-t2d93.4-0 ; 
       Tarus_Station-t2d94.4-0 ; 
       Tarus_Station-t2d95.4-0 ; 
       Tarus_Station-t2d96.4-0 ; 
       Tarus_Station-t2d97.4-0 ; 
       Tarus_Station-t2d98.4-0 ; 
       Tarus_Station-t2d99.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 10 110 ; 
       1 10 110 ; 
       2 1 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 12 110 ; 
       11 1 110 ; 
       13 12 110 ; 
       14 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 35 300 ; 
       0 39 300 ; 
       2 2 300 ; 
       2 54 300 ; 
       2 55 300 ; 
       2 56 300 ; 
       2 57 300 ; 
       2 58 300 ; 
       2 71 300 ; 
       2 72 300 ; 
       3 5 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       4 6 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       4 34 300 ; 
       4 36 300 ; 
       4 37 300 ; 
       5 7 300 ; 
       5 41 300 ; 
       5 42 300 ; 
       5 51 300 ; 
       5 62 300 ; 
       5 24 300 ; 
       5 69 300 ; 
       6 9 300 ; 
       6 43 300 ; 
       6 44 300 ; 
       6 45 300 ; 
       6 46 300 ; 
       6 47 300 ; 
       7 10 300 ; 
       7 48 300 ; 
       7 49 300 ; 
       7 50 300 ; 
       7 52 300 ; 
       7 53 300 ; 
       7 70 300 ; 
       8 3 300 ; 
       8 59 300 ; 
       8 60 300 ; 
       8 61 300 ; 
       8 63 300 ; 
       8 64 300 ; 
       8 65 300 ; 
       8 66 300 ; 
       9 4 300 ; 
       9 15 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       10 67 300 ; 
       10 68 300 ; 
       10 73 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       13 0 300 ; 
       13 1 300 ; 
       14 8 300 ; 
       14 38 300 ; 
       14 40 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 44 401 ; 
       11 39 401 ; 
       12 43 401 ; 
       13 54 401 ; 
       14 3 401 ; 
       15 46 401 ; 
       16 47 401 ; 
       17 48 401 ; 
       18 49 401 ; 
       19 50 401 ; 
       20 51 401 ; 
       21 52 401 ; 
       22 53 401 ; 
       23 55 401 ; 
       24 7 401 ; 
       25 56 401 ; 
       26 57 401 ; 
       27 58 401 ; 
       28 59 401 ; 
       29 60 401 ; 
       30 61 401 ; 
       31 62 401 ; 
       32 63 401 ; 
       33 64 401 ; 
       34 4 401 ; 
       35 8 401 ; 
       36 5 401 ; 
       37 6 401 ; 
       38 9 401 ; 
       39 11 401 ; 
       40 12 401 ; 
       41 2 401 ; 
       42 10 401 ; 
       43 14 401 ; 
       44 15 401 ; 
       45 17 401 ; 
       46 18 401 ; 
       47 19 401 ; 
       48 20 401 ; 
       49 21 401 ; 
       50 22 401 ; 
       51 13 401 ; 
       52 23 401 ; 
       53 24 401 ; 
       54 25 401 ; 
       55 26 401 ; 
       56 28 401 ; 
       57 29 401 ; 
       58 30 401 ; 
       59 31 401 ; 
       60 32 401 ; 
       61 33 401 ; 
       62 16 401 ; 
       63 34 401 ; 
       64 35 401 ; 
       65 36 401 ; 
       66 37 401 ; 
       68 27 401 ; 
       69 40 401 ; 
       70 41 401 ; 
       71 42 401 ; 
       72 45 401 ; 
       73 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       2 SCHEM 0 -10 0 MPRFLG 0 ; 
       3 SCHEM 10 -12 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 5 -10 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 15 -12 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 MPRFLG 0 ; 
       14 SCHEM 20 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 40 19 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
