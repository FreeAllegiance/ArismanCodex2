SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       corphq3-cam_int1.31-0 ROOT ; 
       corphq3-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 78     
       PRE_FREEZE-cnostant3.1-0 ; 
       PRE_FREEZE-grage_int1.1-0 ; 
       Tarus_Station-default103.2-0 ; 
       Tarus_Station-default111.2-0 ; 
       Tarus_Station-default112.2-0 ; 
       Tarus_Station-default113.2-0 ; 
       Tarus_Station-default114.2-0 ; 
       Tarus_Station-default33.2-0 ; 
       Tarus_Station-default57.2-0 ; 
       Tarus_Station-default87.2-0 ; 
       Tarus_Station-default95.2-0 ; 
       Tarus_Station-mat10.2-0 ; 
       Tarus_Station-mat11.2-0 ; 
       Tarus_Station-mat12.2-0 ; 
       Tarus_Station-mat13.2-0 ; 
       Tarus_Station-mat131.2-0 ; 
       Tarus_Station-mat132.2-0 ; 
       Tarus_Station-mat133.2-0 ; 
       Tarus_Station-mat134.2-0 ; 
       Tarus_Station-mat135.2-0 ; 
       Tarus_Station-mat136.2-0 ; 
       Tarus_Station-mat137.2-0 ; 
       Tarus_Station-mat138.2-0 ; 
       Tarus_Station-mat139.2-0 ; 
       Tarus_Station-mat14.2-0 ; 
       Tarus_Station-mat140.2-0 ; 
       Tarus_Station-mat141.2-0 ; 
       Tarus_Station-mat142.2-0 ; 
       Tarus_Station-mat143.2-0 ; 
       Tarus_Station-mat144.2-0 ; 
       Tarus_Station-mat145.2-0 ; 
       Tarus_Station-mat146.2-0 ; 
       Tarus_Station-mat147.2-0 ; 
       Tarus_Station-mat148.2-0 ; 
       Tarus_Station-mat149.2-0 ; 
       Tarus_Station-mat15.2-0 ; 
       Tarus_Station-mat150.2-0 ; 
       Tarus_Station-mat151.2-0 ; 
       Tarus_Station-mat17.2-0 ; 
       Tarus_Station-mat2.2-0 ; 
       Tarus_Station-mat25.2-0 ; 
       Tarus_Station-mat26.2-0 ; 
       Tarus_Station-mat3.2-0 ; 
       Tarus_Station-mat4.2-0 ; 
       Tarus_Station-mat42.2-0 ; 
       Tarus_Station-mat43.2-0 ; 
       Tarus_Station-mat44.2-0 ; 
       Tarus_Station-mat45.2-0 ; 
       Tarus_Station-mat46.2-0 ; 
       Tarus_Station-mat47.2-0 ; 
       Tarus_Station-mat48.2-0 ; 
       Tarus_Station-mat49.2-0 ; 
       Tarus_Station-mat5.2-0 ; 
       Tarus_Station-mat50.2-0 ; 
       Tarus_Station-mat51.2-0 ; 
       Tarus_Station-mat52.2-0 ; 
       Tarus_Station-mat53.2-0 ; 
       Tarus_Station-mat54.2-0 ; 
       Tarus_Station-mat55.2-0 ; 
       Tarus_Station-mat56.2-0 ; 
       Tarus_Station-mat57.2-0 ; 
       Tarus_Station-mat58.2-0 ; 
       Tarus_Station-mat59.2-0 ; 
       Tarus_Station-mat6.2-0 ; 
       Tarus_Station-mat60.2-0 ; 
       Tarus_Station-mat61.2-0 ; 
       Tarus_Station-mat62.2-0 ; 
       Tarus_Station-mat63.2-0 ; 
       Tarus_Station-mat7.2-0 ; 
       Tarus_Station-mat78.2-0 ; 
       Tarus_Station-mat79.2-0 ; 
       Tarus_Station-mat8.2-0 ; 
       Tarus_Station-mat80.2-0 ; 
       Tarus_Station-mat81.2-0 ; 
       Tarus_Station-mat82.2-0 ; 
       Tarus_Station-mat83.2-0 ; 
       Tarus_Station-mat84.2-0 ; 
       Tarus_Station-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       ss23-antenn1.1-0 ; 
       ss23-antenn2.1-0 ; 
       ss23-antenn3.1-0 ; 
       ss23-bdoccon.1-0 ; 
       ss23-bfuselg.1-0 ; 
       ss23-corrdr0.1-0 ; 
       ss23-corrdr1.1-0 ; 
       ss23-corrdr10.1-0 ; 
       ss23-corrdr11.1-0 ; 
       ss23-corrdr2.1-0 ; 
       ss23-corrdr3.1-0 ; 
       ss23-corrdr4.1-0 ; 
       ss23-corrdr6.1-0 ; 
       ss23-corrdr9.1-0 ; 
       ss23-mfuselg.4-0 ; 
       ss23-null1.1-0 ; 
       ss23-ss23.23-0 ROOT ; 
       ss23-tarus.1-0 ; 
       ss23-tdoccon.1-0 ; 
       ss23-tfuselg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss23/PICTURES/bgrnd57 ; 
       E:/Pete_Data2/space_station/ss/ss23/PICTURES/ss23 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       NEW_SS23-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 68     
       PRE_FREEZE-t2d108.2-0 ; 
       PRE_FREEZE-t2d109.2-0 ; 
       Tarus_Station-t2d1.3-0 ; 
       Tarus_Station-t2d10.3-0 ; 
       Tarus_Station-t2d100.3-0 ; 
       Tarus_Station-t2d101.3-0 ; 
       Tarus_Station-t2d102.3-0 ; 
       Tarus_Station-t2d11.3-0 ; 
       Tarus_Station-t2d12.4-0 ; 
       Tarus_Station-t2d13.5-0 ; 
       Tarus_Station-t2d2.3-0 ; 
       Tarus_Station-t2d21.4-0 ; 
       Tarus_Station-t2d22.5-0 ; 
       Tarus_Station-t2d3.3-0 ; 
       Tarus_Station-t2d38.3-0 ; 
       Tarus_Station-t2d39.3-0 ; 
       Tarus_Station-t2d4.3-0 ; 
       Tarus_Station-t2d40.3-0 ; 
       Tarus_Station-t2d41.3-0 ; 
       Tarus_Station-t2d42.3-0 ; 
       Tarus_Station-t2d43.3-0 ; 
       Tarus_Station-t2d44.3-0 ; 
       Tarus_Station-t2d45.3-0 ; 
       Tarus_Station-t2d46.3-0 ; 
       Tarus_Station-t2d47.3-0 ; 
       Tarus_Station-t2d48.3-0 ; 
       Tarus_Station-t2d49.3-0 ; 
       Tarus_Station-t2d5.3-0 ; 
       Tarus_Station-t2d50.3-0 ; 
       Tarus_Station-t2d51.3-0 ; 
       Tarus_Station-t2d52.3-0 ; 
       Tarus_Station-t2d53.3-0 ; 
       Tarus_Station-t2d54.3-0 ; 
       Tarus_Station-t2d55.3-0 ; 
       Tarus_Station-t2d56.3-0 ; 
       Tarus_Station-t2d57.3-0 ; 
       Tarus_Station-t2d58.3-0 ; 
       Tarus_Station-t2d59.3-0 ; 
       Tarus_Station-t2d6.3-0 ; 
       Tarus_Station-t2d7.3-0 ; 
       Tarus_Station-t2d74.3-0 ; 
       Tarus_Station-t2d75.3-0 ; 
       Tarus_Station-t2d76.3-0 ; 
       Tarus_Station-t2d77.3-0 ; 
       Tarus_Station-t2d78.3-0 ; 
       Tarus_Station-t2d79.3-0 ; 
       Tarus_Station-t2d8.3-0 ; 
       Tarus_Station-t2d80.3-0 ; 
       Tarus_Station-t2d81.3-0 ; 
       Tarus_Station-t2d82.3-0 ; 
       Tarus_Station-t2d83.3-0 ; 
       Tarus_Station-t2d84.3-0 ; 
       Tarus_Station-t2d85.3-0 ; 
       Tarus_Station-t2d86.3-0 ; 
       Tarus_Station-t2d87.3-0 ; 
       Tarus_Station-t2d88.3-0 ; 
       Tarus_Station-t2d89.3-0 ; 
       Tarus_Station-t2d9.3-0 ; 
       Tarus_Station-t2d90.3-0 ; 
       Tarus_Station-t2d91.3-0 ; 
       Tarus_Station-t2d92.3-0 ; 
       Tarus_Station-t2d93.3-0 ; 
       Tarus_Station-t2d94.3-0 ; 
       Tarus_Station-t2d95.3-0 ; 
       Tarus_Station-t2d96.3-0 ; 
       Tarus_Station-t2d97.3-0 ; 
       Tarus_Station-t2d98.3-0 ; 
       Tarus_Station-t2d99.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       17 16 110 ; 
       0 5 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 14 110 ; 
       5 14 110 ; 
       6 5 110 ; 
       7 15 110 ; 
       8 15 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 16 110 ; 
       15 5 110 ; 
       18 19 110 ; 
       19 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       17 0 300 ; 
       17 1 300 ; 
       0 39 300 ; 
       0 69 300 ; 
       1 72 300 ; 
       2 70 300 ; 
       4 35 300 ; 
       4 40 300 ; 
       6 2 300 ; 
       6 55 300 ; 
       6 56 300 ; 
       6 57 300 ; 
       6 58 300 ; 
       6 59 300 ; 
       6 75 300 ; 
       6 76 300 ; 
       7 5 300 ; 
       7 22 300 ; 
       7 23 300 ; 
       7 25 300 ; 
       7 26 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       7 29 300 ; 
       8 6 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       8 34 300 ; 
       8 36 300 ; 
       8 37 300 ; 
       9 7 300 ; 
       9 42 300 ; 
       9 43 300 ; 
       9 52 300 ; 
       9 63 300 ; 
       9 24 300 ; 
       9 73 300 ; 
       10 9 300 ; 
       10 44 300 ; 
       10 45 300 ; 
       10 46 300 ; 
       10 47 300 ; 
       10 48 300 ; 
       11 10 300 ; 
       11 49 300 ; 
       11 50 300 ; 
       11 51 300 ; 
       11 53 300 ; 
       11 54 300 ; 
       11 74 300 ; 
       12 3 300 ; 
       12 60 300 ; 
       12 61 300 ; 
       12 62 300 ; 
       12 64 300 ; 
       12 65 300 ; 
       12 66 300 ; 
       12 67 300 ; 
       13 4 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       13 17 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       14 68 300 ; 
       14 71 300 ; 
       14 77 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       14 13 300 ; 
       14 14 300 ; 
       19 8 300 ; 
       19 38 300 ; 
       19 41 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 47 401 ; 
       11 39 401 ; 
       12 46 401 ; 
       13 57 401 ; 
       14 3 401 ; 
       15 49 401 ; 
       16 50 401 ; 
       17 51 401 ; 
       18 52 401 ; 
       19 53 401 ; 
       20 54 401 ; 
       21 55 401 ; 
       22 56 401 ; 
       23 58 401 ; 
       24 7 401 ; 
       25 59 401 ; 
       26 60 401 ; 
       27 61 401 ; 
       28 62 401 ; 
       29 63 401 ; 
       30 64 401 ; 
       31 65 401 ; 
       32 66 401 ; 
       33 67 401 ; 
       34 4 401 ; 
       35 8 401 ; 
       36 5 401 ; 
       37 6 401 ; 
       38 9 401 ; 
       40 11 401 ; 
       41 12 401 ; 
       42 2 401 ; 
       43 10 401 ; 
       44 14 401 ; 
       45 15 401 ; 
       46 17 401 ; 
       47 18 401 ; 
       48 19 401 ; 
       49 20 401 ; 
       50 21 401 ; 
       51 22 401 ; 
       52 13 401 ; 
       53 23 401 ; 
       54 24 401 ; 
       55 25 401 ; 
       56 26 401 ; 
       57 28 401 ; 
       58 29 401 ; 
       59 30 401 ; 
       60 31 401 ; 
       61 32 401 ; 
       62 33 401 ; 
       63 16 401 ; 
       64 34 401 ; 
       65 35 401 ; 
       66 36 401 ; 
       67 37 401 ; 
       69 40 401 ; 
       70 41 401 ; 
       71 27 401 ; 
       72 42 401 ; 
       73 43 401 ; 
       74 44 401 ; 
       75 45 401 ; 
       76 48 401 ; 
       77 38 401 ; 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       17 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 23.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 22.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 25 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 30 -6 0 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -4 0 DISPLAY 3 2 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 12.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 20 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       15 SCHEM 16.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 27.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 40 40 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
