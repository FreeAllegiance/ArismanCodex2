SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       corphq3-cam_int1.77-0 ROOT ; 
       corphq3-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 8     
       Tarus_Station-light1_2_2.17-0 ROOT ; 
       Tarus_Station-light1_3_2.17-0 ROOT ; 
       Tarus_Station-light1_4_2.17-0 ROOT ; 
       Tarus_Station-light1_5_2.17-0 ROOT ; 
       Tarus_Station-light1_6_2.17-0 ROOT ; 
       Tarus_Station-light1_7_2.17-0 ROOT ; 
       Tarus_Station-light1_8_2.17-0 ROOT ; 
       Tarus_Station-light2_3_1_2.17-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 73     
       STATIC-cnostant4.1-0 ; 
       STATIC-grage_int2.1-0 ; 
       STATIC-mat152.2-0 ; 
       STATIC-mat153.2-0 ; 
       STATIC-mat154.2-0 ; 
       STATIC-mat155.2-0 ; 
       Tarus_Station-default103.5-0 ; 
       Tarus_Station-default111.5-0 ; 
       Tarus_Station-default112.5-0 ; 
       Tarus_Station-default113.5-0 ; 
       Tarus_Station-default114.5-0 ; 
       Tarus_Station-default33.5-0 ; 
       Tarus_Station-default87.5-0 ; 
       Tarus_Station-default95.5-0 ; 
       Tarus_Station-mat10.5-0 ; 
       Tarus_Station-mat11.5-0 ; 
       Tarus_Station-mat12.5-0 ; 
       Tarus_Station-mat13.5-0 ; 
       Tarus_Station-mat131.5-0 ; 
       Tarus_Station-mat132.5-0 ; 
       Tarus_Station-mat133.5-0 ; 
       Tarus_Station-mat134.5-0 ; 
       Tarus_Station-mat135.5-0 ; 
       Tarus_Station-mat136.5-0 ; 
       Tarus_Station-mat137.5-0 ; 
       Tarus_Station-mat138.5-0 ; 
       Tarus_Station-mat139.5-0 ; 
       Tarus_Station-mat14.5-0 ; 
       Tarus_Station-mat140.5-0 ; 
       Tarus_Station-mat141.5-0 ; 
       Tarus_Station-mat142.5-0 ; 
       Tarus_Station-mat143.5-0 ; 
       Tarus_Station-mat144.5-0 ; 
       Tarus_Station-mat145.5-0 ; 
       Tarus_Station-mat146.5-0 ; 
       Tarus_Station-mat147.5-0 ; 
       Tarus_Station-mat148.5-0 ; 
       Tarus_Station-mat149.5-0 ; 
       Tarus_Station-mat150.5-0 ; 
       Tarus_Station-mat151.5-0 ; 
       Tarus_Station-mat3.5-0 ; 
       Tarus_Station-mat4.5-0 ; 
       Tarus_Station-mat42.5-0 ; 
       Tarus_Station-mat43.5-0 ; 
       Tarus_Station-mat44.5-0 ; 
       Tarus_Station-mat45.5-0 ; 
       Tarus_Station-mat46.5-0 ; 
       Tarus_Station-mat47.5-0 ; 
       Tarus_Station-mat48.5-0 ; 
       Tarus_Station-mat49.5-0 ; 
       Tarus_Station-mat5.5-0 ; 
       Tarus_Station-mat50.5-0 ; 
       Tarus_Station-mat51.5-0 ; 
       Tarus_Station-mat52.5-0 ; 
       Tarus_Station-mat53.5-0 ; 
       Tarus_Station-mat54.5-0 ; 
       Tarus_Station-mat55.5-0 ; 
       Tarus_Station-mat56.5-0 ; 
       Tarus_Station-mat57.5-0 ; 
       Tarus_Station-mat58.5-0 ; 
       Tarus_Station-mat59.5-0 ; 
       Tarus_Station-mat6.5-0 ; 
       Tarus_Station-mat60.5-0 ; 
       Tarus_Station-mat61.5-0 ; 
       Tarus_Station-mat62.5-0 ; 
       Tarus_Station-mat63.5-0 ; 
       Tarus_Station-mat7.5-0 ; 
       Tarus_Station-mat8.5-0 ; 
       Tarus_Station-mat81.5-0 ; 
       Tarus_Station-mat82.5-0 ; 
       Tarus_Station-mat83.5-0 ; 
       Tarus_Station-mat84.5-0 ; 
       Tarus_Station-mat9.5-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       ss23-corrdr0.1-0 ; 
       ss23-corrdr1.1-0 ; 
       ss23-corrdr10.1-0 ; 
       ss23-corrdr11.1-0 ; 
       ss23-corrdr2.1-0 ; 
       ss23-corrdr3.1-0 ; 
       ss23-corrdr4.1-0 ; 
       ss23-corrdr6.1-0 ; 
       ss23-corrdr9.1-0 ; 
       ss23-LAUNCH_TUBE.1-0 ; 
       ss23-LAUNCH_TUBE_1.1-0 ; 
       ss23-mfuselg.4-0 ; 
       ss23-null1.1-0 ; 
       ss23-ss23_2.12-0 ROOT ; 
       ss23-tarus1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss23/PICTURES/bgrnd57 ; 
       E:/Pete_Data2/space_station/ss/ss23/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/ss23/PICTURES/ss23 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       NEW_SS23-STATIC.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 65     
       STATIC-t2d110.2-0 ; 
       STATIC-t2d111.2-0 ; 
       STATIC-t2d112.2-0 ; 
       STATIC-t2d113.2-0 ; 
       STATIC-t2d114.1-0 ; 
       STATIC-t2d115.1-0 ; 
       Tarus_Station-t2d1.7-0 ; 
       Tarus_Station-t2d10.7-0 ; 
       Tarus_Station-t2d100.7-0 ; 
       Tarus_Station-t2d101.7-0 ; 
       Tarus_Station-t2d102.7-0 ; 
       Tarus_Station-t2d11.7-0 ; 
       Tarus_Station-t2d2.7-0 ; 
       Tarus_Station-t2d3.7-0 ; 
       Tarus_Station-t2d38.7-0 ; 
       Tarus_Station-t2d39.7-0 ; 
       Tarus_Station-t2d4.7-0 ; 
       Tarus_Station-t2d40.7-0 ; 
       Tarus_Station-t2d41.7-0 ; 
       Tarus_Station-t2d42.7-0 ; 
       Tarus_Station-t2d43.7-0 ; 
       Tarus_Station-t2d44.7-0 ; 
       Tarus_Station-t2d45.7-0 ; 
       Tarus_Station-t2d46.7-0 ; 
       Tarus_Station-t2d47.7-0 ; 
       Tarus_Station-t2d48.7-0 ; 
       Tarus_Station-t2d49.7-0 ; 
       Tarus_Station-t2d5.7-0 ; 
       Tarus_Station-t2d50.7-0 ; 
       Tarus_Station-t2d51.7-0 ; 
       Tarus_Station-t2d52.7-0 ; 
       Tarus_Station-t2d53.7-0 ; 
       Tarus_Station-t2d54.7-0 ; 
       Tarus_Station-t2d55.7-0 ; 
       Tarus_Station-t2d56.7-0 ; 
       Tarus_Station-t2d57.7-0 ; 
       Tarus_Station-t2d58.7-0 ; 
       Tarus_Station-t2d59.7-0 ; 
       Tarus_Station-t2d6.7-0 ; 
       Tarus_Station-t2d7.7-0 ; 
       Tarus_Station-t2d77.7-0 ; 
       Tarus_Station-t2d78.7-0 ; 
       Tarus_Station-t2d79.7-0 ; 
       Tarus_Station-t2d8.7-0 ; 
       Tarus_Station-t2d80.7-0 ; 
       Tarus_Station-t2d81.7-0 ; 
       Tarus_Station-t2d82.7-0 ; 
       Tarus_Station-t2d83.7-0 ; 
       Tarus_Station-t2d84.7-0 ; 
       Tarus_Station-t2d85.7-0 ; 
       Tarus_Station-t2d86.7-0 ; 
       Tarus_Station-t2d87.7-0 ; 
       Tarus_Station-t2d88.7-0 ; 
       Tarus_Station-t2d89.7-0 ; 
       Tarus_Station-t2d9.7-0 ; 
       Tarus_Station-t2d90.7-0 ; 
       Tarus_Station-t2d91.7-0 ; 
       Tarus_Station-t2d92.7-0 ; 
       Tarus_Station-t2d93.7-0 ; 
       Tarus_Station-t2d94.7-0 ; 
       Tarus_Station-t2d95.7-0 ; 
       Tarus_Station-t2d96.7-0 ; 
       Tarus_Station-t2d97.7-0 ; 
       Tarus_Station-t2d98.7-0 ; 
       Tarus_Station-t2d99.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 0 110 ; 
       2 12 110 ; 
       3 12 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 12 110 ; 
       8 12 110 ; 
       9 13 110 ; 
       10 13 110 ; 
       11 13 110 ; 
       12 0 110 ; 
       14 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 6 300 ; 
       1 53 300 ; 
       1 54 300 ; 
       1 55 300 ; 
       1 56 300 ; 
       1 57 300 ; 
       1 70 300 ; 
       1 71 300 ; 
       2 9 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       2 32 300 ; 
       3 10 300 ; 
       3 33 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 36 300 ; 
       3 37 300 ; 
       3 38 300 ; 
       3 39 300 ; 
       4 11 300 ; 
       4 40 300 ; 
       4 41 300 ; 
       4 50 300 ; 
       4 61 300 ; 
       4 27 300 ; 
       4 68 300 ; 
       5 12 300 ; 
       5 42 300 ; 
       5 43 300 ; 
       5 44 300 ; 
       5 45 300 ; 
       5 46 300 ; 
       6 13 300 ; 
       6 47 300 ; 
       6 48 300 ; 
       6 49 300 ; 
       6 51 300 ; 
       6 52 300 ; 
       6 69 300 ; 
       7 7 300 ; 
       7 58 300 ; 
       7 59 300 ; 
       7 60 300 ; 
       7 62 300 ; 
       7 63 300 ; 
       7 64 300 ; 
       7 65 300 ; 
       8 8 300 ; 
       8 18 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       10 4 300 ; 
       10 5 300 ; 
       11 66 300 ; 
       11 67 300 ; 
       11 72 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       14 0 300 ; 
       14 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 5 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 44 401 ; 
       14 39 401 ; 
       15 43 401 ; 
       16 54 401 ; 
       17 7 401 ; 
       18 46 401 ; 
       19 47 401 ; 
       20 48 401 ; 
       21 49 401 ; 
       22 50 401 ; 
       23 51 401 ; 
       24 52 401 ; 
       25 53 401 ; 
       26 55 401 ; 
       27 11 401 ; 
       28 56 401 ; 
       29 57 401 ; 
       30 58 401 ; 
       31 59 401 ; 
       32 60 401 ; 
       33 61 401 ; 
       34 62 401 ; 
       35 63 401 ; 
       36 64 401 ; 
       37 8 401 ; 
       38 9 401 ; 
       39 10 401 ; 
       40 6 401 ; 
       41 12 401 ; 
       42 14 401 ; 
       43 15 401 ; 
       44 17 401 ; 
       45 18 401 ; 
       46 19 401 ; 
       47 20 401 ; 
       48 21 401 ; 
       49 22 401 ; 
       50 13 401 ; 
       51 23 401 ; 
       52 24 401 ; 
       53 25 401 ; 
       54 26 401 ; 
       55 28 401 ; 
       56 29 401 ; 
       57 30 401 ; 
       58 31 401 ; 
       59 32 401 ; 
       60 33 401 ; 
       61 16 401 ; 
       62 34 401 ; 
       63 35 401 ; 
       64 36 401 ; 
       65 37 401 ; 
       67 27 401 ; 
       68 40 401 ; 
       69 41 401 ; 
       70 42 401 ; 
       71 45 401 ; 
       72 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 20 -8 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       13 SCHEM 15 0 0 SRT 0.9999999 1 0.9999999 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       4 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 40 40 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
