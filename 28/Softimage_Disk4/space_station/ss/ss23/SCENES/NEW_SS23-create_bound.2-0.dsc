SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       corphq3-cam_int1.95-0 ROOT ; 
       corphq3-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 16     
       Tarus_Station-light1_2_2_1.4-0 ROOT ; 
       Tarus_Station-light1_2_2_2.6-0 ROOT ; 
       Tarus_Station-light1_3_2.30-0 ROOT ; 
       Tarus_Station-light1_3_2_1.4-0 ROOT ; 
       Tarus_Station-light1_4_2.30-0 ROOT ; 
       Tarus_Station-light1_4_2_1.4-0 ROOT ; 
       Tarus_Station-light1_5_2.30-0 ROOT ; 
       Tarus_Station-light1_5_2_1.4-0 ROOT ; 
       Tarus_Station-light1_6_2.32-0 ROOT ; 
       Tarus_Station-light1_6_2_1.2-0 ROOT ; 
       Tarus_Station-light1_7_2.32-0 ROOT ; 
       Tarus_Station-light1_7_2_1.2-0 ROOT ; 
       Tarus_Station-light1_8_2.32-0 ROOT ; 
       Tarus_Station-light1_8_2_1.2-0 ROOT ; 
       Tarus_Station-light2_3_1_2.32-0 ROOT ; 
       Tarus_Station-light2_3_1_2_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 125     
       create_bound-cnostant5.1-0 ; 
       create_bound-cnostant6.1-0 ; 
       create_bound-grage_int3.1-0 ; 
       create_bound-grage_int4.1-0 ; 
       rotate-cnostant4.2-0 ; 
       rotate-grage_int2.2-0 ; 
       rotate-mat152.1-0 ; 
       rotate-mat153.1-0 ; 
       rotate-mat154.1-0 ; 
       rotate-mat155.1-0 ; 
       Tarus_Station-default103.6-0 ; 
       Tarus_Station-default111.6-0 ; 
       Tarus_Station-default112.6-0 ; 
       Tarus_Station-default113.6-0 ; 
       Tarus_Station-default114.6-0 ; 
       Tarus_Station-default33.6-0 ; 
       Tarus_Station-default87.6-0 ; 
       Tarus_Station-default95.6-0 ; 
       Tarus_Station-mat10.6-0 ; 
       Tarus_Station-mat100.6-0 ; 
       Tarus_Station-mat101.6-0 ; 
       Tarus_Station-mat102.6-0 ; 
       Tarus_Station-mat103.6-0 ; 
       Tarus_Station-mat104.6-0 ; 
       Tarus_Station-mat105.6-0 ; 
       Tarus_Station-mat106.6-0 ; 
       Tarus_Station-mat107.6-0 ; 
       Tarus_Station-mat108.6-0 ; 
       Tarus_Station-mat109.6-0 ; 
       Tarus_Station-mat11.6-0 ; 
       Tarus_Station-mat110.6-0 ; 
       Tarus_Station-mat111.6-0 ; 
       Tarus_Station-mat112.6-0 ; 
       Tarus_Station-mat113.6-0 ; 
       Tarus_Station-mat114.6-0 ; 
       Tarus_Station-mat115.6-0 ; 
       Tarus_Station-mat116.6-0 ; 
       Tarus_Station-mat117.6-0 ; 
       Tarus_Station-mat118.6-0 ; 
       Tarus_Station-mat119.6-0 ; 
       Tarus_Station-mat12.6-0 ; 
       Tarus_Station-mat120.6-0 ; 
       Tarus_Station-mat121.6-0 ; 
       Tarus_Station-mat122.6-0 ; 
       Tarus_Station-mat123.6-0 ; 
       Tarus_Station-mat124.6-0 ; 
       Tarus_Station-mat125.6-0 ; 
       Tarus_Station-mat126.6-0 ; 
       Tarus_Station-mat127.6-0 ; 
       Tarus_Station-mat128.6-0 ; 
       Tarus_Station-mat129.6-0 ; 
       Tarus_Station-mat13.6-0 ; 
       Tarus_Station-mat130.6-0 ; 
       Tarus_Station-mat131.6-0 ; 
       Tarus_Station-mat132.6-0 ; 
       Tarus_Station-mat133.6-0 ; 
       Tarus_Station-mat134.6-0 ; 
       Tarus_Station-mat135.6-0 ; 
       Tarus_Station-mat136.6-0 ; 
       Tarus_Station-mat137.6-0 ; 
       Tarus_Station-mat138.6-0 ; 
       Tarus_Station-mat139.6-0 ; 
       Tarus_Station-mat14.6-0 ; 
       Tarus_Station-mat140.6-0 ; 
       Tarus_Station-mat141.6-0 ; 
       Tarus_Station-mat142.6-0 ; 
       Tarus_Station-mat143.6-0 ; 
       Tarus_Station-mat144.6-0 ; 
       Tarus_Station-mat145.6-0 ; 
       Tarus_Station-mat146.6-0 ; 
       Tarus_Station-mat147.6-0 ; 
       Tarus_Station-mat148.6-0 ; 
       Tarus_Station-mat149.6-0 ; 
       Tarus_Station-mat150.6-0 ; 
       Tarus_Station-mat151.6-0 ; 
       Tarus_Station-mat2.6-0 ; 
       Tarus_Station-mat3.6-0 ; 
       Tarus_Station-mat4.6-0 ; 
       Tarus_Station-mat42.6-0 ; 
       Tarus_Station-mat43.6-0 ; 
       Tarus_Station-mat44.6-0 ; 
       Tarus_Station-mat45.6-0 ; 
       Tarus_Station-mat46.6-0 ; 
       Tarus_Station-mat47.6-0 ; 
       Tarus_Station-mat48.6-0 ; 
       Tarus_Station-mat49.6-0 ; 
       Tarus_Station-mat5.6-0 ; 
       Tarus_Station-mat50.6-0 ; 
       Tarus_Station-mat51.6-0 ; 
       Tarus_Station-mat52.6-0 ; 
       Tarus_Station-mat53.6-0 ; 
       Tarus_Station-mat54.6-0 ; 
       Tarus_Station-mat55.6-0 ; 
       Tarus_Station-mat56.6-0 ; 
       Tarus_Station-mat57.6-0 ; 
       Tarus_Station-mat58.6-0 ; 
       Tarus_Station-mat59.6-0 ; 
       Tarus_Station-mat6.6-0 ; 
       Tarus_Station-mat60.6-0 ; 
       Tarus_Station-mat61.6-0 ; 
       Tarus_Station-mat62.6-0 ; 
       Tarus_Station-mat63.6-0 ; 
       Tarus_Station-mat7.6-0 ; 
       Tarus_Station-mat78.6-0 ; 
       Tarus_Station-mat79.6-0 ; 
       Tarus_Station-mat8.6-0 ; 
       Tarus_Station-mat80.6-0 ; 
       Tarus_Station-mat81.6-0 ; 
       Tarus_Station-mat82.6-0 ; 
       Tarus_Station-mat83.6-0 ; 
       Tarus_Station-mat84.6-0 ; 
       Tarus_Station-mat87.6-0 ; 
       Tarus_Station-mat88.6-0 ; 
       Tarus_Station-mat89.6-0 ; 
       Tarus_Station-mat9.6-0 ; 
       Tarus_Station-mat90.6-0 ; 
       Tarus_Station-mat91.6-0 ; 
       Tarus_Station-mat92.6-0 ; 
       Tarus_Station-mat93.6-0 ; 
       Tarus_Station-mat94.6-0 ; 
       Tarus_Station-mat95.6-0 ; 
       Tarus_Station-mat96.6-0 ; 
       Tarus_Station-mat97.6-0 ; 
       Tarus_Station-mat98.6-0 ; 
       Tarus_Station-mat99.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 83     
       bound-bound1.1-0 ; 
       bound-bound13.1-0 ; 
       bound-bound14.1-0 ; 
       bound-bound2.1-0 ; 
       bound-root.5-0 ROOT ; 
       create_bound-tarus3.1-0 ROOT ; 
       ss23-antenn1.1-0 ; 
       ss23-antenn2.1-0 ; 
       ss23-antenn3.1-0 ; 
       ss23-bay2.1-0 ; 
       ss23-bay4.1-0 ; 
       ss23-corrdr0.1-0 ; 
       ss23-corrdr1.1-0 ; 
       ss23-corrdr10.1-0 ; 
       ss23-corrdr11.1-0 ; 
       ss23-corrdr2.1-0 ; 
       ss23-corrdr3.1-0 ; 
       ss23-corrdr4.1-0 ; 
       ss23-corrdr6.1-0 ; 
       ss23-corrdr9.1-0 ; 
       ss23-garage2A.1-0 ; 
       ss23-garage2B.1-0 ; 
       ss23-garage2C.1-0 ; 
       ss23-garage2D.1-0 ; 
       ss23-garage2E.1-0 ; 
       ss23-garage4A.1-0 ; 
       ss23-garage4B.1-0 ; 
       ss23-garage4C.1-0 ; 
       ss23-garage4D.1-0 ; 
       ss23-garage4E.1-0 ; 
       ss23-LAUNCH_TUBE.1-0 ; 
       ss23-LAUNCH_TUBE_1.1-0 ; 
       ss23-launch1.1-0 ; 
       ss23-launch2.1-0 ; 
       ss23-mfuselg.4-0 ; 
       ss23-null1.1-0 ; 
       ss23-SS0.1-0 ; 
       ss23-SS1.1-0 ; 
       ss23-SS10.1-0 ; 
       ss23-SS11.1-0 ; 
       ss23-SS12.1-0 ; 
       ss23-SS13.3-0 ; 
       ss23-SS14.3-0 ; 
       ss23-SS15.3-0 ; 
       ss23-SS16.1-0 ; 
       ss23-SS17.3-0 ; 
       ss23-SS18.3-0 ; 
       ss23-SS19.3-0 ; 
       ss23-SS2.1-0 ; 
       ss23-SS20.3-0 ; 
       ss23-SS21.3-0 ; 
       ss23-SS22.3-0 ; 
       ss23-SS23.3-0 ; 
       ss23-ss23_2.19-0 ROOT ; 
       ss23-SS24.3-0 ; 
       ss23-SS25.3-0 ; 
       ss23-SS26.3-0 ; 
       ss23-SS27.3-0 ; 
       ss23-SS28.3-0 ; 
       ss23-SS29.2-0 ; 
       ss23-SS3.1-0 ; 
       ss23-SS30.2-0 ; 
       ss23-SS31.2-0 ; 
       ss23-SS32.2-0 ; 
       ss23-SS33.2-0 ; 
       ss23-SS34.2-0 ; 
       ss23-SS35.2-0 ; 
       ss23-SS36.2-0 ; 
       ss23-SS37.2-0 ; 
       ss23-SS38.2-0 ; 
       ss23-SS39.2-0 ; 
       ss23-SS4.1-0 ; 
       ss23-SS40.2-0 ; 
       ss23-SS41.2-0 ; 
       ss23-SS42.2-0 ; 
       ss23-SS43.2-0 ; 
       ss23-SS44.2-0 ; 
       ss23-SS5.1-0 ; 
       ss23-SS6.1-0 ; 
       ss23-SS7.1-0 ; 
       ss23-SS8.1-0 ; 
       ss23-SS9.1-0 ; 
       ss23-tarus1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss23/PICTURES/bgrnd57 ; 
       E:/Pete_Data2/space_station/ss/ss23/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/ss23/PICTURES/ss23 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       NEW_SS23-create_bound.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 72     
       create_bound-t2d116.1-0 ; 
       create_bound-t2d117.1-0 ; 
       create_bound-t2d118.1-0 ; 
       create_bound-t2d119.1-0 ; 
       rotate-t2d110.1-0 ; 
       rotate-t2d111.1-0 ; 
       rotate-t2d112.1-0 ; 
       rotate-t2d113.1-0 ; 
       rotate-t2d114.3-0 ; 
       rotate-t2d115.3-0 ; 
       Tarus_Station-t2d1.9-0 ; 
       Tarus_Station-t2d10.9-0 ; 
       Tarus_Station-t2d100.9-0 ; 
       Tarus_Station-t2d101.9-0 ; 
       Tarus_Station-t2d102.9-0 ; 
       Tarus_Station-t2d11.9-0 ; 
       Tarus_Station-t2d2.9-0 ; 
       Tarus_Station-t2d3.9-0 ; 
       Tarus_Station-t2d38.9-0 ; 
       Tarus_Station-t2d39.9-0 ; 
       Tarus_Station-t2d4.9-0 ; 
       Tarus_Station-t2d40.9-0 ; 
       Tarus_Station-t2d41.9-0 ; 
       Tarus_Station-t2d42.9-0 ; 
       Tarus_Station-t2d43.9-0 ; 
       Tarus_Station-t2d44.9-0 ; 
       Tarus_Station-t2d45.9-0 ; 
       Tarus_Station-t2d46.9-0 ; 
       Tarus_Station-t2d47.9-0 ; 
       Tarus_Station-t2d48.9-0 ; 
       Tarus_Station-t2d49.9-0 ; 
       Tarus_Station-t2d5.9-0 ; 
       Tarus_Station-t2d50.9-0 ; 
       Tarus_Station-t2d51.9-0 ; 
       Tarus_Station-t2d52.9-0 ; 
       Tarus_Station-t2d53.9-0 ; 
       Tarus_Station-t2d54.9-0 ; 
       Tarus_Station-t2d55.9-0 ; 
       Tarus_Station-t2d56.9-0 ; 
       Tarus_Station-t2d57.9-0 ; 
       Tarus_Station-t2d58.9-0 ; 
       Tarus_Station-t2d59.9-0 ; 
       Tarus_Station-t2d6.9-0 ; 
       Tarus_Station-t2d7.9-0 ; 
       Tarus_Station-t2d74.10-0 ; 
       Tarus_Station-t2d75.10-0 ; 
       Tarus_Station-t2d76.10-0 ; 
       Tarus_Station-t2d77.9-0 ; 
       Tarus_Station-t2d78.9-0 ; 
       Tarus_Station-t2d79.9-0 ; 
       Tarus_Station-t2d8.9-0 ; 
       Tarus_Station-t2d80.9-0 ; 
       Tarus_Station-t2d81.9-0 ; 
       Tarus_Station-t2d82.9-0 ; 
       Tarus_Station-t2d83.9-0 ; 
       Tarus_Station-t2d84.9-0 ; 
       Tarus_Station-t2d85.9-0 ; 
       Tarus_Station-t2d86.9-0 ; 
       Tarus_Station-t2d87.9-0 ; 
       Tarus_Station-t2d88.9-0 ; 
       Tarus_Station-t2d89.9-0 ; 
       Tarus_Station-t2d9.9-0 ; 
       Tarus_Station-t2d90.9-0 ; 
       Tarus_Station-t2d91.9-0 ; 
       Tarus_Station-t2d92.9-0 ; 
       Tarus_Station-t2d93.9-0 ; 
       Tarus_Station-t2d94.9-0 ; 
       Tarus_Station-t2d95.9-0 ; 
       Tarus_Station-t2d96.9-0 ; 
       Tarus_Station-t2d97.9-0 ; 
       Tarus_Station-t2d98.9-0 ; 
       Tarus_Station-t2d99.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 11 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 53 110 ; 
       10 53 110 ; 
       11 34 110 ; 
       12 11 110 ; 
       13 35 110 ; 
       14 35 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 11 110 ; 
       18 35 110 ; 
       19 35 110 ; 
       20 9 110 ; 
       21 9 110 ; 
       22 9 110 ; 
       23 9 110 ; 
       24 9 110 ; 
       25 10 110 ; 
       26 10 110 ; 
       27 10 110 ; 
       28 10 110 ; 
       29 10 110 ; 
       30 53 110 ; 
       31 53 110 ; 
       32 53 110 ; 
       33 53 110 ; 
       34 53 110 ; 
       35 11 110 ; 
       36 34 110 ; 
       37 36 110 ; 
       38 36 110 ; 
       39 36 110 ; 
       40 36 110 ; 
       41 36 110 ; 
       42 36 110 ; 
       43 36 110 ; 
       44 36 110 ; 
       45 36 110 ; 
       46 36 110 ; 
       47 36 110 ; 
       48 36 110 ; 
       49 36 110 ; 
       50 36 110 ; 
       51 36 110 ; 
       52 36 110 ; 
       54 36 110 ; 
       55 36 110 ; 
       56 36 110 ; 
       57 36 110 ; 
       58 36 110 ; 
       59 36 110 ; 
       60 36 110 ; 
       61 36 110 ; 
       62 36 110 ; 
       63 36 110 ; 
       64 36 110 ; 
       65 36 110 ; 
       66 36 110 ; 
       67 36 110 ; 
       68 36 110 ; 
       69 36 110 ; 
       70 36 110 ; 
       71 36 110 ; 
       72 36 110 ; 
       73 36 110 ; 
       74 36 110 ; 
       75 36 110 ; 
       76 36 110 ; 
       77 36 110 ; 
       78 36 110 ; 
       79 36 110 ; 
       80 36 110 ; 
       81 36 110 ; 
       82 53 110 ; 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 75 300 ; 
       6 103 300 ; 
       7 106 300 ; 
       8 104 300 ; 
       12 10 300 ; 
       12 89 300 ; 
       12 90 300 ; 
       12 91 300 ; 
       12 92 300 ; 
       12 93 300 ; 
       12 109 300 ; 
       12 110 300 ; 
       13 13 300 ; 
       13 60 300 ; 
       13 61 300 ; 
       13 63 300 ; 
       13 64 300 ; 
       13 65 300 ; 
       13 66 300 ; 
       13 67 300 ; 
       14 14 300 ; 
       14 68 300 ; 
       14 69 300 ; 
       14 70 300 ; 
       14 71 300 ; 
       14 72 300 ; 
       14 73 300 ; 
       14 74 300 ; 
       15 15 300 ; 
       15 76 300 ; 
       15 77 300 ; 
       15 86 300 ; 
       15 97 300 ; 
       15 62 300 ; 
       15 107 300 ; 
       16 16 300 ; 
       16 78 300 ; 
       16 79 300 ; 
       16 80 300 ; 
       16 81 300 ; 
       16 82 300 ; 
       17 17 300 ; 
       17 83 300 ; 
       17 84 300 ; 
       17 85 300 ; 
       17 87 300 ; 
       17 88 300 ; 
       17 108 300 ; 
       18 11 300 ; 
       18 94 300 ; 
       18 95 300 ; 
       18 96 300 ; 
       18 98 300 ; 
       18 99 300 ; 
       18 100 300 ; 
       18 101 300 ; 
       19 12 300 ; 
       19 53 300 ; 
       19 54 300 ; 
       19 55 300 ; 
       19 56 300 ; 
       19 57 300 ; 
       19 58 300 ; 
       19 59 300 ; 
       30 6 300 ; 
       30 7 300 ; 
       31 8 300 ; 
       31 9 300 ; 
       34 102 300 ; 
       34 105 300 ; 
       34 114 300 ; 
       34 18 300 ; 
       34 29 300 ; 
       34 40 300 ; 
       34 51 300 ; 
       37 39 300 ; 
       38 52 300 ; 
       39 45 300 ; 
       40 46 300 ; 
       41 113 300 ; 
       42 117 300 ; 
       43 118 300 ; 
       44 111 300 ; 
       45 115 300 ; 
       46 116 300 ; 
       47 119 300 ; 
       48 41 300 ; 
       49 112 300 ; 
       50 124 300 ; 
       51 21 300 ; 
       52 121 300 ; 
       54 123 300 ; 
       55 19 300 ; 
       56 20 300 ; 
       57 120 300 ; 
       58 122 300 ; 
       59 37 300 ; 
       60 42 300 ; 
       61 26 300 ; 
       62 27 300 ; 
       63 38 300 ; 
       64 25 300 ; 
       65 34 300 ; 
       66 33 300 ; 
       67 24 300 ; 
       68 28 300 ; 
       69 35 300 ; 
       70 36 300 ; 
       71 43 300 ; 
       72 30 300 ; 
       73 23 300 ; 
       74 32 300 ; 
       75 31 300 ; 
       76 22 300 ; 
       77 44 300 ; 
       78 47 300 ; 
       79 48 300 ; 
       80 49 300 ; 
       81 50 300 ; 
       82 4 300 ; 
       82 5 300 ; 
       5 0 300 ; 
       5 2 300 ; 
       3 1 300 ; 
       3 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 8 401 ; 
       5 9 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 51 401 ; 
       18 43 401 ; 
       29 50 401 ; 
       40 61 401 ; 
       51 11 401 ; 
       53 53 401 ; 
       54 54 401 ; 
       55 55 401 ; 
       56 56 401 ; 
       57 57 401 ; 
       58 58 401 ; 
       59 59 401 ; 
       60 60 401 ; 
       61 62 401 ; 
       62 15 401 ; 
       63 63 401 ; 
       64 64 401 ; 
       65 65 401 ; 
       66 66 401 ; 
       67 67 401 ; 
       68 68 401 ; 
       69 69 401 ; 
       70 70 401 ; 
       71 71 401 ; 
       72 12 401 ; 
       73 13 401 ; 
       74 14 401 ; 
       76 10 401 ; 
       77 16 401 ; 
       78 18 401 ; 
       79 19 401 ; 
       80 21 401 ; 
       81 22 401 ; 
       82 23 401 ; 
       83 24 401 ; 
       84 25 401 ; 
       85 26 401 ; 
       86 17 401 ; 
       87 27 401 ; 
       88 28 401 ; 
       89 29 401 ; 
       90 30 401 ; 
       91 32 401 ; 
       92 33 401 ; 
       93 34 401 ; 
       94 35 401 ; 
       95 36 401 ; 
       96 37 401 ; 
       97 20 401 ; 
       98 38 401 ; 
       99 39 401 ; 
       100 40 401 ; 
       101 41 401 ; 
       103 44 401 ; 
       104 45 401 ; 
       105 31 401 ; 
       106 46 401 ; 
       107 47 401 ; 
       108 48 401 ; 
       109 49 401 ; 
       110 52 401 ; 
       114 42 401 ; 
       0 0 401 ; 
       2 1 401 ; 
       1 2 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 2.5 -18.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 5 -18.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -18.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 0 -20.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -20.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 5 -20.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -20.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 0 -22.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -22.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -22.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -22.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 0 -24.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -24.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 5 -24.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -24.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 0 -26.05063 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       8 SCHEM 25 -12 0 MPRFLG 0 ; 
       9 SCHEM 142.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 155 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       14 SCHEM 15 -12 0 MPRFLG 0 ; 
       15 SCHEM 5 -10 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 10 -10 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       19 SCHEM 20 -12 0 MPRFLG 0 ; 
       20 SCHEM 137.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 140 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 142.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 145 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 147.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 150 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 152.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 155 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 157.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 160 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 167.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 170 -6 0 MPRFLG 0 ; 
       32 SCHEM 162.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 165 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 68.75 -6 0 MPRFLG 0 ; 
       35 SCHEM 16.25 -10 0 MPRFLG 0 ; 
       36 SCHEM 81.25 -8 0 MPRFLG 0 ; 
       37 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 50 -10 0 MPRFLG 0 ; 
       39 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       40 SCHEM 55 -10 0 MPRFLG 0 ; 
       41 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       42 SCHEM 60 -10 0 MPRFLG 0 ; 
       43 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       44 SCHEM 65 -10 0 MPRFLG 0 ; 
       45 SCHEM 67.5 -10 0 MPRFLG 0 ; 
       46 SCHEM 70 -10 0 MPRFLG 0 ; 
       47 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       48 SCHEM 30 -10 0 MPRFLG 0 ; 
       49 SCHEM 75 -10 0 MPRFLG 0 ; 
       50 SCHEM 77.5 -10 0 MPRFLG 0 ; 
       51 SCHEM 80 -10 0 MPRFLG 0 ; 
       52 SCHEM 82.5 -10 0 MPRFLG 0 ; 
       53 SCHEM 85 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       54 SCHEM 85 -10 0 MPRFLG 0 ; 
       55 SCHEM 87.5 -10 0 MPRFLG 0 ; 
       56 SCHEM 90 -10 0 MPRFLG 0 ; 
       57 SCHEM 92.5 -10 0 MPRFLG 0 ; 
       58 SCHEM 95 -10 0 MPRFLG 0 ; 
       59 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       60 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       61 SCHEM 100 -10 0 MPRFLG 0 ; 
       62 SCHEM 102.5 -10 0 MPRFLG 0 ; 
       63 SCHEM 105 -10 0 MPRFLG 0 ; 
       64 SCHEM 107.5 -10 0 MPRFLG 0 ; 
       65 SCHEM 110 -10 0 MPRFLG 0 ; 
       66 SCHEM 112.5 -10 0 MPRFLG 0 ; 
       67 SCHEM 115 -10 0 MPRFLG 0 ; 
       68 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       69 SCHEM 120 -10 0 MPRFLG 0 ; 
       70 SCHEM 122.5 -10 0 MPRFLG 0 ; 
       71 SCHEM 35 -10 0 MPRFLG 0 ; 
       72 SCHEM 125 -10 0 MPRFLG 0 ; 
       73 SCHEM 127.5 -10 0 MPRFLG 0 ; 
       74 SCHEM 130 -10 0 MPRFLG 0 ; 
       75 SCHEM 132.5 -10 0 MPRFLG 0 ; 
       76 SCHEM 135 -10 0 MPRFLG 0 ; 
       77 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       78 SCHEM 40 -10 0 MPRFLG 0 ; 
       79 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       80 SCHEM 45 -10 0 MPRFLG 0 ; 
       81 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       82 SCHEM 0 -6 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -16 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 0 -18.05063 0 SRT 1 1 1 -1.570796 -3.141593 1.509958e-007 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -16 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -16 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 3.339378 -14 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 0 -16.05063 0 USR DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 166.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 166.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 169 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 169 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 136.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 86.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 89 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 134 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 126.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 114 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 106.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 99 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 101.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 116.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 136.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 124 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 131.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 129 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 111.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 109 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 119 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 121.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 96.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 104 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 136.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 136.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       95 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       96 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       97 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       98 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       99 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       100 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       101 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       102 SCHEM 136.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       103 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       104 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       105 SCHEM 136.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       106 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       107 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       108 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       109 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       110 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       111 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       112 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       113 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       114 SCHEM 136.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       115 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       116 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       117 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       118 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       119 SCHEM 71.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       120 SCHEM 91.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       121 SCHEM 81.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       122 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       123 SCHEM 84 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       124 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM -1 -20.05063 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -1 -20.05063 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM -1 -18.05063 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM -1 -18.05063 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       4 SCHEM 166.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 166.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 169 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 169 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 136.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 136.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 136.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 136.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 136.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 136.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       69 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       70 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       71 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM -1 -22.05063 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM -1 -22.05063 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -1 -20.05063 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -1 -20.05063 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 40 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
