SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.30-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       bound-bcyl_center_end.1-0 ; 
       bound-bcyl_center_start.1-0 ; 
       bound-bcyl_Radius.1-0 ; 
       bound-bound.2-0 ; 
       bound-bounding_Cylinder.1-0 ; 
       bound-bounding_sphere.1-0 ; 
       bound-bsphere_center.1-0 ; 
       bound-bsphere_radius.1-0 ; 
       bound-ss24a_bound.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new-bound.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 8 110 ; 
       0 4 110 ; 
       2 4 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       5 8 110 ; 
       1 4 110 ; 
       4 8 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
