SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss24a-ss24a.21-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.21-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       clean-light3.1-0 ROOT ; 
       clean-light4.1-0 ROOT ; 
       clean-light5.1-0 ROOT ; 
       clean-light6.1-0 ROOT ; 
       set-light1.13-0 ROOT ; 
       set-light2.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       clean-entrance2.1-0 ; 
       clean-mat298.1-0 ; 
       clean-mat299.1-0 ; 
       clean-mat300.1-0 ; 
       clean-mat301.1-0 ; 
       clean-mat302.1-0 ; 
       clean-mat303.1-0 ; 
       clean-mat304.1-0 ; 
       clean-top1.1-0 ; 
       merge-bottom.2-0 ; 
       merge-mat1.2-0 ; 
       merge-mat173.2-0 ; 
       merge-mat188.2-0 ; 
       merge-mat189.2-0 ; 
       merge-mat190.2-0 ; 
       merge-mat196.2-0 ; 
       merge-mat206.1-0 ; 
       merge-mat207.1-0 ; 
       merge-mat223.2-0 ; 
       merge-mat225.2-0 ; 
       merge-mat228.2-0 ; 
       merge-mat230.2-0 ; 
       merge-mat232.2-0 ; 
       ready-mat2.3-0 ; 
       set-entrance1.2-0 ; 
       set-tunnel1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 92     
       ss24a-antenn1.1-0 ; 
       ss24a-antenn2.1-0 ; 
       ss24a-bounding_Clyinder.1-0 ; 
       ss24a-doccon_1.1-0 ; 
       ss24a-ffuselg_1.1-0 ; 
       ss24a-ffuselg1_1.1-0 ; 
       ss24a-fuselg2.2-0 ; 
       ss24a-lndpad1.1-0 ; 
       ss24a-lndpad2.1-0 ; 
       ss24a-lndpad3.1-0 ; 
       ss24a-lndpad4.1-0 ; 
       ss24a-lndpad5.1-0 ; 
       ss24a-lndpad6.1-0 ; 
       ss24a-lndpad7.1-0 ; 
       ss24a-lndpad8.1-0 ; 
       ss24a-lturatt.1-0 ; 
       ss24a-nurbs3.2-0 ; 
       ss24a-rantenn1_1.1-0 ; 
       ss24a-rantenn1_2.1-0 ; 
       ss24a-rantenn2_1.1-0 ; 
       ss24a-rantenn2_2.1-0 ; 
       ss24a-rfuselg_1.1-0 ; 
       ss24a-rfuselg_2.1-0 ; 
       ss24a-rturatt.1-0 ; 
       ss24a-SS1_1.1-0 ; 
       ss24a-SS10_1.1-0 ; 
       ss24a-SS11_1.1-0 ; 
       ss24a-SS12_1.1-0 ; 
       ss24a-SS13_1.1-0 ; 
       ss24a-SS14_1.1-0 ; 
       ss24a-SS15_1.1-0 ; 
       ss24a-SS17_1.1-0 ; 
       ss24a-SS18_1.1-0 ; 
       ss24a-SS19_1.1-0 ; 
       ss24a-SS2_1.1-0 ; 
       ss24a-SS20_1.1-0 ; 
       ss24a-SS21_1.1-0 ; 
       ss24a-SS22_1.1-0 ; 
       ss24a-SS23_1.1-0 ; 
       ss24a-SS24_1.1-0 ; 
       ss24a-ss24a.18-0 ROOT ; 
       ss24a-SS25_1.1-0 ; 
       ss24a-SS26_1.1-0 ; 
       ss24a-SS27_1.1-0 ; 
       ss24a-SS28_1.1-0 ; 
       ss24a-SS29_1.1-0 ; 
       ss24a-SS3_1.1-0 ; 
       ss24a-SS30_1.1-0 ; 
       ss24a-SS31_1.1-0 ; 
       ss24a-SS32_1.1-0 ; 
       ss24a-SS33_1.1-0 ; 
       ss24a-SS34_1.1-0 ; 
       ss24a-SS35_1.1-0 ; 
       ss24a-SS36_1.1-0 ; 
       ss24a-SS37_1.1-0 ; 
       ss24a-SS38_1.1-0 ; 
       ss24a-SS39_1.1-0 ; 
       ss24a-SS4_1.1-0 ; 
       ss24a-SS40_1.1-0 ; 
       ss24a-SS41_1.1-0 ; 
       ss24a-SS42_1.1-0 ; 
       ss24a-SS43_1.1-0 ; 
       ss24a-SS44_1.1-0 ; 
       ss24a-SS45_1.1-0 ; 
       ss24a-SS46_1.1-0 ; 
       ss24a-SS47_1.1-0 ; 
       ss24a-SS48_1.1-0 ; 
       ss24a-SS49_1.1-0 ; 
       ss24a-SS5_1.1-0 ; 
       ss24a-SS6_1.1-0 ; 
       ss24a-SS7_1.1-0 ; 
       ss24a-SS8_1.1-0 ; 
       ss24a-SS9_1.1-0 ; 
       ss24a-SSaa1.1-0 ; 
       ss24a-SSaa2.1-0 ; 
       ss24a-SSal.1-0 ; 
       ss24a-SSar.1-0 ; 
       ss24a-SSf1_1.1-0 ; 
       ss24a-SSf1_2.1-0 ; 
       ss24a-SSf1_3.1-0 ; 
       ss24a-SSf1_4.1-0 ; 
       ss24a-SSf2_1.1-0 ; 
       ss24a-SSf2_2.1-0 ; 
       ss24a-SSf2_3.1-0 ; 
       ss24a-SSf2_4.1-0 ; 
       ss24a-SSfl.1-0 ; 
       ss24a-SSfr.1-0 ; 
       ss24a-SSrt_1.1-0 ; 
       ss24a-SSrt_2.1-0 ; 
       ss24a-tplatfm.1-0 ; 
       ss24a-ttractr1.1-0 ; 
       ss24a-ttractr2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       D:/Pete_Data/Softimage/space_station/ss/ss24a/PICTURES/rendermap_light ; 
       D:/Pete_Data/Softimage/space_station/ss/ss24a/PICTURES/ss24a ; 
       D:/Pete_Data/Softimage/space_station/ss/ss24a/PICTURES/ss24b ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new-clean.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       clean-sq1.1-0 ; 
       clean-t2d140.1-0 ; 
       clean-t2d145.1-0 ; 
       clean-t2d146.1-0 ; 
       clean-t2d147.1-0 ; 
       clean-t2d148.1-0 ; 
       clean-t2d149.1-0 ; 
       clean-t2d152.1-0 ; 
       clean-t2d153.1-0 ; 
       clean-t2d154.1-0 ; 
       clean-t2d155.1-0 ; 
       clean-t2d156.1-0 ; 
       clean-t2d157.1-0 ; 
       clean-t2d158.1-0 ; 
       clean-t2d159.1-0 ; 
       merge-sq.2-0 ; 
       merge-t2d102.2-0 ; 
       merge-t2d125.4-0 ; 
       merge-t2d89.3-0 ; 
       merge-t2d90.3-0 ; 
       merge-t2d91.3-0 ; 
       ready-t2d1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       22 89 110 ; 
       18 22 110 ; 
       20 18 110 ; 
       88 18 110 ; 
       16 40 110 ; 
       0 16 110 ; 
       1 0 110 ; 
       3 4 110 ; 
       4 89 110 ; 
       5 89 110 ; 
       6 40 110 ; 
       7 89 110 ; 
       8 89 110 ; 
       9 89 110 ; 
       10 89 110 ; 
       11 89 110 ; 
       12 89 110 ; 
       13 89 110 ; 
       14 89 110 ; 
       15 16 110 ; 
       17 21 110 ; 
       19 17 110 ; 
       21 89 110 ; 
       23 16 110 ; 
       24 7 110 ; 
       25 70 110 ; 
       26 70 110 ; 
       27 70 110 ; 
       28 9 110 ; 
       29 28 110 ; 
       30 28 110 ; 
       31 28 110 ; 
       32 28 110 ; 
       33 28 110 ; 
       34 24 110 ; 
       35 10 110 ; 
       36 35 110 ; 
       37 35 110 ; 
       38 35 110 ; 
       39 35 110 ; 
       41 35 110 ; 
       42 11 110 ; 
       43 42 110 ; 
       44 42 110 ; 
       45 42 110 ; 
       46 24 110 ; 
       47 42 110 ; 
       48 42 110 ; 
       49 12 110 ; 
       50 49 110 ; 
       51 49 110 ; 
       52 49 110 ; 
       53 49 110 ; 
       54 49 110 ; 
       55 13 110 ; 
       56 55 110 ; 
       57 24 110 ; 
       58 55 110 ; 
       59 55 110 ; 
       60 55 110 ; 
       61 55 110 ; 
       62 14 110 ; 
       63 62 110 ; 
       64 62 110 ; 
       65 62 110 ; 
       66 62 110 ; 
       67 62 110 ; 
       68 24 110 ; 
       69 24 110 ; 
       70 8 110 ; 
       71 70 110 ; 
       72 70 110 ; 
       73 1 110 ; 
       74 1 110 ; 
       75 4 110 ; 
       76 4 110 ; 
       77 16 110 ; 
       78 16 110 ; 
       79 16 110 ; 
       80 16 110 ; 
       81 6 110 ; 
       82 6 110 ; 
       83 6 110 ; 
       84 6 110 ; 
       85 5 110 ; 
       86 5 110 ; 
       87 17 110 ; 
       89 91 110 ; 
       90 6 110 ; 
       91 90 110 ; 
       2 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       22 1 300 ; 
       22 2 300 ; 
       22 3 300 ; 
       18 4 300 ; 
       20 5 300 ; 
       88 6 300 ; 
       16 23 300 ; 
       16 24 300 ; 
       16 25 300 ; 
       0 16 300 ; 
       1 17 300 ; 
       4 7 300 ; 
       5 20 300 ; 
       6 8 300 ; 
       6 0 300 ; 
       17 4 300 ; 
       19 5 300 ; 
       21 18 300 ; 
       21 15 300 ; 
       21 19 300 ; 
       25 22 300 ; 
       26 22 300 ; 
       27 22 300 ; 
       29 21 300 ; 
       30 22 300 ; 
       31 22 300 ; 
       32 22 300 ; 
       33 22 300 ; 
       34 21 300 ; 
       36 21 300 ; 
       37 22 300 ; 
       38 22 300 ; 
       39 22 300 ; 
       41 22 300 ; 
       43 21 300 ; 
       44 22 300 ; 
       45 22 300 ; 
       46 22 300 ; 
       47 22 300 ; 
       48 22 300 ; 
       50 21 300 ; 
       51 22 300 ; 
       52 22 300 ; 
       53 22 300 ; 
       54 22 300 ; 
       56 21 300 ; 
       57 22 300 ; 
       58 22 300 ; 
       59 22 300 ; 
       60 22 300 ; 
       61 22 300 ; 
       63 21 300 ; 
       64 22 300 ; 
       65 22 300 ; 
       66 22 300 ; 
       67 22 300 ; 
       68 22 300 ; 
       69 22 300 ; 
       71 21 300 ; 
       72 22 300 ; 
       73 21 300 ; 
       74 21 300 ; 
       75 21 300 ; 
       76 21 300 ; 
       77 21 300 ; 
       78 21 300 ; 
       79 21 300 ; 
       80 21 300 ; 
       81 21 300 ; 
       82 21 300 ; 
       83 21 300 ; 
       84 21 300 ; 
       85 21 300 ; 
       86 21 300 ; 
       87 6 300 ; 
       89 9 300 ; 
       89 8 300 ; 
       89 0 300 ; 
       90 10 300 ; 
       90 12 300 ; 
       90 13 300 ; 
       90 14 300 ; 
       91 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       40 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       11 6 401 ; 
       1 0 401 ; 
       2 7 401 ; 
       3 8 401 ; 
       4 9 401 ; 
       5 10 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 20 401 ; 
       7 14 401 ; 
       15 16 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       9 12 401 ; 
       8 13 401 ; 
       18 15 401 ; 
       19 17 401 ; 
       20 11 401 ; 
       23 21 401 ; 
       24 4 401 ; 
       25 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       4 SCHEM 212.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 215 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 217.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 220 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 222.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 225 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       22 SCHEM 138.75 -10 0 MPRFLG 0 ; 
       18 SCHEM 135 -12 0 MPRFLG 0 ; 
       20 SCHEM 132.5 -14 0 MPRFLG 0 ; 
       88 SCHEM 135 -14 0 MPRFLG 0 ; 
       16 SCHEM 193.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 181.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 180 -6 0 MPRFLG 0 ; 
       3 SCHEM 20 -12 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -10 0 MPRFLG 0 ; 
       5 SCHEM 127.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 88.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 30 -10 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 55 -10 0 MPRFLG 0 ; 
       10 SCHEM 67.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 80 -10 0 MPRFLG 0 ; 
       12 SCHEM 92.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 105 -10 0 MPRFLG 0 ; 
       14 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 202.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 3.75 -12 0 MPRFLG 0 ; 
       19 SCHEM 2.5 -14 0 MPRFLG 0 ; 
       21 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 197.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 30 -12 0 MPRFLG 0 ; 
       25 SCHEM 42.5 -14 0 MPRFLG 0 ; 
       26 SCHEM 45 -14 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -14 0 MPRFLG 0 ; 
       28 SCHEM 55 -12 0 MPRFLG 0 ; 
       29 SCHEM 50 -14 0 MPRFLG 0 ; 
       30 SCHEM 52.5 -14 0 MPRFLG 0 ; 
       31 SCHEM 55 -14 0 MPRFLG 0 ; 
       32 SCHEM 57.5 -14 0 MPRFLG 0 ; 
       33 SCHEM 60 -14 0 MPRFLG 0 ; 
       34 SCHEM 25 -14 0 MPRFLG 0 ; 
       35 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       36 SCHEM 62.5 -14 0 MPRFLG 0 ; 
       37 SCHEM 65 -14 0 MPRFLG 0 ; 
       38 SCHEM 67.5 -14 0 MPRFLG 0 ; 
       39 SCHEM 70 -14 0 MPRFLG 0 ; 
       40 SCHEM 106.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       41 SCHEM 72.5 -14 0 MPRFLG 0 ; 
       42 SCHEM 80 -12 0 MPRFLG 0 ; 
       43 SCHEM 75 -14 0 MPRFLG 0 ; 
       44 SCHEM 77.5 -14 0 MPRFLG 0 ; 
       45 SCHEM 80 -14 0 MPRFLG 0 ; 
       46 SCHEM 27.5 -14 0 MPRFLG 0 ; 
       47 SCHEM 82.5 -14 0 MPRFLG 0 ; 
       48 SCHEM 85 -14 0 MPRFLG 0 ; 
       49 SCHEM 92.5 -12 0 MPRFLG 0 ; 
       50 SCHEM 87.5 -14 0 MPRFLG 0 ; 
       51 SCHEM 90 -14 0 MPRFLG 0 ; 
       52 SCHEM 92.5 -14 0 MPRFLG 0 ; 
       53 SCHEM 95 -14 0 MPRFLG 0 ; 
       54 SCHEM 97.5 -14 0 MPRFLG 0 ; 
       55 SCHEM 105 -12 0 MPRFLG 0 ; 
       56 SCHEM 100 -14 0 MPRFLG 0 ; 
       57 SCHEM 30 -14 0 MPRFLG 0 ; 
       58 SCHEM 102.5 -14 0 MPRFLG 0 ; 
       59 SCHEM 105 -14 0 MPRFLG 0 ; 
       60 SCHEM 107.5 -14 0 MPRFLG 0 ; 
       61 SCHEM 110 -14 0 MPRFLG 0 ; 
       62 SCHEM 117.5 -12 0 MPRFLG 0 ; 
       63 SCHEM 112.5 -14 0 MPRFLG 0 ; 
       64 SCHEM 115 -14 0 MPRFLG 0 ; 
       65 SCHEM 117.5 -14 0 MPRFLG 0 ; 
       66 SCHEM 120 -14 0 MPRFLG 0 ; 
       67 SCHEM 122.5 -14 0 MPRFLG 0 ; 
       68 SCHEM 32.5 -14 0 MPRFLG 0 ; 
       69 SCHEM 35 -14 0 MPRFLG 0 ; 
       70 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       71 SCHEM 37.5 -14 0 MPRFLG 0 ; 
       72 SCHEM 40 -14 0 MPRFLG 0 ; 
       73 SCHEM 177.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 180 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       76 SCHEM 15 -12 0 MPRFLG 0 ; 
       77 SCHEM 187.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 190 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 195 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 200 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 162.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 165 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 167.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 170 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 127.5 -12 0 MPRFLG 0 ; 
       86 SCHEM 125 -12 0 MPRFLG 0 ; 
       87 SCHEM 5 -14 0 MPRFLG 0 ; 
       89 SCHEM 75 -8 0 MPRFLG 0 ; 
       90 SCHEM 81.25 -4 0 MPRFLG 0 ; 
       91 SCHEM 76.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 192.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 176.2914 -11.08411 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 145 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 140 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 142.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 137.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 132.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 155 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 185 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 147.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 135 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 130 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 125 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 75.97108 -18.78378 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 205 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 210 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       16 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 185 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 182.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 147.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 145 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 140 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 142.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 130 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 137.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 132.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 176.2914 -13.08411 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 207.5 -6 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 210 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 152.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 155 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 205 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 211.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
