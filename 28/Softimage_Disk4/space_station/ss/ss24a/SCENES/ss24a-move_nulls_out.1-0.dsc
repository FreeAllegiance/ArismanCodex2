SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.1-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       move_nulls_out-light1_1.1-0 ROOT ; 
       move_nulls_out-light2_1.1-0 ROOT ; 
       move_nulls_out-light3_1.1-0 ROOT ; 
       move_nulls_out-light4_1.1-0 ROOT ; 
       move_nulls_out-light5_1.1-0 ROOT ; 
       move_nulls_out-light6_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       add_pipe-mat270.4-0 ; 
       add_pipe-mat271.2-0 ; 
       add_pipe-mat272.2-0 ; 
       add_pipe-mat273.2-0 ; 
       move_nulls_out-mat204_1.1-0 ; 
       move_nulls_out-mat205_1.1-0 ; 
       move_nulls_out-mat206_1.1-0 ; 
       move_nulls_out-mat207_1.1-0 ; 
       move_nulls_out-mat208_1.1-0 ; 
       move_nulls_out-mat209_1.1-0 ; 
       move_nulls_out-mat210_1.1-0 ; 
       move_nulls_out-mat268_1.1-0 ; 
       move_nulls_out-mat269_1.1-0 ; 
       move_nulls_out-mat270.1-0 ; 
       move_nulls_out-mat271.1-0 ; 
       move_nulls_out-mat274.1-0 ; 
       move_nulls_out-mat275.1-0 ; 
       move_nulls_out-mat276.1-0 ; 
       move_nulls_out-mat277.1-0 ; 
       new-mat2_1.1-0 ; 
       plaza8_F-mat1.12-0 ; 
       plaza8_F-mat40.12-0 ; 
       plaza8_F-mat41.12-0 ; 
       plaza8_F-mat42.12-0 ; 
       plaza8_F-mat43.12-0 ; 
       plaza8_F-mat44.12-0 ; 
       plaza8_F-mat45.12-0 ; 
       plaza8_F-mat46.12-0 ; 
       plaza8_F-mat47.12-0 ; 
       plaza8_F-mat48.12-0 ; 
       plaza8_F-mat49.12-0 ; 
       plaza8_F-mat50.12-0 ; 
       plaza8_F-mat51.12-0 ; 
       plaza8_F-mat52.12-0 ; 
       plaza8_F-mat53.12-0 ; 
       plaza8_F-mat54.12-0 ; 
       plaza8_F-mat55.12-0 ; 
       plaza8_F-mat56.12-0 ; 
       plaza8_F-mat57.12-0 ; 
       plaza8_F-mat58.12-0 ; 
       plaza8_F-mat59.12-0 ; 
       plaza8_F-mat60.12-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       ss24a-bay1.1-0 ; 
       ss24a-bay2.1-0 ; 
       ss24a-cube1.3-0 ; 
       ss24a-cyl1.3-0 ; 
       ss24a-fuselg.5-0 ; 
       ss24a-fuselg1.1-0 ; 
       ss24a-fuselg5.1-0 ; 
       ss24a-fuselg6.1-0 ; 
       ss24a-Garage1A.1-0 ; 
       ss24a-Garage1B.1-0 ; 
       ss24a-Garage1C.1-0 ; 
       ss24a-Garage1D.1-0 ; 
       ss24a-Garage1E.1-0 ; 
       ss24a-Garage2A.1-0 ; 
       ss24a-Garage2B.1-0 ; 
       ss24a-Garage2C.1-0 ; 
       ss24a-Garage2D.1-0 ; 
       ss24a-Garage2E.1-0 ; 
       ss24a-Launch1.1-0 ; 
       ss24a-radar_base.1-0 ; 
       ss24a-radar_Dish.1-0 ; 
       ss24a-skin2.22-0 ; 
       ss24a-ss18.24-0 ; 
       ss24a-ss19a.1-0 ; 
       ss24a-ss24a_1_1.4-0 ROOT ; 
       ss24a-tfuselg1.1-0 ; 
       ss24a-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-move_nulls_out.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 40     
       add_pipe-t2d262.4-0 ; 
       add_pipe-t2d263.2-0 ; 
       add_pipe-t2d264.3-0 ; 
       add_pipe-t2d265.3-0 ; 
       move_nulls_out-t2d196_1.1-0 ; 
       move_nulls_out-t2d197_1.1-0 ; 
       move_nulls_out-t2d198_1.1-0 ; 
       move_nulls_out-t2d199_1.1-0 ; 
       move_nulls_out-t2d200_1.1-0 ; 
       move_nulls_out-t2d201_1.1-0 ; 
       move_nulls_out-t2d260_1.1-0 ; 
       move_nulls_out-t2d261_1.1-0 ; 
       move_nulls_out-t2d262.1-0 ; 
       move_nulls_out-t2d263.1-0 ; 
       move_nulls_out-t2d266.1-0 ; 
       move_nulls_out-t2d267.1-0 ; 
       move_nulls_out-t2d268.1-0 ; 
       move_nulls_out-t2d269.1-0 ; 
       new-t2d2.15-0 ; 
       plaza8_F-t2d38_1.1-0 ; 
       plaza8_F-t2d39_1.1-0 ; 
       plaza8_F-t2d40_1.1-0 ; 
       plaza8_F-t2d41_1.1-0 ; 
       plaza8_F-t2d42_1.1-0 ; 
       plaza8_F-t2d43_1.1-0 ; 
       plaza8_F-t2d44_1.1-0 ; 
       plaza8_F-t2d45_1.1-0 ; 
       plaza8_F-t2d46_1.1-0 ; 
       plaza8_F-t2d47_1.1-0 ; 
       plaza8_F-t2d48_1.1-0 ; 
       plaza8_F-t2d49_1.1-0 ; 
       plaza8_F-t2d50_1.1-0 ; 
       plaza8_F-t2d51_1.1-0 ; 
       plaza8_F-t2d52_1.1-0 ; 
       plaza8_F-t2d53_1.1-0 ; 
       plaza8_F-t2d54_1.1-0 ; 
       plaza8_F-t2d55_1.1-0 ; 
       plaza8_F-t2d56_1.1-0 ; 
       plaza8_F-t2d57_1.1-0 ; 
       plaza8_F-t2d58_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 24 110 ; 
       1 24 110 ; 
       2 21 110 ; 
       3 21 110 ; 
       4 25 110 ; 
       5 2 110 ; 
       6 21 110 ; 
       7 21 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 1 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 19 110 ; 
       21 24 110 ; 
       22 21 110 ; 
       23 22 110 ; 
       25 26 110 ; 
       26 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 12 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       19 14 300 ; 
       20 13 300 ; 
       21 19 300 ; 
       21 11 300 ; 
       25 20 300 ; 
       25 31 300 ; 
       25 38 300 ; 
       25 39 300 ; 
       25 40 300 ; 
       25 41 300 ; 
       26 20 300 ; 
       26 21 300 ; 
       26 22 300 ; 
       26 23 300 ; 
       26 24 300 ; 
       26 25 300 ; 
       26 26 300 ; 
       26 27 300 ; 
       26 28 300 ; 
       26 29 300 ; 
       26 30 300 ; 
       26 32 300 ; 
       26 33 300 ; 
       26 34 300 ; 
       26 35 300 ; 
       26 36 300 ; 
       26 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 11 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       19 18 401 ; 
       21 23 401 ; 
       22 25 401 ; 
       23 27 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 21 401 ; 
       27 22 401 ; 
       28 24 401 ; 
       29 26 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 146.804 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 149.304 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 151.804 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 154.304 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 156.804 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 159.304 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 126.804 -2 0 MPRFLG 0 ; 
       1 SCHEM 139.304 -2 0 MPRFLG 0 ; 
       2 SCHEM 81.80405 -4 0 MPRFLG 0 ; 
       3 SCHEM 95.55405 -4 0 MPRFLG 0 ; 
       4 SCHEM 13.05405 -10 0 MPRFLG 0 ; 
       5 SCHEM 79.30405 -6 0 MPRFLG 0 ; 
       6 SCHEM 100.554 -4 0 MPRFLG 0 ; 
       7 SCHEM 105.554 -4 0 MPRFLG 0 ; 
       8 SCHEM 121.804 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 124.304 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 126.804 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 129.304 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 131.804 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 134.304 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 136.804 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 139.304 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 141.804 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 144.304 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 119.304 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 115.554 -2 0 MPRFLG 0 ; 
       20 SCHEM 114.304 -4 0 MPRFLG 0 ; 
       21 SCHEM 60.55405 -2 0 MPRFLG 0 ; 
       22 SCHEM 39.30405 -4 0 MPRFLG 0 ; 
       23 SCHEM 9.304047 -6 0 MPRFLG 0 ; 
       24 SCHEM 76.80405 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 20.55405 -8 0 MPRFLG 0 ; 
       26 SCHEM 40.55405 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 96.80405 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 94.30405 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14.30405 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.80405 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 111.804 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -21.96488 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 39.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 41.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 49.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 51.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 54.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 56.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 59.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 61.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 64.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 66.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 69.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 86.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 71.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 74.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 76.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 79.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 81.80405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 84.30405 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 109.304 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 91.80405 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 114.304 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 116.804 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 101.804 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 99.30405 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 106.804 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 104.304 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 96.80405 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 94.30405 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14.30405 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.80405 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 111.804 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 36.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 41.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 44.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 46.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 54.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 56.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 59.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 61.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 64.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 66.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 69.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 71.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 74.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 76.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 79.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 81.80405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 84.30405 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 109.304 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 89.30405 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 114.304 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 116.804 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 101.804 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 99.30405 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 106.804 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 104.304 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
