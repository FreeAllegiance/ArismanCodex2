SDSC3.81
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.1-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       add_pipe-mat270.4-0 ; 
       add_pipe-mat271.2-0 ; 
       add_pipe-mat272.2-0 ; 
       add_pipe-mat273.2-0 ; 
       new-mat2.7-0 ; 
       plaza8_F-mat1.12-0 ; 
       plaza8_F-mat40.12-0 ; 
       plaza8_F-mat41.12-0 ; 
       plaza8_F-mat42.12-0 ; 
       plaza8_F-mat43.12-0 ; 
       plaza8_F-mat44.12-0 ; 
       plaza8_F-mat45.12-0 ; 
       plaza8_F-mat46.12-0 ; 
       plaza8_F-mat47.12-0 ; 
       plaza8_F-mat48.12-0 ; 
       plaza8_F-mat49.12-0 ; 
       plaza8_F-mat50.12-0 ; 
       plaza8_F-mat51.12-0 ; 
       plaza8_F-mat52.12-0 ; 
       plaza8_F-mat53.12-0 ; 
       plaza8_F-mat54.12-0 ; 
       plaza8_F-mat55.12-0 ; 
       plaza8_F-mat56.12-0 ; 
       plaza8_F-mat57.12-0 ; 
       plaza8_F-mat58.12-0 ; 
       plaza8_F-mat59.12-0 ; 
       plaza8_F-mat60.12-0 ; 
       replace_old_saucers-mat274.1-0 ; 
       replace_old_saucers-mat275.1-0 ; 
       replace_old_saucers-mat276.1-0 ; 
       replace_old_saucers-mat277.1-0 ; 
       rotate-mat204.5-0 ; 
       rotate-mat205.5-0 ; 
       rotate-mat206.5-0 ; 
       rotate-mat207.5-0 ; 
       rotate-mat208.5-0 ; 
       rotate-mat209.5-0 ; 
       rotate-mat210.5-0 ; 
       rotate-mat268.5-0 ; 
       rotate-mat269.5-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       ss24a-cube1.3-0 ; 
       ss24a-cyl1.3-0 ; 
       ss24a-fuselg.5-0 ; 
       ss24a-fuselg1.1-0 ; 
       ss24a-fuselg5.1-0 ; 
       ss24a-fuselg6.1-0 ; 
       ss24a-skin2.22-0 ; 
       ss24a-ss18.24-0 ; 
       ss24a-ss19a.1-0 ; 
       ss24a-ss24a_1.6-0 ROOT ; 
       ss24a-tfuselg1.1-0 ; 
       ss24a-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-replace_old_saucers.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       add_pipe-t2d262.4-0 ; 
       add_pipe-t2d263.2-0 ; 
       add_pipe-t2d264.2-0 ; 
       add_pipe-t2d265.2-0 ; 
       new-t2d2.15-0 ; 
       plaza8_F-t2d38.18-0 ; 
       plaza8_F-t2d39.18-0 ; 
       plaza8_F-t2d40.18-0 ; 
       plaza8_F-t2d41.18-0 ; 
       plaza8_F-t2d42.18-0 ; 
       plaza8_F-t2d43.18-0 ; 
       plaza8_F-t2d44.18-0 ; 
       plaza8_F-t2d45.18-0 ; 
       plaza8_F-t2d46.18-0 ; 
       plaza8_F-t2d47.18-0 ; 
       plaza8_F-t2d48.18-0 ; 
       plaza8_F-t2d49.18-0 ; 
       plaza8_F-t2d50.18-0 ; 
       plaza8_F-t2d51.18-0 ; 
       plaza8_F-t2d52.18-0 ; 
       plaza8_F-t2d53.18-0 ; 
       plaza8_F-t2d54.18-0 ; 
       plaza8_F-t2d55.18-0 ; 
       plaza8_F-t2d56.18-0 ; 
       plaza8_F-t2d57.18-0 ; 
       plaza8_F-t2d58.18-0 ; 
       replace_old_saucers-t2d266.1-0 ; 
       replace_old_saucers-t2d267.1-0 ; 
       replace_old_saucers-t2d268.1-0 ; 
       replace_old_saucers-t2d269.1-0 ; 
       rotate-t2d196.6-0 ; 
       rotate-t2d197.6-0 ; 
       rotate-t2d198.6-0 ; 
       rotate-t2d199.6-0 ; 
       rotate-t2d200.6-0 ; 
       rotate-t2d201.6-0 ; 
       rotate-t2d260.9-0 ; 
       rotate-t2d261.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 10 110 ; 
       3 0 110 ; 
       6 9 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       10 11 110 ; 
       11 7 110 ; 
       4 6 110 ; 
       5 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 39 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 36 300 ; 
       3 37 300 ; 
       6 4 300 ; 
       6 38 300 ; 
       10 5 300 ; 
       10 16 300 ; 
       10 23 300 ; 
       10 24 300 ; 
       10 25 300 ; 
       10 26 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       11 22 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       5 29 300 ; 
       5 30 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 37 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       6 9 401 ; 
       7 11 401 ; 
       8 13 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 77.54324 -4 0 MPRFLG 0 ; 
       1 SCHEM 91.29324 -4 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -12.05711 0 USR MPRFLG 0 ; 
       3 SCHEM 75.04324 -6 0 MPRFLG 0 ; 
       6 SCHEM 56.29324 -2 0 MPRFLG 0 ; 
       7 SCHEM 35.04324 -4 0 MPRFLG 0 ; 
       8 SCHEM 5.043243 -6 0 MPRFLG 0 ; 
       9 SCHEM 56.29324 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 16.29324 -8 0 MPRFLG 0 ; 
       11 SCHEM 36.29324 -6 0 MPRFLG 0 ; 
       4 SCHEM 96.29324 -4 0 MPRFLG 0 ; 
       5 SCHEM 101.2932 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 90.04324 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 92.54324 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -14.05711 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -14.05711 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 107.5432 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25.7392 -21.96488 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 27.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 30.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 32.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 35.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 37.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 45.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 47.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 50.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 52.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 55.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 57.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 60.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 62.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 82.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 67.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 70.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 72.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 75.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 77.54324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 80.04324 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 105.0432 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 87.54324 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 95.04324 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 97.54324 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 100.0432 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 102.5432 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 90.04324 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 92.54324 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -16.05711 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -16.05711 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 107.5432 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 32.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 35.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 37.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 40.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 45.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 27.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 47.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 30.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 50.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 52.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 55.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 57.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 60.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 62.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 65.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 67.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 70.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 72.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 75.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 77.54324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 80.04324 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 105.0432 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 85.04324 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 95.04324 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 97.54324 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 100.0432 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 102.5432 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
