SDSC3.81
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.1-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       static-light1.1-0 ROOT ; 
       static-light2.1-0 ROOT ; 
       static-light3.1-0 ROOT ; 
       static-light4.1-0 ROOT ; 
       static-light5.1-0 ROOT ; 
       static-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       add_pipe-mat270.4-0 ; 
       add_pipe-mat271.2-0 ; 
       add_pipe-mat272.2-0 ; 
       add_pipe-mat273.2-0 ; 
       new-mat2_1.1-0 ; 
       plaza8_F-mat1.12-0 ; 
       plaza8_F-mat40.12-0 ; 
       plaza8_F-mat41.12-0 ; 
       plaza8_F-mat42.12-0 ; 
       plaza8_F-mat43.12-0 ; 
       plaza8_F-mat44.12-0 ; 
       plaza8_F-mat45.12-0 ; 
       plaza8_F-mat46.12-0 ; 
       plaza8_F-mat47.12-0 ; 
       plaza8_F-mat48.12-0 ; 
       plaza8_F-mat49.12-0 ; 
       plaza8_F-mat50.12-0 ; 
       plaza8_F-mat51.12-0 ; 
       plaza8_F-mat52.12-0 ; 
       plaza8_F-mat53.12-0 ; 
       plaza8_F-mat54.12-0 ; 
       plaza8_F-mat55.12-0 ; 
       plaza8_F-mat56.12-0 ; 
       plaza8_F-mat57.12-0 ; 
       plaza8_F-mat58.12-0 ; 
       plaza8_F-mat59.12-0 ; 
       plaza8_F-mat60.12-0 ; 
       static-mat204_1.1-0 ; 
       static-mat205_1.1-0 ; 
       static-mat206_1.1-0 ; 
       static-mat207_1.1-0 ; 
       static-mat208_1.1-0 ; 
       static-mat209_1.1-0 ; 
       static-mat210_1.1-0 ; 
       static-mat268_1.1-0 ; 
       static-mat269_1.1-0 ; 
       static-mat274.1-0 ; 
       static-mat275.1-0 ; 
       static-mat276.1-0 ; 
       static-mat277.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       ss24a-cube1.3-0 ; 
       ss24a-cyl1.3-0 ; 
       ss24a-fuselg.5-0 ; 
       ss24a-fuselg1.1-0 ; 
       ss24a-fuselg5.1-0 ; 
       ss24a-fuselg6.1-0 ; 
       ss24a-skin2.22-0 ; 
       ss24a-ss18.24-0 ; 
       ss24a-ss19a.1-0 ; 
       ss24a-ss24a_1.10-0 ROOT ; 
       ss24a-tfuselg1.1-0 ; 
       ss24a-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       add_pipe-t2d262.4-0 ; 
       add_pipe-t2d263.2-0 ; 
       add_pipe-t2d264.2-0 ; 
       add_pipe-t2d265.2-0 ; 
       new-t2d2.15-0 ; 
       plaza8_F-t2d38_1.1-0 ; 
       plaza8_F-t2d39_1.1-0 ; 
       plaza8_F-t2d40_1.1-0 ; 
       plaza8_F-t2d41_1.1-0 ; 
       plaza8_F-t2d42_1.1-0 ; 
       plaza8_F-t2d43_1.1-0 ; 
       plaza8_F-t2d44_1.1-0 ; 
       plaza8_F-t2d45_1.1-0 ; 
       plaza8_F-t2d46_1.1-0 ; 
       plaza8_F-t2d47_1.1-0 ; 
       plaza8_F-t2d48_1.1-0 ; 
       plaza8_F-t2d49_1.1-0 ; 
       plaza8_F-t2d50_1.1-0 ; 
       plaza8_F-t2d51_1.1-0 ; 
       plaza8_F-t2d52_1.1-0 ; 
       plaza8_F-t2d53_1.1-0 ; 
       plaza8_F-t2d54_1.1-0 ; 
       plaza8_F-t2d55_1.1-0 ; 
       plaza8_F-t2d56_1.1-0 ; 
       plaza8_F-t2d57_1.1-0 ; 
       plaza8_F-t2d58_1.1-0 ; 
       static-t2d196_1.1-0 ; 
       static-t2d197_1.1-0 ; 
       static-t2d198_1.1-0 ; 
       static-t2d199_1.1-0 ; 
       static-t2d200_1.1-0 ; 
       static-t2d201_1.1-0 ; 
       static-t2d260_1.1-0 ; 
       static-t2d261_1.1-0 ; 
       static-t2d266.1-0 ; 
       static-t2d267.1-0 ; 
       static-t2d268.1-0 ; 
       static-t2d269.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 10 110 ; 
       3 0 110 ; 
       6 9 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       10 11 110 ; 
       11 7 110 ; 
       4 6 110 ; 
       5 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 35 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       6 4 300 ; 
       6 34 300 ; 
       10 5 300 ; 
       10 16 300 ; 
       10 23 300 ; 
       10 24 300 ; 
       10 25 300 ; 
       10 26 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       11 22 300 ; 
       4 36 300 ; 
       4 37 300 ; 
       5 38 300 ; 
       5 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 33 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       6 9 401 ; 
       7 11 401 ; 
       8 13 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       28 26 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -10 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 8.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -13.05405 -21.96488 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
