SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss25-ss25.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       diner_s-cam_int1.1-0 ROOT ; 
       diner_s-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       trading_post_stL-light1.1-0 ROOT ; 
       trading_post_stL-light2.1-0 ROOT ; 
       trading_post_stL-light3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       trading_post_F-mat29.1-0 ; 
       trading_post_F-mat30.1-0 ; 
       trading_post_F-mat31.1-0 ; 
       trading_post_F-mat32.1-0 ; 
       trading_post_F-mat33.1-0 ; 
       trading_post_F-mat34.1-0 ; 
       trading_post_F-mat35.1-0 ; 
       trading_post_F-mat37.1-0 ; 
       trading_post_F-mat38.1-0 ; 
       trading_post_F-mat42.1-0 ; 
       trading_post_F-mat49.1-0 ; 
       trading_post_F-mat50.1-0 ; 
       trading_post_F-mat51.1-0 ; 
       trading_post_F-mat52.1-0 ; 
       trading_post_F-mat53.1-0 ; 
       trading_post_F-mat54.1-0 ; 
       trading_post_F-mat55.1-0 ; 
       trading_post_F-mat56.1-0 ; 
       trading_post_F-mat57.1-0 ; 
       trading_post_F-mat58.1-0 ; 
       trading_post_F-mat59.1-0 ; 
       trading_post_F-mat60.1-0 ; 
       trading_post_stL-mat1.1-0 ; 
       trading_post_stL-mat10.1-0 ; 
       trading_post_stL-mat11.1-0 ; 
       trading_post_stL-mat12.1-0 ; 
       trading_post_stL-mat13.1-0 ; 
       trading_post_stL-mat14.1-0 ; 
       trading_post_stL-mat15.1-0 ; 
       trading_post_stL-mat16.1-0 ; 
       trading_post_stL-mat17.1-0 ; 
       trading_post_stL-mat18.1-0 ; 
       trading_post_stL-mat19.1-0 ; 
       trading_post_stL-mat2.1-0 ; 
       trading_post_stL-mat20.1-0 ; 
       trading_post_stL-mat21.1-0 ; 
       trading_post_stL-mat22.1-0 ; 
       trading_post_stL-mat23.1-0 ; 
       trading_post_stL-mat24.1-0 ; 
       trading_post_stL-mat25.1-0 ; 
       trading_post_stL-mat26.1-0 ; 
       trading_post_stL-mat27.1-0 ; 
       trading_post_stL-mat28.1-0 ; 
       trading_post_stL-mat3.1-0 ; 
       trading_post_stL-mat4.1-0 ; 
       trading_post_stL-mat5.1-0 ; 
       trading_post_stL-mat6.1-0 ; 
       trading_post_stL-mat7.1-0 ; 
       trading_post_stL-mat8.1-0 ; 
       trading_post_stL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       ss25-afuselg.1-0 ; 
       ss25-bfuselg.1-0 ; 
       ss25-fuselg.1-0 ; 
       ss25-SS1.1-0 ; 
       ss25-SS10.1-0 ; 
       ss25-SS11.1-0 ; 
       ss25-SS12.1-0 ; 
       ss25-SS13.1-0 ; 
       ss25-SS14.1-0 ; 
       ss25-SS15.1-0 ; 
       ss25-SS16.1-0 ; 
       ss25-SS17.1-0 ; 
       ss25-SS18.1-0 ; 
       ss25-SS19.1-0 ; 
       ss25-SS2.1-0 ; 
       ss25-SS20.1-0 ; 
       ss25-ss25.1-0 ROOT ; 
       ss25-SS3.1-0 ; 
       ss25-SS4.1-0 ; 
       ss25-SS5.1-0 ; 
       ss25-SS6.1-0 ; 
       ss25-SS7.1-0 ; 
       ss25-SS8.1-0 ; 
       ss25-SS9.1-0 ; 
       ss25-SSb.1-0 ; 
       ss25-SSt.1-0 ; 
       ss25-tfuselg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss25/PICTURES/ss25 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss25-trading_post_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       trading_post_stL-t2d1.1-0 ; 
       trading_post_stL-t2d10.1-0 ; 
       trading_post_stL-t2d11.1-0 ; 
       trading_post_stL-t2d12.1-0 ; 
       trading_post_stL-t2d13.1-0 ; 
       trading_post_stL-t2d14.1-0 ; 
       trading_post_stL-t2d15.1-0 ; 
       trading_post_stL-t2d16.1-0 ; 
       trading_post_stL-t2d17.1-0 ; 
       trading_post_stL-t2d18.1-0 ; 
       trading_post_stL-t2d19.1-0 ; 
       trading_post_stL-t2d2.1-0 ; 
       trading_post_stL-t2d20.1-0 ; 
       trading_post_stL-t2d21.1-0 ; 
       trading_post_stL-t2d22.1-0 ; 
       trading_post_stL-t2d23.1-0 ; 
       trading_post_stL-t2d3.1-0 ; 
       trading_post_stL-t2d4.1-0 ; 
       trading_post_stL-t2d5.1-0 ; 
       trading_post_stL-t2d6.1-0 ; 
       trading_post_stL-t2d7.1-0 ; 
       trading_post_stL-t2d8.1-0 ; 
       trading_post_stL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 26 110 ; 
       4 26 110 ; 
       5 26 110 ; 
       6 26 110 ; 
       7 26 110 ; 
       8 26 110 ; 
       9 26 110 ; 
       10 26 110 ; 
       11 26 110 ; 
       12 26 110 ; 
       13 26 110 ; 
       14 26 110 ; 
       15 26 110 ; 
       17 26 110 ; 
       18 26 110 ; 
       19 26 110 ; 
       20 26 110 ; 
       21 26 110 ; 
       22 26 110 ; 
       23 26 110 ; 
       24 1 110 ; 
       25 26 110 ; 
       0 2 110 ; 
       1 26 110 ; 
       2 16 110 ; 
       26 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 8 300 ; 
       4 5 300 ; 
       5 6 300 ; 
       6 1 300 ; 
       7 3 300 ; 
       8 7 300 ; 
       9 14 300 ; 
       10 15 300 ; 
       11 16 300 ; 
       12 17 300 ; 
       13 18 300 ; 
       14 13 300 ; 
       15 21 300 ; 
       17 12 300 ; 
       18 11 300 ; 
       19 10 300 ; 
       20 9 300 ; 
       21 2 300 ; 
       22 4 300 ; 
       23 0 300 ; 
       24 19 300 ; 
       25 20 300 ; 
       0 31 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       1 22 300 ; 
       1 33 300 ; 
       1 43 300 ; 
       1 44 300 ; 
       2 45 300 ; 
       2 48 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 32 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 36 300 ; 
       2 37 300 ; 
       2 38 300 ; 
       2 41 300 ; 
       26 46 300 ; 
       26 47 300 ; 
       26 49 300 ; 
       26 23 300 ; 
       26 24 300 ; 
       26 25 300 ; 
       26 26 300 ; 
       26 42 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       16 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       24 20 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       27 1 401 ; 
       28 2 401 ; 
       29 3 401 ; 
       30 4 401 ; 
       32 5 401 ; 
       33 0 401 ; 
       34 6 401 ; 
       35 7 401 ; 
       36 8 401 ; 
       37 9 401 ; 
       38 10 401 ; 
       39 12 401 ; 
       40 13 401 ; 
       41 14 401 ; 
       42 15 401 ; 
       43 11 401 ; 
       44 16 401 ; 
       47 17 401 ; 
       48 18 401 ; 
       49 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 60 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 35 -6 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 45 -6 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 50 -6 0 MPRFLG 0 ; 
       12 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 55 -6 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 10 -6 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 15 -6 0 MPRFLG 0 ; 
       20 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 30 -6 0 MPRFLG 0 ; 
       24 SCHEM 20 -8 0 MPRFLG 0 ; 
       25 SCHEM 25 -6 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 20 -6 0 MPRFLG 0 ; 
       2 SCHEM 30 -2 0 MPRFLG 0 ; 
       16 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 31.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 59 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 12 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
