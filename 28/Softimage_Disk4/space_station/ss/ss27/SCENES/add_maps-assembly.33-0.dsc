SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       assembly-bool1_1.33-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.179-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       assembly-light1.33-0 ROOT ; 
       assembly-light2.33-0 ROOT ; 
       assembly-light3.33-0 ROOT ; 
       assembly-light4.33-0 ROOT ; 
       assembly-light5.33-0 ROOT ; 
       assembly-light6.33-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       assembly-bool1_1.19-0 ROOT ; 
       assembly-control_tower.2-0 ; 
       assembly-east_bay_antenna1.1-0 ; 
       assembly-east_bay_antenna2.1-0 ; 
       assembly-east_bay_strut.4-0 ; 
       assembly-east_bay_strut1.4-0 ; 
       assembly-east_bay_strut2.1-0 ; 
       assembly-east_bay_strut3.1-0 ; 
       assembly-east_bay1.1-0 ; 
       assembly-east_bay2.1-0 ; 
       assembly-main_antenna.1-0 ; 
       assembly-south_block.1-0 ; 
       assembly-south_block1.1-0 ; 
       assembly-south_block2.1-0 ; 
       assembly-south_hull.4-0 ; 
       assembly-south_hull1.2-0 ; 
       assembly-SSc1.1-0 ; 
       assembly-SSc2.1-0 ; 
       assembly-SSc3.1-0 ; 
       assembly-SSc4.1-0 ; 
       assembly-SSc5.1-0 ; 
       assembly-SSc6.1-0 ; 
       assembly-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       add_maps-assembly.33-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 8 110 ; 
       1 0 110 ; 
       15 0 110 ; 
       8 4 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       9 7 110 ; 
       10 1 110 ; 
       11 22 110 ; 
       12 22 110 ; 
       13 22 110 ; 
       4 5 110 ; 
       16 11 110 ; 
       17 11 110 ; 
       18 13 110 ; 
       19 13 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 0 110 ; 
       5 0 110 ; 
       3 9 110 ; 
       14 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 MPRFLG 0 ; 
       11 SCHEM 11.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 10 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 20 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 17.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 15 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 29 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
