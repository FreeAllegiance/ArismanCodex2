SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       pod_texture-bool1_1.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.122-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       pod_texture-light1.10-0 ROOT ; 
       pod_texture-light2.10-0 ROOT ; 
       pod_texture-light3.10-0 ROOT ; 
       pod_texture-light4.10-0 ROOT ; 
       pod_texture-light5.10-0 ROOT ; 
       pod_texture-light6.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       pod_texture-bay_strut_bridge.1-0 ; 
       pod_texture-bool1_1.6-0 ROOT ; 
       pod_texture-control_tower.2-0 ; 
       pod_texture-east_bay.1-0 ; 
       pod_texture-east_bay_antenna.1-0 ; 
       pod_texture-east_bay_strut.1-0 ; 
       pod_texture-east_large_sub-strut.1-0 ; 
       pod_texture-east_small_sub-strut.1-0 ; 
       pod_texture-main_antenna.1-0 ; 
       pod_texture-south_block.1-0 ; 
       pod_texture-south_block1.1-0 ; 
       pod_texture-south_block2.1-0 ; 
       pod_texture-south_hull.2-0 ; 
       pod_texture-SSc1.1-0 ; 
       pod_texture-SSc2.1-0 ; 
       pod_texture-SSc3.1-0 ; 
       pod_texture-SSc4.1-0 ; 
       pod_texture-SSc5.1-0 ; 
       pod_texture-SSc6.1-0 ; 
       pod_texture-utl28a_2.1-0 ; 
       pod_texture-west_bay.1-0 ; 
       pod_texture-west_bay_antenna.1-0 ; 
       pod_texture-west_bay_strut.1-0 ; 
       pod_texture-west_large_sub-strut_1.3-0 ; 
       pod_texture-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       add_maps-pod_texture.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 2 110 ; 
       9 19 110 ; 
       10 19 110 ; 
       11 19 110 ; 
       12 1 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 10 110 ; 
       18 10 110 ; 
       19 1 110 ; 
       20 22 110 ; 
       21 20 110 ; 
       22 0 110 ; 
       23 22 110 ; 
       24 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 36.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 25 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -2 0 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 55 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 40 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 45 -2 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 10 -6 0 MPRFLG 0 ; 
       24 SCHEM 15 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
