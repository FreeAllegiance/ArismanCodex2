SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.359-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       bounding_model-bbound1.1-0 ; 
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bounding_model.7-0 ROOT ; 
       bounding_model-midbound1.1-0 ; 
       bounding_model-midbound2.1-0 ; 
       bounding_model-midbound3.1-0 ; 
       bounding_model-midbound5.1-0 ; 
       bounding_model-midbound6.1-0 ; 
       bounding_model-tbound1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-BOUND.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
