SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.296-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 86     
       EDIT_garage_points-bool1_5_1.2-0 ; 
       EDIT_garage_points-control_tower.2-0 ; 
       EDIT_garage_points-east_bay_11.1-0 ; 
       EDIT_garage_points-east_bay_7.7-0 ; 
       EDIT_garage_points-east_bay_antenna.1-0 ; 
       EDIT_garage_points-east_bay_antenna1.1-0 ; 
       EDIT_garage_points-east_bay_strut_1.1-0 ; 
       EDIT_garage_points-east_bay_strut_2.1-0 ; 
       EDIT_garage_points-east_bay_strut1.1-0 ; 
       EDIT_garage_points-east_bay_strut2.1-0 ; 
       EDIT_garage_points-garage1A.1-0 ; 
       EDIT_garage_points-garage1B.1-0 ; 
       EDIT_garage_points-garage1C.1-0 ; 
       EDIT_garage_points-garage1D.1-0 ; 
       EDIT_garage_points-garage1E.1-0 ; 
       EDIT_garage_points-garage2A.1-0 ; 
       EDIT_garage_points-garage2B.1-0 ; 
       EDIT_garage_points-garage2C.1-0 ; 
       EDIT_garage_points-garage2D.1-0 ; 
       EDIT_garage_points-garage2E.1-0 ; 
       EDIT_garage_points-landing_lights.1-0 ; 
       EDIT_garage_points-landing_lights1.1-0 ; 
       EDIT_garage_points-landing_lights2.1-0 ; 
       EDIT_garage_points-landing_lights3.1-0 ; 
       EDIT_garage_points-landing_lights4.1-0 ; 
       EDIT_garage_points-landing_lights5.1-0 ; 
       EDIT_garage_points-launch1.1-0 ; 
       EDIT_garage_points-launch2.1-0 ; 
       EDIT_garage_points-main_antenna.1-0 ; 
       EDIT_garage_points-root.7-0 ROOT ; 
       EDIT_garage_points-south_block.1-0 ; 
       EDIT_garage_points-south_block5.1-0 ; 
       EDIT_garage_points-south_block6.1-0 ; 
       EDIT_garage_points-south_hull_1.3-0 ; 
       EDIT_garage_points-south_hull_2.1-0 ; 
       EDIT_garage_points-SS_01.1-0 ; 
       EDIT_garage_points-SS_10.1-0 ; 
       EDIT_garage_points-SS_11.1-0 ; 
       EDIT_garage_points-SS_12.1-0 ; 
       EDIT_garage_points-SS_13.1-0 ; 
       EDIT_garage_points-SS_14.1-0 ; 
       EDIT_garage_points-SS_15.1-0 ; 
       EDIT_garage_points-SS_16.1-0 ; 
       EDIT_garage_points-SS_17.1-0 ; 
       EDIT_garage_points-SS_18.1-0 ; 
       EDIT_garage_points-SS_19.1-0 ; 
       EDIT_garage_points-SS_2.1-0 ; 
       EDIT_garage_points-SS_20.1-0 ; 
       EDIT_garage_points-SS_21.1-0 ; 
       EDIT_garage_points-SS_22.1-0 ; 
       EDIT_garage_points-SS_23.1-0 ; 
       EDIT_garage_points-SS_24.1-0 ; 
       EDIT_garage_points-SS_25.1-0 ; 
       EDIT_garage_points-SS_26.1-0 ; 
       EDIT_garage_points-SS_27.1-0 ; 
       EDIT_garage_points-SS_28.1-0 ; 
       EDIT_garage_points-SS_29.1-0 ; 
       EDIT_garage_points-SS_3.1-0 ; 
       EDIT_garage_points-SS_30.1-0 ; 
       EDIT_garage_points-SS_31.1-0 ; 
       EDIT_garage_points-SS_32.1-0 ; 
       EDIT_garage_points-SS_33.1-0 ; 
       EDIT_garage_points-SS_34.1-0 ; 
       EDIT_garage_points-SS_35.1-0 ; 
       EDIT_garage_points-SS_36.1-0 ; 
       EDIT_garage_points-SS_37.1-0 ; 
       EDIT_garage_points-SS_38.1-0 ; 
       EDIT_garage_points-SS_39.1-0 ; 
       EDIT_garage_points-SS_40.1-0 ; 
       EDIT_garage_points-SS_41.1-0 ; 
       EDIT_garage_points-SS_42.1-0 ; 
       EDIT_garage_points-SS_43.1-0 ; 
       EDIT_garage_points-SS_44.1-0 ; 
       EDIT_garage_points-SS_45.1-0 ; 
       EDIT_garage_points-SS_46.1-0 ; 
       EDIT_garage_points-SS_6.1-0 ; 
       EDIT_garage_points-SS_7.1-0 ; 
       EDIT_garage_points-SS_8.1-0 ; 
       EDIT_garage_points-SS_9.1-0 ; 
       EDIT_garage_points-strobe_set.1-0 ; 
       EDIT_garage_points-strobe_set1.1-0 ; 
       EDIT_garage_points-turwepemt1.1-0 ; 
       EDIT_garage_points-turwepemt2.1-0 ; 
       EDIT_garage_points-turwepemt3.1-0 ; 
       EDIT_garage_points-turwepemt4.1-0 ; 
       EDIT_garage_points-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-EDIT_garage_points.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 29 110 ; 
       1 0 110 ; 
       2 6 110 ; 
       3 7 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 8 110 ; 
       7 9 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 79 110 ; 
       21 79 110 ; 
       22 79 110 ; 
       23 80 110 ; 
       24 80 110 ; 
       25 80 110 ; 
       26 3 110 ; 
       27 2 110 ; 
       28 1 110 ; 
       30 85 110 ; 
       31 85 110 ; 
       32 85 110 ; 
       33 0 110 ; 
       34 0 110 ; 
       35 30 110 ; 
       36 4 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 20 110 ; 
       42 20 110 ; 
       43 21 110 ; 
       44 21 110 ; 
       45 21 110 ; 
       46 31 110 ; 
       47 21 110 ; 
       48 21 110 ; 
       49 21 110 ; 
       50 22 110 ; 
       51 22 110 ; 
       52 22 110 ; 
       53 22 110 ; 
       54 22 110 ; 
       55 22 110 ; 
       56 23 110 ; 
       57 32 110 ; 
       58 23 110 ; 
       59 23 110 ; 
       60 23 110 ; 
       61 23 110 ; 
       62 23 110 ; 
       63 24 110 ; 
       64 24 110 ; 
       65 24 110 ; 
       66 24 110 ; 
       67 24 110 ; 
       68 24 110 ; 
       69 25 110 ; 
       70 25 110 ; 
       71 25 110 ; 
       72 25 110 ; 
       73 25 110 ; 
       74 25 110 ; 
       75 28 110 ; 
       76 33 110 ; 
       77 34 110 ; 
       78 5 110 ; 
       79 2 110 ; 
       80 3 110 ; 
       81 34 110 ; 
       82 2 110 ; 
       83 3 110 ; 
       84 33 110 ; 
       85 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 76.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 137.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 103.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 103.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 103.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 57.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 60 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 65 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 122.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 125 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 127.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 132.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 130 -10 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       20 SCHEM 83.75 -12 0 MPRFLG 0 ; 
       21 SCHEM 98.75 -12 0 MPRFLG 0 ; 
       22 SCHEM 113.75 -12 0 MPRFLG 0 ; 
       23 SCHEM 33.75 -12 0 MPRFLG 0 ; 
       24 SCHEM 18.75 -12 0 MPRFLG 0 ; 
       25 SCHEM 48.75 -12 0 MPRFLG 0 ; 
       26 SCHEM 70 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 135 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 137.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 76.25 0 0 SRT 1 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 140 -6 0 MPRFLG 0 ; 
       31 SCHEM 142.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 145 -6 0 MPRFLG 0 ; 
       33 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       34 SCHEM 148.75 -4 0 MPRFLG 0 ; 
       35 SCHEM 140 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 7.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 90 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 87.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 77.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 80 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 85 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 105 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 92.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 95 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 142.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 97.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 100 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 102.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 120 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 107.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 110 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 112.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 115 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 117.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 40 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 145 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 27.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 30 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 32.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 35 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 37.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 25 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 12.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 15 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 17.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 20 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 22.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 55 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 42.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       71 SCHEM 45 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       72 SCHEM 47.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       73 SCHEM 50 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 52.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 137.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 150 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 72.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 98.75 -10 0 MPRFLG 0 ; 
       80 SCHEM 33.75 -10 0 MPRFLG 0 ; 
       81 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 142.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
