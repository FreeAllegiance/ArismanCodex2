SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       new_lighting-bool1_5.26-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.231-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 8     
       new_lighting-inf_light1_5.26-0 ROOT ; 
       new_lighting-inf_light2_5.26-0 ROOT ; 
       new_lighting-spot11.1-0 ; 
       new_lighting-spot11_int.4-0 ROOT ; 
       new_lighting-spot12.1-0 ; 
       new_lighting-spot12_int.2-0 ROOT ; 
       new_lighting-spot13.1-0 ; 
       new_lighting-spot13_int.22-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       new_lighting-bay_strut_bridge.1-0 ; 
       new_lighting-bool1_5.18-0 ROOT ; 
       new_lighting-control_tower.2-0 ; 
       new_lighting-east_bay.1-0 ; 
       new_lighting-east_bay_antenna.1-0 ; 
       new_lighting-east_bay_strut.1-0 ; 
       new_lighting-east_bay_strut1.1-0 ROOT ; 
       new_lighting-east_large_sub-strut.1-0 ; 
       new_lighting-main_antenna.1-0 ; 
       new_lighting-south_block.1-0 ; 
       new_lighting-south_block1.1-0 ; 
       new_lighting-south_block2.1-0 ; 
       new_lighting-south_hull.2-0 ; 
       new_lighting-SSc1.1-0 ; 
       new_lighting-SSc2.1-0 ; 
       new_lighting-SSc3.1-0 ; 
       new_lighting-SSc4.1-0 ; 
       new_lighting-SSc5.1-0 ; 
       new_lighting-SSc6.1-0 ; 
       new_lighting-utl28a_2.1-0 ; 
       new_lighting-west_bay.1-0 ; 
       new_lighting-west_bay_antenna.1-0 ; 
       new_lighting-west_bay_strut.1-0 ROOT ; 
       new_lighting-west_large_sub-strut_1.3-0 ; 
       new_lighting-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 15     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/black ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/circuits ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/clay_panel ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/gantry ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/lights ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/logo ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/numbers ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/rendermap1 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/round node ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/side ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/sidelights ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/single_light ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/solar ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/text ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-new_lighting.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 7 110 ; 
       4 3 110 ; 
       5 6 110 ; 
       7 5 110 ; 
       8 2 110 ; 
       9 19 110 ; 
       10 19 110 ; 
       11 19 110 ; 
       12 1 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 10 110 ; 
       18 10 110 ; 
       19 1 110 ; 
       20 22 110 ; 
       21 20 110 ; 
       23 22 110 ; 
       24 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
       4 5 2110 ; 
       6 7 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 7.5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 10 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 15 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 15 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 0 -34 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 0 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 47.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 50 -6 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -20 0 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -16 0 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 25 -14 0 SRT 1 1 0.9999999 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -18 0 MPRFLG 0 ; 
       8 SCHEM 48.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 58.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 71.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 83.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 USR MPRFLG 0 ; 
       13 SCHEM 56.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 53.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 81.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 78.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 68.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 66.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 71.25 -6 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -28 0 MPRFLG 0 ; 
       21 SCHEM 2.5 -30 0 MPRFLG 0 ; 
       22 SCHEM 2.5 -26 0 DISPLAY 0 0 SRT 1 1 0.9999999 0 0 0 0 0 0 MPRFLG 0 ; 
       23 SCHEM 0 -28 0 MPRFLG 0 ; 
       24 SCHEM 5 -28 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 90.25 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
