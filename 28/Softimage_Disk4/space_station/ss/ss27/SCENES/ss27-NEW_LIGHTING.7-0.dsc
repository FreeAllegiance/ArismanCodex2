SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       NEW_LIGHTING-bool1.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.192-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       NEW_LIGHTING-inf_light1.7-0 ROOT ; 
       NEW_LIGHTING-inf_light2.7-0 ROOT ; 
       NEW_LIGHTING-spot11.1-0 ; 
       NEW_LIGHTING-spot11_int.7-0 ROOT ; 
       NEW_LIGHTING-spot12.1-0 ; 
       NEW_LIGHTING-spot12_int.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       NEW_LIGHTING-bay_strut_bridge.1-0 ; 
       NEW_LIGHTING-bool1.6-0 ROOT ; 
       NEW_LIGHTING-control_tower.2-0 ; 
       NEW_LIGHTING-east_bay.2-0 ROOT ; 
       NEW_LIGHTING-east_bay_antenna.1-0 ; 
       NEW_LIGHTING-east_bay_strut.1-0 ROOT ; 
       NEW_LIGHTING-east_bay_strut1.1-0 ROOT ; 
       NEW_LIGHTING-east_large_sub-strut.1-0 ROOT ; 
       NEW_LIGHTING-east_small_sub-strut.1-0 ROOT ; 
       NEW_LIGHTING-main_antenna.1-0 ; 
       NEW_LIGHTING-south_block.1-0 ; 
       NEW_LIGHTING-south_block1.1-0 ; 
       NEW_LIGHTING-south_block2.1-0 ; 
       NEW_LIGHTING-south_hull.2-0 ; 
       NEW_LIGHTING-SSc1.1-0 ; 
       NEW_LIGHTING-SSc2.1-0 ; 
       NEW_LIGHTING-SSc3.1-0 ; 
       NEW_LIGHTING-SSc4.1-0 ; 
       NEW_LIGHTING-SSc5.1-0 ; 
       NEW_LIGHTING-SSc6.1-0 ; 
       NEW_LIGHTING-utl28a_2.1-0 ; 
       NEW_LIGHTING-west_bay.1-0 ; 
       NEW_LIGHTING-west_bay_antenna.1-0 ; 
       NEW_LIGHTING-west_bay_strut.1-0 ROOT ; 
       NEW_LIGHTING-west_large_sub-strut_1.3-0 ; 
       NEW_LIGHTING-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 12     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/black ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/gantry ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/landing-area ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/lights ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/logo ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/rendermap1 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/side ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/sidelights ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/single_light ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/solar ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/text ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-NEW_LIGHTING.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       4 3 110 ; 
       9 2 110 ; 
       10 20 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       13 1 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 1 110 ; 
       21 23 110 ; 
       22 21 110 ; 
       24 23 110 ; 
       25 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
       4 5 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 0 -38 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -40 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -38 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 5 -40 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 5 -38 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -38 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 33.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -14 0 SRT 1 1 1 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 0 -16 0 MPRFLG 0 ; 
       5 SCHEM 5 -20 0 SRT 1 1 1 0 3.141593 0.06 12.00735 2.931329 0 MPRFLG 0 ; 
       6 SCHEM 5 -26 0 SRT 1 1 0.9999999 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 0 -32 0 SRT 0.9 0.9 0.5759999 0 -3.141593 0 13.1291 2.127311 -1.147782e-006 MPRFLG 0 ; 
       8 SCHEM 2.5 -32 0 SRT 0.3942001 0.3942001 0.2522879 0 -3.141593 0 20.47204 1.579811 -1.789722e-006 MPRFLG 0 ; 
       9 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       10 SCHEM 38.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 51.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 63.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.25 -6 0 USR MPRFLG 0 ; 
       14 SCHEM 36.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 33.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 61.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 58.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 48.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 46.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 7.5 -34 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -36 0 MPRFLG 0 ; 
       23 SCHEM 7.5 -32 0 DISPLAY 0 0 SRT 1 1 0.9999999 0 0 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 5 -34 0 MPRFLG 0 ; 
       25 SCHEM 10 -34 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 70.25 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
