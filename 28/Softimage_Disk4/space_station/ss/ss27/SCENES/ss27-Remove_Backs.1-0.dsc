SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.280-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 74     
       ss27-bool1_5_1.2-0 ; 
       ss27-control_tower.2-0 ; 
       ss27-east_bay_11.1-0 ; 
       ss27-east_bay_7.7-0 ; 
       ss27-east_bay_antenna.1-0 ; 
       ss27-east_bay_antenna1.1-0 ; 
       ss27-east_bay_strut_1.1-0 ; 
       ss27-east_bay_strut_2.1-0 ; 
       ss27-east_bay_strut1.1-0 ; 
       ss27-east_bay_strut2.1-0 ; 
       ss27-landing_lights.1-0 ; 
       ss27-landing_lights1.1-0 ; 
       ss27-landing_lights2.1-0 ; 
       ss27-landing_lights3.1-0 ; 
       ss27-landing_lights4.1-0 ; 
       ss27-landing_lights5.1-0 ; 
       ss27-main_antenna.1-0 ; 
       ss27-root.1-0 ROOT ; 
       ss27-south_block.1-0 ; 
       ss27-south_block5.1-0 ; 
       ss27-south_block6.1-0 ; 
       ss27-south_hull_1.3-0 ; 
       ss27-south_hull_2.1-0 ; 
       ss27-SS_01.1-0 ; 
       ss27-SS_10.1-0 ; 
       ss27-SS_11.1-0 ; 
       ss27-SS_12.1-0 ; 
       ss27-SS_13.1-0 ; 
       ss27-SS_14.1-0 ; 
       ss27-SS_15.1-0 ; 
       ss27-SS_16.1-0 ; 
       ss27-SS_17.1-0 ; 
       ss27-SS_18.1-0 ; 
       ss27-SS_19.1-0 ; 
       ss27-SS_2.1-0 ; 
       ss27-SS_20.1-0 ; 
       ss27-SS_21.1-0 ; 
       ss27-SS_22.1-0 ; 
       ss27-SS_23.1-0 ; 
       ss27-SS_24.1-0 ; 
       ss27-SS_25.1-0 ; 
       ss27-SS_26.1-0 ; 
       ss27-SS_27.1-0 ; 
       ss27-SS_28.1-0 ; 
       ss27-SS_29.1-0 ; 
       ss27-SS_3.1-0 ; 
       ss27-SS_30.1-0 ; 
       ss27-SS_31.1-0 ; 
       ss27-SS_32.1-0 ; 
       ss27-SS_33.1-0 ; 
       ss27-SS_34.1-0 ; 
       ss27-SS_35.1-0 ; 
       ss27-SS_36.1-0 ; 
       ss27-SS_37.1-0 ; 
       ss27-SS_38.1-0 ; 
       ss27-SS_39.1-0 ; 
       ss27-SS_40.1-0 ; 
       ss27-SS_41.1-0 ; 
       ss27-SS_42.1-0 ; 
       ss27-SS_43.1-0 ; 
       ss27-SS_44.1-0 ; 
       ss27-SS_45.1-0 ; 
       ss27-SS_46.1-0 ; 
       ss27-SS_6.1-0 ; 
       ss27-SS_7.1-0 ; 
       ss27-SS_8.1-0 ; 
       ss27-SS_9.1-0 ; 
       ss27-strobe_set.1-0 ; 
       ss27-strobe_set1.1-0 ; 
       ss27-turwepemt1.1-0 ; 
       ss27-turwepemt2.1-0 ; 
       ss27-turwepemt3.1-0 ; 
       ss27-turwepemt4.1-0 ; 
       ss27-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-Remove_Backs.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       1 0 110 ; 
       2 6 110 ; 
       3 7 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 8 110 ; 
       7 9 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 67 110 ; 
       11 67 110 ; 
       12 67 110 ; 
       13 68 110 ; 
       14 68 110 ; 
       15 68 110 ; 
       16 1 110 ; 
       18 73 110 ; 
       19 73 110 ; 
       20 73 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 18 110 ; 
       24 4 110 ; 
       25 10 110 ; 
       26 10 110 ; 
       27 10 110 ; 
       28 10 110 ; 
       29 10 110 ; 
       30 10 110 ; 
       31 11 110 ; 
       32 11 110 ; 
       33 11 110 ; 
       34 19 110 ; 
       35 11 110 ; 
       36 11 110 ; 
       37 11 110 ; 
       38 12 110 ; 
       39 12 110 ; 
       40 12 110 ; 
       41 12 110 ; 
       42 12 110 ; 
       43 12 110 ; 
       44 13 110 ; 
       45 20 110 ; 
       46 13 110 ; 
       47 13 110 ; 
       48 13 110 ; 
       49 13 110 ; 
       50 13 110 ; 
       51 14 110 ; 
       52 14 110 ; 
       53 14 110 ; 
       54 14 110 ; 
       55 14 110 ; 
       56 14 110 ; 
       57 15 110 ; 
       58 15 110 ; 
       59 15 110 ; 
       60 15 110 ; 
       61 15 110 ; 
       62 15 110 ; 
       63 16 110 ; 
       64 21 110 ; 
       65 22 110 ; 
       66 5 110 ; 
       67 2 110 ; 
       68 3 110 ; 
       69 22 110 ; 
       70 2 110 ; 
       71 3 110 ; 
       72 21 110 ; 
       73 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 656.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 702.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 676.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 626.25 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 602.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 652.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 676.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 626.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 676.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 626.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 663.75 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 678.75 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 693.75 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 628.75 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 613.75 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 643.75 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 702.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 656.25 0 0 DISPLAY 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 705 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 707.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 710 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 598.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 713.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 705 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 602.5 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 670 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 667.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 657.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 660 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 662.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 665 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 685 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 672.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 675 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 707.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 677.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 680 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 682.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 700 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 687.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 690 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 692.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 695 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 697.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 635 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 710 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 622.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 625 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 627.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 630 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 632.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 620 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 607.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 610 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 612.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 615 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 617.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 650 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 637.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 640 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 642.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 645 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 647.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 702.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 600 -6 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 715 -6 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 652.5 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 678.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 628.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 712.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 655 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 605 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 597.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 707.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
