SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_decals__od-bool1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.98-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 19     
       add_decals__od-light1.1-0 ROOT ; 
       add_decals__od-light10.1-0 ROOT ; 
       add_decals__od-light2.1-0 ROOT ; 
       add_decals__od-light3.1-0 ROOT ; 
       add_decals__od-light4.1-0 ROOT ; 
       add_decals__od-light5.1-0 ROOT ; 
       add_decals__od-light6.1-0 ROOT ; 
       add_decals__od-light8.1-0 ROOT ; 
       add_decals__od-light9.1-0 ROOT ; 
       add_decals__od-spot1.1-0 ; 
       add_decals__od-spot1_int.1-0 ROOT ; 
       add_decals__od-spot2.1-0 ; 
       add_decals__od-spot2_int.1-0 ROOT ; 
       add_decals__od-spot3.1-0 ; 
       add_decals__od-spot3_int.1-0 ROOT ; 
       add_decals__od-spot4.1-0 ; 
       add_decals__od-spot4_int.1-0 ROOT ; 
       add_decals__od-spot5.1-0 ; 
       add_decals__od-spot5_int.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       add_decals__od-bay_strut_bridge.1-0 ; 
       add_decals__od-bool1.1-0 ROOT ; 
       add_decals__od-control_tower.2-0 ; 
       add_decals__od-east_bay.1-0 ; 
       add_decals__od-east_bay_antenna.1-0 ; 
       add_decals__od-east_bay_strut.1-0 ; 
       add_decals__od-east_large_sub-strut.1-0 ; 
       add_decals__od-east_small_sub-strut.1-0 ; 
       add_decals__od-main_antenna.1-0 ; 
       add_decals__od-north_hull.1-0 ; 
       add_decals__od-south_block.1-0 ; 
       add_decals__od-south_block1.1-0 ; 
       add_decals__od-south_block2.1-0 ; 
       add_decals__od-south_hull.2-0 ; 
       add_decals__od-SSc1.1-0 ; 
       add_decals__od-SSc2.1-0 ; 
       add_decals__od-SSc3.1-0 ; 
       add_decals__od-SSc4.1-0 ; 
       add_decals__od-SSc5.1-0 ; 
       add_decals__od-SSc6.1-0 ; 
       add_decals__od-utl28a_2.1-0 ; 
       add_decals__od-west_bay.1-0 ; 
       add_decals__od-west_bay_antenna.1-0 ; 
       add_decals__od-west_bay_strut.1-0 ; 
       add_decals__od-west_large_sub-strut_1.3-0 ; 
       add_decals__od-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/rendermap1 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/side ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-add_decals__od.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 20 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       13 1 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 1 110 ; 
       21 23 110 ; 
       22 21 110 ; 
       23 0 110 ; 
       24 23 110 ; 
       25 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       1 13 2111 ; 
       7 9 2111 ; 
       8 11 2111 ; 
       9 10 2110 ; 
       11 12 2110 ; 
       13 14 2110 ; 
       15 16 2110 ; 
       17 18 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 0 -16 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -16 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 5 -16 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 7.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 2.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 32.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 0 -10 0 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 MPRFLG 0 ; 
       10 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.25 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 32.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 30 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 55 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 45 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 42.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 10 -10 0 MPRFLG 0 ; 
       22 SCHEM 10 -12 0 MPRFLG 0 ; 
       23 SCHEM 10 -8 0 MPRFLG 0 ; 
       24 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 66.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
