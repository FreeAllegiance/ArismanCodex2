SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_decals_pod-bool1.9-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.110-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 20     
       add_decals_pod-light1.9-0 ROOT ; 
       add_decals_pod-light10.9-0 ROOT ; 
       add_decals_pod-light11.1-0 ROOT ; 
       add_decals_pod-light2.9-0 ROOT ; 
       add_decals_pod-light3.9-0 ROOT ; 
       add_decals_pod-light4.9-0 ROOT ; 
       add_decals_pod-light5.9-0 ROOT ; 
       add_decals_pod-light6.9-0 ROOT ; 
       add_decals_pod-light8.9-0 ROOT ; 
       add_decals_pod-light9.9-0 ROOT ; 
       add_decals_pod-spot1.1-0 ; 
       add_decals_pod-spot1_int.9-0 ROOT ; 
       add_decals_pod-spot2.1-0 ; 
       add_decals_pod-spot2_int.9-0 ROOT ; 
       add_decals_pod-spot3.1-0 ; 
       add_decals_pod-spot3_int.9-0 ROOT ; 
       add_decals_pod-spot4.1-0 ; 
       add_decals_pod-spot4_int.9-0 ROOT ; 
       add_decals_pod-spot5.1-0 ; 
       add_decals_pod-spot5_int.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       add_decals_pod-bay_strut_bridge.1-0 ; 
       add_decals_pod-bool1.8-0 ROOT ; 
       add_decals_pod-control_tower.2-0 ; 
       add_decals_pod-east_bay.1-0 ; 
       add_decals_pod-east_bay_antenna.1-0 ; 
       add_decals_pod-east_bay_strut.1-0 ; 
       add_decals_pod-east_large_sub-strut.1-0 ; 
       add_decals_pod-east_small_sub-strut.1-0 ; 
       add_decals_pod-main_antenna.1-0 ; 
       add_decals_pod-north_hull.1-0 ; 
       add_decals_pod-south_block.1-0 ; 
       add_decals_pod-south_block1.1-0 ; 
       add_decals_pod-south_block2.1-0 ; 
       add_decals_pod-south_hull.2-0 ; 
       add_decals_pod-SSc1.1-0 ; 
       add_decals_pod-SSc2.1-0 ; 
       add_decals_pod-SSc3.1-0 ; 
       add_decals_pod-SSc4.1-0 ; 
       add_decals_pod-SSc5.1-0 ; 
       add_decals_pod-SSc6.1-0 ; 
       add_decals_pod-utl28a_2.1-0 ; 
       add_decals_pod-west_bay.1-0 ; 
       add_decals_pod-west_bay_antenna.1-0 ; 
       add_decals_pod-west_bay_strut.1-0 ; 
       add_decals_pod-west_large_sub-strut_1.3-0 ; 
       add_decals_pod-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/lights ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/rendermap1 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/side ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/single_light ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/solar ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/text ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-add_decals_pod.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 20 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       13 1 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 1 110 ; 
       21 23 110 ; 
       22 21 110 ; 
       23 0 110 ; 
       24 23 110 ; 
       25 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       1 14 2111 ; 
       8 10 2111 ; 
       9 12 2111 ; 
       10 11 2110 ; 
       12 13 2110 ; 
       14 15 2110 ; 
       16 17 2110 ; 
       18 19 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 122.5 0 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 77.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 80 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 82.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 85 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 87.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 90 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 92.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 95 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 95 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 97.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 97.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 100 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 102.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 105 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 36.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -2 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 35 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 60 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 50 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 10 -6 0 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
