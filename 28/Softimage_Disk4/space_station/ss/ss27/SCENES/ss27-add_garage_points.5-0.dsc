SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.285-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 84     
       add_garage_points-BL1.1-0 ; 
       add_garage_points-BL2.1-0 ; 
       add_garage_points-bool1_5_1.2-0 ; 
       add_garage_points-BR1.1-0 ; 
       add_garage_points-BR2.1-0 ; 
       add_garage_points-control_tower.2-0 ; 
       add_garage_points-east_bay_11.1-0 ; 
       add_garage_points-east_bay_7.7-0 ; 
       add_garage_points-east_bay_antenna.1-0 ; 
       add_garage_points-east_bay_antenna1.1-0 ; 
       add_garage_points-east_bay_strut_1.1-0 ; 
       add_garage_points-east_bay_strut_2.1-0 ; 
       add_garage_points-east_bay_strut1.1-0 ; 
       add_garage_points-east_bay_strut2.1-0 ; 
       add_garage_points-landing_lights.1-0 ; 
       add_garage_points-landing_lights1.1-0 ; 
       add_garage_points-landing_lights2.1-0 ; 
       add_garage_points-landing_lights3.1-0 ; 
       add_garage_points-landing_lights4.1-0 ; 
       add_garage_points-landing_lights5.1-0 ; 
       add_garage_points-LP1.1-0 ; 
       add_garage_points-LP2.1-0 ; 
       add_garage_points-main_antenna.1-0 ; 
       add_garage_points-root.3-0 ROOT ; 
       add_garage_points-south_block.1-0 ; 
       add_garage_points-south_block5.1-0 ; 
       add_garage_points-south_block6.1-0 ; 
       add_garage_points-south_hull_1.3-0 ; 
       add_garage_points-south_hull_2.1-0 ; 
       add_garage_points-SS_01.1-0 ; 
       add_garage_points-SS_10.1-0 ; 
       add_garage_points-SS_11.1-0 ; 
       add_garage_points-SS_12.1-0 ; 
       add_garage_points-SS_13.1-0 ; 
       add_garage_points-SS_14.1-0 ; 
       add_garage_points-SS_15.1-0 ; 
       add_garage_points-SS_16.1-0 ; 
       add_garage_points-SS_17.1-0 ; 
       add_garage_points-SS_18.1-0 ; 
       add_garage_points-SS_19.1-0 ; 
       add_garage_points-SS_2.1-0 ; 
       add_garage_points-SS_20.1-0 ; 
       add_garage_points-SS_21.1-0 ; 
       add_garage_points-SS_22.1-0 ; 
       add_garage_points-SS_23.1-0 ; 
       add_garage_points-SS_24.1-0 ; 
       add_garage_points-SS_25.1-0 ; 
       add_garage_points-SS_26.1-0 ; 
       add_garage_points-SS_27.1-0 ; 
       add_garage_points-SS_28.1-0 ; 
       add_garage_points-SS_29.1-0 ; 
       add_garage_points-SS_3.1-0 ; 
       add_garage_points-SS_30.1-0 ; 
       add_garage_points-SS_31.1-0 ; 
       add_garage_points-SS_32.1-0 ; 
       add_garage_points-SS_33.1-0 ; 
       add_garage_points-SS_34.1-0 ; 
       add_garage_points-SS_35.1-0 ; 
       add_garage_points-SS_36.1-0 ; 
       add_garage_points-SS_37.1-0 ; 
       add_garage_points-SS_38.1-0 ; 
       add_garage_points-SS_39.1-0 ; 
       add_garage_points-SS_40.1-0 ; 
       add_garage_points-SS_41.1-0 ; 
       add_garage_points-SS_42.1-0 ; 
       add_garage_points-SS_43.1-0 ; 
       add_garage_points-SS_44.1-0 ; 
       add_garage_points-SS_45.1-0 ; 
       add_garage_points-SS_46.1-0 ; 
       add_garage_points-SS_6.1-0 ; 
       add_garage_points-SS_7.1-0 ; 
       add_garage_points-SS_8.1-0 ; 
       add_garage_points-SS_9.1-0 ; 
       add_garage_points-strobe_set.1-0 ; 
       add_garage_points-strobe_set1.1-0 ; 
       add_garage_points-TL1.1-0 ; 
       add_garage_points-TL2.1-0 ; 
       add_garage_points-TR1.1-0 ; 
       add_garage_points-TR2.1-0 ; 
       add_garage_points-turwepemt1.1-0 ; 
       add_garage_points-turwepemt2.1-0 ; 
       add_garage_points-turwepemt3.1-0 ; 
       add_garage_points-turwepemt4.1-0 ; 
       add_garage_points-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-add_garage_points.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 7 110 ; 
       77 7 110 ; 
       0 7 110 ; 
       75 7 110 ; 
       20 7 110 ; 
       76 6 110 ; 
       4 6 110 ; 
       1 6 110 ; 
       78 6 110 ; 
       21 6 110 ; 
       2 23 110 ; 
       5 2 110 ; 
       6 10 110 ; 
       7 11 110 ; 
       8 7 110 ; 
       9 6 110 ; 
       10 12 110 ; 
       11 13 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 73 110 ; 
       15 73 110 ; 
       16 73 110 ; 
       17 74 110 ; 
       18 74 110 ; 
       19 74 110 ; 
       22 5 110 ; 
       24 83 110 ; 
       25 83 110 ; 
       26 83 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 24 110 ; 
       30 8 110 ; 
       31 14 110 ; 
       32 14 110 ; 
       33 14 110 ; 
       34 14 110 ; 
       35 14 110 ; 
       36 14 110 ; 
       37 15 110 ; 
       38 15 110 ; 
       39 15 110 ; 
       40 25 110 ; 
       41 15 110 ; 
       42 15 110 ; 
       43 15 110 ; 
       44 16 110 ; 
       45 16 110 ; 
       46 16 110 ; 
       47 16 110 ; 
       48 16 110 ; 
       49 16 110 ; 
       50 17 110 ; 
       51 26 110 ; 
       52 17 110 ; 
       53 17 110 ; 
       54 17 110 ; 
       55 17 110 ; 
       56 17 110 ; 
       57 18 110 ; 
       58 18 110 ; 
       59 18 110 ; 
       60 18 110 ; 
       61 18 110 ; 
       62 18 110 ; 
       63 19 110 ; 
       64 19 110 ; 
       65 19 110 ; 
       66 19 110 ; 
       67 19 110 ; 
       68 19 110 ; 
       69 22 110 ; 
       70 27 110 ; 
       71 28 110 ; 
       72 9 110 ; 
       73 6 110 ; 
       74 7 110 ; 
       79 28 110 ; 
       80 6 110 ; 
       81 7 110 ; 
       82 27 110 ; 
       83 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 70 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       77 SCHEM 67.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       0 SCHEM 72.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       75 SCHEM 75 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 77.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       76 SCHEM 152.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 155 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       1 SCHEM 157.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       78 SCHEM 160 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 162.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       2 SCHEM 126.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 188.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 136.25 -8 0 MPRFLG 0 ; 
       7 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       8 SCHEM 16.25 -10 0 MPRFLG 0 ; 
       9 SCHEM 101.25 -10 0 MPRFLG 0 ; 
       10 SCHEM 138.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 141.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 113.75 -12 0 MPRFLG 0 ; 
       15 SCHEM 128.75 -12 0 MPRFLG 0 ; 
       16 SCHEM 143.75 -12 0 MPRFLG 0 ; 
       17 SCHEM 43.75 -12 0 MPRFLG 0 ; 
       18 SCHEM 28.75 -12 0 MPRFLG 0 ; 
       19 SCHEM 58.75 -12 0 MPRFLG 0 ; 
       22 SCHEM 186.25 -6 0 MPRFLG 0 ; 
       23 SCHEM 126.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 200 -6 0 MPRFLG 0 ; 
       25 SCHEM 212.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 225 -6 0 MPRFLG 0 ; 
       27 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 237.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 195 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 120 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 117.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 107.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 110 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 112.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 115 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 135 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 122.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 125 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 207.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 127.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 130 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 132.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 150 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 137.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 140 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 142.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 145 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 147.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 50 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 220 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 37.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 40 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 42.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 45 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 47.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 35 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 22.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 25 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 27.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 30 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 32.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 65 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 52.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 55 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 57.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 60 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 62.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 185 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       71 SCHEM 235 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       72 SCHEM 100 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       73 SCHEM 128.75 -10 0 MPRFLG 0 ; 
       74 SCHEM 43.75 -10 0 MPRFLG 0 ; 
       79 SCHEM 232.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 105 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 212.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
