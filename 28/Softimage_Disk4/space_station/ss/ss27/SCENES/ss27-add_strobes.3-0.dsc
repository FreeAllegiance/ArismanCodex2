SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.243-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       add_strobes-bool1_5_1.1-0 ROOT ; 
       add_strobes-control_tower.2-0 ; 
       add_strobes-east_bay_7.7-0 ; 
       add_strobes-east_bay_9.1-0 ; 
       add_strobes-east_bay_antenna.1-0 ; 
       add_strobes-east_bay_antenna1.1-0 ; 
       add_strobes-east_bay_strut.1-0 ; 
       add_strobes-east_bay_strut_1.1-0 ; 
       add_strobes-east_bay_strut1.1-0 ; 
       add_strobes-east_bay_strut1_1.3-0 ; 
       add_strobes-main_antenna.1-0 ; 
       add_strobes-south_block.1-0 ; 
       add_strobes-south_block5.1-0 ; 
       add_strobes-south_block6.1-0 ; 
       add_strobes-south_hull_1.3-0 ; 
       add_strobes-south_hull_2.1-0 ; 
       add_strobes-SS_01.1-0 ; 
       add_strobes-SS_10.1-0 ; 
       add_strobes-SS_2.1-0 ; 
       add_strobes-SS_3.1-0 ; 
       add_strobes-SS_6.1-0 ; 
       add_strobes-SS_7.1-0 ; 
       add_strobes-SS_8.1-0 ; 
       add_strobes-SS_9.1-0 ; 
       add_strobes-turret.1-0 ; 
       add_strobes-turret1.1-0 ; 
       add_strobes-turret2.1-0 ; 
       add_strobes-turret4.1-0 ; 
       add_strobes-turwepemt1.1-0 ; 
       add_strobes-turwepemt2.1-0 ; 
       add_strobes-turwepemt3.1-0 ; 
       add_strobes-turwepemt4.1-0 ; 
       add_strobes-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-add_strobes.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       21 14 110 ; 
       22 15 110 ; 
       23 5 110 ; 
       17 4 110 ; 
       1 0 110 ; 
       2 6 110 ; 
       3 7 110 ; 
       4 2 110 ; 
       5 3 110 ; 
       6 9 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 1 110 ; 
       11 32 110 ; 
       12 32 110 ; 
       13 32 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 11 110 ; 
       18 12 110 ; 
       19 13 110 ; 
       24 3 110 ; 
       25 14 110 ; 
       26 15 110 ; 
       27 2 110 ; 
       28 26 110 ; 
       29 24 110 ; 
       30 27 110 ; 
       31 25 110 ; 
       32 0 110 ; 
       20 10 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       21 SCHEM 7.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 125 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 45 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 71.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 76.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 MPRFLG 0 ; 
       3 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 55 -4 0 MPRFLG 0 ; 
       8 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 30 -2 0 MPRFLG 0 ; 
       10 SCHEM 73.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 100 -4 0 MPRFLG 0 ; 
       13 SCHEM 112.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 126.25 -2 0 MPRFLG 0 ; 
       16 SCHEM 82.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 95 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 107.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       25 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       26 SCHEM 121.25 -4 0 MPRFLG 0 ; 
       27 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       28 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 100 -2 0 MPRFLG 0 ; 
       20 SCHEM 72.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 32 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
