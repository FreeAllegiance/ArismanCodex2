SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.244-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 77     
       add_strobes-bool1_5_1.2-0 ROOT ; 
       add_strobes-control_tower.2-0 ; 
       add_strobes-east_bay_7.7-0 ; 
       add_strobes-east_bay_9.1-0 ; 
       add_strobes-east_bay_antenna.1-0 ; 
       add_strobes-east_bay_antenna1.1-0 ; 
       add_strobes-east_bay_strut.1-0 ; 
       add_strobes-east_bay_strut_1.1-0 ; 
       add_strobes-east_bay_strut1.1-0 ; 
       add_strobes-east_bay_strut1_1.3-0 ; 
       add_strobes-landing_lights.1-0 ; 
       add_strobes-landing_lights1.1-0 ; 
       add_strobes-landing_lights2.1-0 ; 
       add_strobes-landing_lights3.1-0 ; 
       add_strobes-landing_lights4.1-0 ; 
       add_strobes-landing_lights5.1-0 ; 
       add_strobes-main_antenna.1-0 ; 
       add_strobes-south_block.1-0 ; 
       add_strobes-south_block5.1-0 ; 
       add_strobes-south_block6.1-0 ; 
       add_strobes-south_hull_1.3-0 ; 
       add_strobes-south_hull_2.1-0 ; 
       add_strobes-SS_01.1-0 ; 
       add_strobes-SS_10.1-0 ; 
       add_strobes-SS_11.1-0 ; 
       add_strobes-SS_12.1-0 ; 
       add_strobes-SS_13.1-0 ; 
       add_strobes-SS_14.1-0 ; 
       add_strobes-SS_15.1-0 ; 
       add_strobes-SS_16.1-0 ; 
       add_strobes-SS_17.1-0 ; 
       add_strobes-SS_18.1-0 ; 
       add_strobes-SS_19.1-0 ; 
       add_strobes-SS_2.1-0 ; 
       add_strobes-SS_20.1-0 ; 
       add_strobes-SS_21.1-0 ; 
       add_strobes-SS_22.1-0 ; 
       add_strobes-SS_23.1-0 ; 
       add_strobes-SS_24.1-0 ; 
       add_strobes-SS_25.1-0 ; 
       add_strobes-SS_26.1-0 ; 
       add_strobes-SS_27.1-0 ; 
       add_strobes-SS_28.1-0 ; 
       add_strobes-SS_29.1-0 ; 
       add_strobes-SS_3.1-0 ; 
       add_strobes-SS_30.1-0 ; 
       add_strobes-SS_31.1-0 ; 
       add_strobes-SS_32.1-0 ; 
       add_strobes-SS_33.1-0 ; 
       add_strobes-SS_34.1-0 ; 
       add_strobes-SS_35.1-0 ; 
       add_strobes-SS_36.1-0 ; 
       add_strobes-SS_37.1-0 ; 
       add_strobes-SS_38.1-0 ; 
       add_strobes-SS_39.1-0 ; 
       add_strobes-SS_40.1-0 ; 
       add_strobes-SS_41.1-0 ; 
       add_strobes-SS_42.1-0 ; 
       add_strobes-SS_43.1-0 ; 
       add_strobes-SS_44.1-0 ; 
       add_strobes-SS_45.1-0 ; 
       add_strobes-SS_46.1-0 ; 
       add_strobes-SS_6.1-0 ; 
       add_strobes-SS_7.1-0 ; 
       add_strobes-SS_8.1-0 ; 
       add_strobes-SS_9.1-0 ; 
       add_strobes-strobe_set.1-0 ; 
       add_strobes-strobe_set1.1-0 ; 
       add_strobes-turret.1-0 ; 
       add_strobes-turret1.1-0 ; 
       add_strobes-turret2.1-0 ; 
       add_strobes-turret4.1-0 ; 
       add_strobes-turwepemt1.1-0 ; 
       add_strobes-turwepemt2.1-0 ; 
       add_strobes-turwepemt3.1-0 ; 
       add_strobes-turwepemt4.1-0 ; 
       add_strobes-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-add_strobes.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       63 20 110 ; 
       64 21 110 ; 
       65 5 110 ; 
       23 4 110 ; 
       24 10 110 ; 
       10 66 110 ; 
       11 66 110 ; 
       25 10 110 ; 
       26 10 110 ; 
       27 10 110 ; 
       28 10 110 ; 
       29 10 110 ; 
       30 11 110 ; 
       31 11 110 ; 
       32 11 110 ; 
       34 11 110 ; 
       35 11 110 ; 
       36 11 110 ; 
       12 66 110 ; 
       37 12 110 ; 
       38 12 110 ; 
       39 12 110 ; 
       40 12 110 ; 
       41 12 110 ; 
       42 12 110 ; 
       66 3 110 ; 
       67 2 110 ; 
       13 67 110 ; 
       43 13 110 ; 
       45 13 110 ; 
       1 0 110 ; 
       2 6 110 ; 
       3 7 110 ; 
       4 2 110 ; 
       5 3 110 ; 
       6 9 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       16 1 110 ; 
       17 76 110 ; 
       18 76 110 ; 
       19 76 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 17 110 ; 
       33 18 110 ; 
       44 19 110 ; 
       68 3 110 ; 
       69 20 110 ; 
       70 21 110 ; 
       71 2 110 ; 
       72 70 110 ; 
       73 68 110 ; 
       74 71 110 ; 
       75 69 110 ; 
       76 0 110 ; 
       62 16 110 ; 
       46 13 110 ; 
       47 13 110 ; 
       48 13 110 ; 
       49 13 110 ; 
       14 67 110 ; 
       50 14 110 ; 
       51 14 110 ; 
       52 14 110 ; 
       53 14 110 ; 
       54 14 110 ; 
       55 14 110 ; 
       15 67 110 ; 
       56 15 110 ; 
       57 15 110 ; 
       58 15 110 ; 
       59 15 110 ; 
       60 15 110 ; 
       61 15 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       63 SCHEM 2.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 117.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 55 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 66.25 -14 0 MPRFLG 0 ; 
       11 SCHEM 81.25 -14 0 MPRFLG 0 ; 
       25 SCHEM 70 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 60 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 65 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 67.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 87.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 75 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 77.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 80 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 82.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 85 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 96.25 -14 0 MPRFLG 0 ; 
       37 SCHEM 102.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 90 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 58.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM 92.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 95 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 97.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 100 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 81.25 -12 0 MPRFLG 0 ; 
       67 SCHEM 31.25 -12 0 MPRFLG 0 ; 
       13 SCHEM 31.25 -14 0 MPRFLG 0 ; 
       43 SCHEM 37.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 25 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       1 SCHEM 105 -6 0 MPRFLG 0 ; 
       2 SCHEM 28.75 -10 0 MPRFLG 0 ; 
       3 SCHEM 78.75 -10 0 MPRFLG 0 ; 
       4 SCHEM 5 -12 0 MPRFLG 0 ; 
       5 SCHEM 55 -12 0 MPRFLG 0 ; 
       6 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       7 SCHEM 78.75 -8 0 MPRFLG 0 ; 
       8 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 105 -8 0 MPRFLG 0 ; 
       17 SCHEM 107.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 110 -8 0 MPRFLG 0 ; 
       19 SCHEM 112.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 1.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 116.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 107.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 110 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 112.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       69 SCHEM 0 -8 0 MPRFLG 0 ; 
       70 SCHEM 115 -8 0 MPRFLG 0 ; 
       71 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       72 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 7.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 110 -6 0 MPRFLG 0 ; 
       62 SCHEM 105 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 27.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 30 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 32.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 35 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 16.25 -14 0 MPRFLG 0 ; 
       50 SCHEM 22.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 10 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 12.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 15 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 17.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 20 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 46.25 -14 0 MPRFLG 0 ; 
       56 SCHEM 52.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 40 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 42.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 45 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 47.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 50 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
