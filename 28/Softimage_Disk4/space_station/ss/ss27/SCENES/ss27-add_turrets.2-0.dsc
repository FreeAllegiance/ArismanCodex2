SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       new_lighting-bool1_5_1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.235-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       add_turrets-tetra1.1-0 ROOT ; 
       new_lighting-bool1_5_1.4-0 ROOT ; 
       new_lighting-control_tower.2-0 ; 
       new_lighting-east_bay_7.7-0 ; 
       new_lighting-east_bay_9.1-0 ; 
       new_lighting-east_bay_antenna.1-0 ; 
       new_lighting-east_bay_antenna1.1-0 ; 
       new_lighting-east_bay_strut.1-0 ; 
       new_lighting-east_bay_strut_1.1-0 ; 
       new_lighting-east_bay_strut1.1-0 ; 
       new_lighting-east_bay_strut1_1.3-0 ; 
       new_lighting-main_antenna.1-0 ; 
       new_lighting-south_block.1-0 ; 
       new_lighting-south_block5.1-0 ; 
       new_lighting-south_block6.1-0 ; 
       new_lighting-south_hull_1.3-0 ; 
       new_lighting-south_hull_2.1-0 ; 
       new_lighting-SSc1.1-0 ; 
       new_lighting-SSc2.1-0 ; 
       new_lighting-SSc3.1-0 ; 
       new_lighting-SSc4.1-0 ; 
       new_lighting-SSc5.1-0 ; 
       new_lighting-SSc6.1-0 ; 
       new_lighting-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-add_turrets.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 7 110 ; 
       4 8 110 ; 
       5 3 110 ; 
       6 4 110 ; 
       7 10 110 ; 
       8 9 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 2 110 ; 
       12 23 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 12 110 ; 
       18 12 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 13 110 ; 
       22 13 110 ; 
       23 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 58.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 MPRFLG 0 ; 
       6 SCHEM 30 -8 0 MPRFLG 0 ; 
       7 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       10 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 50 -4 0 MPRFLG 0 ; 
       12 SCHEM 63.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 78.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 93.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 MPRFLG 0 ; 
       16 SCHEM 105 -2 0 MPRFLG 0 ; 
       17 SCHEM 60 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 57.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 90 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 87.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 75 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 78.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 117.5 0 0 DISPLAY 1 2 SRT 0.3519999 0.3519999 0.3519999 2.094395 0 0 19.9015 -2.392067 3.413786 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 109 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
