SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       new_lighting-bool1_5_1.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.238-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       new_lighting-bool1_5_1.7-0 ROOT ; 
       new_lighting-control_tower.2-0 ; 
       new_lighting-east_bay_7.7-0 ; 
       new_lighting-east_bay_9.1-0 ; 
       new_lighting-east_bay_antenna.1-0 ; 
       new_lighting-east_bay_antenna1.1-0 ; 
       new_lighting-east_bay_strut.1-0 ; 
       new_lighting-east_bay_strut_1.1-0 ; 
       new_lighting-east_bay_strut1.1-0 ; 
       new_lighting-east_bay_strut1_1.3-0 ; 
       new_lighting-main_antenna.1-0 ; 
       new_lighting-south_block.1-0 ; 
       new_lighting-south_block5.1-0 ; 
       new_lighting-south_block6.1-0 ; 
       new_lighting-south_hull_1.3-0 ; 
       new_lighting-south_hull_2.1-0 ; 
       new_lighting-SSc1.1-0 ; 
       new_lighting-SSc2.1-0 ; 
       new_lighting-SSc3.1-0 ; 
       new_lighting-SSc4.1-0 ; 
       new_lighting-SSc5.1-0 ; 
       new_lighting-SSc6.1-0 ; 
       new_lighting-turret.1-0 ; 
       new_lighting-turret1.1-0 ; 
       new_lighting-turret2.1-0 ; 
       new_lighting-turret4.1-0 ; 
       new_lighting-turwepemt1.1-0 ; 
       new_lighting-turwepemt2.1-0 ; 
       new_lighting-turwepemt3.1-0 ; 
       new_lighting-turwepemt4.1-0 ; 
       new_lighting-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-add_turrets.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 6 110 ; 
       3 7 110 ; 
       4 2 110 ; 
       5 3 110 ; 
       6 9 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 1 110 ; 
       11 30 110 ; 
       12 30 110 ; 
       13 30 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 11 110 ; 
       17 11 110 ; 
       18 13 110 ; 
       19 13 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       30 0 110 ; 
       22 3 110 ; 
       23 14 110 ; 
       24 15 110 ; 
       26 24 110 ; 
       25 2 110 ; 
       27 22 110 ; 
       28 25 110 ; 
       29 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 MPRFLG 0 ; 
       6 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 MPRFLG 0 ; 
       11 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 20 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 30 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 25 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 22.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 34 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
