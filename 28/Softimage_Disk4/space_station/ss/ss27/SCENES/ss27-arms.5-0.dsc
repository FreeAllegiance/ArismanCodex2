SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       arms-bool1.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.173-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 21     
       arms-light10.5-0 ROOT ; 
       arms-light8.5-0 ROOT ; 
       arms-light9.5-0 ROOT ; 
       arms-spot1.1-0 ; 
       arms-spot1_int.5-0 ROOT ; 
       arms-spot2.1-0 ; 
       arms-spot2_int.5-0 ROOT ; 
       arms-spot3.1-0 ; 
       arms-spot3_int.5-0 ROOT ; 
       arms-spot4.1-0 ; 
       arms-spot4_int.5-0 ROOT ; 
       arms-spot5.1-0 ; 
       arms-spot5_int.5-0 ROOT ; 
       arms-spot6.1-0 ; 
       arms-spot6_int.5-0 ROOT ; 
       arms-spot7.1-0 ; 
       arms-spot7_int.5-0 ROOT ; 
       arms-spot8.1-0 ; 
       arms-spot8_int1.5-0 ROOT ; 
       arms-spot9.1-0 ; 
       arms-spot9_int1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       arms-bay_strut_bridge.1-0 ; 
       arms-bool1.5-0 ROOT ; 
       arms-control_tower.2-0 ; 
       arms-east_bay.2-0 ROOT ; 
       arms-east_bay_antenna.1-0 ; 
       arms-east_bay_strut.4-0 ROOT ; 
       arms-east_bay_strut1.2-0 ROOT ; 
       arms-east_large_sub-strut.2-0 ROOT ; 
       arms-east_small_sub-strut.2-0 ROOT ; 
       arms-main_antenna.1-0 ; 
       arms-north_hull.1-0 ; 
       arms-south_block.1-0 ; 
       arms-south_block1.1-0 ; 
       arms-south_block2.1-0 ; 
       arms-south_hull.2-0 ; 
       arms-SSc1.1-0 ; 
       arms-SSc2.1-0 ; 
       arms-SSc3.1-0 ; 
       arms-SSc4.1-0 ; 
       arms-SSc5.1-0 ; 
       arms-SSc6.1-0 ; 
       arms-utl28a_2.1-0 ; 
       arms-west_bay.1-0 ; 
       arms-west_bay_antenna.1-0 ; 
       arms-west_bay_strut.2-0 ROOT ; 
       arms-west_large_sub-strut_1.3-0 ; 
       arms-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 11     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/black ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/gantry ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/landing-area ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/lights ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/logo ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/rendermap1 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/side ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/single_light ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/solar ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/text ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-arms.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       4 3 110 ; 
       9 2 110 ; 
       10 1 110 ; 
       11 21 110 ; 
       12 21 110 ; 
       13 21 110 ; 
       14 1 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 13 110 ; 
       18 13 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       21 1 110 ; 
       22 24 110 ; 
       23 22 110 ; 
       25 24 110 ; 
       26 24 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 7 2111 ; 
       1 3 2111 ; 
       2 5 2111 ; 
       3 4 2110 ; 
       5 6 2110 ; 
       7 8 2110 ; 
       9 10 2110 ; 
       11 12 2110 ; 
       13 14 2110 ; 
       15 16 2110 ; 
       17 18 2110 ; 
       19 20 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 12.5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 15 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 20 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 0 -34 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 0 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -34 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 5 -34 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 5 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -34 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 7.5 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 10 -34 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 10 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 12.5 -34 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 15 -34 0 WIRECOL 7 7 MPRFLG 0 ; 
       20 SCHEM 15 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 33.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 30 -6 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -14 0 SRT 1 1 1 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 0 -16 0 MPRFLG 0 ; 
       5 SCHEM 5 -20 0 DISPLAY 1 2 SRT 1 1 1 0 3.141593 0.06 12.00735 2.931329 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -20 0 SRT 1 1 0.9999999 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 0 -26 0 SRT 0.9 0.9 0.5759999 0 -3.141593 0 13.1291 2.127311 -1.147782e-006 MPRFLG 0 ; 
       8 SCHEM 2.5 -26 0 SRT 0.3942001 0.3942001 0.2522879 0 -3.141593 0 20.47204 1.579811 -1.789722e-006 MPRFLG 0 ; 
       9 SCHEM 30 -8 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       15 SCHEM 35 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 60 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 57.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 45 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 50 -6 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -28 0 MPRFLG 0 ; 
       23 SCHEM 7.5 -30 0 MPRFLG 0 ; 
       24 SCHEM 7.5 -26 0 DISPLAY 0 0 SRT 1 1 0.9999999 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 5 -28 0 MPRFLG 0 ; 
       26 SCHEM 10 -28 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 69 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
