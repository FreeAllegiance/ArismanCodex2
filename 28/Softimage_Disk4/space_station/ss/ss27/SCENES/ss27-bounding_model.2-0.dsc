SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.316-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 96     
       bounding_model-cube1.1-0 ; 
       bounding_model-cube2.1-0 ; 
       bounding_model-cube3.1-0 ; 
       bounding_model-cube5.1-0 ; 
       bounding_model-cube6.1-0 ; 
       bounding_model-cube7.1-0 ; 
       bounding_model-cube8.1-0 ; 
       bounding_model-cube9.1-0 ; 
       bounding_model-null1.1-0 ; 
       bounding_model-null2.1-0 ROOT ; 
       bounding_model-south_block.1-0 ; 
       bounding_model-south_block5.1-0 ; 
       bounding_model-south_block6.1-0 ; 
       bounding_model-SS_01.1-0 ; 
       bounding_model-SS_2.1-0 ; 
       bounding_model-SS_3.1-0 ; 
       edit_garage_frames-bool1_5_1.2-0 ; 
       edit_garage_frames-control_tower.2-0 ; 
       edit_garage_frames-east_bay_11.1-0 ; 
       edit_garage_frames-east_bay_7.7-0 ; 
       edit_garage_frames-east_bay_antenna.1-0 ; 
       edit_garage_frames-east_bay_antenna1.1-0 ; 
       edit_garage_frames-east_bay_strut_1.1-0 ; 
       edit_garage_frames-east_bay_strut_2.1-0 ; 
       edit_garage_frames-east_bay_strut1.1-0 ; 
       edit_garage_frames-garage1A.1-0 ; 
       edit_garage_frames-garage1B.1-0 ; 
       edit_garage_frames-garage1C.1-0 ; 
       edit_garage_frames-garage1D.1-0 ; 
       edit_garage_frames-garage1E.1-0 ; 
       edit_garage_frames-garage2A.1-0 ; 
       edit_garage_frames-garage2B.1-0 ; 
       edit_garage_frames-garage2C.1-0 ; 
       edit_garage_frames-garage2D.1-0 ; 
       edit_garage_frames-garage2E.1-0 ; 
       edit_garage_frames-landing_lights.1-0 ; 
       edit_garage_frames-landing_lights1.1-0 ; 
       edit_garage_frames-landing_lights2.1-0 ; 
       edit_garage_frames-landing_lights3.1-0 ; 
       edit_garage_frames-landing_lights4.1-0 ; 
       edit_garage_frames-landing_lights5.1-0 ; 
       edit_garage_frames-launch1.1-0 ; 
       edit_garage_frames-launch2.1-0 ; 
       edit_garage_frames-main_antenna.1-0 ; 
       edit_garage_frames-root.4-0 ROOT ; 
       edit_garage_frames-south_hull_1.3-0 ; 
       edit_garage_frames-south_hull_2.1-0 ; 
       edit_garage_frames-SS_10.1-0 ; 
       edit_garage_frames-SS_11.1-0 ; 
       edit_garage_frames-SS_12.1-0 ; 
       edit_garage_frames-SS_13.1-0 ; 
       edit_garage_frames-SS_14.1-0 ; 
       edit_garage_frames-SS_15.1-0 ; 
       edit_garage_frames-SS_16.1-0 ; 
       edit_garage_frames-SS_17.1-0 ; 
       edit_garage_frames-SS_18.1-0 ; 
       edit_garage_frames-SS_19.1-0 ; 
       edit_garage_frames-SS_20.1-0 ; 
       edit_garage_frames-SS_21.1-0 ; 
       edit_garage_frames-SS_22.1-0 ; 
       edit_garage_frames-SS_23.1-0 ; 
       edit_garage_frames-SS_24.1-0 ; 
       edit_garage_frames-SS_25.1-0 ; 
       edit_garage_frames-SS_26.1-0 ; 
       edit_garage_frames-SS_27.1-0 ; 
       edit_garage_frames-SS_28.1-0 ; 
       edit_garage_frames-SS_29.1-0 ; 
       edit_garage_frames-SS_30.1-0 ; 
       edit_garage_frames-SS_31.1-0 ; 
       edit_garage_frames-SS_32.1-0 ; 
       edit_garage_frames-SS_33.1-0 ; 
       edit_garage_frames-SS_34.1-0 ; 
       edit_garage_frames-SS_35.1-0 ; 
       edit_garage_frames-SS_36.1-0 ; 
       edit_garage_frames-SS_37.1-0 ; 
       edit_garage_frames-SS_38.1-0 ; 
       edit_garage_frames-SS_39.1-0 ; 
       edit_garage_frames-SS_40.1-0 ; 
       edit_garage_frames-SS_41.1-0 ; 
       edit_garage_frames-SS_42.1-0 ; 
       edit_garage_frames-SS_43.1-0 ; 
       edit_garage_frames-SS_44.1-0 ; 
       edit_garage_frames-SS_45.1-0 ; 
       edit_garage_frames-SS_46.1-0 ; 
       edit_garage_frames-SS_6.1-0 ; 
       edit_garage_frames-SS_7.1-0 ; 
       edit_garage_frames-SS_8.1-0 ; 
       edit_garage_frames-SS_9.1-0 ; 
       edit_garage_frames-strobe_set.1-0 ; 
       edit_garage_frames-strobe_set1.1-0 ; 
       edit_garage_frames-turwepemt1.1-0 ; 
       edit_garage_frames-turwepemt2.1-0 ; 
       edit_garage_frames-turwepemt3.1-0 ; 
       edit_garage_frames-turwepemt4.1-0 ; 
       edit_garage_frames-utl28a_2.1-0 ; 
       edit_garage_frames-west_bay_strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-bounding_model.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       16 44 110 ; 
       17 16 110 ; 
       18 22 110 ; 
       19 23 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 24 110 ; 
       23 95 110 ; 
       24 16 110 ; 
       25 19 110 ; 
       26 19 110 ; 
       27 19 110 ; 
       28 19 110 ; 
       29 19 110 ; 
       30 18 110 ; 
       31 18 110 ; 
       32 18 110 ; 
       33 18 110 ; 
       34 18 110 ; 
       35 88 110 ; 
       36 88 110 ; 
       37 88 110 ; 
       38 89 110 ; 
       39 89 110 ; 
       40 89 110 ; 
       41 19 110 ; 
       42 18 110 ; 
       43 17 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       45 16 110 ; 
       46 16 110 ; 
       13 10 110 ; 
       47 20 110 ; 
       48 35 110 ; 
       49 35 110 ; 
       50 35 110 ; 
       51 35 110 ; 
       52 35 110 ; 
       53 35 110 ; 
       54 36 110 ; 
       55 36 110 ; 
       56 36 110 ; 
       14 11 110 ; 
       57 36 110 ; 
       58 36 110 ; 
       59 36 110 ; 
       60 37 110 ; 
       61 37 110 ; 
       62 37 110 ; 
       63 37 110 ; 
       64 37 110 ; 
       65 37 110 ; 
       66 38 110 ; 
       15 12 110 ; 
       67 38 110 ; 
       68 38 110 ; 
       69 38 110 ; 
       70 38 110 ; 
       71 38 110 ; 
       72 39 110 ; 
       73 39 110 ; 
       74 39 110 ; 
       75 39 110 ; 
       76 39 110 ; 
       77 39 110 ; 
       78 40 110 ; 
       79 40 110 ; 
       80 40 110 ; 
       81 40 110 ; 
       82 40 110 ; 
       83 40 110 ; 
       84 43 110 ; 
       85 45 110 ; 
       86 46 110 ; 
       87 21 110 ; 
       88 18 110 ; 
       89 19 110 ; 
       90 46 110 ; 
       91 18 110 ; 
       92 19 110 ; 
       93 45 110 ; 
       94 16 110 ; 
       95 16 110 ; 
       0 9 110 ; 
       8 9 110 ; 
       1 8 110 ; 
       2 8 110 ; 
       4 9 110 ; 
       3 9 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       7 9 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       16 SCHEM 71.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 135 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 101.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 36.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 70 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 101.25 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 36.25 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 101.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 60 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 55 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 62.5 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 65 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 57.5 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 127.5 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 122.5 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 120 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 130 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 125 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 81.25 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 96.25 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 111.25 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 31.25 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 16.25 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 46.25 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 67.5 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 132.5 -14 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 135 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 71.25 -4 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 0 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 2.5 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 5 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 1.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 141.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 0 -24 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 5 -16 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 87.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 85 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 75 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 77.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 80 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 82.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 102.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 90 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 92.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -24 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 95 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 97.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 100 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 117.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 105 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 107.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 110 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 112.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 115 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 37.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 5 -24 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       67 SCHEM 25 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 27.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 30 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 32.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 35 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 22.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 10 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 12.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 15 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 17.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM 20 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 52.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 40 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 42.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       81 SCHEM 45 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       82 SCHEM 47.5 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       83 SCHEM 50 -18 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       84 SCHEM 135 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       85 SCHEM 2.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       86 SCHEM 142.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       87 SCHEM 70 -16 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       88 SCHEM 96.25 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       89 SCHEM 31.25 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       90 SCHEM 140 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       91 SCHEM 72.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       92 SCHEM 7.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       93 SCHEM 0 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       94 SCHEM 137.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       95 SCHEM 36.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 11.25 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 12.5 -24 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 10 -24 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 17.5 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 15 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 20 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 22.5 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 25 -22 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 12.5 -20 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
