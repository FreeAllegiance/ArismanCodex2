SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.354-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 87     
       edit_garage_frames-bool1_5_1.2-0 ; 
       edit_garage_frames-control_tower.2-0 ; 
       edit_garage_frames-east_bay_11.1-0 ; 
       edit_garage_frames-east_bay_12.1-0 ; 
       edit_garage_frames-east_bay_7.5-0 ROOT ; 
       edit_garage_frames-east_bay_antenna.1-0 ; 
       edit_garage_frames-east_bay_antenna1.1-0 ; 
       edit_garage_frames-east_bay_strut_1.1-0 ; 
       edit_garage_frames-east_bay_strut_2.1-0 ; 
       edit_garage_frames-east_bay_strut1.1-0 ; 
       edit_garage_frames-garage1A.1-0 ; 
       edit_garage_frames-garage1B.1-0 ; 
       edit_garage_frames-garage1C.1-0 ; 
       edit_garage_frames-garage1D.1-0 ; 
       edit_garage_frames-garage1E.1-0 ; 
       edit_garage_frames-garage2A.1-0 ; 
       edit_garage_frames-garage2B.1-0 ; 
       edit_garage_frames-garage2C.1-0 ; 
       edit_garage_frames-garage2D.1-0 ; 
       edit_garage_frames-garage2E.1-0 ; 
       edit_garage_frames-landing_lights.1-0 ; 
       edit_garage_frames-landing_lights1.1-0 ; 
       edit_garage_frames-landing_lights2.1-0 ; 
       edit_garage_frames-landing_lights6.1-0 ; 
       edit_garage_frames-landing_lights7.1-0 ; 
       edit_garage_frames-landing_lights8.1-0 ; 
       edit_garage_frames-launch1.1-0 ; 
       edit_garage_frames-launch2.1-0 ; 
       edit_garage_frames-main_antenna.1-0 ; 
       edit_garage_frames-root_1.13-0 ROOT ; 
       edit_garage_frames-south_block.1-0 ; 
       edit_garage_frames-south_block5.1-0 ; 
       edit_garage_frames-south_block6.1-0 ; 
       edit_garage_frames-south_hull_1.3-0 ; 
       edit_garage_frames-south_hull_2.1-0 ; 
       edit_garage_frames-SS_01.1-0 ; 
       edit_garage_frames-SS_10.1-0 ; 
       edit_garage_frames-SS_11.1-0 ; 
       edit_garage_frames-SS_12.1-0 ; 
       edit_garage_frames-SS_13.1-0 ; 
       edit_garage_frames-SS_14.1-0 ; 
       edit_garage_frames-SS_15.1-0 ; 
       edit_garage_frames-SS_16.1-0 ; 
       edit_garage_frames-SS_17.1-0 ; 
       edit_garage_frames-SS_18.1-0 ; 
       edit_garage_frames-SS_19.1-0 ; 
       edit_garage_frames-SS_2.1-0 ; 
       edit_garage_frames-SS_20.1-0 ; 
       edit_garage_frames-SS_21.1-0 ; 
       edit_garage_frames-SS_22.1-0 ; 
       edit_garage_frames-SS_23.1-0 ; 
       edit_garage_frames-SS_24.1-0 ; 
       edit_garage_frames-SS_25.1-0 ; 
       edit_garage_frames-SS_26.1-0 ; 
       edit_garage_frames-SS_27.1-0 ; 
       edit_garage_frames-SS_28.1-0 ; 
       edit_garage_frames-SS_3.1-0 ; 
       edit_garage_frames-SS_47.1-0 ; 
       edit_garage_frames-SS_48.1-0 ; 
       edit_garage_frames-SS_49.1-0 ; 
       edit_garage_frames-SS_50.1-0 ; 
       edit_garage_frames-SS_51.1-0 ; 
       edit_garage_frames-SS_52.1-0 ; 
       edit_garage_frames-SS_53.1-0 ; 
       edit_garage_frames-SS_54.1-0 ; 
       edit_garage_frames-SS_55.1-0 ; 
       edit_garage_frames-SS_56.1-0 ; 
       edit_garage_frames-SS_57.1-0 ; 
       edit_garage_frames-SS_58.1-0 ; 
       edit_garage_frames-SS_59.1-0 ; 
       edit_garage_frames-SS_6.1-0 ; 
       edit_garage_frames-SS_60.1-0 ; 
       edit_garage_frames-SS_61.1-0 ; 
       edit_garage_frames-SS_62.1-0 ; 
       edit_garage_frames-SS_63.1-0 ; 
       edit_garage_frames-SS_64.1-0 ; 
       edit_garage_frames-SS_7.1-0 ; 
       edit_garage_frames-SS_8.1-0 ; 
       edit_garage_frames-SS_9.1-0 ; 
       edit_garage_frames-strobe_set.1-0 ; 
       edit_garage_frames-strobe_set2.1-0 ; 
       edit_garage_frames-turwepemt1.1-0 ; 
       edit_garage_frames-turwepemt2.1-0 ; 
       edit_garage_frames-turwepemt3.1-0 ; 
       edit_garage_frames-turwepemt4.1-0 ; 
       edit_garage_frames-utl28a_2.1-0 ; 
       edit_garage_frames-west_bay_strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-edit_bays.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 29 110 ; 
       1 0 110 ; 
       2 7 110 ; 
       3 8 110 ; 
       5 8 110 ; 
       6 2 110 ; 
       7 9 110 ; 
       8 86 110 ; 
       9 0 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       13 8 110 ; 
       14 8 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 79 110 ; 
       21 79 110 ; 
       22 79 110 ; 
       23 80 110 ; 
       24 80 110 ; 
       25 80 110 ; 
       26 8 110 ; 
       27 2 110 ; 
       28 1 110 ; 
       30 85 110 ; 
       31 85 110 ; 
       32 85 110 ; 
       33 0 110 ; 
       34 0 110 ; 
       35 30 110 ; 
       36 5 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 20 110 ; 
       42 20 110 ; 
       43 21 110 ; 
       44 21 110 ; 
       45 21 110 ; 
       46 31 110 ; 
       47 21 110 ; 
       48 21 110 ; 
       49 21 110 ; 
       50 22 110 ; 
       51 22 110 ; 
       52 22 110 ; 
       53 22 110 ; 
       54 22 110 ; 
       55 22 110 ; 
       56 32 110 ; 
       57 23 110 ; 
       58 23 110 ; 
       59 23 110 ; 
       60 23 110 ; 
       61 23 110 ; 
       62 23 110 ; 
       63 24 110 ; 
       64 24 110 ; 
       65 24 110 ; 
       66 24 110 ; 
       67 24 110 ; 
       68 24 110 ; 
       69 25 110 ; 
       70 28 110 ; 
       71 25 110 ; 
       72 25 110 ; 
       73 25 110 ; 
       74 25 110 ; 
       75 25 110 ; 
       76 33 110 ; 
       77 34 110 ; 
       78 6 110 ; 
       79 2 110 ; 
       80 8 110 ; 
       81 34 110 ; 
       82 2 110 ; 
       83 8 110 ; 
       84 33 110 ; 
       85 0 110 ; 
       86 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 80 -2 0 MPRFLG 0 ; 
       1 SCHEM 142.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 108.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 30 -8 0 MPRFLG 0 ; 
       4 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 0.998 0.998 0.998 -1.570796 3.141592 0 8.339124e-008 -7.633436e-007 -9.779408e-008 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 MPRFLG 0 ; 
       6 SCHEM 77.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 108.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 108.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 25 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 125.3017 -10 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 130.3017 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 127.8017 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 135.3017 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 132.8017 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 88.75 -12 0 MPRFLG 0 ; 
       21 SCHEM 103.75 -12 0 MPRFLG 0 ; 
       22 SCHEM 118.75 -12 0 MPRFLG 0 ; 
       23 SCHEM 53.75 -10 0 MPRFLG 0 ; 
       24 SCHEM 38.75 -10 0 MPRFLG 0 ; 
       25 SCHEM 68.75 -10 0 MPRFLG 0 ; 
       26 SCHEM 27.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 137.8017 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 142.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 80 0 0 SRT 1 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 145 -6 0 MPRFLG 0 ; 
       31 SCHEM 147.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 150 -6 0 MPRFLG 0 ; 
       33 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       34 SCHEM 153.75 -4 0 MPRFLG 0 ; 
       35 SCHEM 145 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 10 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 95 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 92.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 82.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 85 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 87.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 90 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 110 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 97.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 100 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 147.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 102.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 105 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 107.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 125 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 112.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 115 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 117.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 120 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 122.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 150 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 60 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 47.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 50 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 52.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 55 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 57.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 45 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 32.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 35 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 37.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 40 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 42.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 75 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 142.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       71 SCHEM 62.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       72 SCHEM 65 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       73 SCHEM 67.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 70 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 72.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 7.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 155 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 76.25 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 103.75 -10 0 MPRFLG 0 ; 
       80 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       81 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 147.5 -4 0 MPRFLG 0 ; 
       86 SCHEM 42.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
