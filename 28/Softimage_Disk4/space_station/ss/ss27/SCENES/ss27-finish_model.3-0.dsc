SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       finish_model-bool1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.37-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       finish_model-bay_strut_bridge.1-0 ; 
       finish_model-bool1.2-0 ROOT ; 
       finish_model-control_tower.2-0 ; 
       finish_model-east_bay.1-0 ; 
       finish_model-east_bay_antenna.1-0 ; 
       finish_model-east_bay_strut.1-0 ; 
       finish_model-east_large_sub-strut.1-0 ; 
       finish_model-east_small_sub-strut.1-0 ; 
       finish_model-fuselg1.1-0 ; 
       finish_model-fuselg1_1.1-0 ; 
       finish_model-fuselg1_2.1-0 ; 
       finish_model-main_antenna.1-0 ; 
       finish_model-south_hull.2-0 ; 
       finish_model-south_hull2.1-0 ; 
       finish_model-SSc1.1-0 ; 
       finish_model-SSc2.1-0 ; 
       finish_model-SSc3.1-0 ; 
       finish_model-SSc4.1-0 ; 
       finish_model-SSc5.1-0 ; 
       finish_model-SSc6.1-0 ; 
       finish_model-utl28a_2.1-0 ; 
       finish_model-west_bay.1-0 ; 
       finish_model-west_bay_antenna.1-0 ; 
       finish_model-west_bay_strut.1-0 ; 
       finish_model-west_large_sub-strut_1.3-0 ; 
       finish_model-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-finish_model.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       9 20 110 ; 
       10 20 110 ; 
       5 0 110 ; 
       25 23 110 ; 
       13 1 110 ; 
       6 5 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       7 5 110 ; 
       12 1 110 ; 
       11 2 110 ; 
       24 23 110 ; 
       23 0 110 ; 
       21 23 110 ; 
       2 1 110 ; 
       22 21 110 ; 
       8 20 110 ; 
       14 8 110 ; 
       15 8 110 ; 
       16 10 110 ; 
       17 10 110 ; 
       18 9 110 ; 
       19 9 110 ; 
       20 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 28.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 33.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 15 -6 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 0 -10 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 MPRFLG 0 ; 
       24 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 10 -8 0 MPRFLG 0 ; 
       21 SCHEM 10 -10 0 MPRFLG 0 ; 
       2 SCHEM 20 -6 0 MPRFLG 0 ; 
       22 SCHEM 10 -12 0 MPRFLG 0 ; 
       8 SCHEM 23.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 35 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 30 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 28.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 36.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
