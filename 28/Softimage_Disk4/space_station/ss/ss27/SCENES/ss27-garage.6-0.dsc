SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl28a-utl28a_2.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.12-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       garage-cyl1.1-0 ROOT ; 
       garage-mesh2.2-0 ; 
       garage-mesh6.2-0 ROOT ; 
       garage-null6.3-0 ROOT ; 
       ss21a-hullroot.3-0 ROOT ; 
       StarBase-bool20.1-0 ; 
       StarBase-bool21.3-0 ; 
       StarBase-garage_mast1.5-0 ROOT ; 
       StarBase-garage_outside3.1-0 ; 
       StarBase-garage_root3_1.6-0 ; 
       StarBase-main_body.3-0 ROOT ; 
       StarBase-main_body1.3-0 ROOT ; 
       StarBase-mesh_5.5-0 ROOT ; 
       StarBase-tetra4.1-0 ; 
       StarBase2-bool21.1-0 ; 
       StarBase2-garage_mast1.1-0 ROOT ; 
       StarBase2-garage_outside3.1-0 ; 
       StarBase2-garage_root3_1.1-0 ; 
       StarBase2-tetra4.1-0 ; 
       utl28a-crgatt.1-0 ; 
       utl28a-ffuselg.1-0 ; 
       utl28a-fuselg1.1-0 ; 
       utl28a-fuselg2.1-0 ; 
       utl28a-fuselg3.1-0 ; 
       utl28a-SSc1.1-0 ; 
       utl28a-SSc2.1-0 ; 
       utl28a-SSc3.1-0 ; 
       utl28a-SSc4.1-0 ; 
       utl28a-SSc5.1-0 ; 
       utl28a-SSc6.1-0 ; 
       utl28a-utl28a_2.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-garage.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       14 15 110 ; 
       17 15 110 ; 
       16 15 110 ; 
       18 16 110 ; 
       1 3 110 ; 
       5 12 110 ; 
       6 7 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       13 8 110 ; 
       19 20 110 ; 
       20 30 110 ; 
       21 30 110 ; 
       22 30 110 ; 
       23 30 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 22 110 ; 
       29 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       30 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 55 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -12 0 SRT 1.918 1.918 1.918 0 0 0 0 1.514102 0 MPRFLG 0 ; 
       2 SCHEM 55 -14 0 SRT 1 1 1 0 0 0 0 0 5.292734 MPRFLG 0 ; 
       15 SCHEM 10 -20 0 SRT 1 1 1 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 1.25 -22 0 MPRFLG 0 ; 
       17 SCHEM 13.75 -22 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -22 0 MPRFLG 0 ; 
       18 SCHEM 6.25 -24 0 MPRFLG 0 ; 
       4 SCHEM 23.75 -20 0 SRT 1 1 1 0 -0.5199996 3.141593 1.490116e-008 -2.941025 -0.5887883 MPRFLG 0 ; 
       1 SCHEM 55 -6 0 MPRFLG 0 ; 
       5 SCHEM 1.25 -38 0 MPRFLG 0 ; 
       6 SCHEM 1.25 -30 0 MPRFLG 0 ; 
       7 SCHEM 10 -28 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -30 0 MPRFLG 0 ; 
       9 SCHEM 13.75 -30 0 MPRFLG 0 ; 
       10 SCHEM 23.75 -28 0 SRT 1 1 1 3.019916e-007 0 0 -0.0171088 2.056531 10.17154 MPRFLG 0 ; 
       11 SCHEM 28.75 -28 0 SRT 1 1 1 -3.019916e-007 -3.141592 1.216414e-013 -0.0171088 2.056538 -10.17164 MPRFLG 0 ; 
       12 SCHEM 8.75 -36 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 6.25 -32 0 MPRFLG 0 ; 
       19 SCHEM 0 -46 0 MPRFLG 0 ; 
       20 SCHEM 8.75 -44 0 WIRECOL 7 7 MPRFLG 0 ; 
       21 SCHEM 66.25 -44 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 96.25 -44 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 35 -44 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 55 -46 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 52.5 -46 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 22.5 -46 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 20 -46 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 85 -46 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -46 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 56.25 -42 0 SRT 1 1 1 -1.570796 0 0 0 -17.31209 -1.234713e-005 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 111.5 -44 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 41 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
