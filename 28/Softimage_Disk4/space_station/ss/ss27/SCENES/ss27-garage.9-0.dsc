SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl28a-utl28a_2.15-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.15-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       garage-cyl1.3-0 ROOT ; 
       garage-mesh2.2-0 ; 
       garage-mesh6.3-0 ROOT ; 
       garage-null6.4-0 ROOT ; 
       StarBase-bool20.1-0 ; 
       StarBase-bool21.3-0 ; 
       StarBase-garage_mast1.6-0 ROOT ; 
       StarBase-garage_outside3.1-0 ; 
       StarBase-garage_root3_1.6-0 ; 
       StarBase-main_body.4-0 ROOT ; 
       StarBase-main_body1.4-0 ROOT ; 
       StarBase-mesh_5.7-0 ROOT ; 
       StarBase-tetra4.1-0 ; 
       StarBase2-bool21.1-0 ; 
       StarBase2-garage_mast1.2-0 ROOT ; 
       StarBase2-garage_outside3.1-0 ; 
       StarBase2-garage_root3_1.1-0 ; 
       StarBase2-tetra4.1-0 ; 
       utl28a-crgatt.1-0 ; 
       utl28a-ffuselg.1-0 ; 
       utl28a-fuselg1.1-0 ; 
       utl28a-fuselg2.1-0 ; 
       utl28a-fuselg3.1-0 ; 
       utl28a-SSc1.1-0 ; 
       utl28a-SSc2.1-0 ; 
       utl28a-SSc3.1-0 ; 
       utl28a-SSc4.1-0 ; 
       utl28a-SSc5.1-0 ; 
       utl28a-SSc6.1-0 ; 
       utl28a-utl28a_2.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-garage.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 14 110 ; 
       16 14 110 ; 
       15 14 110 ; 
       17 15 110 ; 
       1 3 110 ; 
       4 11 110 ; 
       5 6 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       12 7 110 ; 
       18 19 110 ; 
       19 29 110 ; 
       20 29 110 ; 
       21 29 110 ; 
       22 29 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 22 110 ; 
       26 22 110 ; 
       27 21 110 ; 
       28 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 SRT 1.918 1.588104 1.918 0 -0.5235988 0 0 2.888669 0 MPRFLG 0 ; 
       2 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 5.292734 MPRFLG 0 ; 
       14 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 25 0 0 DISPLAY 0 0 SRT 1 1 1 3.019916e-007 0 0 -0.0171088 2.056531 10.17154 MPRFLG 0 ; 
       10 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 -3.019916e-007 -3.141592 1.216414e-013 -0.0171088 2.056538 -10.17164 MPRFLG 0 ; 
       11 SCHEM 30 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 20 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 32.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 32.5 -2 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 41.25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 46.25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 36.25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 42.5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 40 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 37.5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 35 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 45 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 40 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 41 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
