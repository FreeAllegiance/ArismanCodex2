SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.268-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 77     
       ss27-bool1_5_1.2-0 ; 
       ss27-control_tower.2-0 ; 
       ss27-east_bay_7.7-0 ; 
       ss27-east_bay_9.1-0 ; 
       ss27-east_bay_antenna.1-0 ; 
       ss27-east_bay_antenna1.1-0 ; 
       ss27-east_bay_strut_1.1-0 ; 
       ss27-east_bay_strut_2.1-0 ; 
       ss27-east_bay_strut1.1-0 ; 
       ss27-east_bay_strut2.1-0 ; 
       ss27-landing_lights.1-0 ; 
       ss27-landing_lights1.1-0 ; 
       ss27-landing_lights2.1-0 ; 
       ss27-landing_lights3.1-0 ; 
       ss27-landing_lights4.1-0 ; 
       ss27-landing_lights5.1-0 ; 
       ss27-main_antenna.1-0 ; 
       ss27-root.1-0 ROOT ; 
       ss27-south_block.1-0 ; 
       ss27-south_block5.1-0 ; 
       ss27-south_block6.1-0 ; 
       ss27-south_hull_1.3-0 ; 
       ss27-south_hull_2.1-0 ; 
       ss27-SS_01.1-0 ; 
       ss27-SS_10.1-0 ; 
       ss27-SS_11.1-0 ; 
       ss27-SS_12.1-0 ; 
       ss27-SS_13.1-0 ; 
       ss27-SS_14.1-0 ; 
       ss27-SS_15.1-0 ; 
       ss27-SS_16.1-0 ; 
       ss27-SS_17.1-0 ; 
       ss27-SS_18.1-0 ; 
       ss27-SS_19.1-0 ; 
       ss27-SS_2.1-0 ; 
       ss27-SS_20.1-0 ; 
       ss27-SS_21.1-0 ; 
       ss27-SS_22.1-0 ; 
       ss27-SS_23.1-0 ; 
       ss27-SS_24.1-0 ; 
       ss27-SS_25.1-0 ; 
       ss27-SS_26.1-0 ; 
       ss27-SS_27.1-0 ; 
       ss27-SS_28.1-0 ; 
       ss27-SS_29.1-0 ; 
       ss27-SS_3.1-0 ; 
       ss27-SS_30.1-0 ; 
       ss27-SS_31.1-0 ; 
       ss27-SS_32.1-0 ; 
       ss27-SS_33.1-0 ; 
       ss27-SS_34.1-0 ; 
       ss27-SS_35.1-0 ; 
       ss27-SS_36.1-0 ; 
       ss27-SS_37.1-0 ; 
       ss27-SS_38.1-0 ; 
       ss27-SS_39.1-0 ; 
       ss27-SS_40.1-0 ; 
       ss27-SS_41.1-0 ; 
       ss27-SS_42.1-0 ; 
       ss27-SS_43.1-0 ; 
       ss27-SS_44.1-0 ; 
       ss27-SS_45.1-0 ; 
       ss27-SS_46.1-0 ; 
       ss27-SS_6.1-0 ; 
       ss27-SS_7.1-0 ; 
       ss27-SS_8.1-0 ; 
       ss27-SS_9.1-0 ; 
       ss27-strobe_set.1-0 ; 
       ss27-strobe_set1.1-0 ; 
       ss27-turret.1-0 ; 
       ss27-turret2.1-0 ; 
       ss27-turret4.1-0 ; 
       ss27-turwepemt1.1-0 ; 
       ss27-turwepemt2.1-0 ; 
       ss27-turwepemt3.1-0 ; 
       ss27-turwepemt4.1-0 ; 
       ss27-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-incorporate_turrets.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       1 0 110 ; 
       2 7 110 ; 
       3 6 110 ; 
       4 2 110 ; 
       5 3 110 ; 
       6 8 110 ; 
       7 9 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 67 110 ; 
       11 67 110 ; 
       12 67 110 ; 
       13 68 110 ; 
       14 68 110 ; 
       15 68 110 ; 
       16 1 110 ; 
       18 76 110 ; 
       19 76 110 ; 
       20 76 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 18 110 ; 
       24 4 110 ; 
       25 10 110 ; 
       26 10 110 ; 
       27 10 110 ; 
       28 10 110 ; 
       29 10 110 ; 
       30 10 110 ; 
       31 11 110 ; 
       32 11 110 ; 
       33 11 110 ; 
       34 19 110 ; 
       35 11 110 ; 
       36 11 110 ; 
       37 11 110 ; 
       38 12 110 ; 
       39 12 110 ; 
       40 12 110 ; 
       41 12 110 ; 
       42 12 110 ; 
       43 12 110 ; 
       44 13 110 ; 
       45 20 110 ; 
       46 13 110 ; 
       47 13 110 ; 
       48 13 110 ; 
       49 13 110 ; 
       50 13 110 ; 
       51 14 110 ; 
       52 14 110 ; 
       53 14 110 ; 
       54 14 110 ; 
       55 14 110 ; 
       56 14 110 ; 
       57 15 110 ; 
       58 15 110 ; 
       59 15 110 ; 
       60 15 110 ; 
       61 15 110 ; 
       62 15 110 ; 
       63 16 110 ; 
       64 21 110 ; 
       65 22 110 ; 
       66 5 110 ; 
       67 3 110 ; 
       68 2 110 ; 
       69 3 110 ; 
       70 22 110 ; 
       71 2 110 ; 
       72 70 110 ; 
       73 69 110 ; 
       74 71 110 ; 
       75 21 110 ; 
       76 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 161.25 -8 0 MPRFLG 0 ; 
       2 SCHEM 42.5 -12 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 115 -12 0 MPRFLG 0 ; 
       4 SCHEM 13.75 -14 0 MPRFLG 0 ; 
       5 SCHEM 86.25 -14 0 MPRFLG 0 ; 
       6 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 45 -10 0 MPRFLG 0 ; 
       8 SCHEM 120 -8 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 101.25 -16 0 MPRFLG 0 ; 
       11 SCHEM 116.25 -16 0 MPRFLG 0 ; 
       12 SCHEM 131.25 -16 0 MPRFLG 0 ; 
       13 SCHEM 43.75 -16 0 MPRFLG 0 ; 
       14 SCHEM 28.75 -16 0 MPRFLG 0 ; 
       15 SCHEM 58.75 -16 0 MPRFLG 0 ; 
       16 SCHEM 158.75 -10 0 MPRFLG 0 ; 
       17 SCHEM 112.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 172.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 185 -10 0 MPRFLG 0 ; 
       20 SCHEM 197.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 5 -8 0 MPRFLG 0 ; 
       22 SCHEM 211.25 -8 0 MPRFLG 0 ; 
       23 SCHEM 167.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 12.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 107.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 105 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 95 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 97.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 100 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 102.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 122.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 110 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 112.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 180 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 115 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 117.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 120 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 137.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 125 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 127.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 130 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 132.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 135 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 50 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 192.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 37.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 40 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 42.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 45 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 47.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 35 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 22.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 25 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 27.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 30 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 32.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 65 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 52.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 55 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 57.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 60 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 62.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 157.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 2.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 210 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 85 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 116.25 -14 0 MPRFLG 0 ; 
       68 SCHEM 43.75 -14 0 MPRFLG 0 ; 
       69 SCHEM 91.25 -14 0 MPRFLG 0 ; 
       70 SCHEM 206.25 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 18.75 -14 0 MPRFLG 0 ; 
       72 SCHEM 205 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 90 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 17.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 185 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
