SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1tt.7-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 24     
       inside_bays-light1.6-0 ROOT ; 
       inside_bays-light10.4-0 ROOT ; 
       inside_bays-light11.3-0 ROOT ; 
       inside_bays-light12.3-0 ROOT ; 
       inside_bays-light2.5-0 ROOT ; 
       inside_bays-light3.5-0 ROOT ; 
       inside_bays-light4.5-0 ROOT ; 
       inside_bays-light5.5-0 ROOT ; 
       inside_bays-light6.5-0 ROOT ; 
       inside_bays-light7.5-0 ROOT ; 
       inside_bays-light8.5-0 ROOT ; 
       inside_bays-light9.5-0 ROOT ; 
       inside_bays-spot14.1-0 ; 
       inside_bays-spot14_int.5-0 ROOT ; 
       inside_bays-spot15.1-0 ; 
       inside_bays-spot15_int1.5-0 ROOT ; 
       inside_bays-spot16.1-0 ; 
       inside_bays-spot16_int1.5-0 ROOT ; 
       new_lighting-inf_light1_5.33-0 ROOT ; 
       new_lighting-inf_light2_5.33-0 ROOT ; 
       new_lighting-spot11.1-0 ; 
       new_lighting-spot11_int.4-0 ROOT ; 
       new_lighting-spot12.1-0 ; 
       new_lighting-spot12_int.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       new_lighting-east_bay_9.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-inside_bays.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       12 13 2110 ; 
       14 15 2110 ; 
       16 17 2110 ; 
       20 21 2110 ; 
       22 23 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 10 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 15 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 10 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       22 SCHEM 15 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       23 SCHEM 15 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 0 0 SRT 1 1 1 0 0 0 -5.915268e-013 0 7.633436e-007 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
