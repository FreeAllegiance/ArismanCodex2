SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.203-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       locL-light1.8-0 ROOT ; 
       locL-light2.8-0 ROOT ; 
       locL-light3.8-0 ROOT ; 
       locL-light4.8-0 ROOT ; 
       locL-light5.8-0 ROOT ; 
       locL-light6.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       locL-back.2-0 ; 
       locL-bay_inside_bottom1.2-0 ; 
       locL-bay_top1.2-0 ; 
       locL-front_edge1.1-0 ; 
       locL-mat1.1-0 ; 
       locL-mat2.1-0 ; 
       locL-mat5.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       locL-south_hull.5-0 ROOT ; 
       new_lighting-east_bay.1-0 ROOT ; 
       new_lighting-east_bay_antenna.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/rendermap1 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-locL.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       locL-rendermap1.1-0 ; 
       locL-rendermap3.4-0 ; 
       locL-rendermap4.4-0 ; 
       locL-rendermap5.4-0 ; 
       locL-t2d1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       1 6 300 ; 
       1 2 300 ; 
       1 1 300 ; 
       1 0 300 ; 
       1 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 0 401 ; 
       3 4 401 ; 
       2 1 401 ; 
       1 2 401 ; 
       0 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 0 0 SRT 1 1 0.9999999 0 0 0 0 0 -0.5968131 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 DISPLAY 1 2 SRT 1 1 1 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 0 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
