SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       garage-utl28a_2.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.32-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       garage-bay_strut_bridge.1-0 ROOT ; 
       garage-control_tower.2-0 ROOT ; 
       garage-fuselg1.1-0 ; 
       garage-fuselg1_1.1-0 ; 
       garage-fuselg1_2.1-0 ; 
       garage-main_antenna.1-0 ; 
       garage-south_hull.2-0 ROOT ; 
       garage-SSc1.1-0 ; 
       garage-SSc2.1-0 ; 
       garage-SSc3.1-0 ; 
       garage-SSc4.1-0 ; 
       garage-SSc5.1-0 ; 
       garage-SSc6.1-0 ; 
       garage-utl28a_2.1-0 ROOT ; 
       garage-west_bay.1-0 ; 
       garage-west_bay_antenna.1-0 ; 
       garage-west_bay_strut.1-0 ROOT ; 
       garage-west_large_sub-strut.3-0 ; 
       garage-west_small_sub-strut.1-0 ; 
       middle-bool1.6-0 ROOT ; 
       middle-circle1.2-0 ROOT ; 
       middle-circle2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-middle.17-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 13 110 ; 
       4 13 110 ; 
       18 16 110 ; 
       5 1 110 ; 
       17 16 110 ; 
       14 16 110 ; 
       15 14 110 ; 
       2 13 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 3 110 ; 
       12 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       13 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 31.25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 26.21514 1.278904 0 USR DISPLAY 0 0 SRT 0.5419999 0.5419999 0.5419999 0 0 0 0 -1.640358 0 MPRFLG 0 ; 
       21 SCHEM 29.2023 1.193644 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 40 1.278904 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 -0.5968131 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 21.25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 20 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 30 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 25 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 26.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 34 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
