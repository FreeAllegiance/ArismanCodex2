SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       middle-bool1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.34-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       middle-bay_strut_bridge.1-0 ; 
       middle-bool1.8-0 ROOT ; 
       middle-control_tower.2-0 ; 
       middle-fuselg1.1-0 ; 
       middle-fuselg1_1.1-0 ; 
       middle-fuselg1_2.1-0 ; 
       middle-main_antenna.1-0 ; 
       middle-south_hull.2-0 ; 
       middle-south_hull2.1-0 ; 
       middle-SSc1.1-0 ; 
       middle-SSc2.1-0 ; 
       middle-SSc3.1-0 ; 
       middle-SSc4.1-0 ; 
       middle-SSc5.1-0 ; 
       middle-SSc6.1-0 ; 
       middle-utl28a_2.1-0 ; 
       middle-west_bay.1-0 ; 
       middle-west_bay_5.1-0 ; 
       middle-west_bay_antenna.1-0 ; 
       middle-west_bay_antenna_1.1-0 ; 
       middle-west_bay_strut.1-0 ; 
       middle-west_bay_strut_1.1-0 ; 
       middle-west_large_sub-strut.1-0 ; 
       middle-west_large_sub-strut_1.3-0 ; 
       middle-west_small_sub-strut.1-0 ; 
       middle-west_small_sub-strut_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-middle.19-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       4 15 110 ; 
       5 15 110 ; 
       21 0 110 ; 
       24 20 110 ; 
       8 1 110 ; 
       22 21 110 ; 
       17 21 110 ; 
       19 17 110 ; 
       25 21 110 ; 
       7 1 110 ; 
       6 2 110 ; 
       23 20 110 ; 
       20 0 110 ; 
       16 20 110 ; 
       2 1 110 ; 
       18 16 110 ; 
       3 15 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 4 110 ; 
       14 4 110 ; 
       15 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 31.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 36.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 5 -4 0 MPRFLG 0 ; 
       24 SCHEM 15 -6 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       19 SCHEM 5 -8 0 MPRFLG 0 ; 
       25 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 10 -6 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 25 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 35 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 30 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 31.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 39 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
