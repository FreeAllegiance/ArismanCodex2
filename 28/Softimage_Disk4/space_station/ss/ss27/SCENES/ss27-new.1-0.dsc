SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl28a-utl28a_2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.2-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       garage-light1_1.2-0 ROOT ; 
       plan_surv_vesR6-light3_2_1_1_2.2-0 ROOT ; 
       pre_cargo_ship7-light1_2_1_1_2.2-0 ROOT ; 
       pre_cargo_ship7-light2_2_1_1_2.2-0 ROOT ; 
       StarBase-light2_2_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       ss21a-hullroot.2-0 ROOT ; 
       star_base_textured-mesh1.2-0 ROOT ; 
       star_base_textured-mesh2.2-0 ROOT ; 
       star_base_textured-null.2-0 ROOT ; 
       star_base_textured-polygon.1-0 ; 
       star_base_textured-polygon1.1-0 ; 
       StarBase-bool11.1-0 ; 
       StarBase-bool13.1-0 ; 
       StarBase-bool14_1.2-0 ROOT ; 
       StarBase-bool15_1.2-0 ROOT ; 
       StarBase-bool16_1.2-0 ROOT ; 
       StarBase-bool17_1.2-0 ROOT ; 
       StarBase-bool20.1-0 ; 
       StarBase-bool21.2-0 ROOT ; 
       StarBase-bool22.1-0 ; 
       StarBase-bool23.1-0 ; 
       StarBase-bool4.8-0 ; 
       StarBase-bool8.2-0 ROOT ; 
       StarBase-garage_mast.2-0 ROOT ; 
       StarBase-garage_mast1.2-0 ROOT ; 
       StarBase-garage_outside.2-0 ; 
       StarBase-garage_outside1.1-0 ; 
       StarBase-garage_outside2.1-0 ; 
       StarBase-garage_outside3.1-0 ; 
       StarBase-garage_root_1.2-0 ROOT ; 
       StarBase-garage_root1_1.2-0 ROOT ; 
       StarBase-garage_root2_1.2-0 ROOT ; 
       StarBase-garage_root3_1.2-0 ROOT ; 
       StarBase-main_body.2-0 ROOT ; 
       StarBase-main_body1.2-0 ROOT ; 
       StarBase-mesh_1.1-0 ; 
       StarBase-mesh_2.1-0 ; 
       StarBase-mesh_3.1-0 ; 
       StarBase-mesh_4.1-0 ; 
       StarBase-mesh_5.2-0 ROOT ; 
       StarBase-null1.2-0 ROOT ; 
       StarBase-null3_2.2-0 ROOT ; 
       StarBase-null4_1_1.2-0 ROOT ; 
       StarBase-null5.2-0 ROOT ; 
       StarBase-tetra1.2-0 ; 
       StarBase-tetra2.1-0 ; 
       StarBase-tetra3.1-0 ; 
       StarBase-tetra4.1-0 ; 
       textured_dummy2-main_body2.2-0 ROOT ; 
       utl28a-crgatt.1-0 ; 
       utl28a-ffuselg.1-0 ; 
       utl28a-fuselg1.1-0 ; 
       utl28a-fuselg2.1-0 ; 
       utl28a-fuselg3.1-0 ; 
       utl28a-SSc1.1-0 ; 
       utl28a-SSc2.1-0 ; 
       utl28a-SSc3.1-0 ; 
       utl28a-SSc4.1-0 ; 
       utl28a-SSc5.1-0 ; 
       utl28a-SSc6.1-0 ; 
       utl28a-utl28a_2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-new.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 3 110 ; 
       5 3 110 ; 
       6 21 110 ; 
       7 20 110 ; 
       12 16 110 ; 
       14 22 110 ; 
       15 23 110 ; 
       16 36 110 ; 
       20 24 110 ; 
       21 25 110 ; 
       22 26 110 ; 
       23 27 110 ; 
       30 23 110 ; 
       31 22 110 ; 
       32 20 110 ; 
       33 21 110 ; 
       39 24 110 ; 
       40 25 110 ; 
       41 26 110 ; 
       42 27 110 ; 
       44 45 110 ; 
       45 55 110 ; 
       46 55 110 ; 
       47 55 110 ; 
       48 55 110 ; 
       49 46 110 ; 
       50 46 110 ; 
       51 48 110 ; 
       52 48 110 ; 
       53 47 110 ; 
       54 47 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       55 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 12.66532 0.7466499 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 12.66532 -1.25335 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 12.66532 -3.253351 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 0 0 SRT 1 1 1 0 -0.5199996 3.141593 1.490116e-008 -2.941025 -0.5887883 MPRFLG 0 ; 
       1 SCHEM 555.5 0.7678118 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 575.5 0.7678118 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 558 0.7678118 0 SRT 1 1 1 0 0 0 12.26616 -0.3568467 1.056952 MPRFLG 0 ; 
       4 SCHEM 560.5 0.7678118 0 MPRFLG 0 ; 
       5 SCHEM 565.5 0.7678118 0 MPRFLG 0 ; 
       6 SCHEM 62.5 -11.70171 0 MPRFLG 0 ; 
       7 SCHEM 47.5 -11.70171 0 MPRFLG 0 ; 
       8 SCHEM 73.75 -7.701713 0 SRT 1 1 1 -0.8788456 -2.621615 -0.5348774 -0.00284267 8.066623 -0.01291743 MPRFLG 0 ; 
       9 SCHEM 76.25 -7.701713 0 SRT 1 1 1 0.8788456 2.621615 -0.5348774 -0.002842688 8.066623 0.01291743 MPRFLG 0 ; 
       10 SCHEM 78.75 -7.701713 0 SRT 1 1 1 -0.8788456 0.519978 0.5348774 0.002842669 8.066623 0.01291743 MPRFLG 0 ; 
       11 SCHEM 81.25 -7.701713 0 SRT 1 1 1 0.8788455 -0.519978 0.5348772 0.002842682 8.066623 -0.01291743 MPRFLG 0 ; 
       12 SCHEM 57.5 -5.701713 0 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 37.5 -5.701713 0 SRT 1 1 1 2.468668e-007 -3.141593 -0.5 -6.703097 2.436879 -9.533818e-005 MPRFLG 0 ; 
       14 SCHEM 30 -11.70171 0 MPRFLG 0 ; 
       15 SCHEM 20 -11.70171 0 MPRFLG 0 ; 
       16 SCHEM 57.5 -3.701712 0 MPRFLG 0 ; 
       17 SCHEM 47.5 -5.701713 0 SRT 1 1 1 -5.140944e-008 -1.109296e-008 0.5 6.668884 2.436879 -3.350085e-007 MPRFLG 0 ; 
       18 SCHEM 50 -5.701713 0 SRT 1 1 1 3.582776e-007 7.262646e-008 -0.2 12.55424 4.280047 1.314328e-006 MPRFLG 0 ; 
       19 SCHEM 40 -5.701713 0 SRT 1 1 1 -3.110308e-007 -3.141592 0.2 -12.58845 4.280047 -9.139701e-005 MPRFLG 0 ; 
       20 SCHEM 48.75 -9.701714 0 MPRFLG 0 ; 
       21 SCHEM 61.25 -9.701714 0 MPRFLG 0 ; 
       22 SCHEM 31.25 -9.701714 0 MPRFLG 0 ; 
       23 SCHEM 18.75 -9.701714 0 MPRFLG 0 ; 
       24 SCHEM 50 -7.701713 0 SRT 1 1 1 -6.357295e-008 -6.636296e-015 -5.932443e-007 20.12041 1.675979 -7.388192e-007 MPRFLG 0 ; 
       25 SCHEM 62.5 -7.701713 0 SRT 1 1 1 -6.357295e-008 -6.636296e-015 -5.932443e-007 12.32556 2.596223 -7.388197e-007 MPRFLG 0 ; 
       26 SCHEM 32.5 -7.701713 0 SRT 1 1 1 6.357282e-008 -3.141592 5.932443e-007 -12.35978 2.596223 -9.275184e-005 MPRFLG 0 ; 
       27 SCHEM 20 -7.701713 0 SRT 1 1 1 6.357282e-008 -3.141592 5.932443e-007 -20.15462 1.675979 -8.959941e-005 MPRFLG 0 ; 
       28 SCHEM 51.25 -3.701712 0 SRT 1 1 1 3.019916e-007 0 0 -0.0171088 2.056531 10.17154 MPRFLG 0 ; 
       29 SCHEM 41.25 -3.701712 0 SRT 1 1 1 -3.019916e-007 -3.141592 1.216414e-013 -0.0171088 2.056538 -10.17164 MPRFLG 0 ; 
       30 SCHEM 13.36407 -12.20991 0 USR MPRFLG 0 ; 
       31 SCHEM 35.49395 -15.09642 0 USR MPRFLG 0 ; 
       32 SCHEM 50 -11.70171 0 MPRFLG 0 ; 
       33 SCHEM 60 -11.70171 0 MPRFLG 0 ; 
       34 SCHEM 65.6227 -2.25157 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       35 SCHEM 56.25 -7.701713 0 SRT 1 1 1 -6.357295e-008 -6.636296e-015 -5.932443e-007 -5.032049 -2.282211 -6.523667e-007 MPRFLG 0 ; 
       36 SCHEM 47.5 -1.701712 0 SRT 1 1 1 -6.283185 0 0 -7.288244e-007 -2.446407e-008 -3.359717e-014 MPRFLG 0 ; 
       37 SCHEM 47.5 0.2982877 0 SRT 1 1 1 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
       38 SCHEM 26.25 -7.701713 0 SRT 1 1 1 6.357281e-008 -3.141592 5.932443e-007 4.997836 -2.282211 -9.897733e-005 MPRFLG 0 ; 
       39 SCHEM 52.5 -9.701714 0 MPRFLG 0 ; 
       40 SCHEM 65 -9.701714 0 MPRFLG 0 ; 
       41 SCHEM 35 -9.701714 0 MPRFLG 0 ; 
       42 SCHEM 22.5 -9.701714 0 MPRFLG 0 ; 
       43 SCHEM 550.5 0.7678118 0 SRT 1 1 1 -6.3573e-008 3.141592 -2.071471e-014 -0.0171088 2.056534 10.17164 MPRFLG 0 ; 
       44 SCHEM 22.65022 -0.9821882 0 MPRFLG 0 ; 
       45 SCHEM 19.15022 -0.9821882 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       46 SCHEM 19.56603 -11.15273 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 19.61801 -7.720141 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 19.20219 -3.982189 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 23.06603 -12.15273 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 23.06603 -10.15273 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 22.70219 -4.982189 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 22.70219 -2.982189 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 23.11801 -8.72014 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 23.11801 -6.720141 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 15.70219 -6.98219 0 USR SRT 1 1 1 -1.570796 0 0 0 -17.31209 -1.234713e-005 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 19.20219 0.7678118 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 41 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
