SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       rendermap-bool1.16-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.67-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 17     
       rendermap-light1.16-0 ROOT ; 
       rendermap-light10.10-0 ROOT ; 
       rendermap-light2.16-0 ROOT ; 
       rendermap-light3.16-0 ROOT ; 
       rendermap-light4.16-0 ROOT ; 
       rendermap-light5.16-0 ROOT ; 
       rendermap-light6.16-0 ROOT ; 
       rendermap-light8.11-0 ROOT ; 
       rendermap-light9.10-0 ROOT ; 
       rendermap-spot1.1-0 ; 
       rendermap-spot1_int.14-0 ROOT ; 
       rendermap-spot2.1-0 ; 
       rendermap-spot2_int.14-0 ROOT ; 
       rendermap-spot3.1-0 ; 
       rendermap-spot3_int.14-0 ROOT ; 
       rendermap-spot4.1-0 ; 
       rendermap-spot4_int.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       rendermap-bay_strut_bridge.1-0 ; 
       rendermap-bool1.10-0 ROOT ; 
       rendermap-control_tower.2-0 ; 
       rendermap-east_bay.1-0 ; 
       rendermap-east_bay_antenna.1-0 ; 
       rendermap-east_bay_strut.1-0 ; 
       rendermap-east_large_sub-strut.1-0 ; 
       rendermap-east_small_sub-strut.1-0 ; 
       rendermap-main_antenna.1-0 ; 
       rendermap-north_hull.1-0 ; 
       rendermap-south_block.1-0 ; 
       rendermap-south_block1.1-0 ; 
       rendermap-south_block2.1-0 ; 
       rendermap-south_hull.2-0 ; 
       rendermap-SSc1.1-0 ; 
       rendermap-SSc2.1-0 ; 
       rendermap-SSc3.1-0 ; 
       rendermap-SSc4.1-0 ; 
       rendermap-SSc5.1-0 ; 
       rendermap-SSc6.1-0 ; 
       rendermap-utl28a_2.1-0 ; 
       rendermap-west_bay.1-0 ; 
       rendermap-west_bay_antenna.1-0 ; 
       rendermap-west_bay_strut.1-0 ; 
       rendermap-west_large_sub-strut_1.3-0 ; 
       rendermap-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/lights ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/rendermap1 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/side ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-rendermap.16-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 20 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       13 1 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 1 110 ; 
       21 23 110 ; 
       22 21 110 ; 
       23 0 110 ; 
       24 23 110 ; 
       25 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       7 9 2111 ; 
       8 11 2111 ; 
       1 13 2111 ; 
       9 10 2110 ; 
       11 12 2110 ; 
       13 14 2110 ; 
       15 16 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 80 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 107.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 110 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 82.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 85 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 87.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 90 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 92.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 95 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 95 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 97.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 97.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 100 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 102.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 35 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 35 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 40 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 65 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 62.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 52.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 50 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 55 -2 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 10 -6 0 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
