SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl28a-utl28a_2.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.5-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 57     
       shell-mesh1.2-0 ; 
       shell-mesh2.2-0 ; 
       shell-null6.2-0 ROOT ; 
       ss21a-hullroot.2-0 ROOT ; 
       star_base_textured-null.2-0 ROOT ; 
       star_base_textured-polygon.1-0 ; 
       star_base_textured-polygon1.1-0 ; 
       StarBase-bool11.1-0 ; 
       StarBase-bool13.1-0 ; 
       StarBase-bool14_1.2-0 ROOT ; 
       StarBase-bool15_1.2-0 ROOT ; 
       StarBase-bool16_1.2-0 ROOT ; 
       StarBase-bool17_1.2-0 ROOT ; 
       StarBase-bool20.1-0 ; 
       StarBase-bool21.2-0 ROOT ; 
       StarBase-bool22.1-0 ; 
       StarBase-bool23.1-0 ; 
       StarBase-bool4.8-0 ; 
       StarBase-bool8.2-0 ROOT ; 
       StarBase-garage_mast.2-0 ROOT ; 
       StarBase-garage_mast1.2-0 ROOT ; 
       StarBase-garage_outside.2-0 ; 
       StarBase-garage_outside1.1-0 ; 
       StarBase-garage_outside2.1-0 ; 
       StarBase-garage_outside3.1-0 ; 
       StarBase-garage_root_1.2-0 ROOT ; 
       StarBase-garage_root1_1.2-0 ROOT ; 
       StarBase-garage_root2_1.2-0 ROOT ; 
       StarBase-garage_root3_1.2-0 ROOT ; 
       StarBase-main_body.2-0 ROOT ; 
       StarBase-main_body1.2-0 ROOT ; 
       StarBase-mesh_1.1-0 ; 
       StarBase-mesh_2.1-0 ; 
       StarBase-mesh_3.1-0 ; 
       StarBase-mesh_4.1-0 ; 
       StarBase-mesh_5.2-0 ROOT ; 
       StarBase-null1.2-0 ROOT ; 
       StarBase-null3_2.2-0 ROOT ; 
       StarBase-null4_1_1.2-0 ROOT ; 
       StarBase-null5.2-0 ROOT ; 
       StarBase-tetra1.2-0 ; 
       StarBase-tetra2.1-0 ; 
       StarBase-tetra3.1-0 ; 
       StarBase-tetra4.1-0 ; 
       textured_dummy2-main_body2.2-0 ROOT ; 
       utl28a-crgatt.1-0 ; 
       utl28a-ffuselg.1-0 ; 
       utl28a-fuselg1.1-0 ; 
       utl28a-fuselg2.1-0 ; 
       utl28a-fuselg3.1-0 ; 
       utl28a-SSc1.1-0 ; 
       utl28a-SSc2.1-0 ; 
       utl28a-SSc3.1-0 ; 
       utl28a-SSc4.1-0 ; 
       utl28a-SSc5.1-0 ; 
       utl28a-SSc6.1-0 ; 
       utl28a-utl28a_2.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-shell.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 22 110 ; 
       8 21 110 ; 
       13 17 110 ; 
       15 23 110 ; 
       16 24 110 ; 
       17 37 110 ; 
       21 25 110 ; 
       22 26 110 ; 
       23 27 110 ; 
       24 28 110 ; 
       31 24 110 ; 
       32 23 110 ; 
       33 21 110 ; 
       34 22 110 ; 
       40 25 110 ; 
       41 26 110 ; 
       42 27 110 ; 
       43 28 110 ; 
       45 46 110 ; 
       46 56 110 ; 
       47 56 110 ; 
       48 56 110 ; 
       49 56 110 ; 
       50 47 110 ; 
       51 47 110 ; 
       52 49 110 ; 
       53 49 110 ; 
       54 48 110 ; 
       55 48 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       56 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 3.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 -0.5199996 3.141593 1.490116e-008 -2.941025 -0.5887883 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 11.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 12.26616 -0.3568467 1.056952 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 45 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 35 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1 1 -0.8788456 -2.621615 -0.5348774 -0.00284267 8.066623 -0.01291743 MPRFLG 0 ; 
       10 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 1 1 1 0.8788456 2.621615 -0.5348774 -0.002842688 8.066623 0.01291743 MPRFLG 0 ; 
       11 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 -0.8788456 0.519978 0.5348774 0.002842669 8.066623 0.01291743 MPRFLG 0 ; 
       12 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1 1 1 0.8788455 -0.519978 0.5348772 0.002842682 8.066623 -0.01291743 MPRFLG 0 ; 
       13 SCHEM 79.62988 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 25 0 0 DISPLAY 0 0 SRT 1 1 1 2.468668e-007 -3.141593 -0.5 -6.703097 2.436879 -9.533818e-005 MPRFLG 0 ; 
       15 SCHEM 50 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 60.49395 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 79.62988 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 -5.140944e-008 -1.109296e-008 0.5 6.668884 2.436879 -3.350085e-007 MPRFLG 0 ; 
       19 SCHEM 30 0 0 DISPLAY 0 0 SRT 1 1 1 3.582776e-007 7.262646e-008 -0.2 12.55424 4.280047 1.314328e-006 MPRFLG 0 ; 
       20 SCHEM 32.5 0 0 DISPLAY 0 0 SRT 1 1 1 -3.110308e-007 -3.141592 0.2 -12.58845 4.280047 -9.139701e-005 MPRFLG 0 ; 
       21 SCHEM 36.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 43.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 51.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 63.37988 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 37.5 0 0 DISPLAY 0 0 SRT 1 1 1 -6.357295e-008 -6.636296e-015 -5.932443e-007 20.12041 1.675979 -7.388192e-007 MPRFLG 0 ; 
       26 SCHEM 45 0 0 DISPLAY 0 0 SRT 1 1 1 -6.357295e-008 -6.636296e-015 -5.932443e-007 12.32556 2.596223 -7.388197e-007 MPRFLG 0 ; 
       27 SCHEM 52.5 0 0 DISPLAY 0 0 SRT 1 1 1 6.357282e-008 -3.141592 5.932443e-007 -12.35978 2.596223 -9.275184e-005 MPRFLG 0 ; 
       28 SCHEM 64.62988 0 0 DISPLAY 0 0 SRT 1 1 1 6.357282e-008 -3.141592 5.932443e-007 -20.15462 1.675979 -8.959941e-005 MPRFLG 0 ; 
       29 SCHEM 69.62988 0 0 DISPLAY 0 0 SRT 1 1 1 3.019916e-007 0 0 -0.0171088 2.056531 10.17154 MPRFLG 0 ; 
       30 SCHEM 72.12988 0 0 DISPLAY 0 0 SRT 1 1 1 -3.019916e-007 -3.141592 1.216414e-013 -0.0171088 2.056538 -10.17164 MPRFLG 0 ; 
       31 SCHEM 57.99395 -4.508198 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 55.49395 -7.394707 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 37.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 42.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 74.62988 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       36 SCHEM 77.12988 0 0 DISPLAY 0 0 SRT 1 1 1 -6.357295e-008 -6.636296e-015 -5.932443e-007 -5.032049 -2.282211 -6.523667e-007 MPRFLG 0 ; 
       37 SCHEM 79.62988 0 0 DISPLAY 0 0 SRT 1 1 1 -6.283185 0 0 -7.288244e-007 -2.446407e-008 -3.359717e-014 MPRFLG 0 ; 
       38 SCHEM 82.12988 0 0 DISPLAY 0 0 SRT 1 1 1 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM 84.62988 0 0 DISPLAY 0 0 SRT 1 1 1 6.357281e-008 -3.141592 5.932443e-007 4.997836 -2.282211 -9.897733e-005 MPRFLG 0 ; 
       40 SCHEM 40 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 47.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 55 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 67.12988 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 87.12988 0 0 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 3.141592 -2.071471e-014 -0.0171088 2.056534 10.17164 MPRFLG 0 ; 
       45 SCHEM 89.62988 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 89.62988 -2 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 98.37988 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 103.3799 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 93.37988 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 99.62988 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 97.12988 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 94.62988 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 92.12988 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 104.6299 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 102.1299 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 97.12988 0 0 DISPLAY 0 0 SRT 1 1 1 -1.570796 0 0 0 -17.31209 -1.234713e-005 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 106.1299 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 41 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
