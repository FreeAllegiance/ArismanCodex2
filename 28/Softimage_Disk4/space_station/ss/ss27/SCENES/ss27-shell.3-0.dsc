SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl28a-utl28a_2.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.6-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       shell-mesh2.2-0 ; 
       shell-mesh5.1-0 ROOT ; 
       shell-null6.3-0 ROOT ; 
       ss21a-hullroot.2-0 ROOT ; 
       StarBase-bool14_1.2-0 ROOT ; 
       StarBase-bool15_1.2-0 ROOT ; 
       StarBase-bool16_1.2-0 ROOT ; 
       StarBase-bool17_1.2-0 ROOT ; 
       StarBase-bool20.1-0 ; 
       StarBase-bool21.2-0 ROOT ; 
       StarBase-bool4.8-0 ; 
       StarBase-bool8.2-0 ROOT ; 
       StarBase-garage_mast.2-0 ROOT ; 
       StarBase-garage_mast1.2-0 ROOT ; 
       StarBase-garage_outside3.1-0 ; 
       StarBase-garage_root3_1.3-0 ROOT ; 
       StarBase-main_body.2-0 ROOT ; 
       StarBase-main_body1.2-0 ROOT ; 
       StarBase-mesh_5.2-0 ROOT ; 
       StarBase-null1.2-0 ROOT ; 
       StarBase-null3_2.2-0 ROOT ; 
       StarBase-null4_1_1.2-0 ROOT ; 
       StarBase-null5.2-0 ROOT ; 
       StarBase-tetra4.1-0 ; 
       textured_dummy2-main_body2.2-0 ROOT ; 
       utl28a-crgatt.1-0 ; 
       utl28a-ffuselg.1-0 ; 
       utl28a-fuselg1.1-0 ; 
       utl28a-fuselg2.1-0 ; 
       utl28a-fuselg3.1-0 ; 
       utl28a-SSc1.1-0 ; 
       utl28a-SSc2.1-0 ; 
       utl28a-SSc3.1-0 ; 
       utl28a-SSc4.1-0 ; 
       utl28a-SSc5.1-0 ; 
       utl28a-SSc6.1-0 ; 
       utl28a-utl28a_2.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-shell.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       8 10 110 ; 
       10 20 110 ; 
       14 15 110 ; 
       23 15 110 ; 
       25 26 110 ; 
       26 36 110 ; 
       27 36 110 ; 
       28 36 110 ; 
       29 36 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 29 110 ; 
       33 29 110 ; 
       34 28 110 ; 
       35 28 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       36 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 1 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 1.960165 MPRFLG 0 ; 
       3 SCHEM 7.5 0 0 SRT 1 1 1 0 -0.5199996 3.141593 1.490116e-008 -2.941025 -0.5887883 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 0 0 SRT 1 1 1 -0.8788456 -2.621615 -0.5348774 -0.00284267 8.066623 -0.01291743 MPRFLG 0 ; 
       5 SCHEM 12.5 0 0 SRT 1 1 1 0.8788456 2.621615 -0.5348774 -0.002842688 8.066623 0.01291743 MPRFLG 0 ; 
       6 SCHEM 15 0 0 SRT 1 1 1 -0.8788456 0.519978 0.5348774 0.002842669 8.066623 0.01291743 MPRFLG 0 ; 
       7 SCHEM 17.5 0 0 SRT 1 1 1 0.8788455 -0.519978 0.5348772 0.002842682 8.066623 -0.01291743 MPRFLG 0 ; 
       8 SCHEM 45 -4 0 MPRFLG 0 ; 
       9 SCHEM 20 0 0 SRT 1 1 1 2.468668e-007 -3.141593 -0.5 -6.703097 2.436879 -9.533818e-005 MPRFLG 0 ; 
       10 SCHEM 45 -2 0 MPRFLG 0 ; 
       11 SCHEM 22.5 0 0 SRT 1 1 1 -5.140944e-008 -1.109296e-008 0.5 6.668884 2.436879 -3.350085e-007 MPRFLG 0 ; 
       12 SCHEM 25 0 0 SRT 1 1 1 3.582776e-007 7.262646e-008 -0.2 12.55424 4.280047 1.314328e-006 MPRFLG 0 ; 
       13 SCHEM 27.5 0 0 SRT 1 1 1 -3.110308e-007 -3.141592 0.2 -12.58845 4.280047 -9.139701e-005 MPRFLG 0 ; 
       14 SCHEM 30 -2 0 MPRFLG 0 ; 
       15 SCHEM 31.25 0 0 SRT 1 1 1 6.357282e-008 -3.141592 5.932443e-007 -20.15462 1.675979 -8.959941e-005 MPRFLG 0 ; 
       16 SCHEM 35 0 0 SRT 1 1 1 3.019916e-007 0 0 -0.0171088 2.056531 10.17154 MPRFLG 0 ; 
       17 SCHEM 37.5 0 0 SRT 1 1 1 -3.019916e-007 -3.141592 1.216414e-013 -0.0171088 2.056538 -10.17164 MPRFLG 0 ; 
       18 SCHEM 40 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 42.5 0 0 SRT 1 1 1 -6.357295e-008 -6.636296e-015 -5.932443e-007 -5.032049 -2.282211 -6.523667e-007 MPRFLG 0 ; 
       20 SCHEM 45 0 0 SRT 1 1 1 -6.283185 0 0 -7.288244e-007 -2.446407e-008 -3.359717e-014 MPRFLG 0 ; 
       21 SCHEM 47.5 0 0 SRT 1 1 1 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 50 0 0 SRT 1 1 1 6.357281e-008 -3.141592 5.932443e-007 4.997836 -2.282211 -9.897733e-005 MPRFLG 0 ; 
       23 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       24 SCHEM 52.5 0 0 SRT 1 1 1 -6.3573e-008 3.141592 -2.071471e-014 -0.0171088 2.056534 10.17164 MPRFLG 0 ; 
       25 SCHEM 55 -4 0 MPRFLG 0 ; 
       26 SCHEM 55 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       27 SCHEM 63.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 68.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 58.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 65 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 62.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 60 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 57.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 70 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 67.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 62.5 0 0 SRT 1 1 1 -1.570796 0 0 0 -17.31209 -1.234713e-005 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 41 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
