SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.350-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       edit_garage_frames-bool1_5_1.2-0 ; 
       edit_garage_frames-control_tower.2-0 ; 
       edit_garage_frames-east_bay_11.1-0 ; 
       edit_garage_frames-east_bay_12.1-0 ; 
       edit_garage_frames-east_bay_7.2-0 ROOT ; 
       edit_garage_frames-east_bay_antenna.1-0 ; 
       edit_garage_frames-east_bay_antenna1.1-0 ; 
       edit_garage_frames-east_bay_strut_1.1-0 ; 
       edit_garage_frames-east_bay_strut_2.1-0 ; 
       edit_garage_frames-east_bay_strut1.1-0 ; 
       edit_garage_frames-landing_lights.1-0 ; 
       edit_garage_frames-landing_lights1.1-0 ; 
       edit_garage_frames-landing_lights2.1-0 ; 
       edit_garage_frames-landing_lights6.1-0 ; 
       edit_garage_frames-landing_lights7.1-0 ; 
       edit_garage_frames-landing_lights8.1-0 ; 
       edit_garage_frames-root_1_3.1-0 ROOT ; 
       edit_garage_frames-south_block.1-0 ; 
       edit_garage_frames-south_block5.1-0 ; 
       edit_garage_frames-south_block6.1-0 ; 
       edit_garage_frames-south_hull_1.3-0 ; 
       edit_garage_frames-south_hull_2.1-0 ; 
       edit_garage_frames-strobe_set.1-0 ; 
       edit_garage_frames-strobe_set2.1-0 ; 
       edit_garage_frames-utl28a_2.1-0 ; 
       edit_garage_frames-west_bay_strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-STATIC.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 0 110 ; 
       2 7 110 ; 
       3 8 110 ; 
       5 8 110 ; 
       6 2 110 ; 
       7 9 110 ; 
       8 25 110 ; 
       9 0 110 ; 
       10 22 110 ; 
       11 22 110 ; 
       12 22 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       17 24 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 2 110 ; 
       23 8 110 ; 
       24 0 110 ; 
       25 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 30 -4 0 MPRFLG 0 ; 
       2 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 MPRFLG 0 ; 
       4 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 0.998 0.998 0.998 -1.570796 3.141592 0 8.339124e-008 -7.633436e-007 -9.779408e-008 MPRFLG 0 ; 
       5 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 20 -10 0 MPRFLG 0 ; 
       7 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       11 SCHEM 25 -12 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 15 -10 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 22.5 0 0 SRT 1 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 35 -6 0 MPRFLG 0 ; 
       19 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 5 -4 0 MPRFLG 0 ; 
       21 SCHEM 40 -4 0 MPRFLG 0 ; 
       22 SCHEM 25 -10 0 MPRFLG 0 ; 
       23 SCHEM 15 -8 0 MPRFLG 0 ; 
       24 SCHEM 35 -4 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
