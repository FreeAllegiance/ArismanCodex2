SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.80-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       test-light1.2-0 ROOT ; 
       test-light2.2-0 ROOT ; 
       test-light3.2-0 ROOT ; 
       test-light4.2-0 ROOT ; 
       test-light5.2-0 ROOT ; 
       test-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       test-south_hull1.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-test.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 0 0 SRT 1 1 0.9999999 0 0 0 0 0 -0.5968131 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
