SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       text-bool1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.49-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 12     
       text-light1.4-0 ROOT ; 
       text-light2.4-0 ROOT ; 
       text-light3.4-0 ROOT ; 
       text-light4.4-0 ROOT ; 
       text-light5.4-0 ROOT ; 
       text-light6.4-0 ROOT ; 
       text-spot1.1-0 ; 
       text-spot1_int1.1-0 ROOT ; 
       text-spot2.1-0 ; 
       text-spot2_int1.1-0 ROOT ; 
       text-spot3.1-0 ; 
       text-spot3_int1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       text-bay_strut_bridge.1-0 ; 
       text-bool1.3-0 ROOT ; 
       text-control_tower.2-0 ; 
       text-east_bay.1-0 ; 
       text-east_bay_antenna.1-0 ; 
       text-east_bay_strut.1-0 ; 
       text-east_large_sub-strut.1-0 ; 
       text-east_small_sub-strut.1-0 ; 
       text-main_antenna.1-0 ; 
       text-north_hull.1-0 ; 
       text-south_block.1-0 ; 
       text-south_block1.1-0 ; 
       text-south_block2.1-0 ; 
       text-south_hull.2-0 ; 
       text-SSc1.1-0 ; 
       text-SSc2.1-0 ; 
       text-SSc3.1-0 ; 
       text-SSc4.1-0 ; 
       text-SSc5.1-0 ; 
       text-SSc6.1-0 ; 
       text-utl28a_2.1-0 ; 
       text-west_bay.1-0 ; 
       text-west_bay_antenna.1-0 ; 
       text-west_bay_strut.1-0 ; 
       text-west_large_sub-strut_1.3-0 ; 
       text-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-text.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       5 0 110 ; 
       25 23 110 ; 
       9 1 110 ; 
       6 5 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       7 5 110 ; 
       13 1 110 ; 
       8 2 110 ; 
       24 23 110 ; 
       23 0 110 ; 
       21 23 110 ; 
       2 1 110 ; 
       22 21 110 ; 
       10 20 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       6 7 2110 ; 
       8 9 2110 ; 
       10 11 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 62.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 65 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 67.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 70 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 72.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 75 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 80 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 82.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 85 -2 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 85 0 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 31.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 20 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 10 -6 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 50 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 40 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 37.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 42.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
