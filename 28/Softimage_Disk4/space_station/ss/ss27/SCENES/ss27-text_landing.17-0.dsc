SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       text_landing-bool1.17-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.144-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 21     
       text_landing-light10.17-0 ROOT ; 
       text_landing-light8.17-0 ROOT ; 
       text_landing-light9.17-0 ROOT ; 
       text_landing-spot1.1-0 ; 
       text_landing-spot1_int.17-0 ROOT ; 
       text_landing-spot2.1-0 ; 
       text_landing-spot2_int.17-0 ROOT ; 
       text_landing-spot3.1-0 ; 
       text_landing-spot3_int.17-0 ROOT ; 
       text_landing-spot4.1-0 ; 
       text_landing-spot4_int.17-0 ROOT ; 
       text_landing-spot5.1-0 ; 
       text_landing-spot5_int.17-0 ROOT ; 
       text_landing-spot6.1-0 ; 
       text_landing-spot6_int.16-0 ROOT ; 
       text_landing-spot7.1-0 ; 
       text_landing-spot7_int.15-0 ROOT ; 
       text_landing-spot8.1-0 ; 
       text_landing-spot8_int1.15-0 ROOT ; 
       text_landing-spot9.1-0 ; 
       text_landing-spot9_int1.15-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       text_landing-bay_strut_bridge.1-0 ; 
       text_landing-bool1.14-0 ROOT ; 
       text_landing-control_tower.2-0 ; 
       text_landing-east_bay.1-0 ; 
       text_landing-east_bay_antenna.1-0 ; 
       text_landing-east_bay_strut.1-0 ; 
       text_landing-east_large_sub-strut.1-0 ; 
       text_landing-east_small_sub-strut.1-0 ; 
       text_landing-main_antenna.1-0 ; 
       text_landing-north_hull.1-0 ; 
       text_landing-south_block.1-0 ; 
       text_landing-south_block1.1-0 ; 
       text_landing-south_block2.1-0 ; 
       text_landing-south_hull.2-0 ; 
       text_landing-SSc1.1-0 ; 
       text_landing-SSc2.1-0 ; 
       text_landing-SSc3.1-0 ; 
       text_landing-SSc4.1-0 ; 
       text_landing-SSc5.1-0 ; 
       text_landing-SSc6.1-0 ; 
       text_landing-utl28a_2.1-0 ; 
       text_landing-west_bay.1-0 ; 
       text_landing-west_bay_antenna.1-0 ; 
       text_landing-west_bay_strut.1-0 ; 
       text_landing-west_large_sub-strut_1.3-0 ; 
       text_landing-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 10     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/black ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/landing-area ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/lights ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/logo ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/rendermap1 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/side ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/single_light ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/solar ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/text ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-text_landing.17-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 20 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       13 1 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 1 110 ; 
       21 23 110 ; 
       22 21 110 ; 
       23 0 110 ; 
       24 23 110 ; 
       25 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 7 2111 ; 
       1 3 2111 ; 
       2 5 2111 ; 
       3 4 2110 ; 
       5 6 2110 ; 
       7 8 2110 ; 
       9 10 2110 ; 
       11 12 2110 ; 
       13 14 2110 ; 
       15 16 2110 ; 
       17 18 2110 ; 
       19 20 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 0 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 0 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 0 -30 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 0 -36 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 0 -34 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 0 -40 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 0 -38 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 0 -44 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 0 -42 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 0 -48 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 0 -46 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 0 -52 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 0 -50 0 WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 0 -56 0 WIRECOL 7 7 MPRFLG 0 ; 
       20 SCHEM 0 -54 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 51.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 65 -6 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 0 -10 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 65 -8 0 MPRFLG 0 ; 
       9 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 51.25 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 70 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 95 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 92.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 82.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 80 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 85 -6 0 MPRFLG 0 ; 
       21 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 30 -10 0 MPRFLG 0 ; 
       25 SCHEM 35 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 104 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
