SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       top_map-bool1.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.87-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 19     
       top_map-light1.12-0 ROOT ; 
       top_map-light10.12-0 ROOT ; 
       top_map-light2.12-0 ROOT ; 
       top_map-light3.12-0 ROOT ; 
       top_map-light4.12-0 ROOT ; 
       top_map-light5.12-0 ROOT ; 
       top_map-light6.12-0 ROOT ; 
       top_map-light8.12-0 ROOT ; 
       top_map-light9.12-0 ROOT ; 
       top_map-spot1.1-0 ; 
       top_map-spot1_int.12-0 ROOT ; 
       top_map-spot2.1-0 ; 
       top_map-spot2_int.12-0 ROOT ; 
       top_map-spot3.1-0 ; 
       top_map-spot3_int.12-0 ROOT ; 
       top_map-spot4.1-0 ; 
       top_map-spot4_int.12-0 ROOT ; 
       top_map-spot5.1-0 ; 
       top_map-spot5_int.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       top_map-bay_strut_bridge.1-0 ; 
       top_map-bool1.10-0 ROOT ; 
       top_map-control_tower.2-0 ; 
       top_map-east_bay.1-0 ; 
       top_map-east_bay_antenna.1-0 ; 
       top_map-east_bay_strut.1-0 ; 
       top_map-east_large_sub-strut.1-0 ; 
       top_map-east_small_sub-strut.1-0 ; 
       top_map-main_antenna.1-0 ; 
       top_map-north_hull.1-0 ; 
       top_map-south_block.1-0 ; 
       top_map-south_block1.1-0 ; 
       top_map-south_block2.1-0 ; 
       top_map-south_hull.2-0 ; 
       top_map-SSc1.1-0 ; 
       top_map-SSc2.1-0 ; 
       top_map-SSc3.1-0 ; 
       top_map-SSc4.1-0 ; 
       top_map-SSc5.1-0 ; 
       top_map-SSc6.1-0 ; 
       top_map-utl28a_2.1-0 ; 
       top_map-west_bay.1-0 ; 
       top_map-west_bay_antenna.1-0 ; 
       top_map-west_bay_strut.1-0 ; 
       top_map-west_large_sub-strut_1.3-0 ; 
       top_map-west_small_sub-strut.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/rendermap1 ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/side ; 
       E:/Pete_Data2/space_station/ss/ss27/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-top_map.12-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 20 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       13 1 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 1 110 ; 
       21 23 110 ; 
       22 21 110 ; 
       23 0 110 ; 
       24 23 110 ; 
       25 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       1 13 2111 ; 
       7 9 2111 ; 
       8 11 2111 ; 
       9 10 2110 ; 
       11 12 2110 ; 
       13 14 2110 ; 
       15 16 2110 ; 
       17 18 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -16 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -16 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 0 -18 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -18 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 0 -20 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 0 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 0 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 0 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 0 -30 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 2.5 -32 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -30 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 31.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 0 -10 0 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 MPRFLG 0 ; 
       8 SCHEM 25 -8 0 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 30 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 55 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 40 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 45 -6 0 MPRFLG 0 ; 
       21 SCHEM 10 -10 0 MPRFLG 0 ; 
       22 SCHEM 10 -12 0 MPRFLG 0 ; 
       23 SCHEM 10 -8 0 MPRFLG 0 ; 
       24 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 64 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
