SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.152-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_colors-light1.3-0 ROOT ; 
       add_colors-light2.3-0 ROOT ; 
       add_colors-light3.3-0 ROOT ; 
       add_colors-light4.3-0 ROOT ; 
       add_colors-light5.3-0 ROOT ; 
       add_colors-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       add_colors-COLORED_BAY.1-0 ; 
       add_colors-cube1.2-0 ROOT ; 
       add_colors-cube2.1-0 ; 
       add_colors-cube3.1-0 ; 
       add_colors-cube4.3-0 ; 
       add_colors-cube5.1-0 ; 
       add_colors-cyl1.1-0 ; 
       add_colors-east_bay_antenna1.1-0 ; 
       add_colors-east_bay_strut_2.1-0 ; 
       add_colors-east_bay_strut2.2-0 ; 
       add_colors-garage1A.1-0 ; 
       add_colors-garage1B.1-0 ; 
       add_colors-garage1C.1-0 ; 
       add_colors-garage1D.1-0 ; 
       add_colors-garage1E.1-0 ; 
       add_colors-landing_lights1.1-0 ; 
       add_colors-landing_lights2.1-0 ; 
       add_colors-launch.1-0 ; 
       add_colors-null1.2-0 ; 
       add_colors-south_block.1-0 ; 
       add_colors-south_block5.1-0 ; 
       add_colors-south_block6.1-0 ; 
       add_colors-south_hull_1.3-0 ; 
       add_colors-south_hull_3.1-0 ; 
       add_colors-SS_01.1-0 ; 
       add_colors-SS_17.1-0 ; 
       add_colors-SS_18.1-0 ; 
       add_colors-SS_19.1-0 ; 
       add_colors-SS_2.1-0 ; 
       add_colors-SS_20.1-0 ; 
       add_colors-SS_21.1-0 ; 
       add_colors-SS_22.1-0 ; 
       add_colors-SS_23.1-0 ; 
       add_colors-SS_24.1-0 ; 
       add_colors-SS_25.1-0 ; 
       add_colors-SS_26.1-0 ; 
       add_colors-SS_27.1-0 ; 
       add_colors-SS_28.1-0 ; 
       add_colors-SS_3.1-0 ; 
       add_colors-SS_7.1-0 ; 
       add_colors-SS_9.1-0 ; 
       add_colors-strobe_set.1-0 ; 
       add_colors-turwepemt2.1-0 ; 
       add_colors-turwepemt4.1-0 ; 
       add_colors-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_ss28-add_colors.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 0 110 ; 
       8 9 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 1 110 ; 
       14 1 110 ; 
       15 41 110 ; 
       16 41 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 44 110 ; 
       20 44 110 ; 
       21 44 110 ; 
       22 4 110 ; 
       23 5 110 ; 
       24 19 110 ; 
       25 15 110 ; 
       26 15 110 ; 
       27 15 110 ; 
       28 20 110 ; 
       29 15 110 ; 
       30 15 110 ; 
       31 15 110 ; 
       32 16 110 ; 
       33 16 110 ; 
       34 16 110 ; 
       35 16 110 ; 
       36 16 110 ; 
       37 16 110 ; 
       38 21 110 ; 
       39 22 110 ; 
       40 7 110 ; 
       41 0 110 ; 
       42 0 110 ; 
       43 22 110 ; 
       44 6 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -17.80511 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -17.80511 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -17.80511 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -17.80511 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -17.80511 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -19.80511 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 26.09555 -9.805111 0 USR MPRFLG 0 ; 
       1 SCHEM 33.75 -4 0 SRT 1 1 1 1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -8 0 MPRFLG 0 ; 
       4 SCHEM 1.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 45 -6 0 MPRFLG 0 ; 
       6 SCHEM 50 -6 0 MPRFLG 0 ; 
       7 SCHEM 9.845551 -11.80511 0 MPRFLG 0 ; 
       8 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 55 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 60 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 65 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 21.09555 -13.80511 0 MPRFLG 0 ; 
       16 SCHEM 36.09555 -13.80511 0 MPRFLG 0 ; 
       17 SCHEM 67.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 50 -10 0 MPRFLG 0 ; 
       21 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 1.25 -8 0 MPRFLG 0 ; 
       23 SCHEM 45 -8 0 MPRFLG 0 ; 
       24 SCHEM 47.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 27.34555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 14.84555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 17.34555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 50 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 19.84555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 22.34555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 24.84555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 42.34555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 29.84555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 32.34555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 34.84555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 37.34555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 39.84555 -15.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 52.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 2.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 9.845551 -13.80511 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 28.59555 -11.80511 0 MPRFLG 0 ; 
       42 SCHEM 12.34555 -11.80511 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 50 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
