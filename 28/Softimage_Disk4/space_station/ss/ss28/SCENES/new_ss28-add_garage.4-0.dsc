SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.144-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_garage-light1.4-0 ROOT ; 
       add_garage-light2.4-0 ROOT ; 
       add_garage-light3.4-0 ROOT ; 
       add_garage-light4.4-0 ROOT ; 
       add_garage-light5.4-0 ROOT ; 
       add_garage-light6.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       add_garage-cube1.3-0 ROOT ; 
       add_garage-cube2.1-0 ; 
       add_garage-cube3.1-0 ; 
       add_garage-cube4.3-0 ; 
       add_garage-cube5.1-0 ; 
       add_garage-cyl1.1-0 ; 
       add_garage-east_bay_11.5-0 ; 
       add_garage-east_bay_antenna1.1-0 ; 
       add_garage-east_bay_strut_2.1-0 ; 
       add_garage-east_bay_strut2.2-0 ; 
       add_garage-garage1A.1-0 ; 
       add_garage-garage1B.1-0 ; 
       add_garage-garage1C.1-0 ; 
       add_garage-garage1D.1-0 ; 
       add_garage-garage1E.1-0 ; 
       add_garage-landing_lights1.1-0 ; 
       add_garage-landing_lights2.1-0 ; 
       add_garage-launch.1-0 ; 
       add_garage-null1.2-0 ; 
       add_garage-south_block.1-0 ; 
       add_garage-south_block5.1-0 ; 
       add_garage-south_block6.1-0 ; 
       add_garage-south_hull_1.3-0 ; 
       add_garage-south_hull_3.1-0 ; 
       add_garage-SS_01.1-0 ; 
       add_garage-SS_17.1-0 ; 
       add_garage-SS_18.1-0 ; 
       add_garage-SS_19.1-0 ; 
       add_garage-SS_2.1-0 ; 
       add_garage-SS_20.1-0 ; 
       add_garage-SS_21.1-0 ; 
       add_garage-SS_22.1-0 ; 
       add_garage-SS_23.1-0 ; 
       add_garage-SS_24.1-0 ; 
       add_garage-SS_25.1-0 ; 
       add_garage-SS_26.1-0 ; 
       add_garage-SS_27.1-0 ; 
       add_garage-SS_28.1-0 ; 
       add_garage-SS_3.1-0 ; 
       add_garage-SS_7.1-0 ; 
       add_garage-SS_9.1-0 ; 
       add_garage-strobe_set.1-0 ; 
       add_garage-turwepemt2.1-0 ; 
       add_garage-turwepemt4.1-0 ; 
       add_garage-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_ss28-add_garage.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       17 0 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 0 110 ; 
       15 41 110 ; 
       16 41 110 ; 
       18 0 110 ; 
       19 44 110 ; 
       20 44 110 ; 
       21 44 110 ; 
       22 3 110 ; 
       23 4 110 ; 
       24 19 110 ; 
       25 15 110 ; 
       26 15 110 ; 
       27 15 110 ; 
       28 20 110 ; 
       29 15 110 ; 
       30 15 110 ; 
       31 15 110 ; 
       32 16 110 ; 
       33 16 110 ; 
       34 16 110 ; 
       35 16 110 ; 
       36 16 110 ; 
       37 16 110 ; 
       38 21 110 ; 
       39 22 110 ; 
       40 7 110 ; 
       41 6 110 ; 
       42 6 110 ; 
       43 22 110 ; 
       44 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 57.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 60 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 62.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 65 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 67.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 70 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 36.25 0 0 DISPLAY 3 2 SRT 1 1 1 1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 3.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 47.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 52.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 26.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 10 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 26.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 26.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 21.25 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 36.25 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 45 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 50 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 52.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 55 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 3.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 47.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 50 -8 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 27.5 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 15 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 17.5 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 52.5 -8 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 20 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 22.5 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 25 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 42.5 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 30 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 32.5 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 35 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 37.5 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 40 -12 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 55 -8 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 5 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 10 -10 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 28.75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 12.5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 2.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
