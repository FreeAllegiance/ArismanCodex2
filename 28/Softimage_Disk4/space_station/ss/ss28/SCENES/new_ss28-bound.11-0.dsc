SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.164-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       bounding_model-bound1.3-0 ; 
       bounding_model-bound10.1-0 ; 
       bounding_model-bound11.1-0 ; 
       bounding_model-bound12.1-0 ; 
       bounding_model-bound13.5-0 ; 
       bounding_model-bound2.1-0 ; 
       bounding_model-bound3.1-0 ; 
       bounding_model-bound4.1-0 ; 
       bounding_model-bound5.1-0 ; 
       bounding_model-bound6.1-0 ; 
       bounding_model-bound7.1-0 ; 
       bounding_model-bound8.1-0 ; 
       bounding_model-bound9.1-0 ; 
       bounding_model-bounding_model.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_ss28-bound.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       10 13 110 ; 
       11 13 110 ; 
       12 13 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 10 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 20 -2 0 MPRFLG 0 ; 
       12 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
