SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.132-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       new-light1.20-0 ROOT ; 
       new-light2.20-0 ROOT ; 
       new-light3.20-0 ROOT ; 
       new-light4.20-0 ROOT ; 
       new-light5.20-0 ROOT ; 
       new-light6.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       new-cube1.13-0 ROOT ; 
       new-cube2.1-0 ; 
       new-cube3.1-0 ; 
       new-cube4.3-0 ; 
       new-cube5.1-0 ; 
       new-cyl1.1-0 ; 
       new-east_bay_11.5-0 ; 
       new-east_bay_antenna1.1-0 ; 
       new-east_bay_strut_2.1-0 ; 
       new-east_bay_strut2.2-0 ; 
       new-landing_lights1.1-0 ; 
       new-landing_lights2.1-0 ; 
       new-null1.2-0 ; 
       new-south_block.1-0 ; 
       new-south_block5.1-0 ; 
       new-south_block6.1-0 ; 
       new-south_hull_1.3-0 ; 
       new-south_hull_3.1-0 ; 
       new-SS_01.1-0 ; 
       new-SS_17.1-0 ; 
       new-SS_18.1-0 ; 
       new-SS_19.1-0 ; 
       new-SS_2.1-0 ; 
       new-SS_20.1-0 ; 
       new-SS_21.1-0 ; 
       new-SS_22.1-0 ; 
       new-SS_23.1-0 ; 
       new-SS_24.1-0 ; 
       new-SS_25.1-0 ; 
       new-SS_26.1-0 ; 
       new-SS_27.1-0 ; 
       new-SS_28.1-0 ; 
       new-SS_3.1-0 ; 
       new-SS_7.1-0 ; 
       new-SS_9.1-0 ; 
       new-strobe_set.1-0 ; 
       new-turwepemt2.1-0 ; 
       new-turwepemt4.1-0 ; 
       new-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_ss28-new.20-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 0 110 ; 
       4 0 110 ; 
       17 4 110 ; 
       5 0 110 ; 
       13 38 110 ; 
       14 38 110 ; 
       16 3 110 ; 
       15 38 110 ; 
       18 13 110 ; 
       22 14 110 ; 
       32 15 110 ; 
       33 16 110 ; 
       37 16 110 ; 
       38 5 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 0 110 ; 
       10 35 110 ; 
       11 35 110 ; 
       12 0 110 ; 
       19 10 110 ; 
       20 10 110 ; 
       21 10 110 ; 
       23 10 110 ; 
       24 10 110 ; 
       25 10 110 ; 
       26 11 110 ; 
       27 11 110 ; 
       28 11 110 ; 
       29 11 110 ; 
       30 11 110 ; 
       31 11 110 ; 
       34 7 110 ; 
       35 6 110 ; 
       36 6 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 83.75 -6 0 MPRFLG 0 ; 
       17 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 108.75 -6 0 MPRFLG 0 ; 
       13 SCHEM 95 -10 0 MPRFLG 0 ; 
       14 SCHEM 107.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 5 -8 0 MPRFLG 0 ; 
       15 SCHEM 120 -10 0 MPRFLG 0 ; 
       18 SCHEM 90 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 102.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 115 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 2.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 107.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 65 -4 0 SRT 1 1 1 3.141593 0 0 0 3.096874 9.345922 MPRFLG 0 ; 
       1 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 21.25 -12 0 MPRFLG 0 ; 
       8 SCHEM 45 -8 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 33.75 -14 0 MPRFLG 0 ; 
       11 SCHEM 48.75 -14 0 MPRFLG 0 ; 
       12 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 40 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 30 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 35 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 55 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 42.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 45 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 47.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 50 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 52.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 20 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 41.25 -12 0 MPRFLG 0 ; 
       36 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
