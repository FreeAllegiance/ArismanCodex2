SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.153-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       static-light1.1-0 ROOT ; 
       static-light2.1-0 ROOT ; 
       static-light3.1-0 ROOT ; 
       static-light4.1-0 ROOT ; 
       static-light5.1-0 ROOT ; 
       static-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       static-COLORED_BAY.1-0 ; 
       static-cube1.1-0 ROOT ; 
       static-cube2.1-0 ; 
       static-cube3.1-0 ; 
       static-cube4.3-0 ; 
       static-cube5.1-0 ; 
       static-cyl1.1-0 ; 
       static-east_bay_strut_2.1-0 ; 
       static-east_bay_strut2.2-0 ; 
       static-south_block.1-0 ; 
       static-south_block5.1-0 ; 
       static-south_block6.1-0 ; 
       static-south_hull_1.3-0 ; 
       static-south_hull_3.1-0 ; 
       static-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_ss28-static.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 8 110 ; 
       8 1 110 ; 
       9 14 110 ; 
       10 14 110 ; 
       11 14 110 ; 
       12 4 110 ; 
       13 5 110 ; 
       14 6 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 9.845551 -5.805111 0 USR MPRFLG 0 ; 
       1 SCHEM 10 0 0 SRT 1 1 1 1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 10 -4 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
