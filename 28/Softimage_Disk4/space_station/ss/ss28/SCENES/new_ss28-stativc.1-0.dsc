SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.145-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       stativc-light1.1-0 ROOT ; 
       stativc-light2.1-0 ROOT ; 
       stativc-light3.1-0 ROOT ; 
       stativc-light4.1-0 ROOT ; 
       stativc-light5.1-0 ROOT ; 
       stativc-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       stativc-cube1.1-0 ROOT ; 
       stativc-cube2.1-0 ; 
       stativc-cube3.1-0 ; 
       stativc-cube4.3-0 ; 
       stativc-cube5.1-0 ; 
       stativc-cyl1.1-0 ; 
       stativc-east_bay_11.5-0 ; 
       stativc-east_bay_antenna1.1-0 ; 
       stativc-east_bay_strut_2.1-0 ; 
       stativc-east_bay_strut2.2-0 ; 
       stativc-landing_lights1.1-0 ; 
       stativc-landing_lights2.1-0 ; 
       stativc-null1.2-0 ; 
       stativc-south_block.1-0 ; 
       stativc-south_block5.1-0 ; 
       stativc-south_block6.1-0 ; 
       stativc-south_hull_1.3-0 ; 
       stativc-south_hull_3.1-0 ; 
       stativc-strobe_set.1-0 ; 
       stativc-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_ss28-stativc.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 0 110 ; 
       10 18 110 ; 
       11 18 110 ; 
       12 0 110 ; 
       13 19 110 ; 
       14 19 110 ; 
       15 19 110 ; 
       16 3 110 ; 
       17 4 110 ; 
       18 6 110 ; 
       19 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 0 0 SRT 1 1 1 1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 MPRFLG 0 ; 
       10 SCHEM 10 -10 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
