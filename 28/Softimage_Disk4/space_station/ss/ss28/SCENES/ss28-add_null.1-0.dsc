SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.99-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       add_null-antenn.1-0 ; 
       add_null-bwingzz.1-0 ; 
       add_null-gargout.1-0 ; 
       add_null-gargrt.1-0 ; 
       add_null-hullroot_1.1-0 ; 
       add_null-hull_1.17-0 ; 
       add_null-south_block.1-0 ; 
       add_null-SS_01.1-0 ; 
       add_null-tetra1.1-0 ROOT ; 
       add_null-turret2.1-0 ; 
       add_null-turret3.1-0 ; 
       add_null-turwepemt1.1-0 ; 
       add_null-turwepemt2.1-0 ; 
       add_null-twingzz.20-0 ; 
       add_null-wlkway.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss28-add_null.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 13 110 ; 
       2 3 110 ; 
       3 13 110 ; 
       4 5 110 ; 
       5 8 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       9 2 110 ; 
       10 5 110 ; 
       11 9 110 ; 
       12 10 110 ; 
       13 5 110 ; 
       14 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 5 -10 0 MPRFLG 0 ; 
       8 SCHEM 8.75 0 0 SRT 1 1 1 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
