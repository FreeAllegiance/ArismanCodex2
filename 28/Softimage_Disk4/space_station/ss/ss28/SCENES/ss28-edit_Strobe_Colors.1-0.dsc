SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.105-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       edit_Strobe_Colors-antenn.1-0 ; 
       edit_Strobe_Colors-bwingzz.1-0 ; 
       edit_Strobe_Colors-gargout.1-0 ; 
       edit_Strobe_Colors-gargrt.1-0 ; 
       edit_Strobe_Colors-hullroot_1.1-0 ; 
       edit_Strobe_Colors-hull_1.17-0 ; 
       edit_Strobe_Colors-null1.1-0 ; 
       edit_Strobe_Colors-south_block.1-0 ; 
       edit_Strobe_Colors-SS_01.1-0 ; 
       edit_Strobe_Colors-SS_10.1-0 ; 
       edit_Strobe_Colors-SS_11.1-0 ; 
       edit_Strobe_Colors-SS_12.1-0 ; 
       edit_Strobe_Colors-SS_13.1-0 ; 
       edit_Strobe_Colors-SS_14.1-0 ; 
       edit_Strobe_Colors-SS_15.1-0 ; 
       edit_Strobe_Colors-SS_16.1-0 ; 
       edit_Strobe_Colors-SS_17.1-0 ; 
       edit_Strobe_Colors-SS_18.1-0 ; 
       edit_Strobe_Colors-SS_19.1-0 ; 
       edit_Strobe_Colors-SS_20.1-0 ; 
       edit_Strobe_Colors-SS_8.1-0 ; 
       edit_Strobe_Colors-SS_9.1-0 ; 
       edit_Strobe_Colors-tetra1.1-0 ROOT ; 
       edit_Strobe_Colors-turret2.1-0 ; 
       edit_Strobe_Colors-turret3.1-0 ; 
       edit_Strobe_Colors-turwepemt1.1-0 ; 
       edit_Strobe_Colors-turwepemt2.1-0 ; 
       edit_Strobe_Colors-twingzz.20-0 ; 
       edit_Strobe_Colors-wlkway.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss28-edit_Strobe_Colors.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 27 110 ; 
       2 3 110 ; 
       3 27 110 ; 
       4 5 110 ; 
       5 22 110 ; 
       6 2 110 ; 
       7 4 110 ; 
       8 7 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 6 110 ; 
       20 7 110 ; 
       21 5 110 ; 
       23 2 110 ; 
       24 5 110 ; 
       25 23 110 ; 
       26 24 110 ; 
       27 5 110 ; 
       28 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 35 -12 0 MPRFLG 0 ; 
       1 SCHEM 40 -10 0 MPRFLG 0 ; 
       2 SCHEM 16.25 -12 0 MPRFLG 0 ; 
       3 SCHEM 18.75 -10 0 MPRFLG 0 ; 
       4 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       5 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -14 0 MPRFLG 0 ; 
       7 SCHEM 50 -10 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       9 SCHEM 65 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 67.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 70 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 10 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 15 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 20 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 25 -16 0 WIRECOL 3 7 DISPLAY 1 2 MPRFLG 0 ; 
       20 SCHEM 50 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 62.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 38.75 -4 0 SRT 1 1 1 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
       23 SCHEM 1.25 -14 0 MPRFLG 0 ; 
       24 SCHEM 58.75 -8 0 MPRFLG 0 ; 
       25 SCHEM 0 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 5 -14 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
