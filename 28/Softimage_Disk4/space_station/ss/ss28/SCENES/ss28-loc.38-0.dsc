SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.91-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       map-antenn.1-0 ; 
       map-bwingzz.1-0 ; 
       map-gargout.1-0 ; 
       map-gargrt.1-0 ; 
       map-hullroot_1.1-0 ; 
       map-hull_1.17-0 ROOT ; 
       map-south_block.1-0 ; 
       map-SS_01.1-0 ; 
       map-turret2.1-0 ; 
       map-turret3.1-0 ; 
       map-turwepemt1.1-0 ; 
       map-turwepemt2.1-0 ; 
       map-twingzz.20-0 ; 
       map-wlkway.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss28-loc.38-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 12 110 ; 
       2 3 110 ; 
       3 12 110 ; 
       9 5 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 2 110 ; 
       10 8 110 ; 
       12 5 110 ; 
       13 2 110 ; 
       11 9 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 30 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       8 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
