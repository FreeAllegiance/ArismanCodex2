SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.65-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       map-inf_light1.39-0 ROOT ; 
       map-inf_light2.39-0 ROOT ; 
       map-spot1.1-0 ; 
       map-spot1_int.3-0 ROOT ; 
       map-spot2.1-0 ; 
       map-spot2_int.19-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       map-antenn.1-0 ; 
       map-bwingzz.1-0 ; 
       map-gargout.1-0 ; 
       map-gargrt.1-0 ; 
       map-hull.35-0 ROOT ; 
       map-hullroot_1.1-0 ; 
       map-south_block.1-0 ; 
       map-SS_01.1-0 ; 
       map-turret2.1-0 ; 
       map-turwepemt1.1-0 ; 
       map-twingzz.20-0 ; 
       map-wlkway.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 11     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/black ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/clay_panel ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/gantry ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/lights ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/logo ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/map ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/round node ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/side ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/solar ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss28-map.39-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 10 110 ; 
       2 3 110 ; 
       3 10 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 4 110 ; 
       11 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
       4 5 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 10 -10 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 40 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       7 SCHEM 40 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       8 SCHEM 0 -12 0 MPRFLG 0 ; 
       9 SCHEM 0 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 18.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
