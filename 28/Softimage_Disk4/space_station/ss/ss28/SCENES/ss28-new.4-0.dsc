SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.5-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       new-antenn.1-0 ; 
       new-bwingzz.1-0 ; 
       new-gargout.1-0 ; 
       new-gargrt.1-0 ; 
       new-hull.3-0 ROOT ; 
       new-hullroot_1.1-0 ; 
       new-south_block.1-0 ; 
       new-SS_01.1-0 ; 
       new-turret2.1-0 ; 
       new-turwepemt1.1-0 ; 
       new-twingzz.20-0 ; 
       new-wlkway.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss28-new.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 10 110 ; 
       2 3 110 ; 
       3 10 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 4 110 ; 
       11 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0.2591019 2.330817 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 6.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 94 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
