SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.108-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       ss28-antenn.1-0 ; 
       ss28-bwingzz.1-0 ; 
       ss28-gargout.1-0 ; 
       ss28-gargrt.1-0 ; 
       ss28-hullroot_1.1-0 ; 
       ss28-hull_1.17-0 ; 
       ss28-null1.1-0 ; 
       ss28-root.1-0 ROOT ; 
       ss28-south_block.1-0 ; 
       ss28-SS_01.1-0 ; 
       ss28-SS_10.1-0 ; 
       ss28-SS_11.1-0 ; 
       ss28-SS_12.1-0 ; 
       ss28-SS_13.1-0 ; 
       ss28-SS_14.1-0 ; 
       ss28-SS_15.1-0 ; 
       ss28-SS_16.1-0 ; 
       ss28-SS_17.1-0 ; 
       ss28-SS_18.1-0 ; 
       ss28-SS_19.1-0 ; 
       ss28-SS_20.1-0 ; 
       ss28-SS_8.1-0 ; 
       ss28-SS_9.1-0 ; 
       ss28-turret2.1-0 ; 
       ss28-turret3.1-0 ; 
       ss28-turwepemt1.1-0 ; 
       ss28-turwepemt2.1-0 ; 
       ss28-twingzz.20-0 ; 
       ss28-wlkway.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss28/PICTURES/ss28 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss28-rotate.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 27 110 ; 
       2 3 110 ; 
       3 27 110 ; 
       4 5 110 ; 
       5 7 110 ; 
       6 2 110 ; 
       8 4 110 ; 
       9 8 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 6 110 ; 
       20 6 110 ; 
       21 8 110 ; 
       22 5 110 ; 
       23 2 110 ; 
       24 5 110 ; 
       25 23 110 ; 
       26 24 110 ; 
       27 5 110 ; 
       28 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 30 -6 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 15 -6 0 MPRFLG 0 ; 
       4 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -10 0 MPRFLG 0 ; 
       8 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 45 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 47.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 10 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 20 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 25 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 35 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 40 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       7 SCHEM 25 0 0 SRT 1 1 1 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
       23 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       28 SCHEM 5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
