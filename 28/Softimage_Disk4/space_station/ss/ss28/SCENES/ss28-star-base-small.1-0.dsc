SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.1-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       Baby_StarBase-light2_2_3.1-0 ROOT ; 
       Baby_StarBase-light3.1-0 ROOT ; 
       garage-light1_3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 59     
       Baby_StarBase-cube1.1-0 ROOT ; 
       Baby_StarBase-fhullzz.1-0 ROOT ; 
       Baby_StarBase-fhullzz1.1-0 ROOT ; 
       Baby_StarBase-polygon.1-0 ; 
       Baby_StarBase-polygon22.1-0 ; 
       Baby_StarBase-polygon23.1-0 ; 
       Baby_StarBase-polygon24.1-0 ; 
       Baby_StarBase-polygon25.1-0 ; 
       Baby_StarBase-polygon26.1-0 ; 
       Baby_StarBase-polygon27.1-0 ; 
       Baby_StarBase-polygon28.1-0 ; 
       Baby_StarBase-polygon29.1-0 ; 
       Baby_StarBase-polygon30.1-0 ; 
       Baby_StarBase-polygon31.1-0 ; 
       Baby_StarBase-polygon32.1-0 ; 
       Baby_StarBase-polygon33.1-0 ; 
       Baby_StarBase-polygon34.1-0 ; 
       Baby_StarBase-polygon35.1-0 ; 
       Baby_StarBase-polygon36.1-0 ; 
       Baby_StarBase-polygon37.1-0 ; 
       Baby_StarBase-polygon38.1-0 ; 
       Baby_StarBase-polygon39.1-0 ; 
       Baby_StarBase-polygon40.1-0 ; 
       Baby_StarBase-polygon41.1-0 ; 
       Baby_StarBase-polygon42.1-0 ; 
       Baby_StarBase-polygon43.1-0 ROOT ; 
       Baby_StarBase-polygon45.1-0 ROOT ; 
       Baby_StarBase-polygon46.1-0 ROOT ; 
       Baby_StarBase-polygon47.1-0 ROOT ; 
       Baby_StarBase-polygon49.1-0 ROOT ; 
       Baby_StarBase-polygon50.1-0 ROOT ; 
       Baby_StarBase-polygon51.1-0 ROOT ; 
       Baby_StarBase-polygon52.1-0 ROOT ; 
       Baby_StarBase-polygon54.1-0 ROOT ; 
       Baby_StarBase-polygon55.1-0 ROOT ; 
       Baby_StarBase-polygon56.1-0 ROOT ; 
       Baby_StarBase-polygon57.1-0 ROOT ; 
       Baby_StarBase-polygon58.1-0 ROOT ; 
       Baby_StarBase-polygon59.1-0 ROOT ; 
       Baby_StarBase-polygon60.1-0 ROOT ; 
       Baby_StarBase-polygon61.1-0 ROOT ; 
       Baby_StarBase-polygon62.1-0 ROOT ; 
       Baby_StarBase-polygon63.1-0 ROOT ; 
       Baby_StarBase-polygon64.1-0 ROOT ; 
       ss21a-ahullzz.1-0 ROOT ; 
       ss21a-antenn.1-0 ; 
       ss21a-antenn1.1-0 ROOT ; 
       ss21a-antenn2.1-0 ROOT ; 
       ss21a-bwingzz.1-0 ; 
       ss21a-gargin.1-0 ROOT ; 
       ss21a-gargout.1-0 ; 
       ss21a-gargrt.1-0 ROOT ; 
       ss21a-hullroot_1.1-0 ROOT ; 
       ss21a-lndpad.1-0 ROOT ; 
       ss21a-portal.1-0 ROOT ; 
       ss21a-ss21.1-0 ROOT ; 
       ss21a-twingzz.20-0 ; 
       ss21a-wlkway.1-0 ; 
       star_base_small-mesh.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss28/PICTURES/ss27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss28-star-base-small.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 1 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       45 51 110 ; 
       48 55 110 ; 
       50 51 110 ; 
       56 55 110 ; 
       57 50 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 1 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 1 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 1687.5 0 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 1710 0 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 1690 0 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 1665 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 -7.229352 1.263872 MPRFLG 0 ; 
       1 SCHEM 1631.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 1717.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 1605 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 1607.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 1610 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 1612.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 1615 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 1617.5 -2 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 1620 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 1622.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 1625 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 1627.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 1630 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 1632.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 1635 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 1637.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 1640 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 1642.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 1645 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 1647.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 1650 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 1652.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 1655 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 1657.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 1722.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 1745 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 1752.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 1760 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 1775 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 1782.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       31 SCHEM 1790 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       32 SCHEM 1797.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       33 SCHEM 1812.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 1820 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       35 SCHEM 1827.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       36 SCHEM 1835 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       37 SCHEM 1842.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       38 SCHEM 1850 0 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM 1857.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       40 SCHEM 1865 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       41 SCHEM 1872.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       42 SCHEM 1880 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       43 SCHEM 1892.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       44 SCHEM 1685 0 0 DISPLAY 3 0 SRT 1 1 1 0 0 0 0 0 -0.1106499 MPRFLG 0 ; 
       45 SCHEM 1602.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 1667.5 0 0 DISPLAY 3 2 SRT 1 1 1 1.273097e-007 -2.0936 4.833291e-007 -1.374532 6.463658 -0.0001038928 MPRFLG 0 ; 
       47 SCHEM 1670 0 0 DISPLAY 3 2 SRT 1 1 1 -1.273097e-007 5.235192 -4.83329e-007 1.374532 6.463658 -0.0001038928 MPRFLG 0 ; 
       48 SCHEM 1660 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 1675 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 9.093198 -3.147125e-005 0.1082078 MPRFLG 0 ; 
       50 SCHEM 1600 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 1601.25 0 0 DISPLAY 3 2 SRT 1 1 1 -6.357295e-008 -6.636296e-015 -5.932443e-007 9.091412 3.395251 -7.388197e-007 MPRFLG 0 ; 
       52 SCHEM 1677.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 -0.5199996 3.141593 1.490116e-008 -2.326216 -0.5887883 MPRFLG 0 ; 
       53 SCHEM 1680 0 0 DISPLAY 3 2 SRT 1 1 1 0 3.141593 0 9.051733 -0.7583233 0.40444 MPRFLG 0 ; 
       54 SCHEM 1682.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 3.141593 0 9.051733 0.6572135 4.651049 MPRFLG 0 ; 
       55 SCHEM 1661.25 0 0 DISPLAY 3 2 SRT 1 1 1 -6.3573e-008 0 0 -7.288246e-007 -2.446408e-008 -2.484815e-014 MPRFLG 0 ; 
       56 SCHEM 1662.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 1600 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 1907.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
