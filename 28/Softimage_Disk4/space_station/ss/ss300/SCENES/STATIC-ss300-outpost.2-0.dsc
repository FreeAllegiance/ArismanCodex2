SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.87-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       ss300_outpost-mat58.4-0 ; 
       ss300_outpost-mat59.4-0 ; 
       ss300_outpost-mat60.3-0 ; 
       ss300_outpost-mat61.2-0 ; 
       ss300_outpost-mat62.1-0 ; 
       ss300_outpost-mat63.1-0 ; 
       ss300_outpost-mat64.2-0 ; 
       ss300_outpost-mat65.2-0 ; 
       ss300_outpost-mat66.1-0 ; 
       ss300_outpost-mat67.2-0 ; 
       ss300_outpost-mat68.1-0 ; 
       ss300_outpost-mat69.1-0 ; 
       ss300_outpost-mat70.1-0 ; 
       ss300_outpost-mat71.2-0 ; 
       ss300_outpost-mat72.1-0 ; 
       ss300_outpost-mat73.2-0 ; 
       ss300_outpost-mat74.1-0 ; 
       ss300_outpost-mat75.1-0 ; 
       ss300_outpost-mat76.1-0 ; 
       ss300_outpost-mat77.1-0 ; 
       ss300_outpost-mat78.1-0 ; 
       ss300_outpost-mat79.1-0 ; 
       ss300_outpost-mat80.1-0 ; 
       ss300_outpost-mat81.1-0 ; 
       ss300_outpost-mat82.2-0 ; 
       ss300_outpost-mat83.1-0 ; 
       ss300_outpost-mat85.1-0 ; 
       ss300_outpost-mat86.1-0 ; 
       ss300_outpost-mat87.1-0 ; 
       ss300_outpost-mat88.1-0 ; 
       ss300_outpost-mat89.1-0 ; 
       ss300_outpost-mat90.1-0 ; 
       ss300_outpost-mat91.1-0 ; 
       ss300_outpost-mat92.1-0 ; 
       ss300_outpost-mat93.1-0 ; 
       ss300_outpost-mat94.1-0 ; 
       ss300_outpost-mat95.1-0 ; 
       ss305_elect_station-mat52.4-0 ; 
       ss305_elect_station-mat53.4-0 ; 
       ss305_elect_station-mat54.4-0 ; 
       ss305_elect_station-mat55.2-0 ; 
       ss305_elect_station-mat56.2-0 ; 
       ss305_elect_station-mat57.2-0 ; 
       ss306_ripcord-mat5.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube23.1-0 ; 
       root-cube24.1-0 ; 
       root-cube26.1-0 ; 
       root-cube28.1-0 ; 
       root-cube29.1-0 ; 
       root-cube3.1-0 ; 
       root-cube30.1-0 ; 
       root-cube31.1-0 ; 
       root-cube32.1-0 ; 
       root-cube35.1-0 ; 
       root-cube36.1-0 ; 
       root-cube38.1-0 ; 
       root-cube39.1-0 ; 
       root-cube4.1-0 ; 
       root-cube41.1-0 ; 
       root-cube42.1-0 ; 
       root-cube7.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl2.1-0 ; 
       root-east_bay_11_10.1-0 ; 
       root-east_bay_11_11.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.2-0 ; 
       root-root.65-0 ROOT ; 
       root-sphere6.2-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss300/PICTURES/beltersbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss300/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss300/PICTURES/ss300 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss300-outpost.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       ss300_outpost-t2d58.5-0 ; 
       ss300_outpost-t2d59.4-0 ; 
       ss300_outpost-t2d60.4-0 ; 
       ss300_outpost-t2d61.4-0 ; 
       ss300_outpost-t2d62.5-0 ; 
       ss300_outpost-t2d63.5-0 ; 
       ss300_outpost-t2d64.5-0 ; 
       ss300_outpost-t2d65.4-0 ; 
       ss300_outpost-t2d66.4-0 ; 
       ss300_outpost-t2d67.4-0 ; 
       ss300_outpost-t2d68.9-0 ; 
       ss300_outpost-t2d69.3-0 ; 
       ss300_outpost-t2d70.3-0 ; 
       ss300_outpost-t2d71.2-0 ; 
       ss300_outpost-t2d72.1-0 ; 
       ss300_outpost-t2d73.5-0 ; 
       ss300_outpost-t2d74.4-0 ; 
       ss300_outpost-t2d75.1-0 ; 
       ss300_outpost-t2d76.1-0 ; 
       ss300_outpost-t2d77.2-0 ; 
       ss300_outpost-t2d78.1-0 ; 
       ss300_outpost-t2d79.1-0 ; 
       ss300_outpost-t2d80.1-0 ; 
       ss300_outpost-t2d81.1-0 ; 
       ss300_outpost-t2d82.1-0 ; 
       ss300_outpost-t2d83.1-0 ; 
       ss300_outpost-t2d84.2-0 ; 
       ss300_outpost-t2d85.1-0 ; 
       ss300_outpost-t2d86.1-0 ; 
       ss300_outpost-t2d87.1-0 ; 
       ss300_outpost-t2d88.1-0 ; 
       ss300_outpost-t2d89.3-0 ; 
       ss300_outpost-t2d90.3-0 ; 
       ss300_outpost-t2d91.3-0 ; 
       ss300_outpost-t2d92.2-0 ; 
       ss300_outpost-t2d93.2-0 ; 
       ss300_outpost-t2d94.2-0 ; 
       ss305_elect_station-t2d52.6-0 ; 
       ss305_elect_station-t2d53.6-0 ; 
       ss305_elect_station-t2d54.6-0 ; 
       ss305_elect_station-t2d55.4-0 ; 
       ss305_elect_station-t2d56.4-0 ; 
       ss305_elect_station-t2d57.4-0 ; 
       ss306_ripcord-t2d7.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 19 110 ; 
       1 19 110 ; 
       2 0 110 ; 
       3 27 110 ; 
       4 27 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       7 12 110 ; 
       8 4 110 ; 
       9 7 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 13 110 ; 
       13 21 110 ; 
       14 4 110 ; 
       15 3 110 ; 
       16 27 110 ; 
       17 3 110 ; 
       18 20 110 ; 
       19 16 110 ; 
       20 12 110 ; 
       21 4 110 ; 
       22 19 110 ; 
       23 14 110 ; 
       24 27 110 ; 
       25 27 110 ; 
       27 26 110 ; 
       28 6 110 ; 
       29 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       1 10 300 ; 
       2 30 300 ; 
       3 0 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       4 1 300 ; 
       4 3 300 ; 
       4 18 300 ; 
       5 7 300 ; 
       5 20 300 ; 
       6 13 300 ; 
       6 21 300 ; 
       7 24 300 ; 
       8 22 300 ; 
       9 26 300 ; 
       10 4 300 ; 
       11 5 300 ; 
       12 23 300 ; 
       13 28 300 ; 
       14 8 300 ; 
       15 14 300 ; 
       16 2 300 ; 
       17 12 300 ; 
       18 29 300 ; 
       19 9 300 ; 
       19 17 300 ; 
       20 25 300 ; 
       21 6 300 ; 
       21 19 300 ; 
       22 31 300 ; 
       22 32 300 ; 
       22 33 300 ; 
       23 34 300 ; 
       23 35 300 ; 
       23 36 300 ; 
       24 40 300 ; 
       24 41 300 ; 
       24 42 300 ; 
       25 37 300 ; 
       25 38 300 ; 
       25 39 300 ; 
       27 43 300 ; 
       29 27 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 10 401 ; 
       1 0 401 ; 
       2 14 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 26 401 ; 
       25 25 401 ; 
       26 27 401 ; 
       27 29 401 ; 
       28 24 401 ; 
       29 28 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       37 37 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 40 401 ; 
       41 41 401 ; 
       42 42 401 ; 
       43 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 17.5 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 20 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 32.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 7.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 15 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 35 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 12.5 -16 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 0 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 12.5 -18 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 2.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 11.25 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 11.25 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 7.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 30 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 20 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 32.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 7.548592 -16.57013 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 20 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 10 -16 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 11.25 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 22.5 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 7.5 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 25 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 27.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 17.5 -4 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 35 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 5 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9.048592 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.548592 -18.57013 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9.048592 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.548592 -20.57013 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
