SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bars-cam_int1.1-0 ROOT ; 
       bars-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       bars-cyl1.1-0 ; 
       bars-cyl10.1-0 ; 
       bars-cyl11.1-0 ; 
       bars-cyl12.1-0 ; 
       bars-cyl13.1-0 ; 
       bars-cyl14.1-0 ; 
       bars-cyl15.1-0 ; 
       bars-cyl16.1-0 ; 
       bars-cyl17.1-0 ; 
       bars-cyl18.1-0 ; 
       bars-cyl2.1-0 ; 
       bars-cyl3.1-0 ; 
       bars-cyl4.1-0 ; 
       bars-cyl5.1-0 ; 
       bars-cyl6.1-0 ; 
       bars-cyl7.1-0 ; 
       bars-cyl8.1-0 ; 
       bars-cyl9.1-0 ; 
       bars-null1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bars-bars.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       10 18 110 ; 
       11 18 110 ; 
       12 18 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 10 110 ; 
       1 10 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       9 10 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 40 -2 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 5 -4 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 10 -4 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 MPRFLG 0 ; 
       7 SCHEM 35 -4 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 30 -4 0 MPRFLG 0 ; 
       18 SCHEM 22.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
