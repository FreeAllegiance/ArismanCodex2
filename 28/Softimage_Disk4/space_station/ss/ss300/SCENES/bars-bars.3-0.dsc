SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bars-cam_int1.3-0 ROOT ; 
       bars-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 8     
       bars-light1.2-0 ROOT ; 
       bars-light2.2-0 ROOT ; 
       bars-light3.1-0 ROOT ; 
       bars-light4.1-0 ROOT ; 
       bars-light5.1-0 ROOT ; 
       bars-light6.1-0 ROOT ; 
       bars-spot1.1-0 ; 
       bars-spot1_int1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       bars-mat1.1-0 ; 
       bars-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       bars-Static_Cling1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       bars-cyl1.1-0 ; 
       bars-cyl1_1.1-0 ; 
       bars-cyl10.1-0 ; 
       bars-cyl10_1.1-0 ; 
       bars-cyl11.1-0 ; 
       bars-cyl11_1.1-0 ; 
       bars-cyl12.1-0 ; 
       bars-cyl12_1.1-0 ; 
       bars-cyl13.1-0 ; 
       bars-cyl13_1.1-0 ; 
       bars-cyl14.1-0 ; 
       bars-cyl14_1.1-0 ; 
       bars-cyl15.1-0 ; 
       bars-cyl15_1.1-0 ; 
       bars-cyl16.1-0 ; 
       bars-cyl16_1.1-0 ; 
       bars-cyl17.1-0 ; 
       bars-cyl17_1.1-0 ; 
       bars-cyl18.1-0 ; 
       bars-cyl18_1.1-0 ; 
       bars-cyl2.1-0 ; 
       bars-cyl2_1.1-0 ; 
       bars-cyl3.1-0 ; 
       bars-cyl3_1.1-0 ; 
       bars-cyl4.1-0 ; 
       bars-cyl4_1.1-0 ; 
       bars-cyl5.1-0 ; 
       bars-cyl5_1.1-0 ; 
       bars-cyl6.1-0 ; 
       bars-cyl6_1.1-0 ; 
       bars-cyl7.1-0 ; 
       bars-cyl7_1.1-0 ; 
       bars-cyl8.1-0 ; 
       bars-cyl8_1.1-0 ; 
       bars-cyl9.1-0 ; 
       bars-cyl9_1.1-0 ; 
       bars-grid1.1-0 ; 
       bars-null1.1-0 ; 
       bars-null1_1.1-0 ; 
       bars-null4.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bars-bars.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 37 110 ; 
       20 37 110 ; 
       22 37 110 ; 
       24 37 110 ; 
       26 20 110 ; 
       28 20 110 ; 
       30 20 110 ; 
       32 20 110 ; 
       34 20 110 ; 
       2 20 110 ; 
       4 20 110 ; 
       6 20 110 ; 
       8 20 110 ; 
       10 20 110 ; 
       12 20 110 ; 
       14 20 110 ; 
       16 20 110 ; 
       18 20 110 ; 
       37 39 110 ; 
       38 39 110 ; 
       25 38 110 ; 
       23 38 110 ; 
       1 38 110 ; 
       21 38 110 ; 
       19 21 110 ; 
       17 21 110 ; 
       15 21 110 ; 
       13 21 110 ; 
       11 21 110 ; 
       9 21 110 ; 
       7 21 110 ; 
       5 21 110 ; 
       3 21 110 ; 
       35 21 110 ; 
       33 21 110 ; 
       31 21 110 ; 
       29 21 110 ; 
       27 21 110 ; 
       36 39 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       36 0 300 ; 
       39 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       6 7 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 92.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 95 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 97.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 107.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 107.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 40 -4 0 MPRFLG 0 ; 
       24 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 5 -6 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 10 -6 0 MPRFLG 0 ; 
       34 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 25 -6 0 MPRFLG 0 ; 
       14 SCHEM 35 -6 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 30 -6 0 MPRFLG 0 ; 
       37 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       38 SCHEM 65 -2 0 MPRFLG 0 ; 
       25 SCHEM 85 -4 0 MPRFLG 0 ; 
       23 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 80 -4 0 MPRFLG 0 ; 
       21 SCHEM 61.25 -4 0 MPRFLG 0 ; 
       19 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 75 -6 0 MPRFLG 0 ; 
       15 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 70 -6 0 MPRFLG 0 ; 
       9 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 65 -6 0 MPRFLG 0 ; 
       5 SCHEM 60 -6 0 MPRFLG 0 ; 
       3 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 55 -6 0 MPRFLG 0 ; 
       33 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 50 -6 0 MPRFLG 0 ; 
       29 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 45 -6 0 MPRFLG 0 ; 
       36 SCHEM 87.5 -2 0 MPRFLG 0 ; 
       39 SCHEM 46.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 90 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
