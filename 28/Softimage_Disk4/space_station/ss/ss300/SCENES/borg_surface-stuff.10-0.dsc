SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       stuff-cam_int1.10-0 ROOT ; 
       stuff-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       stuff-inf_light1.1-0 ROOT ; 
       stuff-spot2.1-0 ; 
       stuff-spot2_int1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       stuff-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 63     
       stuff25-cyl10.1-0 ; 
       stuff25-cyl11.1-0 ; 
       stuff25-cyl11_1.1-0 ; 
       stuff25-cyl11_10.1-0 ; 
       stuff25-cyl11_11.1-0 ; 
       stuff25-cyl11_12.1-0 ; 
       stuff25-cyl11_13.1-0 ; 
       stuff25-cyl11_14.1-0 ; 
       stuff25-cyl11_15.1-0 ; 
       stuff25-cyl11_16.1-0 ; 
       stuff25-cyl11_17.1-0 ; 
       stuff25-cyl11_18.1-0 ; 
       stuff25-cyl11_19.1-0 ; 
       stuff25-cyl11_20.1-0 ; 
       stuff25-cyl11_21.1-0 ; 
       stuff25-cyl11_22.1-0 ; 
       stuff25-cyl11_23.1-0 ; 
       stuff25-cyl11_24.1-0 ; 
       stuff25-cyl11_25.1-0 ; 
       stuff25-cyl11_3.1-0 ; 
       stuff25-cyl11_4.1-0 ; 
       stuff25-cyl11_5.1-0 ; 
       stuff25-cyl11_6.1-0 ; 
       stuff25-cyl11_7.1-0 ; 
       stuff25-cyl11_8.1-0 ; 
       stuff25-cyl11_9.1-0 ; 
       stuff25-cyl3.2-0 ; 
       stuff25-cyl4.1-0 ; 
       stuff25-cyl7.1-0 ; 
       stuff25-cyl8.1-0 ; 
       stuff25-cyl9.1-0 ; 
       stuff25-Splines1.1-0 ROOT ; 
       stuff25-spl_1.1-0 ; 
       stuff25-spl_10.1-0 ; 
       stuff25-spl_11.1-0 ; 
       stuff25-spl_12.1-0 ; 
       stuff25-spl_13.1-0 ; 
       stuff25-spl_14.1-0 ; 
       stuff25-spl_15.1-0 ; 
       stuff25-spl_16.1-0 ; 
       stuff25-spl_17.1-0 ; 
       stuff25-spl_18.1-0 ; 
       stuff25-spl_19.1-0 ; 
       stuff25-spl_2.1-0 ; 
       stuff25-spl_20.1-0 ; 
       stuff25-spl_21.1-0 ; 
       stuff25-spl_22.1-0 ; 
       stuff25-spl_23.1-0 ; 
       stuff25-spl_24.1-0 ; 
       stuff25-spl_25.1-0 ; 
       stuff25-spl_26.1-0 ; 
       stuff25-spl_27.1-0 ; 
       stuff25-spl_28.1-0 ; 
       stuff25-spl_29.1-0 ; 
       stuff25-spl_3.1-0 ; 
       stuff25-spl_30.1-0 ; 
       stuff25-spl_31.1-0 ; 
       stuff25-spl_4.1-0 ; 
       stuff25-spl_5.1-0 ; 
       stuff25-spl_6.1-0 ; 
       stuff25-spl_7.1-0 ; 
       stuff25-spl_8.1-0 ; 
       stuff25-spl_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss300/PICTURES/rust ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       borg_surface-stuff.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       stuff-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       26 39 110 ; 
       26 39 220 RELDATA SCLE 0.47 9.623425 0.47 ROLL 0 TRNS -5.960464e-008 22.42571 2.980232e-008 EndOfRELDATA ; 
       26 39 220 2 ; 
       29 41 110 ; 
       29 41 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1132823 1.490116e-008 EndOfRELDATA ; 
       29 41 220 2 ; 
       27 40 110 ; 
       27 40 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1229151 1.490116e-008 EndOfRELDATA ; 
       27 40 220 2 ; 
       30 42 110 ; 
       30 42 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1089929 1.490116e-008 EndOfRELDATA ; 
       30 42 220 2 ; 
       28 43 110 ; 
       28 43 220 RELDATA SCLE 1 1.070837 1 ROLL 0 TRNS -2.980232e-008 1.183085 1.490116e-008 EndOfRELDATA ; 
       28 43 220 2 ; 
       0 44 110 ; 
       0 44 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1266951 1.490116e-008 EndOfRELDATA ; 
       0 44 220 2 ; 
       8 45 110 ; 
       8 45 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1071317 1.490116e-008 EndOfRELDATA ; 
       8 45 220 2 ; 
       9 46 110 ; 
       9 46 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1024677 1.490116e-008 EndOfRELDATA ; 
       9 46 220 2 ; 
       10 47 110 ; 
       10 47 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1118239 1.490116e-008 EndOfRELDATA ; 
       10 47 220 2 ; 
       11 48 110 ; 
       11 48 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1079287 1.490116e-008 EndOfRELDATA ; 
       11 48 220 2 ; 
       13 49 110 ; 
       13 49 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1270708 1.490116e-008 EndOfRELDATA ; 
       13 49 220 2 ; 
       14 50 110 ; 
       14 50 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1307463 1.490116e-008 EndOfRELDATA ; 
       14 50 220 2 ; 
       15 51 110 ; 
       15 51 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1068517 1.490116e-008 EndOfRELDATA ; 
       15 51 220 2 ; 
       16 52 110 ; 
       16 52 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.101617 1.490116e-008 EndOfRELDATA ; 
       16 52 220 2 ; 
       17 53 110 ; 
       17 53 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1051216 1.490116e-008 EndOfRELDATA ; 
       17 53 220 2 ; 
       18 55 110 ; 
       18 55 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1229152 1.490116e-008 EndOfRELDATA ; 
       18 55 220 2 ; 
       1 56 110 ; 
       1 56 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1108502 1.490116e-008 EndOfRELDATA ; 
       1 56 220 2 ; 
       2 54 110 ; 
       2 54 220 RELDATA SCLE 1 1.250138 1 ROLL 0 TRNS -2.980232e-008 0.1223802 1.490116e-008 EndOfRELDATA ; 
       2 54 220 2 ; 
       19 57 110 ; 
       19 57 220 RELDATA SCLE 1 1.247898 1 ROLL 0 TRNS -2.980232e-008 0.1236886 1.490116e-008 EndOfRELDATA ; 
       19 57 220 2 ; 
       21 58 110 ; 
       21 58 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1244319 1.490116e-008 EndOfRELDATA ; 
       21 58 220 2 ; 
       20 59 110 ; 
       20 59 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1046188 1.490116e-008 EndOfRELDATA ; 
       20 59 220 2 ; 
       22 60 110 ; 
       22 60 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1013895 1.490116e-008 EndOfRELDATA ; 
       22 60 220 2 ; 
       24 61 110 ; 
       24 61 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.09853804 1.490116e-008 EndOfRELDATA ; 
       24 61 220 2 ; 
       23 62 110 ; 
       23 62 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1173548 1.490116e-008 EndOfRELDATA ; 
       23 62 220 2 ; 
       25 33 110 ; 
       25 33 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1156956 1.490116e-008 EndOfRELDATA ; 
       25 33 220 2 ; 
       12 34 110 ; 
       12 34 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1097923 1.490116e-008 EndOfRELDATA ; 
       12 34 220 2 ; 
       3 35 110 ; 
       3 35 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1059327 1.490116e-008 EndOfRELDATA ; 
       3 35 220 2 ; 
       4 36 110 ; 
       4 36 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1035479 1.490116e-008 EndOfRELDATA ; 
       4 36 220 2 ; 
       5 37 110 ; 
       5 37 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1058219 1.490116e-008 EndOfRELDATA ; 
       5 37 220 2 ; 
       6 38 110 ; 
       6 38 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.130315 1.490116e-008 EndOfRELDATA ; 
       6 38 220 2 ; 
       7 32 110 ; 
       7 32 220 RELDATA SCLE 1 1.2 1 ROLL 0 TRNS -2.980232e-008 0.1028805 1.490116e-008 EndOfRELDATA ; 
       7 32 220 2 ; 
       32 31 110 ; 
       43 31 110 ; 
       54 31 110 ; 
       57 31 110 ; 
       58 31 110 ; 
       59 31 110 ; 
       60 31 110 ; 
       61 31 110 ; 
       62 31 110 ; 
       33 31 110 ; 
       34 31 110 ; 
       35 31 110 ; 
       36 31 110 ; 
       37 31 110 ; 
       38 31 110 ; 
       39 31 110 ; 
       40 31 110 ; 
       41 31 110 ; 
       42 31 110 ; 
       44 31 110 ; 
       45 31 110 ; 
       46 31 110 ; 
       47 31 110 ; 
       48 31 110 ; 
       49 31 110 ; 
       50 31 110 ; 
       51 31 110 ; 
       52 31 110 ; 
       53 31 110 ; 
       55 31 110 ; 
       56 31 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       31 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       31 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       1 2 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 80.02973 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 80.02973 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 82.52973 0 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       26 SCHEM 2.912174 -5.249171 0 USR MPRFLG 0 ; 
       29 SCHEM 45.02973 -5.097767 0 USR MPRFLG 0 ; 
       27 SCHEM 42.75136 -5.09949 0 USR MPRFLG 0 ; 
       30 SCHEM 47.52973 -5.097767 0 USR MPRFLG 0 ; 
       28 SCHEM 5.412174 -5.249171 0 USR MPRFLG 0 ; 
       0 SCHEM 50.02973 -5.097767 0 USR MPRFLG 0 ; 
       8 SCHEM 52.52973 -5.097767 0 USR MPRFLG 0 ; 
       9 SCHEM 55.02973 -5.097767 0 USR MPRFLG 0 ; 
       10 SCHEM 57.52973 -5.097767 0 USR MPRFLG 0 ; 
       11 SCHEM 60.02973 -5.097767 0 USR MPRFLG 0 ; 
       13 SCHEM 62.52973 -5.097767 0 USR MPRFLG 0 ; 
       14 SCHEM 65.02973 -5.097767 0 USR MPRFLG 0 ; 
       15 SCHEM 67.52973 -5.097767 0 USR MPRFLG 0 ; 
       16 SCHEM 70.02973 -5.097767 0 USR MPRFLG 0 ; 
       17 SCHEM 72.52973 -5.097767 0 USR MPRFLG 0 ; 
       18 SCHEM 75.02973 -5.097767 0 USR MPRFLG 0 ; 
       1 SCHEM 77.52973 -5.097767 0 USR MPRFLG 0 ; 
       2 SCHEM 7.751362 -5.09949 0 USR MPRFLG 0 ; 
       19 SCHEM 10.25136 -5.09949 0 USR MPRFLG 0 ; 
       21 SCHEM 12.75136 -5.09949 0 USR MPRFLG 0 ; 
       20 SCHEM 15.25136 -5.09949 0 USR MPRFLG 0 ; 
       22 SCHEM 17.75136 -5.09949 0 USR MPRFLG 0 ; 
       24 SCHEM 20.25135 -5.09949 0 USR MPRFLG 0 ; 
       23 SCHEM 22.75135 -5.09949 0 USR MPRFLG 0 ; 
       25 SCHEM 25.25135 -5.09949 0 USR MPRFLG 0 ; 
       12 SCHEM 27.75136 -5.09949 0 USR MPRFLG 0 ; 
       3 SCHEM 30.25136 -5.09949 0 USR MPRFLG 0 ; 
       4 SCHEM 32.75136 -5.09949 0 USR MPRFLG 0 ; 
       5 SCHEM 35.25136 -5.09949 0 USR MPRFLG 0 ; 
       6 SCHEM 37.75136 -5.09949 0 USR MPRFLG 0 ; 
       7 SCHEM 40.25136 -5.09949 0 USR MPRFLG 0 ; 
       31 SCHEM 40 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       32 SCHEM 40 -4 0 MPRFLG 0 ; 
       43 SCHEM 5 -4 0 MPRFLG 0 ; 
       54 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       57 SCHEM 10 -4 0 MPRFLG 0 ; 
       58 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       59 SCHEM 15 -4 0 MPRFLG 0 ; 
       60 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       61 SCHEM 20 -4 0 MPRFLG 0 ; 
       62 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 25 -4 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       35 SCHEM 30 -4 0 MPRFLG 0 ; 
       36 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       37 SCHEM 35 -4 0 MPRFLG 0 ; 
       38 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       40 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       41 SCHEM 45 -4 0 MPRFLG 0 ; 
       42 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       44 SCHEM 50 -4 0 MPRFLG 0 ; 
       45 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       46 SCHEM 55 -4 0 MPRFLG 0 ; 
       47 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       48 SCHEM 60 -4 0 MPRFLG 0 ; 
       49 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       50 SCHEM 65 -4 0 MPRFLG 0 ; 
       51 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       52 SCHEM 70 -4 0 MPRFLG 0 ; 
       53 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       55 SCHEM 75 -4 0 MPRFLG 0 ; 
       56 SCHEM 77.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 82.52973 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 85.02973 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
