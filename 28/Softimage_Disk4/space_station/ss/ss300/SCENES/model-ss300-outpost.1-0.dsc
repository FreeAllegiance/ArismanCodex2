SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.4-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 76     
       root-cube23.1-0 ; 
       root-cube24.1-0 ; 
       root-cube26.1-0 ; 
       root-cube28.1-0 ; 
       root-cube29.1-0 ; 
       root-cube3.1-0 ; 
       root-cube30.1-0 ; 
       root-cube31.1-0 ; 
       root-cube32.1-0 ; 
       root-cube33.1-0 ; 
       root-cube34.1-0 ; 
       root-cube35.1-0 ; 
       root-cube36.1-0 ; 
       root-cube38.1-0 ; 
       root-cube39.1-0 ; 
       root-cube41.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl2.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.2-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-root.2-0 ROOT ; 
       root-sphere6.2-0 ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss300-outpost.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss306_ripcord-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       31 32 110 ; 
       70 29 110 ; 
       30 32 110 ; 
       58 29 110 ; 
       60 31 110 ; 
       55 30 110 ; 
       61 31 110 ; 
       53 30 110 ; 
       62 31 110 ; 
       51 30 110 ; 
       5 1 110 ; 
       63 31 110 ; 
       0 33 110 ; 
       64 27 110 ; 
       16 17 110 ; 
       17 11 110 ; 
       65 27 110 ; 
       27 32 110 ; 
       28 32 110 ; 
       29 32 110 ; 
       66 27 110 ; 
       56 28 110 ; 
       46 31 110 ; 
       57 28 110 ; 
       47 31 110 ; 
       48 29 110 ; 
       50 29 110 ; 
       52 29 110 ; 
       54 29 110 ; 
       49 30 110 ; 
       59 30 110 ; 
       71 30 110 ; 
       18 1 110 ; 
       19 32 110 ; 
       20 32 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 19 110 ; 
       25 19 110 ; 
       26 20 110 ; 
       34 20 110 ; 
       35 19 110 ; 
       36 19 110 ; 
       37 20 110 ; 
       38 19 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 19 110 ; 
       42 20 110 ; 
       43 19 110 ; 
       44 19 110 ; 
       45 20 110 ; 
       74 19 110 ; 
       75 20 110 ; 
       67 27 110 ; 
       68 27 110 ; 
       69 27 110 ; 
       33 15 110 ; 
       1 33 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 11 110 ; 
       6 4 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 0 110 ; 
       10 1 110 ; 
       11 12 110 ; 
       12 18 110 ; 
       13 1 110 ; 
       14 0 110 ; 
       15 32 110 ; 
       72 3 110 ; 
       73 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       70 0 300 ; 
       58 9 300 ; 
       60 10 300 ; 
       55 1 300 ; 
       61 11 300 ; 
       53 2 300 ; 
       62 12 300 ; 
       51 3 300 ; 
       63 13 300 ; 
       64 14 300 ; 
       65 15 300 ; 
       66 16 300 ; 
       56 8 300 ; 
       46 29 300 ; 
       57 7 300 ; 
       47 29 300 ; 
       48 28 300 ; 
       50 28 300 ; 
       52 28 300 ; 
       54 28 300 ; 
       49 4 300 ; 
       59 5 300 ; 
       71 6 300 ; 
       19 23 300 ; 
       19 24 300 ; 
       19 25 300 ; 
       20 20 300 ; 
       20 21 300 ; 
       20 22 300 ; 
       34 26 300 ; 
       35 27 300 ; 
       36 27 300 ; 
       37 26 300 ; 
       38 27 300 ; 
       39 26 300 ; 
       40 26 300 ; 
       41 27 300 ; 
       42 26 300 ; 
       43 27 300 ; 
       44 27 300 ; 
       45 26 300 ; 
       67 17 300 ; 
       68 18 300 ; 
       69 19 300 ; 
       33 30 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       20 0 401 ; 
       21 1 401 ; 
       22 2 401 ; 
       23 3 401 ; 
       24 4 401 ; 
       25 5 401 ; 
       30 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       31 SCHEM 45.00701 -25.52405 0 USR MPRFLG 0 ; 
       70 SCHEM 51.59078 -31.11833 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 26.03826 -12.5338 0 USR MPRFLG 0 ; 
       58 SCHEM 49.09078 -31.11833 0 WIRECOL 4 7 MPRFLG 0 ; 
       60 SCHEM 41.257 -27.52405 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 19.78827 -14.5338 0 WIRECOL 4 7 MPRFLG 0 ; 
       61 SCHEM 46.25701 -27.52405 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 22.28826 -14.5338 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 48.75701 -27.52405 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 24.78826 -14.5338 0 WIRECOL 4 7 MPRFLG 0 ; 
       5 SCHEM 82.80932 -8.954098 0 MPRFLG 0 ; 
       63 SCHEM 51.257 -27.52405 0 WIRECOL 2 7 MPRFLG 0 ; 
       0 SCHEM 60.30931 -6.954097 0 MPRFLG 0 ; 
       64 SCHEM 37.78624 -23.61704 0 WIRECOL 2 7 MPRFLG 0 ; 
       16 SCHEM 65.30932 -16.9541 0 MPRFLG 0 ; 
       17 SCHEM 65.30932 -14.9541 0 MPRFLG 0 ; 
       65 SCHEM 40.28624 -23.61704 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 44.03624 -21.61704 0 USR MPRFLG 0 ; 
       28 SCHEM 29.56889 -22.72774 0 USR MPRFLG 0 ; 
       29 SCHEM 45.34078 -29.11833 0 USR MPRFLG 0 ; 
       66 SCHEM 42.78624 -23.61704 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 28.31889 -24.72774 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 38.757 -27.52405 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 30.81889 -24.72774 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 43.75701 -27.52405 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 46.59078 -31.11833 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 44.09078 -31.11833 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 41.59078 -31.11833 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 39.09078 -31.11833 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 27.28826 -14.5338 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 29.78826 -14.5338 0 WIRECOL 4 7 MPRFLG 0 ; 
       71 SCHEM 32.28826 -14.5338 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 66.55932 -8.954098 0 MPRFLG 0 ; 
       19 SCHEM 21.55932 -2.954098 0 MPRFLG 0 ; 
       20 SCHEM 46.55931 -2.954098 0 MPRFLG 0 ; 
       21 SCHEM 22.80932 -4.954097 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 27.80932 -4.954097 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 25.30932 -4.954097 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 32.80932 -4.954097 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 30.30932 -4.954097 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 55.30931 -4.954097 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 42.80931 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 12.80932 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 7.809317 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 37.80931 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 10.30932 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 40.30931 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 50.30931 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 20.30932 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 45.30931 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 15.30932 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 17.80931 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 47.80931 -4.954097 0 WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 35.30931 -4.954097 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 52.80931 -4.954097 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 45.28624 -23.61704 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 47.78624 -23.61704 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 50.28623 -23.61704 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 70.30932 -4.954097 0 MPRFLG 0 ; 
       1 SCHEM 74.05932 -6.954097 0 MPRFLG 0 ; 
       2 SCHEM 70.30932 -8.954098 0 MPRFLG 0 ; 
       3 SCHEM 57.80931 -8.954098 0 MPRFLG 0 ; 
       4 SCHEM 67.80932 -14.9541 0 MPRFLG 0 ; 
       6 SCHEM 67.80932 -16.9541 0 MPRFLG 0 ; 
       7 SCHEM 72.80932 -8.954098 0 MPRFLG 0 ; 
       8 SCHEM 75.30932 -8.954098 0 MPRFLG 0 ; 
       9 SCHEM 60.30931 -8.954098 0 MPRFLG 0 ; 
       10 SCHEM 77.80932 -8.954098 0 MPRFLG 0 ; 
       11 SCHEM 66.55932 -12.9541 0 MPRFLG 0 ; 
       12 SCHEM 66.55932 -10.9541 0 MPRFLG 0 ; 
       13 SCHEM 80.30932 -8.954098 0 MPRFLG 0 ; 
       14 SCHEM 62.80931 -8.954098 0 MPRFLG 0 ; 
       15 SCHEM 70.30932 -2.954098 0 MPRFLG 0 ; 
       72 SCHEM 57.80931 -10.9541 0 MPRFLG 0 ; 
       73 SCHEM 75.30932 -10.9541 0 MPRFLG 0 ; 
       32 SCHEM 44.69184 2.75076 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 72.65297 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.47525 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 74.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 79.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 84.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 58.17583 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 59.22066 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 54.54168 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 46.56545 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM -5.297054 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 49 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 89.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 105.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 110.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 115.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 120.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 97.46411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 104.9641 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 49.47525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.71853 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.71853 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 52.71853 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 52.71853 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 93.69379 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 118.8715 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       6 SCHEM 49.47525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 58.17583 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59.22066 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54.54168 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
