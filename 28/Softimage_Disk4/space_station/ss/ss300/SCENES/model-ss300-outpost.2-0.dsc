SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.5-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 79     
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube23.1-0 ; 
       root-cube24.1-0 ; 
       root-cube26.1-0 ; 
       root-cube28.1-0 ; 
       root-cube29.1-0 ; 
       root-cube3.1-0 ; 
       root-cube30.1-0 ; 
       root-cube31.1-0 ; 
       root-cube32.1-0 ; 
       root-cube35.1-0 ; 
       root-cube36.1-0 ; 
       root-cube38.1-0 ; 
       root-cube39.1-0 ; 
       root-cube4.1-0 ; 
       root-cube41.1-0 ; 
       root-cube7.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl2.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.2-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-root.3-0 ROOT ; 
       root-sphere6.2-0 ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss300-outpost.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss306_ripcord-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 36 110 ; 
       4 36 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       7 12 110 ; 
       8 4 110 ; 
       9 7 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 13 110 ; 
       13 21 110 ; 
       14 4 110 ; 
       15 3 110 ; 
       17 3 110 ; 
       19 20 110 ; 
       20 12 110 ; 
       21 4 110 ; 
       22 36 110 ; 
       23 36 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       26 22 110 ; 
       27 22 110 ; 
       28 22 110 ; 
       29 23 110 ; 
       30 36 110 ; 
       31 36 110 ; 
       32 36 110 ; 
       33 36 110 ; 
       34 36 110 ; 
       36 35 110 ; 
       37 23 110 ; 
       38 22 110 ; 
       39 22 110 ; 
       40 23 110 ; 
       41 22 110 ; 
       42 23 110 ; 
       43 23 110 ; 
       44 22 110 ; 
       45 23 110 ; 
       46 22 110 ; 
       47 22 110 ; 
       48 23 110 ; 
       49 34 110 ; 
       50 34 110 ; 
       51 32 110 ; 
       52 33 110 ; 
       53 32 110 ; 
       54 33 110 ; 
       55 32 110 ; 
       56 33 110 ; 
       57 32 110 ; 
       58 33 110 ; 
       59 31 110 ; 
       60 31 110 ; 
       61 32 110 ; 
       62 33 110 ; 
       63 34 110 ; 
       64 34 110 ; 
       65 34 110 ; 
       66 34 110 ; 
       67 30 110 ; 
       68 30 110 ; 
       69 30 110 ; 
       70 30 110 ; 
       71 30 110 ; 
       72 30 110 ; 
       73 32 110 ; 
       74 33 110 ; 
       75 6 110 ; 
       76 11 110 ; 
       77 22 110 ; 
       78 23 110 ; 
       0 18 110 ; 
       1 18 110 ; 
       2 0 110 ; 
       16 36 110 ; 
       18 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       22 23 300 ; 
       22 24 300 ; 
       22 25 300 ; 
       23 20 300 ; 
       23 21 300 ; 
       23 22 300 ; 
       36 30 300 ; 
       37 26 300 ; 
       38 27 300 ; 
       39 27 300 ; 
       40 26 300 ; 
       41 27 300 ; 
       42 26 300 ; 
       43 26 300 ; 
       44 27 300 ; 
       45 26 300 ; 
       46 27 300 ; 
       47 27 300 ; 
       48 26 300 ; 
       49 29 300 ; 
       50 29 300 ; 
       51 28 300 ; 
       52 4 300 ; 
       53 28 300 ; 
       54 3 300 ; 
       55 28 300 ; 
       56 2 300 ; 
       57 28 300 ; 
       58 1 300 ; 
       59 8 300 ; 
       60 7 300 ; 
       61 9 300 ; 
       62 5 300 ; 
       63 10 300 ; 
       64 11 300 ; 
       65 12 300 ; 
       66 13 300 ; 
       67 14 300 ; 
       68 15 300 ; 
       69 16 300 ; 
       70 17 300 ; 
       71 18 300 ; 
       72 19 300 ; 
       73 0 300 ; 
       74 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       20 0 401 ; 
       21 1 401 ; 
       22 2 401 ; 
       23 3 401 ; 
       24 4 401 ; 
       25 5 401 ; 
       30 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM -8.221285 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 4.278719 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 1.778718 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM -10.72129 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM -0.7212844 -17.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 11.77872 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM -0.7212844 -19.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 4.278719 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 6.778718 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM -1.971284 -15.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM -1.971284 -13.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 9.278718 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM -8.221285 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM -5.721284 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM -3.221285 -19.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM -3.221285 -17.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM -1.971284 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 33.02871 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 123.0287 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 34.27871 -11.56053 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 39.27871 -11.56053 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 36.77871 -11.56053 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 44.27872 -11.56053 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 41.77872 -11.56053 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 131.7787 -11.56053 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 75.52872 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 65.52872 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 105.5287 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 55.52872 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 90.52872 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 60.52872 -5.560531 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       36 SCHEM 60.52872 -7.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 119.2787 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 24.27872 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 19.27872 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 114.2787 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 21.77872 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 116.7787 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 126.7787 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 31.77872 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 121.7787 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 26.77872 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 29.27872 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 124.2787 -11.56053 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 84.27872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 89.27872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 106.7787 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 56.77872 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 104.2787 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 54.27872 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 101.7787 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 51.77872 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 99.27872 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 49.27872 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 64.27872 -11.56053 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 66.77872 -11.56053 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       61 SCHEM 109.2787 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       62 SCHEM 59.27872 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       63 SCHEM 86.77872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       64 SCHEM 91.77872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       65 SCHEM 94.27872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       66 SCHEM 96.77872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       67 SCHEM 69.27872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       68 SCHEM 71.77872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       69 SCHEM 74.27872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       70 SCHEM 76.77872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       71 SCHEM 79.27872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       72 SCHEM 81.77872 -11.56053 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       73 SCHEM 111.7787 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       74 SCHEM 61.77872 -11.56053 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       75 SCHEM -10.72129 -13.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       76 SCHEM 6.778718 -13.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       77 SCHEM 46.77872 -11.56053 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       78 SCHEM 129.2787 -11.56053 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 14.27872 -13.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 16.77872 -13.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 14.27872 -15.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 15.52872 -9.560531 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 15.52872 -11.56053 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 104.1867 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 84.25227 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 84.25227 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 84.25227 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 84.25227 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 125.2275 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 150.4052 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 43.03374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 40.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 79.00899 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 106.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 111.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 116.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 121.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 137.4044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 142.4044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 147.4044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 152.4044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 128.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 136.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 89.70957 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 90.7544 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 86.07542 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 60.53374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 60.53374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 60.53374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 78.09919 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.23668 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 38.03374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 80.53374 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 81.00899 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 89.70957 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 90.7544 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 86.07542 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 60.53374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60.53374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 60.53374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 81.00899 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
