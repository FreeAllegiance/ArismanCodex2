SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.6-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 89     
       bounding_model-bounding_model.1-0 ROOT ; 
       bounding_model-cube42.1-0 ; 
       bounding_model-cube43.1-0 ; 
       bounding_model-cube44.1-0 ; 
       bounding_model-cube45.1-0 ; 
       bounding_model-cube46.1-0 ; 
       bounding_model-cube47.1-0 ; 
       bounding_model-cube48.1-0 ; 
       bounding_model-cube50.1-0 ; 
       bounding_model-sphere7.1-0 ; 
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube23.1-0 ; 
       root-cube24.1-0 ; 
       root-cube26.1-0 ; 
       root-cube28.1-0 ; 
       root-cube29.1-0 ; 
       root-cube3.1-0 ; 
       root-cube30.1-0 ; 
       root-cube31.1-0 ; 
       root-cube32.1-0 ; 
       root-cube35.1-0 ; 
       root-cube36.1-0 ; 
       root-cube38.1-0 ; 
       root-cube39.1-0 ; 
       root-cube4.1-0 ; 
       root-cube41.1-0 ; 
       root-cube7.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl2.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.2-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-root.3-0 ROOT ; 
       root-sphere6.2-0 ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss300-outpost.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss306_ripcord-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 46 110 ; 
       14 46 110 ; 
       15 14 110 ; 
       16 13 110 ; 
       17 22 110 ; 
       18 14 110 ; 
       19 17 110 ; 
       20 14 110 ; 
       21 14 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       22 23 110 ; 
       23 31 110 ; 
       24 14 110 ; 
       25 13 110 ; 
       27 13 110 ; 
       29 30 110 ; 
       30 22 110 ; 
       31 14 110 ; 
       32 46 110 ; 
       33 46 110 ; 
       34 32 110 ; 
       35 32 110 ; 
       36 32 110 ; 
       37 32 110 ; 
       38 32 110 ; 
       39 33 110 ; 
       40 46 110 ; 
       41 46 110 ; 
       42 46 110 ; 
       43 46 110 ; 
       44 46 110 ; 
       46 45 110 ; 
       47 33 110 ; 
       48 32 110 ; 
       49 32 110 ; 
       50 33 110 ; 
       51 32 110 ; 
       52 33 110 ; 
       53 33 110 ; 
       54 32 110 ; 
       55 33 110 ; 
       56 32 110 ; 
       57 32 110 ; 
       58 33 110 ; 
       59 44 110 ; 
       60 44 110 ; 
       61 42 110 ; 
       62 43 110 ; 
       63 42 110 ; 
       64 43 110 ; 
       65 42 110 ; 
       66 43 110 ; 
       67 42 110 ; 
       68 43 110 ; 
       69 41 110 ; 
       70 41 110 ; 
       71 42 110 ; 
       72 43 110 ; 
       73 44 110 ; 
       74 44 110 ; 
       75 44 110 ; 
       76 44 110 ; 
       77 40 110 ; 
       78 40 110 ; 
       79 40 110 ; 
       80 40 110 ; 
       81 40 110 ; 
       82 40 110 ; 
       83 42 110 ; 
       84 43 110 ; 
       85 16 110 ; 
       86 21 110 ; 
       87 32 110 ; 
       88 33 110 ; 
       3 0 110 ; 
       9 0 110 ; 
       10 28 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       11 28 110 ; 
       6 0 110 ; 
       12 10 110 ; 
       7 0 110 ; 
       26 46 110 ; 
       8 0 110 ; 
       28 26 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       32 23 300 ; 
       32 24 300 ; 
       32 25 300 ; 
       33 20 300 ; 
       33 21 300 ; 
       33 22 300 ; 
       46 30 300 ; 
       47 26 300 ; 
       48 27 300 ; 
       49 27 300 ; 
       50 26 300 ; 
       51 27 300 ; 
       52 26 300 ; 
       53 26 300 ; 
       54 27 300 ; 
       55 26 300 ; 
       56 27 300 ; 
       57 27 300 ; 
       58 26 300 ; 
       59 29 300 ; 
       60 29 300 ; 
       61 28 300 ; 
       62 4 300 ; 
       63 28 300 ; 
       64 3 300 ; 
       65 28 300 ; 
       66 2 300 ; 
       67 28 300 ; 
       68 1 300 ; 
       69 8 300 ; 
       70 7 300 ; 
       71 9 300 ; 
       72 5 300 ; 
       73 10 300 ; 
       74 11 300 ; 
       75 12 300 ; 
       76 13 300 ; 
       77 14 300 ; 
       78 15 300 ; 
       79 16 300 ; 
       80 17 300 ; 
       81 18 300 ; 
       82 19 300 ; 
       83 0 300 ; 
       84 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       20 0 401 ; 
       21 1 401 ; 
       22 2 401 ; 
       23 3 401 ; 
       24 4 401 ; 
       25 5 401 ; 
       30 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM -8.221285 -9.560531 0 MPRFLG 0 ; 
       14 SCHEM 4.278719 -9.560531 0 MPRFLG 0 ; 
       15 SCHEM 1.778718 -11.56053 0 MPRFLG 0 ; 
       16 SCHEM -10.72129 -11.56053 0 MPRFLG 0 ; 
       17 SCHEM -0.7212844 -17.56053 0 MPRFLG 0 ; 
       18 SCHEM 11.77872 -11.56053 0 MPRFLG 0 ; 
       19 SCHEM -0.7212844 -19.56053 0 MPRFLG 0 ; 
       20 SCHEM 4.278719 -11.56053 0 MPRFLG 0 ; 
       21 SCHEM 6.778718 -11.56053 0 MPRFLG 0 ; 
       1 SCHEM 57.40377 1.708077 0 MPRFLG 0 ; 
       2 SCHEM 59.90377 1.708077 0 MPRFLG 0 ; 
       22 SCHEM -1.971284 -15.56053 0 MPRFLG 0 ; 
       23 SCHEM -1.971284 -13.56053 0 MPRFLG 0 ; 
       24 SCHEM 9.278718 -11.56053 0 MPRFLG 0 ; 
       25 SCHEM -8.221285 -11.56053 0 MPRFLG 0 ; 
       27 SCHEM -5.721284 -11.56053 0 MPRFLG 0 ; 
       29 SCHEM -3.221285 -19.56053 0 MPRFLG 0 ; 
       30 SCHEM -3.221285 -17.56053 0 MPRFLG 0 ; 
       31 SCHEM -1.971284 -11.56053 0 MPRFLG 0 ; 
       32 SCHEM 33.02871 -9.560531 0 MPRFLG 0 ; 
       33 SCHEM 123.0287 -9.560531 0 MPRFLG 0 ; 
       34 SCHEM 34.27871 -11.56053 0 WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 39.27871 -11.56053 0 WIRECOL 9 7 MPRFLG 0 ; 
       36 SCHEM 36.77871 -11.56053 0 WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM 44.27872 -11.56053 0 WIRECOL 9 7 MPRFLG 0 ; 
       38 SCHEM 41.77872 -11.56053 0 WIRECOL 9 7 MPRFLG 0 ; 
       39 SCHEM 131.7787 -11.56053 0 WIRECOL 9 7 MPRFLG 0 ; 
       40 SCHEM 75.52872 -9.560531 0 MPRFLG 0 ; 
       41 SCHEM 65.52872 -9.560531 0 MPRFLG 0 ; 
       42 SCHEM 105.5287 -9.560531 0 MPRFLG 0 ; 
       43 SCHEM 55.52872 -9.560531 0 MPRFLG 0 ; 
       44 SCHEM 90.52872 -9.560531 0 MPRFLG 0 ; 
       45 SCHEM 60.52872 -5.560531 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       46 SCHEM 60.52872 -7.560531 0 MPRFLG 0 ; 
       47 SCHEM 119.2787 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 24.27872 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 19.27872 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 114.2787 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 21.77872 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 116.7787 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 126.7787 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 31.77872 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 121.7787 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 26.77872 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 29.27872 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 124.2787 -11.56053 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 84.27872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 89.27872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 106.7787 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 56.77872 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 104.2787 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 54.27872 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 101.7787 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 51.77872 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       67 SCHEM 99.27872 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       68 SCHEM 49.27872 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       69 SCHEM 64.27872 -11.56053 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 66.77872 -11.56053 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 109.2787 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       72 SCHEM 59.27872 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 86.77872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       74 SCHEM 91.77872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       75 SCHEM 94.27872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       76 SCHEM 96.77872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       77 SCHEM 69.27872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       78 SCHEM 71.77872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       79 SCHEM 74.27872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       80 SCHEM 76.77872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       81 SCHEM 79.27872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       82 SCHEM 81.77872 -11.56053 0 WIRECOL 2 7 MPRFLG 0 ; 
       83 SCHEM 111.7787 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       84 SCHEM 61.77872 -11.56053 0 WIRECOL 4 7 MPRFLG 0 ; 
       85 SCHEM -10.72129 -13.56053 0 MPRFLG 0 ; 
       86 SCHEM 6.778718 -13.56053 0 MPRFLG 0 ; 
       87 SCHEM 46.77872 -11.56053 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 129.2787 -11.56053 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.40377 1.708077 0 MPRFLG 0 ; 
       9 SCHEM 54.90377 1.708077 0 MPRFLG 0 ; 
       10 SCHEM 14.27872 -13.56053 0 MPRFLG 0 ; 
       4 SCHEM 64.90377 1.708077 0 MPRFLG 0 ; 
       5 SCHEM 67.40377 1.708077 0 MPRFLG 0 ; 
       11 SCHEM 16.77872 -13.56053 0 MPRFLG 0 ; 
       6 SCHEM 69.90377 1.708077 0 MPRFLG 0 ; 
       12 SCHEM 14.27872 -15.56053 0 MPRFLG 0 ; 
       0 SCHEM 64.90377 3.708077 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 72.40377 1.708077 0 MPRFLG 0 ; 
       26 SCHEM 15.52872 -9.560531 0 MPRFLG 0 ; 
       8 SCHEM 74.90377 1.708077 0 MPRFLG 0 ; 
       28 SCHEM 15.52872 -11.56053 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 104.1867 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 84.25227 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 84.25227 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 84.25227 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 84.25227 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 125.2275 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 150.4052 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 43.03374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 40.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 79.00899 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 106.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 111.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 116.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 121.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 137.4044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 142.4044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 147.4044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 152.4044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 128.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 136.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 89.70957 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 90.7544 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 86.07542 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 60.53374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 60.53374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 60.53374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 78.09919 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.23668 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 38.03374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 80.53374 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 81.00899 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 89.70957 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 90.7544 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 86.07542 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 60.53374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60.53374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 60.53374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 81.00899 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
