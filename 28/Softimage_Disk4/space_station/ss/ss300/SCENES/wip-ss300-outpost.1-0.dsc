SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.2-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-mat66.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 86     
       root-cube1.1-0 ; 
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube3.1-0 ROOT ; 
       root-cube4.1-0 ; 
       root-cube4_1.1-0 ; 
       root-cyl2_1.1-0 ; 
       root-null18.1-0 ROOT ; 
       root-null18_1.1-0 ROOT ; 
       root-null19.1-0 ROOT ; 
       root-null20.2-0 ; 
       root-null21.1-0 ; 
       root-null22.1-0 ; 
       root-rock.2-0 ; 
       root-root.1-0 ROOT ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       ss300_outpost-cube25.1-0 ROOT ; 
       ss300_outpost-cube27.1-0 ROOT ; 
       ss300_outpost-cube37.1-0 ROOT ; 
       ss300_outpost-east_bay_11_8.1-0 ROOT ; 
       ss300_outpost-east_bay_11_9.1-0 ROOT ; 
       ss300_outpost-extru44.1-0 ROOT ; 
       ss300_outpost-garage1A.1-0 ; 
       ss300_outpost-garage1B.1-0 ; 
       ss300_outpost-garage1C.1-0 ; 
       ss300_outpost-garage1D.1-0 ; 
       ss300_outpost-garage1E.1-0 ; 
       ss300_outpost-launch1.1-0 ; 
       ss300_outpost-SS_11.1-0 ; 
       ss300_outpost-SS_11_1.1-0 ; 
       ss300_outpost-SS_13_2.1-0 ; 
       ss300_outpost-SS_13_3.1-0 ; 
       ss300_outpost-SS_15_1.1-0 ; 
       ss300_outpost-SS_15_3.1-0 ; 
       ss300_outpost-SS_23.1-0 ; 
       ss300_outpost-SS_23_2.1-0 ; 
       ss300_outpost-SS_24.1-0 ; 
       ss300_outpost-SS_24_1.1-0 ; 
       ss300_outpost-SS_26.1-0 ; 
       ss300_outpost-SS_26_3.1-0 ; 
       ss300_outpost-turwepemt2.1-0 ; 
       ss300_outpost-turwepemt2_3.1-0 ; 
       ss306_ripcord-cube23.1-0 ; 
       ss306_ripcord-cube24.1-0 ; 
       ss306_ripcord-cube26.1-0 ; 
       ss306_ripcord-cube28.1-0 ; 
       ss306_ripcord-cube29.1-0 ; 
       ss306_ripcord-cube30.1-0 ; 
       ss306_ripcord-cube31.1-0 ; 
       ss306_ripcord-cube32.1-0 ; 
       ss306_ripcord-cube33.1-0 ; 
       ss306_ripcord-cube34.1-0 ; 
       ss306_ripcord-cube35.1-0 ; 
       ss306_ripcord-cube36.1-0 ; 
       ss306_ripcord-cube38.1-0 ; 
       ss306_ripcord-cube8.1-0 ; 
       ss306_ripcord-cube9.1-0 ; 
       ss306_ripcord-cyl2.1-0 ; 
       ss306_ripcord-sphere6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss300-outpost.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       ss304_research-rendermap2.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
       ss306_ripcord-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 4 110 ; 
       32 16 110 ; 
       33 14 110 ; 
       2 4 110 ; 
       34 14 110 ; 
       3 1 110 ; 
       35 14 110 ; 
       6 13 110 ; 
       36 14 110 ; 
       69 85 110 ; 
       37 14 110 ; 
       82 83 110 ; 
       83 79 110 ; 
       38 14 110 ; 
       39 14 110 ; 
       22 14 110 ; 
       23 14 110 ; 
       30 15 110 ; 
       24 14 110 ; 
       31 15 110 ; 
       25 14 110 ; 
       26 16 110 ; 
       27 16 110 ; 
       28 16 110 ; 
       29 16 110 ; 
       4 19 110 ; 
       5 19 110 ; 
       7 5 110 ; 
       8 4 110 ; 
       11 18 110 ; 
       12 18 110 ; 
       84 70 110 ; 
       13 12 110 ; 
       49 46 110 ; 
       50 46 110 ; 
       51 46 110 ; 
       52 46 110 ; 
       53 46 110 ; 
       54 47 110 ; 
       17 19 110 ; 
       18 7 110 ; 
       19 20 110 ; 
       55 47 110 ; 
       56 46 110 ; 
       57 46 110 ; 
       58 47 110 ; 
       59 46 110 ; 
       60 47 110 ; 
       61 47 110 ; 
       62 46 110 ; 
       63 47 110 ; 
       64 46 110 ; 
       65 46 110 ; 
       66 47 110 ; 
       67 46 110 ; 
       68 47 110 ; 
       20 21 110 ; 
       9 20 110 ; 
       40 14 110 ; 
       41 14 110 ; 
       42 14 110 ; 
       70 85 110 ; 
       71 70 110 ; 
       72 69 110 ; 
       73 79 110 ; 
       74 73 110 ; 
       75 70 110 ; 
       76 70 110 ; 
       77 69 110 ; 
       78 70 110 ; 
       79 80 110 ; 
       80 84 110 ; 
       81 70 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       32 3 300 ; 
       33 4 300 ; 
       34 5 300 ; 
       35 6 300 ; 
       36 7 300 ; 
       37 8 300 ; 
       38 9 300 ; 
       48 20 300 ; 
       39 10 300 ; 
       22 24 300 ; 
       23 24 300 ; 
       30 1 300 ; 
       24 24 300 ; 
       31 0 300 ; 
       25 24 300 ; 
       26 23 300 ; 
       27 23 300 ; 
       28 23 300 ; 
       29 23 300 ; 
       46 17 300 ; 
       46 18 300 ; 
       46 19 300 ; 
       47 14 300 ; 
       47 15 300 ; 
       47 16 300 ; 
       55 21 300 ; 
       56 22 300 ; 
       57 22 300 ; 
       58 21 300 ; 
       59 22 300 ; 
       60 21 300 ; 
       61 21 300 ; 
       62 22 300 ; 
       63 21 300 ; 
       64 22 300 ; 
       65 22 300 ; 
       66 21 300 ; 
       20 2 300 ; 
       40 11 300 ; 
       41 12 300 ; 
       42 13 300 ; 
       85 25 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       14 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       18 5 401 ; 
       19 6 401 ; 
       20 7 401 ; 
       25 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       21 SCHEM 20.03844 7.115469 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -29.59629 -18.29465 -7.299141 MPRFLG 0 ; 
       0 SCHEM 23.78844 -8.884532 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 11.28845 -0.8845316 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 8.993713 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 20.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       2 SCHEM 18.78844 -0.8845316 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 25.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       3 SCHEM 11.28845 -2.884531 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 33.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       6 SCHEM 26.28844 -8.884532 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 28.78844 3.115468 0 SRT 1 1 1 2.123366e-007 1.601602 1.769374e-007 -41.98022 -50.65072 -10.6193 MPRFLG 0 ; 
       36 SCHEM 40.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 9.288092 -38.24014 0 MPRFLG 0 ; 
       37 SCHEM 28.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       82 SCHEM 13.03809 -48.24014 0 MPRFLG 0 ; 
       83 SCHEM 13.03809 -46.24014 0 MPRFLG 0 ; 
       38 SCHEM 30.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM -2.5934 -32.07596 0 SRT 1 1 1 6.654338e-008 1.484 9.140323e-008 -1.437419 -11.54897 29.1652 MPRFLG 0 ; 
       14 SCHEM 29.66346 -22.14666 0 USR SRT 1 1 1 0 1.570796 0 4.85354 -4.10054 9.374343 MPRFLG 0 ; 
       15 SCHEM 11.25 -20 0 SRT 1 1 1 0 0.4847831 0 3.571483 45.33204 7.009114 MPRFLG 0 ; 
       16 SCHEM 3.993713 -22.03095 0 USR SRT 1 1 1 0 0 0 -2.549636 -0.1496916 8.341749 MPRFLG 0 ; 
       39 SCHEM 35.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 13.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 15.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 10 -22 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 18.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       31 SCHEM 12.5 -22 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 23.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 6.493713 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 3.993713 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 1.493714 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM -1.006286 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       4 SCHEM 15.03845 1.115469 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 23.78844 1.115469 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 23.78844 -0.8845316 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 16.28844 -0.8845316 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 21.28844 -4.884531 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 25.03844 -4.884531 0 DISPLAY 0 0 MPRFLG 0 ; 
       84 SCHEM 14.28809 -40.24014 0 MPRFLG 0 ; 
       13 SCHEM 25.03844 -6.884531 0 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 13.6566 -32.07596 0 SRT 1 0.9999999 1 0 0 0 13.65474 -13.28342 12.5402 MPRFLG 0 ; 
       47 SCHEM 40.34942 -33.78661 0 SRT 1 1 1 0 0 0 -40.67249 3.507909 -6.894608 MPRFLG 0 ; 
       49 SCHEM 14.9066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       50 SCHEM 19.9066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       51 SCHEM 17.4066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       52 SCHEM 24.9066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       53 SCHEM 22.4066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       54 SCHEM 49.09942 -35.78661 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 8.788445 1.115469 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 23.78844 -2.884531 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 17.53844 3.115468 0 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 36.59942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 4.9066 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM -0.09339941 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 31.59941 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 2.406601 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 34.09942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 44.09942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 12.4066 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 39.09942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 7.406601 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 9.906602 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 41.59942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 27.4066 -34.07595 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 46.59942 -35.78661 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20.03844 5.115468 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 31.28844 3.115468 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 38.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 43.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 45.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       85 SCHEM 18.03809 -36.24014 0 USR SRT 3.762446 3.762446 3.762446 -0.03243348 -3.255823 -1.340906 32.81688 8.656929 0.8049829 MPRFLG 0 ; 
       70 SCHEM 20.53809 -38.24014 0 MPRFLG 0 ; 
       43 SCHEM 50.97525 7.115469 0 DISPLAY 1 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       71 SCHEM 18.03809 -40.24014 0 MPRFLG 0 ; 
       44 SCHEM 55.97525 7.115469 0 DISPLAY 0 0 SRT 1.154 0.7976452 1.154 0 0 0 -42.22049 -5.232989 0.2529884 MPRFLG 0 ; 
       72 SCHEM 8.038092 -40.24014 0 MPRFLG 0 ; 
       73 SCHEM 15.53809 -46.24014 0 MPRFLG 0 ; 
       74 SCHEM 15.53809 -48.24014 0 MPRFLG 0 ; 
       75 SCHEM 20.53809 -40.24014 0 MPRFLG 0 ; 
       76 SCHEM 23.03809 -40.24014 0 MPRFLG 0 ; 
       77 SCHEM 10.53809 -40.24014 0 MPRFLG 0 ; 
       78 SCHEM 25.53809 -40.24014 0 MPRFLG 0 ; 
       79 SCHEM 14.28809 -44.24014 0 MPRFLG 0 ; 
       80 SCHEM 14.28809 -42.24014 0 MPRFLG 0 ; 
       45 SCHEM 63.47525 7.115469 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -46.18194 -11.15611 -2.094088 MPRFLG 0 ; 
       81 SCHEM 28.03809 -40.24014 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.47525 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 74.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 79.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 84.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 58.17583 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 59.22066 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 54.54168 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 175.8722 -3.799718 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.56545 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM -5.297054 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 89.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 105.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 110.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 115.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 120.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 97.46411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 104.9641 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 49.47525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49.47525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 58.17583 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59.22066 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54.54168 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 175.8722 -5.799718 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
