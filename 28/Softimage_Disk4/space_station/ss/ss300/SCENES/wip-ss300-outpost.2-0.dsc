SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.3-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 73     
       root-null18.1-0 ROOT ; 
       root-null18_1.1-0 ROOT ; 
       root-null19.1-0 ROOT ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       ss300_outpost-cube23.1-0 ; 
       ss300_outpost-cube24.1-0 ; 
       ss300_outpost-cube25.1-0 ROOT ; 
       ss300_outpost-cube26.1-0 ; 
       ss300_outpost-cube27.1-0 ROOT ; 
       ss300_outpost-cube28.1-0 ; 
       ss300_outpost-cube29.1-0 ; 
       ss300_outpost-cube3.1-0 ; 
       ss300_outpost-cube30.1-0 ; 
       ss300_outpost-cube31.1-0 ; 
       ss300_outpost-cube32.1-0 ; 
       ss300_outpost-cube33.1-0 ; 
       ss300_outpost-cube34.1-0 ; 
       ss300_outpost-cube35.1-0 ; 
       ss300_outpost-cube36.1-0 ; 
       ss300_outpost-cube37.1-0 ROOT ; 
       ss300_outpost-cube38.1-0 ; 
       ss300_outpost-cube39.1-0 ; 
       ss300_outpost-cube40.1-0 ROOT ; 
       ss300_outpost-cube41.1-0 ROOT ; 
       ss300_outpost-cube8.1-0 ; 
       ss300_outpost-cube9.1-0 ; 
       ss300_outpost-cyl2.1-0 ; 
       ss300_outpost-east_bay_11_8.1-0 ROOT ; 
       ss300_outpost-east_bay_11_9.2-0 ROOT ; 
       ss300_outpost-extru44.1-0 ROOT ; 
       ss300_outpost-garage1A.1-0 ; 
       ss300_outpost-garage1B.1-0 ; 
       ss300_outpost-garage1C.1-0 ; 
       ss300_outpost-garage1D.1-0 ; 
       ss300_outpost-garage1E.1-0 ; 
       ss300_outpost-launch1.1-0 ; 
       ss300_outpost-sphere6.2-0 ; 
       ss300_outpost-SS_11.1-0 ; 
       ss300_outpost-SS_11_1.1-0 ; 
       ss300_outpost-SS_13_2.1-0 ; 
       ss300_outpost-SS_13_3.1-0 ; 
       ss300_outpost-SS_15_1.1-0 ; 
       ss300_outpost-SS_15_3.1-0 ; 
       ss300_outpost-SS_23.1-0 ; 
       ss300_outpost-SS_23_2.1-0 ; 
       ss300_outpost-SS_24.1-0 ; 
       ss300_outpost-SS_24_1.1-0 ; 
       ss300_outpost-SS_26.1-0 ; 
       ss300_outpost-SS_26_3.1-0 ; 
       ss300_outpost-tetra1.1-0 ; 
       ss300_outpost-tetra2.1-0 ; 
       ss300_outpost-turwepemt2.1-0 ; 
       ss300_outpost-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss300/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss300-outpost.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
       ss306_ripcord-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 2 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       31 25 110 ; 
       17 0 110 ; 
       24 56 110 ; 
       18 0 110 ; 
       44 45 110 ; 
       45 37 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       11 1 110 ; 
       5 0 110 ; 
       12 1 110 ; 
       6 0 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       46 25 110 ; 
       50 47 110 ; 
       51 47 110 ; 
       52 47 110 ; 
       53 47 110 ; 
       54 47 110 ; 
       55 48 110 ; 
       57 48 110 ; 
       58 47 110 ; 
       59 47 110 ; 
       60 48 110 ; 
       61 47 110 ; 
       62 48 110 ; 
       63 48 110 ; 
       64 47 110 ; 
       65 48 110 ; 
       66 47 110 ; 
       67 47 110 ; 
       68 48 110 ; 
       71 47 110 ; 
       72 48 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       56 43 110 ; 
       25 56 110 ; 
       27 25 110 ; 
       29 24 110 ; 
       30 37 110 ; 
       32 30 110 ; 
       33 25 110 ; 
       34 25 110 ; 
       35 24 110 ; 
       36 25 110 ; 
       37 38 110 ; 
       38 46 110 ; 
       40 25 110 ; 
       41 24 110 ; 
       69 29 110 ; 
       70 34 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 2 300 ; 
       14 3 300 ; 
       15 4 300 ; 
       16 5 300 ; 
       17 6 300 ; 
       18 7 300 ; 
       19 8 300 ; 
       49 19 300 ; 
       20 9 300 ; 
       3 23 300 ; 
       4 23 300 ; 
       11 1 300 ; 
       5 23 300 ; 
       12 0 300 ; 
       6 23 300 ; 
       7 22 300 ; 
       8 22 300 ; 
       9 22 300 ; 
       10 22 300 ; 
       47 16 300 ; 
       47 17 300 ; 
       47 18 300 ; 
       48 13 300 ; 
       48 14 300 ; 
       48 15 300 ; 
       57 20 300 ; 
       58 21 300 ; 
       59 21 300 ; 
       60 20 300 ; 
       61 21 300 ; 
       62 20 300 ; 
       63 20 300 ; 
       64 21 300 ; 
       65 20 300 ; 
       66 21 300 ; 
       67 21 300 ; 
       68 20 300 ; 
       21 10 300 ; 
       22 11 300 ; 
       23 12 300 ; 
       56 24 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       13 0 401 ; 
       14 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       18 5 401 ; 
       19 6 401 ; 
       24 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 8.993713 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 20.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       15 SCHEM 25.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       16 SCHEM 33.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       31 SCHEM 31.80608 -6.908741 0 MPRFLG 0 ; 
       17 SCHEM 40.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 9.306095 -4.908745 0 MPRFLG 0 ; 
       18 SCHEM 28.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 14.3061 -14.90874 0 MPRFLG 0 ; 
       45 SCHEM 14.3061 -12.90874 0 MPRFLG 0 ; 
       19 SCHEM 30.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 47.21503 6.637709 0 USR DISPLAY 1 0 SRT 1 1 1 6.654338e-008 1.484 9.140323e-008 -1.437419 -11.54897 29.1652 MPRFLG 0 ; 
       0 SCHEM 29.66346 -22.14666 0 USR SRT 1 1 1 0 1.570796 0 4.85354 -4.10054 9.374343 MPRFLG 0 ; 
       1 SCHEM 11.25 -20 0 SRT 1 1 1 0 0.4847831 0 3.571483 45.33204 7.009114 MPRFLG 0 ; 
       2 SCHEM 3.993713 -22.03095 0 USR SRT 1 1 1 0 0 0 -2.549636 -0.1496916 8.341749 MPRFLG 0 ; 
       20 SCHEM 35.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       3 SCHEM 13.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 15.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       11 SCHEM 10 -22 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 18.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -22 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 23.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       7 SCHEM 6.493713 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       8 SCHEM 3.993713 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 1.493714 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM -1.006286 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 15.5561 -6.908741 0 MPRFLG 0 ; 
       47 SCHEM 13.6566 -32.07596 0 SRT 1 0.9999999 1 0 0 0 13.65474 -13.28342 12.5402 MPRFLG 0 ; 
       48 SCHEM 40.34942 -33.78661 0 SRT 1 1 1 0 0 0 -48.02409 3.507909 -6.894608 MPRFLG 0 ; 
       50 SCHEM 14.9066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       51 SCHEM 19.9066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       52 SCHEM 17.4066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       53 SCHEM 24.9066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       54 SCHEM 22.4066 -34.07595 0 WIRECOL 9 7 MPRFLG 0 ; 
       55 SCHEM 49.09942 -35.78661 0 WIRECOL 9 7 MPRFLG 0 ; 
       57 SCHEM 36.59942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 4.9066 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM -0.09339941 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 31.59941 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 2.406601 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 34.09942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 44.09942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 12.4066 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 39.09942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 7.406601 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 9.906602 -34.07595 0 WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 41.59942 -35.78661 0 WIRECOL 3 7 MPRFLG 0 ; 
       71 SCHEM 27.4066 -34.07595 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 46.59942 -35.78661 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 38.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 43.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 45.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 19.30609 -2.908745 0 MPRFLG 0 ; 
       25 SCHEM 23.05609 -4.908745 0 MPRFLG 0 ; 
       26 SCHEM 50.97525 7.115469 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 19.30609 -6.908741 0 MPRFLG 0 ; 
       28 SCHEM 55.97525 7.115469 0 DISPLAY 0 0 SRT 1.154 0.7976452 1.154 0 0 0 -42.22049 -5.232989 0.2529884 MPRFLG 0 ; 
       29 SCHEM 6.806098 -6.908741 0 MPRFLG 0 ; 
       30 SCHEM 16.8061 -12.90874 0 MPRFLG 0 ; 
       32 SCHEM 16.8061 -14.90874 0 MPRFLG 0 ; 
       33 SCHEM 21.80609 -6.908741 0 MPRFLG 0 ; 
       34 SCHEM 24.3061 -6.908741 0 MPRFLG 0 ; 
       35 SCHEM 9.306095 -6.908741 0 MPRFLG 0 ; 
       36 SCHEM 26.80609 -6.908741 0 MPRFLG 0 ; 
       37 SCHEM 15.5561 -10.90874 0 MPRFLG 0 ; 
       38 SCHEM 15.5561 -8.908745 0 MPRFLG 0 ; 
       39 SCHEM 63.47525 7.115469 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -46.18194 -11.15611 -2.094088 MPRFLG 0 ; 
       40 SCHEM 29.30608 -6.908741 0 MPRFLG 0 ; 
       41 SCHEM 11.8061 -6.908741 0 MPRFLG 0 ; 
       42 SCHEM 68.47525 7.115469 0 DISPLAY 0 0 SRT 2.424 0.9986878 2.424 0 0 0 9.944728 -24.19314 21.87778 MPRFLG 0 ; 
       43 SCHEM 19.30609 -0.9087417 0 USR SRT 1 1 1.82 0 0 0 4.294565 -6.206711 22.54117 MPRFLG 0 ; 
       69 SCHEM 6.806098 -8.908745 0 MPRFLG 0 ; 
       70 SCHEM 24.3061 -8.908745 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 47.47525 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 74.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 79.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 84.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 58.17583 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 59.22066 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 54.54168 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 175.8722 -3.799718 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.56545 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM -5.297054 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 49 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 89.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 105.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 110.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 115.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 120.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 97.46411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 104.9641 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49.47525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 49.47525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 58.17583 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59.22066 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54.54168 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 175.8722 -5.799718 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
