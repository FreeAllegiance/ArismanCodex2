SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.105-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       ss301_garrison-mat2.2-0 ; 
       ss301_garrison-mat58.4-0 ; 
       ss301_garrison-mat59.5-0 ; 
       ss301_garrison-mat60.3-0 ; 
       ss301_garrison-mat61.5-0 ; 
       ss301_garrison-mat62.3-0 ; 
       ss301_garrison-mat63.3-0 ; 
       ss301_garrison-mat64.3-0 ; 
       ss301_garrison-mat65.5-0 ; 
       ss301_garrison-mat66.7-0 ; 
       ss301_garrison-mat67.5-0 ; 
       ss301_garrison-mat68.4-0 ; 
       ss301_garrison-mat69.4-0 ; 
       ss301_garrison-mat70.4-0 ; 
       ss301_garrison-mat71.3-0 ; 
       ss301_garrison-mat72.5-0 ; 
       ss301_garrison-mat73.3-0 ; 
       ss301_garrison-mat74.3-0 ; 
       ss301_garrison-mat75.2-0 ; 
       ss301_garrison-mat76.2-0 ; 
       ss301_garrison-mat77.1-0 ; 
       ss301_garrison-mat78.2-0 ; 
       ss301_garrison-mat79.2-0 ; 
       ss301_garrison-mat80.2-0 ; 
       ss301_garrison-mat81.2-0 ; 
       ss301_garrison-mat82.2-0 ; 
       ss301_garrison-mat83.2-0 ; 
       ss301_garrison-mat84.3-0 ; 
       ss301_garrison-mat85.1-0 ; 
       ss301_garrison-mat86.1-0 ; 
       ss301_garrison-mat87.3-0 ; 
       ss301_garrison-mat88.2-0 ; 
       ss301_garrison-mat89.3-0 ; 
       ss301_garrison-mat90.1-0 ; 
       ss301_garrison-mat91.1-0 ; 
       ss305_elect_station-mat52.2-0 ; 
       ss305_elect_station-mat53.2-0 ; 
       ss305_elect_station-mat54.2-0 ; 
       ss305_elect_station-mat55.2-0 ; 
       ss305_elect_station-mat56.2-0 ; 
       ss305_elect_station-mat57.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       2-bmerge1.1-0 ; 
       2-cube1.1-0 ; 
       2-cube10.1-0 ; 
       2-cube11.1-0 ; 
       2-cube12.1-0 ; 
       2-cube13.1-0 ; 
       2-cube14.1-0 ; 
       2-cube16.1-0 ; 
       2-cube17.1-0 ; 
       2-cube2.1-0 ; 
       2-cube3.1-0 ; 
       2-cube4.1-0 ; 
       2-cube5.2-0 ; 
       2-cube6.1-0 ; 
       2-cube7.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ; 
       2-east_bay_11_8.1-0 ; 
       2-east_bay_11_9.1-0 ; 
       2-extru44.1-0 ; 
       2-null20.2-0 ; 
       2-skin2.74-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss301/PICTURES/beltersbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss301/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss301/PICTURES/ss301 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss301-garrison.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       ss301_garrison-t2d2.3-0 ; 
       ss301_garrison-t2d58.7-0 ; 
       ss301_garrison-t2d59.3-0 ; 
       ss301_garrison-t2d60.4-0 ; 
       ss301_garrison-t2d61.2-0 ; 
       ss301_garrison-t2d62.4-0 ; 
       ss301_garrison-t2d63.3-0 ; 
       ss301_garrison-t2d64.5-0 ; 
       ss301_garrison-t2d65.4-0 ; 
       ss301_garrison-t2d66.2-0 ; 
       ss301_garrison-t2d67.2-0 ; 
       ss301_garrison-t2d68.1-0 ; 
       ss301_garrison-t2d69.2-0 ; 
       ss301_garrison-t2d70.3-0 ; 
       ss301_garrison-t2d71.2-0 ; 
       ss301_garrison-t2d72.5-0 ; 
       ss301_garrison-t2d73.3-0 ; 
       ss301_garrison-t2d74.2-0 ; 
       ss301_garrison-t2d75.3-0 ; 
       ss301_garrison-t2d76.4-0 ; 
       ss301_garrison-t2d77.2-0 ; 
       ss301_garrison-t2d78.2-0 ; 
       ss301_garrison-t2d79.2-0 ; 
       ss301_garrison-t2d80.2-0 ; 
       ss301_garrison-t2d81.1-0 ; 
       ss301_garrison-t2d82.5-0 ; 
       ss301_garrison-t2d83.4-0 ; 
       ss301_garrison-t2d84.4-0 ; 
       ss301_garrison-t2d85.4-0 ; 
       ss301_garrison-t2d86.3-0 ; 
       ss301_garrison-t2d87.3-0 ; 
       ss301_garrison-t2d88.1-0 ; 
       ss301_garrison-t2d89.3-0 ; 
       ss301_garrison-t2d90.2-0 ; 
       ss301_garrison-t2d91.2-0 ; 
       ss305_elect_station-t2d52.3-0 ; 
       ss305_elect_station-t2d53.3-0 ; 
       ss305_elect_station-t2d54.3-0 ; 
       ss305_elect_station-t2d55.3-0 ; 
       ss305_elect_station-t2d56.3-0 ; 
       ss305_elect_station-t2d57.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 22 110 ; 
       1 17 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 11 110 ; 
       5 14 110 ; 
       6 22 110 ; 
       7 2 110 ; 
       8 11 110 ; 
       9 17 110 ; 
       10 22 110 ; 
       11 22 110 ; 
       12 22 110 ; 
       13 22 110 ; 
       14 12 110 ; 
       15 17 110 ; 
       16 15 110 ; 
       17 14 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 22 110 ; 
       21 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 20 300 ; 
       1 5 300 ; 
       2 12 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       3 11 300 ; 
       3 25 300 ; 
       4 14 300 ; 
       5 15 300 ; 
       5 27 300 ; 
       5 29 300 ; 
       6 2 300 ; 
       6 21 300 ; 
       7 13 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       8 18 300 ; 
       9 6 300 ; 
       10 3 300 ; 
       11 17 300 ; 
       11 23 300 ; 
       12 9 300 ; 
       12 34 300 ; 
       13 16 300 ; 
       13 22 300 ; 
       14 10 300 ; 
       14 24 300 ; 
       14 28 300 ; 
       15 7 300 ; 
       16 8 300 ; 
       17 4 300 ; 
       17 26 300 ; 
       18 38 300 ; 
       18 39 300 ; 
       18 40 300 ; 
       19 35 300 ; 
       19 36 300 ; 
       19 37 300 ; 
       20 19 300 ; 
       22 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 3 401 ; 
       3 5 401 ; 
       4 17 401 ; 
       5 33 401 ; 
       6 32 401 ; 
       7 12 401 ; 
       8 11 401 ; 
       9 15 401 ; 
       10 16 401 ; 
       11 18 401 ; 
       12 25 401 ; 
       13 28 401 ; 
       14 10 401 ; 
       15 19 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 9 401 ; 
       19 6 401 ; 
       20 2 401 ; 
       21 4 401 ; 
       22 7 401 ; 
       23 8 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 31 401 ; 
       27 23 401 ; 
       28 20 401 ; 
       29 24 401 ; 
       30 29 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 30 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       37 37 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 40 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 20 -8 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 30 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 25 -8 0 MPRFLG 0 ; 
       16 SCHEM 25 -10 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 35 -4 0 MPRFLG 0 ; 
       20 SCHEM 10 -2 0 MPRFLG 0 ; 
       21 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       22 SCHEM 20 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 39 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 43 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 44.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 41.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 43 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 44.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
