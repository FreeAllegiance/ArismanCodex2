SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.101-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       ss301_garrison-bound_model.2-0 ROOT ; 
       ss301_garrison-bound1.1-0 ; 
       ss301_garrison-bound2.1-0 ; 
       ss301_garrison-bound3.1-0 ; 
       ss301_garrison-bound4.1-0 ; 
       ss301_garrison-bound5.1-0 ; 
       ss301_garrison-bound6.1-0 ; 
       ss301_garrison-bound7.1-0 ; 
       ss301_garrison-bound8.1-0 ; 
       ss301_garrison-bound9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bound-ss301-garrison.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 0 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 22.50001 -2 0 USR DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
