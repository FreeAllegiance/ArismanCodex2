SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.107-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       ss301_garrison-bound_model.3-0 ROOT ; 
       ss301_garrison-bound1.1-0 ; 
       ss301_garrison-bound10.1-0 ; 
       ss301_garrison-bound11.1-0 ; 
       ss301_garrison-bound12.1-0 ; 
       ss301_garrison-bound13.1-0 ; 
       ss301_garrison-bound2.1-0 ; 
       ss301_garrison-bound3.1-0 ; 
       ss301_garrison-bound4.1-0 ; 
       ss301_garrison-bound5.1-0 ; 
       ss301_garrison-bound6.1-0 ; 
       ss301_garrison-bound7.1-0 ; 
       ss301_garrison-bound8.1-0 ; 
       ss301_garrison-bound9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bound-ss301-garrison.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       1 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       2 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 17.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 10 -2 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 15 -2 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 20 -2 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
