SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.4-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       ss301_garrison-default1.1-0 ; 
       ss301_garrison-mat2.1-0 ; 
       ss301_garrison-mat65.2-0 ; 
       ss301_garrison-mat66.2-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 66     
       2-bmerge1.1-0 ; 
       2-cube1.1-0 ; 
       2-cube10.1-0 ; 
       2-cube11.1-0 ; 
       2-cube12.1-0 ; 
       2-cube13.1-0 ; 
       2-cube14.1-0 ; 
       2-cube16.1-0 ; 
       2-cube17.1-0 ; 
       2-cube2.1-0 ; 
       2-cube3.1-0 ; 
       2-cube4.1-0 ; 
       2-cube5.2-0 ; 
       2-cube6.1-0 ; 
       2-cube7.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ; 
       2-east_bay_11_8.1-0 ; 
       2-east_bay_11_9.1-0 ; 
       2-extru44.1-0 ; 
       2-garage1A.1-0 ; 
       2-garage1B.1-0 ; 
       2-garage1C.1-0 ; 
       2-garage1D.1-0 ; 
       2-garage1E.1-0 ; 
       2-launch1.1-0 ; 
       2-null18.1-0 ; 
       2-null18_1.1-0 ; 
       2-null19.1-0 ; 
       2-null20.2-0 ; 
       2-skin2.4-0 ROOT ; 
       2-SS_11.1-0 ; 
       2-SS_11_1.1-0 ; 
       2-SS_13_2.1-0 ; 
       2-SS_13_3.1-0 ; 
       2-SS_15_1.1-0 ; 
       2-SS_15_3.1-0 ; 
       2-SS_23.1-0 ; 
       2-SS_23_2.1-0 ; 
       2-SS_24.1-0 ; 
       2-SS_24_1.1-0 ; 
       2-SS_26.1-0 ; 
       2-SS_26_3.1-0 ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_30_1.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_31_1.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       2-turwepemt2.1-0 ; 
       2-turwepemt2_3.1-0 ; 
       ss301_garrison-bound_model.1-0 ROOT ; 
       ss301_garrison-bound1.1-0 ; 
       ss301_garrison-bound2.1-0 ; 
       ss301_garrison-bound3.1-0 ; 
       ss301_garrison-bound4.1-0 ; 
       ss301_garrison-bound5.1-0 ; 
       ss301_garrison-bound6.1-0 ; 
       ss301_garrison-bound7.1-0 ; 
       ss301_garrison-bound8.1-0 ; 
       ss301_garrison-bound9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bound_N_model-ss301-garrison.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ss301_garrison-t2d2.2-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       57 56 110 ; 
       58 56 110 ; 
       59 56 110 ; 
       60 56 110 ; 
       61 56 110 ; 
       62 56 110 ; 
       63 56 110 ; 
       65 56 110 ; 
       64 56 110 ; 
       0 31 110 ; 
       1 17 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 14 110 ; 
       5 14 110 ; 
       6 31 110 ; 
       7 2 110 ; 
       8 11 110 ; 
       9 17 110 ; 
       10 31 110 ; 
       11 31 110 ; 
       12 31 110 ; 
       13 31 110 ; 
       14 12 110 ; 
       15 17 110 ; 
       16 15 110 ; 
       17 31 110 ; 
       18 30 110 ; 
       19 30 110 ; 
       20 31 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 19 110 ; 
       27 31 110 ; 
       28 31 110 ; 
       29 31 110 ; 
       30 31 110 ; 
       32 19 110 ; 
       33 18 110 ; 
       34 18 110 ; 
       35 19 110 ; 
       36 18 110 ; 
       37 19 110 ; 
       38 19 110 ; 
       39 18 110 ; 
       40 19 110 ; 
       41 18 110 ; 
       42 18 110 ; 
       43 19 110 ; 
       44 27 110 ; 
       45 27 110 ; 
       46 28 110 ; 
       47 27 110 ; 
       48 28 110 ; 
       49 27 110 ; 
       50 29 110 ; 
       51 29 110 ; 
       52 29 110 ; 
       53 29 110 ; 
       54 18 110 ; 
       55 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       18 9 300 ; 
       18 10 300 ; 
       18 11 300 ; 
       19 6 300 ; 
       19 7 300 ; 
       19 8 300 ; 
       20 12 300 ; 
       31 1 300 ; 
       32 13 300 ; 
       33 14 300 ; 
       34 14 300 ; 
       35 13 300 ; 
       36 14 300 ; 
       37 13 300 ; 
       38 13 300 ; 
       39 14 300 ; 
       40 13 300 ; 
       41 14 300 ; 
       42 14 300 ; 
       43 13 300 ; 
       44 16 300 ; 
       45 16 300 ; 
       46 5 300 ; 
       47 16 300 ; 
       48 4 300 ; 
       49 16 300 ; 
       50 15 300 ; 
       51 15 300 ; 
       52 15 300 ; 
       53 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       57 SCHEM 44.24486 7.53755 0 MPRFLG 0 ; 
       58 SCHEM 46.74486 7.53755 0 MPRFLG 0 ; 
       59 SCHEM 49.24486 7.53755 0 MPRFLG 0 ; 
       60 SCHEM 51.74486 7.53755 0 MPRFLG 0 ; 
       61 SCHEM 54.24486 7.53755 0 MPRFLG 0 ; 
       62 SCHEM 56.74486 7.53755 0 MPRFLG 0 ; 
       56 SCHEM 54.24486 9.53755 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       63 SCHEM 59.24486 7.53755 0 USR MPRFLG 0 ; 
       65 SCHEM 64.24487 7.53755 0 USR MPRFLG 0 ; 
       64 SCHEM 61.74486 7.53755 0 USR MPRFLG 0 ; 
       0 SCHEM 2.5 -1.83923 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 7.033737 -3.678458 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 22.03374 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 24.53374 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 27.03374 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 29.53374 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 7.499992 -1.83923 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 22.03374 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 17.03374 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 9.533737 -3.678458 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 5.000008 -1.83923 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 17.03374 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 25.78374 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 19.53374 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 25.78374 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 12.03374 -3.678458 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 12.03374 -5.678458 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 9.533737 -1.678458 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 65.78374 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 40.78374 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 14.53374 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 67.03374 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 72.03374 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 69.53374 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 77.03374 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 74.53374 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 49.53374 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 90.78374 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 83.28374 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 100.7837 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 55.78374 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 65.58658 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       32 SCHEM 37.03374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 57.03374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 52.03374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 32.03374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 54.53374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 34.53374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 44.53374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 64.53374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 39.53374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 59.53374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 62.03374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 42.03374 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 87.03374 -4 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 89.53374 -4 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 82.03374 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 92.03374 -4 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 84.53374 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 94.53374 -4 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 104.5337 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 102.0337 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 99.53374 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 97.03374 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 79.53374 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 47.03374 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -3.83923 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 106.0337 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 1.5 -3.83923 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -3.83923 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 83.53374 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 81.03374 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 65.31167 13.69819 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 66.35648 13.97075 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 61.67751 15.01558 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 81.03374 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 81.03374 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 81.03374 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 141.0044 3.219735 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 52.45128 8.971362 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 53.08877 15.01678 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 103.5337 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 86.03374 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 106.0337 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 65.31167 11.69819 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 66.35648 11.97075 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 61.67751 13.01558 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 81.03374 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 81.03374 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 81.03374 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 141.0044 1.219735 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
