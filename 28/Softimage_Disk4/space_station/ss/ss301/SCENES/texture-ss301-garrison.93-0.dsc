SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.97-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       ss301_garrison-mat2.2-0 ; 
       ss301_garrison-mat58.4-0 ; 
       ss301_garrison-mat59.5-0 ; 
       ss301_garrison-mat60.3-0 ; 
       ss301_garrison-mat61.5-0 ; 
       ss301_garrison-mat62.3-0 ; 
       ss301_garrison-mat63.3-0 ; 
       ss301_garrison-mat64.3-0 ; 
       ss301_garrison-mat65.5-0 ; 
       ss301_garrison-mat66.7-0 ; 
       ss301_garrison-mat67.5-0 ; 
       ss301_garrison-mat68.4-0 ; 
       ss301_garrison-mat69.4-0 ; 
       ss301_garrison-mat70.4-0 ; 
       ss301_garrison-mat71.3-0 ; 
       ss301_garrison-mat72.5-0 ; 
       ss301_garrison-mat73.3-0 ; 
       ss301_garrison-mat74.3-0 ; 
       ss301_garrison-mat75.2-0 ; 
       ss301_garrison-mat76.2-0 ; 
       ss301_garrison-mat77.1-0 ; 
       ss301_garrison-mat78.2-0 ; 
       ss301_garrison-mat79.2-0 ; 
       ss301_garrison-mat80.2-0 ; 
       ss301_garrison-mat81.2-0 ; 
       ss301_garrison-mat82.2-0 ; 
       ss301_garrison-mat83.2-0 ; 
       ss301_garrison-mat84.3-0 ; 
       ss301_garrison-mat85.1-0 ; 
       ss301_garrison-mat86.1-0 ; 
       ss301_garrison-mat87.3-0 ; 
       ss301_garrison-mat88.2-0 ; 
       ss301_garrison-mat89.3-0 ; 
       ss301_garrison-mat90.1-0 ; 
       ss301_garrison-mat91.1-0 ; 
       ss301_garrison-white_strobe1_33.2-0 ; 
       ss301_garrison-white_strobe1_34.2-0 ; 
       ss305_elect_station-mat52.2-0 ; 
       ss305_elect_station-mat53.2-0 ; 
       ss305_elect_station-mat54.2-0 ; 
       ss305_elect_station-mat55.2-0 ; 
       ss305_elect_station-mat56.2-0 ; 
       ss305_elect_station-mat57.2-0 ; 
       ss305_elect_station-white_strobe1_25.2-0 ; 
       ss305_elect_station-white_strobe1_31.2-0 ; 
       ss305_elect_station-white_strobe1_5.2-0 ; 
       ss305_elect_station-white_strobe1_9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       2-bmerge1.1-0 ; 
       2-cube1.1-0 ; 
       2-cube10.1-0 ; 
       2-cube11.1-0 ; 
       2-cube12.1-0 ; 
       2-cube13.1-0 ; 
       2-cube14.1-0 ; 
       2-cube16.1-0 ; 
       2-cube17.1-0 ; 
       2-cube2.1-0 ; 
       2-cube3.1-0 ; 
       2-cube4.1-0 ; 
       2-cube5.2-0 ; 
       2-cube6.1-0 ; 
       2-cube7.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ; 
       2-east_bay_11_8.1-0 ; 
       2-east_bay_11_9.1-0 ; 
       2-extru44.1-0 ; 
       2-garage1A.1-0 ; 
       2-garage1B.1-0 ; 
       2-garage1C.1-0 ; 
       2-garage1D.1-0 ; 
       2-garage1E.1-0 ; 
       2-launch1.1-0 ; 
       2-null18.1-0 ; 
       2-null18_1.1-0 ; 
       2-null19.1-0 ; 
       2-null20.2-0 ; 
       2-skin2.72-0 ROOT ; 
       2-SS_11.1-0 ; 
       2-SS_11_1.1-0 ; 
       2-SS_13_2.1-0 ; 
       2-SS_13_3.1-0 ; 
       2-SS_15_1.1-0 ; 
       2-SS_15_3.1-0 ; 
       2-SS_23.1-0 ; 
       2-SS_23_2.1-0 ; 
       2-SS_24.1-0 ; 
       2-SS_24_1.1-0 ; 
       2-SS_26.1-0 ; 
       2-SS_26_3.1-0 ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_30_1.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_31_1.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       2-turwepemt2.1-0 ; 
       2-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss301/PICTURES/beltersbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss301/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss301/PICTURES/ss301 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-ss301-garrison.93-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       ss301_garrison-t2d2.3-0 ; 
       ss301_garrison-t2d58.7-0 ; 
       ss301_garrison-t2d59.3-0 ; 
       ss301_garrison-t2d60.4-0 ; 
       ss301_garrison-t2d61.2-0 ; 
       ss301_garrison-t2d62.4-0 ; 
       ss301_garrison-t2d63.3-0 ; 
       ss301_garrison-t2d64.5-0 ; 
       ss301_garrison-t2d65.4-0 ; 
       ss301_garrison-t2d66.2-0 ; 
       ss301_garrison-t2d67.2-0 ; 
       ss301_garrison-t2d68.1-0 ; 
       ss301_garrison-t2d69.2-0 ; 
       ss301_garrison-t2d70.3-0 ; 
       ss301_garrison-t2d71.2-0 ; 
       ss301_garrison-t2d72.5-0 ; 
       ss301_garrison-t2d73.3-0 ; 
       ss301_garrison-t2d74.2-0 ; 
       ss301_garrison-t2d75.3-0 ; 
       ss301_garrison-t2d76.4-0 ; 
       ss301_garrison-t2d77.2-0 ; 
       ss301_garrison-t2d78.2-0 ; 
       ss301_garrison-t2d79.2-0 ; 
       ss301_garrison-t2d80.2-0 ; 
       ss301_garrison-t2d81.1-0 ; 
       ss301_garrison-t2d82.5-0 ; 
       ss301_garrison-t2d83.4-0 ; 
       ss301_garrison-t2d84.4-0 ; 
       ss301_garrison-t2d85.4-0 ; 
       ss301_garrison-t2d86.3-0 ; 
       ss301_garrison-t2d87.3-0 ; 
       ss301_garrison-t2d88.1-0 ; 
       ss301_garrison-t2d89.3-0 ; 
       ss301_garrison-t2d90.2-0 ; 
       ss301_garrison-t2d91.2-0 ; 
       ss305_elect_station-t2d52.3-0 ; 
       ss305_elect_station-t2d53.3-0 ; 
       ss305_elect_station-t2d54.3-0 ; 
       ss305_elect_station-t2d55.3-0 ; 
       ss305_elect_station-t2d56.3-0 ; 
       ss305_elect_station-t2d57.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 31 110 ; 
       1 17 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 11 110 ; 
       5 14 110 ; 
       6 31 110 ; 
       7 2 110 ; 
       8 11 110 ; 
       9 17 110 ; 
       10 31 110 ; 
       11 31 110 ; 
       12 31 110 ; 
       13 31 110 ; 
       14 12 110 ; 
       15 17 110 ; 
       16 15 110 ; 
       17 14 110 ; 
       18 30 110 ; 
       19 30 110 ; 
       20 31 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 19 110 ; 
       27 31 110 ; 
       28 31 110 ; 
       29 31 110 ; 
       30 31 110 ; 
       32 19 110 ; 
       33 18 110 ; 
       34 18 110 ; 
       35 19 110 ; 
       36 18 110 ; 
       37 19 110 ; 
       38 19 110 ; 
       39 18 110 ; 
       40 19 110 ; 
       41 18 110 ; 
       42 18 110 ; 
       43 19 110 ; 
       44 27 110 ; 
       45 27 110 ; 
       46 28 110 ; 
       47 27 110 ; 
       48 28 110 ; 
       49 27 110 ; 
       50 29 110 ; 
       51 29 110 ; 
       52 29 110 ; 
       53 29 110 ; 
       54 18 110 ; 
       55 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 20 300 ; 
       1 5 300 ; 
       2 12 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       3 11 300 ; 
       3 25 300 ; 
       4 14 300 ; 
       5 15 300 ; 
       5 27 300 ; 
       5 29 300 ; 
       6 2 300 ; 
       6 21 300 ; 
       7 13 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       8 18 300 ; 
       9 6 300 ; 
       10 3 300 ; 
       11 17 300 ; 
       11 23 300 ; 
       12 9 300 ; 
       12 34 300 ; 
       13 16 300 ; 
       13 22 300 ; 
       14 10 300 ; 
       14 24 300 ; 
       14 28 300 ; 
       15 7 300 ; 
       16 8 300 ; 
       17 4 300 ; 
       17 26 300 ; 
       18 40 300 ; 
       18 41 300 ; 
       18 42 300 ; 
       19 37 300 ; 
       19 38 300 ; 
       19 39 300 ; 
       20 19 300 ; 
       31 0 300 ; 
       32 43 300 ; 
       33 44 300 ; 
       34 44 300 ; 
       35 43 300 ; 
       36 44 300 ; 
       37 43 300 ; 
       38 43 300 ; 
       39 44 300 ; 
       40 43 300 ; 
       41 44 300 ; 
       42 44 300 ; 
       43 43 300 ; 
       44 46 300 ; 
       45 46 300 ; 
       46 36 300 ; 
       47 46 300 ; 
       48 35 300 ; 
       49 46 300 ; 
       50 45 300 ; 
       51 45 300 ; 
       52 45 300 ; 
       53 45 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 3 401 ; 
       3 5 401 ; 
       4 17 401 ; 
       5 33 401 ; 
       6 32 401 ; 
       7 12 401 ; 
       8 11 401 ; 
       9 15 401 ; 
       10 16 401 ; 
       11 18 401 ; 
       12 25 401 ; 
       13 28 401 ; 
       14 10 401 ; 
       15 19 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 9 401 ; 
       19 6 401 ; 
       20 2 401 ; 
       21 4 401 ; 
       22 7 401 ; 
       23 8 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 31 401 ; 
       27 23 401 ; 
       28 20 401 ; 
       29 24 401 ; 
       30 29 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 30 401 ; 
       34 34 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 40 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 MPRFLG 0 ; 
       5 SCHEM 70 -6 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 50 -8 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 35 -8 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       13 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       16 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       18 SCHEM 132.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 100 -4 0 MPRFLG 0 ; 
       20 SCHEM 15 -2 0 MPRFLG 0 ; 
       21 SCHEM 130 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 135 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 132.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 140 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 137.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 105 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 161.25 -2 0 MPRFLG 0 ; 
       28 SCHEM 153.75 -2 0 MPRFLG 0 ; 
       29 SCHEM 171.25 -2 0 MPRFLG 0 ; 
       30 SCHEM 118.75 -2 0 MPRFLG 0 ; 
       31 SCHEM 90 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       32 SCHEM 92.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 120 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 115 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 87.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 117.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 90 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 100 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 127.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 95 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 122.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 125 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 97.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 157.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 160 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 152.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 162.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 155 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 165 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 175 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 172.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 170 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 167.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 177.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 155 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 108 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 109.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 106.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 91.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 119 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 175 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 177.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 108 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 109.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 106.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 150 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 147.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
