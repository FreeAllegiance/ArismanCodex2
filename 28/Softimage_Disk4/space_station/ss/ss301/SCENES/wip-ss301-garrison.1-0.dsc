SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.1-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       ss301_garrison-mat2.1-0 ; 
       ss301_garrison-mat58.1-0 ; 
       ss301_garrison-mat59.1-0 ; 
       ss301_garrison-mat60.1-0 ; 
       ss301_garrison-mat61.1-0 ; 
       ss301_garrison-mat62.1-0 ; 
       ss301_garrison-mat63.1-0 ; 
       ss301_garrison-mat64.1-0 ; 
       ss301_garrison-mat65.1-0 ; 
       ss301_garrison-mat66.1-0 ; 
       ss301_garrison-mat67.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 62     
       2-bmerge1.1-0 ; 
       2-cube1.1-0 ; 
       2-cube10.1-0 ; 
       2-cube11.1-0 ; 
       2-cube12.1-0 ; 
       2-cube13.1-0 ; 
       2-cube14.1-0 ; 
       2-cube2.1-0 ; 
       2-cube4.1-0 ; 
       2-cube5.2-0 ; 
       2-cube6.1-0 ; 
       2-cube7.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ; 
       2-skin2.1-0 ROOT ; 
       ss100_nulls-extru44.1-0 ROOT ; 
       ss301_garrison-cube15.1-0 ROOT ; 
       ss301_garrison-east_bay_11_10.1-0 ROOT ; 
       ss301_garrison-null.1-0 ROOT ; 
       ss301_garrison-polygon.1-0 ; 
       ss301_garrison-polygon1.1-0 ; 
       ss301_garrison-polygon2.1-0 ; 
       ss303_ordinance-cube3.1-0 ROOT ; 
       ss303_ordinance-sphere1.1-0 ROOT ; 
       ss305_elect_station-east_bay_11_8.1-0 ; 
       ss305_elect_station-east_bay_11_9.1-0 ; 
       ss305_elect_station-garage1A.1-0 ; 
       ss305_elect_station-garage1B.1-0 ; 
       ss305_elect_station-garage1C.1-0 ; 
       ss305_elect_station-garage1D.1-0 ; 
       ss305_elect_station-garage1E.1-0 ; 
       ss305_elect_station-landing_lights_2.1-0 ; 
       ss305_elect_station-landing_lights_3.1-0 ; 
       ss305_elect_station-landing_lights2.1-0 ; 
       ss305_elect_station-landing_lights2_3.1-0 ; 
       ss305_elect_station-launch1.1-0 ; 
       ss305_elect_station-null18.1-0 ; 
       ss305_elect_station-null19.1-0 ROOT ; 
       ss305_elect_station-null20.1-0 ROOT ; 
       ss305_elect_station-SS_11.1-0 ; 
       ss305_elect_station-SS_11_1.1-0 ; 
       ss305_elect_station-SS_13_2.1-0 ; 
       ss305_elect_station-SS_13_3.1-0 ; 
       ss305_elect_station-SS_15_1.1-0 ; 
       ss305_elect_station-SS_15_3.1-0 ; 
       ss305_elect_station-SS_23.1-0 ; 
       ss305_elect_station-SS_23_2.1-0 ; 
       ss305_elect_station-SS_24.1-0 ; 
       ss305_elect_station-SS_24_1.1-0 ; 
       ss305_elect_station-SS_26.1-0 ; 
       ss305_elect_station-SS_26_3.1-0 ; 
       ss305_elect_station-SS_29.1-0 ; 
       ss305_elect_station-SS_30.1-0 ; 
       ss305_elect_station-SS_31.1-0 ; 
       ss305_elect_station-SS_32.1-0 ; 
       ss305_elect_station-SS_33.1-0 ; 
       ss305_elect_station-SS_34.1-0 ; 
       ss305_elect_station-SS_35.1-0 ; 
       ss305_elect_station-SS_36.1-0 ; 
       ss305_elect_station-turwepemt2.1-0 ; 
       ss305_elect_station-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss301-garrison.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       ss301_garrison-t2d2.1-0 ; 
       ss301_garrison-t2d58.1-0 ; 
       ss301_garrison-t2d59.1-0 ; 
       ss301_garrison-t2d60.1-0 ; 
       ss301_garrison-t2d61.1-0 ; 
       ss301_garrison-t2d62.1-0 ; 
       ss301_garrison-t2d63.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 14 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       7 14 110 ; 
       8 9 110 ; 
       9 15 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 11 110 ; 
       25 39 110 ; 
       26 39 110 ; 
       27 25 110 ; 
       28 25 110 ; 
       29 25 110 ; 
       30 25 110 ; 
       31 25 110 ; 
       32 25 110 ; 
       33 26 110 ; 
       34 25 110 ; 
       35 26 110 ; 
       36 26 110 ; 
       37 39 110 ; 
       40 33 110 ; 
       41 32 110 ; 
       42 32 110 ; 
       43 33 110 ; 
       44 32 110 ; 
       45 33 110 ; 
       46 35 110 ; 
       47 34 110 ; 
       48 35 110 ; 
       49 34 110 ; 
       50 34 110 ; 
       51 35 110 ; 
       52 37 110 ; 
       53 37 110 ; 
       54 37 110 ; 
       55 37 110 ; 
       56 38 110 ; 
       57 38 110 ; 
       58 38 110 ; 
       59 38 110 ; 
       60 25 110 ; 
       61 26 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       0 15 110 ; 
       6 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       16 17 300 ; 
       15 0 300 ; 
       9 10 300 ; 
       25 14 300 ; 
       25 15 300 ; 
       25 16 300 ; 
       26 11 300 ; 
       26 12 300 ; 
       26 13 300 ; 
       40 18 300 ; 
       41 19 300 ; 
       42 19 300 ; 
       43 18 300 ; 
       44 19 300 ; 
       45 18 300 ; 
       46 18 300 ; 
       47 19 300 ; 
       48 18 300 ; 
       49 19 300 ; 
       50 19 300 ; 
       51 18 300 ; 
       52 21 300 ; 
       53 21 300 ; 
       54 21 300 ; 
       55 21 300 ; 
       56 20 300 ; 
       57 20 300 ; 
       58 20 300 ; 
       59 20 300 ; 
       18 1 300 ; 
       18 2 300 ; 
       18 3 300 ; 
       20 4 300 ; 
       21 5 300 ; 
       22 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       11 7 401 ; 
       0 0 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       16 SCHEM 9.018858 -2.881901 0 USR SRT 0.786 0.786 0.786 0 1.484001 0 14.2777 -23.12767 7.573972 MPRFLG 0 ; 
       15 SCHEM 12.48605 -5.264392 0 USR SRT 4.040069 7.056277 4.708705 0.2298919 -0.007112224 1.601179 0 -5.446305 0.1361576 MPRFLG 0 ; 
       1 SCHEM 4.986052 -13.26439 0 MPRFLG 0 ; 
       2 SCHEM 12.48605 -11.26439 0 MPRFLG 0 ; 
       3 SCHEM 14.98605 -11.26439 0 MPRFLG 0 ; 
       4 SCHEM 17.48605 -11.26439 0 MPRFLG 0 ; 
       5 SCHEM 19.98605 -11.26439 0 MPRFLG 0 ; 
       7 SCHEM 7.486053 -13.26439 0 MPRFLG 0 ; 
       23 SCHEM 24.01886 -2.881901 0 USR SRT 11.36883 7.870412 11.36883 0 1.601602 0 -0.6084107 -37.22437 1.764431 MPRFLG 0 ; 
       8 SCHEM -0.0139485 -9.264392 0 MPRFLG 0 ; 
       9 SCHEM 9.986053 -7.264392 0 MPRFLG 0 ; 
       10 SCHEM 2.486052 -9.264392 0 MPRFLG 0 ; 
       11 SCHEM 12.48605 -9.264392 0 MPRFLG 0 ; 
       12 SCHEM 9.986053 -11.26439 0 MPRFLG 0 ; 
       13 SCHEM 9.986053 -13.26439 0 MPRFLG 0 ; 
       14 SCHEM 6.236052 -11.26439 0 MPRFLG 0 ; 
       24 SCHEM 16.51886 -2.881901 0 USR DISPLAY 0 0 SRT 4.267863 4.267863 4.267863 0 0 0 0 -4.136997 0 MPRFLG 0 ; 
       25 SCHEM 18.61199 -18.67942 0 MPRFLG 0 ; 
       26 SCHEM -6.388005 -18.67942 0 MPRFLG 0 ; 
       27 SCHEM 19.86199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 24.86199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 22.36199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 29.86199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 27.36199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 7.361996 -20.67942 0 MPRFLG 0 ; 
       33 SCHEM -12.63801 -20.67942 0 MPRFLG 0 ; 
       34 SCHEM 14.86199 -20.67942 0 MPRFLG 0 ; 
       35 SCHEM -5.138005 -20.67942 0 MPRFLG 0 ; 
       36 SCHEM 2.361996 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM 38.612 -18.67942 0 MPRFLG 0 ; 
       38 SCHEM 24.71638 -27.03216 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM 13.61199 -16.67942 0 USR SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       40 SCHEM -10.13801 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 9.861995 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 4.861996 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM -15.13801 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 7.361996 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM -12.63801 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM -2.638005 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 17.36199 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM -7.638005 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 12.36199 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 14.86199 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM -5.138005 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 34.862 -20.67942 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 37.362 -20.67942 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 39.862 -20.67942 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 42.362 -20.67942 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 28.46638 -29.03216 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 25.96638 -29.03216 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 23.46638 -29.03216 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 20.96638 -29.03216 0 WIRECOL 4 7 MPRFLG 0 ; 
       60 SCHEM 32.362 -20.67942 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM -0.1380049 -20.67942 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14.01886 -2.881901 0 USR DISPLAY 0 0 SRT 1 1 1 0 1.570796 0 -1.866284e-006 -0.2238874 5.218509 MPRFLG 0 ; 
       19 SCHEM 16.97867 2.684044 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.1237426 -3.81665 -1.383635 MPRFLG 0 ; 
       20 SCHEM 14.47867 0.6840441 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 16.97867 0.6840441 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 19.47867 0.6840441 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 22.48605 -7.264392 0 DISPLAY 1 2 MPRFLG 0 ; 
       17 SCHEM 44.862 2.684044 0 DISPLAY 0 0 SRT 0.8178252 0.6507799 1.0495 -0.02483415 -0.2745665 0.3910595 -13.3037 18.01624 -3.862124 MPRFLG 0 ; 
       6 SCHEM 24.98605 -7.264392 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       11 SCHEM 21.03819 5.787482 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 104 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 22.08301 6.060043 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 17.40403 7.104864 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 96.73093 -4.690975 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 8.177807 1.060651 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 8.815296 7.106074 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 117.2882 7.787482 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 118.333 8.060043 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 113.654 9.104864 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 94 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 99 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.362 2.684044 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 21.03819 3.787482 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.08301 4.060043 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.40403 5.104864 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 96.73093 -6.690975 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 117.2882 5.787482 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 104 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 118.333 6.060043 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 113.654 7.104864 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 94 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 99 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
