SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.2-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       ss301_garrison-default1.1-0 ; 
       ss301_garrison-mat2.1-0 ; 
       ss301_garrison-mat58.1-0 ; 
       ss301_garrison-mat59.1-0 ; 
       ss301_garrison-mat60.1-0 ; 
       ss301_garrison-mat61.1-0 ; 
       ss301_garrison-mat62.1-0 ; 
       ss301_garrison-mat63.1-0 ; 
       ss301_garrison-mat65.2-0 ; 
       ss301_garrison-mat66.2-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 64     
       2-bmerge1.1-0 ; 
       2-cube1.1-0 ; 
       2-cube10.1-0 ; 
       2-cube11.1-0 ; 
       2-cube12.1-0 ; 
       2-cube13.1-0 ; 
       2-cube14.1-0 ; 
       2-cube16.1-0 ; 
       2-cube2.1-0 ; 
       2-cube3.1-0 ; 
       2-cube4.1-0 ; 
       2-cube5.2-0 ; 
       2-cube6.1-0 ; 
       2-cube7.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ; 
       2-extru44.1-0 ; 
       2-skin2.2-0 ROOT ; 
       ss301_garrison-cube15.1-0 ROOT ; 
       ss301_garrison-cube17.1-0 ROOT ; 
       ss301_garrison-east_bay_11_10.1-0 ROOT ; 
       ss301_garrison-null.1-0 ROOT ; 
       ss301_garrison-polygon.1-0 ; 
       ss301_garrison-polygon1.1-0 ; 
       ss301_garrison-polygon2.1-0 ; 
       ss303_ordinance-sphere1.1-0 ROOT ; 
       ss305_elect_station-east_bay_11_8.1-0 ; 
       ss305_elect_station-east_bay_11_9.1-0 ; 
       ss305_elect_station-garage1A.1-0 ; 
       ss305_elect_station-garage1B.1-0 ; 
       ss305_elect_station-garage1C.1-0 ; 
       ss305_elect_station-garage1D.1-0 ; 
       ss305_elect_station-garage1E.1-0 ; 
       ss305_elect_station-landing_lights_2.1-0 ; 
       ss305_elect_station-landing_lights_3.1-0 ; 
       ss305_elect_station-landing_lights2.1-0 ; 
       ss305_elect_station-landing_lights2_3.1-0 ; 
       ss305_elect_station-launch1.1-0 ; 
       ss305_elect_station-null18.1-0 ; 
       ss305_elect_station-null19.1-0 ROOT ; 
       ss305_elect_station-null20.2-0 ROOT ; 
       ss305_elect_station-SS_11.1-0 ; 
       ss305_elect_station-SS_11_1.1-0 ; 
       ss305_elect_station-SS_13_2.1-0 ; 
       ss305_elect_station-SS_13_3.1-0 ; 
       ss305_elect_station-SS_15_1.1-0 ; 
       ss305_elect_station-SS_15_3.1-0 ; 
       ss305_elect_station-SS_23.1-0 ; 
       ss305_elect_station-SS_23_2.1-0 ; 
       ss305_elect_station-SS_24.1-0 ; 
       ss305_elect_station-SS_24_1.1-0 ; 
       ss305_elect_station-SS_26.1-0 ; 
       ss305_elect_station-SS_26_3.1-0 ; 
       ss305_elect_station-SS_29.1-0 ; 
       ss305_elect_station-SS_30.1-0 ; 
       ss305_elect_station-SS_31.1-0 ; 
       ss305_elect_station-SS_32.1-0 ; 
       ss305_elect_station-SS_33.1-0 ; 
       ss305_elect_station-SS_34.1-0 ; 
       ss305_elect_station-SS_35.1-0 ; 
       ss305_elect_station-SS_36.1-0 ; 
       ss305_elect_station-turwepemt2.1-0 ; 
       ss305_elect_station-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss301-garrison.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       ss301_garrison-t2d2.1-0 ; 
       ss301_garrison-t2d58.1-0 ; 
       ss301_garrison-t2d59.1-0 ; 
       ss301_garrison-t2d60.1-0 ; 
       ss301_garrison-t2d61.1-0 ; 
       ss301_garrison-t2d62.1-0 ; 
       ss301_garrison-t2d63.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       17 18 110 ; 
       1 16 110 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       8 16 110 ; 
       9 18 110 ; 
       10 11 110 ; 
       11 18 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 14 110 ; 
       16 13 110 ; 
       27 41 110 ; 
       28 41 110 ; 
       29 27 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 27 110 ; 
       33 27 110 ; 
       34 27 110 ; 
       35 28 110 ; 
       36 27 110 ; 
       37 28 110 ; 
       38 28 110 ; 
       39 41 110 ; 
       42 35 110 ; 
       43 34 110 ; 
       44 34 110 ; 
       45 35 110 ; 
       46 34 110 ; 
       47 35 110 ; 
       48 37 110 ; 
       49 36 110 ; 
       50 37 110 ; 
       51 36 110 ; 
       52 36 110 ; 
       53 37 110 ; 
       54 39 110 ; 
       55 39 110 ; 
       56 39 110 ; 
       57 39 110 ; 
       58 40 110 ; 
       59 40 110 ; 
       60 40 110 ; 
       61 40 110 ; 
       62 27 110 ; 
       63 28 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       0 18 110 ; 
       6 18 110 ; 
       7 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       17 16 300 ; 
       18 1 300 ; 
       27 13 300 ; 
       27 14 300 ; 
       27 15 300 ; 
       28 10 300 ; 
       28 11 300 ; 
       28 12 300 ; 
       42 17 300 ; 
       43 18 300 ; 
       44 18 300 ; 
       45 17 300 ; 
       46 18 300 ; 
       47 17 300 ; 
       48 17 300 ; 
       49 18 300 ; 
       50 17 300 ; 
       51 18 300 ; 
       52 18 300 ; 
       53 17 300 ; 
       54 20 300 ; 
       55 20 300 ; 
       56 20 300 ; 
       57 20 300 ; 
       58 19 300 ; 
       59 19 300 ; 
       60 19 300 ; 
       61 19 300 ; 
       21 2 300 ; 
       21 3 300 ; 
       21 4 300 ; 
       23 5 300 ; 
       24 6 300 ; 
       25 7 300 ; 
       0 0 300 ; 
       0 8 300 ; 
       0 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       10 7 401 ; 
       1 0 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       17 SCHEM 9.018858 -2.881901 0 USR MPRFLG 0 ; 
       18 SCHEM 12.48605 -5.264392 0 USR SRT 4.040069 7.056277 4.708705 0.2298919 -0.007112224 1.601179 0 -5.446305 0.1361576 MPRFLG 0 ; 
       1 SCHEM 4.986052 -13.26439 0 MPRFLG 0 ; 
       2 SCHEM 12.48605 -11.26439 0 MPRFLG 0 ; 
       3 SCHEM 14.98605 -11.26439 0 MPRFLG 0 ; 
       4 SCHEM 17.48605 -11.26439 0 MPRFLG 0 ; 
       5 SCHEM 19.98605 -11.26439 0 MPRFLG 0 ; 
       8 SCHEM 7.486053 -13.26439 0 MPRFLG 0 ; 
       9 SCHEM 24.01886 -2.881901 0 USR MPRFLG 0 ; 
       10 SCHEM -0.0139485 -9.264392 0 MPRFLG 0 ; 
       11 SCHEM 9.986053 -7.264392 0 MPRFLG 0 ; 
       12 SCHEM 2.486052 -9.264392 0 MPRFLG 0 ; 
       13 SCHEM 12.48605 -9.264392 0 MPRFLG 0 ; 
       14 SCHEM 9.986053 -11.26439 0 DISPLAY 3 2 MPRFLG 0 ; 
       15 SCHEM 9.986053 -13.26439 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 6.236052 -11.26439 0 MPRFLG 0 ; 
       26 SCHEM 16.51886 -2.881901 0 USR DISPLAY 0 0 SRT 4.267863 4.267863 4.267863 0 0 0 0 -4.136997 0 MPRFLG 0 ; 
       27 SCHEM 18.61199 -18.67942 0 MPRFLG 0 ; 
       28 SCHEM -6.388005 -18.67942 0 MPRFLG 0 ; 
       29 SCHEM 19.86199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 24.86199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 22.36199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 29.86199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 27.36199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 7.361996 -20.67942 0 MPRFLG 0 ; 
       35 SCHEM -12.63801 -20.67942 0 MPRFLG 0 ; 
       36 SCHEM 14.86199 -20.67942 0 MPRFLG 0 ; 
       37 SCHEM -5.138005 -20.67942 0 MPRFLG 0 ; 
       38 SCHEM 2.361996 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       39 SCHEM 38.612 -18.67942 0 MPRFLG 0 ; 
       40 SCHEM 24.71638 -27.03216 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       41 SCHEM 13.61199 -16.67942 0 USR SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       42 SCHEM -10.13801 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 9.861995 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 4.861996 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM -15.13801 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 7.361996 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM -12.63801 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM -2.638005 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 17.36199 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM -7.638005 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 12.36199 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 14.86199 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM -5.138005 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 34.862 -20.67942 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 37.362 -20.67942 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 39.862 -20.67942 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 42.362 -20.67942 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 28.46638 -29.03216 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 25.96638 -29.03216 0 WIRECOL 4 7 MPRFLG 0 ; 
       60 SCHEM 23.46638 -29.03216 0 WIRECOL 4 7 MPRFLG 0 ; 
       61 SCHEM 20.96638 -29.03216 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 32.362 -20.67942 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM -0.1380049 -20.67942 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14.01886 -2.881901 0 USR DISPLAY 0 0 SRT 1 1 1 0 1.570796 0 -1.866284e-006 -0.2238874 5.218509 MPRFLG 0 ; 
       22 SCHEM 16.97867 2.684044 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.1237426 -3.81665 -1.383635 MPRFLG 0 ; 
       23 SCHEM 14.47867 0.6840441 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 16.97867 0.6840441 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 19.47867 0.6840441 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 22.48605 -7.264392 0 MPRFLG 0 ; 
       19 SCHEM 44.862 2.684044 0 DISPLAY 0 0 SRT 0.8178252 0.6507799 1.0495 -0.02483415 -0.2745665 0.3910595 -13.3037 18.01624 -3.862124 MPRFLG 0 ; 
       6 SCHEM 24.98605 -7.264392 0 MPRFLG 0 ; 
       7 SCHEM 47.362 2.684044 0 MPRFLG 0 ; 
       20 SCHEM 49.862 2.684044 0 SRT 6.372959 6.249107 9.026852 -1.078445e-008 9.307044e-009 -4.766173e-008 90.26582 13.30595 5.263857 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       10 SCHEM 21.03819 5.787482 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 104 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 22.08301 6.060043 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.40403 7.104864 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 96.73093 -4.690975 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 8.177807 1.060651 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 8.815296 7.106074 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 117.2882 7.787482 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 118.333 8.060043 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 113.654 9.104864 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 94 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 99 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 21.03819 3.787482 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.08301 4.060043 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.40403 5.104864 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 96.73093 -6.690975 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 117.2882 5.787482 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 104 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 118.333 6.060043 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 113.654 7.104864 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 94 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 99 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
