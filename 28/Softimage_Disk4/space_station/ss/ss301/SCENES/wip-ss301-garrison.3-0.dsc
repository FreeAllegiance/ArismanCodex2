SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.3-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       ss301_garrison-default1.1-0 ; 
       ss301_garrison-mat2.1-0 ; 
       ss301_garrison-mat65.2-0 ; 
       ss301_garrison-mat66.2-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       2-bmerge1.1-0 ; 
       2-cube1.1-0 ; 
       2-cube10.1-0 ; 
       2-cube11.1-0 ; 
       2-cube12.1-0 ; 
       2-cube13.1-0 ; 
       2-cube14.1-0 ; 
       2-cube16.1-0 ; 
       2-cube17.1-0 ; 
       2-cube2.1-0 ; 
       2-cube3.1-0 ; 
       2-cube4.1-0 ; 
       2-cube5.2-0 ; 
       2-cube6.1-0 ; 
       2-cube7.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ; 
       2-east_bay_11_8.1-0 ; 
       2-east_bay_11_9.1-0 ; 
       2-extru44.1-0 ; 
       2-garage1A.1-0 ; 
       2-garage1B.1-0 ; 
       2-garage1C.1-0 ; 
       2-garage1D.1-0 ; 
       2-garage1E.1-0 ; 
       2-launch1.1-0 ; 
       2-null18.1-0 ; 
       2-null18_1.1-0 ; 
       2-null19.1-0 ; 
       2-null20.2-0 ; 
       2-skin2.3-0 ROOT ; 
       2-SS_11.1-0 ; 
       2-SS_11_1.1-0 ; 
       2-SS_13_2.1-0 ; 
       2-SS_13_3.1-0 ; 
       2-SS_15_1.1-0 ; 
       2-SS_15_3.1-0 ; 
       2-SS_23.1-0 ; 
       2-SS_23_2.1-0 ; 
       2-SS_24.1-0 ; 
       2-SS_24_1.1-0 ; 
       2-SS_26.1-0 ; 
       2-SS_26_3.1-0 ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_30_1.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_31_1.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       2-turwepemt2.1-0 ; 
       2-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss301/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss301-garrison.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ss301_garrison-t2d2.2-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       20 31 110 ; 
       1 17 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 14 110 ; 
       5 14 110 ; 
       9 17 110 ; 
       10 31 110 ; 
       11 31 110 ; 
       12 31 110 ; 
       13 31 110 ; 
       14 12 110 ; 
       15 17 110 ; 
       16 15 110 ; 
       17 31 110 ; 
       18 30 110 ; 
       19 30 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 19 110 ; 
       27 31 110 ; 
       29 31 110 ; 
       30 31 110 ; 
       32 19 110 ; 
       33 18 110 ; 
       34 18 110 ; 
       35 19 110 ; 
       36 18 110 ; 
       37 19 110 ; 
       38 19 110 ; 
       39 18 110 ; 
       40 19 110 ; 
       41 18 110 ; 
       42 18 110 ; 
       43 19 110 ; 
       44 27 110 ; 
       45 27 110 ; 
       47 27 110 ; 
       49 27 110 ; 
       50 29 110 ; 
       51 29 110 ; 
       52 29 110 ; 
       53 29 110 ; 
       54 18 110 ; 
       55 19 110 ; 
       0 31 110 ; 
       6 31 110 ; 
       7 2 110 ; 
       8 11 110 ; 
       28 31 110 ; 
       48 28 110 ; 
       46 28 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       20 12 300 ; 
       31 1 300 ; 
       18 9 300 ; 
       18 10 300 ; 
       18 11 300 ; 
       19 6 300 ; 
       19 7 300 ; 
       19 8 300 ; 
       32 13 300 ; 
       33 14 300 ; 
       34 14 300 ; 
       35 13 300 ; 
       36 14 300 ; 
       37 13 300 ; 
       38 13 300 ; 
       39 14 300 ; 
       40 13 300 ; 
       41 14 300 ; 
       42 14 300 ; 
       43 13 300 ; 
       44 16 300 ; 
       45 16 300 ; 
       47 16 300 ; 
       49 16 300 ; 
       50 15 300 ; 
       51 15 300 ; 
       52 15 300 ; 
       53 15 300 ; 
       0 0 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       48 4 300 ; 
       46 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 1 401 ; 
       1 0 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       20 SCHEM -34.12382 -8.393139 0 MPRFLG 0 ; 
       31 SCHEM 18.37617 -6.393138 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 58.37618 -10.39314 0 MPRFLG 0 ; 
       2 SCHEM -26.62383 -12.39314 0 MPRFLG 0 ; 
       3 SCHEM -24.12383 -12.39314 0 MPRFLG 0 ; 
       4 SCHEM -21.62383 -12.39314 0 MPRFLG 0 ; 
       5 SCHEM -19.12383 -12.39314 0 MPRFLG 0 ; 
       9 SCHEM 60.87618 -10.39314 0 MPRFLG 0 ; 
       10 SCHEM 68.37617 -8.393139 0 MPRFLG 0 ; 
       11 SCHEM -31.62382 -8.393139 0 MPRFLG 0 ; 
       12 SCHEM -22.87383 -8.393139 0 MPRFLG 0 ; 
       13 SCHEM -29.12383 -8.393139 0 MPRFLG 0 ; 
       14 SCHEM -22.87383 -10.39314 0 MPRFLG 0 ; 
       15 SCHEM 63.37618 -10.39314 0 MPRFLG 0 ; 
       16 SCHEM 63.37618 -12.39314 0 MPRFLG 0 ; 
       17 SCHEM 60.87618 -8.393139 0 MPRFLG 0 ; 
       18 SCHEM 17.12617 -10.39314 0 MPRFLG 0 ; 
       19 SCHEM -7.873831 -10.39314 0 MPRFLG 0 ; 
       21 SCHEM 18.37617 -12.39314 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 23.37617 -12.39314 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 20.87617 -12.39314 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 28.37617 -12.39314 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 25.87617 -12.39314 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 0.8761695 -12.39314 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 42.12618 -8.393139 0 MPRFLG 0 ; 
       29 SCHEM 52.12618 -8.393139 0 MPRFLG 0 ; 
       30 SCHEM 7.12617 -8.393139 0 MPRFLG 0 ; 
       32 SCHEM -11.62383 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 8.376169 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 3.376169 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM -16.62383 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 5.87617 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM -14.12383 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM -4.123831 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 15.87617 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM -9.12383 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 10.87617 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 13.37617 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM -6.623831 -12.39314 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 38.37618 -10.39314 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 40.87618 -10.39314 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 43.37618 -10.39314 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 45.87618 -10.39314 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 55.87618 -10.39314 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 53.37618 -10.39314 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 50.87618 -10.39314 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 48.37618 -10.39314 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 30.87617 -12.39314 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM -1.623831 -12.39314 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 65.87617 -8.393139 0 MPRFLG 0 ; 
       6 SCHEM 70.87617 -8.393139 0 MPRFLG 0 ; 
       7 SCHEM -26.62383 -14.39314 0 MPRFLG 0 ; 
       8 SCHEM -31.62382 -10.39314 0 MPRFLG 0 ; 
       28 SCHEM 34.62618 -8.393139 0 MPRFLG 0 ; 
       48 SCHEM 35.87618 -10.39314 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 33.37617 -10.39314 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 21.03819 5.787482 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 104 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 22.08301 6.060043 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.40403 7.104864 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 96.73093 -4.690975 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 8.177807 1.060651 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 8.815296 7.106074 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 77.92294 27.42161 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 77.92294 27.42161 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 21.03819 3.787482 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.08301 4.060043 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.40403 5.104864 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 96.73093 -6.690975 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 104 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
