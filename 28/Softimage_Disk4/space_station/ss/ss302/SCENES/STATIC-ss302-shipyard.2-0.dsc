SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.74-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       ss302_shipyard-mat61.4-0 ; 
       ss302_shipyard-mat62.4-0 ; 
       ss302_shipyard-mat63.5-0 ; 
       ss302_shipyard-mat64.4-0 ; 
       ss302_shipyard-mat65.4-0 ; 
       ss302_shipyard-mat66.4-0 ; 
       ss302_shipyard-mat67.5-0 ; 
       ss302_shipyard-mat68.4-0 ; 
       ss302_shipyard-mat69.4-0 ; 
       ss302_shipyard-mat70.4-0 ; 
       ss302_shipyard-mat71.6-0 ; 
       ss302_shipyard-mat72.4-0 ; 
       ss302_shipyard-mat73.4-0 ; 
       ss302_shipyard-mat74.4-0 ; 
       ss302_shipyard-mat75.5-0 ; 
       ss302_shipyard-mat76.4-0 ; 
       ss302_shipyard-mat77.5-0 ; 
       ss302_shipyard-mat78.3-0 ; 
       ss302_shipyard-mat79.4-0 ; 
       ss302_shipyard-mat80.3-0 ; 
       ss302_shipyard-mat81.2-0 ; 
       ss302_shipyard-mat82.2-0 ; 
       ss302_shipyard-mat83.2-0 ; 
       ss302_shipyard-mat84.2-0 ; 
       ss302_shipyard-mat85.1-0 ; 
       ss302_shipyard-mat86.1-0 ; 
       ss302_shipyard-mat87.1-0 ; 
       ss302_shipyard-mat88.1-0 ; 
       ss305_elect_station-mat52.3-0 ; 
       ss305_elect_station-mat53.3-0 ; 
       ss305_elect_station-mat54.3-0 ; 
       ss305_elect_station-mat55.3-0 ; 
       ss305_elect_station-mat56.3-0 ; 
       ss305_elect_station-mat57.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       root-cube2.1-0 ; 
       root-cube3.1-0 ; 
       root-cube35.1-0 ; 
       root-cube36.1-0 ; 
       root-cube4.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube46.1-0 ; 
       root-cube46_1.1-0 ; 
       root-cube46_2.1-0 ; 
       root-cube46_3.1-0 ; 
       root-cube46_4.1-0 ; 
       root-cube48.1-0 ; 
       root-cube49.1-0 ; 
       root-cube5.1-0 ; 
       root-cube5_1.1-0 ; 
       root-cube5_2.1-0 ; 
       root-cube50.2-0 ; 
       root-cube8.2-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.2-0 ; 
       root-null22.2-0 ; 
       root-null24.1-0 ; 
       root-root.55-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere8.1-0 ; 
       root-tetra2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss302/PICTURES/beltersbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss302/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss302/PICTURES/ss18a ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss302/PICTURES/ss302 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss302-shipyard.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       ss302_shipyard-t2d61.5-0 ; 
       ss302_shipyard-t2d62.6-0 ; 
       ss302_shipyard-t2d63.4-0 ; 
       ss302_shipyard-t2d64.3-0 ; 
       ss302_shipyard-t2d65.2-0 ; 
       ss302_shipyard-t2d66.5-0 ; 
       ss302_shipyard-t2d67.3-0 ; 
       ss302_shipyard-t2d68.3-0 ; 
       ss302_shipyard-t2d69.2-0 ; 
       ss302_shipyard-t2d70.2-0 ; 
       ss302_shipyard-t2d71.5-0 ; 
       ss302_shipyard-t2d72.3-0 ; 
       ss302_shipyard-t2d73.3-0 ; 
       ss302_shipyard-t2d74.3-0 ; 
       ss302_shipyard-t2d75.3-0 ; 
       ss302_shipyard-t2d76.2-0 ; 
       ss302_shipyard-t2d77.2-0 ; 
       ss302_shipyard-t2d78.3-0 ; 
       ss302_shipyard-t2d79.2-0 ; 
       ss302_shipyard-t2d80.3-0 ; 
       ss302_shipyard-t2d81.2-0 ; 
       ss302_shipyard-t2d82.1-0 ; 
       ss302_shipyard-t2d83.1-0 ; 
       ss302_shipyard-t2d84.1-0 ; 
       ss302_shipyard-t2d85.1-0 ; 
       ss302_shipyard-t2d86.2-0 ; 
       ss302_shipyard-t2d87.2-0 ; 
       ss305_elect_station-t2d52.6-0 ; 
       ss305_elect_station-t2d53.6-0 ; 
       ss305_elect_station-t2d54.6-0 ; 
       ss305_elect_station-t2d55.6-0 ; 
       ss305_elect_station-t2d56.6-0 ; 
       ss305_elect_station-t2d57.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 14 110 ; 
       2 3 110 ; 
       3 5 110 ; 
       4 16 110 ; 
       5 21 110 ; 
       6 22 110 ; 
       7 24 110 ; 
       8 25 110 ; 
       9 25 110 ; 
       10 25 110 ; 
       11 24 110 ; 
       12 25 110 ; 
       13 7 110 ; 
       14 10 110 ; 
       15 24 110 ; 
       16 15 110 ; 
       17 24 110 ; 
       18 13 110 ; 
       19 6 110 ; 
       20 5 110 ; 
       21 22 110 ; 
       22 23 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       0 23 300 ; 
       1 7 300 ; 
       2 25 300 ; 
       3 24 300 ; 
       4 14 300 ; 
       4 22 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       5 20 300 ; 
       6 16 300 ; 
       6 21 300 ; 
       7 9 300 ; 
       8 2 300 ; 
       9 3 300 ; 
       10 4 300 ; 
       11 15 300 ; 
       12 8 300 ; 
       13 10 300 ; 
       13 26 300 ; 
       14 5 300 ; 
       15 12 300 ; 
       16 13 300 ; 
       17 27 300 ; 
       18 11 300 ; 
       19 31 300 ; 
       19 32 300 ; 
       19 33 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       20 30 300 ; 
       24 0 300 ; 
       25 1 300 ; 
       26 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 8 401 ; 
       3 17 401 ; 
       4 9 401 ; 
       5 14 401 ; 
       6 25 401 ; 
       7 23 401 ; 
       8 7 401 ; 
       9 12 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 15 401 ; 
       13 13 401 ; 
       14 24 401 ; 
       15 16 401 ; 
       16 5 401 ; 
       18 2 401 ; 
       19 3 401 ; 
       20 4 401 ; 
       21 6 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       25 26 401 ; 
       26 21 401 ; 
       27 22 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 29 401 ; 
       31 30 401 ; 
       32 31 401 ; 
       33 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -8 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 25 -4 0 MPRFLG 0 ; 
       10 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 5 -6 0 MPRFLG 0 ; 
       14 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       15 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 10 -4 0 MPRFLG 0 ; 
       18 SCHEM 5 -8 0 MPRFLG 0 ; 
       19 SCHEM 15 -6 0 MPRFLG 0 ; 
       20 SCHEM 20 -8 0 MPRFLG 0 ; 
       21 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       23 SCHEM 17.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       25 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       26 SCHEM 12.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 28 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 28 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
