SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.91-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       ss302_shipyard-mat61.4-0 ; 
       ss302_shipyard-mat62.4-0 ; 
       ss302_shipyard-mat63.5-0 ; 
       ss302_shipyard-mat64.4-0 ; 
       ss302_shipyard-mat65.4-0 ; 
       ss302_shipyard-mat66.4-0 ; 
       ss302_shipyard-mat67.5-0 ; 
       ss302_shipyard-mat68.4-0 ; 
       ss302_shipyard-mat69.4-0 ; 
       ss302_shipyard-mat70.4-0 ; 
       ss302_shipyard-mat71.6-0 ; 
       ss302_shipyard-mat72.4-0 ; 
       ss302_shipyard-mat73.4-0 ; 
       ss302_shipyard-mat74.4-0 ; 
       ss302_shipyard-mat75.5-0 ; 
       ss302_shipyard-mat76.4-0 ; 
       ss302_shipyard-mat77.5-0 ; 
       ss302_shipyard-mat78.3-0 ; 
       ss302_shipyard-mat82.2-0 ; 
       ss302_shipyard-mat83.2-0 ; 
       ss302_shipyard-mat84.2-0 ; 
       ss302_shipyard-mat87.1-0 ; 
       ss302_shipyard-mat88.1-0 ; 
       ss305_elect_station-mat55.3-0 ; 
       ss305_elect_station-mat56.3-0 ; 
       ss305_elect_station-mat57.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       root-cube2.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube43.1-0 ; 
       root-cube46.1-0 ; 
       root-cube46_1.1-0 ; 
       root-cube46_2.1-0 ; 
       root-cube46_3.1-0 ; 
       root-cube46_4.1-0 ; 
       root-cube48.1-0 ; 
       root-cube49.1-0 ; 
       root-cube5.1-0 ; 
       root-cube5_1.1-0 ; 
       root-cube5_2.1-0 ; 
       root-cube50.2-0 ; 
       root-cube8.2-0 ; 
       root-gar444age1.1-0 ; 
       root-null24.1-0 ; 
       root-root.66-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere8.1-0 ; 
       root-tetra2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss302/PICTURES/beltersbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss302/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss302/PICTURES/ss18a ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss302/PICTURES/ss302 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss302-shipyard.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       ss302_shipyard-t2d61.9-0 ; 
       ss302_shipyard-t2d62.10-0 ; 
       ss302_shipyard-t2d66.9-0 ; 
       ss302_shipyard-t2d67.7-0 ; 
       ss302_shipyard-t2d68.7-0 ; 
       ss302_shipyard-t2d69.6-0 ; 
       ss302_shipyard-t2d70.6-0 ; 
       ss302_shipyard-t2d71.9-0 ; 
       ss302_shipyard-t2d72.7-0 ; 
       ss302_shipyard-t2d73.7-0 ; 
       ss302_shipyard-t2d74.7-0 ; 
       ss302_shipyard-t2d75.7-0 ; 
       ss302_shipyard-t2d76.6-0 ; 
       ss302_shipyard-t2d77.6-0 ; 
       ss302_shipyard-t2d78.7-0 ; 
       ss302_shipyard-t2d79.6-0 ; 
       ss302_shipyard-t2d80.7-0 ; 
       ss302_shipyard-t2d82.5-0 ; 
       ss302_shipyard-t2d83.5-0 ; 
       ss302_shipyard-t2d84.5-0 ; 
       ss302_shipyard-t2d85.5-0 ; 
       ss302_shipyard-t2d86.6-0 ; 
       ss305_elect_station-t2d55.10-0 ; 
       ss305_elect_station-t2d56.10-0 ; 
       ss305_elect_station-t2d57.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 11 110 ; 
       2 13 110 ; 
       3 17 110 ; 
       4 19 110 ; 
       5 20 110 ; 
       6 20 110 ; 
       7 20 110 ; 
       8 19 110 ; 
       9 20 110 ; 
       10 4 110 ; 
       11 7 110 ; 
       12 19 110 ; 
       13 12 110 ; 
       14 19 110 ; 
       15 10 110 ; 
       16 3 110 ; 
       17 18 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       0 20 300 ; 
       1 7 300 ; 
       2 14 300 ; 
       2 19 300 ; 
       3 16 300 ; 
       3 18 300 ; 
       4 9 300 ; 
       5 2 300 ; 
       6 3 300 ; 
       7 4 300 ; 
       8 15 300 ; 
       9 8 300 ; 
       10 10 300 ; 
       10 21 300 ; 
       11 5 300 ; 
       12 12 300 ; 
       13 13 300 ; 
       14 22 300 ; 
       15 11 300 ; 
       16 23 300 ; 
       16 24 300 ; 
       16 25 300 ; 
       19 0 300 ; 
       20 1 300 ; 
       21 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 5 401 ; 
       3 14 401 ; 
       4 6 401 ; 
       5 11 401 ; 
       6 21 401 ; 
       7 19 401 ; 
       8 4 401 ; 
       9 9 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 12 401 ; 
       13 10 401 ; 
       14 20 401 ; 
       15 13 401 ; 
       16 2 401 ; 
       18 3 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 25 -8 0 MPRFLG 0 ; 
       1 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 MPRFLG 0 ; 
       7 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 MPRFLG 0 ; 
       11 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 10 -4 0 MPRFLG 0 ; 
       15 SCHEM 5 -8 0 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 MPRFLG 0 ; 
       17 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       18 SCHEM 15 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
