SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.8-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       ss302_shipyard-light1.4-0 ROOT ; 
       ss302_shipyard-light2.4-0 ROOT ; 
       ss302_shipyard-light3.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss302_shipyard-mat61.1-0 ; 
       ss302_shipyard-mat62.1-0 ; 
       ss302_shipyard-white_strobe1_53.1-0 ; 
       ss302_shipyard-white_strobe1_54.1-0 ; 
       ss302_shipyard-white_strobe1_55.1-0 ; 
       ss302_shipyard-white_strobe1_56.1-0 ; 
       ss302_shipyard-white_strobe1_57.1-0 ; 
       ss302_shipyard-white_strobe1_58.1-0 ; 
       ss302_shipyard-white_strobe1_59.1-0 ; 
       ss302_shipyard-white_strobe1_60.1-0 ; 
       ss302_shipyard-white_strobe1_61.1-0 ; 
       ss302_shipyard-white_strobe1_62.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 88     
       root-null18.1-0 ROOT ; 
       root-null18_1.1-0 ROOT ; 
       root-null19.1-0 ROOT ; 
       root-null19_1.1-0 ROOT ; 
       root-null20.1-0 ROOT ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
       root-SS_62.1-0 ; 
       root-SS_63.1-0 ; 
       ss302_shipyard-cube2.1-0 ; 
       ss302_shipyard-cube3.1-0 ; 
       ss302_shipyard-cube35.1-0 ; 
       ss302_shipyard-cube36.1-0 ; 
       ss302_shipyard-cube4.1-0 ; 
       ss302_shipyard-cube42.1-0 ; 
       ss302_shipyard-cube43.1-0 ; 
       ss302_shipyard-cube46.1-0 ; 
       ss302_shipyard-cube46_1.1-0 ; 
       ss302_shipyard-cube46_2.1-0 ; 
       ss302_shipyard-cube46_3.1-0 ; 
       ss302_shipyard-cube46_4.1-0 ; 
       ss302_shipyard-cube48.1-0 ; 
       ss302_shipyard-cube49.1-0 ; 
       ss302_shipyard-cube5.1-0 ; 
       ss302_shipyard-cube5_1.1-0 ; 
       ss302_shipyard-cube5_2.1-0 ; 
       ss302_shipyard-cube8.2-0 ; 
       ss302_shipyard-east_bay_11_8.1-0 ; 
       ss302_shipyard-east_bay_11_9.2-0 ; 
       ss302_shipyard-garage1A.2-0 ; 
       ss302_shipyard-garage1B.2-0 ; 
       ss302_shipyard-garage1C.2-0 ; 
       ss302_shipyard-garage1D.2-0 ; 
       ss302_shipyard-garage1E.2-0 ; 
       ss302_shipyard-launch1.1-0 ; 
       ss302_shipyard-null22.2-0 ; 
       ss302_shipyard-null23.4-0 ROOT ; 
       ss302_shipyard-null24.1-0 ; 
       ss302_shipyard-sphere6.1-0 ; 
       ss302_shipyard-sphere8.1-0 ; 
       ss302_shipyard-spline1.1-0 ROOT ; 
       ss302_shipyard-SS_11.1-0 ; 
       ss302_shipyard-SS_11_1.2-0 ; 
       ss302_shipyard-SS_13_2.2-0 ; 
       ss302_shipyard-SS_13_3.1-0 ; 
       ss302_shipyard-SS_15_1.2-0 ; 
       ss302_shipyard-SS_15_3.1-0 ; 
       ss302_shipyard-SS_23.1-0 ; 
       ss302_shipyard-SS_23_2.2-0 ; 
       ss302_shipyard-SS_24.1-0 ; 
       ss302_shipyard-SS_24_1.2-0 ; 
       ss302_shipyard-SS_26.2-0 ; 
       ss302_shipyard-SS_26_3.1-0 ; 
       ss302_shipyard-tetra2.1-0 ; 
       ss302_shipyard-turwepemt2.2-0 ; 
       ss302_shipyard-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss302-shipyard.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ss302_shipyard-t2d61.1-0 ; 
       ss302_shipyard-t2d62.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       52 70 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       39 1 110 ; 
       40 1 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 2 110 ; 
       8 3 110 ; 
       9 2 110 ; 
       10 3 110 ; 
       11 2 110 ; 
       12 3 110 ; 
       13 2 110 ; 
       14 3 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 2 110 ; 
       18 3 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 4 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 2 110 ; 
       30 3 110 ; 
       41 55 110 ; 
       42 55 110 ; 
       43 44 110 ; 
       44 46 110 ; 
       45 57 110 ; 
       46 67 110 ; 
       47 69 110 ; 
       50 71 110 ; 
       48 70 110 ; 
       55 51 110 ; 
       58 54 110 ; 
       59 47 110 ; 
       60 46 110 ; 
       61 59 110 ; 
       62 59 110 ; 
       63 59 110 ; 
       64 59 110 ; 
       65 59 110 ; 
       66 60 110 ; 
       69 68 110 ; 
       67 69 110 ; 
       71 68 110 ; 
       73 60 110 ; 
       74 59 110 ; 
       75 59 110 ; 
       76 60 110 ; 
       77 59 110 ; 
       78 60 110 ; 
       79 60 110 ; 
       80 59 110 ; 
       81 60 110 ; 
       82 59 110 ; 
       83 59 110 ; 
       84 60 110 ; 
       85 47 110 ; 
       86 59 110 ; 
       87 60 110 ; 
       70 68 110 ; 
       57 56 110 ; 
       56 70 110 ; 
       49 71 110 ; 
       51 71 110 ; 
       53 71 110 ; 
       54 48 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       31 11 300 ; 
       32 12 300 ; 
       33 13 300 ; 
       34 14 300 ; 
       35 15 300 ; 
       36 16 300 ; 
       37 17 300 ; 
       38 18 300 ; 
       39 19 300 ; 
       40 20 300 ; 
       5 41 300 ; 
       6 41 300 ; 
       7 40 300 ; 
       8 4 300 ; 
       9 40 300 ; 
       10 3 300 ; 
       11 40 300 ; 
       12 2 300 ; 
       13 40 300 ; 
       14 1 300 ; 
       15 8 300 ; 
       16 7 300 ; 
       17 21 300 ; 
       18 5 300 ; 
       19 22 300 ; 
       20 23 300 ; 
       21 24 300 ; 
       22 25 300 ; 
       23 26 300 ; 
       24 27 300 ; 
       25 28 300 ; 
       26 29 300 ; 
       27 30 300 ; 
       28 31 300 ; 
       29 0 300 ; 
       30 6 300 ; 
       59 35 300 ; 
       59 36 300 ; 
       59 37 300 ; 
       60 32 300 ; 
       60 33 300 ; 
       60 34 300 ; 
       71 10 300 ; 
       73 38 300 ; 
       74 39 300 ; 
       75 39 300 ; 
       76 38 300 ; 
       77 39 300 ; 
       78 38 300 ; 
       79 38 300 ; 
       80 39 300 ; 
       81 38 300 ; 
       82 39 300 ; 
       83 39 300 ; 
       84 38 300 ; 
       70 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 0 401 ; 
       10 1 401 ; 
       32 2 401 ; 
       33 3 401 ; 
       34 4 401 ; 
       35 5 401 ; 
       36 6 401 ; 
       37 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 91.43105 18.83884 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 93.93105 18.83884 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 96.43105 18.83884 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       52 SCHEM 20.42847 5.76847 0 MPRFLG 0 ; 
       72 SCHEM 98.93105 19.2441 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       31 SCHEM 31.32855 24.95268 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 33.82855 24.95268 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.32855 24.95268 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 38.82855 24.95268 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 41.32855 24.95268 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 30.82452 22.27645 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 33.32452 22.27645 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 35.82453 22.27645 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 38.32453 22.27645 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 40.82453 22.27645 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 68.00395 -19.08405 0 USR SRT 0.974 0.974 0.974 1.418776e-008 1.748817 -2.967078e-008 -44.9987 -4.771349 -29.57491 MPRFLG 0 ; 
       1 SCHEM 33.82855 26.95268 0 USR SRT 1 1 1 -6.987861e-008 2.055579 -5.739427e-008 11.15207 45.33204 -63.89658 MPRFLG 0 ; 
       2 SCHEM 50.82722 28.10422 0 USR SRT 1.579394 1.579394 1.579394 3.925611e-009 9.276415e-008 -2.275048e-008 -69.86872 -6.954268 -18.00755 MPRFLG 0 ; 
       3 SCHEM 52.87391 24.37746 0 USR SRT 0.6728219 0.6728219 0.6728219 3.925611e-009 9.276415e-008 -2.275048e-008 -44.35632 -15.45752 -6.447072 MPRFLG 0 ; 
       4 SCHEM 66.70353 -14.70935 0 USR SRT 1.25 1.25 1.25 3.925611e-009 9.276415e-008 -2.275048e-008 -47.51837 19.40459 -17.82138 MPRFLG 0 ; 
       5 SCHEM 60.45353 -16.70935 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       6 SCHEM 65.45353 -16.70935 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       7 SCHEM 52.07722 26.10423 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       8 SCHEM 51.93747 21.44041 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 49.57721 26.10423 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM 49.43747 21.44041 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 47.07723 26.10423 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 46.93747 21.44041 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 44.57723 26.10423 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 44.43747 21.44041 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 26.32855 24.95268 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 28.82855 24.95268 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 54.57721 26.10423 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 55.38052 21.44492 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 62.95353 -16.70935 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 67.95353 -16.70935 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 70.45353 -16.70935 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 72.95353 -16.70935 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 61.75395 -21.08405 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 64.25395 -21.08405 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 66.75395 -21.08405 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 69.25395 -21.08405 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 71.75395 -21.08405 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 74.25396 -21.08405 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 57.0772 26.10423 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 57.88052 21.44492 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 62.04991 -1.745024 0 MPRFLG 0 ; 
       42 SCHEM 59.54991 -1.745024 0 MPRFLG 0 ; 
       43 SCHEM 41.44214 -5.907173 0 MPRFLG 0 ; 
       44 SCHEM 41.44214 -3.907173 0 MPRFLG 0 ; 
       45 SCHEM 15.42848 1.76847 0 MPRFLG 0 ; 
       46 SCHEM 51.44215 -1.907173 0 MPRFLG 0 ; 
       47 SCHEM 23.94213 0.09282671 0 MPRFLG 0 ; 
       50 SCHEM 57.04991 2.254976 0 MPRFLG 0 ; 
       48 SCHEM 17.92847 5.76847 0 MPRFLG 0 ; 
       55 SCHEM 60.79991 0.2549767 0 MPRFLG 0 ; 
       58 SCHEM 17.92847 1.76847 0 MPRFLG 0 ; 
       59 SCHEM 25.19213 -1.907173 0 MPRFLG 0 ; 
       60 SCHEM 52.69215 -3.907173 0 MPRFLG 0 ; 
       61 SCHEM 26.44213 -3.907173 0 WIRECOL 9 7 MPRFLG 0 ; 
       62 SCHEM 31.44214 -3.907173 0 WIRECOL 9 7 MPRFLG 0 ; 
       63 SCHEM 28.94213 -3.907173 0 WIRECOL 9 7 MPRFLG 0 ; 
       64 SCHEM 36.44214 -3.907173 0 WIRECOL 9 7 MPRFLG 0 ; 
       65 SCHEM 33.94214 -3.907173 0 WIRECOL 9 7 MPRFLG 0 ; 
       66 SCHEM 61.44215 -5.907173 0 WIRECOL 9 7 MPRFLG 0 ; 
       69 SCHEM 31.67864 6.552261 0 USR MPRFLG 0 ; 
       67 SCHEM 51.44215 0.09282671 0 MPRFLG 0 ; 
       71 SCHEM 59.54991 4.254976 0 USR MPRFLG 0 ; 
       73 SCHEM 48.94215 -5.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 16.44214 -3.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 11.44214 -3.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 43.94214 -5.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 13.94214 -3.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 46.44215 -5.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 56.44215 -5.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 23.94213 -3.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 51.44215 -5.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 18.94214 -3.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 21.44214 -3.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 53.94215 -5.907173 0 WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 8.942143 -1.907173 0 MPRFLG 0 ; 
       86 SCHEM 38.94214 -3.907173 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 58.94215 -5.907173 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 17.92847 7.76847 0 USR MPRFLG 0 ; 
       57 SCHEM 15.42848 3.76847 0 MPRFLG 0 ; 
       56 SCHEM 15.42848 5.76847 0 MPRFLG 0 ; 
       49 SCHEM 54.54991 2.254976 0 MPRFLG 0 ; 
       51 SCHEM 60.79991 2.254976 0 MPRFLG 0 ; 
       68 SCHEM 29.65157 17.60626 0 USR SRT 1 0.9999999 0.9999999 4.371141e-008 3.178651e-008 -1.270549e-021 2.399017e-008 8.729084e-008 -2.119473e-009 MPRFLG 0 ; 
       53 SCHEM 64.5499 2.254976 0 MPRFLG 0 ; 
       54 SCHEM 17.92847 3.76847 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 128.6867 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 149.7275 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 174.9052 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 65.03374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 130.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 135.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 140.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 35.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 103.509 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 130.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 135.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 140.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 145.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 161.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 166.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 171.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 176.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 153.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 160.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 114.2096 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 115.2544 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 110.5754 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 102.5992 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 50.73668 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 62.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 105.0337 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 145.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 195.9586 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 192.2086 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 199.7086 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 207.2086 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 147.0674 20.18679 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 152.0674 20.18679 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 35.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 114.2096 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 115.2544 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 110.5754 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
