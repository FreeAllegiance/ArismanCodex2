SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.10-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       ss302_shipyard-light1.6-0 ROOT ; 
       ss302_shipyard-light2.6-0 ROOT ; 
       ss302_shipyard-light3.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss302_shipyard-mat61.1-0 ; 
       ss302_shipyard-mat62.1-0 ; 
       ss302_shipyard-white_strobe1_53.1-0 ; 
       ss302_shipyard-white_strobe1_54.1-0 ; 
       ss302_shipyard-white_strobe1_55.1-0 ; 
       ss302_shipyard-white_strobe1_56.1-0 ; 
       ss302_shipyard-white_strobe1_57.1-0 ; 
       ss302_shipyard-white_strobe1_58.1-0 ; 
       ss302_shipyard-white_strobe1_59.1-0 ; 
       ss302_shipyard-white_strobe1_60.1-0 ; 
       ss302_shipyard-white_strobe1_61.1-0 ; 
       ss302_shipyard-white_strobe1_62.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 99     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bound07.1-0 ; 
       bounding_model-bound08.1-0 ; 
       bounding_model-bound09.1-0 ; 
       bounding_model-bounding_model.1-0 ROOT ; 
       bounding_model-cube50.1-0 ROOT ; 
       bounding_model-spline1.1-0 ROOT ; 
       ss302_shipyard-cube2.1-0 ; 
       ss302_shipyard-cube3.1-0 ; 
       ss302_shipyard-cube35.1-0 ; 
       ss302_shipyard-cube36.1-0 ; 
       ss302_shipyard-cube4.1-0 ; 
       ss302_shipyard-cube42.1-0 ; 
       ss302_shipyard-cube43.1-0 ; 
       ss302_shipyard-cube46.1-0 ; 
       ss302_shipyard-cube46_1.1-0 ; 
       ss302_shipyard-cube46_2.1-0 ; 
       ss302_shipyard-cube46_3.1-0 ; 
       ss302_shipyard-cube46_4.1-0 ; 
       ss302_shipyard-cube48.1-0 ; 
       ss302_shipyard-cube49.1-0 ; 
       ss302_shipyard-cube5.1-0 ; 
       ss302_shipyard-cube5_1.1-0 ; 
       ss302_shipyard-cube5_2.1-0 ; 
       ss302_shipyard-cube8.2-0 ; 
       ss302_shipyard-east_bay_11_8.1-0 ; 
       ss302_shipyard-east_bay_11_9.2-0 ; 
       ss302_shipyard-garage1A.2-0 ; 
       ss302_shipyard-garage1B.2-0 ; 
       ss302_shipyard-garage1C.2-0 ; 
       ss302_shipyard-garage1D.2-0 ; 
       ss302_shipyard-garage1E.2-0 ; 
       ss302_shipyard-launch1.1-0 ; 
       ss302_shipyard-null18.1-0 ; 
       ss302_shipyard-null18_1.1-0 ; 
       ss302_shipyard-null19.1-0 ; 
       ss302_shipyard-null19_1.1-0 ; 
       ss302_shipyard-null20.1-0 ; 
       ss302_shipyard-null22.2-0 ; 
       ss302_shipyard-null23.6-0 ROOT ; 
       ss302_shipyard-null24.1-0 ; 
       ss302_shipyard-sphere6.1-0 ; 
       ss302_shipyard-sphere8.1-0 ; 
       ss302_shipyard-SS_11.1-0 ; 
       ss302_shipyard-SS_11_1.2-0 ; 
       ss302_shipyard-SS_13_2.2-0 ; 
       ss302_shipyard-SS_13_3.1-0 ; 
       ss302_shipyard-SS_15_1.2-0 ; 
       ss302_shipyard-SS_15_3.1-0 ; 
       ss302_shipyard-SS_23.1-0 ; 
       ss302_shipyard-SS_23_2.2-0 ; 
       ss302_shipyard-SS_24.1-0 ; 
       ss302_shipyard-SS_24_1.2-0 ; 
       ss302_shipyard-SS_26.2-0 ; 
       ss302_shipyard-SS_26_3.1-0 ; 
       ss302_shipyard-SS_31.1-0 ; 
       ss302_shipyard-SS_32.1-0 ; 
       ss302_shipyard-SS_33.1-0 ; 
       ss302_shipyard-SS_33_1.1-0 ; 
       ss302_shipyard-SS_34.1-0 ; 
       ss302_shipyard-SS_34_1.1-0 ; 
       ss302_shipyard-SS_35.1-0 ; 
       ss302_shipyard-SS_35_1.1-0 ; 
       ss302_shipyard-SS_36.1-0 ; 
       ss302_shipyard-SS_36_1.1-0 ; 
       ss302_shipyard-SS_40.1-0 ; 
       ss302_shipyard-SS_41.1-0 ; 
       ss302_shipyard-SS_42.1-0 ; 
       ss302_shipyard-SS_42_1.1-0 ; 
       ss302_shipyard-SS_43.1-0 ; 
       ss302_shipyard-SS_44.1-0 ; 
       ss302_shipyard-SS_45.1-0 ; 
       ss302_shipyard-SS_46.1-0 ; 
       ss302_shipyard-SS_47.1-0 ; 
       ss302_shipyard-SS_48.1-0 ; 
       ss302_shipyard-SS_49.1-0 ; 
       ss302_shipyard-SS_50.1-0 ; 
       ss302_shipyard-SS_51.1-0 ; 
       ss302_shipyard-SS_52.1-0 ; 
       ss302_shipyard-SS_53.1-0 ; 
       ss302_shipyard-SS_53_1.1-0 ; 
       ss302_shipyard-SS_54.1-0 ; 
       ss302_shipyard-SS_55.1-0 ; 
       ss302_shipyard-SS_56.1-0 ; 
       ss302_shipyard-SS_57.1-0 ; 
       ss302_shipyard-SS_58.1-0 ; 
       ss302_shipyard-SS_59.1-0 ; 
       ss302_shipyard-SS_60.1-0 ; 
       ss302_shipyard-SS_61.1-0 ; 
       ss302_shipyard-SS_62.1-0 ; 
       ss302_shipyard-SS_63.1-0 ; 
       ss302_shipyard-tetra2.1-0 ; 
       ss302_shipyard-turwepemt2.2-0 ; 
       ss302_shipyard-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss302-shipyard.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ss302_shipyard-t2d61.2-0 ; 
       ss302_shipyard-t2d62.2-0 ; 
       ss305_elect_station-t2d52.2-0 ; 
       ss305_elect_station-t2d53.2-0 ; 
       ss305_elect_station-t2d54.2-0 ; 
       ss305_elect_station-t2d55.2-0 ; 
       ss305_elect_station-t2d56.2-0 ; 
       ss305_elect_station-t2d57.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       23 46 110 ; 
       86 39 110 ; 
       87 39 110 ; 
       88 39 110 ; 
       89 39 110 ; 
       90 39 110 ; 
       91 39 110 ; 
       92 39 110 ; 
       93 39 110 ; 
       94 39 110 ; 
       95 39 110 ; 
       0 9 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       38 44 110 ; 
       39 44 110 ; 
       40 44 110 ; 
       41 44 110 ; 
       42 44 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       60 42 110 ; 
       61 42 110 ; 
       62 40 110 ; 
       63 41 110 ; 
       64 40 110 ; 
       65 41 110 ; 
       66 40 110 ; 
       67 41 110 ; 
       68 40 110 ; 
       69 41 110 ; 
       70 39 110 ; 
       71 39 110 ; 
       72 40 110 ; 
       73 41 110 ; 
       74 42 110 ; 
       75 42 110 ; 
       76 42 110 ; 
       77 42 110 ; 
       78 38 110 ; 
       79 38 110 ; 
       80 38 110 ; 
       81 38 110 ; 
       82 38 110 ; 
       83 38 110 ; 
       84 40 110 ; 
       85 41 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       12 26 110 ; 
       13 26 110 ; 
       7 9 110 ; 
       14 15 110 ; 
       15 17 110 ; 
       16 28 110 ; 
       17 43 110 ; 
       18 45 110 ; 
       8 9 110 ; 
       21 47 110 ; 
       19 46 110 ; 
       26 22 110 ; 
       29 25 110 ; 
       30 18 110 ; 
       31 17 110 ; 
       32 30 110 ; 
       33 30 110 ; 
       34 30 110 ; 
       35 30 110 ; 
       36 30 110 ; 
       37 31 110 ; 
       45 44 110 ; 
       43 45 110 ; 
       47 44 110 ; 
       48 31 110 ; 
       49 30 110 ; 
       50 30 110 ; 
       51 31 110 ; 
       52 30 110 ; 
       53 31 110 ; 
       54 31 110 ; 
       55 30 110 ; 
       56 31 110 ; 
       57 30 110 ; 
       58 30 110 ; 
       59 31 110 ; 
       96 18 110 ; 
       97 30 110 ; 
       98 31 110 ; 
       46 44 110 ; 
       28 27 110 ; 
       27 46 110 ; 
       20 47 110 ; 
       22 47 110 ; 
       24 47 110 ; 
       25 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       86 11 300 ; 
       87 12 300 ; 
       88 13 300 ; 
       89 14 300 ; 
       90 15 300 ; 
       91 16 300 ; 
       92 17 300 ; 
       93 18 300 ; 
       94 19 300 ; 
       95 20 300 ; 
       60 41 300 ; 
       61 41 300 ; 
       62 40 300 ; 
       63 4 300 ; 
       64 40 300 ; 
       65 3 300 ; 
       66 40 300 ; 
       67 2 300 ; 
       68 40 300 ; 
       69 1 300 ; 
       70 8 300 ; 
       71 7 300 ; 
       72 21 300 ; 
       73 5 300 ; 
       74 22 300 ; 
       75 23 300 ; 
       76 24 300 ; 
       77 25 300 ; 
       78 26 300 ; 
       79 27 300 ; 
       80 28 300 ; 
       81 29 300 ; 
       82 30 300 ; 
       83 31 300 ; 
       84 0 300 ; 
       85 6 300 ; 
       30 35 300 ; 
       30 36 300 ; 
       30 37 300 ; 
       31 32 300 ; 
       31 33 300 ; 
       31 34 300 ; 
       47 10 300 ; 
       48 38 300 ; 
       49 39 300 ; 
       50 39 300 ; 
       51 38 300 ; 
       52 39 300 ; 
       53 38 300 ; 
       54 38 300 ; 
       55 39 300 ; 
       56 38 300 ; 
       57 39 300 ; 
       58 39 300 ; 
       59 38 300 ; 
       46 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 0 401 ; 
       10 1 401 ; 
       32 2 401 ; 
       33 3 401 ; 
       34 4 401 ; 
       35 5 401 ; 
       36 6 401 ; 
       37 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 91.43105 18.83884 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 93.93105 18.83884 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 96.43105 18.83884 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       23 SCHEM 14.4102 1.638643 0 MPRFLG 0 ; 
       11 SCHEM 134.4437 27.72317 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       86 SCHEM 87.43649 1.794556 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 89.93651 1.794556 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 92.43651 1.794556 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 94.93651 1.794556 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 97.43651 1.794556 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45.57842 23.20511 0 DISPLAY 0 0 SRT 10.94048 4.830691 4.888042 0 0 0 -28.32983 109.873 5.960464e-008 MPRFLG 0 ; 
       91 SCHEM 86.93246 -0.8816719 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 89.43248 -0.8816719 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 91.93248 -0.8816719 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 94.43248 -0.8816719 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       95 SCHEM 96.93248 -0.8816719 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 48.07844 23.20511 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 50.57844 23.20511 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 53.07844 23.20511 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 123.3347 0.49933 0 USR MPRFLG 0 ; 
       39 SCHEM 89.93651 3.794555 0 USR MPRFLG 0 ; 
       40 SCHEM 106.9352 4.946095 0 USR MPRFLG 0 ; 
       41 SCHEM 108.9819 1.219334 0 USR MPRFLG 0 ; 
       42 SCHEM 122.0343 4.874027 0 USR MPRFLG 0 ; 
       3 SCHEM 55.57844 23.20511 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 58.07845 23.20511 0 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 115.7843 2.874027 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 120.7843 2.874027 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 108.1852 2.946102 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 108.0454 -1.717712 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 105.6852 2.946102 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 105.5454 -1.717712 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 103.1852 2.946102 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       67 SCHEM 103.0454 -1.717712 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       68 SCHEM 100.6852 2.946102 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       69 SCHEM 100.5454 -1.717712 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       70 SCHEM 82.43649 1.794556 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 84.93649 1.794556 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 110.6852 2.946102 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 111.4885 -1.713207 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 118.2843 2.874027 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       75 SCHEM 123.2843 2.874027 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       76 SCHEM 125.7843 2.874027 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       77 SCHEM 128.2843 2.874027 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       78 SCHEM 117.0847 -1.500671 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       79 SCHEM 119.5847 -1.500671 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       80 SCHEM 122.0847 -1.500671 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       81 SCHEM 124.5847 -1.500671 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       82 SCHEM 127.0847 -1.500671 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       83 SCHEM 129.5848 -1.500671 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       84 SCHEM 113.1852 2.946102 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       85 SCHEM 113.9885 -1.713207 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       5 SCHEM 60.57845 23.20511 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 63.07845 23.20511 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 79.4102 -2.361356 0 MPRFLG 0 ; 
       9 SCHEM 56.82844 25.20511 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 76.9102 -2.361356 0 MPRFLG 0 ; 
       7 SCHEM 65.57845 23.20511 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 49.41019 -4.361356 0 MPRFLG 0 ; 
       15 SCHEM 49.41019 -2.361356 0 MPRFLG 0 ; 
       16 SCHEM 9.4102 -2.361356 0 MPRFLG 0 ; 
       17 SCHEM 59.41019 -0.3613563 0 MPRFLG 0 ; 
       18 SCHEM 31.9102 1.638643 0 MPRFLG 0 ; 
       8 SCHEM 68.07845 23.20511 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 74.4102 1.638643 0 MPRFLG 0 ; 
       19 SCHEM 11.9102 1.638643 0 MPRFLG 0 ; 
       26 SCHEM 78.1602 -0.3613563 0 MPRFLG 0 ; 
       29 SCHEM 11.9102 -2.361356 0 MPRFLG 0 ; 
       30 SCHEM 33.16019 -0.3613563 0 MPRFLG 0 ; 
       31 SCHEM 60.66019 -2.361356 0 MPRFLG 0 ; 
       32 SCHEM 34.41019 -2.361356 0 WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 39.41019 -2.361356 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 36.91019 -2.361356 0 WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 44.41019 -2.361356 0 WIRECOL 9 7 MPRFLG 0 ; 
       36 SCHEM 41.91019 -2.361356 0 WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM 69.4102 -4.361356 0 WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM 43.16019 3.638643 0 MPRFLG 0 ; 
       43 SCHEM 59.41019 1.638643 0 MPRFLG 0 ; 
       47 SCHEM 76.9102 3.638643 0 MPRFLG 0 ; 
       48 SCHEM 56.91019 -4.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 24.4102 -2.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 19.4102 -2.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 51.91019 -4.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 21.9102 -2.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 54.41019 -4.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 64.4102 -4.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 31.9102 -2.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 59.41019 -4.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 26.9102 -2.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 29.4102 -2.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 61.9102 -4.361356 0 WIRECOL 3 7 MPRFLG 0 ; 
       96 SCHEM 16.9102 -0.3613563 0 MPRFLG 0 ; 
       97 SCHEM 46.91019 -2.361356 0 WIRECOL 1 7 MPRFLG 0 ; 
       98 SCHEM 66.9102 -4.361356 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 11.9102 3.638643 0 MPRFLG 0 ; 
       28 SCHEM 9.4102 -0.3613563 0 MPRFLG 0 ; 
       27 SCHEM 9.4102 1.638643 0 MPRFLG 0 ; 
       20 SCHEM 71.9102 1.638643 0 MPRFLG 0 ; 
       22 SCHEM 78.1602 1.638643 0 MPRFLG 0 ; 
       44 SCHEM 45.19393 12.16644 0 USR SRT 1 1 1 4.371141e-008 3.178651e-008 -1.270549e-021 2.399017e-008 8.729084e-008 -2.119473e-009 MPRFLG 0 ; 
       24 SCHEM 81.9102 1.638643 0 MPRFLG 0 ; 
       25 SCHEM 11.9102 -0.3613563 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 128.6867 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 149.7275 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 174.9052 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 65.03374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 130.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 135.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 140.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 35.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 103.509 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 130.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 135.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 140.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 145.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 161.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 166.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 171.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 176.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 153.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 160.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 114.2096 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 115.2544 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 110.5754 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 102.5992 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 50.73668 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 62.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 105.0337 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 145.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 195.9586 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 192.2086 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 199.7086 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 207.2086 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 147.0674 20.18679 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 152.0674 20.18679 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 35.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 114.2096 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 115.2544 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 110.5754 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
