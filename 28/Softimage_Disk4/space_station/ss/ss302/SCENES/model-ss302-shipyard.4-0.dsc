SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.12-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss302_shipyard-mat61.1-0 ; 
       ss302_shipyard-mat62.1-0 ; 
       ss302_shipyard-white_strobe1_53.1-0 ; 
       ss302_shipyard-white_strobe1_54.1-0 ; 
       ss302_shipyard-white_strobe1_55.1-0 ; 
       ss302_shipyard-white_strobe1_56.1-0 ; 
       ss302_shipyard-white_strobe1_57.1-0 ; 
       ss302_shipyard-white_strobe1_58.1-0 ; 
       ss302_shipyard-white_strobe1_59.1-0 ; 
       ss302_shipyard-white_strobe1_60.1-0 ; 
       ss302_shipyard-white_strobe1_61.1-0 ; 
       ss302_shipyard-white_strobe1_62.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 87     
       root-cube2.1-0 ; 
       root-cube3.1-0 ; 
       root-cube35.1-0 ; 
       root-cube36.1-0 ; 
       root-cube4.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube46.1-0 ; 
       root-cube46_1.1-0 ; 
       root-cube46_2.1-0 ; 
       root-cube46_3.1-0 ; 
       root-cube46_4.1-0 ; 
       root-cube48.1-0 ; 
       root-cube49.1-0 ; 
       root-cube5.1-0 ; 
       root-cube5_1.1-0 ; 
       root-cube5_2.1-0 ; 
       root-cube8.2-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.2-0 ; 
       root-garage1A.2-0 ; 
       root-garage1B.2-0 ; 
       root-garage1C.2-0 ; 
       root-garage1D.2-0 ; 
       root-garage1E.2-0 ; 
       root-launch1.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-null22.2-0 ; 
       root-null24.1-0 ; 
       root-root.4-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere8.1-0 ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.2-0 ; 
       root-SS_13_2.2-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.2-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.2-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.2-0 ; 
       root-SS_26.2-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
       root-SS_62.1-0 ; 
       root-SS_63.1-0 ; 
       root-tetra2.1-0 ; 
       root-turwepemt2.2-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss302-shipyard.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ss302_shipyard-t2d61.2-0 ; 
       ss302_shipyard-t2d62.2-0 ; 
       ss305_elect_station-t2d52.2-0 ; 
       ss305_elect_station-t2d53.2-0 ; 
       ss305_elect_station-t2d54.2-0 ; 
       ss305_elect_station-t2d55.2-0 ; 
       ss305_elect_station-t2d56.2-0 ; 
       ss305_elect_station-t2d57.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 34 110 ; 
       74 27 110 ; 
       75 27 110 ; 
       76 27 110 ; 
       77 27 110 ; 
       78 27 110 ; 
       79 27 110 ; 
       80 27 110 ; 
       81 27 110 ; 
       82 27 110 ; 
       83 27 110 ; 
       26 33 110 ; 
       27 33 110 ; 
       28 33 110 ; 
       29 33 110 ; 
       30 33 110 ; 
       48 30 110 ; 
       49 30 110 ; 
       50 28 110 ; 
       51 29 110 ; 
       52 28 110 ; 
       53 29 110 ; 
       54 28 110 ; 
       55 29 110 ; 
       56 28 110 ; 
       57 29 110 ; 
       58 27 110 ; 
       59 27 110 ; 
       60 28 110 ; 
       61 29 110 ; 
       62 30 110 ; 
       63 30 110 ; 
       64 30 110 ; 
       65 30 110 ; 
       66 26 110 ; 
       67 26 110 ; 
       68 26 110 ; 
       69 26 110 ; 
       70 26 110 ; 
       71 26 110 ; 
       72 28 110 ; 
       73 29 110 ; 
       0 14 110 ; 
       1 14 110 ; 
       2 3 110 ; 
       3 5 110 ; 
       4 16 110 ; 
       5 31 110 ; 
       6 32 110 ; 
       9 35 110 ; 
       7 34 110 ; 
       14 10 110 ; 
       17 13 110 ; 
       18 6 110 ; 
       19 5 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 19 110 ; 
       32 33 110 ; 
       31 32 110 ; 
       35 33 110 ; 
       36 19 110 ; 
       37 18 110 ; 
       38 18 110 ; 
       39 19 110 ; 
       40 18 110 ; 
       41 19 110 ; 
       42 19 110 ; 
       43 18 110 ; 
       44 19 110 ; 
       45 18 110 ; 
       46 18 110 ; 
       47 19 110 ; 
       84 6 110 ; 
       85 18 110 ; 
       86 19 110 ; 
       34 33 110 ; 
       16 15 110 ; 
       15 34 110 ; 
       8 35 110 ; 
       10 35 110 ; 
       12 35 110 ; 
       13 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       74 11 300 ; 
       75 12 300 ; 
       76 13 300 ; 
       77 14 300 ; 
       78 15 300 ; 
       79 16 300 ; 
       80 17 300 ; 
       81 18 300 ; 
       82 19 300 ; 
       83 20 300 ; 
       48 41 300 ; 
       49 41 300 ; 
       50 40 300 ; 
       51 4 300 ; 
       52 40 300 ; 
       53 3 300 ; 
       54 40 300 ; 
       55 2 300 ; 
       56 40 300 ; 
       57 1 300 ; 
       58 8 300 ; 
       59 7 300 ; 
       60 21 300 ; 
       61 5 300 ; 
       62 22 300 ; 
       63 23 300 ; 
       64 24 300 ; 
       65 25 300 ; 
       66 26 300 ; 
       67 27 300 ; 
       68 28 300 ; 
       69 29 300 ; 
       70 30 300 ; 
       71 31 300 ; 
       72 0 300 ; 
       73 6 300 ; 
       18 35 300 ; 
       18 36 300 ; 
       18 37 300 ; 
       19 32 300 ; 
       19 33 300 ; 
       19 34 300 ; 
       35 10 300 ; 
       36 38 300 ; 
       37 39 300 ; 
       38 39 300 ; 
       39 38 300 ; 
       40 39 300 ; 
       41 38 300 ; 
       42 38 300 ; 
       43 39 300 ; 
       44 38 300 ; 
       45 39 300 ; 
       46 39 300 ; 
       47 38 300 ; 
       34 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 0 401 ; 
       10 1 401 ; 
       32 2 401 ; 
       33 3 401 ; 
       34 4 401 ; 
       35 5 401 ; 
       36 6 401 ; 
       37 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 14.4102 1.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       74 SCHEM 87.43649 1.794556 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       75 SCHEM 89.93651 1.794556 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       76 SCHEM 92.43651 1.794556 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       77 SCHEM 94.93651 1.794556 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       78 SCHEM 97.43651 1.794556 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       79 SCHEM 86.93246 -0.8816719 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       80 SCHEM 89.43248 -0.8816719 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       81 SCHEM 91.93248 -0.8816719 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       82 SCHEM 94.43248 -0.8816719 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       83 SCHEM 96.93248 -0.8816719 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 123.3347 0.49933 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 89.93651 3.794555 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 106.9352 4.946095 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 108.9819 1.219334 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 122.0343 4.874027 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 115.7843 2.874027 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 120.7843 2.874027 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 108.1852 2.946102 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 108.0454 -1.717712 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 105.6852 2.946102 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 105.5454 -1.717712 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 103.1852 2.946102 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 103.0454 -1.717712 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 100.6852 2.946102 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 100.5454 -1.717712 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 82.43649 1.794556 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 84.93649 1.794556 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 110.6852 2.946102 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       61 SCHEM 111.4885 -1.713207 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       62 SCHEM 118.2843 2.874027 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       63 SCHEM 123.2843 2.874027 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       64 SCHEM 125.7843 2.874027 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       65 SCHEM 128.2843 2.874027 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       66 SCHEM 117.0847 -1.500671 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       67 SCHEM 119.5847 -1.500671 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       68 SCHEM 122.0847 -1.500671 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       69 SCHEM 124.5847 -1.500671 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       70 SCHEM 127.0847 -1.500671 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       71 SCHEM 129.5848 -1.500671 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       72 SCHEM 113.1852 2.946102 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       73 SCHEM 113.9885 -1.713207 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 79.4102 -2.361356 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 76.9102 -2.361356 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 49.41019 -4.361356 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 49.41019 -2.361356 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 9.4102 -2.361356 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 59.41019 -0.3613563 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 31.9102 1.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 74.4102 1.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 11.9102 1.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 78.1602 -0.3613563 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 11.9102 -2.361356 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 33.16019 -0.3613563 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 60.66019 -2.361356 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 34.41019 -2.361356 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 39.41019 -2.361356 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 36.91019 -2.361356 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 44.41019 -2.361356 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 41.91019 -2.361356 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 69.4102 -4.361356 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 43.16019 3.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 59.41019 1.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 76.9102 3.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 56.91019 -4.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 24.4102 -2.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 19.4102 -2.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 51.91019 -4.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 21.9102 -2.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 54.41019 -4.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 64.4102 -4.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 31.9102 -2.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 59.41019 -4.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 26.9102 -2.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 29.4102 -2.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 61.9102 -4.361356 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       84 SCHEM 16.9102 -0.3613563 0 DISPLAY 2 2 MPRFLG 0 ; 
       85 SCHEM 46.91019 -2.361356 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       86 SCHEM 66.9102 -4.361356 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 11.9102 3.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 9.4102 -0.3613563 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 9.4102 1.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 71.9102 1.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 78.1602 1.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 45.19393 12.16644 0 USR DISPLAY 3 2 SRT 1 1 1 4.371141e-008 3.178651e-008 -1.270549e-021 2.399017e-008 8.729084e-008 -2.119473e-009 MPRFLG 0 ; 
       12 SCHEM 81.9102 1.638643 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 11.9102 -0.3613563 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 128.6867 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 149.7275 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 174.9052 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 65.03374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 130.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 135.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 140.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 35.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 103.509 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 130.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 135.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 140.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 145.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 161.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 166.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 171.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 176.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 153.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 160.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 114.2096 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 115.2544 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 110.5754 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 102.5992 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 50.73668 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 62.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 105.0337 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 145.1749 22.5316 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 195.9586 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 192.2086 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 199.7086 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 207.2086 66.71839 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 147.0674 20.18679 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 152.0674 20.18679 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 35.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 114.2096 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 115.2544 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 110.5754 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
