SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.1-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       ss306_ripcord-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       ss302_shipyard-cube1.1-0 ROOT ; 
       ss302_shipyard-cube2.1-0 ROOT ; 
       ss302_shipyard-cube3.1-0 ; 
       ss302_shipyard-cube4.1-0 ; 
       ss302_shipyard-cube5.1-0 ; 
       ss302_shipyard-cube6.1-0 ROOT ; 
       ss302_shipyard-cube7.1-0 ROOT ; 
       ss302_shipyard-cube8.1-0 ROOT ; 
       ss302_shipyard-sphere7.1-0 ROOT ; 
       ss306_ripcord-sphere6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/bgrnd03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss302-shipyard.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       ss306_ripcord-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 0.328 0.328 0.328 -0.9020001 0 0 -21.08782 -1.011552 10.06848 MPRFLG 0 ; 
       1 SCHEM 5 0 0 SRT 0.202704 1.018836 0.202704 0 0 0 -10.63081 -6.818869 -8.300102 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1.081987 0.6737154 0.6737154 0 0 0 58.68601 4.072176 -85.25522 MPRFLG 0 ; 
       6 SCHEM 15 0 0 SRT 0.328 0.07965935 0.328 0 4.712389 4.712389 9.674454 -1.664711 -0.03107476 MPRFLG 0 ; 
       9 SCHEM 7.826674 -4.357355 0 USR SRT 1.8432 2.104935 1.8432 -0.03243348 -3.255823 -1.540905 8.564915 6.051208 0.8049829 MPRFLG 0 ; 
       7 SCHEM 20 0 0 SRT 0.328 0.328 0.328 -0.9020001 0 0 -2.859968 0.5796288 11.39674 MPRFLG 0 ; 
       8 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 2.969366 4.7586 3.172399 0 0 1.570796 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
