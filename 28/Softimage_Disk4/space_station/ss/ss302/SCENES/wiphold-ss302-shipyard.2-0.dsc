SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.4-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss302_shipyard-mat58.1-0 ; 
       ss302_shipyard-mat59.1-0 ; 
       ss302_shipyard-mat60.1-0 ; 
       ss302_shipyard-mat61.1-0 ; 
       ss302_shipyard-mat62.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss306_ripcord-mat5_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 99     
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube23.1-0 ; 
       root-cube24.1-0 ; 
       root-cube26.1-0 ; 
       root-cube28.1-0 ; 
       root-cube3.2-0 ROOT ; 
       root-cube31.1-0 ; 
       root-cube32.1-0 ; 
       root-cube38.1-0 ; 
       root-cube39.1-0 ; 
       root-cube4.1-0 ; 
       root-cube41.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl2.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-root.2-0 ROOT ; 
       root-sphere6_2.2-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-tetra1.1-0 ; 
       ss302_shipyard-cube1.1-0 ; 
       ss302_shipyard-cube2.1-0 ; 
       ss302_shipyard-cube29.1-0 ; 
       ss302_shipyard-cube3.1-0 ; 
       ss302_shipyard-cube30.1-0 ; 
       ss302_shipyard-cube35.1-0 ; 
       ss302_shipyard-cube36.1-0 ; 
       ss302_shipyard-cube4.1-0 ; 
       ss302_shipyard-cube42.1-0 ; 
       ss302_shipyard-cube43.1-0 ; 
       ss302_shipyard-cube44.1-0 ; 
       ss302_shipyard-cube45.1-0 ROOT ; 
       ss302_shipyard-cube46.1-0 ROOT ; 
       ss302_shipyard-cube5.1-0 ; 
       ss302_shipyard-cube6.3-0 ROOT ; 
       ss302_shipyard-cube8.1-0 ; 
       ss302_shipyard-cube8_1.1-0 ; 
       ss302_shipyard-cube9.1-0 ; 
       ss302_shipyard-east_bay_11_10.2-0 ROOT ; 
       ss302_shipyard-east_bay_11_8.1-0 ; 
       ss302_shipyard-east_bay_11_9.2-0 ; 
       ss302_shipyard-garage1A.1-0 ; 
       ss302_shipyard-garage1B.1-0 ; 
       ss302_shipyard-garage1C.1-0 ; 
       ss302_shipyard-garage1D.1-0 ; 
       ss302_shipyard-garage1E.1-0 ; 
       ss302_shipyard-launch1.1-0 ; 
       ss302_shipyard-null21.3-0 ROOT ; 
       ss302_shipyard-null22.2-0 ROOT ; 
       ss302_shipyard-sphere7.3-0 ROOT ; 
       ss302_shipyard-sphere8.1-0 ROOT ; 
       ss302_shipyard-SS_11.1-0 ; 
       ss302_shipyard-SS_11_1.1-0 ; 
       ss302_shipyard-SS_13_2.1-0 ; 
       ss302_shipyard-SS_13_3.1-0 ; 
       ss302_shipyard-SS_15_1.1-0 ; 
       ss302_shipyard-SS_15_3.1-0 ; 
       ss302_shipyard-SS_23.1-0 ; 
       ss302_shipyard-SS_23_2.1-0 ; 
       ss302_shipyard-SS_24.1-0 ; 
       ss302_shipyard-SS_24_1.1-0 ; 
       ss302_shipyard-SS_26.1-0 ; 
       ss302_shipyard-SS_26_3.1-0 ; 
       ss302_shipyard-tetra2.1-0 ; 
       ss302_shipyard-turwepemt2.1-0 ; 
       ss302_shipyard-turwepemt2_3.1-0 ; 
       ss306_ripcord-cube7.2-0 ROOT ; 
       ss306_ripcord-sphere6.3-0 ROOT ; 
       ss306_ripcord1-sphere6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wiphold-ss302-shipyard.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       ss302_shipyard-t2d58.1-0 ; 
       ss302_shipyard-t2d59.1-0 ; 
       ss302_shipyard-t2d60.1-0 ; 
       ss302_shipyard-t2d61.1-0 ; 
       ss302_shipyard-t2d62.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss306_ripcord-t2d7.2-0 ; 
       ss306_ripcord-t2d7_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 14 110 ; 
       2 0 110 ; 
       3 22 110 ; 
       4 22 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 3 110 ; 
       12 22 110 ; 
       13 3 110 ; 
       14 12 110 ; 
       15 4 110 ; 
       16 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       22 21 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 18 110 ; 
       26 19 110 ; 
       27 18 110 ; 
       28 19 110 ; 
       29 18 110 ; 
       30 19 110 ; 
       31 18 110 ; 
       32 19 110 ; 
       33 17 110 ; 
       34 17 110 ; 
       35 18 110 ; 
       36 19 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 16 110 ; 
       42 16 110 ; 
       43 16 110 ; 
       44 16 110 ; 
       45 16 110 ; 
       46 16 110 ; 
       47 18 110 ; 
       48 19 110 ; 
       49 6 110 ; 
       50 77 110 ; 
       51 77 110 ; 
       52 55 110 ; 
       53 51 110 ; 
       54 52 110 ; 
       55 56 110 ; 
       56 58 110 ; 
       57 51 110 ; 
       58 78 110 ; 
       59 78 110 ; 
       60 58 110 ; 
       63 51 110 ; 
       65 77 110 ; 
       66 67 110 ; 
       67 55 110 ; 
       69 59 110 ; 
       70 58 110 ; 
       71 69 110 ; 
       72 69 110 ; 
       73 69 110 ; 
       74 69 110 ; 
       75 69 110 ; 
       76 70 110 ; 
       81 70 110 ; 
       82 69 110 ; 
       83 69 110 ; 
       84 70 110 ; 
       85 69 110 ; 
       86 70 110 ; 
       87 70 110 ; 
       88 69 110 ; 
       89 70 110 ; 
       90 69 110 ; 
       91 69 110 ; 
       92 70 110 ; 
       93 59 110 ; 
       94 69 110 ; 
       95 70 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       80 13 300 ; 
       22 36 300 ; 
       23 34 300 ; 
       24 34 300 ; 
       25 33 300 ; 
       26 4 300 ; 
       27 33 300 ; 
       28 3 300 ; 
       29 33 300 ; 
       30 2 300 ; 
       31 33 300 ; 
       32 1 300 ; 
       33 8 300 ; 
       34 7 300 ; 
       35 14 300 ; 
       36 5 300 ; 
       37 15 300 ; 
       38 16 300 ; 
       39 17 300 ; 
       40 18 300 ; 
       41 19 300 ; 
       42 20 300 ; 
       43 21 300 ; 
       44 22 300 ; 
       45 23 300 ; 
       46 24 300 ; 
       47 0 300 ; 
       48 6 300 ; 
       68 9 300 ; 
       68 10 300 ; 
       68 11 300 ; 
       69 28 300 ; 
       69 29 300 ; 
       69 30 300 ; 
       70 25 300 ; 
       70 26 300 ; 
       70 27 300 ; 
       81 31 300 ; 
       82 32 300 ; 
       83 32 300 ; 
       84 31 300 ; 
       85 32 300 ; 
       86 31 300 ; 
       87 31 300 ; 
       88 32 300 ; 
       89 31 300 ; 
       90 32 300 ; 
       91 32 300 ; 
       92 31 300 ; 
       97 35 300 ; 
       98 12 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 0 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       25 5 401 ; 
       26 6 401 ; 
       27 7 401 ; 
       28 8 401 ; 
       29 9 401 ; 
       30 10 401 ; 
       35 11 401 ; 
       36 12 401 ; 
       12 3 401 ; 
       13 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       80 SCHEM 101.4902 34.27773 0 USR SRT 4.129808 4.155969 4.088917 0.2889749 1.688856 -1.315965 -13.00005 16.18784 -39.43577 MPRFLG 0 ; 
       62 SCHEM 98.99017 34.27773 0 SRT 0.4039999 0.116352 6.565 0 0 0 -0.1200001 42.97598 -6.081508 MPRFLG 0 ; 
       0 SCHEM 20.17855 -31.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 22.67856 -31.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 20.17855 -33.14228 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 2.678554 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 12.67856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10.17856 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 0.1785543 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 36.27872 -11.56053 0 DISPLAY 0 0 SRT 1 1 1 2.624681e-006 4.681583 3.14159 -4.307003 -5.434197 -59.14524 MPRFLG 0 ; 
       8 SCHEM 12.67856 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 15.17856 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 17.67855 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 2.678554 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 21.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 5.178555 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 21.42856 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 7.678556 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 51.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 41.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 81.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 31.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 66.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 43.92856 -23.14227 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 43.92856 -25.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 60.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 65.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 82.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 32.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 80.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 30.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 77.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 27.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 75.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 25.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 40.17856 -29.14227 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 42.67856 -29.14227 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 85.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 35.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 62.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 67.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 70.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 72.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 45.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 47.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 50.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 52.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 55.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 57.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 87.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 37.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 0.1785543 -31.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 38.63158 32.27773 0 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 33.63158 32.27773 0 MPRFLG 0 ; 
       52 SCHEM 58.17423 16.54758 0 MPRFLG 0 ; 
       53 SCHEM 31.13158 30.27773 0 MPRFLG 0 ; 
       54 SCHEM 58.17423 14.54758 0 MPRFLG 0 ; 
       55 SCHEM 56.92423 18.54757 0 MPRFLG 0 ; 
       56 SCHEM 56.92423 20.54757 0 MPRFLG 0 ; 
       57 SCHEM 33.63158 30.27773 0 MPRFLG 0 ; 
       58 SCHEM 68.17423 22.54757 0 MPRFLG 0 ; 
       59 SCHEM 38.17424 22.54757 0 DISPLAY 3 2 MPRFLG 0 ; 
       60 SCHEM 80.67426 20.54757 0 MPRFLG 0 ; 
       61 SCHEM 43.63158 32.27773 0 DISPLAY 0 0 SRT 0.7992944 0.06962025 0.3907826 -6.557179 -7.969113 3.376084 -26.99958 43.67587 -11.42042 MPRFLG 0 ; 
       63 SCHEM 36.13158 30.27773 0 MPRFLG 0 ; 
       64 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1.081987 0.6737154 0.6737154 0 0 0 58.68601 4.072176 -85.25522 MPRFLG 0 ; 
       65 SCHEM 41.13158 32.27773 0 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 55.67423 14.54758 0 MPRFLG 0 ; 
       67 SCHEM 55.67423 16.54758 0 MPRFLG 0 ; 
       68 SCHEM 93.99017 31.95513 0 USR SRT 1 1 1 -1.604524e-007 1.856808 2.70896e-014 13.54433 -5.192739 1.006159 MPRFLG 0 ; 
       69 SCHEM 39.42424 20.54757 0 DISPLAY 2 2 MPRFLG 0 ; 
       70 SCHEM 69.42423 20.54757 0 MPRFLG 0 ; 
       71 SCHEM 40.67424 18.54757 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       72 SCHEM 45.67424 18.54757 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       73 SCHEM 43.17424 18.54757 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       74 SCHEM 50.67423 18.54757 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       75 SCHEM 48.17423 18.54757 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       76 SCHEM 78.17426 18.54757 0 WIRECOL 9 7 MPRFLG 0 ; 
       77 SCHEM 37.38158 34.27773 0 USR SRT 1 1 1 0 0 -3.141593 -7.514229 16.86377 32.53855 MPRFLG 0 ; 
       78 SCHEM 51.92423 24.54757 0 USR SRT 1 1 1 0 0 0 -3.079112 11.35215 45.47983 MPRFLG 0 ; 
       79 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 2.969366 4.7586 3.172399 0 0 1.570796 0 0 0 MPRFLG 0 ; 
       81 SCHEM 65.67424 18.54757 0 WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 30.67424 18.54757 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       83 SCHEM 25.67424 18.54757 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       84 SCHEM 60.67424 18.54757 0 WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 28.17424 18.54757 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       86 SCHEM 63.17424 18.54757 0 WIRECOL 3 7 MPRFLG 0 ; 
       87 SCHEM 73.17424 18.54757 0 WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 38.17424 18.54757 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       89 SCHEM 68.17423 18.54757 0 WIRECOL 3 7 MPRFLG 0 ; 
       90 SCHEM 33.17424 18.54757 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       91 SCHEM 35.67424 18.54757 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       92 SCHEM 70.67423 18.54757 0 WIRECOL 3 7 MPRFLG 0 ; 
       93 SCHEM 23.17423 20.54757 0 DISPLAY 2 2 MPRFLG 0 ; 
       94 SCHEM 53.17423 18.54757 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       95 SCHEM 75.67425 18.54757 0 WIRECOL 1 7 MPRFLG 0 ; 
       96 SCHEM 86.11937 1.875719 0 DISPLAY 0 0 SRT 0.9141061 0.2220033 1.212105 -1.606775 3.110701 -4.057471 -95.36144 42.08097 -4.12871 MPRFLG 0 ; 
       97 SCHEM 82.36937 3.875719 0 USR DISPLAY 0 0 SRT 5.188201 6.851788 5.136831 -0.2889743 -1.688856 -1.315965 35.70973 6.051208 0.5335348 MPRFLG 0 ; 
       98 SCHEM 96.49017 34.27773 0 USR SRT 5.188201 6.851788 5.136831 -0.2889743 -1.688856 -1.315965 36.9549 16.18784 42.20316 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 128.6867 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 149.7275 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 174.9052 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 65.03374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 169.0182 8.362499 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 170.063 8.635059 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 165.3841 9.679878 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 103.509 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 130.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 135.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 140.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 145.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 161.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 166.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 171.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 176.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 153.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 160.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 114.2096 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 115.2544 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 110.5754 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 102.5992 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 50.73668 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 62.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 105.0337 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 105.509 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 35.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 169.0182 6.362499 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 170.063 6.635059 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 165.3841 7.679878 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 114.2096 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 115.2544 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 110.5754 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 105.509 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 30.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 35.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
