SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.7-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       ss302_shipyard-light1.3-0 ROOT ; 
       ss302_shipyard-light2.3-0 ROOT ; 
       ss302_shipyard-light3.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss302_shipyard-mat58.1-0 ; 
       ss302_shipyard-mat59.1-0 ; 
       ss302_shipyard-mat60.1-0 ; 
       ss302_shipyard-mat61.1-0 ; 
       ss302_shipyard-mat62.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss306_ripcord-mat5_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 110     
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube23.1-0 ; 
       root-cube24.1-0 ; 
       root-cube26.1-0 ; 
       root-cube28.1-0 ; 
       root-cube3.3-0 ROOT ; 
       root-cube31.1-0 ; 
       root-cube32.1-0 ; 
       root-cube38.1-0 ; 
       root-cube39.1-0 ; 
       root-cube4.1-0 ; 
       root-cube41.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl2.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-root.3-0 ROOT ; 
       root-sphere6_2.2-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-tetra1.1-0 ; 
       ss302_shipyard-cube1.2-0 ROOT ; 
       ss302_shipyard-cube2.1-0 ; 
       ss302_shipyard-cube29.1-0 ROOT ; 
       ss302_shipyard-cube3.1-0 ; 
       ss302_shipyard-cube30.1-0 ROOT ; 
       ss302_shipyard-cube35.1-0 ; 
       ss302_shipyard-cube36.1-0 ; 
       ss302_shipyard-cube4.1-0 ; 
       ss302_shipyard-cube42.1-0 ; 
       ss302_shipyard-cube43.1-0 ; 
       ss302_shipyard-cube44.1-0 ROOT ; 
       ss302_shipyard-cube46.1-0 ; 
       ss302_shipyard-cube46_1.1-0 ; 
       ss302_shipyard-cube46_2.1-0 ; 
       ss302_shipyard-cube46_3.1-0 ; 
       ss302_shipyard-cube47.1-0 ROOT ; 
       ss302_shipyard-cube48.1-0 ; 
       ss302_shipyard-cube49.1-0 ; 
       ss302_shipyard-cube5.1-0 ; 
       ss302_shipyard-cube5_1.1-0 ; 
       ss302_shipyard-cube5_2.1-0 ; 
       ss302_shipyard-cube50.1-0 ROOT ; 
       ss302_shipyard-cube51.1-0 ROOT ; 
       ss302_shipyard-cube52.1-0 ROOT ; 
       ss302_shipyard-cube6.4-0 ROOT ; 
       ss302_shipyard-cube8.2-0 ROOT ; 
       ss302_shipyard-cube8_1.1-0 ROOT ; 
       ss302_shipyard-cube9.1-0 ROOT ; 
       ss302_shipyard-east_bay_11_10.3-0 ROOT ; 
       ss302_shipyard-east_bay_11_8.1-0 ; 
       ss302_shipyard-east_bay_11_9.2-0 ; 
       ss302_shipyard-garage1A.2-0 ROOT ; 
       ss302_shipyard-garage1B.2-0 ROOT ; 
       ss302_shipyard-garage1C.2-0 ROOT ; 
       ss302_shipyard-garage1D.2-0 ROOT ; 
       ss302_shipyard-garage1E.2-0 ROOT ; 
       ss302_shipyard-launch1.1-0 ROOT ; 
       ss302_shipyard-null22.2-0 ; 
       ss302_shipyard-null23.3-0 ROOT ; 
       ss302_shipyard-null24.1-0 ; 
       ss302_shipyard-sphere6.1-0 ; 
       ss302_shipyard-sphere7.4-0 ROOT ; 
       ss302_shipyard-sphere8.1-0 ; 
       ss302_shipyard-SS_11.1-0 ROOT ; 
       ss302_shipyard-SS_11_1.2-0 ROOT ; 
       ss302_shipyard-SS_13_2.2-0 ROOT ; 
       ss302_shipyard-SS_13_3.1-0 ROOT ; 
       ss302_shipyard-SS_15_1.2-0 ROOT ; 
       ss302_shipyard-SS_15_3.1-0 ROOT ; 
       ss302_shipyard-SS_23.1-0 ROOT ; 
       ss302_shipyard-SS_23_2.2-0 ROOT ; 
       ss302_shipyard-SS_24.1-0 ROOT ; 
       ss302_shipyard-SS_24_1.2-0 ROOT ; 
       ss302_shipyard-SS_26.2-0 ROOT ; 
       ss302_shipyard-SS_26_3.1-0 ROOT ; 
       ss302_shipyard-tetra2.1-0 ; 
       ss302_shipyard-turwepemt2.2-0 ROOT ; 
       ss302_shipyard-turwepemt2_3.1-0 ROOT ; 
       ss306_ripcord-cube7.3-0 ROOT ; 
       ss306_ripcord-sphere6.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss302/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wiphold-ss302-shipyard.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       ss302_shipyard-t2d58.1-0 ; 
       ss302_shipyard-t2d59.1-0 ; 
       ss302_shipyard-t2d60.1-0 ; 
       ss302_shipyard-t2d61.1-0 ; 
       ss302_shipyard-t2d62.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss306_ripcord-t2d7.2-0 ; 
       ss306_ripcord-t2d7_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 14 110 ; 
       2 0 110 ; 
       3 22 110 ; 
       4 22 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 3 110 ; 
       12 22 110 ; 
       13 3 110 ; 
       14 12 110 ; 
       15 4 110 ; 
       16 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       22 21 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 18 110 ; 
       26 19 110 ; 
       27 18 110 ; 
       28 19 110 ; 
       29 18 110 ; 
       30 19 110 ; 
       31 18 110 ; 
       32 19 110 ; 
       33 17 110 ; 
       34 17 110 ; 
       35 18 110 ; 
       36 19 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 16 110 ; 
       42 16 110 ; 
       43 16 110 ; 
       44 16 110 ; 
       45 16 110 ; 
       46 16 110 ; 
       47 18 110 ; 
       48 19 110 ; 
       49 6 110 ; 
       51 68 110 ; 
       53 68 110 ; 
       55 56 110 ; 
       56 58 110 ; 
       57 70 110 ; 
       58 87 110 ; 
       59 89 110 ; 
       63 92 110 ; 
       61 90 110 ; 
       68 64 110 ; 
       79 59 110 ; 
       80 58 110 ; 
       89 88 110 ; 
       87 89 110 ; 
       92 88 110 ; 
       105 59 110 ; 
       90 88 110 ; 
       70 69 110 ; 
       69 90 110 ; 
       62 92 110 ; 
       64 92 110 ; 
       66 92 110 ; 
       67 61 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       22 36 300 ; 
       23 34 300 ; 
       24 34 300 ; 
       25 33 300 ; 
       26 4 300 ; 
       27 33 300 ; 
       28 3 300 ; 
       29 33 300 ; 
       30 2 300 ; 
       31 33 300 ; 
       32 1 300 ; 
       33 8 300 ; 
       34 7 300 ; 
       35 14 300 ; 
       36 5 300 ; 
       37 15 300 ; 
       38 16 300 ; 
       39 17 300 ; 
       40 18 300 ; 
       41 19 300 ; 
       42 20 300 ; 
       43 21 300 ; 
       44 22 300 ; 
       45 23 300 ; 
       46 24 300 ; 
       47 0 300 ; 
       48 6 300 ; 
       78 9 300 ; 
       78 10 300 ; 
       78 11 300 ; 
       79 28 300 ; 
       79 29 300 ; 
       79 30 300 ; 
       80 25 300 ; 
       80 26 300 ; 
       80 27 300 ; 
       92 13 300 ; 
       93 31 300 ; 
       94 32 300 ; 
       95 32 300 ; 
       96 31 300 ; 
       97 32 300 ; 
       98 31 300 ; 
       99 31 300 ; 
       100 32 300 ; 
       101 31 300 ; 
       102 32 300 ; 
       103 32 300 ; 
       104 31 300 ; 
       109 35 300 ; 
       90 12 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 0 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 4 401 ; 
       25 5 401 ; 
       26 6 401 ; 
       27 7 401 ; 
       28 8 401 ; 
       29 9 401 ; 
       30 10 401 ; 
       35 11 401 ; 
       36 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 91.43105 18.83884 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 93.93105 18.83884 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 96.43105 18.83884 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20.17855 -31.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 22.67856 -31.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 20.17855 -33.14228 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 2.678554 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 12.67856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10.17856 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 0.1785543 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 12.77611 -4.465402 0 USR DISPLAY 0 0 SRT 1 1 1 2.624681e-006 4.681583 3.14159 -4.307003 -5.434197 -59.14524 MPRFLG 0 ; 
       8 SCHEM 12.67856 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 15.17856 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 17.67855 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 2.678554 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 21.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 5.178555 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 21.42856 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 7.678556 -29.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 51.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 41.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 81.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 31.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 66.42856 -27.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 43.92856 -23.14227 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 43.92856 -25.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 60.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 65.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 82.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 32.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 80.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 30.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 77.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 27.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 75.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 25.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 40.17856 -29.14227 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 42.67856 -29.14227 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 85.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 35.17856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 62.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 67.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 70.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 72.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 45.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 47.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 50.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 52.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 55.17856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 57.67856 -29.14227 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 87.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 37.67856 -29.14227 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 0.1785543 -31.14227 0 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 44.00106 14.83884 0 DISPLAY 0 0 SRT 0.9141061 0.9141058 0.914106 -5.614389 2.071754e-008 3.141592 23.85998 41.70071 23.28196 MPRFLG 0 ; 
       51 SCHEM 57.75106 10.83884 0 MPRFLG 0 ; 
       52 SCHEM 66.43102 -0.7488295 0 DISPLAY 0 0 SRT 1 1 1 3.141592 0.03600004 -1.262822e-008 -0.2862328 -0.0666082 70.66486 MPRFLG 0 ; 
       53 SCHEM 55.25106 10.83884 0 MPRFLG 0 ; 
       54 SCHEM 66.43102 -2.748829 0 DISPLAY 0 0 SRT 1 1 1 3.141592 0.03600004 -1.262855e-008 -0.2715622 8.589921 71.07199 MPRFLG 0 ; 
       55 SCHEM 65.18102 1.251171 0 MPRFLG 0 ; 
       56 SCHEM 65.18102 3.251171 0 MPRFLG 0 ; 
       57 SCHEM 41.50106 10.83884 0 MPRFLG 0 ; 
       58 SCHEM 76.43105 5.251171 0 MPRFLG 0 ; 
       59 SCHEM 42.74139 2.459908 0 USR MPRFLG 0 ; 
       60 SCHEM 88.93105 3.251171 0 DISPLAY 0 0 SRT 1 1 1 -3.019916e-007 1.570796 1.035042e-007 -13.29473 14.37703 79.19988 MPRFLG 0 ; 
       63 SCHEM 54.00106 14.83884 0 MPRFLG 0 ; 
       61 SCHEM 49.00106 14.83884 0 MPRFLG 0 ; 
       68 SCHEM 56.50106 12.83884 0 MPRFLG 0 ; 
       74 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1.081987 0.6737154 0.6737154 0 0 0 58.68601 4.072176 -85.25522 MPRFLG 0 ; 
       75 SCHEM 46.50106 14.83884 0 SRT 0.3071395 0.3071395 0.3071395 -5.614389 3.141593 3.141593 -63.92545 46.33591 -18.61674 MPRFLG 0 ; 
       76 SCHEM 63.93102 -2.748829 0 DISPLAY 0 0 SRT 1 1 1 -7.134793e-008 0.03600004 -6.6153e-008 -0.2715638 -17.52153 71.07199 MPRFLG 0 ; 
       77 SCHEM 63.93102 -0.7488295 0 DISPLAY 0 0 SRT 1 1 1 -7.134793e-008 0.03600004 -6.615301e-008 -0.2862315 -8.865008 70.66486 MPRFLG 0 ; 
       78 SCHEM 12.69182 -1.451101 0 USR DISPLAY 0 0 SRT 1 1 1 -1.604524e-007 1.856808 2.70896e-014 13.54433 -5.192739 1.006159 MPRFLG 0 ; 
       79 SCHEM 43.99139 0.459907 0 MPRFLG 0 ; 
       80 SCHEM 77.68105 3.251171 0 MPRFLG 0 ; 
       81 SCHEM 43.56996 -6.368689 0 WIRECOL 9 7 SRT 1 0.9999998 1 -9.536736e-007 -3.141593 -4.371135e-008 -62.28717 24.15856 -67.80251 MPRFLG 0 ; 
       82 SCHEM 48.56996 -6.368689 0 WIRECOL 9 7 SRT 1 0.9999998 1 2.185572e-008 -4.712389 1.748456e-007 -73.77042 23.99535 -71.82916 MPRFLG 0 ; 
       83 SCHEM 46.06996 -6.368689 0 WIRECOL 9 7 SRT 1 0.9999998 1 2.523375e-007 4.712389 -3.019916e-007 -52.48158 24.01835 -72.0839 MPRFLG 0 ; 
       84 SCHEM 53.56996 -6.368689 0 WIRECOL 9 7 SRT 1 0.9999998 1 1.570795 -3.141593 -4.371135e-008 -62.00538 28.11554 -71.78143 MPRFLG 0 ; 
       85 SCHEM 51.06996 -6.368689 0 WIRECOL 9 7 SRT 1 0.9999998 1 -1.570797 -3.141593 -4.371135e-008 -62.22828 18.63002 -71.83945 MPRFLG 0 ; 
       86 SCHEM 88.00765 -5.929453 0 USR WIRECOL 9 7 SRT 1 1 1 2.363728e-007 0 1.575819e-007 -28.19669 9.933935 86.97993 MPRFLG 0 ; 
       89 SCHEM 62.71174 9.857845 0 USR MPRFLG 0 ; 
       87 SCHEM 76.43105 7.25117 0 USR MPRFLG 0 ; 
       91 SCHEM 12.7442 -2.956304 0 USR DISPLAY 0 0 SRT 2.969366 4.7586 3.172399 0 0 1.570796 0 0 0 MPRFLG 0 ; 
       92 SCHEM 54.00106 16.83884 0 MPRFLG 0 ; 
       93 SCHEM 75.50764 -5.929453 0 USR WIRECOL 3 7 SRT 1 1 1 -3.019916e-007 1.570796 1.903913e-007 -32.62695 8.773946 78.65334 MPRFLG 0 ; 
       94 SCHEM 33.56995 -6.368689 0 WIRECOL 3 7 SRT 1 0.9999998 1 -9.536736e-007 -3.141593 -4.371113e-008 -58.2083 22.84499 -67.57275 MPRFLG 0 ; 
       95 SCHEM 28.56995 -6.368689 0 WIRECOL 3 7 SRT 1 0.9999998 1 -9.536736e-007 -3.141593 -4.371113e-008 -58.2083 22.84499 -66.1222 MPRFLG 0 ; 
       96 SCHEM 70.50763 -5.929453 0 USR WIRECOL 3 7 SRT 1 1 1 -3.019916e-007 1.570796 1.903913e-007 -32.62695 8.773946 77.2028 MPRFLG 0 ; 
       97 SCHEM 31.06995 -6.368689 0 WIRECOL 3 7 SRT 1 0.9999998 1 -9.536736e-007 -3.141593 -4.371113e-008 -58.2083 22.84499 -64.67165 MPRFLG 0 ; 
       98 SCHEM 73.00765 -5.929453 0 USR WIRECOL 3 7 SRT 1 1 1 -3.019916e-007 1.570796 1.903913e-007 -32.62695 8.773946 75.75225 MPRFLG 0 ; 
       99 SCHEM 83.00765 -5.929453 0 USR WIRECOL 3 7 SRT 1 1 1 -3.019916e-007 1.570796 1.903913e-007 -23.59175 8.773948 78.65334 MPRFLG 0 ; 
       100 SCHEM 41.06995 -6.368689 0 WIRECOL 3 7 SRT 1 0.9999998 1 -9.536736e-007 -3.141593 -4.371113e-008 -67.2435 22.845 -67.57275 MPRFLG 0 ; 
       101 SCHEM 78.00765 -5.929453 0 USR WIRECOL 3 7 SRT 1 1 1 -3.019916e-007 1.570796 1.903913e-007 -23.59175 8.773948 77.2028 MPRFLG 0 ; 
       102 SCHEM 36.06995 -6.368689 0 WIRECOL 3 7 SRT 1 0.9999998 1 -9.536736e-007 -3.141593 -4.371113e-008 -67.2435 22.84499 -66.12219 MPRFLG 0 ; 
       103 SCHEM 38.56995 -6.368689 0 WIRECOL 3 7 SRT 1 0.9999998 1 -9.536736e-007 -3.141593 -4.371113e-008 -67.2435 22.84499 -64.67165 MPRFLG 0 ; 
       104 SCHEM 80.50765 -5.929453 0 USR WIRECOL 3 7 SRT 1 1 1 -3.019916e-007 1.570796 1.903913e-007 -23.59175 8.773948 75.75225 MPRFLG 0 ; 
       105 SCHEM 27.74138 0.459907 0 MPRFLG 0 ; 
       106 SCHEM 56.06996 -6.368689 0 WIRECOL 1 7 SRT 1 0.9999998 1 -9.536736e-007 -3.141593 -4.371135e-008 -62.66739 22.18961 -66.26611 MPRFLG 0 ; 
       107 SCHEM 85.50765 -5.929453 0 USR WIRECOL 1 7 SRT 1 1 1 -3.019916e-007 1.570796 1.903913e-007 -28.16786 8.118567 77.3467 MPRFLG 0 ; 
       108 SCHEM 12.65523 -7.732269 0 USR DISPLAY 0 0 SRT 0.9141061 0.2220033 1.212105 -1.606775 3.110701 -4.057471 -95.36144 42.08097 -4.12871 MPRFLG 0 ; 
       109 SCHEM 12.45279 -6.175715 0 USR DISPLAY 0 0 SRT 5.188201 6.851788 5.136831 -0.2889743 -1.688856 -1.315965 35.70973 6.051208 0.5335348 MPRFLG 0 ; 
       90 SCHEM 45.25106 16.83884 0 MPRFLG 0 ; 
       70 SCHEM 41.50106 12.83884 0 MPRFLG 0 ; 
       69 SCHEM 41.50106 14.83884 0 MPRFLG 0 ; 
       62 SCHEM 51.50106 14.83884 0 MPRFLG 0 ; 
       64 SCHEM 56.50106 14.83884 0 MPRFLG 0 ; 
       88 SCHEM 49.00106 18.83884 0 USR SRT 1 0.9999999 0.9999999 4.371141e-008 3.178651e-008 -1.270549e-021 2.399017e-008 8.729084e-008 -2.119473e-009 MPRFLG 0 ; 
       65 SCHEM 98.93105 18.83884 0 DISPLAY 0 0 SRT 1 1 1 1.748456e-007 1.570796 2.536365e-007 -22.73593 10.01901 79.00479 MPRFLG 0 ; 
       66 SCHEM 60.32657 15.4788 0 USR MPRFLG 0 ; 
       67 SCHEM 101.4311 18.83884 0 MPRFLG 0 ; 
       71 SCHEM 103.9311 18.83884 0 DISPLAY 0 0 SRT 0.9141061 0.9141058 0.914106 -5.614389 2.071754e-008 3.141592 16.31581 46.99421 24.01642 MPRFLG 0 ; 
       72 SCHEM 106.4311 18.83884 0 DISPLAY 0 0 SRT 10.27229 5.177568 5.177568 0 0 0 -35.605 21.48756 2.117302 MPRFLG 0 ; 
       73 SCHEM 108.9311 18.83884 0 DISPLAY 0 0 SRT 0.9141061 0.9141058 0.914106 -5.614389 2.071754e-008 3.141592 17.79228 46.99421 24.01642 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 128.6867 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 108.7523 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 149.7275 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 174.9052 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 65.03374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 169.0182 8.362499 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 170.063 8.635059 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 165.3841 9.679878 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 35.6208 28.40201 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 103.509 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 130.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 135.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 140.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 145.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 161.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 166.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 171.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 176.9044 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 153.4978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 160.9978 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 114.2096 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 115.2544 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 110.5754 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 85.03374 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 102.5992 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 50.73668 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 62.53374 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 105.0337 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 105.509 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 169.0182 6.362499 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 170.063 6.635059 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 165.3841 7.679878 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 30.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 35.6208 26.40201 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 114.2096 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 115.2544 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 110.5754 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 85.03374 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 105.509 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
