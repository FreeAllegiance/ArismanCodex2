SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.11-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 45     
       ss303_ordinance-mat6.1-0 ; 
       ss303_ordinance-white_strobe1_32.1-0 ; 
       ss303_ordinance-white_strobe1_33.1-0 ; 
       ss303_ordinance-white_strobe1_34.1-0 ; 
       ss303_ordinance-white_strobe1_35.1-0 ; 
       ss303_ordinance-white_strobe1_36.1-0 ; 
       ss303_ordinance-white_strobe1_37.1-0 ; 
       ss303_ordinance-white_strobe1_38.1-0 ; 
       ss303_ordinance-white_strobe1_39.1-0 ; 
       ss303_ordinance-white_strobe1_40.1-0 ; 
       ss303_ordinance-white_strobe1_41.1-0 ; 
       ss303_ordinance-white_strobe1_42.1-0 ; 
       ss303_ordinance-white_strobe1_43.1-0 ; 
       ss303_ordinance-white_strobe1_44.1-0 ; 
       ss303_ordinance-white_strobe1_45.1-0 ; 
       ss303_ordinance-white_strobe1_46.1-0 ; 
       ss303_ordinance-white_strobe1_47.1-0 ; 
       ss303_ordinance-white_strobe1_48.1-0 ; 
       ss303_ordinance-white_strobe1_49.1-0 ; 
       ss303_ordinance-white_strobe1_50.1-0 ; 
       ss303_ordinance-white_strobe1_51.1-0 ; 
       ss303_ordinance-white_strobe1_52.1-0 ; 
       ss303_ordinance-white_strobe1_53.1-0 ; 
       ss303_ordinance-white_strobe1_54.1-0 ; 
       ss303_ordinance-white_strobe1_55.1-0 ; 
       ss303_ordinance-white_strobe1_56.1-0 ; 
       ss303_ordinance-white_strobe1_57.1-0 ; 
       ss303_ordinance-white_strobe1_58.1-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_electronic-mat59.1-0 ; 
       ss305_electronic-mat60.1-0 ; 
       ss305_electronic-mat61.1-0 ; 
       ss305_electronic-mat62.1-0 ; 
       ss305_electronic-mat63.1-0 ; 
       ss305_electronic-mat64.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 86     
       bounding_model-bound_model.1-0 ROOT ; 
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bound07.1-0 ; 
       bounding_model-bound08.1-0 ; 
       root-craterer1.1-0 ; 
       root-cube1.1-0 ; 
       root-cube12.2-0 ; 
       root-cube2.1-0 ; 
       root-cube22.2-0 ; 
       root-cube3.1-0 ; 
       root-cube33.1-0 ; 
       root-cube34.1-0 ; 
       root-cube35.1-0 ; 
       root-cube4.1-0 ; 
       root-cyl1.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_8_1.3-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-east_bay_11_9_1.4-0 ; 
       root-extru48.1-0 ; 
       root-extru50.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.3-0 ; 
       root-null19.1-0 ; 
       root-null19_1.3-0 ; 
       root-root.1-0 ROOT ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
       root-SS_39.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
       root-SS_62.1-0 ; 
       root-SS_63.1-0 ; 
       root-SS_64.1-0 ; 
       root-tetra1.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-ss303-ordinance.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       ss303_ordinance-rendermap4.3-0 ; 
       ss305_electronic-t2d58.2-0 ; 
       ss305_electronic-t2d59.1-0 ; 
       ss305_electronic-t2d60.1-0 ; 
       ss305_electronic-t2d61.1-0 ; 
       ss305_electronic-t2d62.1-0 ; 
       ss305_electronic-t2d63.1-0 ; 
       ss305_electronic-t2d64.1-0 ; 
       ss305_elect_station-t2d52.2-0 ; 
       ss305_elect_station-t2d53.2-0 ; 
       ss305_elect_station-t2d54.2-0 ; 
       ss305_elect_station-t2d55.2-0 ; 
       ss305_elect_station-t2d56.2-0 ; 
       ss305_elect_station-t2d57.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       56 32 110 ; 
       57 32 110 ; 
       68 33 110 ; 
       61 32 110 ; 
       60 32 110 ; 
       32 9 110 ; 
       34 9 110 ; 
       48 32 110 ; 
       49 32 110 ; 
       50 32 110 ; 
       51 32 110 ; 
       52 34 110 ; 
       53 34 110 ; 
       54 34 110 ; 
       55 34 110 ; 
       59 32 110 ; 
       58 32 110 ; 
       21 9 110 ; 
       62 32 110 ; 
       63 32 110 ; 
       26 21 110 ; 
       27 21 110 ; 
       28 21 110 ; 
       29 21 110 ; 
       30 21 110 ; 
       37 21 110 ; 
       38 21 110 ; 
       40 21 110 ; 
       43 21 110 ; 
       45 21 110 ; 
       46 21 110 ; 
       84 21 110 ; 
       9 35 110 ; 
       10 19 110 ; 
       11 16 110 ; 
       12 19 110 ; 
       13 16 110 ; 
       14 9 110 ; 
       64 34 110 ; 
       65 34 110 ; 
       66 34 110 ; 
       15 17 110 ; 
       16 17 110 ; 
       17 9 110 ; 
       2 0 110 ; 
       4 0 110 ; 
       18 9 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       19 9 110 ; 
       20 18 110 ; 
       22 17 110 ; 
       23 9 110 ; 
       24 9 110 ; 
       25 18 110 ; 
       31 23 110 ; 
       67 34 110 ; 
       1 0 110 ; 
       36 23 110 ; 
       39 23 110 ; 
       41 23 110 ; 
       42 23 110 ; 
       44 23 110 ; 
       47 23 110 ; 
       83 19 110 ; 
       85 23 110 ; 
       69 33 110 ; 
       33 9 110 ; 
       70 33 110 ; 
       71 33 110 ; 
       72 33 110 ; 
       73 34 110 ; 
       74 34 110 ; 
       75 34 110 ; 
       76 34 110 ; 
       77 34 110 ; 
       78 34 110 ; 
       3 0 110 ; 
       79 32 110 ; 
       80 32 110 ; 
       81 32 110 ; 
       82 32 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       56 1 300 ; 
       57 2 300 ; 
       68 13 300 ; 
       61 3 300 ; 
       60 4 300 ; 
       48 44 300 ; 
       49 44 300 ; 
       50 44 300 ; 
       51 44 300 ; 
       52 43 300 ; 
       53 43 300 ; 
       54 43 300 ; 
       55 43 300 ; 
       59 5 300 ; 
       58 6 300 ; 
       21 29 300 ; 
       21 30 300 ; 
       21 31 300 ; 
       62 7 300 ; 
       63 8 300 ; 
       37 42 300 ; 
       38 42 300 ; 
       40 42 300 ; 
       43 42 300 ; 
       45 42 300 ; 
       46 42 300 ; 
       9 0 300 ; 
       64 9 300 ; 
       65 10 300 ; 
       66 11 300 ; 
       20 38 300 ; 
       20 39 300 ; 
       20 40 300 ; 
       22 35 300 ; 
       22 36 300 ; 
       22 37 300 ; 
       23 32 300 ; 
       23 33 300 ; 
       23 34 300 ; 
       24 28 300 ; 
       67 12 300 ; 
       36 41 300 ; 
       39 41 300 ; 
       41 41 300 ; 
       42 41 300 ; 
       44 41 300 ; 
       47 41 300 ; 
       69 14 300 ; 
       70 15 300 ; 
       71 16 300 ; 
       72 17 300 ; 
       73 18 300 ; 
       74 19 300 ; 
       75 20 300 ; 
       76 21 300 ; 
       77 22 300 ; 
       78 23 300 ; 
       79 24 300 ; 
       80 25 300 ; 
       81 26 300 ; 
       82 27 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       28 1 401 ; 
       29 2 401 ; 
       30 3 401 ; 
       31 4 401 ; 
       32 5 401 ; 
       33 6 401 ; 
       34 7 401 ; 
       35 8 401 ; 
       36 9 401 ; 
       37 10 401 ; 
       38 11 401 ; 
       39 12 401 ; 
       40 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       56 SCHEM 21.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 23.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 111.2022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 33.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 31.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 29.95219 -8.260105 0 MPRFLG 0 ; 
       34 SCHEM 92.45218 -8.260105 0 MPRFLG 0 ; 
       48 SCHEM 11.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 13.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 16.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 18.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 83.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 81.20218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 78.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 76.20221 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 28.70218 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 26.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM -5.047804 -8.260105 0 MPRFLG 0 ; 
       62 SCHEM 36.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 38.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM -3.797804 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 1.202192 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM -1.297808 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 6.202195 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 3.702192 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM -13.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM -18.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM -16.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM -6.297804 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM -11.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM -8.797803 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 8.702194 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 39.95219 -6.260105 0 USR MPRFLG 0 ; 
       10 SCHEM 66.20221 -10.2601 0 MPRFLG 0 ; 
       11 SCHEM 56.20219 -12.2601 0 MPRFLG 0 ; 
       12 SCHEM 63.70219 -10.2601 0 MPRFLG 0 ; 
       13 SCHEM 58.70219 -12.2601 0 MPRFLG 0 ; 
       14 SCHEM 68.70221 -8.260105 0 MPRFLG 0 ; 
       64 SCHEM 86.20218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 88.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 91.20218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 53.70219 -10.2601 0 MPRFLG 0 ; 
       16 SCHEM 57.45219 -10.2601 0 MPRFLG 0 ; 
       17 SCHEM 54.95219 -8.260105 0 MPRFLG 0 ; 
       2 SCHEM 32.1616 6.085018 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 37.1616 6.085018 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 72.45221 -8.260105 0 MPRFLG 0 ; 
       5 SCHEM 39.6616 6.085018 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 42.1616 6.085018 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 44.6616 6.085018 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 47.1616 6.085018 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 63.70219 -8.260105 0 MPRFLG 0 ; 
       20 SCHEM 73.70221 -10.2601 0 MPRFLG 0 ; 
       22 SCHEM 51.20219 -10.2601 0 MPRFLG 0 ; 
       23 SCHEM -32.54781 -8.260105 0 MPRFLG 0 ; 
       24 SCHEM -21.29781 -8.260105 0 MPRFLG 0 ; 
       25 SCHEM 71.20221 -10.2601 0 MPRFLG 0 ; 
       31 SCHEM -23.79781 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 38.4116 8.085018 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       67 SCHEM 93.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       1 SCHEM 29.6616 6.085018 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM -36.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM -41.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM -38.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM -28.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM -33.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM -31.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 61.20219 -10.2601 0 MPRFLG 0 ; 
       85 SCHEM -26.29781 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 113.7022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 116.2022 -8.260105 0 MPRFLG 0 ; 
       70 SCHEM 116.2022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 118.7022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 121.2022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 39.95219 -2.486912 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       73 SCHEM 96.20218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 98.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 101.2022 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       76 SCHEM 103.7022 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       77 SCHEM 106.2022 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       78 SCHEM 108.7022 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 34.6616 6.085018 0 DISPLAY 2 2 MPRFLG 0 ; 
       79 SCHEM 41.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       80 SCHEM 43.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       81 SCHEM 46.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       82 SCHEM 48.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 44.65828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49.65828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 139.8538 -83.65749 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM -9.105298 -62.96942 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM -8.060472 -62.69686 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM -12.73946 -61.65203 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -41.52466 -69.17903 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM -40.47983 -68.90647 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM -45.15881 -67.86164 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM -50.7157 -75.69625 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM -91.46696 -63.43491 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.49999 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 82.4609 40.60352 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.4609 40.60352 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 52.15828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59.65828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 67.15827 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 74.65826 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 78.0063 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 80.5063 52.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 88.0063 52.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 90.5063 54.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 93.0063 56.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 115.704 60.10961 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 123.204 60.10961 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 93.12297 48.3745 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 100.623 48.3745 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 126.2813 73.75259 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 133.7813 73.75259 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 139.8538 -85.65749 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM -9.105298 -64.96942 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM -8.060472 -64.69686 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -12.73946 -63.65203 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -41.52466 -71.17903 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -40.47983 -70.90647 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -45.15881 -69.86164 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
