SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.10-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 45     
       ss303_ordinance-mat6.1-0 ; 
       ss303_ordinance-white_strobe1_32.1-0 ; 
       ss303_ordinance-white_strobe1_33.1-0 ; 
       ss303_ordinance-white_strobe1_34.1-0 ; 
       ss303_ordinance-white_strobe1_35.1-0 ; 
       ss303_ordinance-white_strobe1_36.1-0 ; 
       ss303_ordinance-white_strobe1_37.1-0 ; 
       ss303_ordinance-white_strobe1_38.1-0 ; 
       ss303_ordinance-white_strobe1_39.1-0 ; 
       ss303_ordinance-white_strobe1_40.1-0 ; 
       ss303_ordinance-white_strobe1_41.1-0 ; 
       ss303_ordinance-white_strobe1_42.1-0 ; 
       ss303_ordinance-white_strobe1_43.1-0 ; 
       ss303_ordinance-white_strobe1_44.1-0 ; 
       ss303_ordinance-white_strobe1_45.1-0 ; 
       ss303_ordinance-white_strobe1_46.1-0 ; 
       ss303_ordinance-white_strobe1_47.1-0 ; 
       ss303_ordinance-white_strobe1_48.1-0 ; 
       ss303_ordinance-white_strobe1_49.1-0 ; 
       ss303_ordinance-white_strobe1_50.1-0 ; 
       ss303_ordinance-white_strobe1_51.1-0 ; 
       ss303_ordinance-white_strobe1_52.1-0 ; 
       ss303_ordinance-white_strobe1_53.1-0 ; 
       ss303_ordinance-white_strobe1_54.1-0 ; 
       ss303_ordinance-white_strobe1_55.1-0 ; 
       ss303_ordinance-white_strobe1_56.1-0 ; 
       ss303_ordinance-white_strobe1_57.1-0 ; 
       ss303_ordinance-white_strobe1_58.1-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_electronic-mat59.1-0 ; 
       ss305_electronic-mat60.1-0 ; 
       ss305_electronic-mat61.1-0 ; 
       ss305_electronic-mat62.1-0 ; 
       ss305_electronic-mat63.1-0 ; 
       ss305_electronic-mat64.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 86     
       2-null18.3-0 ROOT ; 
       2-null19.3-0 ROOT ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       2-SS_37.1-0 ; 
       2-SS_39.1-0 ; 
       2-SS_40.1-0 ; 
       2-SS_41.1-0 ; 
       2-SS_42.1-0 ; 
       2-SS_43.1-0 ; 
       2-SS_44.1-0 ; 
       2-SS_45.1-0 ; 
       2-SS_46.1-0 ; 
       2-SS_47.1-0 ; 
       2-SS_48.1-0 ; 
       2-SS_49.1-0 ; 
       2-SS_55.1-0 ; 
       2-SS_56.1-0 ; 
       2-SS_57.1-0 ; 
       2-SS_58.1-0 ; 
       2-SS_59.1-0 ; 
       2-SS_60.1-0 ; 
       2-SS_61.1-0 ; 
       2-SS_62.1-0 ; 
       2-SS_63.1-0 ; 
       2-SS_64.1-0 ; 
       3-craterer1.1-0 ROOT ; 
       3-cube1.1-0 ; 
       3-cube12.2-0 ; 
       3-cube2.1-0 ; 
       3-cube22.2-0 ; 
       3-cube3.1-0 ; 
       3-cube33.1-0 ; 
       3-cube34.1-0 ; 
       3-cube35.1-0 ; 
       3-cube4.1-0 ; 
       3-cyl1.1-0 ; 
       3-east_bay_11_8.1-0 ; 
       3-east_bay_11_9.1-0 ; 
       3-extru48.1-0 ; 
       3-extru50.1-0 ; 
       3-null19.1-0 ROOT ; 
       3-SS_50.1-0 ; 
       3-SS_51.1-0 ; 
       3-SS_52.1-0 ; 
       3-SS_53.1-0 ; 
       3-SS_54.1-0 ; 
       3-tetra1.1-0 ; 
       purpmeteor-east_bay_11_8_1.3-0 ROOT ; 
       purpmeteor-garage1A.1-0 ; 
       purpmeteor-garage1B.1-0 ; 
       purpmeteor-garage1C.1-0 ; 
       purpmeteor-garage1D.1-0 ; 
       purpmeteor-garage1E.1-0 ; 
       purpmeteor-SS_11_1.1-0 ; 
       purpmeteor-SS_13_2.1-0 ; 
       purpmeteor-SS_15_1.1-0 ; 
       purpmeteor-SS_23_2.1-0 ; 
       purpmeteor-SS_24_1.1-0 ; 
       purpmeteor-SS_26.1-0 ; 
       purpmeteor-turwepemt2.1-0 ; 
       ss303_ordinance-cube36.1-0 ; 
       ss303_ordinance-cube37.1-0 ; 
       ss303_ordinance-cube38.1-0 ; 
       ss303_ordinance-cube39.1-0 ; 
       ss303_ordinance-cube40.1-0 ; 
       ss303_ordinance-cube41.1-0 ; 
       ss303_ordinance-cube42.1-0 ; 
       ss303_ordinance-cube43.1-0 ; 
       ss303_ordinance-east_bay_11_9_1.4-0 ROOT ; 
       ss303_ordinance-launch1.1-0 ; 
       ss303_ordinance-null20.2-0 ROOT ; 
       ss303_ordinance-sphere2.1-0 ; 
       ss303_ordinance-SS_11.1-0 ; 
       ss303_ordinance-SS_13_3.1-0 ; 
       ss303_ordinance-SS_15_3.1-0 ; 
       ss303_ordinance-SS_23.1-0 ; 
       ss303_ordinance-SS_24.1-0 ; 
       ss303_ordinance-SS_26_3.1-0 ; 
       ss303_ordinance-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss303-ordinance.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       ss303_ordinance-rendermap4.2-0 ; 
       ss305_electronic-t2d58.1-0 ; 
       ss305_electronic-t2d59.1-0 ; 
       ss305_electronic-t2d60.1-0 ; 
       ss305_electronic-t2d61.1-0 ; 
       ss305_electronic-t2d62.1-0 ; 
       ss305_electronic-t2d63.1-0 ; 
       ss305_electronic-t2d64.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 0 110 ; 
       11 0 110 ; 
       48 47 110 ; 
       15 0 110 ; 
       14 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       13 0 110 ; 
       12 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       55 54 110 ; 
       56 54 110 ; 
       57 54 110 ; 
       58 54 110 ; 
       59 54 110 ; 
       60 54 110 ; 
       61 54 110 ; 
       62 54 110 ; 
       63 54 110 ; 
       64 54 110 ; 
       65 54 110 ; 
       66 54 110 ; 
       33 42 110 ; 
       34 39 110 ; 
       35 42 110 ; 
       36 39 110 ; 
       37 32 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       38 40 110 ; 
       39 40 110 ; 
       40 32 110 ; 
       67 77 110 ; 
       68 77 110 ; 
       69 77 110 ; 
       70 77 110 ; 
       41 32 110 ; 
       71 77 110 ; 
       72 77 110 ; 
       73 77 110 ; 
       74 77 110 ; 
       42 32 110 ; 
       43 41 110 ; 
       44 40 110 ; 
       45 32 110 ; 
       46 41 110 ; 
       76 75 110 ; 
       21 1 110 ; 
       78 77 110 ; 
       79 75 110 ; 
       80 75 110 ; 
       81 75 110 ; 
       82 75 110 ; 
       83 75 110 ; 
       84 75 110 ; 
       53 42 110 ; 
       85 75 110 ; 
       49 47 110 ; 
       50 47 110 ; 
       51 47 110 ; 
       52 47 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 1 110 ; 
       27 1 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 0 110 ; 
       31 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 1 300 ; 
       11 2 300 ; 
       48 13 300 ; 
       15 3 300 ; 
       14 4 300 ; 
       2 44 300 ; 
       3 44 300 ; 
       4 44 300 ; 
       5 44 300 ; 
       6 43 300 ; 
       7 43 300 ; 
       8 43 300 ; 
       9 43 300 ; 
       13 5 300 ; 
       12 6 300 ; 
       54 29 300 ; 
       54 30 300 ; 
       54 31 300 ; 
       16 7 300 ; 
       17 8 300 ; 
       60 42 300 ; 
       61 42 300 ; 
       62 42 300 ; 
       63 42 300 ; 
       64 42 300 ; 
       65 42 300 ; 
       32 0 300 ; 
       18 9 300 ; 
       19 10 300 ; 
       20 11 300 ; 
       43 38 300 ; 
       43 39 300 ; 
       43 40 300 ; 
       44 35 300 ; 
       44 36 300 ; 
       44 37 300 ; 
       75 32 300 ; 
       75 33 300 ; 
       75 34 300 ; 
       45 28 300 ; 
       21 12 300 ; 
       79 41 300 ; 
       80 41 300 ; 
       81 41 300 ; 
       82 41 300 ; 
       83 41 300 ; 
       84 41 300 ; 
       49 14 300 ; 
       50 15 300 ; 
       51 16 300 ; 
       52 17 300 ; 
       22 18 300 ; 
       23 19 300 ; 
       24 20 300 ; 
       25 21 300 ; 
       26 22 300 ; 
       27 23 300 ; 
       28 24 300 ; 
       29 25 300 ; 
       30 26 300 ; 
       31 27 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       28 1 401 ; 
       29 2 401 ; 
       30 3 401 ; 
       31 4 401 ; 
       32 5 401 ; 
       33 6 401 ; 
       34 7 401 ; 
       35 8 401 ; 
       36 9 401 ; 
       37 10 401 ; 
       38 11 401 ; 
       39 12 401 ; 
       40 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 22.85567 -21.84733 0 WIRECOL 2 7 MPRFLG 0 ; 
       11 SCHEM 25.35567 -21.84733 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 55.24718 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 35.17683 -21.77099 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       14 SCHEM 32.67683 -21.77099 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       0 SCHEM 19.10567 -19.84733 0 USR SRT 0.9800001 0.9800001 0.9800001 0 1.570796 0 -3.798211 -24.7352 -46.99055 MPRFLG 0 ; 
       1 SCHEM 44.50883 -14.74244 0 USR SRT 0.4588489 0.4588489 0.4588489 0 0 0 -35.9509 -2.293136 -32.81833 MPRFLG 0 ; 
       2 SCHEM 12.85567 -21.84733 0 WIRECOL 2 7 MPRFLG 0 ; 
       3 SCHEM 15.35567 -21.84733 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 17.85567 -21.84733 0 WIRECOL 2 7 MPRFLG 0 ; 
       5 SCHEM 20.35567 -21.84733 0 WIRECOL 2 7 MPRFLG 0 ; 
       6 SCHEM 35.75883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       7 SCHEM 33.25883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       8 SCHEM 30.75883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 28.25883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 30.17683 -21.77099 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       12 SCHEM 27.67683 -21.77099 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 13.75 -8 0 SRT 0.9999999 0.9999999 1 1.125474e-007 6.283186 2.500181e-008 1.141764 0.9347438 19.95891 MPRFLG 0 ; 
       16 SCHEM 37.67682 -21.77099 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       17 SCHEM 40.17683 -21.77099 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 15 -10 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       56 SCHEM 20 -10 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       57 SCHEM 17.5 -10 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       58 SCHEM 25 -10 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       59 SCHEM 22.5 -10 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       60 SCHEM 5 -10 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 0 -10 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 2.5 -10 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 12.5 -10 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 7.499998 -10 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 9.999998 -10 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 27.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 25.48979 2.242093 0 USR SRT 6.063473 6.063473 6.063473 0 0 0 -9.561106e-008 -2.304166e-006 -2.650467e-009 MPRFLG 0 ; 
       33 SCHEM 30.48979 -1.757907 0 MPRFLG 0 ; 
       34 SCHEM 20.48979 -3.757907 0 MPRFLG 0 ; 
       35 SCHEM 27.98979 -1.757907 0 MPRFLG 0 ; 
       36 SCHEM 22.98979 -3.757907 0 MPRFLG 0 ; 
       37 SCHEM 32.98979 0.2420933 0 MPRFLG 0 ; 
       18 SCHEM 38.25883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 40.75883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 43.25883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 17.98979 -1.757907 0 MPRFLG 0 ; 
       39 SCHEM 21.73979 -1.757907 0 MPRFLG 0 ; 
       40 SCHEM 19.23979 0.2420933 0 MPRFLG 0 ; 
       67 SCHEM 30.65828 9.378092 0 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 33.15828 9.378092 0 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 35.65828 9.378092 0 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 38.15828 9.378092 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 36.73979 0.2420933 0 MPRFLG 0 ; 
       71 SCHEM 40.65828 9.378092 0 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 43.15828 9.378092 0 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 45.65828 9.378092 0 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 48.15828 9.378092 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 27.98979 0.2420933 0 MPRFLG 0 ; 
       43 SCHEM 37.98979 -1.757907 0 MPRFLG 0 ; 
       44 SCHEM 15.48979 -1.757907 0 MPRFLG 0 ; 
       75 SCHEM 8.750001 -12 0 SRT 0.9999999 0.9999999 1 7.877882e-007 6.283185 -1.15339e-007 -21.40607 -8.282479 -8.820705 MPRFLG 0 ; 
       45 SCHEM 12.98979 0.2420933 0 MPRFLG 0 ; 
       46 SCHEM 35.48979 -1.757907 0 MPRFLG 0 ; 
       76 SCHEM 17.5 -14 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       77 SCHEM 38.15828 11.37809 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 45.75883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       78 SCHEM 28.15828 9.378092 0 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 5.000001 -14 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 0 -14 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 2.500001 -14 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 12.5 -14 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 7.500001 -14 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 9.999998 -14 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 25.48979 -1.757907 0 MPRFLG 0 ; 
       85 SCHEM 15 -14 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 57.74718 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 60.24718 11.37809 0 SRT 0.4588489 0.4588489 0.4588489 0 0 0 -35.9509 -2.293136 -32.81833 MPRFLG 0 ; 
       50 SCHEM 60.24718 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 62.74718 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 65.24718 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 48.25883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 50.75883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 53.25883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 55.75883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 58.25883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 60.75883 -16.74244 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 42.71007 -21.72345 0 WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 45.21007 -21.72345 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 47.71008 -21.72345 0 WIRECOL 2 7 MPRFLG 0 ; 
       31 SCHEM 50.21007 -21.72345 0 WIRECOL 2 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 44.65828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49.65828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 139.8538 -83.65749 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM -9.105298 -62.96942 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM -8.060472 -62.69686 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM -12.73946 -61.65203 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -41.52466 -69.17903 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM -40.47983 -68.90647 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM -45.15881 -67.86164 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM -50.7157 -75.69625 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM -91.46696 -63.43491 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.49999 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 82.4609 40.60352 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.4609 40.60352 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 52.15828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59.65828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 67.15827 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 74.65826 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 78.0063 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 80.5063 52.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 88.0063 52.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 90.5063 54.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 93.0063 56.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 115.704 60.10961 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 123.204 60.10961 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 93.12297 48.3745 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 100.623 48.3745 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 126.2813 73.75259 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 133.7813 73.75259 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 139.8538 -85.65749 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM -9.105298 -64.96942 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM -8.060472 -64.69686 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -12.73946 -63.65203 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -41.52466 -71.17903 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -40.47983 -70.90647 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -45.15881 -69.86164 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
