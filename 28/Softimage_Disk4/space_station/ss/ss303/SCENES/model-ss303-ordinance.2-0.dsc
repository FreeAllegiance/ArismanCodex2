SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.12-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 45     
       ss303_ordinance-mat6.1-0 ; 
       ss303_ordinance-white_strobe1_32.1-0 ; 
       ss303_ordinance-white_strobe1_33.1-0 ; 
       ss303_ordinance-white_strobe1_34.1-0 ; 
       ss303_ordinance-white_strobe1_35.1-0 ; 
       ss303_ordinance-white_strobe1_36.1-0 ; 
       ss303_ordinance-white_strobe1_37.1-0 ; 
       ss303_ordinance-white_strobe1_38.1-0 ; 
       ss303_ordinance-white_strobe1_39.1-0 ; 
       ss303_ordinance-white_strobe1_40.1-0 ; 
       ss303_ordinance-white_strobe1_41.1-0 ; 
       ss303_ordinance-white_strobe1_42.1-0 ; 
       ss303_ordinance-white_strobe1_43.1-0 ; 
       ss303_ordinance-white_strobe1_44.1-0 ; 
       ss303_ordinance-white_strobe1_45.1-0 ; 
       ss303_ordinance-white_strobe1_46.1-0 ; 
       ss303_ordinance-white_strobe1_47.1-0 ; 
       ss303_ordinance-white_strobe1_48.1-0 ; 
       ss303_ordinance-white_strobe1_49.1-0 ; 
       ss303_ordinance-white_strobe1_50.1-0 ; 
       ss303_ordinance-white_strobe1_51.1-0 ; 
       ss303_ordinance-white_strobe1_52.1-0 ; 
       ss303_ordinance-white_strobe1_53.1-0 ; 
       ss303_ordinance-white_strobe1_54.1-0 ; 
       ss303_ordinance-white_strobe1_55.1-0 ; 
       ss303_ordinance-white_strobe1_56.1-0 ; 
       ss303_ordinance-white_strobe1_57.1-0 ; 
       ss303_ordinance-white_strobe1_58.1-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_electronic-mat59.1-0 ; 
       ss305_electronic-mat60.1-0 ; 
       ss305_electronic-mat61.1-0 ; 
       ss305_electronic-mat62.1-0 ; 
       ss305_electronic-mat63.1-0 ; 
       ss305_electronic-mat64.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 77     
       root-craterer1.1-0 ; 
       root-cube1.1-0 ; 
       root-cube12.2-0 ; 
       root-cube2.1-0 ; 
       root-cube22.2-0 ; 
       root-cube3.1-0 ; 
       root-cube33.1-0 ; 
       root-cube34.1-0 ; 
       root-cube35.1-0 ; 
       root-cube4.1-0 ; 
       root-cyl1.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_8_1.3-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-east_bay_11_9_1.4-0 ; 
       root-extru48.1-0 ; 
       root-extru50.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.3-0 ; 
       root-null19.1-0 ; 
       root-null19_1.3-0 ; 
       root-root.2-0 ROOT ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
       root-SS_39.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
       root-SS_62.1-0 ; 
       root-SS_63.1-0 ; 
       root-SS_64.1-0 ; 
       root-tetra1.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss303-ordinance.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       ss303_ordinance-rendermap4.3-0 ; 
       ss305_electronic-t2d58.2-0 ; 
       ss305_electronic-t2d59.1-0 ; 
       ss305_electronic-t2d60.1-0 ; 
       ss305_electronic-t2d61.1-0 ; 
       ss305_electronic-t2d62.1-0 ; 
       ss305_electronic-t2d63.1-0 ; 
       ss305_electronic-t2d64.1-0 ; 
       ss305_elect_station-t2d52.2-0 ; 
       ss305_elect_station-t2d53.2-0 ; 
       ss305_elect_station-t2d54.2-0 ; 
       ss305_elect_station-t2d55.2-0 ; 
       ss305_elect_station-t2d56.2-0 ; 
       ss305_elect_station-t2d57.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 26 110 ; 
       1 10 110 ; 
       2 7 110 ; 
       3 10 110 ; 
       4 7 110 ; 
       5 0 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 9 110 ; 
       12 0 110 ; 
       13 8 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 9 110 ; 
       17 12 110 ; 
       18 12 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 14 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       27 14 110 ; 
       28 12 110 ; 
       29 12 110 ; 
       30 14 110 ; 
       31 12 110 ; 
       32 14 110 ; 
       33 14 110 ; 
       34 12 110 ; 
       35 14 110 ; 
       36 12 110 ; 
       37 12 110 ; 
       38 14 110 ; 
       39 23 110 ; 
       40 23 110 ; 
       41 23 110 ; 
       42 23 110 ; 
       43 25 110 ; 
       44 25 110 ; 
       45 25 110 ; 
       46 25 110 ; 
       47 23 110 ; 
       48 23 110 ; 
       49 23 110 ; 
       50 23 110 ; 
       51 23 110 ; 
       52 23 110 ; 
       53 23 110 ; 
       54 23 110 ; 
       55 25 110 ; 
       56 25 110 ; 
       57 25 110 ; 
       58 25 110 ; 
       59 24 110 ; 
       60 24 110 ; 
       61 24 110 ; 
       62 24 110 ; 
       63 24 110 ; 
       64 25 110 ; 
       65 25 110 ; 
       66 25 110 ; 
       67 25 110 ; 
       68 25 110 ; 
       69 25 110 ; 
       70 23 110 ; 
       71 23 110 ; 
       72 23 110 ; 
       73 23 110 ; 
       74 10 110 ; 
       75 12 110 ; 
       76 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       11 38 300 ; 
       11 39 300 ; 
       11 40 300 ; 
       12 29 300 ; 
       12 30 300 ; 
       12 31 300 ; 
       13 35 300 ; 
       13 36 300 ; 
       13 37 300 ; 
       14 32 300 ; 
       14 33 300 ; 
       14 34 300 ; 
       15 28 300 ; 
       27 41 300 ; 
       28 42 300 ; 
       29 42 300 ; 
       30 41 300 ; 
       31 42 300 ; 
       32 41 300 ; 
       33 41 300 ; 
       34 42 300 ; 
       35 41 300 ; 
       36 42 300 ; 
       37 42 300 ; 
       38 41 300 ; 
       39 44 300 ; 
       40 44 300 ; 
       41 44 300 ; 
       42 44 300 ; 
       43 43 300 ; 
       44 43 300 ; 
       45 43 300 ; 
       46 43 300 ; 
       47 1 300 ; 
       48 2 300 ; 
       49 6 300 ; 
       50 5 300 ; 
       51 4 300 ; 
       52 3 300 ; 
       53 7 300 ; 
       54 8 300 ; 
       55 9 300 ; 
       56 10 300 ; 
       57 11 300 ; 
       58 12 300 ; 
       59 13 300 ; 
       60 14 300 ; 
       61 15 300 ; 
       62 16 300 ; 
       63 17 300 ; 
       64 18 300 ; 
       65 19 300 ; 
       66 20 300 ; 
       67 21 300 ; 
       68 22 300 ; 
       69 23 300 ; 
       70 24 300 ; 
       71 25 300 ; 
       72 26 300 ; 
       73 27 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       28 1 401 ; 
       29 2 401 ; 
       30 3 401 ; 
       31 4 401 ; 
       32 5 401 ; 
       33 6 401 ; 
       34 7 401 ; 
       35 8 401 ; 
       36 9 401 ; 
       37 10 401 ; 
       38 11 401 ; 
       39 12 401 ; 
       40 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 39.95219 -6.260105 0 USR MPRFLG 0 ; 
       1 SCHEM 66.20221 -10.2601 0 MPRFLG 0 ; 
       2 SCHEM 56.20219 -12.2601 0 MPRFLG 0 ; 
       3 SCHEM 63.70219 -10.2601 0 MPRFLG 0 ; 
       4 SCHEM 58.70219 -12.2601 0 MPRFLG 0 ; 
       5 SCHEM 68.70221 -8.260105 0 MPRFLG 0 ; 
       6 SCHEM 53.70219 -10.2601 0 MPRFLG 0 ; 
       7 SCHEM 57.45219 -10.2601 0 MPRFLG 0 ; 
       8 SCHEM 54.95219 -8.260105 0 MPRFLG 0 ; 
       9 SCHEM 72.45221 -8.260105 0 MPRFLG 0 ; 
       10 SCHEM 63.70219 -8.260105 0 MPRFLG 0 ; 
       11 SCHEM 73.70221 -10.2601 0 MPRFLG 0 ; 
       12 SCHEM -5.047804 -8.260105 0 MPRFLG 0 ; 
       13 SCHEM 51.20219 -10.2601 0 MPRFLG 0 ; 
       14 SCHEM -32.54781 -8.260105 0 MPRFLG 0 ; 
       15 SCHEM -21.29781 -8.260105 0 MPRFLG 0 ; 
       16 SCHEM 71.20221 -10.2601 0 MPRFLG 0 ; 
       17 SCHEM -3.797804 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 1.202192 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM -1.297808 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 6.202195 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 3.702192 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM -23.79781 -10.2601 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 29.95219 -8.260105 0 MPRFLG 0 ; 
       24 SCHEM 116.2022 -8.260105 0 MPRFLG 0 ; 
       25 SCHEM 92.45218 -8.260105 0 MPRFLG 0 ; 
       26 SCHEM 39.95219 -2.486912 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM -36.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM -13.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM -18.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM -41.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM -16.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM -38.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM -28.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM -6.297804 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM -33.79781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM -11.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM -8.797803 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM -31.29781 -10.2601 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 11.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 13.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 16.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 18.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       43 SCHEM 83.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 81.20218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 78.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 76.20221 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 21.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 23.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 26.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 28.70218 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 31.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 33.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 36.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 38.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 86.20218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 88.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 91.20218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 93.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 111.2022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 113.7022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 116.2022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 118.7022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 121.2022 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 96.20218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 98.70218 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 101.2022 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       67 SCHEM 103.7022 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       68 SCHEM 106.2022 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       69 SCHEM 108.7022 -10.2601 0 WIRECOL 4 7 MPRFLG 0 ; 
       70 SCHEM 41.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       71 SCHEM 43.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       72 SCHEM 46.20219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       73 SCHEM 48.70219 -10.2601 0 WIRECOL 2 7 MPRFLG 0 ; 
       74 SCHEM 61.20219 -10.2601 0 MPRFLG 0 ; 
       75 SCHEM 8.702194 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM -26.29781 -10.2601 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44.65828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49.65828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.80262 15.22542 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 82.4609 40.60352 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.4609 40.60352 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 52.15828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59.65828 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 67.15827 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 74.65826 9.378093 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 78.0063 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 80.5063 52.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 88.0063 52.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 90.5063 54.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 93.0063 56.31734 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 101.3452 34.76287 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 115.704 60.10961 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 123.204 60.10961 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 93.12297 48.3745 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 100.623 48.3745 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 126.2813 73.75259 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 133.7813 73.75259 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 139.8538 -83.65749 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM -9.105298 -62.96942 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM -8.060472 -62.69686 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM -12.73946 -61.65203 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -41.52466 -69.17903 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM -40.47983 -68.90647 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM -45.15881 -67.86164 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM -50.7157 -75.69625 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM -91.46696 -63.43491 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.49999 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 139.8538 -85.65749 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM -9.105298 -64.96942 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM -8.060472 -64.69686 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -12.73946 -63.65203 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -41.52466 -71.17903 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -40.47983 -70.90647 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -45.15881 -69.86164 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
