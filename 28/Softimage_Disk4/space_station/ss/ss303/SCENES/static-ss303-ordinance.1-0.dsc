SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.72-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       ss303_ordinance-mat6.3-0 ; 
       ss303_ordinance-mat65.3-0 ; 
       ss303_ordinance-mat66.4-0 ; 
       ss303_ordinance-mat67.3-0 ; 
       ss303_ordinance-mat68.3-0 ; 
       ss303_ordinance-mat70.5-0 ; 
       ss303_ordinance-mat71.3-0 ; 
       ss303_ordinance-mat72.4-0 ; 
       ss303_ordinance-mat73.5-0 ; 
       ss303_ordinance-mat74.3-0 ; 
       ss303_ordinance-mat75.3-0 ; 
       ss303_ordinance-mat76.3-0 ; 
       ss303_ordinance-mat77.3-0 ; 
       ss303_ordinance-mat78.2-0 ; 
       ss303_ordinance-mat79.3-0 ; 
       ss303_ordinance-mat80.2-0 ; 
       ss303_ordinance-mat81.2-0 ; 
       ss303_ordinance-mat82.2-0 ; 
       ss303_ordinance-mat83.2-0 ; 
       ss303_ordinance-mat84.2-0 ; 
       ss305_electronic-mat58.3-0 ; 
       ss305_electronic-mat59.2-0 ; 
       ss305_electronic-mat60.2-0 ; 
       ss305_electronic-mat61.2-0 ; 
       ss305_electronic-mat62.2-0 ; 
       ss305_electronic-mat63.2-0 ; 
       ss305_electronic-mat64.2-0 ; 
       ss305_elect_station-mat52.3-0 ; 
       ss305_elect_station-mat53.3-0 ; 
       ss305_elect_station-mat54.3-0 ; 
       ss305_elect_station-mat55.2-0 ; 
       ss305_elect_station-mat56.2-0 ; 
       ss305_elect_station-mat57.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       root-craterer1.1-0 ; 
       root-cube1.1-0 ; 
       root-cube12.2-0 ; 
       root-cube2.1-0 ; 
       root-cube22.2-0 ; 
       root-cube3.1-0 ; 
       root-cube33.1-0 ; 
       root-cube34.1-0 ; 
       root-cube35.1-0 ; 
       root-cube4.1-0 ; 
       root-cyl1.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_8_1.3-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-east_bay_11_9_1.4-0 ; 
       root-extru48.1-0 ; 
       root-extru50.1-0 ; 
       root-root.49-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss303/PICTURES/beltersbay ; 
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss303/PICTURES/ss303 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss303/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss303-ordinance.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       ss303_ordinance-rendermap4.6-0 ; 
       ss303_ordinance-t2d65.5-0 ; 
       ss303_ordinance-t2d66.4-0 ; 
       ss303_ordinance-t2d67.3-0 ; 
       ss303_ordinance-t2d68.8-0 ; 
       ss303_ordinance-t2d69.7-0 ; 
       ss303_ordinance-t2d70.6-0 ; 
       ss303_ordinance-t2d71.6-0 ; 
       ss303_ordinance-t2d72.5-0 ; 
       ss303_ordinance-t2d73.5-0 ; 
       ss303_ordinance-t2d74.5-0 ; 
       ss303_ordinance-t2d75.4-0 ; 
       ss303_ordinance-t2d76.7-0 ; 
       ss303_ordinance-t2d77.5-0 ; 
       ss303_ordinance-t2d78.3-0 ; 
       ss303_ordinance-t2d79.3-0 ; 
       ss303_ordinance-t2d80.3-0 ; 
       ss303_ordinance-t2d81.3-0 ; 
       ss303_ordinance-t2d82.2-0 ; 
       ss303_ordinance-t2d83.2-0 ; 
       ss305_electronic-t2d58.6-0 ; 
       ss305_electronic-t2d59.4-0 ; 
       ss305_electronic-t2d60.4-0 ; 
       ss305_electronic-t2d61.4-0 ; 
       ss305_electronic-t2d62.4-0 ; 
       ss305_electronic-t2d63.4-0 ; 
       ss305_electronic-t2d64.4-0 ; 
       ss305_elect_station-t2d52.6-0 ; 
       ss305_elect_station-t2d53.6-0 ; 
       ss305_elect_station-t2d54.6-0 ; 
       ss305_elect_station-t2d55.6-0 ; 
       ss305_elect_station-t2d56.6-0 ; 
       ss305_elect_station-t2d57.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       1 10 110 ; 
       2 7 110 ; 
       3 10 110 ; 
       4 7 110 ; 
       5 0 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 9 110 ; 
       12 0 110 ; 
       13 8 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 3 300 ; 
       2 9 300 ; 
       3 4 300 ; 
       4 10 300 ; 
       5 1 300 ; 
       6 11 300 ; 
       6 18 300 ; 
       7 8 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       8 7 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       9 5 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 2 300 ; 
       10 19 300 ; 
       11 30 300 ; 
       11 31 300 ; 
       11 32 300 ; 
       12 21 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       13 27 300 ; 
       13 28 300 ; 
       13 29 300 ; 
       14 24 300 ; 
       14 25 300 ; 
       14 26 300 ; 
       15 20 300 ; 
       16 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 18 401 ; 
       2 12 401 ; 
       3 17 401 ; 
       4 16 401 ; 
       5 1 401 ; 
       6 19 401 ; 
       7 7 401 ; 
       8 4 401 ; 
       9 14 401 ; 
       10 15 401 ; 
       11 10 401 ; 
       12 2 401 ; 
       13 3 401 ; 
       14 5 401 ; 
       15 6 401 ; 
       16 8 401 ; 
       17 9 401 ; 
       18 11 401 ; 
       19 13 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -8 0 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 MPRFLG 0 ; 
       7 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 17.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 12 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 7 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 4 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 10.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 12 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 5.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 7 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
