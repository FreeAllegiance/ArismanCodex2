SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.6-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       ss303_ordinance-mat4.1-0 ; 
       ss303_ordinance-mat6.1-0 ; 
       ss303_ordinance-mat7.1-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_electronic-mat59.1-0 ; 
       ss305_electronic-mat60.1-0 ; 
       ss305_electronic-mat61.1-0 ; 
       ss305_electronic-mat62.1-0 ; 
       ss305_electronic-mat63.1-0 ; 
       ss305_electronic-mat64.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 64     
       2-cube1.1-0 ; 
       2-cube2.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ROOT ; 
       2-null18.1-0 ROOT ; 
       2-null19.1-0 ROOT ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       asteroid_chain4-craterer.2-0 ROOT ; 
       asteroid_chain5-craterer.2-0 ROOT ; 
       purpmeteor-east_bay_11_8.1-0 ROOT ; 
       purpmeteor-east_bay_11_8_1.1-0 ; 
       purpmeteor-extru44.1-0 ROOT ; 
       purpmeteor-extru47.1-0 ROOT ; 
       purpmeteor-garage1A.1-0 ; 
       purpmeteor-garage1B.1-0 ; 
       purpmeteor-garage1C.1-0 ; 
       purpmeteor-garage1D.1-0 ; 
       purpmeteor-garage1E.1-0 ; 
       purpmeteor-SS_11_1.1-0 ; 
       purpmeteor-SS_13_2.1-0 ; 
       purpmeteor-SS_15_1.1-0 ; 
       purpmeteor-SS_23_2.1-0 ; 
       purpmeteor-SS_24_1.1-0 ; 
       purpmeteor-SS_26.1-0 ; 
       purpmeteor-turwepemt2.1-0 ; 
       ss303_ordinance-craterer1.5-0 ROOT ; 
       ss303_ordinance-cube1.1-0 ; 
       ss303_ordinance-cube12.2-0 ; 
       ss303_ordinance-cube2.1-0 ; 
       ss303_ordinance-cube22.2-0 ; 
       ss303_ordinance-cube3.1-0 ; 
       ss303_ordinance-cube30.1-0 ROOT ; 
       ss303_ordinance-cube31.2-0 ROOT ; 
       ss303_ordinance-cube32.1-0 ROOT ; 
       ss303_ordinance-cube33.1-0 ; 
       ss303_ordinance-cube34.1-0 ; 
       ss303_ordinance-cube35.1-0 ; 
       ss303_ordinance-cube4.1-0 ; 
       ss303_ordinance-cyl1.1-0 ; 
       ss303_ordinance-east_bay_11_9.1-0 ; 
       ss303_ordinance-east_bay_11_9_1.1-0 ROOT ; 
       ss303_ordinance-extru48.1-0 ; 
       ss303_ordinance-extru50.1-0 ; 
       ss303_ordinance-launch1.1-0 ; 
       ss303_ordinance-sphere1.3-0 ROOT ; 
       ss303_ordinance-SS_11.1-0 ; 
       ss303_ordinance-SS_13_3.1-0 ; 
       ss303_ordinance-SS_15_3.1-0 ; 
       ss303_ordinance-SS_23.1-0 ; 
       ss303_ordinance-SS_24.1-0 ; 
       ss303_ordinance-SS_26_3.1-0 ; 
       ss303_ordinance-tetra1.1-0 ; 
       ss303_ordinance-turwepemt2_3.1-0 ; 
       ss305_electronic-cube24.1-0 ROOT ; 
       ss305_electronic-cube29.1-0 ROOT ; 
       ss305_electronic-cube5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss303-ordinance.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       ss303_ordinance-rendermap2.1-0 ; 
       ss303_ordinance-rendermap4.2-0 ; 
       ss303_ordinance-rendermap5.1-0 ; 
       ss305_electronic-t2d58.1-0 ; 
       ss305_electronic-t2d59.1-0 ; 
       ss305_electronic-t2d60.1-0 ; 
       ss305_electronic-t2d61.1-0 ; 
       ss305_electronic-t2d62.1-0 ; 
       ss305_electronic-t2d63.1-0 ; 
       ss305_electronic-t2d64.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       34 46 110 ; 
       36 46 110 ; 
       38 33 110 ; 
       45 33 110 ; 
       46 33 110 ; 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       42 44 110 ; 
       35 43 110 ; 
       43 44 110 ; 
       44 33 110 ; 
       37 43 110 ; 
       47 44 110 ; 
       49 33 110 ; 
       50 45 110 ; 
       51 48 110 ; 
       53 48 110 ; 
       54 48 110 ; 
       55 48 110 ; 
       56 48 110 ; 
       57 48 110 ; 
       58 48 110 ; 
       59 46 110 ; 
       60 48 110 ; 
       18 17 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 18 110 ; 
       27 18 110 ; 
       28 18 110 ; 
       29 18 110 ; 
       30 18 110 ; 
       31 18 110 ; 
       32 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       15 2 300 ; 
       16 0 300 ; 
       33 1 300 ; 
       7 20 300 ; 
       8 20 300 ; 
       9 20 300 ; 
       10 20 300 ; 
       11 19 300 ; 
       12 19 300 ; 
       13 19 300 ; 
       14 19 300 ; 
       17 13 300 ; 
       17 14 300 ; 
       17 15 300 ; 
       47 10 300 ; 
       47 11 300 ; 
       47 12 300 ; 
       48 7 300 ; 
       48 8 300 ; 
       48 9 300 ; 
       19 16 300 ; 
       49 3 300 ; 
       53 17 300 ; 
       54 17 300 ; 
       55 17 300 ; 
       56 17 300 ; 
       57 17 300 ; 
       58 17 300 ; 
       18 4 300 ; 
       18 5 300 ; 
       18 6 300 ; 
       26 18 300 ; 
       27 18 300 ; 
       28 18 300 ; 
       29 18 300 ; 
       30 18 300 ; 
       31 18 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       15 SCHEM 8.925957 4.423571 0 USR DISPLAY 0 0 SRT 1.6 1.6 1.6 0 0 1.570796 0 0 0 MPRFLG 0 ; 
       16 SCHEM 8.805557 3.061208 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 1.570796 0 0 0 MPRFLG 0 ; 
       33 SCHEM 60.35877 -22.15185 0 USR DISPLAY 3 2 SRT 6.063473 6.063473 6.063473 0 0 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 75.35877 -26.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 72.85877 -26.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 77.85876 -24.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 80.35876 -24.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 72.85877 -24.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 11.58894 6.828987 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 12.49999 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 12.49999 -4 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 7.807066e-009 2.59454 -2.653347e-008 151.1425 -14.19661 75.7656 MPRFLG 0 ; 
       5 SCHEM 77.6628 -11.9962 0 USR SRT 1 1 1 0 1.570796 0 104.3961 -4.675931 9.374343 MPRFLG 0 ; 
       39 SCHEM 19.42121 14.93497 0 USR DISPLAY 0 0 SRT 3.094894 1.002122 1.770534 0 0 0 1.105845 0 39.64642 MPRFLG 0 ; 
       6 SCHEM 61.28183 -10.72001 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 104.473 3.878042 8.341749 MPRFLG 0 ; 
       7 SCHEM 73.9128 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       8 SCHEM 76.4128 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 11.39636 13.75116 0 USR DISPLAY 0 0 SRT 3.094894 1.002122 1.770534 0 -1.570796 0 -36.63318 -9.151316 -8.571338 MPRFLG 0 ; 
       9 SCHEM 78.9128 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 24.42121 14.93497 0 USR DISPLAY 0 0 SRT 3.094894 1.002122 1.770534 1.570796 8.742277e-008 -1.570796 -38.67118 56.26406 4.623014 MPRFLG 0 ; 
       10 SCHEM 81.41281 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       11 SCHEM 65.03183 -12.72001 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 62.53183 -12.72001 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 60.03184 -12.72001 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 57.53184 -12.72001 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 62.85877 -26.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 65.35877 -28.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 66.60877 -26.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 55.35877 -24.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 67.85877 -28.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 103.6412 -17.28968 0 USR SRT 0.9999999 0.9999999 1 1.125474e-007 4.132642e-007 2.500179e-008 1.141766 0.9347438 20.57174 MPRFLG 0 ; 
       47 SCHEM 51.60877 -26.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 51.60877 -28.15185 0 SRT 0.9999999 0.9999999 1 7.877882e-007 6.283185 -1.15339e-007 -21.40607 -8.282479 -8.820705 MPRFLG 0 ; 
       19 SCHEM 35.08126 9.803335 0 DISPLAY 0 0 SRT 0.5420001 0.5420001 0.5420003 -7.940933e-008 1.484001 -7.490997e-008 57.94852 -10.83005 -45.92395 MPRFLG 0 ; 
       20 SCHEM 8.773831 -5.305431 0 USR DISPLAY 0 0 SRT 12.57675 0.9520104 1.531824 -3.423803e-008 1.736862e-009 5.122056e-008 86.67359 -0.7831174 -30.36357 MPRFLG 0 ; 
       49 SCHEM 40.35877 -24.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 80.35876 -26.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 60.35877 -30.15185 0 WIRECOL 9 7 MPRFLG 0 ; 
       53 SCHEM 47.85877 -30.15185 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 42.85877 -30.15185 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 45.35877 -30.15185 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 55.35877 -30.15185 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 50.35877 -30.15185 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 52.85877 -30.15185 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 70.35877 -26.15185 0 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 57.85877 -30.15185 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 105.2205 -28.14689 0 USR MPRFLG 0 ; 
       21 SCHEM 103.8268 -30.23399 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 108.8268 -30.23399 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 106.3268 -30.23399 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 113.8268 -30.23399 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 111.3268 -30.23399 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 93.82684 -30.23399 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 88.82684 -30.23399 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 91.32684 -30.23399 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 101.3268 -30.23399 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 96.32684 -30.23399 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 98.82684 -30.23399 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 116.3268 -30.23399 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 5 0 0 DISPLAY 0 0 SRT 1.711165 0.5905364 1.162 0 1.570796 0 -48.73319 11.45754 -1.367697 MPRFLG 0 ; 
       62 SCHEM 1.591737 14.93497 0 USR DISPLAY 0 0 SRT 0.758 0.4441879 0.758 0 0 0 -30.96563 7.131888 -8.462653 MPRFLG 0 ; 
       63 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1.316 1.316 1.316 -1.078445e-008 2.381581e-008 -4.766174e-008 27.36609 -20.99654 -35.87016 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 102.4128 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 108.4128 3.929574 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 108.4128 1.929574 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 142.5238 -41.39674 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 161.7059 -8.188705 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 161.7059 -8.188705 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 161.7059 -8.188705 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -10.10461 -18.91828 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -9.059784 -18.64572 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -13.73877 -17.60089 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM -38.85463 -26.91828 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -37.8098 -26.64572 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -42.48878 -25.60089 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 59.3381 -37.39674 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -51.71501 -31.64511 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM -66.07942 -25.59969 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 147.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 150.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 102.4128 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 142.5238 -43.39674 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 161.7059 -10.18871 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 161.7059 -10.18871 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 161.7059 -10.18871 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -10.10461 -20.91828 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -9.059784 -20.64572 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -13.73877 -19.60089 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -38.85463 -28.91828 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM -37.8098 -28.64572 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM -42.48878 -27.60089 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 59.3381 -39.39674 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 110.9128 3.929574 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 108.4128 -0.07042599 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
