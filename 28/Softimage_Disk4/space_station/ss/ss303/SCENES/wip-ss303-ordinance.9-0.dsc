SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.9-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       ss303_ordinance-mat4.1-0 ; 
       ss303_ordinance-mat6.1-0 ; 
       ss303_ordinance-mat7.1-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_electronic-mat59.1-0 ; 
       ss305_electronic-mat60.1-0 ; 
       ss305_electronic-mat61.1-0 ; 
       ss305_electronic-mat62.1-0 ; 
       ss305_electronic-mat63.1-0 ; 
       ss305_electronic-mat64.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 74     
       2-cube1.1-0 ; 
       2-cube2.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.2-0 ROOT ; 
       2-null18.2-0 ROOT ; 
       2-null19.2-0 ROOT ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       asteroid_chain4-craterer.3-0 ROOT ; 
       asteroid_chain5-craterer.3-0 ROOT ; 
       purpmeteor-east_bay_11_8_1.2-0 ROOT ; 
       purpmeteor-extru44.2-0 ROOT ; 
       purpmeteor-extru47.2-0 ROOT ; 
       purpmeteor-garage1A.1-0 ; 
       purpmeteor-garage1B.1-0 ; 
       purpmeteor-garage1C.1-0 ; 
       purpmeteor-garage1D.1-0 ; 
       purpmeteor-garage1E.1-0 ; 
       purpmeteor-SS_11_1.1-0 ; 
       purpmeteor-SS_13_2.1-0 ; 
       purpmeteor-SS_15_1.1-0 ; 
       purpmeteor-SS_23_2.1-0 ; 
       purpmeteor-SS_24_1.1-0 ; 
       purpmeteor-SS_26.1-0 ; 
       purpmeteor-turwepemt2.1-0 ; 
       ss303_ordinance-craterer1.8-0 ROOT ; 
       ss303_ordinance-cube1.1-0 ; 
       ss303_ordinance-cube12.2-0 ; 
       ss303_ordinance-cube2.1-0 ; 
       ss303_ordinance-cube22.2-0 ; 
       ss303_ordinance-cube3.1-0 ; 
       ss303_ordinance-cube30.2-0 ROOT ; 
       ss303_ordinance-cube31.3-0 ROOT ; 
       ss303_ordinance-cube32.2-0 ROOT ; 
       ss303_ordinance-cube33.1-0 ; 
       ss303_ordinance-cube34.1-0 ; 
       ss303_ordinance-cube35.1-0 ; 
       ss303_ordinance-cube36.1-0 ; 
       ss303_ordinance-cube37.1-0 ; 
       ss303_ordinance-cube38.1-0 ; 
       ss303_ordinance-cube39.1-0 ; 
       ss303_ordinance-cube4.1-0 ; 
       ss303_ordinance-cube40.1-0 ; 
       ss303_ordinance-cube41.1-0 ; 
       ss303_ordinance-cube42.1-0 ; 
       ss303_ordinance-cube43.1-0 ; 
       ss303_ordinance-cyl1.1-0 ; 
       ss303_ordinance-east_bay_11_8.1-0 ; 
       ss303_ordinance-east_bay_11_9.1-0 ; 
       ss303_ordinance-east_bay_11_9_1.3-0 ROOT ; 
       ss303_ordinance-extru48.1-0 ; 
       ss303_ordinance-extru50.1-0 ; 
       ss303_ordinance-launch1.1-0 ; 
       ss303_ordinance-null20.1-0 ROOT ; 
       ss303_ordinance-sphere1.4-0 ROOT ; 
       ss303_ordinance-sphere2.1-0 ; 
       ss303_ordinance-SS_11.1-0 ; 
       ss303_ordinance-SS_13_3.1-0 ; 
       ss303_ordinance-SS_15_3.1-0 ; 
       ss303_ordinance-SS_23.1-0 ; 
       ss303_ordinance-SS_24.1-0 ; 
       ss303_ordinance-SS_26_3.1-0 ; 
       ss303_ordinance-tetra1.1-0 ; 
       ss303_ordinance-turwepemt2_3.1-0 ; 
       ss305_electronic-cube24.2-0 ROOT ; 
       ss305_electronic-cube29.2-0 ROOT ; 
       ss305_electronic-cube5.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss303/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss303-ordinance.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       ss303_ordinance-rendermap2.1-0 ; 
       ss303_ordinance-rendermap4.2-0 ; 
       ss303_ordinance-rendermap5.1-0 ; 
       ss305_electronic-t2d58.1-0 ; 
       ss305_electronic-t2d59.1-0 ; 
       ss305_electronic-t2d60.1-0 ; 
       ss305_electronic-t2d61.1-0 ; 
       ss305_electronic-t2d62.1-0 ; 
       ss305_electronic-t2d63.1-0 ; 
       ss305_electronic-t2d64.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       44 60 110 ; 
       45 60 110 ; 
       46 60 110 ; 
       47 60 110 ; 
       49 60 110 ; 
       50 60 110 ; 
       51 60 110 ; 
       52 60 110 ; 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       20 17 110 ; 
       21 17 110 ; 
       22 17 110 ; 
       23 17 110 ; 
       24 17 110 ; 
       25 17 110 ; 
       26 17 110 ; 
       27 17 110 ; 
       28 17 110 ; 
       29 17 110 ; 
       30 17 110 ; 
       31 17 110 ; 
       33 53 110 ; 
       34 42 110 ; 
       35 53 110 ; 
       36 42 110 ; 
       37 32 110 ; 
       41 43 110 ; 
       42 43 110 ; 
       43 32 110 ; 
       48 32 110 ; 
       53 32 110 ; 
       54 48 110 ; 
       55 43 110 ; 
       57 32 110 ; 
       58 48 110 ; 
       59 56 110 ; 
       63 56 110 ; 
       64 56 110 ; 
       65 56 110 ; 
       66 56 110 ; 
       67 56 110 ; 
       68 56 110 ; 
       69 53 110 ; 
       70 56 110 ; 
       62 60 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 20 300 ; 
       8 20 300 ; 
       9 20 300 ; 
       10 20 300 ; 
       11 19 300 ; 
       12 19 300 ; 
       13 19 300 ; 
       14 19 300 ; 
       15 2 300 ; 
       16 0 300 ; 
       17 4 300 ; 
       17 5 300 ; 
       17 6 300 ; 
       18 16 300 ; 
       25 18 300 ; 
       26 18 300 ; 
       27 18 300 ; 
       28 18 300 ; 
       29 18 300 ; 
       30 18 300 ; 
       32 1 300 ; 
       54 13 300 ; 
       54 14 300 ; 
       54 15 300 ; 
       55 10 300 ; 
       55 11 300 ; 
       55 12 300 ; 
       56 7 300 ; 
       56 8 300 ; 
       56 9 300 ; 
       57 3 300 ; 
       63 17 300 ; 
       64 17 300 ; 
       65 17 300 ; 
       66 17 300 ; 
       67 17 300 ; 
       68 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 9.826515 -19.93379 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 9.826515 -21.93379 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       44 SCHEM 40.50221 43.10342 0 MPRFLG 0 ; 
       45 SCHEM 43.00221 43.10342 0 DISPLAY 1 2 MPRFLG 0 ; 
       46 SCHEM 45.50221 43.10342 0 MPRFLG 0 ; 
       47 SCHEM 48.00221 43.10342 0 MPRFLG 0 ; 
       49 SCHEM 50.50221 43.10342 0 MPRFLG 0 ; 
       50 SCHEM 53.00221 43.10342 0 MPRFLG 0 ; 
       60 SCHEM 48.00221 45.10342 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       51 SCHEM 55.50221 43.10342 0 MPRFLG 0 ; 
       52 SCHEM 58.00221 43.10342 0 MPRFLG 0 ; 
       0 SCHEM 17.32652 -21.93379 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 19.82652 -21.93379 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 22.32651 -21.93379 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 22.32651 -23.93379 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 19.82652 -19.93379 0 USR DISPLAY 0 0 SRT 1 1 1 7.807066e-009 2.59454 -2.653347e-008 151.1425 -14.19661 75.7656 MPRFLG 0 ; 
       5 SCHEM 63.62046 32.63363 0 USR SRT 1 1 1 0 1.570796 0 104.3961 -4.675931 9.374343 MPRFLG 0 ; 
       6 SCHEM 63.08107 36.25274 0 USR SRT 1 1 1 0 0 0 104.473 3.878042 8.341749 MPRFLG 0 ; 
       7 SCHEM 59.87046 30.63363 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       8 SCHEM 62.37046 30.63363 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       9 SCHEM 64.87046 30.63363 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       10 SCHEM 67.37046 30.63363 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       11 SCHEM 66.83107 34.25274 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 64.33107 34.25274 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 61.83108 34.25274 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 59.33108 34.25274 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 18.75248 -15.51022 0 USR DISPLAY 0 0 SRT 1.6 1.6 1.6 0 0 1.570796 0 0 0 MPRFLG 0 ; 
       16 SCHEM 18.63208 -16.87259 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 1.570796 0 0 0 MPRFLG 0 ; 
       17 SCHEM 39.13754 29.83522 0 USR SRT 0.9999999 0.9999999 1 1.125474e-007 6.283186 2.500181e-008 1.141764 0.9347438 20.57174 MPRFLG 0 ; 
       18 SCHEM 44.90777 -10.13046 0 USR DISPLAY 0 0 SRT 0.5420001 0.5420001 0.5420003 -7.940933e-008 1.484001 -7.490997e-008 57.94852 -10.83005 -45.92395 MPRFLG 0 ; 
       19 SCHEM 18.60035 -25.23923 0 USR DISPLAY 0 0 SRT 12.57675 0.9520104 1.531824 -3.423803e-008 1.736862e-009 5.122056e-008 86.67359 -0.7831174 -30.36357 MPRFLG 0 ; 
       20 SCHEM 40.38754 27.83522 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 45.38754 27.83522 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 42.88754 27.83522 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 50.38754 27.83522 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 47.88754 27.83522 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 30.38754 27.83522 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 25.38754 27.83522 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 27.88754 27.83522 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 37.88754 27.83522 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 32.88754 27.83522 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 35.38754 27.83522 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 52.88754 27.83522 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 17.67003 42.26075 0 USR DISPLAY 0 0 SRT 6.063473 6.063473 6.063473 0 0 0 0 0 0 MPRFLG 0 ; 
       33 SCHEM 22.67003 38.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 12.67003 36.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 20.17003 38.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 15.17003 36.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 25.17003 40.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 29.24773 -4.998822 0 USR DISPLAY 0 0 SRT 3.094894 1.002122 1.770534 0 0 0 1.105845 0 39.64642 MPRFLG 0 ; 
       39 SCHEM 21.22288 -6.18263 0 USR DISPLAY 0 0 SRT 3.094894 1.002122 1.770534 0 -1.570796 0 -36.63318 -9.151316 -8.571338 MPRFLG 0 ; 
       40 SCHEM 34.24773 -4.998822 0 USR DISPLAY 0 0 SRT 3.094894 1.002122 1.770534 1.570796 8.742277e-008 -1.570796 -38.67118 56.26406 4.623014 MPRFLG 0 ; 
       41 SCHEM 10.17003 38.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 13.92003 38.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 11.42003 40.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 28.92003 40.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 20.17003 40.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 30.17003 38.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 7.670031 38.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 7.750688 32.05114 0 USR SRT 0.9999999 0.9999999 1 7.877882e-007 6.283185 -1.15339e-007 -21.40607 -8.282479 -8.820705 MPRFLG 0 ; 
       57 SCHEM 5.170031 40.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 27.67003 38.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 16.50069 30.05114 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       61 SCHEM 21.41546 -13.1048 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       63 SCHEM 4.000688 30.05114 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM -0.9993126 30.05114 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 1.500688 30.05114 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 11.50069 30.05114 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 6.500688 30.05114 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 9.000686 30.05114 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 17.67003 38.26075 0 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 14.00069 30.05114 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 14.82652 -19.93379 0 USR DISPLAY 0 0 SRT 1.711165 0.5905364 1.162 0 1.570796 0 -48.73319 11.45754 -1.367697 MPRFLG 0 ; 
       72 SCHEM 11.41825 -4.998822 0 USR DISPLAY 0 0 SRT 0.758 0.4441879 0.758 0 0 0 -30.96563 7.131888 -8.462653 MPRFLG 0 ; 
       73 SCHEM 12.32652 -19.93379 0 USR DISPLAY 0 0 SRT 1.316 1.316 1.316 -1.078445e-008 2.381581e-008 -4.766174e-008 27.36609 -20.99654 -35.87016 MPRFLG 0 ; 
       62 SCHEM 38.00221 43.10342 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 102.4128 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 108.4128 3.929574 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 108.4128 1.929574 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 142.5238 -41.39674 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 161.7059 -8.188705 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 161.7059 -8.188705 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 161.7059 -8.188705 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -10.10461 -18.91828 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -9.059784 -18.64572 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -13.73877 -17.60089 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM -38.85463 -26.91828 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -37.8098 -26.64572 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -42.48878 -25.60089 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 59.3381 -37.39674 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -51.71501 -31.64511 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM -66.07942 -25.59969 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 147.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 150.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 102.4128 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 110.9128 3.929574 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 108.4128 -0.07042599 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 142.5238 -43.39674 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 161.7059 -10.18871 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 161.7059 -10.18871 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 161.7059 -10.18871 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -10.10461 -20.91828 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -9.059784 -20.64572 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -13.73877 -19.60089 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -38.85463 -28.91828 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM -37.8098 -28.64572 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM -42.48878 -27.60089 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 59.3381 -39.39674 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
