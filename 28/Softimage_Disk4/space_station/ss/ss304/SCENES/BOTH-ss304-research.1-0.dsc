SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.4-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-mat66.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 82     
       bounding_model-bounding_model.1-0 ROOT ; 
       bounding_model-cube23.1-0 ; 
       bounding_model-cube24.1-0 ; 
       bounding_model-cube25.1-0 ; 
       bounding_model-cube26.1-0 ; 
       bounding_model-cube27.1-0 ; 
       bounding_model-cube28.1-0 ; 
       bounding_model-cube29.1-0 ; 
       bounding_model-cube30.1-0 ; 
       bounding_model-cube31.1-0 ; 
       bounding_model-cube32.1-0 ; 
       bounding_model-cube33.1-0 ; 
       bounding_model-sphere1.1-0 ; 
       root-cube1.1-0 ; 
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube4_1.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl2.1-0 ; 
       root-cyl2_1.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-extru44.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.2-0 ; 
       root-null18_1.2-0 ; 
       root-null19.2-0 ; 
       root-null20.2-0 ; 
       root-null21.1-0 ; 
       root-null22.1-0 ; 
       root-rock.2-0 ; 
       root-root.3-0 ROOT ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-ss304-research.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ss304_research-rendermap2.2-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       12 0 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       13 29 110 ; 
       14 17 110 ; 
       15 17 110 ; 
       16 14 110 ; 
       17 44 110 ; 
       18 44 110 ; 
       19 29 110 ; 
       20 18 110 ; 
       21 17 110 ; 
       22 45 110 ; 
       23 45 110 ; 
       24 43 110 ; 
       25 43 110 ; 
       26 22 110 ; 
       27 26 110 ; 
       28 24 110 ; 
       29 25 110 ; 
       30 45 110 ; 
       31 45 110 ; 
       32 17 110 ; 
       33 30 110 ; 
       34 30 110 ; 
       35 30 110 ; 
       36 30 110 ; 
       37 30 110 ; 
       38 31 110 ; 
       39 45 110 ; 
       40 45 110 ; 
       41 45 110 ; 
       42 44 110 ; 
       43 20 110 ; 
       44 45 110 ; 
       45 46 110 ; 
       47 31 110 ; 
       48 30 110 ; 
       49 30 110 ; 
       50 31 110 ; 
       51 30 110 ; 
       52 31 110 ; 
       53 31 110 ; 
       54 30 110 ; 
       55 31 110 ; 
       56 30 110 ; 
       57 30 110 ; 
       58 31 110 ; 
       59 39 110 ; 
       60 39 110 ; 
       61 39 110 ; 
       62 39 110 ; 
       63 41 110 ; 
       64 41 110 ; 
       65 41 110 ; 
       66 41 110 ; 
       67 40 110 ; 
       68 40 110 ; 
       69 41 110 ; 
       70 39 110 ; 
       71 39 110 ; 
       72 39 110 ; 
       73 39 110 ; 
       74 39 110 ; 
       75 39 110 ; 
       76 39 110 ; 
       77 39 110 ; 
       78 39 110 ; 
       79 39 110 ; 
       80 30 110 ; 
       81 31 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       30 17 300 ; 
       30 18 300 ; 
       30 19 300 ; 
       31 14 300 ; 
       31 15 300 ; 
       31 16 300 ; 
       32 20 300 ; 
       45 2 300 ; 
       47 21 300 ; 
       48 22 300 ; 
       49 22 300 ; 
       50 21 300 ; 
       51 22 300 ; 
       52 21 300 ; 
       53 21 300 ; 
       54 22 300 ; 
       55 21 300 ; 
       56 22 300 ; 
       57 22 300 ; 
       58 21 300 ; 
       59 24 300 ; 
       60 24 300 ; 
       61 24 300 ; 
       62 24 300 ; 
       63 23 300 ; 
       64 23 300 ; 
       65 23 300 ; 
       66 23 300 ; 
       67 1 300 ; 
       68 0 300 ; 
       69 3 300 ; 
       70 4 300 ; 
       71 5 300 ; 
       72 6 300 ; 
       73 7 300 ; 
       74 8 300 ; 
       75 9 300 ; 
       76 10 300 ; 
       77 11 300 ; 
       78 12 300 ; 
       79 13 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       14 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       18 5 401 ; 
       19 6 401 ; 
       20 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       12 SCHEM 17.29428 -4.323016 0 MPRFLG 0 ; 
       1 SCHEM 19.79428 -4.323016 0 MPRFLG 0 ; 
       2 SCHEM 22.29429 -4.323016 0 MPRFLG 0 ; 
       3 SCHEM 24.79429 -4.323016 0 MPRFLG 0 ; 
       13 SCHEM 31.53453 -28.83238 0 MPRFLG 0 ; 
       14 SCHEM 19.03453 -20.83238 0 MPRFLG 0 ; 
       15 SCHEM 26.53453 -20.83238 0 MPRFLG 0 ; 
       16 SCHEM 19.03453 -22.83238 0 MPRFLG 0 ; 
       17 SCHEM 22.78453 -18.83238 0 MPRFLG 0 ; 
       18 SCHEM 31.53453 -18.83238 0 MPRFLG 0 ; 
       19 SCHEM 34.03453 -28.83238 0 MPRFLG 0 ; 
       20 SCHEM 31.53453 -20.83238 0 MPRFLG 0 ; 
       21 SCHEM 24.03453 -20.83238 0 MPRFLG 0 ; 
       22 SCHEM 36.53454 -16.83238 0 MPRFLG 0 ; 
       23 SCHEM 39.03454 -16.83238 0 MPRFLG 0 ; 
       24 SCHEM 29.03453 -24.83238 0 MPRFLG 0 ; 
       25 SCHEM 32.78453 -24.83238 0 MPRFLG 0 ; 
       26 SCHEM 36.53454 -18.83238 0 MPRFLG 0 ; 
       27 SCHEM 36.53454 -20.83238 0 MPRFLG 0 ; 
       28 SCHEM 29.03453 -26.83238 0 MPRFLG 0 ; 
       29 SCHEM 32.78453 -26.83238 0 MPRFLG 0 ; 
       30 SCHEM 0.2845308 -16.83238 0 MPRFLG 0 ; 
       31 SCHEM -24.71547 -16.83238 0 MPRFLG 0 ; 
       32 SCHEM 21.53453 -20.83238 0 MPRFLG 0 ; 
       33 SCHEM 1.534531 -18.83238 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 6.534531 -18.83238 0 WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 4.034531 -18.83238 0 WIRECOL 9 7 MPRFLG 0 ; 
       36 SCHEM 11.53453 -18.83238 0 WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM 9.034531 -18.83238 0 WIRECOL 9 7 MPRFLG 0 ; 
       38 SCHEM -15.96547 -18.83238 0 WIRECOL 9 7 MPRFLG 0 ; 
       39 SCHEM 57.78454 -16.83238 0 MPRFLG 0 ; 
       40 SCHEM 77.78452 -16.83238 0 MPRFLG 0 ; 
       41 SCHEM 86.53452 -16.83238 0 MPRFLG 0 ; 
       42 SCHEM 16.53453 -18.83238 0 MPRFLG 0 ; 
       43 SCHEM 31.53453 -22.83238 0 MPRFLG 0 ; 
       44 SCHEM 25.28453 -16.83238 0 MPRFLG 0 ; 
       45 SCHEM 29.03453 -14.83238 0 MPRFLG 0 ; 
       46 SCHEM 29.03453 -12.83238 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       47 SCHEM -28.46546 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM -8.465468 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM -13.46547 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM -33.46546 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM -10.96547 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM -30.96546 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM -20.96547 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM -0.9654693 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM -25.96547 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM -5.965469 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM -3.46547 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM -23.46547 -18.83238 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 41.53454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 44.03454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 46.53454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 51.53454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 89.03452 -18.83238 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 86.53452 -18.83238 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 84.03452 -18.83238 0 WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 81.53452 -18.83238 0 WIRECOL 4 7 MPRFLG 0 ; 
       67 SCHEM 76.53452 -18.83238 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 79.03452 -18.83238 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 91.53452 -18.83238 0 WIRECOL 4 7 MPRFLG 0 ; 
       70 SCHEM 49.03454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       71 SCHEM 54.03454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       72 SCHEM 61.53454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       73 SCHEM 69.03453 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       74 SCHEM 56.53454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       75 SCHEM 59.03454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       76 SCHEM 64.03454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       77 SCHEM 66.53454 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       78 SCHEM 71.53452 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       79 SCHEM 74.03452 -18.83238 0 WIRECOL 2 7 MPRFLG 0 ; 
       80 SCHEM 14.03453 -18.83238 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM -18.46547 -18.83238 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 31.04428 -2.323016 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 27.29429 -4.323016 0 MPRFLG 0 ; 
       5 SCHEM 29.79429 -4.323016 0 MPRFLG 0 ; 
       6 SCHEM 32.29428 -4.323016 0 MPRFLG 0 ; 
       7 SCHEM 34.79428 -4.323016 0 MPRFLG 0 ; 
       8 SCHEM 37.29428 -4.323016 0 MPRFLG 0 ; 
       9 SCHEM 42.29428 -4.323016 0 MPRFLG 0 ; 
       10 SCHEM 39.79428 -4.323016 0 MPRFLG 0 ; 
       11 SCHEM 44.79428 -4.323016 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.47525 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 74.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 79.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 84.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 89.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 105.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 110.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 115.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 120.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 97.46411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 104.9641 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 58.17583 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 59.22066 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 54.54168 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 175.8722 -3.799718 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.56545 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM -5.297054 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 58.17583 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59.22066 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54.54168 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 175.8722 -5.799718 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
