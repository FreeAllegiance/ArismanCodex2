SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.80-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       ss304_research-mat102.1-0 ; 
       ss304_research-mat103.1-0 ; 
       ss304_research-mat104.1-0 ; 
       ss304_research-mat105.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bound07.1-0 ; 
       bounding_model-bound08.1-0 ; 
       bounding_model-bound09.1-0 ; 
       bounding_model-bound10.1-0 ; 
       bounding_model-bound11.1-0 ; 
       bounding_model-bound12.1-0 ; 
       bounding_model-bound13.1-0 ; 
       bounding_model-bound14.1-0 ; 
       bounding_model-bound15.1-0 ; 
       bounding_model-bounding_model.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/ss304 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bound-ss304-research.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       ss304_research-t2d91.1-0 ; 
       ss304_research-t2d92.1-0 ; 
       ss304_research-t2d93.1-0 ; 
       ss304_research-t2d94.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 15 110 ; 
       14 15 110 ; 
       0 15 110 ; 
       1 15 110 ; 
       2 15 110 ; 
       3 15 110 ; 
       4 15 110 ; 
       5 15 110 ; 
       6 15 110 ; 
       7 15 110 ; 
       8 15 110 ; 
       9 15 110 ; 
       10 15 110 ; 
       11 15 110 ; 
       12 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 0 300 ; 
       13 1 300 ; 
       14 2 300 ; 
       14 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 2 401 ; 
       3 3 401 ; 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 35 -6 0 MPRFLG 0 ; 
       0 SCHEM 0 -6 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 25 -6 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 47.76767 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.76767 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 42.76767 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.76767 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 47.76767 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 47.76767 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 42.76767 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.76767 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
