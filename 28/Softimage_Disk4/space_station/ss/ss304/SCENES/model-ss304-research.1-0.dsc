SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.3-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-mat66.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 69     
       root-cube1.1-0 ; 
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube4_1.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl2.1-0 ; 
       root-cyl2_1.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-extru44.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.2-0 ; 
       root-null18_1.2-0 ; 
       root-null19.2-0 ; 
       root-null20.2-0 ; 
       root-null21.1-0 ; 
       root-null22.1-0 ; 
       root-rock.2-0 ; 
       root-root.2-0 ROOT ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss304-research.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ss304_research-rendermap2.2-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 4 110 ; 
       56 28 110 ; 
       57 26 110 ; 
       2 4 110 ; 
       58 26 110 ; 
       3 1 110 ; 
       59 26 110 ; 
       6 16 110 ; 
       10 32 110 ; 
       60 26 110 ; 
       61 26 110 ; 
       13 9 110 ; 
       14 13 110 ; 
       62 26 110 ; 
       19 4 110 ; 
       26 32 110 ; 
       27 32 110 ; 
       28 32 110 ; 
       63 26 110 ; 
       46 26 110 ; 
       47 26 110 ; 
       54 27 110 ; 
       48 26 110 ; 
       55 27 110 ; 
       49 26 110 ; 
       50 28 110 ; 
       51 28 110 ; 
       52 28 110 ; 
       53 28 110 ; 
       4 31 110 ; 
       5 31 110 ; 
       7 5 110 ; 
       8 4 110 ; 
       11 30 110 ; 
       12 30 110 ; 
       15 11 110 ; 
       16 12 110 ; 
       17 32 110 ; 
       18 32 110 ; 
       20 17 110 ; 
       21 17 110 ; 
       22 17 110 ; 
       23 17 110 ; 
       24 17 110 ; 
       25 18 110 ; 
       29 31 110 ; 
       30 7 110 ; 
       31 32 110 ; 
       34 18 110 ; 
       35 17 110 ; 
       36 17 110 ; 
       37 18 110 ; 
       38 17 110 ; 
       39 18 110 ; 
       40 18 110 ; 
       41 17 110 ; 
       42 18 110 ; 
       43 17 110 ; 
       44 17 110 ; 
       45 18 110 ; 
       67 17 110 ; 
       68 18 110 ; 
       32 33 110 ; 
       9 32 110 ; 
       64 26 110 ; 
       65 26 110 ; 
       66 26 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       56 3 300 ; 
       57 4 300 ; 
       58 5 300 ; 
       59 6 300 ; 
       60 7 300 ; 
       61 8 300 ; 
       62 9 300 ; 
       19 20 300 ; 
       63 10 300 ; 
       46 24 300 ; 
       47 24 300 ; 
       54 1 300 ; 
       48 24 300 ; 
       55 0 300 ; 
       49 24 300 ; 
       50 23 300 ; 
       51 23 300 ; 
       52 23 300 ; 
       53 23 300 ; 
       17 17 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       18 14 300 ; 
       18 15 300 ; 
       18 16 300 ; 
       34 21 300 ; 
       35 22 300 ; 
       36 22 300 ; 
       37 21 300 ; 
       38 22 300 ; 
       39 21 300 ; 
       40 21 300 ; 
       41 22 300 ; 
       42 21 300 ; 
       43 22 300 ; 
       44 22 300 ; 
       45 21 300 ; 
       32 2 300 ; 
       64 11 300 ; 
       65 12 300 ; 
       66 13 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       14 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       18 5 401 ; 
       19 6 401 ; 
       20 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       33 SCHEM 20.03844 7.115469 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 23.78844 -8.884532 0 MPRFLG 0 ; 
       1 SCHEM 11.28845 -0.8845316 0 MPRFLG 0 ; 
       56 SCHEM 8.993713 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 20.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       2 SCHEM 18.78844 -0.8845316 0 MPRFLG 0 ; 
       58 SCHEM 25.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       3 SCHEM 11.28845 -2.884531 0 MPRFLG 0 ; 
       59 SCHEM 33.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       6 SCHEM 26.28844 -8.884532 0 MPRFLG 0 ; 
       10 SCHEM 28.78844 3.115468 0 MPRFLG 0 ; 
       60 SCHEM 40.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 28.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       13 SCHEM 31.28844 1.115469 0 MPRFLG 0 ; 
       14 SCHEM 31.28844 -0.8845316 0 MPRFLG 0 ; 
       62 SCHEM 30.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       19 SCHEM 13.78845 -0.8845316 0 MPRFLG 0 ; 
       26 SCHEM 29.66346 -22.14666 0 USR MPRFLG 0 ; 
       27 SCHEM 11.25 -20 0 MPRFLG 0 ; 
       28 SCHEM 3.993713 -22.03095 0 USR MPRFLG 0 ; 
       63 SCHEM 35.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 13.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 15.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 10 -22 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 18.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 12.5 -22 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 23.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 6.493713 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 3.993713 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 1.493714 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM -1.006286 -24.03095 0 WIRECOL 4 7 MPRFLG 0 ; 
       4 SCHEM 15.03845 1.115469 0 MPRFLG 0 ; 
       5 SCHEM 23.78844 1.115469 0 MPRFLG 0 ; 
       7 SCHEM 23.78844 -0.8845316 0 MPRFLG 0 ; 
       8 SCHEM 16.28844 -0.8845316 0 MPRFLG 0 ; 
       11 SCHEM 21.28844 -4.884531 0 MPRFLG 0 ; 
       12 SCHEM 25.03844 -4.884531 0 MPRFLG 0 ; 
       15 SCHEM 21.28844 -6.884531 0 MPRFLG 0 ; 
       16 SCHEM 25.03844 -6.884531 0 MPRFLG 0 ; 
       17 SCHEM 12.14501 -13.91351 0 USR MPRFLG 0 ; 
       18 SCHEM 37.22525 -13.91351 0 USR MPRFLG 0 ; 
       20 SCHEM 13.39501 -15.91351 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 18.39501 -15.91351 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 15.89501 -15.91351 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 23.39501 -15.91351 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 20.89501 -15.91351 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 45.97525 -15.91351 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 8.788445 1.115469 0 MPRFLG 0 ; 
       30 SCHEM 23.78844 -2.884531 0 MPRFLG 0 ; 
       31 SCHEM 17.53844 3.115468 0 MPRFLG 0 ; 
       34 SCHEM 33.47525 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 3.395007 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM -1.604993 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 28.47526 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 0.8950064 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 30.97526 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 40.97525 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 10.89501 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 35.97525 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 5.895007 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 8.395006 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 38.47525 -15.91351 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 25.89501 -15.91351 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 43.47525 -15.91351 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 20.03844 5.115468 0 MPRFLG 0 ; 
       9 SCHEM 31.28844 3.115468 0 MPRFLG 0 ; 
       64 SCHEM 38.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 43.41346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 45.91346 -24.14666 0 WIRECOL 2 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.47525 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 74.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 79.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 84.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 58.17583 -1.078068 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 59.22066 -0.8055077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 54.54168 0.2393112 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 175.8722 -3.799718 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.56545 -5.804897 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM -5.297054 0.2405224 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 89.96411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 105.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 110.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 115.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 120.8707 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 97.46411 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 104.9641 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 58.17583 -3.078068 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59.22066 -2.805508 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54.54168 -1.760689 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 175.8722 -5.799718 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
