SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.76-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       ss304_research-mat66.1-0 ; 
       ss304_research-mat67.2-0 ; 
       ss304_research-mat68.2-0 ; 
       ss304_research-mat69.2-0 ; 
       ss304_research-mat70.2-0 ; 
       ss304_research-mat71.1-0 ; 
       ss304_research-mat72.4-0 ; 
       ss304_research-mat73.3-0 ; 
       ss304_research-mat74.4-0 ; 
       ss304_research-mat78.3-0 ; 
       ss304_research-mat79.3-0 ; 
       ss304_research-mat81.2-0 ; 
       ss304_research-mat82.3-0 ; 
       ss304_research-mat83.2-0 ; 
       ss304_research-mat84.3-0 ; 
       ss304_research-mat87.1-0 ; 
       ss304_research-mat88.1-0 ; 
       ss304_research-mat89.1-0 ; 
       ss304_research-mat90.1-0 ; 
       ss304_research-mat91.2-0 ; 
       ss304_research-mat92.1-0 ; 
       ss304_research-mat93.1-0 ; 
       ss304_research-mat94.2-0 ; 
       ss304_research-mat95.1-0 ; 
       ss304_research-mat98.1-0 ; 
       ss305_elect_station-mat52.2-0 ; 
       ss305_elect_station-mat53.2-0 ; 
       ss305_elect_station-mat54.2-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       root-cube13.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube4_1.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl2.1-0 ; 
       root-cyl2_1.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-extru44.1-0 ; 
       root-null22.1-0 ; 
       root-rock.2-0 ; 
       root-root_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/beltersbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/ss304 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-ss304-research.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       ss304_research-rendermap2.4-0 ; 
       ss304_research-t2d58.5-0 ; 
       ss304_research-t2d59.3-0 ; 
       ss304_research-t2d60.5-0 ; 
       ss304_research-t2d61.3-0 ; 
       ss304_research-t2d62.4-0 ; 
       ss304_research-t2d63.7-0 ; 
       ss304_research-t2d64.5-0 ; 
       ss304_research-t2d65.4-0 ; 
       ss304_research-t2d66.3-0 ; 
       ss304_research-t2d67.6-0 ; 
       ss304_research-t2d68.4-0 ; 
       ss304_research-t2d69.2-0 ; 
       ss304_research-t2d70.2-0 ; 
       ss304_research-t2d71.3-0 ; 
       ss304_research-t2d72.2-0 ; 
       ss304_research-t2d73.2-0 ; 
       ss304_research-t2d74.2-0 ; 
       ss304_research-t2d75.2-0 ; 
       ss304_research-t2d76.5-0 ; 
       ss304_research-t2d77.5-0 ; 
       ss304_research-t2d78.3-0 ; 
       ss304_research-t2d79.4-0 ; 
       ss304_research-t2d80.3-0 ; 
       ss304_research-t2d85.4-0 ; 
       ss304_research-t2d86.2-0 ; 
       ss305_elect_station-t2d53.7-0 ; 
       ss305_elect_station-t2d54.7-0 ; 
       ss305_elect_station-t2d55.4-0 ; 
       ss305_elect_station-t2d56.4-0 ; 
       ss305_elect_station-t2d57.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 16 110 ; 
       2 16 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 17 110 ; 
       6 17 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 5 110 ; 
       10 9 110 ; 
       11 7 110 ; 
       12 8 110 ; 
       13 17 110 ; 
       14 17 110 ; 
       15 1 110 ; 
       16 17 110 ; 
       17 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       0 22 300 ; 
       0 23 300 ; 
       1 6 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       2 9 300 ; 
       2 17 300 ; 
       3 10 300 ; 
       3 21 300 ; 
       4 7 300 ; 
       4 24 300 ; 
       5 2 300 ; 
       6 1 300 ; 
       7 11 300 ; 
       8 13 300 ; 
       9 3 300 ; 
       10 4 300 ; 
       11 12 300 ; 
       11 15 300 ; 
       12 14 300 ; 
       12 16 300 ; 
       13 28 300 ; 
       13 29 300 ; 
       13 30 300 ; 
       14 25 300 ; 
       14 26 300 ; 
       14 27 300 ; 
       14 18 300 ; 
       15 31 300 ; 
       16 5 300 ; 
       17 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 15 401 ; 
       2 5 401 ; 
       3 18 401 ; 
       4 17 401 ; 
       6 10 401 ; 
       7 24 401 ; 
       8 19 401 ; 
       9 6 401 ; 
       10 20 401 ; 
       11 14 401 ; 
       12 1 401 ; 
       13 13 401 ; 
       14 3 401 ; 
       15 2 401 ; 
       16 4 401 ; 
       17 7 401 ; 
       18 9 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 25 401 ; 
       25 8 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -8 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 MPRFLG 0 ; 
       3 SCHEM 25 -8 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 30 -4 0 MPRFLG 0 ; 
       6 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       8 SCHEM 26.25 -12 0 MPRFLG 0 ; 
       9 SCHEM 30 -6 0 MPRFLG 0 ; 
       10 SCHEM 30 -8 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -14 0 MPRFLG 0 ; 
       12 SCHEM 26.25 -14 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 15 -8 0 MPRFLG 0 ; 
       16 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 17.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 29 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 12 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 12 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
