SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.70-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-mat66.1-0 ; 
       ss304_research-mat67.2-0 ; 
       ss304_research-mat68.2-0 ; 
       ss304_research-mat69.2-0 ; 
       ss304_research-mat70.2-0 ; 
       ss304_research-mat71.1-0 ; 
       ss304_research-mat72.4-0 ; 
       ss304_research-mat73.3-0 ; 
       ss304_research-mat74.4-0 ; 
       ss304_research-mat75.3-0 ; 
       ss304_research-mat76.3-0 ; 
       ss304_research-mat77.1-0 ; 
       ss304_research-mat78.3-0 ; 
       ss304_research-mat79.3-0 ; 
       ss304_research-mat80.1-0 ; 
       ss304_research-mat81.2-0 ; 
       ss304_research-mat82.3-0 ; 
       ss304_research-mat83.2-0 ; 
       ss304_research-mat84.3-0 ; 
       ss304_research-mat85.1-0 ; 
       ss304_research-mat86.1-0 ; 
       ss304_research-mat87.1-0 ; 
       ss304_research-mat88.1-0 ; 
       ss304_research-mat89.1-0 ; 
       ss304_research-mat90.1-0 ; 
       ss304_research-mat91.2-0 ; 
       ss304_research-mat92.1-0 ; 
       ss304_research-mat93.1-0 ; 
       ss304_research-mat94.2-0 ; 
       ss304_research-mat95.1-0 ; 
       ss304_research-mat96.1-0 ; 
       ss304_research-mat97.1-0 ; 
       ss304_research-mat98.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.2-0 ; 
       ss305_elect_station-mat53.2-0 ; 
       ss305_elect_station-mat54.2-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.3-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 69     
       root-cube1.1-0 ; 
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube4_1.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl2.1-0 ; 
       root-cyl2_1.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-extru44.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.2-0 ; 
       root-null18_1.2-0 ; 
       root-null19_2.2-0 ; 
       root-null20.2-0 ; 
       root-null21.1-0 ; 
       root-null22.1-0 ; 
       root-rock.2-0 ; 
       root-root.50-0 ROOT ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss304/PICTURES/beltersbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss304/PICTURES/ss304 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss304/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-ss304-research.66-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       ss304_research-rendermap2.3-0 ; 
       ss304_research-t2d58.4-0 ; 
       ss304_research-t2d59.2-0 ; 
       ss304_research-t2d60.4-0 ; 
       ss304_research-t2d61.2-0 ; 
       ss304_research-t2d62.3-0 ; 
       ss304_research-t2d63.6-0 ; 
       ss304_research-t2d64.4-0 ; 
       ss304_research-t2d65.2-0 ; 
       ss304_research-t2d66.1-0 ; 
       ss304_research-t2d67.5-0 ; 
       ss304_research-t2d68.3-0 ; 
       ss304_research-t2d69.1-0 ; 
       ss304_research-t2d70.1-0 ; 
       ss304_research-t2d71.2-0 ; 
       ss304_research-t2d72.1-0 ; 
       ss304_research-t2d73.1-0 ; 
       ss304_research-t2d74.1-0 ; 
       ss304_research-t2d75.1-0 ; 
       ss304_research-t2d76.3-0 ; 
       ss304_research-t2d77.4-0 ; 
       ss304_research-t2d78.2-0 ; 
       ss304_research-t2d79.2-0 ; 
       ss304_research-t2d80.1-0 ; 
       ss304_research-t2d81.2-0 ; 
       ss304_research-t2d82.1-0 ; 
       ss304_research-t2d83.2-0 ; 
       ss304_research-t2d84.1-0 ; 
       ss304_research-t2d85.3-0 ; 
       ss304_research-t2d86.1-0 ; 
       ss305_elect_station-t2d53.5-0 ; 
       ss305_elect_station-t2d54.5-0 ; 
       ss305_elect_station-t2d55.3-0 ; 
       ss305_elect_station-t2d56.3-0 ; 
       ss305_elect_station-t2d57.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 1 110 ; 
       4 31 110 ; 
       5 31 110 ; 
       6 16 110 ; 
       7 5 110 ; 
       8 4 110 ; 
       9 32 110 ; 
       10 32 110 ; 
       11 30 110 ; 
       12 30 110 ; 
       13 9 110 ; 
       14 13 110 ; 
       15 11 110 ; 
       16 12 110 ; 
       17 32 110 ; 
       18 32 110 ; 
       19 4 110 ; 
       20 17 110 ; 
       21 17 110 ; 
       22 17 110 ; 
       23 17 110 ; 
       24 17 110 ; 
       25 18 110 ; 
       26 32 110 ; 
       27 32 110 ; 
       28 32 110 ; 
       29 31 110 ; 
       30 7 110 ; 
       31 32 110 ; 
       32 33 110 ; 
       34 18 110 ; 
       35 17 110 ; 
       36 17 110 ; 
       37 18 110 ; 
       38 17 110 ; 
       39 18 110 ; 
       40 18 110 ; 
       41 17 110 ; 
       42 18 110 ; 
       43 17 110 ; 
       44 17 110 ; 
       45 18 110 ; 
       46 26 110 ; 
       47 26 110 ; 
       48 26 110 ; 
       49 26 110 ; 
       50 28 110 ; 
       51 28 110 ; 
       52 28 110 ; 
       53 28 110 ; 
       54 27 110 ; 
       55 27 110 ; 
       56 28 110 ; 
       57 26 110 ; 
       58 26 110 ; 
       59 26 110 ; 
       60 26 110 ; 
       61 26 110 ; 
       62 26 110 ; 
       63 26 110 ; 
       64 26 110 ; 
       65 26 110 ; 
       66 26 110 ; 
       67 17 110 ; 
       68 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 21 300 ; 
       1 11 300 ; 
       1 32 300 ; 
       2 10 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       3 12 300 ; 
       3 33 300 ; 
       4 8 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       5 14 300 ; 
       5 25 300 ; 
       6 22 300 ; 
       7 15 300 ; 
       7 29 300 ; 
       8 9 300 ; 
       8 34 300 ; 
       9 4 300 ; 
       10 3 300 ; 
       11 17 300 ; 
       12 19 300 ; 
       13 5 300 ; 
       14 6 300 ; 
       15 18 300 ; 
       15 23 300 ; 
       16 20 300 ; 
       16 24 300 ; 
       17 49 300 ; 
       17 50 300 ; 
       17 51 300 ; 
       18 46 300 ; 
       18 47 300 ; 
       18 48 300 ; 
       18 26 300 ; 
       19 52 300 ; 
       29 13 300 ; 
       30 16 300 ; 
       31 7 300 ; 
       32 2 300 ; 
       34 53 300 ; 
       35 54 300 ; 
       36 54 300 ; 
       37 53 300 ; 
       38 54 300 ; 
       39 53 300 ; 
       40 53 300 ; 
       41 54 300 ; 
       42 53 300 ; 
       43 54 300 ; 
       44 54 300 ; 
       45 53 300 ; 
       46 56 300 ; 
       47 56 300 ; 
       48 56 300 ; 
       49 56 300 ; 
       50 55 300 ; 
       51 55 300 ; 
       52 55 300 ; 
       53 55 300 ; 
       54 1 300 ; 
       55 0 300 ; 
       56 35 300 ; 
       57 36 300 ; 
       58 37 300 ; 
       59 38 300 ; 
       60 39 300 ; 
       61 40 300 ; 
       62 41 300 ; 
       63 42 300 ; 
       64 43 300 ; 
       65 44 300 ; 
       66 45 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 15 401 ; 
       4 5 401 ; 
       5 18 401 ; 
       6 17 401 ; 
       8 10 401 ; 
       9 28 401 ; 
       10 19 401 ; 
       11 24 401 ; 
       12 26 401 ; 
       14 6 401 ; 
       15 20 401 ; 
       17 14 401 ; 
       18 1 401 ; 
       19 13 401 ; 
       20 3 401 ; 
       23 2 401 ; 
       24 4 401 ; 
       25 7 401 ; 
       26 9 401 ; 
       27 11 401 ; 
       28 12 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 25 401 ; 
       33 27 401 ; 
       46 8 401 ; 
       47 30 401 ; 
       48 31 401 ; 
       49 32 401 ; 
       50 33 401 ; 
       51 34 401 ; 
       52 16 401 ; 
       34 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 162.5 -16 0 MPRFLG 0 ; 
       1 SCHEM 128.75 -8 0 MPRFLG 0 ; 
       2 SCHEM 142.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 126.25 -10 0 MPRFLG 0 ; 
       4 SCHEM 138.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 170 -6 0 MPRFLG 0 ; 
       6 SCHEM 165 -16 0 MPRFLG 0 ; 
       7 SCHEM 167.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 137.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 192.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 197.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 157.5 -12 0 MPRFLG 0 ; 
       12 SCHEM 167.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 191.25 -6 0 MPRFLG 0 ; 
       14 SCHEM 190 -8 0 MPRFLG 0 ; 
       15 SCHEM 156.25 -14 0 MPRFLG 0 ; 
       16 SCHEM 166.25 -14 0 MPRFLG 0 ; 
       17 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       19 SCHEM 135 -8 0 MPRFLG 0 ; 
       20 SCHEM 100 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 105 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 102.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 110 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 107.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 66.25 -4 0 MPRFLG 0 ; 
       27 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       28 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 122.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 165 -10 0 MPRFLG 0 ; 
       31 SCHEM 155 -4 0 MPRFLG 0 ; 
       32 SCHEM 101.25 -2 0 MPRFLG 0 ; 
       33 SCHEM 101.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 25 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 90 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 85 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 20 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 87.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 22.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 32.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 97.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 92.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 95 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 30 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 50 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 52.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 55 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 60 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 10 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 7.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 2.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 12.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 57.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 62.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 70 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 77.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 65 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 67.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 72.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 75 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 80 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 82.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 200 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 197.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 195 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 190 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 187.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 125 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 177.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 175 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 160 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 155 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 172.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 167.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 162.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 165 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 157.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 170 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 45.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 180 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 132.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 41.75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 43.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 40.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 134 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 24 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 89 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 200 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 200 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 155 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 157.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 167.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 170 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 195 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 182.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 185 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 41.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 45.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 172.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 160 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 197.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 134 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 190 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 192.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 177.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 180 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 142.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 145 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 130 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 132.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 125 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 127.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 137.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 43.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 40.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 120 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 202.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
