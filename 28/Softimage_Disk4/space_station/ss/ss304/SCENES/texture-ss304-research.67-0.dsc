SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.71-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-mat66.1-0 ; 
       ss304_research-mat67.2-0 ; 
       ss304_research-mat68.2-0 ; 
       ss304_research-mat69.2-0 ; 
       ss304_research-mat70.2-0 ; 
       ss304_research-mat71.1-0 ; 
       ss304_research-mat72.4-0 ; 
       ss304_research-mat73.3-0 ; 
       ss304_research-mat74.4-0 ; 
       ss304_research-mat75.3-0 ; 
       ss304_research-mat76.3-0 ; 
       ss304_research-mat77.1-0 ; 
       ss304_research-mat78.3-0 ; 
       ss304_research-mat79.3-0 ; 
       ss304_research-mat80.1-0 ; 
       ss304_research-mat81.2-0 ; 
       ss304_research-mat82.3-0 ; 
       ss304_research-mat83.2-0 ; 
       ss304_research-mat84.3-0 ; 
       ss304_research-mat86.2-0 ; 
       ss304_research-mat87.1-0 ; 
       ss304_research-mat88.1-0 ; 
       ss304_research-mat89.1-0 ; 
       ss304_research-mat90.1-0 ; 
       ss304_research-mat91.2-0 ; 
       ss304_research-mat92.1-0 ; 
       ss304_research-mat93.1-0 ; 
       ss304_research-mat94.2-0 ; 
       ss304_research-mat95.1-0 ; 
       ss304_research-mat96.1-0 ; 
       ss304_research-mat97.1-0 ; 
       ss304_research-mat98.1-0 ; 
       ss304_research-mat99.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-mat52.2-0 ; 
       ss305_elect_station-mat53.2-0 ; 
       ss305_elect_station-mat54.2-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.3-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 69     
       root-cube10.1-0 ; 
       root-cube13.1-0 ; 
       root-cube16.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube23.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube4_1.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl2.1-0 ; 
       root-cyl2_1.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-extru44.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.2-0 ; 
       root-null18_1.2-0 ; 
       root-null19_2.2-0 ; 
       root-null20.2-0 ; 
       root-null21.1-0 ; 
       root-null22.1-0 ; 
       root-rock.2-0 ; 
       root-root.51-0 ROOT ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss304/PICTURES/beltersbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss304/PICTURES/ss304 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss304/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-ss304-research.67-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       ss304_research-rendermap2.3-0 ; 
       ss304_research-t2d58.4-0 ; 
       ss304_research-t2d59.2-0 ; 
       ss304_research-t2d60.4-0 ; 
       ss304_research-t2d61.2-0 ; 
       ss304_research-t2d62.3-0 ; 
       ss304_research-t2d63.6-0 ; 
       ss304_research-t2d64.4-0 ; 
       ss304_research-t2d65.3-0 ; 
       ss304_research-t2d66.2-0 ; 
       ss304_research-t2d67.5-0 ; 
       ss304_research-t2d68.3-0 ; 
       ss304_research-t2d69.1-0 ; 
       ss304_research-t2d70.1-0 ; 
       ss304_research-t2d71.2-0 ; 
       ss304_research-t2d72.1-0 ; 
       ss304_research-t2d73.1-0 ; 
       ss304_research-t2d74.1-0 ; 
       ss304_research-t2d75.1-0 ; 
       ss304_research-t2d76.3-0 ; 
       ss304_research-t2d77.4-0 ; 
       ss304_research-t2d78.2-0 ; 
       ss304_research-t2d79.2-0 ; 
       ss304_research-t2d80.1-0 ; 
       ss304_research-t2d81.2-0 ; 
       ss304_research-t2d82.1-0 ; 
       ss304_research-t2d83.2-0 ; 
       ss304_research-t2d84.1-0 ; 
       ss304_research-t2d85.3-0 ; 
       ss304_research-t2d86.1-0 ; 
       ss304_research-t2d87.1-0 ; 
       ss304_research-t2d88.1-0 ; 
       ss305_elect_station-t2d53.6-0 ; 
       ss305_elect_station-t2d54.6-0 ; 
       ss305_elect_station-t2d55.3-0 ; 
       ss305_elect_station-t2d56.3-0 ; 
       ss305_elect_station-t2d57.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       2 0 110 ; 
       3 31 110 ; 
       4 31 110 ; 
       5 16 110 ; 
       6 4 110 ; 
       7 3 110 ; 
       8 32 110 ; 
       10 32 110 ; 
       11 30 110 ; 
       12 30 110 ; 
       13 8 110 ; 
       14 13 110 ; 
       15 11 110 ; 
       16 12 110 ; 
       17 32 110 ; 
       18 32 110 ; 
       19 3 110 ; 
       20 17 110 ; 
       21 17 110 ; 
       22 17 110 ; 
       23 17 110 ; 
       24 17 110 ; 
       25 18 110 ; 
       26 32 110 ; 
       27 32 110 ; 
       28 32 110 ; 
       29 31 110 ; 
       30 6 110 ; 
       31 32 110 ; 
       32 33 110 ; 
       34 18 110 ; 
       35 17 110 ; 
       36 17 110 ; 
       37 18 110 ; 
       38 17 110 ; 
       39 18 110 ; 
       40 18 110 ; 
       41 17 110 ; 
       42 18 110 ; 
       43 17 110 ; 
       44 17 110 ; 
       45 18 110 ; 
       46 26 110 ; 
       47 26 110 ; 
       48 26 110 ; 
       49 26 110 ; 
       50 28 110 ; 
       51 28 110 ; 
       52 28 110 ; 
       53 28 110 ; 
       54 27 110 ; 
       55 27 110 ; 
       56 28 110 ; 
       57 26 110 ; 
       58 26 110 ; 
       59 26 110 ; 
       60 26 110 ; 
       61 26 110 ; 
       62 26 110 ; 
       63 26 110 ; 
       64 26 110 ; 
       65 26 110 ; 
       66 26 110 ; 
       67 17 110 ; 
       68 18 110 ; 
       9 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       0 31 300 ; 
       1 10 300 ; 
       1 29 300 ; 
       1 30 300 ; 
       2 12 300 ; 
       2 32 300 ; 
       3 8 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       4 14 300 ; 
       4 24 300 ; 
       5 21 300 ; 
       6 15 300 ; 
       6 28 300 ; 
       7 9 300 ; 
       7 33 300 ; 
       8 4 300 ; 
       10 3 300 ; 
       11 17 300 ; 
       12 19 300 ; 
       13 5 300 ; 
       14 6 300 ; 
       15 18 300 ; 
       15 22 300 ; 
       16 20 300 ; 
       16 23 300 ; 
       17 49 300 ; 
       17 50 300 ; 
       17 51 300 ; 
       18 46 300 ; 
       18 47 300 ; 
       18 48 300 ; 
       18 25 300 ; 
       19 52 300 ; 
       29 13 300 ; 
       30 16 300 ; 
       31 7 300 ; 
       32 2 300 ; 
       34 53 300 ; 
       35 54 300 ; 
       36 54 300 ; 
       37 53 300 ; 
       38 54 300 ; 
       39 53 300 ; 
       40 53 300 ; 
       41 54 300 ; 
       42 53 300 ; 
       43 54 300 ; 
       44 54 300 ; 
       45 53 300 ; 
       46 56 300 ; 
       47 56 300 ; 
       48 56 300 ; 
       49 56 300 ; 
       50 55 300 ; 
       51 55 300 ; 
       52 55 300 ; 
       53 55 300 ; 
       54 1 300 ; 
       55 0 300 ; 
       56 35 300 ; 
       57 36 300 ; 
       58 37 300 ; 
       59 38 300 ; 
       60 39 300 ; 
       61 40 300 ; 
       62 41 300 ; 
       63 42 300 ; 
       64 43 300 ; 
       65 44 300 ; 
       66 45 300 ; 
       9 34 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 15 401 ; 
       4 5 401 ; 
       5 18 401 ; 
       6 17 401 ; 
       8 10 401 ; 
       9 28 401 ; 
       10 19 401 ; 
       11 24 401 ; 
       12 26 401 ; 
       14 6 401 ; 
       15 20 401 ; 
       17 14 401 ; 
       18 1 401 ; 
       19 13 401 ; 
       20 3 401 ; 
       21 30 401 ; 
       22 2 401 ; 
       23 4 401 ; 
       24 7 401 ; 
       25 9 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 25 401 ; 
       32 27 401 ; 
       46 8 401 ; 
       47 32 401 ; 
       48 33 401 ; 
       49 34 401 ; 
       50 35 401 ; 
       51 36 401 ; 
       52 16 401 ; 
       33 29 401 ; 
       34 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 105 -12 0 MPRFLG 0 ; 
       1 SCHEM 112.5 -12 0 MPRFLG 0 ; 
       2 SCHEM 105 -14 0 MPRFLG 0 ; 
       3 SCHEM 108.75 -10 0 MPRFLG 0 ; 
       4 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 117.5 -20 0 MPRFLG 0 ; 
       6 SCHEM 117.5 -12 0 MPRFLG 0 ; 
       7 SCHEM 110 -12 0 MPRFLG 0 ; 
       8 SCHEM 122.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 125 -8 0 MPRFLG 0 ; 
       11 SCHEM 115 -16 0 MPRFLG 0 ; 
       12 SCHEM 118.75 -16 0 MPRFLG 0 ; 
       13 SCHEM 122.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 122.5 -12 0 MPRFLG 0 ; 
       15 SCHEM 115 -18 0 MPRFLG 0 ; 
       16 SCHEM 118.75 -18 0 MPRFLG 0 ; 
       17 SCHEM 86.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       19 SCHEM 107.5 -12 0 MPRFLG 0 ; 
       20 SCHEM 87.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 92.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 90 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 97.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 95 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 35 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       27 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       28 SCHEM 5 -8 0 MPRFLG 0 ; 
       29 SCHEM 102.5 -10 0 MPRFLG 0 ; 
       30 SCHEM 117.5 -14 0 MPRFLG 0 ; 
       31 SCHEM 111.25 -8 0 MPRFLG 0 ; 
       32 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 62.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 22.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 77.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 72.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 17.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 20 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 30 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 85 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 80 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 82.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 27.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 37.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 40 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 42.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 47.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 7.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 2.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 0 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 10 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 45 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 50 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 57.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 65 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 52.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 55 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 60 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 62.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 67.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 70 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 120 -20 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 126.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 124 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 124 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 124 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 121.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 121.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 114 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 109 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 111.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 104 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 101.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 121.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 121.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 121.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 116.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 114 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 121.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 121.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 116.5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 114 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 121.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 121.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 114 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 114 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 121.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 111.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 111.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 104 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 34.25 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 35.75 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 32.75 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 101.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 101.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 101.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 106.5 -14 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 21.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 76.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 109 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 119 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 126.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 114 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 114 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 121.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 121.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 124 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 121.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 121.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 34.25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 114 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 114 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 114 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 121.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 116.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 124 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 121.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 124 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 111.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 121.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 121.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 111.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 111.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 104 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 104 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 109 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 35.75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 32.75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 101.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 101.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 101.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 109 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 116.5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 119 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
