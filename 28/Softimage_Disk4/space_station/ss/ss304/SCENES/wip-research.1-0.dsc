SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.1-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       research-mat66.1-0 ; 
       ss301_garrison-default1.1-0 ; 
       ss301_garrison-mat2.1-0 ; 
       ss301_garrison-mat65.1-0 ; 
       ss301_garrison-mat66.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 66     
       2-bmerge1.1-0 ; 
       2-cube1.1-0 ; 
       2-cube10.1-0 ROOT ; 
       2-cube11.1-0 ; 
       2-cube12.1-0 ROOT ; 
       2-cube13.1-0 ; 
       2-cube14.1-0 ; 
       2-cube16.1-0 ; 
       2-cube17.1-0 ROOT ; 
       2-cube2.1-0 ; 
       2-cube3.1-0 ; 
       2-cube5.2-0 ; 
       2-cube6.1-0 ; 
       2-cube7.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ; 
       2-extru44.1-0 ; 
       2-null18.1-0 ROOT ; 
       2-null18_1.1-0 ROOT ; 
       2-null19.1-0 ROOT ; 
       2-skin2.1-0 ROOT ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_30_1.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_31_1.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       research-cube18.1-0 ; 
       research-cube19.1-0 ; 
       research-cube20.1-0 ROOT ; 
       research-cube21.1-0 ROOT ; 
       research-cube4.1-0 ; 
       research-cube4_1.1-0 ; 
       research-cyl2.1-0 ; 
       research-cyl2_1.1-0 ; 
       research-east_bay_11_8.1-0 ; 
       research-east_bay_11_9.1-0 ; 
       research-garage1A.1-0 ; 
       research-garage1B.1-0 ; 
       research-garage1C.1-0 ; 
       research-garage1D.1-0 ; 
       research-garage1E.1-0 ; 
       research-launch1.1-0 ; 
       research-null20.2-0 ; 
       research-null21.1-0 ROOT ; 
       research-null22.1-0 ROOT ; 
       research-SS_11.1-0 ; 
       research-SS_11_1.1-0 ; 
       research-SS_13_2.1-0 ; 
       research-SS_13_3.1-0 ; 
       research-SS_15_1.1-0 ; 
       research-SS_15_3.1-0 ; 
       research-SS_23.1-0 ; 
       research-SS_23_2.1-0 ; 
       research-SS_24.1-0 ; 
       research-SS_24_1.1-0 ; 
       research-SS_26.1-0 ; 
       research-SS_26_3.1-0 ; 
       research-turwepemt2.1-0 ; 
       research-turwepemt2_3.1-0 ; 
       utl20-skin2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss304/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-research.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       research-rendermap2.1-0 ; 
       ss301_garrison-t2d2.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       38 36 110 ; 
       37 49 110 ; 
       39 37 110 ; 
       32 50 110 ; 
       33 50 110 ; 
       0 21 110 ; 
       1 16 110 ; 
       3 13 110 ; 
       5 13 110 ; 
       6 21 110 ; 
       7 2 110 ; 
       9 16 110 ; 
       10 21 110 ; 
       36 49 110 ; 
       11 21 110 ; 
       12 21 110 ; 
       13 11 110 ; 
       14 16 110 ; 
       15 14 110 ; 
       16 21 110 ; 
       40 48 110 ; 
       41 48 110 ; 
       17 21 110 ; 
       42 40 110 ; 
       43 40 110 ; 
       44 40 110 ; 
       45 40 110 ; 
       46 40 110 ; 
       47 41 110 ; 
       48 50 110 ; 
       51 41 110 ; 
       52 40 110 ; 
       53 40 110 ; 
       54 41 110 ; 
       55 40 110 ; 
       56 41 110 ; 
       57 41 110 ; 
       58 40 110 ; 
       59 41 110 ; 
       60 40 110 ; 
       61 40 110 ; 
       62 41 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 19 110 ; 
       25 18 110 ; 
       26 19 110 ; 
       27 18 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       63 40 110 ; 
       64 41 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       65 0 300 ; 
       0 1 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       40 10 300 ; 
       40 11 300 ; 
       40 12 300 ; 
       41 7 300 ; 
       41 8 300 ; 
       41 9 300 ; 
       17 13 300 ; 
       21 2 300 ; 
       51 14 300 ; 
       52 15 300 ; 
       53 15 300 ; 
       54 14 300 ; 
       55 15 300 ; 
       56 14 300 ; 
       57 14 300 ; 
       58 15 300 ; 
       59 14 300 ; 
       60 15 300 ; 
       61 15 300 ; 
       62 14 300 ; 
       22 17 300 ; 
       23 17 300 ; 
       24 6 300 ; 
       25 17 300 ; 
       26 5 300 ; 
       27 17 300 ; 
       28 16 300 ; 
       29 16 300 ; 
       30 16 300 ; 
       31 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       38 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       37 SCHEM 85 -2 0 MPRFLG 0 ; 
       39 SCHEM 85 -4 0 MPRFLG 0 ; 
       49 SCHEM 83.75 0 0 SRT 1 1 1 1.570796 0 0 -1.676083 -13.07177 0 MPRFLG 0 ; 
       32 SCHEM 52.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       33 SCHEM 55 -2 0 MPRFLG 0 ; 
       34 SCHEM 125 0 0 SRT 0.7040001 0.5474305 0.7040001 0 0 0 -49.97231 -7.298912 -4.378941 MPRFLG 0 ; 
       50 SCHEM 28.75 0 0 SRT 1 1 1 0 -1.570796 0 0.4186246 13.08433 39.97947 MPRFLG 0 ; 
       35 SCHEM 117.5 0 0 SRT 2.304 0.1330974 1.626624 0 -1.570796 0 -0.8467523 -5.532363 63.13019 MPRFLG 0 ; 
       65 SCHEM 115 0 0 SRT 1.08504 1.08504 1.08504 0 0 0 1.266336 -5.390056 -5.318614 MPRFLG 0 ; 
       0 SCHEM 75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 67.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 112.5 0 0 SRT 0.6119999 0.6119999 0.6119999 -1.078445e-008 2.38158e-008 -4.766174e-008 -8.919128 1.003585 40.34068 MPRFLG 0 ; 
       3 SCHEM 62.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 122.5 0 0 DISPLAY 0 0 SRT 1 1 1 -1.078445e-008 2.38158e-008 -4.766174e-008 94.57163 14.31906 5.263856 MPRFLG 0 ; 
       5 SCHEM 65 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 80 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 112.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 120 0 0 DISPLAY 0 0 SRT 1 1 1 -1.042874e-008 3.832456e-008 -4.766174e-008 90.26583 13.30595 5.263856 MPRFLG 0 ; 
       9 SCHEM 70 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 77.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 82.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 63.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 60 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 63.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 72.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 72.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 70 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       41 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       17 SCHEM 57.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 37.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       43 SCHEM 42.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       44 SCHEM 40 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM 47.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       46 SCHEM 45 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       47 SCHEM 20 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 91.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 1.570796 0 104.3961 -4.675931 9.374343 MPRFLG 0 ; 
       19 SCHEM 108.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0.4847831 0 11.05156 -3.000762 7.009114 MPRFLG 0 ; 
       20 SCHEM 101.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 104.473 3.878042 8.341749 MPRFLG 0 ; 
       48 SCHEM 26.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 68.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       51 SCHEM 7.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 22.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 2.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 25 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 15 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 35 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 10 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 30 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 32.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 12.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 87.5 -2 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 90 -2 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 107.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 92.5 -2 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 110 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 95 -2 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 105 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 102.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 100 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 97.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 81.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 109 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 106.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40.16202 12.18062 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.20684 12.45318 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 36.52786 13.498 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 188.3548 1.702164 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.30164 7.453791 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 27.93913 13.49921 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 104 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 114 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 114 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 81.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 40.16202 10.18062 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 41.20684 10.45318 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.52786 11.498 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 188.3548 -0.2978363 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
