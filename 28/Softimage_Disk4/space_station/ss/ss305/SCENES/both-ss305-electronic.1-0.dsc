SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.8-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       purpmeteor-default9.2-0 ; 
       ss301_garrison-white_strobe1_33.2-0 ; 
       ss301_garrison-white_strobe1_34.2-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_electronic-mat59.1-0 ; 
       ss305_electronic-mat60.1-0 ; 
       ss305_electronic-mat61.1-0 ; 
       ss305_electronic-mat62.1-0 ; 
       ss305_electronic-mat63.1-0 ; 
       ss305_electronic-mat64.1-0 ; 
       ss305_electronic-white_strobe1_35.1-0 ; 
       ss305_electronic-white_strobe1_36.1-0 ; 
       ss305_electronic-white_strobe1_37.1-0 ; 
       ss305_electronic-white_strobe1_38.1-0 ; 
       ss305_electronic-white_strobe1_39.1-0 ; 
       ss305_electronic-white_strobe1_40.1-0 ; 
       ss305_electronic-white_strobe1_41.1-0 ; 
       ss305_electronic-white_strobe1_42.1-0 ; 
       ss305_electronic-white_strobe1_43.1-0 ; 
       ss305_electronic-white_strobe1_44.1-0 ; 
       ss305_electronic-white_strobe1_45.1-0 ; 
       ss305_electronic-white_strobe1_46.1-0 ; 
       ss305_electronic-white_strobe1_47.1-0 ; 
       ss305_electronic-white_strobe1_48.1-0 ; 
       ss305_electronic-white_strobe1_49.1-0 ; 
       ss305_electronic-white_strobe1_50.1-0 ; 
       ss305_electronic-white_strobe1_51.1-0 ; 
       ss305_electronic-white_strobe1_52.1-0 ; 
       ss305_electronic-white_strobe1_53.1-0 ; 
       ss305_electronic-white_strobe1_54.1-0 ; 
       ss305_electronic-white_strobe1_55.1-0 ; 
       ss305_electronic-white_strobe1_56.1-0 ; 
       ss305_electronic-white_strobe1_57.1-0 ; 
       ss305_elect_station-mat52.3-0 ; 
       ss305_elect_station-mat53.3-0 ; 
       ss305_elect_station-mat54.3-0 ; 
       ss305_elect_station-mat55.3-0 ; 
       ss305_elect_station-mat56.3-0 ; 
       ss305_elect_station-mat57.3-0 ; 
       ss305_elect_station-mat7.3-0 ; 
       ss305_elect_station-white_strobe1_25.3-0 ; 
       ss305_elect_station-white_strobe1_31.3-0 ; 
       ss305_elect_station-white_strobe1_5.3-0 ; 
       ss305_elect_station-white_strobe1_9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 110     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bound07.1-0 ; 
       bounding_model-bound08.1-0 ; 
       bounding_model-bound09.1-0 ; 
       bounding_model-bound10.1-0 ; 
       bounding_model-bound11.1-0 ; 
       bounding_model-bounding_model.1-0 ROOT ; 
       root-cube10.1-0 ; 
       root-cube12.1-0 ; 
       root-cube13.1-0 ; 
       root-cube13_1.1-0 ; 
       root-cube13_2.1-0 ; 
       root-cube18.2-0 ; 
       root-cube19.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube23.1-0 ; 
       root-cube25.1-0 ; 
       root-cube26.1-0 ; 
       root-cube26_1.1-0 ; 
       root-cube26_2.1-0 ; 
       root-cube27.1-0 ; 
       root-cube28.1-0 ; 
       root-cube3.1-0 ; 
       root-cube3_1.1-0 ; 
       root-cube4.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_8_1.1-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-east_bay_11_9_1.1-0 ; 
       root-extru44.1-0 ; 
       root-extru48.1-0 ; 
       root-extru49.1-0 ; 
       root-extru50.1-0 ; 
       root-extru51.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.3-0 ; 
       root-null18_1.3-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-null22.1-0 ; 
       root-null23.1-0 ; 
       root-null25.1-0 ; 
       root-root.1-0 ROOT ; 
       root-sphere9.5-0 ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
       root-SS_38.1-0 ; 
       root-SS_39.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
       root-tetra3.1-0 ; 
       root-tetra4.1-0 ; 
       root-tetra5.1-0 ; 
       root-tetra6.1-0 ; 
       root-tetra7.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/bgrnd53 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       both-ss305-electronic.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       purpmeteor-t2d1.3-0 ; 
       ss305_electronic-t2d58.2-0 ; 
       ss305_electronic-t2d59.1-0 ; 
       ss305_electronic-t2d60.1-0 ; 
       ss305_electronic-t2d61.1-0 ; 
       ss305_electronic-t2d62.1-0 ; 
       ss305_electronic-t2d63.1-0 ; 
       ss305_electronic-t2d64.1-0 ; 
       ss305_elect_station-t2d52.4-0 ; 
       ss305_elect_station-t2d53.4-0 ; 
       ss305_elect_station-t2d54.4-0 ; 
       ss305_elect_station-t2d55.4-0 ; 
       ss305_elect_station-t2d56.4-0 ; 
       ss305_elect_station-t2d57.4-0 ; 
       ss305_elect_station-t2d7.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       51 55 110 ; 
       76 51 110 ; 
       77 51 110 ; 
       78 51 110 ; 
       79 51 110 ; 
       82 48 110 ; 
       83 48 110 ; 
       84 48 110 ; 
       52 55 110 ; 
       85 52 110 ; 
       86 52 110 ; 
       87 52 110 ; 
       88 52 110 ; 
       89 52 110 ; 
       90 52 110 ; 
       91 52 110 ; 
       92 52 110 ; 
       53 55 110 ; 
       93 53 110 ; 
       94 53 110 ; 
       95 53 110 ; 
       96 53 110 ; 
       97 53 110 ; 
       98 53 110 ; 
       99 53 110 ; 
       100 53 110 ; 
       10 11 110 ; 
       0 11 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       6 11 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       47 55 110 ; 
       48 55 110 ; 
       68 47 110 ; 
       69 47 110 ; 
       80 48 110 ; 
       70 47 110 ; 
       81 48 110 ; 
       71 47 110 ; 
       72 51 110 ; 
       73 51 110 ; 
       74 51 110 ; 
       75 51 110 ; 
       12 17 110 ; 
       13 17 110 ; 
       14 17 110 ; 
       15 23 110 ; 
       16 20 110 ; 
       17 20 110 ; 
       18 17 110 ; 
       19 20 110 ; 
       20 55 110 ; 
       21 17 110 ; 
       22 50 110 ; 
       23 50 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       26 22 110 ; 
       27 17 110 ; 
       28 20 110 ; 
       29 22 110 ; 
       30 22 110 ; 
       31 17 110 ; 
       32 22 110 ; 
       34 23 110 ; 
       35 55 110 ; 
       36 17 110 ; 
       37 17 110 ; 
       38 17 110 ; 
       39 17 110 ; 
       40 22 110 ; 
       46 35 110 ; 
       49 22 110 ; 
       50 55 110 ; 
       55 54 110 ; 
       56 35 110 ; 
       59 35 110 ; 
       61 35 110 ; 
       62 35 110 ; 
       64 35 110 ; 
       67 35 110 ; 
       101 23 110 ; 
       102 23 110 ; 
       103 23 110 ; 
       104 22 110 ; 
       105 12 110 ; 
       106 12 110 ; 
       107 12 110 ; 
       109 35 110 ; 
       33 55 110 ; 
       41 33 110 ; 
       42 33 110 ; 
       43 33 110 ; 
       44 33 110 ; 
       45 33 110 ; 
       57 33 110 ; 
       58 33 110 ; 
       60 33 110 ; 
       63 33 110 ; 
       65 33 110 ; 
       66 33 110 ; 
       108 33 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       76 10 300 ; 
       77 11 300 ; 
       78 12 300 ; 
       79 13 300 ; 
       82 14 300 ; 
       83 15 300 ; 
       84 16 300 ; 
       85 17 300 ; 
       86 18 300 ; 
       87 19 300 ; 
       88 20 300 ; 
       89 21 300 ; 
       90 22 300 ; 
       91 23 300 ; 
       92 24 300 ; 
       93 25 300 ; 
       94 26 300 ; 
       95 27 300 ; 
       96 28 300 ; 
       97 29 300 ; 
       98 30 300 ; 
       99 31 300 ; 
       100 32 300 ; 
       68 43 300 ; 
       69 43 300 ; 
       80 2 300 ; 
       70 43 300 ; 
       81 1 300 ; 
       71 43 300 ; 
       72 42 300 ; 
       73 42 300 ; 
       74 42 300 ; 
       75 42 300 ; 
       32 36 300 ; 
       32 37 300 ; 
       32 38 300 ; 
       34 33 300 ; 
       34 34 300 ; 
       34 35 300 ; 
       35 7 300 ; 
       35 8 300 ; 
       35 9 300 ; 
       36 39 300 ; 
       37 3 300 ; 
       55 0 300 ; 
       56 40 300 ; 
       59 40 300 ; 
       61 40 300 ; 
       62 40 300 ; 
       64 40 300 ; 
       67 40 300 ; 
       33 4 300 ; 
       33 5 300 ; 
       33 6 300 ; 
       57 41 300 ; 
       58 41 300 ; 
       60 41 300 ; 
       63 41 300 ; 
       65 41 300 ; 
       66 41 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       55 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       33 8 401 ; 
       34 9 401 ; 
       35 10 401 ; 
       36 11 401 ; 
       37 12 401 ; 
       38 13 401 ; 
       39 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000011 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       51 SCHEM 135.612 -13.84001 0 MPRFLG 0 ; 
       76 SCHEM 139.362 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       77 SCHEM 134.362 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       78 SCHEM 129.362 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       79 SCHEM 144.362 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       82 SCHEM 119.362 -15.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 121.862 -15.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 124.362 -15.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 93.11201 -13.84001 0 MPRFLG 0 ; 
       85 SCHEM 84.36201 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       86 SCHEM 86.86201 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       87 SCHEM 89.36201 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       88 SCHEM 91.86201 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 61.86201 -9.840007 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       89 SCHEM 94.36201 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       90 SCHEM 96.86201 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       91 SCHEM 99.36201 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       92 SCHEM 101.862 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 155.612 -13.84001 0 MPRFLG 0 ; 
       93 SCHEM 146.862 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       94 SCHEM 149.362 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       95 SCHEM 151.862 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       96 SCHEM 154.362 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       97 SCHEM 156.862 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       98 SCHEM 159.362 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       99 SCHEM 161.862 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       100 SCHEM 164.362 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 31.25571 2.306587 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 43.75571 0.3065872 0 MPRFLG 0 ; 
       0 SCHEM 18.75571 0.3065872 0 MPRFLG 0 ; 
       1 SCHEM 21.25571 0.3065872 0 MPRFLG 0 ; 
       2 SCHEM 23.75571 0.3065872 0 MPRFLG 0 ; 
       3 SCHEM 26.25571 0.3065872 0 MPRFLG 0 ; 
       4 SCHEM 28.75571 0.3065872 0 MPRFLG 0 ; 
       5 SCHEM 31.25571 0.3065872 0 MPRFLG 0 ; 
       6 SCHEM 33.75571 0.3065872 0 MPRFLG 0 ; 
       7 SCHEM 36.25571 0.3065872 0 MPRFLG 0 ; 
       8 SCHEM 38.75571 0.3065872 0 MPRFLG 0 ; 
       9 SCHEM 41.25571 0.3065872 0 MPRFLG 0 ; 
       47 SCHEM 108.112 -13.84001 0 MPRFLG 0 ; 
       48 SCHEM 119.362 -13.84001 0 MPRFLG 0 ; 
       68 SCHEM 104.362 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 106.862 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       80 SCHEM 114.362 -15.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 109.362 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       81 SCHEM 116.862 -15.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 111.862 -15.84001 0 WIRECOL 2 7 MPRFLG 0 ; 
       72 SCHEM 141.862 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 136.862 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 131.862 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 126.862 -15.84001 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 14.36201 -17.84001 0 MPRFLG 0 ; 
       13 SCHEM 19.36201 -17.84001 0 MPRFLG 0 ; 
       14 SCHEM 24.36201 -17.84001 0 MPRFLG 0 ; 
       15 SCHEM 74.36201 -17.84001 0 MPRFLG 0 ; 
       16 SCHEM 44.36201 -15.84001 0 MPRFLG 0 ; 
       17 SCHEM 26.86201 -15.84001 0 MPRFLG 0 ; 
       18 SCHEM 29.36201 -17.84001 0 MPRFLG 0 ; 
       19 SCHEM 9.362011 -15.84001 0 MPRFLG 0 ; 
       20 SCHEM 28.11201 -13.84001 0 MPRFLG 0 ; 
       21 SCHEM 31.86201 -17.84001 0 MPRFLG 0 ; 
       22 SCHEM 59.36201 -15.84001 0 MPRFLG 0 ; 
       23 SCHEM 76.86201 -15.84001 0 MPRFLG 0 ; 
       24 SCHEM 61.86201 -17.84001 0 MPRFLG 0 ; 
       25 SCHEM 56.86201 -17.84001 0 MPRFLG 0 ; 
       26 SCHEM 64.36201 -17.84001 0 MPRFLG 0 ; 
       27 SCHEM 41.86201 -17.84001 0 MPRFLG 0 ; 
       28 SCHEM 46.86201 -15.84001 0 MPRFLG 0 ; 
       29 SCHEM 49.36201 -17.84001 0 MPRFLG 0 ; 
       30 SCHEM 59.36201 -17.84001 0 MPRFLG 0 ; 
       31 SCHEM 21.86201 -17.84001 0 MPRFLG 0 ; 
       32 SCHEM 51.86201 -17.84001 0 MPRFLG 0 ; 
       34 SCHEM 71.86201 -17.84001 0 MPRFLG 0 ; 
       35 SCHEM -1.887989 -13.84001 0 MPRFLG 0 ; 
       36 SCHEM 34.36201 -17.84001 0 MPRFLG 0 ; 
       37 SCHEM 39.36201 -17.84001 0 MPRFLG 0 ; 
       38 SCHEM 26.86201 -17.84001 0 MPRFLG 0 ; 
       39 SCHEM 36.86201 -17.84001 0 MPRFLG 0 ; 
       40 SCHEM 66.86201 -17.84001 0 MPRFLG 0 ; 
       46 SCHEM 6.862011 -15.84001 0 WIRECOL 9 7 MPRFLG 0 ; 
       49 SCHEM 54.36201 -17.84001 0 MPRFLG 0 ; 
       50 SCHEM 65.61201 -13.84001 0 MPRFLG 0 ; 
       55 SCHEM 61.86201 -11.84001 0 MPRFLG 0 ; 
       56 SCHEM -5.637987 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM -10.63799 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM -8.137987 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 1.862011 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM -3.137987 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM -0.6379888 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       101 SCHEM 76.86201 -17.84001 0 MPRFLG 0 ; 
       102 SCHEM 79.36201 -17.84001 0 MPRFLG 0 ; 
       103 SCHEM 81.86201 -17.84001 0 MPRFLG 0 ; 
       104 SCHEM 69.36201 -17.84001 0 MPRFLG 0 ; 
       105 SCHEM 11.86201 -19.84001 0 MPRFLG 0 ; 
       106 SCHEM 14.36201 -19.84001 0 MPRFLG 0 ; 
       107 SCHEM 16.86201 -19.84001 0 MPRFLG 0 ; 
       109 SCHEM 4.362011 -15.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM -26.88799 -13.84001 0 MPRFLG 0 ; 
       41 SCHEM -25.63799 -15.84001 0 WIRECOL 9 7 MPRFLG 0 ; 
       42 SCHEM -20.63799 -15.84001 0 WIRECOL 9 7 MPRFLG 0 ; 
       43 SCHEM -23.13799 -15.84001 0 WIRECOL 9 7 MPRFLG 0 ; 
       44 SCHEM -15.63799 -15.84001 0 WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM -18.13799 -15.84001 0 WIRECOL 9 7 MPRFLG 0 ; 
       57 SCHEM -35.63799 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM -40.63799 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM -38.13799 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM -28.13799 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM -33.13799 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM -30.63799 -15.84001 0 WIRECOL 3 7 MPRFLG 0 ; 
       108 SCHEM -13.13799 -15.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 191.3949 -5.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 141.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 138.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 172.4185 -30.29415 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -11.90922 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -11.90922 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -11.90922 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.73724 -7.815687 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.78207 -7.543127 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 13.10309 -6.498297 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM -8.959984 -15.81569 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM -7.915154 -15.54313 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -12.59414 -14.4983 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 76.39492 -11.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 76.39492 -11.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 76.39492 -11.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 89.23275 -26.29415 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM -22.13351 -20.54997 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM -36.59393 -14.41 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 166.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 128.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 163.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 158.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 153.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 168.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 143.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 146.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 148.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 108.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 111.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 113.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 116.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 118.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 121.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 123.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 126.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 171.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 173.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 176.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 178.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 181.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 183.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 186.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 188.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 191.3949 -5.25998 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 172.4185 -32.29415 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -13.90922 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -13.90922 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -13.90922 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.73724 -9.815687 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.78207 -9.543127 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 13.10309 -8.498297 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -8.959984 -17.81569 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -7.915154 -17.54313 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -12.59414 -16.4983 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 76.39492 -13.25998 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 76.39492 -13.25998 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 76.39492 -13.25998 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 89.23275 -28.29415 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
