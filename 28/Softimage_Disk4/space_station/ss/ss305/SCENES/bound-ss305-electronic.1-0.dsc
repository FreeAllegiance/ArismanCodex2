SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.85-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bound07.1-0 ; 
       bounding_model-bound08.1-0 ; 
       bounding_model-bound09.1-0 ; 
       bounding_model-bound10.1-0 ; 
       bounding_model-bound11.1-0 ; 
       bounding_model-bounding_model.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOUND-ss305-electronic.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 8 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000011 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 11.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
