SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.92-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bound07.1-0 ; 
       bounding_model-bound08.1-0 ; 
       bounding_model-bound09.1-0 ; 
       bounding_model-bound10.1-0 ; 
       bounding_model-bound11.1-0 ; 
       bounding_model-bound12.1-0 ; 
       bounding_model-bound13.1-0 ; 
       bounding_model-bound14.1-0 ; 
       bounding_model-bound15.1-0 ; 
       bounding_model-bounding_model.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bound-ss305-electronic.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 15 110 ; 
       14 15 110 ; 
       0 15 110 ; 
       4 15 110 ; 
       5 15 110 ; 
       6 15 110 ; 
       7 15 110 ; 
       8 15 110 ; 
       9 15 110 ; 
       10 15 110 ; 
       1 15 110 ; 
       2 15 110 ; 
       3 15 110 ; 
       11 15 110 ; 
       12 15 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 35 -2 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 20 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       11 SCHEM 30 -2 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
