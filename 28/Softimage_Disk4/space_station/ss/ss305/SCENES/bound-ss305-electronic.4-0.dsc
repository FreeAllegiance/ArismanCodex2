SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.94-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       ss305_electronic-mat118.1-0 ; 
       ss305_electronic-mat119.1-0 ; 
       ss305_electronic-mat120.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bound07.1-0 ; 
       bounding_model-bound08.1-0 ; 
       bounding_model-bound09.1-0 ; 
       bounding_model-bound10.1-0 ; 
       bounding_model-bound11.1-0 ; 
       bounding_model-bound12.1-0 ; 
       bounding_model-bound13.1-0 ; 
       bounding_model-bound14.1-0 ; 
       bounding_model-bound15.1-0 ; 
       bounding_model-bound16.1-0 ; 
       bounding_model-bounding_model.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss305/PICTURES/ss305 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bound-ss305-electronic.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       ss305_electronic-t2d112.1-0 ; 
       ss305_electronic-t2d113.1-0 ; 
       ss305_electronic-t2d114.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 16 110 ; 
       2 16 110 ; 
       3 16 110 ; 
       4 16 110 ; 
       5 16 110 ; 
       6 16 110 ; 
       7 16 110 ; 
       8 16 110 ; 
       9 16 110 ; 
       10 16 110 ; 
       11 16 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       15 0 300 ; 
       15 1 300 ; 
       15 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 30 -2 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 35 -2 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 21.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 40 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 39 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
