SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.7-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       purpmeteor-default9.2-0 ; 
       ss301_garrison-white_strobe1_33.2-0 ; 
       ss301_garrison-white_strobe1_34.2-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_electronic-mat59.1-0 ; 
       ss305_electronic-mat60.1-0 ; 
       ss305_electronic-mat61.1-0 ; 
       ss305_electronic-mat62.1-0 ; 
       ss305_electronic-mat63.1-0 ; 
       ss305_electronic-mat64.1-0 ; 
       ss305_electronic-white_strobe1_35.1-0 ; 
       ss305_electronic-white_strobe1_36.1-0 ; 
       ss305_electronic-white_strobe1_37.1-0 ; 
       ss305_electronic-white_strobe1_38.1-0 ; 
       ss305_electronic-white_strobe1_39.1-0 ; 
       ss305_electronic-white_strobe1_40.1-0 ; 
       ss305_electronic-white_strobe1_41.1-0 ; 
       ss305_electronic-white_strobe1_42.1-0 ; 
       ss305_electronic-white_strobe1_43.1-0 ; 
       ss305_electronic-white_strobe1_44.1-0 ; 
       ss305_electronic-white_strobe1_45.1-0 ; 
       ss305_electronic-white_strobe1_46.1-0 ; 
       ss305_electronic-white_strobe1_47.1-0 ; 
       ss305_electronic-white_strobe1_48.1-0 ; 
       ss305_electronic-white_strobe1_49.1-0 ; 
       ss305_electronic-white_strobe1_50.1-0 ; 
       ss305_electronic-white_strobe1_51.1-0 ; 
       ss305_electronic-white_strobe1_52.1-0 ; 
       ss305_electronic-white_strobe1_53.1-0 ; 
       ss305_electronic-white_strobe1_54.1-0 ; 
       ss305_electronic-white_strobe1_55.1-0 ; 
       ss305_electronic-white_strobe1_56.1-0 ; 
       ss305_electronic-white_strobe1_57.1-0 ; 
       ss305_elect_station-mat52.3-0 ; 
       ss305_elect_station-mat53.3-0 ; 
       ss305_elect_station-mat54.3-0 ; 
       ss305_elect_station-mat55.3-0 ; 
       ss305_elect_station-mat56.3-0 ; 
       ss305_elect_station-mat57.3-0 ; 
       ss305_elect_station-mat7.3-0 ; 
       ss305_elect_station-white_strobe1_25.3-0 ; 
       ss305_elect_station-white_strobe1_31.3-0 ; 
       ss305_elect_station-white_strobe1_5.3-0 ; 
       ss305_elect_station-white_strobe1_9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 98     
       root-cube10.1-0 ; 
       root-cube12.1-0 ; 
       root-cube13.1-0 ; 
       root-cube13_1.1-0 ; 
       root-cube13_2.1-0 ; 
       root-cube18.2-0 ; 
       root-cube19.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube23.1-0 ; 
       root-cube25.1-0 ; 
       root-cube26.1-0 ; 
       root-cube26_1.1-0 ; 
       root-cube26_2.1-0 ; 
       root-cube27.1-0 ; 
       root-cube28.1-0 ; 
       root-cube3.1-0 ; 
       root-cube3_1.1-0 ; 
       root-cube4.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_8_1.1-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-east_bay_11_9_1.1-0 ; 
       root-extru44.1-0 ; 
       root-extru48.1-0 ; 
       root-extru49.1-0 ; 
       root-extru50.1-0 ; 
       root-extru51.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18.3-0 ; 
       root-null18_1.3-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-null22.1-0 ; 
       root-null23.1-0 ; 
       root-null25.1-0 ; 
       root-root.1-0 ROOT ; 
       root-sphere9.5-0 ; 
       root-SS_11.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_13_3.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_3.1-0 ; 
       root-SS_23.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_24.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_3.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
       root-SS_38.1-0 ; 
       root-SS_39.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
       root-tetra3.1-0 ; 
       root-tetra4.1-0 ; 
       root-tetra5.1-0 ; 
       root-tetra6.1-0 ; 
       root-tetra7.1-0 ; 
       root-turwepemt2.1-0 ; 
       root-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/bgrnd53 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss305-electronic.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       purpmeteor-t2d1.3-0 ; 
       ss305_electronic-t2d58.2-0 ; 
       ss305_electronic-t2d59.1-0 ; 
       ss305_electronic-t2d60.1-0 ; 
       ss305_electronic-t2d61.1-0 ; 
       ss305_electronic-t2d62.1-0 ; 
       ss305_electronic-t2d63.1-0 ; 
       ss305_electronic-t2d64.1-0 ; 
       ss305_elect_station-t2d52.4-0 ; 
       ss305_elect_station-t2d53.4-0 ; 
       ss305_elect_station-t2d54.4-0 ; 
       ss305_elect_station-t2d55.4-0 ; 
       ss305_elect_station-t2d56.4-0 ; 
       ss305_elect_station-t2d57.4-0 ; 
       ss305_elect_station-t2d7.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       39 43 110 ; 
       64 39 110 ; 
       65 39 110 ; 
       66 39 110 ; 
       67 39 110 ; 
       70 36 110 ; 
       71 36 110 ; 
       72 36 110 ; 
       40 43 110 ; 
       73 40 110 ; 
       74 40 110 ; 
       75 40 110 ; 
       76 40 110 ; 
       77 40 110 ; 
       78 40 110 ; 
       79 40 110 ; 
       80 40 110 ; 
       41 43 110 ; 
       81 41 110 ; 
       82 41 110 ; 
       83 41 110 ; 
       84 41 110 ; 
       85 41 110 ; 
       86 41 110 ; 
       87 41 110 ; 
       88 41 110 ; 
       35 43 110 ; 
       36 43 110 ; 
       56 35 110 ; 
       57 35 110 ; 
       68 36 110 ; 
       58 35 110 ; 
       69 36 110 ; 
       59 35 110 ; 
       60 39 110 ; 
       61 39 110 ; 
       62 39 110 ; 
       63 39 110 ; 
       0 5 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       3 11 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 5 110 ; 
       7 8 110 ; 
       8 43 110 ; 
       9 5 110 ; 
       10 38 110 ; 
       11 38 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 5 110 ; 
       16 8 110 ; 
       17 10 110 ; 
       18 10 110 ; 
       19 5 110 ; 
       20 10 110 ; 
       22 11 110 ; 
       23 43 110 ; 
       24 5 110 ; 
       25 5 110 ; 
       26 5 110 ; 
       27 5 110 ; 
       28 10 110 ; 
       34 23 110 ; 
       37 10 110 ; 
       38 43 110 ; 
       43 42 110 ; 
       44 23 110 ; 
       47 23 110 ; 
       49 23 110 ; 
       50 23 110 ; 
       52 23 110 ; 
       55 23 110 ; 
       89 11 110 ; 
       90 11 110 ; 
       91 11 110 ; 
       92 10 110 ; 
       93 0 110 ; 
       94 0 110 ; 
       95 0 110 ; 
       97 23 110 ; 
       21 43 110 ; 
       29 21 110 ; 
       30 21 110 ; 
       31 21 110 ; 
       32 21 110 ; 
       33 21 110 ; 
       45 21 110 ; 
       46 21 110 ; 
       48 21 110 ; 
       51 21 110 ; 
       53 21 110 ; 
       54 21 110 ; 
       96 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       64 10 300 ; 
       65 11 300 ; 
       66 12 300 ; 
       67 13 300 ; 
       70 14 300 ; 
       71 15 300 ; 
       72 16 300 ; 
       73 17 300 ; 
       74 18 300 ; 
       75 19 300 ; 
       76 20 300 ; 
       77 21 300 ; 
       78 22 300 ; 
       79 23 300 ; 
       80 24 300 ; 
       81 25 300 ; 
       82 26 300 ; 
       83 27 300 ; 
       84 28 300 ; 
       85 29 300 ; 
       86 30 300 ; 
       87 31 300 ; 
       88 32 300 ; 
       56 43 300 ; 
       57 43 300 ; 
       68 2 300 ; 
       58 43 300 ; 
       69 1 300 ; 
       59 43 300 ; 
       60 42 300 ; 
       61 42 300 ; 
       62 42 300 ; 
       63 42 300 ; 
       20 36 300 ; 
       20 37 300 ; 
       20 38 300 ; 
       22 33 300 ; 
       22 34 300 ; 
       22 35 300 ; 
       23 7 300 ; 
       23 8 300 ; 
       23 9 300 ; 
       24 39 300 ; 
       25 3 300 ; 
       43 0 300 ; 
       44 40 300 ; 
       47 40 300 ; 
       49 40 300 ; 
       50 40 300 ; 
       52 40 300 ; 
       55 40 300 ; 
       21 4 300 ; 
       21 5 300 ; 
       21 6 300 ; 
       45 41 300 ; 
       46 41 300 ; 
       48 41 300 ; 
       51 41 300 ; 
       53 41 300 ; 
       54 41 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       43 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       33 8 401 ; 
       34 9 401 ; 
       35 10 401 ; 
       36 11 401 ; 
       37 12 401 ; 
       38 13 401 ; 
       39 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000011 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       39 SCHEM 122.529 -13.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       64 SCHEM 126.279 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       65 SCHEM 121.279 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       66 SCHEM 116.279 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       67 SCHEM 131.279 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       70 SCHEM 106.279 -15.64174 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       71 SCHEM 108.779 -15.64174 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       72 SCHEM 111.279 -15.64174 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 80.02895 -13.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       73 SCHEM 71.27895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       74 SCHEM 73.77895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       75 SCHEM 76.27895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       76 SCHEM 78.77895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 29.19731 -8.381762 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       77 SCHEM 81.27895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       78 SCHEM 83.77895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       79 SCHEM 86.27895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       80 SCHEM 88.77895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 142.529 -13.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       81 SCHEM 133.779 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       82 SCHEM 136.279 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       83 SCHEM 138.779 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       84 SCHEM 141.279 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       85 SCHEM 143.779 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       86 SCHEM 146.279 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       87 SCHEM 148.779 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       88 SCHEM 151.279 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 95.02895 -13.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 106.279 -13.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 91.27895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 93.77895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       68 SCHEM 101.279 -15.64174 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 96.27895 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       69 SCHEM 103.779 -15.64174 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 98.77896 -15.64174 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 128.779 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       61 SCHEM 123.779 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       62 SCHEM 118.779 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       63 SCHEM 113.779 -15.64174 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 1.278951 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 6.278951 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 11.27895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 61.27896 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 31.27896 -15.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 13.77895 -15.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 16.27895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM -3.721049 -15.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 15.02895 -13.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 18.77895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 46.27895 -15.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 63.77896 -15.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 48.77895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 43.77895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 51.27895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 28.77896 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 33.77895 -15.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 36.27896 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 46.27895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 8.778951 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 38.77896 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 58.77896 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM -14.97105 -12.84241 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 21.27895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 26.27896 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 13.77895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 23.77895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 53.77895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM -6.22105 -14.84241 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 41.27895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 52.52895 -13.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 29.47616 -11.64174 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM -18.72105 -14.84241 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM -23.72105 -14.84241 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM -21.22105 -14.84241 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM -11.22105 -14.84241 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM -16.22105 -14.84241 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM -13.72105 -14.84241 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       89 SCHEM 63.77896 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       90 SCHEM 66.27895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       91 SCHEM 68.77895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       92 SCHEM 56.27895 -17.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       93 SCHEM -1.221049 -19.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       94 SCHEM 1.278951 -19.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       95 SCHEM 3.778951 -19.64174 0 DISPLAY 2 2 MPRFLG 0 ; 
       97 SCHEM -8.72105 -14.84241 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM -22.36597 -18.29098 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM -21.11597 -20.29098 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM -16.11597 -20.29098 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM -18.61597 -20.29098 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM -11.11597 -20.29098 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM -13.61597 -20.29098 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM -31.11597 -20.29098 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM -36.11597 -20.29098 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM -33.61597 -20.29098 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM -23.61597 -20.29098 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM -28.61597 -20.29098 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM -26.11597 -20.29098 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       96 SCHEM -8.615971 -20.29098 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 191.3949 -5.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 141.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 138.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 172.4185 -30.29415 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -11.90922 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -11.90922 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -11.90922 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.73724 -7.815687 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.78207 -7.543127 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 13.10309 -6.498297 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM -8.959984 -15.81569 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM -7.915154 -15.54313 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -12.59414 -14.4983 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 76.39492 -11.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 76.39492 -11.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 76.39492 -11.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 89.23275 -26.29415 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM -22.13351 -20.54997 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM -36.59393 -14.41 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 166.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 128.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 163.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 158.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 153.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 168.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 143.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 146.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 148.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 108.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 111.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 113.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 116.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 118.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 121.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 123.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 126.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 171.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 173.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 176.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 178.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 181.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 183.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 186.3949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 188.8949 -9.25998 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 191.3949 -5.25998 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 172.4185 -32.29415 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -13.90922 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -13.90922 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -13.90922 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.73724 -9.815687 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.78207 -9.543127 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 13.10309 -8.498297 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -8.959984 -17.81569 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -7.915154 -17.54313 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -12.59414 -16.4983 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 76.39492 -13.25998 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 76.39492 -13.25998 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 76.39492 -13.25998 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 89.23275 -28.29415 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
