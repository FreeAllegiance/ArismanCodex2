SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.91-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 54     
       purpmeteor-mat98.2-0 ; 
       ss305_electronic-mat100.2-0 ; 
       ss305_electronic-mat101.2-0 ; 
       ss305_electronic-mat102.3-0 ; 
       ss305_electronic-mat103.2-0 ; 
       ss305_electronic-mat104.2-0 ; 
       ss305_electronic-mat105.2-0 ; 
       ss305_electronic-mat106.2-0 ; 
       ss305_electronic-mat107.2-0 ; 
       ss305_electronic-mat108.2-0 ; 
       ss305_electronic-mat109.3-0 ; 
       ss305_electronic-mat110.2-0 ; 
       ss305_electronic-mat111.1-0 ; 
       ss305_electronic-mat112.1-0 ; 
       ss305_electronic-mat113.1-0 ; 
       ss305_electronic-mat114.1-0 ; 
       ss305_electronic-mat66.2-0 ; 
       ss305_electronic-mat67.4-0 ; 
       ss305_electronic-mat68.2-0 ; 
       ss305_electronic-mat69.3-0 ; 
       ss305_electronic-mat70.3-0 ; 
       ss305_electronic-mat72.3-0 ; 
       ss305_electronic-mat73.3-0 ; 
       ss305_electronic-mat74.3-0 ; 
       ss305_electronic-mat75.3-0 ; 
       ss305_electronic-mat76.3-0 ; 
       ss305_electronic-mat77.5-0 ; 
       ss305_electronic-mat78.3-0 ; 
       ss305_electronic-mat79.3-0 ; 
       ss305_electronic-mat80.3-0 ; 
       ss305_electronic-mat81.4-0 ; 
       ss305_electronic-mat82.4-0 ; 
       ss305_electronic-mat83.4-0 ; 
       ss305_electronic-mat84.3-0 ; 
       ss305_electronic-mat85.4-0 ; 
       ss305_electronic-mat86.3-0 ; 
       ss305_electronic-mat87.5-0 ; 
       ss305_electronic-mat88.3-0 ; 
       ss305_electronic-mat89.4-0 ; 
       ss305_electronic-mat91.3-0 ; 
       ss305_electronic-mat92.3-0 ; 
       ss305_electronic-mat93.4-0 ; 
       ss305_electronic-mat94.3-0 ; 
       ss305_electronic-mat95.3-0 ; 
       ss305_electronic-mat96.3-0 ; 
       ss305_electronic-mat97.3-0 ; 
       ss305_electronic-mat99.2-0 ; 
       ss305_elect_station-mat52.5-0 ; 
       ss305_elect_station-mat53.5-0 ; 
       ss305_elect_station-mat54.5-0 ; 
       ss305_elect_station-mat55_1.1-0 ; 
       ss305_elect_station-mat56_1.1-0 ; 
       ss305_elect_station-mat57_1.1-0 ; 
       ss305_elect_station-mat7.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       root-cube10.1-0 ; 
       root-cube12.1-0 ; 
       root-cube13.1-0 ; 
       root-cube13_1.1-0 ; 
       root-cube13_2.1-0 ; 
       root-cube18_1.2-0 ; 
       root-cube19.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube23.1-0 ; 
       root-cube25.1-0 ; 
       root-cube26.1-0 ; 
       root-cube26_1.1-0 ; 
       root-cube26_2.1-0 ; 
       root-cube27.1-0 ; 
       root-cube28.1-0 ; 
       root-cube3_1.1-0 ; 
       root-cube3_2.1-0 ; 
       root-cube4.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-extru44.1-0 ; 
       root-extru49.1-0 ; 
       root-extru50.1-0 ; 
       root-extru51.1-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-root.62-0 ROOT ; 
       root-sphere9.5-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
       root-tetra3.1-0 ; 
       root-tetra4.1-0 ; 
       root-tetra5.1-0 ; 
       root-tetra6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss305/PICTURES/beltersbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss305/PICTURES/bgrnd53 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss305/PICTURES/ss305 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss305-electronic.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       purpmeteor-t2d1.10-0 ; 
       ss305_electronic-t2d100.6-0 ; 
       ss305_electronic-t2d101.5-0 ; 
       ss305_electronic-t2d102.3-0 ; 
       ss305_electronic-t2d103.3-0 ; 
       ss305_electronic-t2d104.3-0 ; 
       ss305_electronic-t2d105.5-0 ; 
       ss305_electronic-t2d106.3-0 ; 
       ss305_electronic-t2d107.3-0 ; 
       ss305_electronic-t2d108.4-0 ; 
       ss305_electronic-t2d65.9-0 ; 
       ss305_electronic-t2d66.8-0 ; 
       ss305_electronic-t2d67.8-0 ; 
       ss305_electronic-t2d68.7-0 ; 
       ss305_electronic-t2d69.7-0 ; 
       ss305_electronic-t2d70.6-0 ; 
       ss305_electronic-t2d71.9-0 ; 
       ss305_electronic-t2d72.8-0 ; 
       ss305_electronic-t2d73.7-0 ; 
       ss305_electronic-t2d74.7-0 ; 
       ss305_electronic-t2d75.8-0 ; 
       ss305_electronic-t2d76.7-0 ; 
       ss305_electronic-t2d77.6-0 ; 
       ss305_electronic-t2d78.6-0 ; 
       ss305_electronic-t2d79.7-0 ; 
       ss305_electronic-t2d80.7-0 ; 
       ss305_electronic-t2d81.7-0 ; 
       ss305_electronic-t2d82.7-0 ; 
       ss305_electronic-t2d83.8-0 ; 
       ss305_electronic-t2d84.7-0 ; 
       ss305_electronic-t2d85.8-0 ; 
       ss305_electronic-t2d86.7-0 ; 
       ss305_electronic-t2d87.6-0 ; 
       ss305_electronic-t2d88.6-0 ; 
       ss305_electronic-t2d89.7-0 ; 
       ss305_electronic-t2d90.6-0 ; 
       ss305_electronic-t2d91.6-0 ; 
       ss305_electronic-t2d92.6-0 ; 
       ss305_electronic-t2d93.8-0 ; 
       ss305_electronic-t2d94.5-0 ; 
       ss305_electronic-t2d95.6-0 ; 
       ss305_electronic-t2d96.5-0 ; 
       ss305_electronic-t2d97.5-0 ; 
       ss305_electronic-t2d98.5-0 ; 
       ss305_electronic-t2d99.6-0 ; 
       ss305_elect_station-t2d52.11-0 ; 
       ss305_elect_station-t2d53.11-0 ; 
       ss305_elect_station-t2d54.11-0 ; 
       ss305_elect_station-t2d55_1.3-0 ; 
       ss305_elect_station-t2d56_1.3-0 ; 
       ss305_elect_station-t2d57_1.3-0 ; 
       ss305_elect_station-t2d7.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       3 11 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 5 110 ; 
       7 8 110 ; 
       8 29 110 ; 
       9 5 110 ; 
       10 27 110 ; 
       11 27 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 5 110 ; 
       16 8 110 ; 
       17 10 110 ; 
       18 13 110 ; 
       19 5 110 ; 
       20 10 110 ; 
       21 11 110 ; 
       22 5 110 ; 
       23 5 110 ; 
       24 5 110 ; 
       25 10 110 ; 
       26 10 110 ; 
       27 29 110 ; 
       29 28 110 ; 
       30 11 110 ; 
       31 11 110 ; 
       32 11 110 ; 
       33 10 110 ; 
       34 0 110 ; 
       35 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 38 300 ; 
       0 1 300 ; 
       1 42 300 ; 
       2 41 300 ; 
       2 13 300 ; 
       3 26 300 ; 
       3 10 300 ; 
       3 12 300 ; 
       4 32 300 ; 
       5 34 300 ; 
       5 46 300 ; 
       6 36 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       7 31 300 ; 
       7 15 300 ; 
       8 30 300 ; 
       8 14 300 ; 
       9 37 300 ; 
       10 17 300 ; 
       10 5 300 ; 
       10 7 300 ; 
       11 25 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       12 20 300 ; 
       13 19 300 ; 
       13 6 300 ; 
       14 21 300 ; 
       15 45 300 ; 
       15 2 300 ; 
       16 33 300 ; 
       17 22 300 ; 
       18 11 300 ; 
       19 35 300 ; 
       20 50 300 ; 
       20 51 300 ; 
       20 52 300 ; 
       21 47 300 ; 
       21 48 300 ; 
       21 49 300 ; 
       22 53 300 ; 
       23 43 300 ; 
       24 44 300 ; 
       25 23 300 ; 
       26 18 300 ; 
       27 16 300 ; 
       29 0 300 ; 
       30 29 300 ; 
       31 27 300 ; 
       32 28 300 ; 
       33 24 300 ; 
       34 40 300 ; 
       35 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       29 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 13 401 ; 
       2 15 401 ; 
       3 17 401 ; 
       4 18 401 ; 
       5 21 401 ; 
       6 23 401 ; 
       7 24 401 ; 
       8 26 401 ; 
       9 27 401 ; 
       10 29 401 ; 
       11 36 401 ; 
       12 41 401 ; 
       13 44 401 ; 
       14 1 401 ; 
       15 2 401 ; 
       17 20 401 ; 
       19 22 401 ; 
       20 42 401 ; 
       21 43 401 ; 
       22 35 401 ; 
       23 32 401 ; 
       24 6 401 ; 
       25 25 401 ; 
       26 28 401 ; 
       27 5 401 ; 
       28 4 401 ; 
       29 3 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 37 401 ; 
       33 9 401 ; 
       34 10 401 ; 
       35 19 401 ; 
       36 16 401 ; 
       37 39 401 ; 
       38 12 401 ; 
       39 8 401 ; 
       40 7 401 ; 
       41 38 401 ; 
       42 40 401 ; 
       43 34 401 ; 
       44 33 401 ; 
       45 14 401 ; 
       46 11 401 ; 
       47 45 401 ; 
       48 46 401 ; 
       49 47 401 ; 
       50 48 401 ; 
       51 49 401 ; 
       52 50 401 ; 
       53 51 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       1 SCHEM 10 -8 0 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 MPRFLG 0 ; 
       3 SCHEM 60 -8 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 20 -8 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 50 -8 0 MPRFLG 0 ; 
       15 SCHEM 30 -8 0 MPRFLG 0 ; 
       16 SCHEM 35 -6 0 MPRFLG 0 ; 
       17 SCHEM 45 -8 0 MPRFLG 0 ; 
       18 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 25 -8 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 40 -8 0 MPRFLG 0 ; 
       27 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 35 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 35 -2 0 MPRFLG 0 ; 
       30 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 65 -8 0 MPRFLG 0 ; 
       32 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 55 -8 0 MPRFLG 0 ; 
       34 SCHEM 5 -10 0 MPRFLG 0 ; 
       35 SCHEM 7.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 169 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 38.80821 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.05821 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 63.55821 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 37.30821 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 18.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 23.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.05821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 28.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 23.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.05821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 33.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 48.55821 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 37.30821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 43.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 46.05821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.05821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24.80821 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM -18.94179 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.05821 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 13.55821 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.05821 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -8.941788 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 1.058212 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.058212 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM -10.19179 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM -13.94179 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM -16.44179 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM -6.441788 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM -11.44179 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM -3.941788 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 6.058212 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 9.808212 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 35.05821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 36.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 33.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 16.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 13.55821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 15.05821 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 3.558212 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 170.5 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 41.05821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 46.05821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 43.55821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 33.55821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -16.44179 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -13.94179 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 13.55821 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.05821 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM -10.19179 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9.808212 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.058212 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM -8.941788 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 37.30821 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 38.80821 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 23.55821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 48.55821 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 37.30821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24.80821 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM -18.94179 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 31.05821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 6.058212 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM -3.941788 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 23.55821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 21.05821 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.05821 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM -6.441788 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 1.058212 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM -11.44179 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 26.05821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 28.55821 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 34.05821 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 35.55821 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 32.55821 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 15.55821 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 12.55821 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 14.05821 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 2.558212 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
