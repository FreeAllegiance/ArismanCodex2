SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.1-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 47     
       ss100_nulls-extru44.1-0 ROOT ; 
       ss303_ordinance-cube3.1-0 ; 
       ss303_ordinance-sphere1.1-0 ROOT ; 
       ss305_elect_station-cube1.1-0 ; 
       ss305_elect_station-cube2.1-0 ; 
       ss305_elect_station-cube4.1-0 ROOT ; 
       ss305_elect_station-cube5.1-0 ROOT ; 
       ss305_elect_station-cube6.1-0 ROOT ; 
       ss305_elect_station-cube7.1-0 ROOT ; 
       ss305_elect_station-cyl1.1-0 ; 
       ss305_elect_station-east_bay_11_8.1-0 ; 
       ss305_elect_station-east_bay_11_9.1-0 ; 
       ss305_elect_station-garage1A.1-0 ; 
       ss305_elect_station-garage1B.1-0 ; 
       ss305_elect_station-garage1C.1-0 ; 
       ss305_elect_station-garage1D.1-0 ; 
       ss305_elect_station-garage1E.1-0 ; 
       ss305_elect_station-landing_lights_2.1-0 ; 
       ss305_elect_station-landing_lights_3.1-0 ; 
       ss305_elect_station-landing_lights2.1-0 ; 
       ss305_elect_station-landing_lights2_3.1-0 ; 
       ss305_elect_station-launch1.1-0 ; 
       ss305_elect_station-null18.1-0 ; 
       ss305_elect_station-null19.1-0 ROOT ; 
       ss305_elect_station-null20.1-0 ROOT ; 
       ss305_elect_station-SS_11.1-0 ; 
       ss305_elect_station-SS_11_1.1-0 ; 
       ss305_elect_station-SS_13_2.1-0 ; 
       ss305_elect_station-SS_13_3.1-0 ; 
       ss305_elect_station-SS_15_1.1-0 ; 
       ss305_elect_station-SS_15_3.1-0 ; 
       ss305_elect_station-SS_23.1-0 ; 
       ss305_elect_station-SS_23_2.1-0 ; 
       ss305_elect_station-SS_24.1-0 ; 
       ss305_elect_station-SS_24_1.1-0 ; 
       ss305_elect_station-SS_26.1-0 ; 
       ss305_elect_station-SS_26_3.1-0 ; 
       ss305_elect_station-SS_29.1-0 ; 
       ss305_elect_station-SS_30.1-0 ; 
       ss305_elect_station-SS_31.1-0 ; 
       ss305_elect_station-SS_32.1-0 ; 
       ss305_elect_station-SS_33.1-0 ; 
       ss305_elect_station-SS_34.1-0 ; 
       ss305_elect_station-SS_35.1-0 ; 
       ss305_elect_station-SS_36.1-0 ; 
       ss305_elect_station-turwepemt2.1-0 ; 
       ss305_elect_station-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss305-elect_station.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 9 110 ; 
       4 9 110 ; 
       1 2 110 ; 
       9 8 110 ; 
       10 24 110 ; 
       11 24 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 10 110 ; 
       18 11 110 ; 
       19 10 110 ; 
       20 11 110 ; 
       21 11 110 ; 
       22 24 110 ; 
       25 18 110 ; 
       26 17 110 ; 
       27 17 110 ; 
       28 18 110 ; 
       29 17 110 ; 
       30 18 110 ; 
       31 20 110 ; 
       32 19 110 ; 
       33 20 110 ; 
       34 19 110 ; 
       35 19 110 ; 
       36 20 110 ; 
       37 22 110 ; 
       38 22 110 ; 
       39 22 110 ; 
       40 22 110 ; 
       41 23 110 ; 
       42 23 110 ; 
       43 23 110 ; 
       44 23 110 ; 
       45 10 110 ; 
       46 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 3 300 ; 
       10 4 300 ; 
       10 5 300 ; 
       11 0 300 ; 
       11 1 300 ; 
       11 2 300 ; 
       0 6 300 ; 
       25 7 300 ; 
       26 8 300 ; 
       27 8 300 ; 
       28 7 300 ; 
       29 8 300 ; 
       30 7 300 ; 
       31 7 300 ; 
       32 8 300 ; 
       33 7 300 ; 
       34 8 300 ; 
       35 8 300 ; 
       36 7 300 ; 
       37 10 300 ; 
       38 10 300 ; 
       39 10 300 ; 
       40 10 300 ; 
       41 9 300 ; 
       42 9 300 ; 
       43 9 300 ; 
       44 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 6 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 11.22946 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 13.72946 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 16.22946 -2 0 MPRFLG 0 ; 
       9 SCHEM 12.47946 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 13.72946 -1.490116e-008 0 USR SRT 4.267863 4.267863 4.267863 0 0 0 0 -4.136997 0 MPRFLG 0 ; 
       24 SCHEM 25.84718 -12.74266 0 USR SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 62.09718 0 0 SRT 4.080808 0.2877112 0.746 0 0 0 38.53677 0 0 MPRFLG 0 ; 
       6 SCHEM 64.59718 0 0 SRT 0.3162853 0.385533 0.9254849 0 0 0 38.53677 0 0 MPRFLG 0 ; 
       7 SCHEM 67.09718 0 0 SRT 4.080808 0.2877112 0.746 0 0 0 16.60714 0 0 MPRFLG 0 ; 
       8 SCHEM 69.59718 0 0 DISPLAY 3 2 SRT 1.757096 1.10139 1.757096 0 0 0 73.62002 1.493304 7.74006 MPRFLG 0 ; 
       10 SCHEM 35.84718 -14.74266 0 MPRFLG 0 ; 
       11 SCHEM 10.84718 -14.74266 0 MPRFLG 0 ; 
       0 SCHEM 20 -6 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 37.09718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 42.09718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 39.59718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 47.09718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 44.59718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 24.59718 -16.74266 0 MPRFLG 0 ; 
       18 SCHEM 4.597178 -16.74266 0 MPRFLG 0 ; 
       19 SCHEM 32.09718 -16.74266 0 MPRFLG 0 ; 
       20 SCHEM 12.09718 -16.74266 0 MPRFLG 0 ; 
       21 SCHEM 19.59718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 55.84718 -14.74266 0 MPRFLG 0 ; 
       23 SCHEM -4.152822 -14.74266 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 7.097177 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 27.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 22.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 2.097178 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 24.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 4.597178 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 14.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 34.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 9.597178 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 29.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 32.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 12.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 52.09718 -16.74266 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 54.59718 -16.74266 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 57.09718 -16.74266 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 59.59718 -16.74266 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM -0.402822 -16.74266 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM -2.902822 -16.74266 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM -5.402822 -16.74266 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM -7.902822 -16.74266 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 49.59718 -16.74266 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 17.09718 -16.74266 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 21.13507 -7.182314 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 20.63537 -6.955178 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.68019 -6.682616 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.00121 -5.637795 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.774984 -11.68201 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 8.412477 -5.636586 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       6 SCHEM 21.13507 -9.182314 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 20.63537 -8.955178 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.68019 -8.682616 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.00121 -7.637795 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
