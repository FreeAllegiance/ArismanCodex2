SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.2-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 52     
       ss100_nulls-extru44.2-0 ROOT ; 
       ss303_ordinance-cube1.1-0 ; 
       ss303_ordinance-cube10.1-0 ; 
       ss303_ordinance-cube11.1-0 ; 
       ss303_ordinance-cube2.1-0 ; 
       ss303_ordinance-cube3.1-0 ; 
       ss303_ordinance-cube4.1-0 ; 
       ss303_ordinance-cube5.1-0 ; 
       ss303_ordinance-cube6.1-0 ; 
       ss303_ordinance-cube7.1-0 ; 
       ss303_ordinance-cube8.1-0 ; 
       ss303_ordinance-cube9.1-0 ; 
       ss303_ordinance-cyl1.1-0 ; 
       ss303_ordinance-sphere1.2-0 ROOT ; 
       ss305_elect_station-cube12.1-0 ROOT ; 
       ss305_elect_station-east_bay_11_8.1-0 ; 
       ss305_elect_station-east_bay_11_9.1-0 ; 
       ss305_elect_station-garage1A.1-0 ; 
       ss305_elect_station-garage1B.1-0 ; 
       ss305_elect_station-garage1C.1-0 ; 
       ss305_elect_station-garage1D.1-0 ; 
       ss305_elect_station-garage1E.1-0 ; 
       ss305_elect_station-landing_lights_2.1-0 ; 
       ss305_elect_station-landing_lights_3.1-0 ; 
       ss305_elect_station-landing_lights2.1-0 ; 
       ss305_elect_station-landing_lights2_3.1-0 ; 
       ss305_elect_station-launch1.1-0 ; 
       ss305_elect_station-null18.1-0 ; 
       ss305_elect_station-null19.2-0 ROOT ; 
       ss305_elect_station-null20.2-0 ROOT ; 
       ss305_elect_station-SS_11.1-0 ; 
       ss305_elect_station-SS_11_1.1-0 ; 
       ss305_elect_station-SS_13_2.1-0 ; 
       ss305_elect_station-SS_13_3.1-0 ; 
       ss305_elect_station-SS_15_1.1-0 ; 
       ss305_elect_station-SS_15_3.1-0 ; 
       ss305_elect_station-SS_23.1-0 ; 
       ss305_elect_station-SS_23_2.1-0 ; 
       ss305_elect_station-SS_24.1-0 ; 
       ss305_elect_station-SS_24_1.1-0 ; 
       ss305_elect_station-SS_26.1-0 ; 
       ss305_elect_station-SS_26_3.1-0 ; 
       ss305_elect_station-SS_29.1-0 ; 
       ss305_elect_station-SS_30.1-0 ; 
       ss305_elect_station-SS_31.1-0 ; 
       ss305_elect_station-SS_32.1-0 ; 
       ss305_elect_station-SS_33.1-0 ; 
       ss305_elect_station-SS_34.1-0 ; 
       ss305_elect_station-SS_35.1-0 ; 
       ss305_elect_station-SS_36.1-0 ; 
       ss305_elect_station-turwepemt2.1-0 ; 
       ss305_elect_station-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss305-elect_station.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 13 110 ; 
       1 12 110 ; 
       4 12 110 ; 
       6 7 110 ; 
       7 13 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       12 9 110 ; 
       15 29 110 ; 
       16 29 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 15 110 ; 
       20 15 110 ; 
       21 15 110 ; 
       22 15 110 ; 
       23 16 110 ; 
       24 15 110 ; 
       25 16 110 ; 
       26 16 110 ; 
       27 29 110 ; 
       30 23 110 ; 
       31 22 110 ; 
       32 22 110 ; 
       33 23 110 ; 
       34 22 110 ; 
       35 23 110 ; 
       36 25 110 ; 
       37 24 110 ; 
       38 25 110 ; 
       39 24 110 ; 
       40 24 110 ; 
       41 25 110 ; 
       42 27 110 ; 
       43 27 110 ; 
       44 27 110 ; 
       45 27 110 ; 
       46 28 110 ; 
       47 28 110 ; 
       48 28 110 ; 
       49 28 110 ; 
       50 15 110 ; 
       51 16 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       2 9 110 ; 
       3 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       15 3 300 ; 
       15 4 300 ; 
       15 5 300 ; 
       16 0 300 ; 
       16 1 300 ; 
       16 2 300 ; 
       30 7 300 ; 
       31 8 300 ; 
       32 8 300 ; 
       33 7 300 ; 
       34 8 300 ; 
       35 7 300 ; 
       36 7 300 ; 
       37 8 300 ; 
       38 7 300 ; 
       39 8 300 ; 
       40 8 300 ; 
       41 7 300 ; 
       42 10 300 ; 
       43 10 300 ; 
       44 10 300 ; 
       45 10 300 ; 
       46 9 300 ; 
       47 9 300 ; 
       48 9 300 ; 
       49 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -6 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 16.22946 -2 0 MPRFLG 0 ; 
       13 SCHEM 13.72946 -1.490116e-008 0 USR SRT 4.267863 4.267863 4.267863 0 0 0 0 -4.136997 0 MPRFLG 0 ; 
       1 SCHEM 11.22946 -4 0 MPRFLG 0 ; 
       4 SCHEM 13.72946 -4 0 MPRFLG 0 ; 
       6 SCHEM 62.09718 0 0 MPRFLG 0 ; 
       7 SCHEM 64.59718 0 0 MPRFLG 0 ; 
       8 SCHEM 67.09718 0 0 MPRFLG 0 ; 
       9 SCHEM 69.59718 0 0 MPRFLG 0 ; 
       12 SCHEM 12.47946 -2 0 MPRFLG 0 ; 
       15 SCHEM 35.84718 -14.74266 0 MPRFLG 0 ; 
       16 SCHEM 10.84718 -14.74266 0 MPRFLG 0 ; 
       17 SCHEM 37.09718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 42.09718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 39.59718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 47.09718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 44.59718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 24.59718 -16.74266 0 MPRFLG 0 ; 
       23 SCHEM 4.597178 -16.74266 0 MPRFLG 0 ; 
       24 SCHEM 32.09718 -16.74266 0 MPRFLG 0 ; 
       25 SCHEM 12.09718 -16.74266 0 MPRFLG 0 ; 
       26 SCHEM 19.59718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 55.84718 -14.74266 0 MPRFLG 0 ; 
       28 SCHEM -4.152822 -14.74266 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 25.84718 -12.74266 0 USR SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 7.097177 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 27.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 22.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 2.097178 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 24.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 4.597178 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 14.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 34.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 9.597178 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 29.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 32.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 12.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 52.09718 -16.74266 0 WIRECOL 2 7 MPRFLG 0 ; 
       43 SCHEM 54.59718 -16.74266 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 57.09718 -16.74266 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 59.59718 -16.74266 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM -0.402822 -16.74266 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM -2.902822 -16.74266 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM -5.402822 -16.74266 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM -7.902822 -16.74266 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 49.59718 -16.74266 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 17.09718 -16.74266 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 72.09718 0 0 MPRFLG 0 ; 
       11 SCHEM 74.59718 0 0 MPRFLG 0 ; 
       2 SCHEM 77.09718 0 0 MPRFLG 0 ; 
       3 SCHEM 79.59718 0 0 MPRFLG 0 ; 
       14 SCHEM 82.09718 0 0 DISPLAY 3 2 SRT 8.026395 7.870412 11.36883 0 0 0 78.24184 14.70643 4.003293 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20.63537 -6.955178 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.68019 -6.682616 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.00121 -5.637795 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.13507 -7.182314 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.774984 -11.68201 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 8.412477 -5.636586 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20.63537 -8.955178 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.68019 -8.682616 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.00121 -7.637795 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.13507 -9.182314 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
