SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.4-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 53     
       ss100_nulls-extru44.3-0 ROOT ; 
       ss303_ordinance-cube1.1-0 ; 
       ss303_ordinance-cube10.1-0 ; 
       ss303_ordinance-cube11.1-0 ; 
       ss303_ordinance-cube12.1-0 ; 
       ss303_ordinance-cube13.1-0 ; 
       ss303_ordinance-cube2.1-0 ; 
       ss303_ordinance-cube3.1-0 ; 
       ss303_ordinance-cube4.1-0 ; 
       ss303_ordinance-cube5.1-0 ROOT ; 
       ss303_ordinance-cube6.1-0 ; 
       ss303_ordinance-cube7.1-0 ; 
       ss303_ordinance-cube8.1-0 ; 
       ss303_ordinance-cube9.1-0 ; 
       ss303_ordinance-cyl1.1-0 ; 
       ss303_ordinance-sphere1.4-0 ROOT ; 
       ss305_elect_station-east_bay_11_8.1-0 ; 
       ss305_elect_station-east_bay_11_9.1-0 ; 
       ss305_elect_station-garage1A.1-0 ; 
       ss305_elect_station-garage1B.1-0 ; 
       ss305_elect_station-garage1C.1-0 ; 
       ss305_elect_station-garage1D.1-0 ; 
       ss305_elect_station-garage1E.1-0 ; 
       ss305_elect_station-landing_lights_2.1-0 ; 
       ss305_elect_station-landing_lights_3.1-0 ; 
       ss305_elect_station-landing_lights2.1-0 ; 
       ss305_elect_station-landing_lights2_3.1-0 ; 
       ss305_elect_station-launch1.1-0 ; 
       ss305_elect_station-null18.1-0 ; 
       ss305_elect_station-null19.3-0 ROOT ; 
       ss305_elect_station-null20.4-0 ROOT ; 
       ss305_elect_station-SS_11.1-0 ; 
       ss305_elect_station-SS_11_1.1-0 ; 
       ss305_elect_station-SS_13_2.1-0 ; 
       ss305_elect_station-SS_13_3.1-0 ; 
       ss305_elect_station-SS_15_1.1-0 ; 
       ss305_elect_station-SS_15_3.1-0 ; 
       ss305_elect_station-SS_23.1-0 ; 
       ss305_elect_station-SS_23_2.1-0 ; 
       ss305_elect_station-SS_24.1-0 ; 
       ss305_elect_station-SS_24_1.1-0 ; 
       ss305_elect_station-SS_26.1-0 ; 
       ss305_elect_station-SS_26_3.1-0 ; 
       ss305_elect_station-SS_29.1-0 ; 
       ss305_elect_station-SS_30.1-0 ; 
       ss305_elect_station-SS_31.1-0 ; 
       ss305_elect_station-SS_32.1-0 ; 
       ss305_elect_station-SS_33.1-0 ; 
       ss305_elect_station-SS_34.1-0 ; 
       ss305_elect_station-SS_35.1-0 ; 
       ss305_elect_station-SS_36.1-0 ; 
       ss305_elect_station-turwepemt2.1-0 ; 
       ss305_elect_station-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss305-elect_station.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 14 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       6 14 110 ; 
       7 15 110 ; 
       8 9 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 11 110 ; 
       4 11 110 ; 
       16 30 110 ; 
       17 30 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 16 110 ; 
       21 16 110 ; 
       22 16 110 ; 
       23 16 110 ; 
       24 17 110 ; 
       25 16 110 ; 
       26 17 110 ; 
       27 17 110 ; 
       28 30 110 ; 
       31 24 110 ; 
       32 23 110 ; 
       33 23 110 ; 
       34 24 110 ; 
       35 23 110 ; 
       36 24 110 ; 
       37 26 110 ; 
       38 25 110 ; 
       39 26 110 ; 
       40 25 110 ; 
       41 25 110 ; 
       42 26 110 ; 
       43 28 110 ; 
       44 28 110 ; 
       45 28 110 ; 
       46 28 110 ; 
       47 29 110 ; 
       48 29 110 ; 
       49 29 110 ; 
       50 29 110 ; 
       51 16 110 ; 
       52 17 110 ; 
       5 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       16 3 300 ; 
       16 4 300 ; 
       16 5 300 ; 
       17 0 300 ; 
       17 1 300 ; 
       17 2 300 ; 
       31 7 300 ; 
       32 8 300 ; 
       33 8 300 ; 
       34 7 300 ; 
       35 8 300 ; 
       36 7 300 ; 
       37 7 300 ; 
       38 8 300 ; 
       39 7 300 ; 
       40 8 300 ; 
       41 8 300 ; 
       42 7 300 ; 
       43 10 300 ; 
       44 10 300 ; 
       45 10 300 ; 
       46 10 300 ; 
       47 9 300 ; 
       48 9 300 ; 
       49 9 300 ; 
       50 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 26.90414 -2.491339 0 USR SRT 0.786 0.786 0.786 0 0 0 -1.876221 -17.84419 14.1627 MPRFLG 0 ; 
       1 SCHEM 10.28128 -11.43321 0 MPRFLG 0 ; 
       2 SCHEM 17.78128 -9.433206 0 MPRFLG 0 ; 
       3 SCHEM 20.28128 -9.433206 0 MPRFLG 0 ; 
       6 SCHEM 12.78128 -11.43321 0 MPRFLG 0 ; 
       7 SCHEM 2.78128 -5.433205 0 MPRFLG 0 ; 
       8 SCHEM 5.28128 -7.433205 0 MPRFLG 0 ; 
       9 SCHEM 15.28128 -5.433205 0 SRT 0.3162853 0.385533 0.9254849 0 0 0 63.15673 0.06861222 5.283679 MPRFLG 0 ; 
       10 SCHEM 7.781279 -7.433205 0 MPRFLG 0 ; 
       11 SCHEM 17.78128 -7.433205 0 MPRFLG 0 ; 
       12 SCHEM 15.28128 -9.433206 0 MPRFLG 0 ; 
       13 SCHEM 15.28128 -11.43321 0 MPRFLG 0 ; 
       14 SCHEM 11.53128 -9.433206 0 MPRFLG 0 ; 
       15 SCHEM 14.03128 -3.433205 0 USR DISPLAY 1 2 SRT 4.267863 4.267863 4.267863 0 0 0 0 -4.136997 0 MPRFLG 0 ; 
       4 SCHEM 22.78128 -9.433206 0 MPRFLG 0 ; 
       16 SCHEM 35.84718 -14.74266 0 MPRFLG 0 ; 
       17 SCHEM 10.84718 -14.74266 0 MPRFLG 0 ; 
       18 SCHEM 37.09718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 42.09718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 39.59718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 47.09718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 44.59718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 24.59718 -16.74266 0 MPRFLG 0 ; 
       24 SCHEM 4.597178 -16.74266 0 MPRFLG 0 ; 
       25 SCHEM 32.09718 -16.74266 0 MPRFLG 0 ; 
       26 SCHEM 12.09718 -16.74266 0 MPRFLG 0 ; 
       27 SCHEM 19.59718 -16.74266 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 55.84718 -14.74266 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM -4.152822 -14.74266 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 25.84718 -12.74266 0 USR SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       31 SCHEM 7.097177 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 27.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 22.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 2.097178 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 24.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 4.597178 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 14.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 34.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 9.597178 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 29.59718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 32.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 12.09718 -18.74266 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 52.09718 -16.74266 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 54.59718 -16.74266 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 57.09718 -16.74266 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 59.59718 -16.74266 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM -0.402822 -16.74266 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM -2.902822 -16.74266 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM -5.402822 -16.74266 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM -7.902822 -16.74266 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 49.59718 -16.74266 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 17.09718 -16.74266 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25.28128 -9.433206 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20.63537 -6.955178 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.68019 -6.682616 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.00121 -5.637795 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.13507 -7.182314 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.774984 -11.68201 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 8.412477 -5.636586 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20.63537 -8.955178 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.68019 -8.682616 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.00121 -7.637795 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.13507 -9.182314 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
