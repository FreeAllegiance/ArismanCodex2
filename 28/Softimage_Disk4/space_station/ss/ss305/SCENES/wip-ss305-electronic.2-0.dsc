SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.3-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       purpmeteor-default9.2-0 ; 
       ss301_garrison-white_strobe1_33.2-0 ; 
       ss301_garrison-white_strobe1_34.2-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_elect_station-mat52.3-0 ; 
       ss305_elect_station-mat53.3-0 ; 
       ss305_elect_station-mat54.3-0 ; 
       ss305_elect_station-mat55.3-0 ; 
       ss305_elect_station-mat56.3-0 ; 
       ss305_elect_station-mat57.3-0 ; 
       ss305_elect_station-mat7.3-0 ; 
       ss305_elect_station-white_strobe1_25.3-0 ; 
       ss305_elect_station-white_strobe1_31.3-0 ; 
       ss305_elect_station-white_strobe1_5.3-0 ; 
       ss305_elect_station-white_strobe1_9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 64     
       2-cube1.1-0 ; 
       2-cube2.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ROOT ; 
       2-null18.2-0 ROOT ; 
       2-null18_1.2-0 ROOT ; 
       2-null19.2-0 ROOT ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_30_1.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_31_1.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       purpmeteor-cube10.1-0 ; 
       purpmeteor-cube12.1-0 ; 
       purpmeteor-cube13.1-0 ; 
       purpmeteor-cube13_1.1-0 ; 
       purpmeteor-cube18.1-0 ROOT ; 
       purpmeteor-cube19.1-0 ; 
       purpmeteor-cube20.1-0 ; 
       purpmeteor-cube21.1-0 ROOT ; 
       purpmeteor-cube22.1-0 ; 
       purpmeteor-cube23.1-0 ; 
       purpmeteor-cube25.1-0 ; 
       purpmeteor-cube3.1-0 ROOT ; 
       purpmeteor-cube4.1-0 ; 
       purpmeteor-east_bay_11_8.1-0 ; 
       purpmeteor-east_bay_11_9.1-0 ; 
       purpmeteor-extru44.1-0 ; 
       purpmeteor-extru47.1-0 ; 
       purpmeteor-garage1A.1-0 ; 
       purpmeteor-garage1B.1-0 ; 
       purpmeteor-garage1C.1-0 ; 
       purpmeteor-garage1D.1-0 ; 
       purpmeteor-garage1E.1-0 ; 
       purpmeteor-launch1.1-0 ; 
       purpmeteor-null20.1-0 ; 
       purpmeteor-null21.1-0 ; 
       purpmeteor-sphere9.3-0 ROOT ; 
       purpmeteor-SS_11.1-0 ; 
       purpmeteor-SS_11_1.1-0 ; 
       purpmeteor-SS_13_2.1-0 ; 
       purpmeteor-SS_13_3.1-0 ; 
       purpmeteor-SS_15_1.1-0 ; 
       purpmeteor-SS_15_3.1-0 ; 
       purpmeteor-SS_23.1-0 ; 
       purpmeteor-SS_23_2.1-0 ; 
       purpmeteor-SS_24.1-0 ; 
       purpmeteor-SS_24_1.1-0 ; 
       purpmeteor-SS_26.1-0 ; 
       purpmeteor-SS_26_3.1-0 ; 
       purpmeteor-turwepemt2.1-0 ; 
       purpmeteor-turwepemt2_3.1-0 ; 
       purpmeteor1-cube3.1-0 ROOT ; 
       ss305_electronic-cube24.1-0 ROOT ; 
       ss305_electronic-cube26.1-0 ROOT ; 
       ss305_electronic-cube5.2-0 ROOT ; 
       ss305_electronic-extru48.1-0 ROOT ; 
       ss305_electronic1-cube26.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/bgrnd53 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss305-electronic.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       purpmeteor-t2d1.2-0 ; 
       ss305_electronic-t2d58.1-0 ; 
       ss305_elect_station-t2d52.3-0 ; 
       ss305_elect_station-t2d53.3-0 ; 
       ss305_elect_station-t2d54.3-0 ; 
       ss305_elect_station-t2d55.3-0 ; 
       ss305_elect_station-t2d56.3-0 ; 
       ss305_elect_station-t2d57.3-0 ; 
       ss305_elect_station-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 6 110 ; 
       11 5 110 ; 
       12 6 110 ; 
       13 5 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 28 110 ; 
       23 22 110 ; 
       24 25 110 ; 
       26 22 110 ; 
       27 42 110 ; 
       28 42 110 ; 
       30 22 110 ; 
       31 41 110 ; 
       32 28 110 ; 
       33 43 110 ; 
       34 22 110 ; 
       35 31 110 ; 
       36 31 110 ; 
       37 31 110 ; 
       38 31 110 ; 
       39 31 110 ; 
       40 32 110 ; 
       41 27 110 ; 
       42 43 110 ; 
       44 32 110 ; 
       45 31 110 ; 
       46 31 110 ; 
       47 32 110 ; 
       48 31 110 ; 
       49 32 110 ; 
       50 32 110 ; 
       51 31 110 ; 
       52 32 110 ; 
       53 31 110 ; 
       54 31 110 ; 
       55 32 110 ; 
       56 31 110 ; 
       57 32 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 14 300 ; 
       9 14 300 ; 
       10 2 300 ; 
       11 14 300 ; 
       12 1 300 ; 
       13 14 300 ; 
       14 13 300 ; 
       15 13 300 ; 
       16 13 300 ; 
       17 13 300 ; 
       31 7 300 ; 
       31 8 300 ; 
       31 9 300 ; 
       32 4 300 ; 
       32 5 300 ; 
       32 6 300 ; 
       33 10 300 ; 
       43 0 300 ; 
       44 11 300 ; 
       45 12 300 ; 
       46 12 300 ; 
       47 11 300 ; 
       48 12 300 ; 
       49 11 300 ; 
       50 11 300 ; 
       51 12 300 ; 
       52 11 300 ; 
       53 12 300 ; 
       54 12 300 ; 
       55 11 300 ; 
       62 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       43 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 88.68874 43.66432 0 USR MPRFLG 0 ; 
       1 SCHEM 88.68874 41.66431 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 96.06781 41.41614 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 98.56781 41.41614 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 101.0678 41.41614 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 101.0678 39.41614 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 98.56781 43.41614 0 USR DISPLAY 0 0 SRT 1 1 1 7.807066e-009 2.59454 -2.653347e-008 151.1425 -14.19661 75.7656 MPRFLG 0 ; 
       5 SCHEM 123.496 44.73706 0 USR SRT 1 1 1 0 1.570796 0 104.3961 -4.675931 9.374343 MPRFLG 0 ; 
       6 SCHEM 128.1605 47.36349 0 USR SRT 1 1 1 0 0.4847831 0 11.05156 -3.000762 7.009114 MPRFLG 0 ; 
       7 SCHEM 118.8432 40.86653 0 USR SRT 1 1 1 0 0 0 104.473 3.878042 8.341749 MPRFLG 0 ; 
       8 SCHEM 119.746 42.73706 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       9 SCHEM 122.246 42.73706 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       10 SCHEM 126.9105 45.36349 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 124.746 42.73706 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       12 SCHEM 129.4105 45.36349 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 127.246 42.73706 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       14 SCHEM 122.5932 38.86653 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 120.0932 38.86653 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 117.5932 38.86653 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 115.0932 38.86653 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 96.14282 30.70576 0 MPRFLG 0 ; 
       19 SCHEM 98.64282 30.70576 0 MPRFLG 0 ; 
       20 SCHEM 103.6428 30.70576 0 MPRFLG 0 ; 
       21 SCHEM 166.1428 30.70576 0 MPRFLG 0 ; 
       22 SCHEM 103.6428 32.70576 0 SRT 2.874776 2.337215 2.337215 -3.556592e-008 2.298739e-009 2.437903e-008 86.67202 0.1648148 -15.02051 MPRFLG 0 ; 
       23 SCHEM 108.6428 30.70576 0 MPRFLG 0 ; 
       24 SCHEM 113.6428 32.70576 0 MPRFLG 0 ; 
       25 SCHEM 104.8928 34.70576 0 SRT 1.372213 0.3753105 1.343386 -3.556592e-008 2.298739e-009 2.437903e-008 31.32545 -0.06458986 -13.63194 MPRFLG 0 ; 
       26 SCHEM 111.1428 30.70576 0 MPRFLG 0 ; 
       27 SCHEM 129.8928 32.70576 0 MPRFLG 0 ; 
       28 SCHEM 156.1428 32.70576 0 MPRFLG 0 ; 
       29 SCHEM 120.5766 15.09375 0 USR SRT 0.68 0.68 0.68 1.570796 0 0 -2.470009 -13.66908 -31.68621 MPRFLG 0 ; 
       30 SCHEM 101.1428 30.70576 0 MPRFLG 0 ; 
       31 SCHEM 129.8928 28.70576 0 MPRFLG 0 ; 
       32 SCHEM 154.8928 30.70576 0 MPRFLG 0 ; 
       33 SCHEM 91.14282 34.70576 0 MPRFLG 0 ; 
       34 SCHEM 106.1428 30.70576 0 MPRFLG 0 ; 
       35 SCHEM 131.1428 26.70576 0 WIRECOL 9 7 MPRFLG 0 ; 
       36 SCHEM 136.1428 26.70576 0 WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM 133.6428 26.70576 0 WIRECOL 9 7 MPRFLG 0 ; 
       38 SCHEM 141.1428 26.70576 0 WIRECOL 9 7 MPRFLG 0 ; 
       39 SCHEM 138.6428 26.70576 0 WIRECOL 9 7 MPRFLG 0 ; 
       40 SCHEM 163.6428 28.70576 0 WIRECOL 9 7 MPRFLG 0 ; 
       41 SCHEM 129.8928 30.70576 0 MPRFLG 0 ; 
       42 SCHEM 141.1428 34.70576 0 MPRFLG 0 ; 
       43 SCHEM 128.6428 36.70576 0 USR SRT 5.284098 7.138733 4.714692 1.527214 0.0445713 0.4540234 0 -2.197154 0 MPRFLG 0 ; 
       44 SCHEM 151.1428 28.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 121.1428 26.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 116.1428 26.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 146.1428 28.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 118.6428 26.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 148.6428 28.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 158.6428 28.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 128.6428 26.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 153.6428 28.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 123.6428 26.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 126.1428 26.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 156.1428 28.70576 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 143.6428 26.70576 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 161.1428 28.70576 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 96.88953 45.32312 0 USR DISPLAY 0 0 SRT 1.711165 0.5905364 1.162 0 1.570796 0 -48.73319 11.45754 -1.367697 MPRFLG 0 ; 
       60 SCHEM 168.6428 47.36349 0 SRT 0.674 0.4660001 1 0 0 0 -18.74382 -2.883908 -28.4376 MPRFLG 0 ; 
       61 SCHEM 94.56315 44.56548 0 USR DISPLAY 0 0 SRT 1.316 1.316 1.316 -1.078445e-008 2.381581e-008 -4.766174e-008 27.36609 -20.99654 -35.87016 MPRFLG 0 ; 
       62 SCHEM 171.1428 47.36349 0 SRT 0.7607601 0.7607601 0.7607601 -9.472758e-008 0 1.570796 82.26605 -5.712818 -25.58943 MPRFLG 0 ; 
       58 SCHEM 173.6428 47.36349 0 USR DISPLAY 1 2 SRT 0.68 0.68 0.68 1.570796 0 0 -2.176014 8.404938 -28.41938 MPRFLG 0 ; 
       63 SCHEM 176.1428 47.36349 0 SRT 0.4340559 0.300104 0.6439999 0 0 1.570796 -20.28998 1.604998 -30.13979 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 46.64044 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 77.92294 27.42161 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 77.92294 27.42161 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 176.7309 7.966751 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 8.538183 5.787482 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9.583002 6.060043 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4.904022 7.104864 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 38.99999 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 38.99999 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 38.99999 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 96.73093 -4.690975 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -4.322201 1.060651 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -3.684711 7.106074 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49.14044 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 176.7309 5.966751 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 8.538183 3.787482 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9.583002 4.060043 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4.904022 5.104864 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 38.99999 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 38.99999 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 38.99999 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 96.73093 -6.690975 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
