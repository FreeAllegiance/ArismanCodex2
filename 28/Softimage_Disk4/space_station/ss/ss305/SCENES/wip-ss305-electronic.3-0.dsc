SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.4-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       purpmeteor-default9.2-0 ; 
       ss301_garrison-white_strobe1_33.2-0 ; 
       ss301_garrison-white_strobe1_34.2-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_elect_station-mat52.3-0 ; 
       ss305_elect_station-mat53.3-0 ; 
       ss305_elect_station-mat54.3-0 ; 
       ss305_elect_station-mat55.3-0 ; 
       ss305_elect_station-mat56.3-0 ; 
       ss305_elect_station-mat57.3-0 ; 
       ss305_elect_station-mat7.3-0 ; 
       ss305_elect_station-white_strobe1_25.3-0 ; 
       ss305_elect_station-white_strobe1_31.3-0 ; 
       ss305_elect_station-white_strobe1_5.3-0 ; 
       ss305_elect_station-white_strobe1_9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 68     
       2-cube1.1-0 ; 
       2-cube2.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.1-0 ROOT ; 
       2-null18.2-0 ROOT ; 
       2-null18_1.2-0 ROOT ; 
       2-null19.2-0 ROOT ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_30_1.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_31_1.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       purpmeteor-cube10.1-0 ; 
       purpmeteor-cube12.1-0 ; 
       purpmeteor-cube13.1-0 ; 
       purpmeteor-cube13_1.1-0 ; 
       purpmeteor-cube18.2-0 ROOT ; 
       purpmeteor-cube19.1-0 ; 
       purpmeteor-cube20.1-0 ; 
       purpmeteor-cube21.1-0 ; 
       purpmeteor-cube22.1-0 ; 
       purpmeteor-cube23.1-0 ; 
       purpmeteor-cube25.1-0 ; 
       purpmeteor-cube26.1-0 ; 
       purpmeteor-cube26_1.1-0 ; 
       purpmeteor-cube26_2.1-0 ; 
       purpmeteor-cube27.1-0 ; 
       purpmeteor-cube3.1-0 ; 
       purpmeteor-cube3_1.1-0 ; 
       purpmeteor-cube4.1-0 ; 
       purpmeteor-east_bay_11_8.1-0 ROOT ; 
       purpmeteor-east_bay_11_9.1-0 ROOT ; 
       purpmeteor-extru44.1-0 ; 
       purpmeteor-extru47.1-0 ; 
       purpmeteor-extru48.1-0 ; 
       purpmeteor-extru49.1-0 ; 
       purpmeteor-extru50.1-0 ; 
       purpmeteor-garage1A.1-0 ; 
       purpmeteor-garage1B.1-0 ; 
       purpmeteor-garage1C.1-0 ; 
       purpmeteor-garage1D.1-0 ; 
       purpmeteor-garage1E.1-0 ; 
       purpmeteor-launch1.1-0 ; 
       purpmeteor-null20.1-0 ; 
       purpmeteor-null21.1-0 ; 
       purpmeteor-sphere9.3-0 ; 
       purpmeteor-SS_11.1-0 ; 
       purpmeteor-SS_11_1.1-0 ; 
       purpmeteor-SS_13_2.1-0 ; 
       purpmeteor-SS_13_3.1-0 ; 
       purpmeteor-SS_15_1.1-0 ; 
       purpmeteor-SS_15_3.1-0 ; 
       purpmeteor-SS_23.1-0 ; 
       purpmeteor-SS_23_2.1-0 ; 
       purpmeteor-SS_24.1-0 ; 
       purpmeteor-SS_24_1.1-0 ; 
       purpmeteor-SS_26.1-0 ; 
       purpmeteor-SS_26_3.1-0 ; 
       purpmeteor-turwepemt2.1-0 ; 
       purpmeteor-turwepemt2_3.1-0 ; 
       ss305_electronic-cube24.1-0 ROOT ; 
       ss305_electronic-cube5.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/bgrnd53 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss305-electronic.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       purpmeteor-t2d1.2-0 ; 
       ss305_electronic-t2d58.1-0 ; 
       ss305_elect_station-t2d52.3-0 ; 
       ss305_elect_station-t2d53.3-0 ; 
       ss305_elect_station-t2d54.3-0 ; 
       ss305_elect_station-t2d55.3-0 ; 
       ss305_elect_station-t2d56.3-0 ; 
       ss305_elect_station-t2d57.3-0 ; 
       ss305_elect_station-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 6 110 ; 
       11 5 110 ; 
       12 6 110 ; 
       13 5 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 28 110 ; 
       23 22 110 ; 
       24 25 110 ; 
       25 35 110 ; 
       26 22 110 ; 
       27 50 110 ; 
       28 50 110 ; 
       33 27 110 ; 
       35 22 110 ; 
       38 51 110 ; 
       39 22 110 ; 
       43 36 110 ; 
       44 36 110 ; 
       45 36 110 ; 
       46 36 110 ; 
       47 36 110 ; 
       48 37 110 ; 
       49 27 110 ; 
       50 51 110 ; 
       51 22 110 ; 
       52 37 110 ; 
       53 36 110 ; 
       54 36 110 ; 
       55 37 110 ; 
       56 36 110 ; 
       57 37 110 ; 
       58 37 110 ; 
       59 36 110 ; 
       60 37 110 ; 
       61 36 110 ; 
       62 36 110 ; 
       63 37 110 ; 
       64 36 110 ; 
       65 37 110 ; 
       30 27 110 ; 
       40 22 110 ; 
       34 27 110 ; 
       29 27 110 ; 
       41 22 110 ; 
       42 22 110 ; 
       32 22 110 ; 
       31 27 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 14 300 ; 
       9 14 300 ; 
       10 2 300 ; 
       11 14 300 ; 
       12 1 300 ; 
       13 14 300 ; 
       14 13 300 ; 
       15 13 300 ; 
       16 13 300 ; 
       17 13 300 ; 
       36 7 300 ; 
       36 8 300 ; 
       36 9 300 ; 
       37 4 300 ; 
       37 5 300 ; 
       37 6 300 ; 
       38 10 300 ; 
       51 0 300 ; 
       52 11 300 ; 
       53 12 300 ; 
       54 12 300 ; 
       55 11 300 ; 
       56 12 300 ; 
       57 11 300 ; 
       58 11 300 ; 
       59 12 300 ; 
       60 11 300 ; 
       61 12 300 ; 
       62 12 300 ; 
       63 11 300 ; 
       40 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       51 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000011 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 12.49999 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 12.49999 -4 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 7.807066e-009 2.59454 -2.653347e-008 151.1425 -14.19661 75.7656 MPRFLG 0 ; 
       5 SCHEM 77.6628 -11.9962 0 USR SRT 1 1 1 0 1.570796 0 104.3961 -4.675931 9.374343 MPRFLG 0 ; 
       6 SCHEM 72.14562 -7.912387 0 USR SRT 1 1 1 0 0.4847831 0 11.05156 -3.000762 7.009114 MPRFLG 0 ; 
       7 SCHEM 61.28183 -10.72001 0 USR SRT 1 1 1 0 0 0 104.473 3.878042 8.341749 MPRFLG 0 ; 
       8 SCHEM 73.9128 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       9 SCHEM 76.4128 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       10 SCHEM 70.89562 -9.912388 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 78.9128 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       12 SCHEM 73.39564 -9.912388 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 81.41281 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       14 SCHEM 65.03183 -12.72001 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 62.53183 -12.72001 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 60.03184 -12.72001 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 57.53184 -12.72001 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 12.289 10.58675 0 MPRFLG 0 ; 
       19 SCHEM 14.789 10.58675 0 MPRFLG 0 ; 
       20 SCHEM 19.789 10.58675 0 MPRFLG 0 ; 
       21 SCHEM 97.28899 4.586751 0 MPRFLG 0 ; 
       22 SCHEM 58.53901 12.58675 0 USR SRT 2.874776 2.337215 2.337215 -3.556592e-008 2.298739e-009 2.437903e-008 86.67202 0.1648148 -15.02051 MPRFLG 0 ; 
       23 SCHEM 27.289 10.58675 0 MPRFLG 0 ; 
       24 SCHEM 17.289 6.58675 0 MPRFLG 0 ; 
       25 SCHEM 17.289 8.586751 0 MPRFLG 0 ; 
       26 SCHEM 29.789 10.58675 0 MPRFLG 0 ; 
       27 SCHEM 54.78901 6.58675 0 MPRFLG 0 ; 
       28 SCHEM 87.28899 6.58675 0 MPRFLG 0 ; 
       33 SCHEM 34.789 4.586751 0 MPRFLG 0 ; 
       35 SCHEM 17.289 10.58675 0 MPRFLG 0 ; 
       36 SCHEM 51.03901 2.586751 0 SRT 0.9999999 0.9999999 1 -1.125474e-007 3.141593 2.500178e-008 -30.79553 -3.855162 -14.9558 MPRFLG 0 ; 
       37 SCHEM 86.03899 4.586751 0 SRT 0.9999999 0.9999999 1 -6.229816e-008 6.283185 2.500177e-008 -22.0285 1.348138 -6.624516 MPRFLG 0 ; 
       38 SCHEM 32.28901 8.586751 0 MPRFLG 0 ; 
       39 SCHEM 22.289 10.58675 0 MPRFLG 0 ; 
       43 SCHEM 52.28901 0.5867507 0 WIRECOL 9 7 MPRFLG 0 ; 
       44 SCHEM 57.28901 0.5867507 0 WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM 54.78901 0.5867507 0 WIRECOL 9 7 MPRFLG 0 ; 
       46 SCHEM 62.28901 0.5867507 0 WIRECOL 9 7 MPRFLG 0 ; 
       47 SCHEM 59.78901 0.5867507 0 WIRECOL 9 7 MPRFLG 0 ; 
       48 SCHEM 94.78899 2.586751 0 WIRECOL 9 7 MPRFLG 0 ; 
       49 SCHEM 51.03901 4.586751 0 MPRFLG 0 ; 
       50 SCHEM 66.039 8.586751 0 MPRFLG 0 ; 
       51 SCHEM 64.789 10.58675 0 MPRFLG 0 ; 
       52 SCHEM 82.28899 2.586751 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 42.28901 0.5867507 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 37.28901 0.5867507 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 77.28899 2.586751 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 39.78901 0.5867507 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 79.78899 2.586751 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 89.78899 2.586751 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 49.78901 0.5867507 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 84.78899 2.586751 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 44.78901 0.5867507 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 47.28901 0.5867507 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 87.28899 2.586751 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 64.789 0.5867507 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 92.28899 2.586751 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 5 0 0 DISPLAY 0 0 SRT 1.711165 0.5905364 1.162 0 1.570796 0 -48.73319 11.45754 -1.367697 MPRFLG 0 ; 
       30 SCHEM 67.28899 4.586751 0 MPRFLG 0 ; 
       67 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1.316 1.316 1.316 -1.078445e-008 2.381581e-008 -4.766174e-008 27.36609 -20.99654 -35.87016 MPRFLG 0 ; 
       40 SCHEM 102.289 10.58675 0 MPRFLG 0 ; 
       34 SCHEM 69.78899 4.586751 0 MPRFLG 0 ; 
       29 SCHEM 72.28899 4.586751 0 MPRFLG 0 ; 
       41 SCHEM 24.789 10.58675 0 MPRFLG 0 ; 
       42 SCHEM 99.78899 10.58675 0 MPRFLG 0 ; 
       32 SCHEM 104.789 10.58675 0 MPRFLG 0 ; 
       31 SCHEM 74.78899 4.586751 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 135.9358 -13.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 162.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 160.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 142.5238 -41.39674 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -38.85463 -26.91828 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -37.8098 -26.64572 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -42.48878 -25.60089 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59.3381 -37.39674 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -51.71501 -31.64511 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -66.07942 -25.59969 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 147.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 150.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 135.9358 -13.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 142.5238 -43.39674 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -38.85463 -28.91828 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -37.8098 -28.64572 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM -42.48878 -27.60089 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59.3381 -39.39674 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
