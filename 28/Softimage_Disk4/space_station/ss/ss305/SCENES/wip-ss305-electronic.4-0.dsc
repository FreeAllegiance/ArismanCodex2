SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.5-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       purpmeteor-default9.2-0 ; 
       ss301_garrison-white_strobe1_33.2-0 ; 
       ss301_garrison-white_strobe1_34.2-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_electronic-mat59.1-0 ; 
       ss305_electronic-mat60.1-0 ; 
       ss305_electronic-mat61.1-0 ; 
       ss305_electronic-mat62.1-0 ; 
       ss305_electronic-mat63.1-0 ; 
       ss305_electronic-mat64.1-0 ; 
       ss305_elect_station-mat52.3-0 ; 
       ss305_elect_station-mat53.3-0 ; 
       ss305_elect_station-mat54.3-0 ; 
       ss305_elect_station-mat55.3-0 ; 
       ss305_elect_station-mat56.3-0 ; 
       ss305_elect_station-mat57.3-0 ; 
       ss305_elect_station-mat7.3-0 ; 
       ss305_elect_station-white_strobe1_25.3-0 ; 
       ss305_elect_station-white_strobe1_31.3-0 ; 
       ss305_elect_station-white_strobe1_5.3-0 ; 
       ss305_elect_station-white_strobe1_9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 73     
       2-cube1.1-0 ; 
       2-cube2.1-0 ; 
       2-cube8.1-0 ; 
       2-cube9.1-0 ; 
       2-cyl1.2-0 ROOT ; 
       2-null18.3-0 ROOT ; 
       2-null18_1.3-0 ROOT ; 
       2-null19.3-0 ROOT ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_30_1.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_31_1.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       purpmeteor-cube10.1-0 ; 
       purpmeteor-cube12.1-0 ; 
       purpmeteor-cube13.1-0 ; 
       purpmeteor-cube13_1.1-0 ; 
       purpmeteor-cube13_2.1-0 ; 
       purpmeteor-cube18.2-0 ; 
       purpmeteor-cube19.1-0 ; 
       purpmeteor-cube20.1-0 ; 
       purpmeteor-cube21.1-0 ; 
       purpmeteor-cube22.1-0 ; 
       purpmeteor-cube23.1-0 ; 
       purpmeteor-cube25.1-0 ; 
       purpmeteor-cube26.1-0 ; 
       purpmeteor-cube26_1.1-0 ; 
       purpmeteor-cube26_2.1-0 ; 
       purpmeteor-cube27.1-0 ; 
       purpmeteor-cube28.1-0 ; 
       purpmeteor-cube3.1-0 ; 
       purpmeteor-cube3_1.1-0 ; 
       purpmeteor-cube4.1-0 ; 
       purpmeteor-east_bay_11_8.1-0 ; 
       purpmeteor-east_bay_11_9.1-0 ; 
       purpmeteor-east_bay_11_9_1.1-0 ROOT ; 
       purpmeteor-extru44.1-0 ; 
       purpmeteor-extru47.1-0 ; 
       purpmeteor-extru48.1-0 ; 
       purpmeteor-extru49.1-0 ; 
       purpmeteor-extru50.1-0 ; 
       purpmeteor-extru51.1-0 ; 
       purpmeteor-launch1.1-0 ; 
       purpmeteor-null20.1-0 ; 
       purpmeteor-null21.1-0 ; 
       purpmeteor-sphere9.4-0 ROOT ; 
       purpmeteor-SS_11.1-0 ; 
       purpmeteor-SS_13_3.1-0 ; 
       purpmeteor-SS_15_3.1-0 ; 
       purpmeteor-SS_23.1-0 ; 
       purpmeteor-SS_24.1-0 ; 
       purpmeteor-SS_26_3.1-0 ; 
       purpmeteor-turwepemt2_3.1-0 ; 
       purpmeteor1-east_bay_11_8.1-0 ROOT ; 
       purpmeteor1-garage1A.1-0 ; 
       purpmeteor1-garage1B.1-0 ; 
       purpmeteor1-garage1C.1-0 ; 
       purpmeteor1-garage1D.1-0 ; 
       purpmeteor1-garage1E.1-0 ; 
       purpmeteor1-SS_11_1.1-0 ; 
       purpmeteor1-SS_13_2.1-0 ; 
       purpmeteor1-SS_15_1.1-0 ; 
       purpmeteor1-SS_23_2.1-0 ; 
       purpmeteor1-SS_24_1.1-0 ; 
       purpmeteor1-SS_26.1-0 ; 
       purpmeteor1-turwepemt2.1-0 ; 
       ss305_electronic-cube24.2-0 ROOT ; 
       ss305_electronic-cube5.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/bgrnd53 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss305-electronic.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       purpmeteor-t2d1.2-0 ; 
       ss305_electronic-t2d58.1-0 ; 
       ss305_electronic-t2d59.1-0 ; 
       ss305_electronic-t2d60.1-0 ; 
       ss305_electronic-t2d61.1-0 ; 
       ss305_electronic-t2d62.1-0 ; 
       ss305_electronic-t2d63.1-0 ; 
       ss305_electronic-t2d64.1-0 ; 
       ss305_elect_station-t2d52.3-0 ; 
       ss305_elect_station-t2d53.3-0 ; 
       ss305_elect_station-t2d54.3-0 ; 
       ss305_elect_station-t2d55.3-0 ; 
       ss305_elect_station-t2d56.3-0 ; 
       ss305_elect_station-t2d57.3-0 ; 
       ss305_elect_station-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       34 26 110 ; 
       46 28 110 ; 
       22 26 110 ; 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 6 110 ; 
       11 5 110 ; 
       12 6 110 ; 
       13 5 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 23 110 ; 
       19 23 110 ; 
       20 23 110 ; 
       21 29 110 ; 
       23 26 110 ; 
       24 23 110 ; 
       25 26 110 ; 
       26 50 110 ; 
       27 23 110 ; 
       28 49 110 ; 
       29 49 110 ; 
       30 28 110 ; 
       31 28 110 ; 
       32 28 110 ; 
       33 23 110 ; 
       35 28 110 ; 
       36 28 110 ; 
       37 23 110 ; 
       38 28 110 ; 
       39 29 110 ; 
       41 23 110 ; 
       42 23 110 ; 
       43 23 110 ; 
       44 23 110 ; 
       45 23 110 ; 
       59 58 110 ; 
       60 58 110 ; 
       61 58 110 ; 
       62 58 110 ; 
       63 58 110 ; 
       47 40 110 ; 
       48 28 110 ; 
       49 50 110 ; 
       51 40 110 ; 
       64 58 110 ; 
       65 58 110 ; 
       52 40 110 ; 
       66 58 110 ; 
       53 40 110 ; 
       54 40 110 ; 
       67 58 110 ; 
       55 40 110 ; 
       68 58 110 ; 
       69 58 110 ; 
       56 40 110 ; 
       70 58 110 ; 
       57 40 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       58 4 300 ; 
       58 5 300 ; 
       58 6 300 ; 
       40 7 300 ; 
       40 8 300 ; 
       40 9 300 ; 
       8 20 300 ; 
       9 20 300 ; 
       10 2 300 ; 
       11 20 300 ; 
       12 1 300 ; 
       13 20 300 ; 
       14 19 300 ; 
       15 19 300 ; 
       16 19 300 ; 
       17 19 300 ; 
       38 13 300 ; 
       38 14 300 ; 
       38 15 300 ; 
       39 10 300 ; 
       39 11 300 ; 
       39 12 300 ; 
       41 16 300 ; 
       43 3 300 ; 
       50 0 300 ; 
       51 17 300 ; 
       64 18 300 ; 
       65 18 300 ; 
       52 17 300 ; 
       66 18 300 ; 
       53 17 300 ; 
       54 17 300 ; 
       67 18 300 ; 
       55 17 300 ; 
       68 18 300 ; 
       69 18 300 ; 
       56 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       50 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 1 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000011 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       34 SCHEM 47.58125 11.80334 0 MPRFLG 0 ; 
       46 SCHEM 67.58125 9.803335 0 MPRFLG 0 ; 
       58 SCHEM 54.16056 -1.053876 0 USR SRT 0.9999999 0.9999999 1 -1.125474e-007 3.141593 2.500178e-008 -30.79553 -5.343078 -17.39048 MPRFLG 0 ; 
       40 SCHEM 61.55548 4.3947 0 USR SRT 0.9999999 0.9999999 1 -1.092888e-008 6.283185 -9.095971e-008 -25.83028 -0.1298406 -9.034264 MPRFLG 0 ; 
       22 SCHEM 45.08125 11.80334 0 MPRFLG 0 ; 
       0 SCHEM 7.5 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 12.49999 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 12.49999 -4 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 7.807066e-009 2.59454 -2.653347e-008 151.1425 -14.19661 75.7656 MPRFLG 0 ; 
       5 SCHEM 77.6628 -11.9962 0 USR SRT 1 1 1 0 1.570796 0 104.3961 -4.675931 9.374343 MPRFLG 0 ; 
       6 SCHEM 72.14562 -7.912387 0 USR SRT 1 1 1 0 0.4847831 0 11.05156 -3.000762 7.009114 MPRFLG 0 ; 
       7 SCHEM 61.28183 -10.72001 0 USR SRT 1 1 1 0 0 0 104.473 3.878042 8.341749 MPRFLG 0 ; 
       8 SCHEM 73.9128 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       9 SCHEM 76.4128 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       10 SCHEM 70.89562 -9.912388 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 78.9128 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       12 SCHEM 73.39564 -9.912388 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 81.41281 -13.9962 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       14 SCHEM 65.03183 -12.72001 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 62.53183 -12.72001 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 60.03184 -12.72001 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 57.53184 -12.72001 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 15.08125 9.803335 0 MPRFLG 0 ; 
       19 SCHEM 17.58125 9.803335 0 MPRFLG 0 ; 
       20 SCHEM 22.58125 9.803335 0 MPRFLG 0 ; 
       21 SCHEM 72.58125 9.803335 0 MPRFLG 0 ; 
       23 SCHEM 28.83125 11.80334 0 MPRFLG 0 ; 
       24 SCHEM 30.08125 9.803335 0 MPRFLG 0 ; 
       25 SCHEM 12.58125 11.80334 0 MPRFLG 0 ; 
       26 SCHEM 30.08125 13.80334 0 MPRFLG 0 ; 
       27 SCHEM 32.58126 9.803335 0 MPRFLG 0 ; 
       28 SCHEM 58.83125 11.80334 0 MPRFLG 0 ; 
       29 SCHEM 71.33125 11.80334 0 MPRFLG 0 ; 
       30 SCHEM 62.58124 9.803335 0 MPRFLG 0 ; 
       31 SCHEM 57.58125 9.803335 0 MPRFLG 0 ; 
       32 SCHEM 65.08124 9.803335 0 MPRFLG 0 ; 
       33 SCHEM 42.58125 9.803335 0 MPRFLG 0 ; 
       35 SCHEM 50.08125 9.803335 0 MPRFLG 0 ; 
       36 SCHEM 60.08125 9.803335 0 MPRFLG 0 ; 
       37 SCHEM 20.08125 9.803335 0 MPRFLG 0 ; 
       38 SCHEM 52.58125 9.803335 0 MPRFLG 0 ; 
       39 SCHEM 70.08125 9.803335 0 MPRFLG 0 ; 
       41 SCHEM 35.08126 9.803335 0 MPRFLG 0 ; 
       42 SCHEM 25.08125 9.803335 0 MPRFLG 0 ; 
       43 SCHEM 40.08126 9.803335 0 MPRFLG 0 ; 
       44 SCHEM 27.58125 9.803335 0 MPRFLG 0 ; 
       45 SCHEM 37.58126 9.803335 0 MPRFLG 0 ; 
       59 SCHEM 52.76692 -3.140975 0 WIRECOL 9 7 MPRFLG 0 ; 
       60 SCHEM 57.76692 -3.140975 0 WIRECOL 9 7 MPRFLG 0 ; 
       61 SCHEM 55.26692 -3.140975 0 WIRECOL 9 7 MPRFLG 0 ; 
       62 SCHEM 62.76692 -3.140975 0 WIRECOL 9 7 MPRFLG 0 ; 
       63 SCHEM 60.26692 -3.140975 0 WIRECOL 9 7 MPRFLG 0 ; 
       47 SCHEM 67.56584 2.402156 0 WIRECOL 9 7 MPRFLG 0 ; 
       48 SCHEM 55.08125 9.803335 0 MPRFLG 0 ; 
       49 SCHEM 61.33125 13.80334 0 MPRFLG 0 ; 
       50 SCHEM 42.58125 15.80334 0 USR SRT 5.284098 7.138733 4.714693 1.527214 0.0445713 0.4540234 -4.702679e-006 -3.687694 -2.421114 MPRFLG 0 ; 
       51 SCHEM 55.06584 2.402156 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 42.76692 -3.140975 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 37.76692 -3.140975 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 50.06584 2.402156 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 40.26692 -3.140975 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 52.56584 2.402156 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 62.56584 2.402156 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 50.26692 -3.140975 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 57.56584 2.402156 0 WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 45.26692 -3.140975 0 WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 47.76692 -3.140975 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 60.06584 2.402156 0 WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 65.26691 -3.140975 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 65.06583 2.402156 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 5 0 0 DISPLAY 0 0 SRT 1.711165 0.5905364 1.162 0 1.570796 0 -48.73319 11.45754 -1.367697 MPRFLG 0 ; 
       72 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1.316 1.316 1.316 -1.078445e-008 2.381581e-008 -4.766174e-008 27.36609 -20.99654 -35.87016 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 135.9358 -13.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 162.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 160.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 142.5238 -41.39674 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM -38.85463 -26.91828 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -37.8098 -26.64572 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -42.48878 -25.60089 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 98.43384 -21.91643 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 59.3381 -37.39674 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -51.71501 -31.64511 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM -66.07942 -25.59969 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 147.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 150.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 161.7059 -8.188705 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 161.7059 -8.188705 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 161.7059 -8.188705 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -10.10461 -18.91828 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -9.059784 -18.64572 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -13.73877 -17.60089 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 135.9358 -13.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 142.5238 -43.39674 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -38.85463 -28.91828 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -37.8098 -28.64572 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -42.48878 -27.60089 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 98.43384 -23.91643 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59.3381 -39.39674 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 161.7059 -10.18871 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 161.7059 -10.18871 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 161.7059 -10.18871 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM -10.10461 -20.91828 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM -9.059784 -20.64572 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -13.73877 -19.60089 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
