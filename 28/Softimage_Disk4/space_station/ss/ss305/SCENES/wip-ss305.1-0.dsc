SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.5-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       ss305-mat2.1-0 ; 
       ss305-mat58.1-0 ; 
       ss305-mat59.1-0 ; 
       ss305-mat60.1-0 ; 
       ss305-mat61.1-0 ; 
       ss305-mat62.1-0 ; 
       ss305-mat63.1-0 ; 
       ss305-mat64.1-0 ; 
       ss305-mat65.1-0 ; 
       ss305-mat66.1-0 ; 
       ss305-mat67.1-0 ; 
       ss305_elect_station-mat52.1-0 ; 
       ss305_elect_station-mat53.1-0 ; 
       ss305_elect_station-mat54.1-0 ; 
       ss305_elect_station-mat55.1-0 ; 
       ss305_elect_station-mat56.1-0 ; 
       ss305_elect_station-mat57.1-0 ; 
       ss305_elect_station-mat7.1-0 ; 
       ss305_elect_station-white_strobe1_25.1-0 ; 
       ss305_elect_station-white_strobe1_31.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       2-skin2.1-0 ROOT ; 
       3-skin2.1-0 ROOT ; 
       ss100_nulls-extru44.4-0 ROOT ; 
       ss303_ordinance-cube1.1-0 ; 
       ss303_ordinance-cube10.1-0 ; 
       ss303_ordinance-cube11.1-0 ; 
       ss303_ordinance-cube12.1-0 ; 
       ss303_ordinance-cube13.1-0 ; 
       ss303_ordinance-cube2.1-0 ; 
       ss303_ordinance-cube3.1-0 ROOT ; 
       ss303_ordinance-cube4.1-0 ; 
       ss303_ordinance-cube5.2-0 ROOT ; 
       ss303_ordinance-cube6.1-0 ; 
       ss303_ordinance-cube7.1-0 ; 
       ss303_ordinance-cube8.1-0 ; 
       ss303_ordinance-cube9.1-0 ; 
       ss303_ordinance-cyl1.1-0 ; 
       ss303_ordinance-sphere1.5-0 ROOT ; 
       ss305-bmerge1.1-0 ROOT ; 
       ss305-east_bay_11_10.1-0 ROOT ; 
       ss305-null.1-0 ROOT ; 
       ss305-polygon.1-0 ; 
       ss305-polygon1.1-0 ; 
       ss305-polygon2.1-0 ; 
       ss305_elect_station-east_bay_11_8.1-0 ; 
       ss305_elect_station-east_bay_11_9.1-0 ; 
       ss305_elect_station-garage1A.1-0 ; 
       ss305_elect_station-garage1B.1-0 ; 
       ss305_elect_station-garage1C.1-0 ; 
       ss305_elect_station-garage1D.1-0 ; 
       ss305_elect_station-garage1E.1-0 ; 
       ss305_elect_station-landing_lights_2.1-0 ; 
       ss305_elect_station-landing_lights_3.1-0 ; 
       ss305_elect_station-landing_lights2.1-0 ; 
       ss305_elect_station-landing_lights2_3.1-0 ; 
       ss305_elect_station-launch1.1-0 ; 
       ss305_elect_station-null18.1-0 ; 
       ss305_elect_station-null19.4-0 ROOT ; 
       ss305_elect_station-null20.5-0 ROOT ; 
       ss305_elect_station-SS_11.1-0 ; 
       ss305_elect_station-SS_11_1.1-0 ; 
       ss305_elect_station-SS_13_2.1-0 ; 
       ss305_elect_station-SS_13_3.1-0 ; 
       ss305_elect_station-SS_15_1.1-0 ; 
       ss305_elect_station-SS_15_3.1-0 ; 
       ss305_elect_station-SS_23.1-0 ; 
       ss305_elect_station-SS_23_2.1-0 ; 
       ss305_elect_station-SS_24.1-0 ; 
       ss305_elect_station-SS_24_1.1-0 ; 
       ss305_elect_station-SS_26.1-0 ; 
       ss305_elect_station-SS_26_3.1-0 ; 
       ss305_elect_station-SS_29.1-0 ; 
       ss305_elect_station-SS_30.1-0 ; 
       ss305_elect_station-SS_31.1-0 ; 
       ss305_elect_station-SS_32.1-0 ; 
       ss305_elect_station-SS_33.1-0 ; 
       ss305_elect_station-SS_34.1-0 ; 
       ss305_elect_station-SS_35.1-0 ; 
       ss305_elect_station-SS_36.1-0 ; 
       ss305_elect_station-turwepemt2.1-0 ; 
       ss305_elect_station-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss305.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       ss305-t2d2.1-0 ; 
       ss305-t2d58.1-0 ; 
       ss305-t2d59.1-0 ; 
       ss305-t2d60.1-0 ; 
       ss305-t2d61.1-0 ; 
       ss305-t2d62.1-0 ; 
       ss305-t2d63.1-0 ; 
       ss305-t2d64.1-0 ; 
       ss305_elect_station-t2d52.1-0 ; 
       ss305_elect_station-t2d53.1-0 ; 
       ss305_elect_station-t2d54.1-0 ; 
       ss305_elect_station-t2d55.1-0 ; 
       ss305_elect_station-t2d56.1-0 ; 
       ss305_elect_station-t2d57.1-0 ; 
       ss305_elect_station-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 16 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 16 110 ; 
       10 11 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 14 110 ; 
       16 13 110 ; 
       24 38 110 ; 
       25 38 110 ; 
       26 24 110 ; 
       27 24 110 ; 
       28 24 110 ; 
       29 24 110 ; 
       30 24 110 ; 
       31 24 110 ; 
       32 25 110 ; 
       33 24 110 ; 
       34 25 110 ; 
       35 25 110 ; 
       36 38 110 ; 
       39 32 110 ; 
       40 31 110 ; 
       41 31 110 ; 
       42 32 110 ; 
       43 31 110 ; 
       44 32 110 ; 
       45 34 110 ; 
       46 33 110 ; 
       47 34 110 ; 
       48 33 110 ; 
       49 33 110 ; 
       50 34 110 ; 
       51 36 110 ; 
       52 36 110 ; 
       53 36 110 ; 
       54 36 110 ; 
       55 37 110 ; 
       56 37 110 ; 
       57 37 110 ; 
       58 37 110 ; 
       59 24 110 ; 
       60 25 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 17 300 ; 
       0 0 300 ; 
       24 14 300 ; 
       24 15 300 ; 
       24 16 300 ; 
       25 11 300 ; 
       25 12 300 ; 
       25 13 300 ; 
       39 18 300 ; 
       40 19 300 ; 
       41 19 300 ; 
       42 18 300 ; 
       43 19 300 ; 
       44 18 300 ; 
       45 18 300 ; 
       46 19 300 ; 
       47 18 300 ; 
       48 19 300 ; 
       49 19 300 ; 
       50 18 300 ; 
       51 21 300 ; 
       52 21 300 ; 
       53 21 300 ; 
       54 21 300 ; 
       55 20 300 ; 
       56 20 300 ; 
       57 20 300 ; 
       58 20 300 ; 
       19 1 300 ; 
       19 2 300 ; 
       19 3 300 ; 
       21 4 300 ; 
       22 5 300 ; 
       23 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       1 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       11 8 401 ; 
       0 0 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       10 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 9.018858 -2.881901 0 USR DISPLAY 0 0 SRT 0.786 0.786 0.786 0 0 0 -1.876221 -17.84419 14.1627 MPRFLG 0 ; 
       0 SCHEM 10.71606 4.051333 0 USR SRT 4.040069 7.056277 4.708705 0.2298919 -0.007112224 1.601179 0 -5.446305 0.1361576 MPRFLG 0 ; 
       3 SCHEM 6.725574 -36.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 14.22557 -34.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 16.72557 -34.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 19.22557 -34.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 21.72557 -34.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 9.225574 -36.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 24.01886 -2.881901 0 USR DISPLAY 0 0 SRT 11.36883 7.870412 11.36883 0 0.5235988 0 -0.9984375 -34.64139 1.329691 MPRFLG 0 ; 
       10 SCHEM 1.725569 -32.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 11.72557 -30.67479 0 USR DISPLAY 0 0 SRT 0.3162853 0.385533 0.9254849 0 0 0 63.15673 0.06861222 5.283679 MPRFLG 0 ; 
       12 SCHEM 4.225574 -32.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 14.22557 -32.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 11.72557 -34.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 11.72557 -36.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 7.975574 -34.67479 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 16.51886 -2.881901 0 USR DISPLAY 0 0 SRT 4.267863 4.267863 4.267863 0 0 0 0 -4.136997 0 MPRFLG 0 ; 
       24 SCHEM 18.61199 -18.67942 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM -6.388005 -18.67942 0 MPRFLG 0 ; 
       26 SCHEM 19.86199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 24.86199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 22.36199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 29.86199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 27.36199 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 7.361996 -20.67942 0 MPRFLG 0 ; 
       32 SCHEM -12.63801 -20.67942 0 MPRFLG 0 ; 
       33 SCHEM 14.86199 -20.67942 0 MPRFLG 0 ; 
       34 SCHEM -5.138005 -20.67942 0 MPRFLG 0 ; 
       35 SCHEM 2.361996 -20.67942 0 WIRECOL 9 7 MPRFLG 0 ; 
       36 SCHEM 38.612 -18.67942 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 24.71638 -27.03216 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       38 SCHEM 13.61199 -16.67942 0 USR SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM -10.13801 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 9.861995 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 4.861996 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM -15.13801 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 7.361996 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM -12.63801 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM -2.638005 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 17.36199 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM -7.638005 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 12.36199 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 14.86199 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM -5.138005 -22.67942 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 34.862 -20.67942 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 37.362 -20.67942 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 39.862 -20.67942 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 42.362 -20.67942 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 28.46638 -29.03216 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 25.96638 -29.03216 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 23.46638 -29.03216 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 20.96638 -29.03216 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 32.362 -20.67942 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM -0.1380049 -20.67942 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14.01886 -2.881901 0 USR DISPLAY 0 0 SRT 1 1 1 0 1.570796 0 -1.866284e-006 -0.2238874 5.218509 MPRFLG 0 ; 
       20 SCHEM 16.97867 2.684044 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.1237426 -3.81665 -1.383635 MPRFLG 0 ; 
       21 SCHEM 14.47867 0.6840441 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 16.97867 0.6840441 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 19.47867 0.6840441 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 19.01886 -2.881901 0 USR SRT 1 1 1 0 0 0 5.970013 0.01023519 12.52498 MPRFLG 0 ; 
       1 SCHEM 22.17569 7.99233 0 USR DISPLAY 1 2 SRT 4.040069 7.056277 4.708705 0.2298919 -0.007112224 1.601179 0 -5.446305 0.1361576 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       11 SCHEM 21.03819 5.787482 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 104 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 22.08301 6.060043 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 17.40403 7.104864 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 96.73093 -4.690975 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 8.177807 1.060651 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 8.815296 7.106074 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 117.2882 7.787482 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 118.333 8.060043 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 113.654 9.104864 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 94 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 99 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 111.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 114 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       8 SCHEM 21.03819 3.787482 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 22.08301 4.060043 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.40403 5.104864 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 96.73093 -6.690975 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 117.2882 5.787482 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 104 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 118.333 6.060043 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 113.654 7.104864 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 94 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 99 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 114 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
