SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.87-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 94     
       purpmeteor-mat98.2-0 ; 
       ss301_garrison-white_strobe1_33.3-0 ; 
       ss301_garrison-white_strobe1_34.3-0 ; 
       ss305_electronic-mat100.2-0 ; 
       ss305_electronic-mat101.2-0 ; 
       ss305_electronic-mat102.3-0 ; 
       ss305_electronic-mat103.2-0 ; 
       ss305_electronic-mat104.2-0 ; 
       ss305_electronic-mat105.2-0 ; 
       ss305_electronic-mat106.2-0 ; 
       ss305_electronic-mat107.2-0 ; 
       ss305_electronic-mat108.2-0 ; 
       ss305_electronic-mat109.3-0 ; 
       ss305_electronic-mat110.2-0 ; 
       ss305_electronic-mat111.1-0 ; 
       ss305_electronic-mat112.1-0 ; 
       ss305_electronic-mat113.1-0 ; 
       ss305_electronic-mat114.1-0 ; 
       ss305_electronic-mat115.1-0 ; 
       ss305_electronic-mat116.1-0 ; 
       ss305_electronic-mat117.1-0 ; 
       ss305_electronic-mat66.2-0 ; 
       ss305_electronic-mat67.4-0 ; 
       ss305_electronic-mat68.2-0 ; 
       ss305_electronic-mat69.3-0 ; 
       ss305_electronic-mat70.3-0 ; 
       ss305_electronic-mat72.3-0 ; 
       ss305_electronic-mat73.3-0 ; 
       ss305_electronic-mat74.3-0 ; 
       ss305_electronic-mat75.3-0 ; 
       ss305_electronic-mat76.3-0 ; 
       ss305_electronic-mat77.5-0 ; 
       ss305_electronic-mat78.3-0 ; 
       ss305_electronic-mat79.3-0 ; 
       ss305_electronic-mat80.3-0 ; 
       ss305_electronic-mat81.4-0 ; 
       ss305_electronic-mat82.4-0 ; 
       ss305_electronic-mat83.4-0 ; 
       ss305_electronic-mat84.3-0 ; 
       ss305_electronic-mat85.4-0 ; 
       ss305_electronic-mat86.3-0 ; 
       ss305_electronic-mat87.5-0 ; 
       ss305_electronic-mat88.3-0 ; 
       ss305_electronic-mat89.4-0 ; 
       ss305_electronic-mat91.3-0 ; 
       ss305_electronic-mat92.3-0 ; 
       ss305_electronic-mat93.4-0 ; 
       ss305_electronic-mat94.3-0 ; 
       ss305_electronic-mat95.3-0 ; 
       ss305_electronic-mat96.3-0 ; 
       ss305_electronic-mat97.3-0 ; 
       ss305_electronic-mat99.2-0 ; 
       ss305_electronic-white_strobe1_35.2-0 ; 
       ss305_electronic-white_strobe1_36.2-0 ; 
       ss305_electronic-white_strobe1_37.2-0 ; 
       ss305_electronic-white_strobe1_38.2-0 ; 
       ss305_electronic-white_strobe1_39.2-0 ; 
       ss305_electronic-white_strobe1_40.2-0 ; 
       ss305_electronic-white_strobe1_41.2-0 ; 
       ss305_electronic-white_strobe1_42.2-0 ; 
       ss305_electronic-white_strobe1_43.2-0 ; 
       ss305_electronic-white_strobe1_44.2-0 ; 
       ss305_electronic-white_strobe1_45.2-0 ; 
       ss305_electronic-white_strobe1_46.2-0 ; 
       ss305_electronic-white_strobe1_47.2-0 ; 
       ss305_electronic-white_strobe1_48.2-0 ; 
       ss305_electronic-white_strobe1_49.2-0 ; 
       ss305_electronic-white_strobe1_50.2-0 ; 
       ss305_electronic-white_strobe1_51.2-0 ; 
       ss305_electronic-white_strobe1_52.2-0 ; 
       ss305_electronic-white_strobe1_53.2-0 ; 
       ss305_electronic-white_strobe1_54.2-0 ; 
       ss305_electronic-white_strobe1_55.2-0 ; 
       ss305_electronic-white_strobe1_56.2-0 ; 
       ss305_electronic-white_strobe1_57.2-0 ; 
       ss305_electronic-white_strobe1_64.1-0 ; 
       ss305_electronic-white_strobe1_65.1-0 ; 
       ss305_electronic-white_strobe1_66.1-0 ; 
       ss305_electronic-white_strobe1_67.1-0 ; 
       ss305_electronic-white_strobe1_68.1-0 ; 
       ss305_electronic-white_strobe1_69.1-0 ; 
       ss305_elect_station-mat52.5-0 ; 
       ss305_elect_station-mat53.5-0 ; 
       ss305_elect_station-mat54.5-0 ; 
       ss305_elect_station-mat55.6-0 ; 
       ss305_elect_station-mat55_1.1-0 ; 
       ss305_elect_station-mat56.6-0 ; 
       ss305_elect_station-mat56_1.1-0 ; 
       ss305_elect_station-mat57.6-0 ; 
       ss305_elect_station-mat57_1.1-0 ; 
       ss305_elect_station-mat7.6-0 ; 
       ss305_elect_station-white_strobe1_31.5-0 ; 
       ss305_elect_station-white_strobe1_5.4-0 ; 
       ss305_elect_station-white_strobe1_9.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 96     
       root-bay_1.1-0 ; 
       root-bay_2.1-0 ; 
       root-cube10.1-0 ; 
       root-cube12.1-0 ; 
       root-cube13.1-0 ; 
       root-cube13_1.1-0 ; 
       root-cube13_2.1-0 ; 
       root-cube18_1.2-0 ; 
       root-cube19.1-0 ; 
       root-cube20.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube23.1-0 ; 
       root-cube25.1-0 ; 
       root-cube26.1-0 ; 
       root-cube26_1.1-0 ; 
       root-cube26_2.1-0 ; 
       root-cube27.1-0 ; 
       root-cube28.1-0 ; 
       root-cube3_1.1-0 ; 
       root-cube3_2.1-0 ; 
       root-cube4.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-east_bay_11_9.1-0 ; 
       root-extru44.1-0 ; 
       root-extru49.1-0 ; 
       root-extru50.1-0 ; 
       root-extru51.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-null18_1_1.3-0 ; 
       root-null18_3.3-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-null22.1-0 ; 
       root-null23.1-0 ; 
       root-null25.1-0 ; 
       root-root.59-0 ROOT ; 
       root-sphere9.5-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
       root-SS_38.1-0 ; 
       root-SS_39.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
       root-SS_70.1-0 ; 
       root-SS_71.1-0 ; 
       root-SS_72.1-0 ; 
       root-SS_73.1-0 ; 
       root-SS_74.1-0 ; 
       root-SS_75.1-0 ; 
       root-SS_85.1-0 ; 
       root-SS_86.1-0 ; 
       root-SS_87.1-0 ; 
       root-SS_88.1-0 ; 
       root-SS_89.1-0 ; 
       root-SS_90.1-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
       root-tetra3.1-0 ; 
       root-tetra4.1-0 ; 
       root-tetra5.1-0 ; 
       root-tetra6.1-0 ; 
       root-turwepemt.1-0 ; 
       root-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/beltersbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/bgrnd53 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss305/PICTURES/ss305 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       z_buffer-ss305-electronic.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 58     
       purpmeteor-t2d1.9-0 ; 
       ss305_electronic-t2d100.6-0 ; 
       ss305_electronic-t2d101.5-0 ; 
       ss305_electronic-t2d102.3-0 ; 
       ss305_electronic-t2d103.3-0 ; 
       ss305_electronic-t2d104.3-0 ; 
       ss305_electronic-t2d105.5-0 ; 
       ss305_electronic-t2d106.3-0 ; 
       ss305_electronic-t2d107.3-0 ; 
       ss305_electronic-t2d108.4-0 ; 
       ss305_electronic-t2d109.1-0 ; 
       ss305_electronic-t2d110.1-0 ; 
       ss305_electronic-t2d111.1-0 ; 
       ss305_electronic-t2d65.9-0 ; 
       ss305_electronic-t2d66.8-0 ; 
       ss305_electronic-t2d67.8-0 ; 
       ss305_electronic-t2d68.7-0 ; 
       ss305_electronic-t2d69.7-0 ; 
       ss305_electronic-t2d70.6-0 ; 
       ss305_electronic-t2d71.9-0 ; 
       ss305_electronic-t2d72.8-0 ; 
       ss305_electronic-t2d73.7-0 ; 
       ss305_electronic-t2d74.7-0 ; 
       ss305_electronic-t2d75.8-0 ; 
       ss305_electronic-t2d76.7-0 ; 
       ss305_electronic-t2d77.6-0 ; 
       ss305_electronic-t2d78.6-0 ; 
       ss305_electronic-t2d79.7-0 ; 
       ss305_electronic-t2d80.7-0 ; 
       ss305_electronic-t2d81.7-0 ; 
       ss305_electronic-t2d82.7-0 ; 
       ss305_electronic-t2d83.8-0 ; 
       ss305_electronic-t2d84.7-0 ; 
       ss305_electronic-t2d85.8-0 ; 
       ss305_electronic-t2d86.7-0 ; 
       ss305_electronic-t2d87.6-0 ; 
       ss305_electronic-t2d88.6-0 ; 
       ss305_electronic-t2d89.7-0 ; 
       ss305_electronic-t2d90.6-0 ; 
       ss305_electronic-t2d91.6-0 ; 
       ss305_electronic-t2d92.6-0 ; 
       ss305_electronic-t2d93.8-0 ; 
       ss305_electronic-t2d94.5-0 ; 
       ss305_electronic-t2d95.6-0 ; 
       ss305_electronic-t2d96.5-0 ; 
       ss305_electronic-t2d97.5-0 ; 
       ss305_electronic-t2d98.5-0 ; 
       ss305_electronic-t2d99.6-0 ; 
       ss305_elect_station-t2d52.11-0 ; 
       ss305_elect_station-t2d53.11-0 ; 
       ss305_elect_station-t2d54.11-0 ; 
       ss305_elect_station-t2d55.10-0 ; 
       ss305_elect_station-t2d55_1.3-0 ; 
       ss305_elect_station-t2d56.10-0 ; 
       ss305_elect_station-t2d56_1.3-0 ; 
       ss305_elect_station-t2d57.10-0 ; 
       ss305_elect_station-t2d57_1.3-0 ; 
       ss305_elect_station-t2d7.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 42 110 ; 
       1 42 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 13 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 7 110 ; 
       9 10 110 ; 
       10 42 110 ; 
       11 7 110 ; 
       12 37 110 ; 
       13 37 110 ; 
       14 12 110 ; 
       15 12 110 ; 
       16 12 110 ; 
       17 7 110 ; 
       18 10 110 ; 
       19 12 110 ; 
       20 15 110 ; 
       21 7 110 ; 
       22 12 110 ; 
       23 13 110 ; 
       24 7 110 ; 
       25 7 110 ; 
       26 7 110 ; 
       27 12 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 0 110 ; 
       31 0 110 ; 
       32 0 110 ; 
       33 1 110 ; 
       34 42 110 ; 
       35 42 110 ; 
       36 12 110 ; 
       37 42 110 ; 
       38 42 110 ; 
       39 42 110 ; 
       40 42 110 ; 
       42 41 110 ; 
       43 35 110 ; 
       44 35 110 ; 
       45 35 110 ; 
       46 35 110 ; 
       47 38 110 ; 
       48 38 110 ; 
       49 38 110 ; 
       50 38 110 ; 
       51 38 110 ; 
       52 38 110 ; 
       53 38 110 ; 
       54 38 110 ; 
       55 34 110 ; 
       56 34 110 ; 
       57 34 110 ; 
       58 34 110 ; 
       59 34 110 ; 
       60 39 110 ; 
       61 39 110 ; 
       62 39 110 ; 
       63 39 110 ; 
       64 39 110 ; 
       65 39 110 ; 
       66 39 110 ; 
       67 39 110 ; 
       68 40 110 ; 
       69 40 110 ; 
       70 40 110 ; 
       71 40 110 ; 
       72 40 110 ; 
       73 40 110 ; 
       74 40 110 ; 
       75 40 110 ; 
       76 0 110 ; 
       77 0 110 ; 
       78 0 110 ; 
       79 0 110 ; 
       80 0 110 ; 
       81 0 110 ; 
       82 1 110 ; 
       83 1 110 ; 
       84 1 110 ; 
       85 1 110 ; 
       86 1 110 ; 
       87 1 110 ; 
       88 13 110 ; 
       89 13 110 ; 
       90 13 110 ; 
       91 12 110 ; 
       92 2 110 ; 
       93 2 110 ; 
       94 0 110 ; 
       95 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 84 300 ; 
       0 86 300 ; 
       0 88 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       2 43 300 ; 
       2 3 300 ; 
       3 47 300 ; 
       4 46 300 ; 
       4 15 300 ; 
       5 31 300 ; 
       5 12 300 ; 
       5 14 300 ; 
       6 37 300 ; 
       7 39 300 ; 
       7 51 300 ; 
       8 41 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       9 36 300 ; 
       9 17 300 ; 
       10 35 300 ; 
       10 16 300 ; 
       11 42 300 ; 
       12 22 300 ; 
       12 7 300 ; 
       12 9 300 ; 
       13 30 300 ; 
       13 10 300 ; 
       13 11 300 ; 
       14 25 300 ; 
       15 24 300 ; 
       15 8 300 ; 
       16 26 300 ; 
       17 50 300 ; 
       17 4 300 ; 
       18 38 300 ; 
       19 27 300 ; 
       20 13 300 ; 
       21 40 300 ; 
       22 85 300 ; 
       22 87 300 ; 
       22 89 300 ; 
       23 81 300 ; 
       23 82 300 ; 
       23 83 300 ; 
       24 90 300 ; 
       25 48 300 ; 
       26 49 300 ; 
       27 28 300 ; 
       36 23 300 ; 
       37 21 300 ; 
       42 0 300 ; 
       43 93 300 ; 
       44 93 300 ; 
       45 93 300 ; 
       46 93 300 ; 
       47 92 300 ; 
       48 92 300 ; 
       49 92 300 ; 
       50 92 300 ; 
       51 52 300 ; 
       52 53 300 ; 
       53 54 300 ; 
       54 55 300 ; 
       55 2 300 ; 
       56 1 300 ; 
       57 56 300 ; 
       58 57 300 ; 
       59 58 300 ; 
       60 59 300 ; 
       61 60 300 ; 
       62 61 300 ; 
       63 62 300 ; 
       64 63 300 ; 
       65 64 300 ; 
       66 65 300 ; 
       67 66 300 ; 
       68 67 300 ; 
       69 68 300 ; 
       70 69 300 ; 
       71 70 300 ; 
       72 71 300 ; 
       73 72 300 ; 
       74 73 300 ; 
       75 74 300 ; 
       76 91 300 ; 
       77 91 300 ; 
       78 91 300 ; 
       79 91 300 ; 
       80 91 300 ; 
       81 91 300 ; 
       82 78 300 ; 
       83 77 300 ; 
       84 76 300 ; 
       85 75 300 ; 
       86 80 300 ; 
       87 79 300 ; 
       88 34 300 ; 
       89 32 300 ; 
       90 33 300 ; 
       91 29 300 ; 
       92 45 300 ; 
       93 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       42 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 16 401 ; 
       4 18 401 ; 
       5 20 401 ; 
       6 21 401 ; 
       7 24 401 ; 
       8 26 401 ; 
       9 27 401 ; 
       10 29 401 ; 
       11 30 401 ; 
       12 32 401 ; 
       13 39 401 ; 
       14 44 401 ; 
       15 47 401 ; 
       16 1 401 ; 
       17 2 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       22 23 401 ; 
       24 25 401 ; 
       25 45 401 ; 
       26 46 401 ; 
       27 38 401 ; 
       28 35 401 ; 
       29 6 401 ; 
       30 28 401 ; 
       31 31 401 ; 
       32 5 401 ; 
       33 4 401 ; 
       34 3 401 ; 
       35 33 401 ; 
       36 34 401 ; 
       37 40 401 ; 
       38 9 401 ; 
       39 13 401 ; 
       40 22 401 ; 
       41 19 401 ; 
       42 42 401 ; 
       43 15 401 ; 
       44 8 401 ; 
       45 7 401 ; 
       46 41 401 ; 
       47 43 401 ; 
       48 37 401 ; 
       49 36 401 ; 
       50 17 401 ; 
       51 14 401 ; 
       81 48 401 ; 
       82 49 401 ; 
       83 50 401 ; 
       84 51 401 ; 
       85 52 401 ; 
       86 53 401 ; 
       87 54 401 ; 
       88 55 401 ; 
       89 56 401 ; 
       90 57 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 161.9618 -6.879369 0 USR MPRFLG 0 ; 
       1 SCHEM 147.8283 -4.894687 0 USR MPRFLG 0 ; 
       2 SCHEM 199.9036 -8 0 MPRFLG 0 ; 
       3 SCHEM 206.1536 -8 0 MPRFLG 0 ; 
       4 SCHEM 212.4036 -8 0 MPRFLG 0 ; 
       5 SCHEM 301.1536 -8 0 MPRFLG 0 ; 
       6 SCHEM 243.6536 -6 0 MPRFLG 0 ; 
       7 SCHEM 218.6536 -6 0 MPRFLG 0 ; 
       8 SCHEM 221.1536 -8 0 MPRFLG 0 ; 
       9 SCHEM 192.4036 -6 0 MPRFLG 0 ; 
       10 SCHEM 221.1536 -4 0 MPRFLG 0 ; 
       11 SCHEM 226.1536 -8 0 MPRFLG 0 ; 
       12 SCHEM 271.1536 -6 0 MPRFLG 0 ; 
       13 SCHEM 304.9036 -6 0 MPRFLG 0 ; 
       14 SCHEM 273.6536 -8 0 MPRFLG 0 ; 
       15 SCHEM 266.1536 -8 0 MPRFLG 0 ; 
       16 SCHEM 276.1536 -8 0 MPRFLG 0 ; 
       17 SCHEM 234.9036 -8 0 MPRFLG 0 ; 
       18 SCHEM 246.1536 -6 0 MPRFLG 0 ; 
       19 SCHEM 271.1536 -8 0 MPRFLG 0 ; 
       20 SCHEM 263.6536 -10 0 MPRFLG 0 ; 
       21 SCHEM 208.6536 -8 0 MPRFLG 0 ; 
       22 SCHEM 256.1536 -8 0 MPRFLG 0 ; 
       23 SCHEM 293.6536 -8 0 MPRFLG 0 ; 
       24 SCHEM 228.6536 -8 0 MPRFLG 0 ; 
       25 SCHEM 216.1536 -8 0 MPRFLG 0 ; 
       26 SCHEM 231.1536 -8 0 MPRFLG 0 ; 
       27 SCHEM 278.6536 -8 0 MPRFLG 0 ; 
       28 SCHEM 170.1136 -11.83213 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 160.1136 -11.83213 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 162.6136 -11.83213 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 165.1136 -11.83213 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 167.6136 -11.83213 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 147.7731 -9.4522 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 136.1536 -4 0 MPRFLG 0 ; 
       35 SCHEM 124.9036 -4 0 MPRFLG 0 ; 
       36 SCHEM 261.1536 -8 0 MPRFLG 0 ; 
       37 SCHEM 287.4036 -4 0 MPRFLG 0 ; 
       38 SCHEM 179.9036 -4 0 MPRFLG 0 ; 
       39 SCHEM 52.40359 -4 0 MPRFLG 0 ; 
       40 SCHEM 72.40359 -4 0 MPRFLG 0 ; 
       41 SCHEM 184.9036 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       42 SCHEM 184.9036 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       43 SCHEM 121.1536 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 123.6536 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 126.1536 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 128.6536 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 186.1536 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 181.1536 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 176.1536 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 171.1536 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 183.6536 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 178.6536 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 173.6536 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 188.6536 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 131.1536 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 133.6536 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 136.1536 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 138.6536 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 141.1536 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 43.65359 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 46.15359 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 48.65359 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 51.15359 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 53.65359 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 56.15359 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 58.65359 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 61.15359 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 63.65359 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       69 SCHEM 66.15359 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       70 SCHEM 68.65359 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       71 SCHEM 71.15359 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       72 SCHEM 73.65359 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 76.15359 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 78.65359 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 81.15359 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       76 SCHEM 160.7118 -8.87937 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 163.2118 -8.87937 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 165.7118 -8.87937 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 168.2118 -8.87937 0 WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 170.7117 -8.87937 0 WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 173.2117 -8.87937 0 WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 154.0783 -6.894687 0 WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 151.5783 -6.894687 0 WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 149.0783 -6.894687 0 WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 146.5783 -6.894687 0 WIRECOL 3 7 MPRFLG 0 ; 
       86 SCHEM 159.0783 -6.894687 0 WIRECOL 3 7 MPRFLG 0 ; 
       87 SCHEM 156.5783 -6.894687 0 WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 306.1536 -8 0 MPRFLG 0 ; 
       89 SCHEM 308.6536 -8 0 MPRFLG 0 ; 
       90 SCHEM 311.1536 -8 0 MPRFLG 0 ; 
       91 SCHEM 281.1536 -8 0 MPRFLG 0 ; 
       92 SCHEM 196.1536 -10 0 MPRFLG 0 ; 
       93 SCHEM 198.6536 -10 0 MPRFLG 0 ; 
       94 SCHEM 172.6227 -11.7867 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       95 SCHEM 149.9567 -9.546274 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 318.9036 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 132.6536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 130.1536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 201.1536 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 233.6536 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 218.6536 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 221.1536 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 284.1536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 266.1536 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 286.6536 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 313.9036 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 316.4036 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 301.4036 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 262.6536 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 303.9036 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 213.9036 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 250.1536 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 193.9036 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 355.7722 -5.105474 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 350.7722 -5.105474 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 353.2722 -5.105474 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 318.9036 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 282.6536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 260.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 267.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 272.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 275.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 270.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 277.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 280.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 311.4036 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 298.9036 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 307.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 310.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 305.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 247.6536 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 191.4036 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 242.6536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 245.1536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 237.6536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 207.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 222.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 225.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 203.9036 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 197.6536 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 195.1536 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 211.4036 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 205.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 215.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 230.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 235.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 240.1536 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 182.6536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 177.6536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 172.6536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 187.6536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 135.1536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 137.6536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 140.1536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 43.65359 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 46.15359 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 48.65359 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 51.15359 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 52.65359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 55.15359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 57.65359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 60.15359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 62.65359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 65.15359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 67.65359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 70.15359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 72.65359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 75.15359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 77.65359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 80.15359 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 333.2722 -7.105474 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 333.2722 -7.105474 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 333.2722 -7.105474 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 333.2722 -7.105474 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 333.2722 -7.105474 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 333.2722 -7.105474 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 291.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 293.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 290.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 255.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 252.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 254.1536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 227.6536 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 102.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 185.1536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 120.1536 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 320.4036 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 250.1536 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 193.9036 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 305.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 310.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 307.6536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 280.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 195.1536 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 197.6536 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 245.1536 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 355.7722 -7.105474 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 350.7722 -7.105474 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 353.2722 -7.105474 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 237.6536 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 240.1536 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 203.9036 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 201.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 235.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 233.6536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 222.6536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 218.6536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 221.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 207.6536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 282.6536 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 284.1536 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 267.6536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 266.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 286.6536 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 311.4036 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 313.9036 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 316.4036 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 298.9036 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 301.4036 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 247.6536 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 191.4036 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 277.6536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 230.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 215.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 270.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 262.6536 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 242.6536 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 211.4036 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 225.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 205.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 303.9036 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 272.6536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 275.1536 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 213.9036 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 290.6536 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 292.1536 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 289.1536 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 254.6536 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 251.6536 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 253.1536 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 226.6536 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
