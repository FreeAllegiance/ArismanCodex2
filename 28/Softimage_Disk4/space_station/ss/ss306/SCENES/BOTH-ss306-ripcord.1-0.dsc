SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.5-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       ss305_electronic-mat58.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss306_ripcord-white_strobe1_10.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bounding_model.1-0 ROOT ; 
       root-cube11.1-0 ; 
       root-cube12.1-0 ; 
       root-cube12_1.1-0 ; 
       root-cube13.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.2-0 ; 
       root-cube33.1-0 ; 
       root-cube34.1-0 ; 
       root-cube35.1-0 ; 
       root-cyl1.1-0 ; 
       root-extru48.1-0 ; 
       root-extru50.1-0 ; 
       root-null18.3-0 ; 
       root-null19.3-0 ; 
       root-root.1-0 ROOT ; 
       root-sphere6.7-0 ; 
       root-sphere7.1-0 ; 
       root-sphere8.3-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
       root-tetra1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-ss306-ripcord.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       ss305_electronic-t2d58.2-0 ; 
       ss306_ripcord-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 21 110 ; 
       7 22 110 ; 
       8 13 110 ; 
       9 22 110 ; 
       10 22 110 ; 
       11 21 110 ; 
       12 11 110 ; 
       13 21 110 ; 
       14 22 110 ; 
       15 6 110 ; 
       16 21 110 ; 
       17 13 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       21 20 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 18 110 ; 
       27 18 110 ; 
       28 19 110 ; 
       29 19 110 ; 
       30 19 110 ; 
       31 19 110 ; 
       32 19 110 ; 
       33 11 110 ; 
       0 5 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       16 0 300 ; 
       21 3 300 ; 
       24 2 300 ; 
       25 2 300 ; 
       26 2 300 ; 
       27 2 300 ; 
       28 1 300 ; 
       29 1 300 ; 
       30 1 300 ; 
       31 1 300 ; 
       32 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       3 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 18.51816 6.813061 0 MPRFLG 0 ; 
       7 SCHEM 8.51816 4.813061 0 MPRFLG 0 ; 
       8 SCHEM 31.01816 4.813061 0 MPRFLG 0 ; 
       9 SCHEM 11.01816 4.813061 0 MPRFLG 0 ; 
       10 SCHEM 13.51816 4.813061 0 MPRFLG 0 ; 
       11 SCHEM 27.26816 6.813061 0 MPRFLG 0 ; 
       12 SCHEM 26.01816 4.813061 0 MPRFLG 0 ; 
       13 SCHEM 32.26816 6.813061 0 MPRFLG 0 ; 
       14 SCHEM 16.01816 4.813061 0 MPRFLG 0 ; 
       15 SCHEM 18.51816 4.813061 0 MPRFLG 0 ; 
       16 SCHEM 23.51816 6.813061 0 MPRFLG 0 ; 
       17 SCHEM 33.51816 4.813061 0 MPRFLG 0 ; 
       18 SCHEM 24.21256 -2.835424 0 USR MPRFLG 0 ; 
       19 SCHEM 11.53421 -1.782466 0 USR MPRFLG 0 ; 
       21 SCHEM 21.01816 8.813061 0 USR MPRFLG 0 ; 
       22 SCHEM 12.26816 6.813061 0 MPRFLG 0 ; 
       23 SCHEM 21.01816 6.813061 0 MPRFLG 0 ; 
       24 SCHEM 20.46256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 22.96256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 25.46256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 27.96256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 14.59506 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 12.09506 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 9.595066 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 7.095066 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 16.78487 -5.450098 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 28.51816 4.813061 0 MPRFLG 0 ; 
       0 SCHEM 36.01816 8.813061 0 MPRFLG 0 ; 
       1 SCHEM 38.51816 8.813061 0 MPRFLG 0 ; 
       2 SCHEM 41.01816 8.813061 0 MPRFLG 0 ; 
       3 SCHEM 43.51816 8.813061 0 MPRFLG 0 ; 
       5 SCHEM 39.71802 11.62156 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       20 SCHEM 20.8127 10.33117 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 45.71715 8.826524 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 224.8538 -81.6575 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 33.99998 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.49998 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.883644 0.2948585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 60.46951 10.2662 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 224.8538 -83.6575 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.855294 -0.741251 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
