SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.3-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       ss305_electronic-mat58.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss306_ripcord-white_strobe1_10.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       2-null18.3-0 ROOT ; 
       2-null19.3-0 ROOT ; 
       2-SS_29.1-0 ; 
       2-SS_30.1-0 ; 
       2-SS_31.1-0 ; 
       2-SS_32.1-0 ; 
       2-SS_33.1-0 ; 
       2-SS_34.1-0 ; 
       2-SS_35.1-0 ; 
       2-SS_36.1-0 ; 
       2-SS_37.1-0 ; 
       ss306_ripcord-cube11.1-0 ; 
       ss306_ripcord-cube12.1-0 ; 
       ss306_ripcord-cube12_1.1-0 ; 
       ss306_ripcord-cube13.1-0 ; 
       ss306_ripcord-cube14.1-0 ; 
       ss306_ripcord-cube15.2-0 ; 
       ss306_ripcord-cube3.1-0 ROOT ; 
       ss306_ripcord-cube33.1-0 ; 
       ss306_ripcord-cube34.1-0 ; 
       ss306_ripcord-cube8.1-0 ; 
       ss306_ripcord-cyl1.1-0 ; 
       ss306_ripcord-extru48.1-0 ; 
       ss306_ripcord-extru50.1-0 ; 
       ss306_ripcord-sphere6.6-0 ROOT ; 
       ss306_ripcord-sphere7.1-0 ; 
       ss306_ripcord-sphere8.3-0 ; 
       ss306_ripcord-tetra1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss306-ripcord.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       ss305_electronic-t2d58.1-0 ; 
       ss306_ripcord-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 1 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       13 19 110 ; 
       18 16 110 ; 
       19 24 110 ; 
       22 24 110 ; 
       23 19 110 ; 
       27 16 110 ; 
       11 24 110 ; 
       12 25 110 ; 
       14 25 110 ; 
       15 25 110 ; 
       16 24 110 ; 
       20 25 110 ; 
       21 11 110 ; 
       25 24 110 ; 
       26 24 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 4 300 ; 
       2 2 300 ; 
       3 2 300 ; 
       4 2 300 ; 
       5 2 300 ; 
       6 1 300 ; 
       7 1 300 ; 
       8 1 300 ; 
       9 1 300 ; 
       22 0 300 ; 
       24 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       3 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 16.78487 -5.450098 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       0 SCHEM 24.21256 -2.835424 0 USR SRT 1 1 1 0 1.570796 0 1.860769 -4.675931 -1.262103 MPRFLG 0 ; 
       1 SCHEM 11.53421 -1.782466 0 USR SRT 1 1 1 0 0 0 1.512217 3.878042 -1.869238 MPRFLG 0 ; 
       2 SCHEM 20.46256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       3 SCHEM 22.96256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 25.46256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       5 SCHEM 27.96256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       6 SCHEM 14.59506 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       7 SCHEM 12.09506 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       8 SCHEM 9.595066 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 7.095066 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 29.76816 4.813061 0 MPRFLG 0 ; 
       17 SCHEM 34.76816 6.813061 0 DISPLAY 0 0 SRT 5.89652 6.633319 5.141478 2.527293 -0.8496851 1.531107 -14.10519 1.615954 -12.38865 MPRFLG 0 ; 
       18 SCHEM 24.76816 4.813061 0 MPRFLG 0 ; 
       19 SCHEM 31.01816 6.813061 0 MPRFLG 0 ; 
       22 SCHEM 22.26816 6.813061 0 MPRFLG 0 ; 
       23 SCHEM 32.26816 4.813061 0 MPRFLG 0 ; 
       27 SCHEM 27.26816 4.813061 0 MPRFLG 0 ; 
       11 SCHEM 17.26816 6.813061 0 MPRFLG 0 ; 
       12 SCHEM 9.768161 4.813061 0 MPRFLG 0 ; 
       14 SCHEM 12.26816 4.813061 0 MPRFLG 0 ; 
       15 SCHEM 14.76816 4.813061 0 MPRFLG 0 ; 
       16 SCHEM 26.01816 6.813061 0 MPRFLG 0 ; 
       20 SCHEM 7.268162 4.813061 0 MPRFLG 0 ; 
       21 SCHEM 17.26816 4.813061 0 MPRFLG 0 ; 
       24 SCHEM 21.01816 8.813061 0 USR SRT 1.760344 1.760344 1.760344 1.119918 1.295611 1.137929 -2.277671 17.36169 0.1881921 MPRFLG 0 ; 
       25 SCHEM 11.01816 6.813061 0 MPRFLG 0 ; 
       26 SCHEM 19.76816 6.813061 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 60.46951 10.2662 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 224.8538 -81.6575 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 33.99998 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.49998 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.883644 0.2948585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 224.8538 -83.6575 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.855294 -0.741251 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
