SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.6-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       ss305_electronic-mat58.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss306_ripcord-white_strobe1_10.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       root-cube11.1-0 ; 
       root-cube12.1-0 ; 
       root-cube12_1.1-0 ; 
       root-cube13.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.2-0 ; 
       root-cube33.1-0 ; 
       root-cube34.1-0 ; 
       root-cube35.1-0 ; 
       root-cyl1.1-0 ; 
       root-extru48.1-0 ; 
       root-extru50.1-0 ; 
       root-null18.3-0 ; 
       root-null19.3-0 ; 
       root-root.1-0 ROOT ; 
       root-sphere6.7-0 ; 
       root-sphere7.1-0 ; 
       root-sphere8.3-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
       root-tetra1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss306-ripcord.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       ss305_electronic-t2d58.2-0 ; 
       ss306_ripcord-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 15 110 ; 
       1 16 110 ; 
       2 7 110 ; 
       3 16 110 ; 
       4 16 110 ; 
       5 15 110 ; 
       6 5 110 ; 
       7 15 110 ; 
       8 16 110 ; 
       9 0 110 ; 
       10 15 110 ; 
       11 7 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       15 14 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 12 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 13 110 ; 
       23 13 110 ; 
       24 13 110 ; 
       25 13 110 ; 
       26 13 110 ; 
       27 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 0 300 ; 
       15 3 300 ; 
       18 2 300 ; 
       19 2 300 ; 
       20 2 300 ; 
       21 2 300 ; 
       22 1 300 ; 
       23 1 300 ; 
       24 1 300 ; 
       25 1 300 ; 
       26 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       3 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.51816 6.813061 0 MPRFLG 0 ; 
       1 SCHEM 8.51816 4.813061 0 MPRFLG 0 ; 
       2 SCHEM 31.01816 4.813061 0 MPRFLG 0 ; 
       3 SCHEM 11.01816 4.813061 0 MPRFLG 0 ; 
       4 SCHEM 13.51816 4.813061 0 MPRFLG 0 ; 
       5 SCHEM 27.26816 6.813061 0 MPRFLG 0 ; 
       6 SCHEM 26.01816 4.813061 0 MPRFLG 0 ; 
       7 SCHEM 32.26816 6.813061 0 MPRFLG 0 ; 
       8 SCHEM 16.01816 4.813061 0 MPRFLG 0 ; 
       9 SCHEM 18.51816 4.813061 0 MPRFLG 0 ; 
       10 SCHEM 23.51816 6.813061 0 MPRFLG 0 ; 
       11 SCHEM 33.51816 4.813061 0 MPRFLG 0 ; 
       12 SCHEM 24.21256 -2.835424 0 USR MPRFLG 0 ; 
       13 SCHEM 11.53421 -1.782466 0 USR MPRFLG 0 ; 
       15 SCHEM 21.01816 8.813061 0 USR MPRFLG 0 ; 
       16 SCHEM 12.26816 6.813061 0 MPRFLG 0 ; 
       17 SCHEM 21.01816 6.813061 0 MPRFLG 0 ; 
       18 SCHEM 20.46256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       19 SCHEM 22.96256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 25.46256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 27.96256 -4.835425 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 14.59506 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 12.09506 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 9.595066 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 7.095066 -5.453141 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 16.78487 -5.450098 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 28.51816 4.813061 0 MPRFLG 0 ; 
       14 SCHEM 20.8127 10.33117 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 224.8538 -81.6575 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 33.99998 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.49998 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.883644 0.2948585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 60.46951 10.2662 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 224.8538 -83.6575 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.855294 -0.741251 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
