SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.43-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       ss306_ripcord-mat11.4-0 ; 
       ss306_ripcord-mat12.3-0 ; 
       ss306_ripcord-mat13.4-0 ; 
       ss306_ripcord-mat14.4-0 ; 
       ss306_ripcord-mat15.4-0 ; 
       ss306_ripcord-mat17.4-0 ; 
       ss306_ripcord-mat18.3-0 ; 
       ss306_ripcord-mat21.2-0 ; 
       ss306_ripcord-mat22.2-0 ; 
       ss306_ripcord-mat23.2-0 ; 
       ss306_ripcord-mat24.2-0 ; 
       ss306_ripcord-mat25.2-0 ; 
       ss306_ripcord-mat26.2-0 ; 
       ss306_ripcord-mat27.2-0 ; 
       ss306_ripcord-mat28.2-0 ; 
       ss306_ripcord-mat29.2-0 ; 
       ss306_ripcord-mat5.4-0 ; 
       ss306_ripcord-mat6.4-0 ; 
       ss306_ripcord-mat7.3-0 ; 
       ss306_ripcord-mat8.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       root-cube11.1-0 ; 
       root-cube12.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.2-0 ; 
       root-cube33.1-0 ; 
       root-cube34.1-0 ; 
       root-cube36.1-0 ; 
       root-cube37.1-0 ; 
       root-cyl1.1-0 ; 
       root-extru48.1-0 ; 
       root-root.29-0 ROOT ; 
       root-sphere6.7-0 ; 
       root-sphere7.1-0 ; 
       root-sphere8.3-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/ss306 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-ss306-ripcord.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       ss306_ripcord-t2d10.5-0 ; 
       ss306_ripcord-t2d11.4-0 ; 
       ss306_ripcord-t2d12.3-0 ; 
       ss306_ripcord-t2d13.3-0 ; 
       ss306_ripcord-t2d14.4-0 ; 
       ss306_ripcord-t2d15.4-0 ; 
       ss306_ripcord-t2d16.4-0 ; 
       ss306_ripcord-t2d17.4-0 ; 
       ss306_ripcord-t2d18.3-0 ; 
       ss306_ripcord-t2d19.5-0 ; 
       ss306_ripcord-t2d20.3-0 ; 
       ss306_ripcord-t2d21.4-0 ; 
       ss306_ripcord-t2d22.3-0 ; 
       ss306_ripcord-t2d23.3-0 ; 
       ss306_ripcord-t2d24.3-0 ; 
       ss306_ripcord-t2d25.4-0 ; 
       ss306_ripcord-t2d28.3-0 ; 
       ss306_ripcord-t2d7.6-0 ; 
       ss306_ripcord-t2d8.3-0 ; 
       ss306_ripcord-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 12 110 ; 
       2 12 110 ; 
       3 11 110 ; 
       4 3 110 ; 
       5 11 110 ; 
       6 12 110 ; 
       7 12 110 ; 
       8 0 110 ; 
       9 11 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 15 300 ; 
       1 19 300 ; 
       1 7 300 ; 
       2 18 300 ; 
       3 3 300 ; 
       3 14 300 ; 
       4 4 300 ; 
       4 12 300 ; 
       5 6 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       8 1 300 ; 
       9 5 300 ; 
       11 16 300 ; 
       12 17 300 ; 
       13 2 300 ; 
       13 13 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       1 8 401 ; 
       2 11 401 ; 
       3 9 401 ; 
       4 7 401 ; 
       5 14 401 ; 
       6 15 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       17 18 401 ; 
       18 19 401 ; 
       19 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 12.5 0 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 16.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24.16161 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24.16161 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 41.7957 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
