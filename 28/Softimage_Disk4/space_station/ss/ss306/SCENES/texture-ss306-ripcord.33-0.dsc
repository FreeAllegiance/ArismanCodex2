SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.39-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       ss305_elect_station-white_strobe1_5.2-0 ; 
       ss305_elect_station-white_strobe1_9.2-0 ; 
       ss306_ripcord-mat11.4-0 ; 
       ss306_ripcord-mat12.3-0 ; 
       ss306_ripcord-mat13.4-0 ; 
       ss306_ripcord-mat14.4-0 ; 
       ss306_ripcord-mat15.4-0 ; 
       ss306_ripcord-mat16.3-0 ; 
       ss306_ripcord-mat17.4-0 ; 
       ss306_ripcord-mat18.3-0 ; 
       ss306_ripcord-mat19.3-0 ; 
       ss306_ripcord-mat20.3-0 ; 
       ss306_ripcord-mat21.2-0 ; 
       ss306_ripcord-mat22.2-0 ; 
       ss306_ripcord-mat23.2-0 ; 
       ss306_ripcord-mat24.2-0 ; 
       ss306_ripcord-mat25.2-0 ; 
       ss306_ripcord-mat26.2-0 ; 
       ss306_ripcord-mat27.2-0 ; 
       ss306_ripcord-mat28.2-0 ; 
       ss306_ripcord-mat29.2-0 ; 
       ss306_ripcord-mat5.4-0 ; 
       ss306_ripcord-mat6.4-0 ; 
       ss306_ripcord-mat7.3-0 ; 
       ss306_ripcord-mat8.4-0 ; 
       ss306_ripcord-white_strobe1_10.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       root-cube11.1-0 ; 
       root-cube12.1-0 ; 
       root-cube12_1.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.2-0 ; 
       root-cube33.1-0 ; 
       root-cube34.1-0 ; 
       root-cube36.1-0 ; 
       root-cube37.1-0 ; 
       root-cyl1.1-0 ; 
       root-extru48.1-0 ; 
       root-extru50.1-0 ; 
       root-null18.3-0 ; 
       root-null19.3-0 ; 
       root-root.27-0 ROOT ; 
       root-sphere6.7-0 ; 
       root-sphere7.1-0 ; 
       root-sphere8.3-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
       root-tetra1_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss306/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss306/PICTURES/ss306 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-ss306-ripcord.33-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       ss306_ripcord-t2d10.4-0 ; 
       ss306_ripcord-t2d11.3-0 ; 
       ss306_ripcord-t2d12.2-0 ; 
       ss306_ripcord-t2d13.2-0 ; 
       ss306_ripcord-t2d14.3-0 ; 
       ss306_ripcord-t2d15.3-0 ; 
       ss306_ripcord-t2d16.3-0 ; 
       ss306_ripcord-t2d17.3-0 ; 
       ss306_ripcord-t2d18.2-0 ; 
       ss306_ripcord-t2d19.4-0 ; 
       ss306_ripcord-t2d20.2-0 ; 
       ss306_ripcord-t2d21.3-0 ; 
       ss306_ripcord-t2d22.2-0 ; 
       ss306_ripcord-t2d23.2-0 ; 
       ss306_ripcord-t2d24.2-0 ; 
       ss306_ripcord-t2d25.3-0 ; 
       ss306_ripcord-t2d26.2-0 ; 
       ss306_ripcord-t2d27.3-0 ; 
       ss306_ripcord-t2d28.2-0 ; 
       ss306_ripcord-t2d29.1-0 ; 
       ss306_ripcord-t2d7.5-0 ; 
       ss306_ripcord-t2d8.2-0 ; 
       ss306_ripcord-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 15 110 ; 
       1 16 110 ; 
       2 6 110 ; 
       3 16 110 ; 
       4 15 110 ; 
       5 4 110 ; 
       6 15 110 ; 
       7 16 110 ; 
       8 16 110 ; 
       9 0 110 ; 
       10 15 110 ; 
       11 6 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       15 14 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 12 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 13 110 ; 
       23 13 110 ; 
       24 13 110 ; 
       25 13 110 ; 
       26 13 110 ; 
       27 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 20 300 ; 
       1 24 300 ; 
       1 12 300 ; 
       2 10 300 ; 
       3 23 300 ; 
       4 5 300 ; 
       4 19 300 ; 
       5 6 300 ; 
       5 17 300 ; 
       6 9 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       9 3 300 ; 
       10 8 300 ; 
       11 11 300 ; 
       15 21 300 ; 
       16 22 300 ; 
       17 4 300 ; 
       17 18 300 ; 
       18 1 300 ; 
       19 1 300 ; 
       20 1 300 ; 
       21 1 300 ; 
       22 0 300 ; 
       23 0 300 ; 
       24 0 300 ; 
       25 0 300 ; 
       26 25 300 ; 
       27 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 6 401 ; 
       3 8 401 ; 
       4 11 401 ; 
       5 9 401 ; 
       6 7 401 ; 
       7 19 401 ; 
       8 14 401 ; 
       9 15 401 ; 
       10 17 401 ; 
       11 16 401 ; 
       12 1 401 ; 
       13 2 401 ; 
       14 3 401 ; 
       15 4 401 ; 
       16 5 401 ; 
       17 10 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 18 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 25 -8 0 MPRFLG 0 ; 
       1 SCHEM 51.25 -10 0 MPRFLG 0 ; 
       2 SCHEM 70 -10 0 MPRFLG 0 ; 
       3 SCHEM 55 -10 0 MPRFLG 0 ; 
       4 SCHEM 40 -8 0 MPRFLG 0 ; 
       5 SCHEM 36.25 -10 0 MPRFLG 0 ; 
       6 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 58.75 -10 0 MPRFLG 0 ; 
       8 SCHEM 63.75 -10 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       13 SCHEM 5 -8 0 MPRFLG 0 ; 
       14 SCHEM 38.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 58.75 -8 0 MPRFLG 0 ; 
       17 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       19 SCHEM 15 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 17.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 20 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 2.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 0 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 10 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 40 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 74.16161 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 74.16161 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 72.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 70 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 68.0457 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 40 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
