SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ripcord-cam_int1.1-0 ROOT ; 
       ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       ripcord-light1.1-0 ROOT ; 
       ripcord-light2.1-0 ROOT ; 
       ripcord-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       ripcord-default1.1-0 ; 
       ripcord-default2.1-0 ; 
       ripcord-mat2.1-0 ; 
       ripcord-mat3.1-0 ; 
       ripcord-mat4.1-0 ; 
       ripcord-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       aa_rock-aa-rock.1-0 ROOT ; 
       ripcord-cube1.1-0 ; 
       ripcord-cube10.1-0 ; 
       ripcord-cube11.1-0 ; 
       ripcord-cube2.1-0 ROOT ; 
       ripcord-cube5.1-0 ROOT ; 
       ripcord-cube7.1-0 ROOT ; 
       ripcord-cube8.1-0 ; 
       ripcord-cube9.1-0 ; 
       ripcord-cyl1.1-0 ; 
       ripcord-cyl2.1-0 ROOT ; 
       ripcord-sphere1.1-0 ROOT ; 
       ripcord-sphere3.1-0 ROOT ; 
       ripcord-sphere4.1-0 ROOT ; 
       ripcord-sphere5.1-0 ROOT ; 
       ripcord-sphere6.1-0 ROOT ; 
       ripcord-sphere7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/fig16 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss306/PICTURES/rendermap_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ripcord.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       ripcord-t2d2.1-0 ; 
       ripcord-t2d3.1-0 ; 
       ripcord-t2d4.1-0 ; 
       ripcord-t2d5.1-0 ; 
       ripcord-t2d6.1-0 ; 
       ripcord-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       16 15 110 ; 
       1 16 110 ; 
       7 16 110 ; 
       8 16 110 ; 
       2 16 110 ; 
       3 15 110 ; 
       9 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 3 300 ; 
       0 0 300 ; 
       11 1 300 ; 
       14 4 300 ; 
       12 2 300 ; 
       15 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 3 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       4 4 401 ; 
       5 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 40 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 42.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 45 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 37.5 0 0 DISPLAY 0 0 SRT 0.5840842 0.5840842 0.5840842 0.1382382 0.3618746 1.557862 0 11.61335 0 MPRFLG 0 ; 
       0 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 11.61335 0 MPRFLG 0 ; 
       11 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 15.0184 0.07754421 1.642731 MPRFLG 0 ; 
       14 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 20 0 0 DISPLAY 0 0 SRT 3.318661 3.318661 3.318661 0.1382382 0.3618746 1.557862 0 11.61335 0 MPRFLG 0 ; 
       15 SCHEM 7.5 0 0 SRT 1 1 1 -0.03243348 -3.255823 -0.2509054 1.712935 6.196995 0.8049829 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 3.533778 11.03926 0 USR DISPLAY 0 0 SRT 0.2735327 0.2735327 0.1973921 1.570796 1.794744e-017 -2.705519e-009 2.074605 3.205756 -0.7581509 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 8.533778 11.03926 0 USR DISPLAY 0 0 SRT 0.2735327 0.2735327 0.1973921 7.277335e-017 1.794744e-017 -2.705519e-009 2.521888 -4.233204 6.856525 MPRFLG 0 ; 
       5 SCHEM 6.033778 11.03926 0 USR DISPLAY 0 0 SRT 0.2735327 0.2735327 0.1973921 1.570796 1.794744e-017 -2.705519e-009 2.069344 -11.65062 -0.7581509 MPRFLG 0 ; 
       8 SCHEM 27.5 0 0 MPRFLG 0 ; 
       2 SCHEM 30 0 0 MPRFLG 0 ; 
       3 SCHEM 32.5 0 0 MPRFLG 0 ; 
       9 SCHEM 35 0 0 MPRFLG 0 ; 
       10 SCHEM 25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 1.570796 -0.8325656 -7.802487 -6.083595 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
