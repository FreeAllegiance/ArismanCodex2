SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.53-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss307_miningplat-mat6.2-0 ; 
       ss307_miningplat-white_strobe1_53.1-0 ; 
       ss307_miningplat-white_strobe1_54.1-0 ; 
       ss307_miningplat-white_strobe1_55.1-0 ; 
       ss307_miningplat-white_strobe1_56.1-0 ; 
       ss307_miningplat-white_strobe1_57.1-0 ; 
       ss307_miningplat-white_strobe1_58.1-0 ; 
       ss307_miningplat-white_strobe1_59.1-0 ; 
       ss307_miningplat-white_strobe1_60.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 71     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bound07.1-0 ; 
       bounding_model-bound08.1-0 ; 
       bounding_model-bound09.1-0 ; 
       bounding_model-bounding_model.1-0 ROOT ; 
       root-cube1.2-0 ; 
       root-cube11.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.1-0 ; 
       root-cube16.1-0 ; 
       root-cube17.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube21.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube5.1-0 ; 
       root-cube6.1-0 ; 
       root-cube7.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl1.1-0 ; 
       root-cyl1_1.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-root_1.10-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere6_1.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss307/PICTURES/bgrnd03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-ss307-miningplat.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       ss306_ripcord-t2d7.4-0 ; 
       ss307_miningplat-t2d8.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 9 110 ; 
       10 34 110 ; 
       11 10 110 ; 
       12 23 110 ; 
       13 11 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 11 110 ; 
       17 13 110 ; 
       18 13 110 ; 
       19 10 110 ; 
       20 10 110 ; 
       21 23 110 ; 
       22 21 110 ; 
       23 10 110 ; 
       24 19 110 ; 
       25 19 110 ; 
       26 10 110 ; 
       27 19 110 ; 
       28 34 110 ; 
       29 34 110 ; 
       30 34 110 ; 
       31 34 110 ; 
       32 34 110 ; 
       33 34 110 ; 
       35 10 110 ; 
       36 10 110 ; 
       37 32 110 ; 
       38 32 110 ; 
       39 30 110 ; 
       40 31 110 ; 
       41 30 110 ; 
       42 31 110 ; 
       43 30 110 ; 
       44 31 110 ; 
       45 30 110 ; 
       46 31 110 ; 
       47 29 110 ; 
       48 29 110 ; 
       2 9 110 ; 
       49 30 110 ; 
       50 31 110 ; 
       51 32 110 ; 
       52 32 110 ; 
       53 32 110 ; 
       54 32 110 ; 
       55 28 110 ; 
       56 28 110 ; 
       57 28 110 ; 
       58 28 110 ; 
       59 28 110 ; 
       60 28 110 ; 
       61 30 110 ; 
       62 31 110 ; 
       63 33 110 ; 
       64 33 110 ; 
       65 33 110 ; 
       66 33 110 ; 
       67 33 110 ; 
       68 33 110 ; 
       69 29 110 ; 
       70 29 110 ; 
       0 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       8 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       35 22 300 ; 
       36 23 300 ; 
       37 21 300 ; 
       38 21 300 ; 
       39 20 300 ; 
       40 4 300 ; 
       41 20 300 ; 
       42 3 300 ; 
       43 20 300 ; 
       44 2 300 ; 
       45 20 300 ; 
       46 1 300 ; 
       47 8 300 ; 
       48 7 300 ; 
       49 9 300 ; 
       50 5 300 ; 
       51 10 300 ; 
       52 11 300 ; 
       53 12 300 ; 
       54 13 300 ; 
       55 14 300 ; 
       56 15 300 ; 
       57 16 300 ; 
       58 17 300 ; 
       59 18 300 ; 
       60 19 300 ; 
       61 0 300 ; 
       62 6 300 ; 
       63 24 300 ; 
       64 25 300 ; 
       65 26 300 ; 
       66 27 300 ; 
       67 28 300 ; 
       68 29 300 ; 
       69 30 300 ; 
       70 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       22 0 401 ; 
       23 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 35.28148 -2.641874 0 MPRFLG 0 ; 
       10 SCHEM -7.706281 -13.88124 0 MPRFLG 0 ; 
       11 SCHEM 1.043719 -15.88124 0 MPRFLG 0 ; 
       12 SCHEM -3.956281 -17.88124 0 MPRFLG 0 ; 
       13 SCHEM -0.2062812 -17.88124 0 MPRFLG 0 ; 
       14 SCHEM 6.043719 -15.88124 0 MPRFLG 0 ; 
       15 SCHEM 8.543718 -15.88124 0 MPRFLG 0 ; 
       16 SCHEM 3.54372 -17.88124 0 MPRFLG 0 ; 
       17 SCHEM -1.456281 -19.88124 0 MPRFLG 0 ; 
       18 SCHEM 1.043719 -19.88124 0 MPRFLG 0 ; 
       19 SCHEM -13.95628 -15.88124 0 MPRFLG 0 ; 
       20 SCHEM -8.956281 -15.88124 0 MPRFLG 0 ; 
       21 SCHEM -6.456281 -17.88124 0 MPRFLG 0 ; 
       22 SCHEM -6.456281 -19.88124 0 MPRFLG 0 ; 
       23 SCHEM -5.206281 -15.88124 0 MPRFLG 0 ; 
       24 SCHEM -13.95628 -17.88124 0 MPRFLG 0 ; 
       25 SCHEM -11.45628 -17.88124 0 MPRFLG 0 ; 
       26 SCHEM -21.45628 -15.88124 0 MPRFLG 0 ; 
       27 SCHEM -16.45628 -17.88124 0 MPRFLG 0 ; 
       28 SCHEM 47.29372 -13.88124 0 MPRFLG 0 ; 
       29 SCHEM 89.79371 -13.88124 0 MPRFLG 0 ; 
       30 SCHEM 32.29372 -13.88124 0 MPRFLG 0 ; 
       31 SCHEM 17.29372 -13.88124 0 MPRFLG 0 ; 
       32 SCHEM 62.29372 -13.88124 0 MPRFLG 0 ; 
       33 SCHEM 77.29371 -13.88124 0 MPRFLG 0 ; 
       34 SCHEM 34.79372 -11.88124 0 USR SRT 1 1 1 0 0 0 24.59785 0 0.5428964 MPRFLG 0 ; 
       35 SCHEM -23.95628 -15.88124 0 MPRFLG 0 ; 
       36 SCHEM -18.95628 -15.88124 0 MPRFLG 0 ; 
       37 SCHEM 56.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 61.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 33.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 18.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 31.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 16.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 28.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 13.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 26.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 11.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 86.04371 -15.88124 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 88.54371 -15.88124 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.78148 -2.641874 0 MPRFLG 0 ; 
       49 SCHEM 36.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 21.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 58.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 63.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 66.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 68.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 41.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 43.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 46.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 48.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 51.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 53.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 38.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 23.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 76.04371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 71.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 78.54371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 73.54371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 81.04371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 83.54371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 91.04371 -15.88124 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 93.54371 -15.88124 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 32.78148 -2.641874 0 MPRFLG 0 ; 
       3 SCHEM 40.28148 -2.641874 0 MPRFLG 0 ; 
       4 SCHEM 42.78148 -2.641874 0 MPRFLG 0 ; 
       5 SCHEM 45.28148 -2.641874 0 MPRFLG 0 ; 
       6 SCHEM 47.78148 -2.641874 0 MPRFLG 0 ; 
       7 SCHEM 50.28148 -2.641874 0 MPRFLG 0 ; 
       9 SCHEM 42.78148 -0.6418741 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 52.78148 -2.641874 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 146.8747 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 126.9403 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 126.9403 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 126.9403 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 126.9403 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 167.9155 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 193.0932 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 85.72176 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 83.22176 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 121.697 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 149.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 154.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 159.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 164.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 180.0924 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 185.0924 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 190.0924 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 195.0924 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 171.6858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 179.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 80.72176 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 123.2217 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 153.2233 22.41722 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 181.6873 47.4792 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 158.2232 22.41722 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 191.6873 47.4792 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 199.1873 47.4792 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 206.6873 47.4792 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 133.2233 18.41722 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 138.2233 18.41722 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
