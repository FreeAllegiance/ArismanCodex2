SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.57-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 71     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bound07.1-0 ; 
       bounding_model-bound08.1-0 ; 
       bounding_model-bound09.1-0 ; 
       bounding_model-bounding_model_1.1-0 ROOT ; 
       root-cube1.2-0 ; 
       root-cube11.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.1-0 ; 
       root-cube17.1-0 ; 
       root-cube17_1.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube5.1-0 ; 
       root-cube6.1-0 ; 
       root-cube7.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl1.1-0 ; 
       root-cyl1_1.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-root.35-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere6_1.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss307/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss307/PICTURES/ss307 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-ss307-miningplat.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 34 110 ; 
       11 10 110 ; 
       12 24 110 ; 
       13 11 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 11 110 ; 
       17 13 110 ; 
       18 13 110 ; 
       19 20 110 ; 
       20 10 110 ; 
       21 10 110 ; 
       22 24 110 ; 
       23 22 110 ; 
       24 10 110 ; 
       25 20 110 ; 
       26 10 110 ; 
       27 20 110 ; 
       28 34 110 ; 
       29 34 110 ; 
       30 34 110 ; 
       31 34 110 ; 
       32 34 110 ; 
       33 34 110 ; 
       35 10 110 ; 
       36 10 110 ; 
       37 32 110 ; 
       38 32 110 ; 
       39 30 110 ; 
       40 31 110 ; 
       41 30 110 ; 
       42 31 110 ; 
       43 30 110 ; 
       44 31 110 ; 
       45 30 110 ; 
       46 31 110 ; 
       47 29 110 ; 
       48 29 110 ; 
       49 30 110 ; 
       50 31 110 ; 
       51 32 110 ; 
       52 32 110 ; 
       53 32 110 ; 
       54 32 110 ; 
       55 28 110 ; 
       56 28 110 ; 
       57 28 110 ; 
       58 28 110 ; 
       59 28 110 ; 
       60 28 110 ; 
       61 30 110 ; 
       62 31 110 ; 
       63 33 110 ; 
       64 33 110 ; 
       65 33 110 ; 
       66 33 110 ; 
       67 33 110 ; 
       68 33 110 ; 
       69 29 110 ; 
       70 29 110 ; 
       0 9 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       8 9 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 25 -8 0 MPRFLG 0 ; 
       12 SCHEM 20 -10 0 MPRFLG 0 ; 
       13 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       14 SCHEM 30 -8 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       18 SCHEM 25 -12 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 10 -8 0 MPRFLG 0 ; 
       21 SCHEM 15 -8 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       24 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       25 SCHEM 10 -10 0 MPRFLG 0 ; 
       26 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 71.25 -6 0 MPRFLG 0 ; 
       29 SCHEM 113.75 -6 0 MPRFLG 0 ; 
       30 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       31 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       32 SCHEM 86.25 -6 0 MPRFLG 0 ; 
       33 SCHEM 101.25 -6 0 MPRFLG 0 ; 
       34 SCHEM 58.75 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       35 SCHEM 0 -8 0 MPRFLG 0 ; 
       36 SCHEM 5 -8 0 MPRFLG 0 ; 
       37 SCHEM 80 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 85 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 57.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 42.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 55 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 40 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 52.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 50 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 35 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 60 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 45 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 82.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 87.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 90 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 92.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 65 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 67.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 70 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 72.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 75 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 77.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 62.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 47.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 100 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 95 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 102.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 97.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 105 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 107.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 43.85762 1.115388 0 MPRFLG 0 ; 
       1 SCHEM 46.35762 1.115388 0 MPRFLG 0 ; 
       2 SCHEM 48.85762 1.115388 0 MPRFLG 0 ; 
       3 SCHEM 51.35762 1.115388 0 MPRFLG 0 ; 
       4 SCHEM 53.85762 1.115388 0 MPRFLG 0 ; 
       5 SCHEM 56.35762 1.115388 0 MPRFLG 0 ; 
       6 SCHEM 58.85762 1.115388 0 MPRFLG 0 ; 
       7 SCHEM 61.35762 1.115388 0 MPRFLG 0 ; 
       8 SCHEM 63.85762 1.115388 0 MPRFLG 0 ; 
       9 SCHEM 53.85762 3.115388 0 USR SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
