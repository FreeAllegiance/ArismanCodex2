SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.56-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss307_miningplat-mat6.2-0 ; 
       ss307_miningplat-white_strobe1_53.1-0 ; 
       ss307_miningplat-white_strobe1_54.1-0 ; 
       ss307_miningplat-white_strobe1_55.1-0 ; 
       ss307_miningplat-white_strobe1_56.1-0 ; 
       ss307_miningplat-white_strobe1_57.1-0 ; 
       ss307_miningplat-white_strobe1_58.1-0 ; 
       ss307_miningplat-white_strobe1_59.1-0 ; 
       ss307_miningplat-white_strobe1_60.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       root-cube1.2-0 ; 
       root-cube11.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.1-0 ; 
       root-cube16.1-0 ; 
       root-cube17.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube21.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube5.1-0 ; 
       root-cube6.1-0 ; 
       root-cube7.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl1.1-0 ; 
       root-cyl1_1.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-root_1.11-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere6_1.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss307/PICTURES/bgrnd03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       tex_model-ss307-miningplat.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       ss306_ripcord-t2d7.4-0 ; 
       ss307_miningplat-t2d8.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 24 110 ; 
       1 0 110 ; 
       2 13 110 ; 
       3 1 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 13 110 ; 
       12 11 110 ; 
       13 0 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 0 110 ; 
       17 9 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 24 110 ; 
       21 24 110 ; 
       22 24 110 ; 
       23 24 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 22 110 ; 
       28 22 110 ; 
       29 20 110 ; 
       30 21 110 ; 
       31 20 110 ; 
       32 21 110 ; 
       33 20 110 ; 
       34 21 110 ; 
       35 20 110 ; 
       36 21 110 ; 
       37 19 110 ; 
       38 19 110 ; 
       39 20 110 ; 
       40 21 110 ; 
       41 22 110 ; 
       42 22 110 ; 
       43 22 110 ; 
       44 22 110 ; 
       45 18 110 ; 
       46 18 110 ; 
       47 18 110 ; 
       48 18 110 ; 
       49 18 110 ; 
       50 18 110 ; 
       51 20 110 ; 
       52 21 110 ; 
       53 23 110 ; 
       54 23 110 ; 
       55 23 110 ; 
       56 23 110 ; 
       57 23 110 ; 
       58 23 110 ; 
       59 19 110 ; 
       60 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       25 22 300 ; 
       26 23 300 ; 
       27 21 300 ; 
       28 21 300 ; 
       29 20 300 ; 
       30 4 300 ; 
       31 20 300 ; 
       32 3 300 ; 
       33 20 300 ; 
       34 2 300 ; 
       35 20 300 ; 
       36 1 300 ; 
       37 8 300 ; 
       38 7 300 ; 
       39 9 300 ; 
       40 5 300 ; 
       41 10 300 ; 
       42 11 300 ; 
       43 12 300 ; 
       44 13 300 ; 
       45 14 300 ; 
       46 15 300 ; 
       47 16 300 ; 
       48 17 300 ; 
       49 18 300 ; 
       50 19 300 ; 
       51 0 300 ; 
       52 6 300 ; 
       53 24 300 ; 
       54 25 300 ; 
       55 26 300 ; 
       56 27 300 ; 
       57 28 300 ; 
       58 29 300 ; 
       59 30 300 ; 
       60 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       22 0 401 ; 
       23 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM -7.706281 -13.88124 0 MPRFLG 0 ; 
       1 SCHEM 1.043719 -15.88124 0 MPRFLG 0 ; 
       2 SCHEM -3.956281 -17.88124 0 MPRFLG 0 ; 
       3 SCHEM -0.2062812 -17.88124 0 MPRFLG 0 ; 
       4 SCHEM 6.043719 -15.88124 0 MPRFLG 0 ; 
       5 SCHEM 8.543718 -15.88124 0 MPRFLG 0 ; 
       6 SCHEM 3.54372 -17.88124 0 MPRFLG 0 ; 
       7 SCHEM -1.456281 -19.88124 0 MPRFLG 0 ; 
       8 SCHEM 1.043719 -19.88124 0 MPRFLG 0 ; 
       9 SCHEM -13.95628 -15.88124 0 MPRFLG 0 ; 
       10 SCHEM -8.956281 -15.88124 0 MPRFLG 0 ; 
       11 SCHEM -6.456281 -17.88124 0 MPRFLG 0 ; 
       12 SCHEM -6.456281 -19.88124 0 MPRFLG 0 ; 
       13 SCHEM -5.206281 -15.88124 0 MPRFLG 0 ; 
       14 SCHEM -13.95628 -17.88124 0 MPRFLG 0 ; 
       15 SCHEM -11.45628 -17.88124 0 MPRFLG 0 ; 
       16 SCHEM -21.45628 -15.88124 0 MPRFLG 0 ; 
       17 SCHEM -16.45628 -17.88124 0 MPRFLG 0 ; 
       18 SCHEM 47.29372 -13.88124 0 MPRFLG 0 ; 
       19 SCHEM 89.79371 -13.88124 0 MPRFLG 0 ; 
       20 SCHEM 32.29372 -13.88124 0 MPRFLG 0 ; 
       21 SCHEM 17.29372 -13.88124 0 MPRFLG 0 ; 
       22 SCHEM 62.29372 -13.88124 0 MPRFLG 0 ; 
       23 SCHEM 77.29371 -13.88124 0 MPRFLG 0 ; 
       24 SCHEM 34.79372 -11.88124 0 USR SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM -23.95628 -15.88124 0 MPRFLG 0 ; 
       26 SCHEM -18.95628 -15.88124 0 MPRFLG 0 ; 
       27 SCHEM 56.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 61.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 33.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 18.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 31.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 16.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 28.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 13.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 26.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 11.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 86.04371 -15.88124 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 88.54371 -15.88124 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 21.04372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 58.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 63.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       43 SCHEM 66.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 68.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 41.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 43.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 46.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 48.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 51.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 53.54372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 38.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 23.54372 -15.88124 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 76.04371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 71.04372 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 78.54371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 73.54371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 81.04371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 83.54371 -15.88124 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 91.04371 -15.88124 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 93.54371 -15.88124 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 146.8747 59.59717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 126.9403 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 126.9403 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 126.9403 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 126.9403 12.2338 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 167.9155 37.68453 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 193.0932 95.83097 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 85.72176 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 83.22176 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 121.697 1.450737 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 149.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 154.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 159.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 164.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 180.0924 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 185.0924 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 190.0924 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 195.0924 32.64222 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 171.6858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 179.1858 5.061973 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 80.72176 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 123.2217 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 153.2233 22.41722 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 181.6873 47.4792 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 158.2232 22.41722 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 191.6873 47.4792 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 199.1873 47.4792 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 206.6873 47.4792 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 133.2233 18.41722 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 138.2233 18.41722 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
