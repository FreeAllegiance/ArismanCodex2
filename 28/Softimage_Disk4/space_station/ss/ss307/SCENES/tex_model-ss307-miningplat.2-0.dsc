SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.58-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       root-cube1.2-0 ; 
       root-cube11.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.1-0 ; 
       root-cube17.1-0 ; 
       root-cube17_1.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube5.1-0 ; 
       root-cube6.1-0 ; 
       root-cube7.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl1.1-0 ; 
       root-cyl1_1.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-root.36-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere6_1.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss307/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss307/PICTURES/ss307 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       tex_model-ss307-miningplat.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 24 110 ; 
       1 0 110 ; 
       2 14 110 ; 
       3 1 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 10 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 0 110 ; 
       15 10 110 ; 
       16 0 110 ; 
       17 10 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 24 110 ; 
       21 24 110 ; 
       22 24 110 ; 
       23 24 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 22 110 ; 
       28 22 110 ; 
       29 20 110 ; 
       30 21 110 ; 
       31 20 110 ; 
       32 21 110 ; 
       33 20 110 ; 
       34 21 110 ; 
       35 20 110 ; 
       36 21 110 ; 
       37 19 110 ; 
       38 19 110 ; 
       39 20 110 ; 
       40 21 110 ; 
       41 22 110 ; 
       42 22 110 ; 
       43 22 110 ; 
       44 22 110 ; 
       45 18 110 ; 
       46 18 110 ; 
       47 18 110 ; 
       48 18 110 ; 
       49 18 110 ; 
       50 18 110 ; 
       51 20 110 ; 
       52 21 110 ; 
       53 23 110 ; 
       54 23 110 ; 
       55 23 110 ; 
       56 23 110 ; 
       57 23 110 ; 
       58 23 110 ; 
       59 19 110 ; 
       60 19 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 20 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 23.75 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 30 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 32.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 27.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 25 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 12.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 10 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 17.5 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 18.75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 10 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 2.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 71.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 113.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 56.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 41.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 86.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 101.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 58.75 -4 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 0 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 80 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 85 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 57.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 42.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 55 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 40 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 52.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 37.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 50 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 35 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 110 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 112.5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 60 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 45 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 82.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 87.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 90 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 92.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 65 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 67.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 70 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 72.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 75 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 77.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 62.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 47.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 100 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 95 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 102.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 97.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 105 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 107.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 115 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 117.5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
