SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.51-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 56     
       ss300_outpost-white_strobe1_46.2-0 ; 
       ss300_outpost-white_strobe1_47.2-0 ; 
       ss300_outpost-white_strobe1_48.2-0 ; 
       ss300_outpost-white_strobe1_49.2-0 ; 
       ss300_outpost-white_strobe1_50.2-0 ; 
       ss300_outpost-white_strobe1_51.2-0 ; 
       ss300_outpost-white_strobe1_52.2-0 ; 
       ss301_garrison-white_strobe1_33.2-0 ; 
       ss301_garrison-white_strobe1_34.2-0 ; 
       ss304_research-white_strobe1_35.2-0 ; 
       ss304_research-white_strobe1_36.2-0 ; 
       ss304_research-white_strobe1_37.2-0 ; 
       ss304_research-white_strobe1_38.2-0 ; 
       ss304_research-white_strobe1_39.2-0 ; 
       ss304_research-white_strobe1_40.2-0 ; 
       ss304_research-white_strobe1_41.2-0 ; 
       ss304_research-white_strobe1_42.2-0 ; 
       ss304_research-white_strobe1_43.2-0 ; 
       ss304_research-white_strobe1_44.2-0 ; 
       ss304_research-white_strobe1_45.2-0 ; 
       ss305_elect_station-white_strobe1_5.2-0 ; 
       ss305_elect_station-white_strobe1_9.2-0 ; 
       ss306_ripcord-mat5.4-0 ; 
       ss307_miningplat-mat10.3-0 ; 
       ss307_miningplat-mat12.4-0 ; 
       ss307_miningplat-mat13.3-0 ; 
       ss307_miningplat-mat14.3-0 ; 
       ss307_miningplat-mat15.4-0 ; 
       ss307_miningplat-mat16.4-0 ; 
       ss307_miningplat-mat17.3-0 ; 
       ss307_miningplat-mat18.3-0 ; 
       ss307_miningplat-mat19.3-0 ; 
       ss307_miningplat-mat20.3-0 ; 
       ss307_miningplat-mat21.3-0 ; 
       ss307_miningplat-mat22.3-0 ; 
       ss307_miningplat-mat25.3-0 ; 
       ss307_miningplat-mat26.2-0 ; 
       ss307_miningplat-mat27.2-0 ; 
       ss307_miningplat-mat28.2-0 ; 
       ss307_miningplat-mat29.1-0 ; 
       ss307_miningplat-mat30.1-0 ; 
       ss307_miningplat-mat32.1-0 ; 
       ss307_miningplat-mat33.1-0 ; 
       ss307_miningplat-mat34.1-0 ; 
       ss307_miningplat-mat6.4-0 ; 
       ss307_miningplat-mat7.4-0 ; 
       ss307_miningplat-mat8.6-0 ; 
       ss307_miningplat-mat9.3-0 ; 
       ss307_miningplat-white_strobe1_53.2-0 ; 
       ss307_miningplat-white_strobe1_54.2-0 ; 
       ss307_miningplat-white_strobe1_55.2-0 ; 
       ss307_miningplat-white_strobe1_56.2-0 ; 
       ss307_miningplat-white_strobe1_57.2-0 ; 
       ss307_miningplat-white_strobe1_58.2-0 ; 
       ss307_miningplat-white_strobe1_59.2-0 ; 
       ss307_miningplat-white_strobe1_60.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       root-cube1.2-0 ; 
       root-cube11.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.1-0 ; 
       root-cube17.1-0 ; 
       root-cube17_1.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube21.1-0 ; 
       root-cube22.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube5.1-0 ; 
       root-cube6.1-0 ; 
       root-cube7.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl1.1-0 ; 
       root-cyl1_1.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-root_1.9-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere6_1.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss307/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss307/PICTURES/ss307 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-ss307-miningplat.45-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       ss306_ripcord-t2d7.7-0 ; 
       ss307_miningplat-t2d10.3-0 ; 
       ss307_miningplat-t2d11.3-0 ; 
       ss307_miningplat-t2d12.4-0 ; 
       ss307_miningplat-t2d13.3-0 ; 
       ss307_miningplat-t2d14.2-0 ; 
       ss307_miningplat-t2d15.4-0 ; 
       ss307_miningplat-t2d16.4-0 ; 
       ss307_miningplat-t2d17.5-0 ; 
       ss307_miningplat-t2d18.5-0 ; 
       ss307_miningplat-t2d19.4-0 ; 
       ss307_miningplat-t2d20.2-0 ; 
       ss307_miningplat-t2d21.2-0 ; 
       ss307_miningplat-t2d22.2-0 ; 
       ss307_miningplat-t2d23.3-0 ; 
       ss307_miningplat-t2d24.3-0 ; 
       ss307_miningplat-t2d25.2-0 ; 
       ss307_miningplat-t2d26.3-0 ; 
       ss307_miningplat-t2d27.1-0 ; 
       ss307_miningplat-t2d28.3-0 ; 
       ss307_miningplat-t2d29.1-0 ; 
       ss307_miningplat-t2d30.1-0 ; 
       ss307_miningplat-t2d31.1-0 ; 
       ss307_miningplat-t2d32.1-0 ; 
       ss307_miningplat-t2d8.6-0 ; 
       ss307_miningplat-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 24 110 ; 
       1 0 110 ; 
       2 14 110 ; 
       3 1 110 ; 
       4 0 110 ; 
       6 1 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 10 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 0 110 ; 
       15 10 110 ; 
       16 0 110 ; 
       17 10 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 24 110 ; 
       21 24 110 ; 
       22 24 110 ; 
       23 24 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 22 110 ; 
       28 22 110 ; 
       29 20 110 ; 
       30 21 110 ; 
       31 20 110 ; 
       32 21 110 ; 
       33 20 110 ; 
       34 21 110 ; 
       35 20 110 ; 
       36 21 110 ; 
       37 19 110 ; 
       38 19 110 ; 
       39 20 110 ; 
       40 21 110 ; 
       41 22 110 ; 
       42 22 110 ; 
       43 22 110 ; 
       44 22 110 ; 
       45 18 110 ; 
       46 18 110 ; 
       47 18 110 ; 
       48 18 110 ; 
       49 18 110 ; 
       50 18 110 ; 
       51 20 110 ; 
       52 21 110 ; 
       53 23 110 ; 
       54 23 110 ; 
       55 23 110 ; 
       56 23 110 ; 
       57 23 110 ; 
       58 23 110 ; 
       59 19 110 ; 
       60 19 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 45 300 ; 
       0 43 300 ; 
       1 30 300 ; 
       2 27 300 ; 
       2 37 300 ; 
       3 31 300 ; 
       4 41 300 ; 
       6 34 300 ; 
       7 33 300 ; 
       8 32 300 ; 
       9 38 300 ; 
       10 46 300 ; 
       10 35 300 ; 
       10 36 300 ; 
       11 29 300 ; 
       12 25 300 ; 
       13 26 300 ; 
       14 24 300 ; 
       14 39 300 ; 
       15 23 300 ; 
       16 28 300 ; 
       16 40 300 ; 
       17 47 300 ; 
       25 22 300 ; 
       26 44 300 ; 
       27 21 300 ; 
       28 21 300 ; 
       29 20 300 ; 
       30 4 300 ; 
       31 20 300 ; 
       32 3 300 ; 
       33 20 300 ; 
       34 2 300 ; 
       35 20 300 ; 
       36 1 300 ; 
       37 8 300 ; 
       38 7 300 ; 
       39 9 300 ; 
       40 5 300 ; 
       41 10 300 ; 
       42 11 300 ; 
       43 12 300 ; 
       44 13 300 ; 
       45 14 300 ; 
       46 15 300 ; 
       47 16 300 ; 
       48 17 300 ; 
       49 18 300 ; 
       50 19 300 ; 
       51 0 300 ; 
       52 6 300 ; 
       53 48 300 ; 
       54 49 300 ; 
       55 50 300 ; 
       56 51 300 ; 
       57 52 300 ; 
       58 53 300 ; 
       59 54 300 ; 
       60 55 300 ; 
       5 42 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       22 0 401 ; 
       23 12 401 ; 
       24 8 401 ; 
       25 6 401 ; 
       26 7 401 ; 
       27 9 401 ; 
       28 1 401 ; 
       29 2 401 ; 
       30 16 401 ; 
       31 15 401 ; 
       32 18 401 ; 
       33 17 401 ; 
       34 14 401 ; 
       43 23 401 ; 
       41 21 401 ; 
       35 4 401 ; 
       36 5 401 ; 
       37 10 401 ; 
       38 13 401 ; 
       39 19 401 ; 
       44 24 401 ; 
       45 25 401 ; 
       46 3 401 ; 
       47 11 401 ; 
       40 20 401 ; 
       42 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 MPRFLG 0 ; 
       2 SCHEM 20 -10 0 MPRFLG 0 ; 
       3 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       4 SCHEM 30 -8 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       8 SCHEM 25 -12 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 10 -8 0 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       14 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 10 -10 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 71.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 113.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 86.25 -6 0 MPRFLG 0 ; 
       23 SCHEM 101.25 -6 0 MPRFLG 0 ; 
       24 SCHEM 58.75 -4 0 SRT 1 1 1 0 0 0 24.59785 0 0.5428964 MPRFLG 0 ; 
       25 SCHEM 0 -8 0 MPRFLG 0 ; 
       26 SCHEM 5 -8 0 MPRFLG 0 ; 
       27 SCHEM 80 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 85 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 57.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 42.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 55 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 40 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 52.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 50 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 35 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 60 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 45 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 87.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       43 SCHEM 90 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 92.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 65 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 67.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 70 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 72.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 75 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 77.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 62.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 47.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 100 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 95 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 102.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 97.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 105 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 107.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 111.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 109 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 81.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 69 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 71.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 120 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 99 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 101.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 96.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 104 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 106.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 114 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 116.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 122.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
