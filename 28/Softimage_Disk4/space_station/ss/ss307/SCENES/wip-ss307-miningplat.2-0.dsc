SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.3-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       ss306_ripcord-mat5.1-0 ; 
       ss307_miningplat-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       ss307_miningplat-cube1.2-0 ROOT ; 
       ss307_miningplat-cube10.1-0 ROOT ; 
       ss307_miningplat-cube11.1-0 ROOT ; 
       ss307_miningplat-cube12.1-0 ROOT ; 
       ss307_miningplat-cube14.1-0 ROOT ; 
       ss307_miningplat-cube2.2-0 ROOT ; 
       ss307_miningplat-cube3.1-0 ; 
       ss307_miningplat-cube4.1-0 ; 
       ss307_miningplat-cube5.1-0 ; 
       ss307_miningplat-cube6.1-0 ; 
       ss307_miningplat-cube7.1-0 ; 
       ss307_miningplat-cube8.1-0 ; 
       ss307_miningplat-cube9.1-0 ; 
       ss307_miningplat-cyl1.1-0 ; 
       ss307_miningplat-cyl1_1.1-0 ; 
       ss307_miningplat-sphere6.1-0 ; 
       ss307_miningplat-sphere6_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss307/PICTURES/bgrnd03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss307-miningplat.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       ss306_ripcord-t2d7.2-0 ; 
       ss307_miningplat-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 0 110 ; 
       7 0 110 ; 
       8 10 110 ; 
       9 8 110 ; 
       10 0 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 0 110 ; 
       14 6 110 ; 
       15 0 110 ; 
       16 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       15 0 300 ; 
       16 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 9.86608 5.900124 0 USR SRT 12.6173 0.2352587 6.909178 0 0 0 -10.20374 2.874163 -0.3923201 MPRFLG 0 ; 
       5 SCHEM 10 0 0 DISPLAY 0 0 SRT 1.53 1.53 1.53 0 0 0 -12.92387 1.561386 -8.803096 MPRFLG 0 ; 
       6 SCHEM 11.11608 3.900123 0 MPRFLG 0 ; 
       7 SCHEM 16.11608 3.900123 0 MPRFLG 0 ; 
       8 SCHEM 18.61609 1.900123 0 MPRFLG 0 ; 
       9 SCHEM 18.61609 -0.09987645 0 MPRFLG 0 ; 
       10 SCHEM 18.61609 3.900123 0 MPRFLG 0 ; 
       11 SCHEM 11.11608 1.900123 0 MPRFLG 0 ; 
       12 SCHEM 13.61608 1.900123 0 MPRFLG 0 ; 
       13 SCHEM 3.616081 3.900123 0 MPRFLG 0 ; 
       14 SCHEM 8.61608 1.900123 0 MPRFLG 0 ; 
       15 SCHEM 1.116081 3.900123 0 MPRFLG 0 ; 
       16 SCHEM 6.116081 3.900123 0 MPRFLG 0 ; 
       1 SCHEM 21.11609 5.900124 0 DISPLAY 0 0 SRT 0.234668 0.234668 0.234668 0 0 0 -7.438226 2.594093 7.727016 MPRFLG 0 ; 
       2 SCHEM 23.61609 5.900124 0 SRT 1.323135 0.026352 0.1879319 0 0.47 0 -13.82273 -1.809306 2.171472 MPRFLG 0 ; 
       3 SCHEM 26.11609 5.900124 0 SRT -0.3458464 -0.1582097 -0.1582097 0 0 0 -18.71814 -2.702636 4.537398 MPRFLG 0 ; 
       4 SCHEM 28.61609 5.900124 0 SRT 0.196 0.196 0.196 0 0 0 -7.438226 2.393456 7.727016 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
