SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.4-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       ss306_ripcord-mat5.1-0 ; 
       ss307_miningplat-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       root-cube1.2-0 ; 
       root-cube11.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.1-0 ; 
       root-cube16.1-0 ; 
       root-cube17.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube21.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube5.1-0 ; 
       root-cube6.1-0 ; 
       root-cube7.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl1.1-0 ; 
       root-cyl1_1.1-0 ; 
       root-root.1-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere6_1.1-0 ; 
       ss307_miningplat-cube10.1-0 ROOT ; 
       ss307_miningplat-cube2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss307/PICTURES/bgrnd03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss307-miningplat.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       ss306_ripcord-t2d7.2-0 ; 
       ss307_miningplat-t2d8.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 13 110 ; 
       12 11 110 ; 
       13 0 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 0 110 ; 
       17 9 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       1 0 110 ; 
       4 0 110 ; 
       3 1 110 ; 
       2 13 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 3 110 ; 
       8 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       19 0 300 ; 
       20 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.93802 -5.94709 0 USR MPRFLG 0 ; 
       22 SCHEM 18.65962 5.737664 0 USR DISPLAY 0 0 SRT 1.53 1.53 1.53 0 0 0 -12.92387 1.561386 -8.803096 MPRFLG 0 ; 
       9 SCHEM 5.688015 -7.947091 0 MPRFLG 0 ; 
       10 SCHEM 10.68802 -7.947091 0 MPRFLG 0 ; 
       11 SCHEM 13.18802 -9.947091 0 MPRFLG 0 ; 
       12 SCHEM 13.18802 -11.94709 0 MPRFLG 0 ; 
       13 SCHEM 14.43802 -7.947091 0 MPRFLG 0 ; 
       14 SCHEM 5.688015 -9.947091 0 MPRFLG 0 ; 
       15 SCHEM 8.188017 -9.947091 0 MPRFLG 0 ; 
       16 SCHEM -1.811985 -7.947091 0 MPRFLG 0 ; 
       17 SCHEM 3.188014 -9.947091 0 MPRFLG 0 ; 
       19 SCHEM -4.311985 -7.947091 0 MPRFLG 0 ; 
       20 SCHEM 0.6880146 -7.947091 0 MPRFLG 0 ; 
       21 SCHEM 21.11609 5.900124 0 DISPLAY 0 0 SRT 0.234668 0.234668 0.234668 0 0 0 -7.438226 2.594093 7.727016 MPRFLG 0 ; 
       1 SCHEM 20.68801 -7.947091 0 MPRFLG 0 ; 
       4 SCHEM 25.68801 -7.947091 0 MPRFLG 0 ; 
       3 SCHEM 19.43801 -9.947091 0 MPRFLG 0 ; 
       2 SCHEM 15.68801 -9.947091 0 MPRFLG 0 ; 
       5 SCHEM 28.18801 -7.947091 0 MPRFLG 0 ; 
       6 SCHEM 23.18801 -9.947091 0 MPRFLG 0 ; 
       7 SCHEM 18.18801 -11.94709 0 MPRFLG 0 ; 
       18 SCHEM 12.19998 -3.715777 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 20.68801 -11.94709 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
