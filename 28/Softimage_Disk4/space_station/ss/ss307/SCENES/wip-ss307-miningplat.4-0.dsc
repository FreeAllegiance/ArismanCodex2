SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.5-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       ss307_miningplat-light1.1-0 ROOT ; 
       ss307_miningplat-light2.1-0 ROOT ; 
       ss307_miningplat-light3.1-0 ROOT ; 
       ss307_miningplat-light4.1-0 ROOT ; 
       ss307_miningplat-light5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       ss306_ripcord-mat5.1-0 ; 
       ss307_miningplat-mat6.1-0 ; 
       ss307_miningplat-mat7.1-0 ; 
       ss307_miningplat-mat8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 2     
       ss307_miningplat-EnvAtmos_Moon1.1-0 ; 
       ss307_miningplat-EnvAtmos_Moon2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       root-cube1.2-0 ; 
       root-cube11.1-0 ; 
       root-cube14.1-0 ; 
       root-cube15.1-0 ; 
       root-cube16.1-0 ; 
       root-cube17.1-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube21.1-0 ; 
       root-cube3.1-0 ; 
       root-cube4.1-0 ; 
       root-cube5.1-0 ; 
       root-cube6.1-0 ; 
       root-cube7.1-0 ; 
       root-cube8.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl1.1-0 ; 
       root-cyl1_1.1-0 ; 
       root-root.2-0 ROOT ; 
       root-sphere6.1-0 ; 
       root-sphere6_1.1-0 ; 
       ss307_miningplat-sphere7.1-0 ROOT ; 
       ss307_miningplat-sphere8.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss307/PICTURES/bgrnd03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ss307-miningplat.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       ss306_ripcord-t2d7.3-0 ; 
       ss307_miningplat-t2d8.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 13 110 ; 
       12 11 110 ; 
       13 0 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 0 110 ; 
       17 9 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       1 0 110 ; 
       4 0 110 ; 
       3 1 110 ; 
       2 13 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 3 110 ; 
       8 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       21 2 300 ; 
       19 0 300 ; 
       20 1 300 ; 
       22 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       2 0 550 ; 
       3 1 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30.68801 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 33.18801 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35.68801 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 38.18801 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 40.68801 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.88806 -8.178356 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 7.20425 3.678381 0 USR DISPLAY 0 0 SRT 93.80779 93.80779 93.80779 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 11.63806 -10.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 16.63806 -10.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 19.13806 -12.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 19.13806 -14.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 20.38806 -10.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 11.63806 -12.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 14.13806 -12.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 4.138058 -10.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 9.138058 -12.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 1.638058 -10.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 6.638058 -10.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 12.53483 4.751242 0 USR DISPLAY 0 0 SRT 27.28324 27.28324 27.28324 0 0 0 0 -3.624754 0 MPRFLG 0 ; 
       1 SCHEM 26.63806 -10.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 31.63806 -10.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 25.38806 -12.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 21.63806 -12.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 34.13805 -10.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 29.13806 -12.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 24.13806 -14.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 18.15003 -5.947043 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 26.63806 -14.17836 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 45.68801 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 81.67177 -2.384186e-007 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 3 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
