SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.52-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       bay-light1.1-0 ROOT ; 
       bay-light2.1-0 ROOT ; 
       bay-light3.1-0 ROOT ; 
       bay-light4.1-0 ROOT ; 
       bay-light5.1-0 ROOT ; 
       bay-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       bay-cube1.1-0 ; 
       bay-east_bay_7.7-0 ; 
       bay-east_bay_antenna.1-0 ; 
       bay-east_bay_strut_2.1-0 ; 
       bay-east_bay_strut2.5-0 ; 
       bay-hullroot_1.1-0 ; 
       bay-south_block.1-0 ; 
       bay-south_hull_2.3-0 ; 
       bay-sphere1.1-0 ; 
       bay-sphere1_1.1-0 ROOT ; 
       bay-SS_01.1-0 ; 
       bay-SS_10.1-0 ; 
       bay-SS_8.1-0 ; 
       bay-SS_8_1.1-0 ; 
       bay-strobe_set1.1-0 ; 
       bay-turwepemt1.1-0 ; 
       bay-turwepemt2.1-0 ; 
       bay-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-bay.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 3 110 ; 
       2 1 110 ; 
       3 4 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 5 110 ; 
       7 9 110 ; 
       8 0 110 ; 
       10 6 110 ; 
       11 2 110 ; 
       12 6 110 ; 
       13 7 110 ; 
       14 1 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -6 0 MPRFLG 0 ; 
       1 SCHEM 15 -10 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       3 SCHEM 15 -8 0 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 10 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 0 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       15 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
