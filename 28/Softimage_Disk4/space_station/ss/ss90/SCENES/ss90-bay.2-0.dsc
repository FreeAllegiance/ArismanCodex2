SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.60-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       bay-light1.2-0 ROOT ; 
       bay-light2.2-0 ROOT ; 
       bay-light3.2-0 ROOT ; 
       bay-light4.2-0 ROOT ; 
       bay-light5.2-0 ROOT ; 
       bay-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       bay-cube1.1-0 ; 
       bay-east_bay_7.7-0 ; 
       bay-east_bay_antenna.1-0 ; 
       bay-east_bay_strut_2.1-0 ; 
       bay-east_bay_strut2.5-0 ; 
       bay-garage1A.1-0 ; 
       bay-garage1B.1-0 ; 
       bay-garage1C.1-0 ; 
       bay-garage1D.1-0 ; 
       bay-garage1E.1-0 ; 
       bay-hullroot_1.1-0 ; 
       bay-launch.1-0 ; 
       bay-south_block.1-0 ; 
       bay-south_hull_2.3-0 ; 
       bay-sphere1.1-0 ; 
       bay-sphere1_1.2-0 ROOT ; 
       bay-SS_01.1-0 ; 
       bay-SS_10.1-0 ; 
       bay-SS_8.1-0 ; 
       bay-SS_8_1.1-0 ; 
       bay-strobe_set1.1-0 ; 
       bay-turwepemt1.1-0 ; 
       bay-turwepemt2.1-0 ; 
       bay-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-bay.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 15 110 ; 
       7 15 110 ; 
       8 15 110 ; 
       9 15 110 ; 
       11 15 110 ; 
       0 15 110 ; 
       1 3 110 ; 
       2 1 110 ; 
       3 4 110 ; 
       4 15 110 ; 
       10 15 110 ; 
       12 10 110 ; 
       13 15 110 ; 
       14 0 110 ; 
       16 12 110 ; 
       17 2 110 ; 
       18 12 110 ; 
       19 13 110 ; 
       20 1 110 ; 
       21 13 110 ; 
       22 13 110 ; 
       23 1 110 ; 
       5 15 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 5 -2 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 20 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 15 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 20 -8 0 MPRFLG 0 ; 
       21 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
