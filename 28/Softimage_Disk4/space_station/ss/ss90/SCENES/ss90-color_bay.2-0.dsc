SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.70-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       color_bay-light1.2-0 ROOT ; 
       color_bay-light2.2-0 ROOT ; 
       color_bay-light3.2-0 ROOT ; 
       color_bay-light4.2-0 ROOT ; 
       color_bay-light5.2-0 ROOT ; 
       color_bay-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       color_bay-COLORED_BAY.1-0 ; 
       color_bay-cube1.1-0 ; 
       color_bay-east_bay_7.1-0 ROOT ; 
       color_bay-east_bay_strut_2.1-0 ; 
       color_bay-east_bay_strut2.5-0 ; 
       color_bay-hullroot_1.1-0 ; 
       color_bay-south_block.1-0 ; 
       color_bay-south_hull_2.3-0 ; 
       color_bay-sphere1.1-0 ; 
       color_bay-sphere1_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-color_bay.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 9 110 ; 
       3 4 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 5 110 ; 
       7 9 110 ; 
       8 1 110 ; 
       0 3 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 25 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 0.9980001 0.9980001 0.998 1.570796 5.6426e-007 8.742278e-008 -10.48147 4.14711 -2.269423 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 MPRFLG 0 ; 
       9 SCHEM 22.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 20 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
