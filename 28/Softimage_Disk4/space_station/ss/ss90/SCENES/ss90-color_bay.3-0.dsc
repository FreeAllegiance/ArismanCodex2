SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.74-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       color_bay-light1.3-0 ROOT ; 
       color_bay-light2.3-0 ROOT ; 
       color_bay-light3.3-0 ROOT ; 
       color_bay-light4.3-0 ROOT ; 
       color_bay-light5.3-0 ROOT ; 
       color_bay-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       color_bay-COLORED_BAY.1-0 ; 
       color_bay-cube1.1-0 ; 
       color_bay-east_bay_7.2-0 ROOT ; 
       color_bay-east_bay_antenna.1-0 ; 
       color_bay-east_bay_strut_2.1-0 ; 
       color_bay-east_bay_strut2.5-0 ; 
       color_bay-garage1A.1-0 ; 
       color_bay-garage1B.1-0 ; 
       color_bay-garage1C.1-0 ; 
       color_bay-garage1D.1-0 ; 
       color_bay-garage1E.1-0 ; 
       color_bay-hullroot_1.1-0 ; 
       color_bay-launch.1-0 ; 
       color_bay-south_block.1-0 ; 
       color_bay-south_hull_2.3-0 ; 
       color_bay-sphere1.1-0 ; 
       color_bay-sphere1_1.3-0 ROOT ; 
       color_bay-SS_01.1-0 ; 
       color_bay-SS_10.1-0 ; 
       color_bay-SS_8.1-0 ; 
       color_bay-SS_8_1.1-0 ; 
       color_bay-strobe_set1.1-0 ; 
       color_bay-turwepemt1.1-0 ; 
       color_bay-turwepemt2.1-0 ; 
       color_bay-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-color_bay.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 16 110 ; 
       3 0 110 ; 
       4 5 110 ; 
       5 16 110 ; 
       6 16 110 ; 
       7 16 110 ; 
       8 16 110 ; 
       9 16 110 ; 
       10 16 110 ; 
       11 16 110 ; 
       12 16 110 ; 
       13 11 110 ; 
       14 16 110 ; 
       15 1 110 ; 
       17 13 110 ; 
       18 3 110 ; 
       19 13 110 ; 
       20 14 110 ; 
       21 0 110 ; 
       22 14 110 ; 
       23 14 110 ; 
       24 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -6 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 0.9980001 0.9980001 0.998 1.570796 5.6426e-007 8.742278e-008 -10.48147 4.14711 -2.269423 MPRFLG 0 ; 
       3 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 40 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 25 -4 0 MPRFLG 0 ; 
       16 SCHEM 22.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 15 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
