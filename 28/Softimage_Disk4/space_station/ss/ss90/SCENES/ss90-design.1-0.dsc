SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.8-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       text-light1.8-0 ROOT ; 
       text-light2.8-0 ROOT ; 
       text-light3.8-0 ROOT ; 
       text-light4.8-0 ROOT ; 
       text-light5.8-0 ROOT ; 
       text-light6.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 65     
       design-east_bay_9.1-0 ; 
       design-east_bay_antenna2.1-0 ; 
       design-east_bay_strut_4.1-0 ; 
       design-east_bay_strut4.1-0 ROOT ; 
       design-hullroot_1.1-0 ROOT ; 
       design-landing_lights10.1-0 ; 
       design-landing_lights11.1-0 ; 
       design-landing_lights9.1-0 ; 
       design-south_block.1-0 ; 
       design-sphere1.1-0 ROOT ; 
       design-sphere2.1-0 ; 
       design-SS_01.1-0 ; 
       design-SS_66.1-0 ; 
       design-SS_67.1-0 ; 
       design-SS_68.1-0 ; 
       design-SS_69.1-0 ; 
       design-SS_70.1-0 ; 
       design-SS_71.1-0 ; 
       design-SS_72.1-0 ; 
       design-SS_73.1-0 ; 
       design-SS_74.1-0 ; 
       design-SS_75.1-0 ; 
       design-SS_76.1-0 ; 
       design-SS_77.1-0 ; 
       design-SS_78.1-0 ; 
       design-SS_79.1-0 ; 
       design-SS_8.1-0 ; 
       design-SS_80.1-0 ; 
       design-SS_81.1-0 ; 
       design-SS_82.1-0 ; 
       design-SS_83.1-0 ; 
       design-SS_84.1-0 ; 
       design-strobe_set3.1-0 ; 
       design-turwepemt5.1-0 ; 
       ss27-east_bay_7.7-0 ; 
       ss27-east_bay_antenna.1-0 ; 
       ss27-east_bay_strut_2.1-0 ; 
       ss27-east_bay_strut2.5-0 ROOT ; 
       ss27-landing_lights3.1-0 ; 
       ss27-landing_lights4.1-0 ; 
       ss27-landing_lights5.1-0 ; 
       ss27-south_hull_2.3-0 ROOT ; 
       ss27-SS_10.1-0 ; 
       ss27-SS_29.1-0 ; 
       ss27-SS_30.1-0 ; 
       ss27-SS_31.1-0 ; 
       ss27-SS_32.1-0 ; 
       ss27-SS_33.1-0 ; 
       ss27-SS_34.1-0 ; 
       ss27-SS_35.1-0 ; 
       ss27-SS_36.1-0 ; 
       ss27-SS_37.1-0 ; 
       ss27-SS_38.1-0 ; 
       ss27-SS_39.1-0 ; 
       ss27-SS_40.1-0 ; 
       ss27-SS_41.1-0 ; 
       ss27-SS_42.1-0 ; 
       ss27-SS_43.1-0 ; 
       ss27-SS_44.1-0 ; 
       ss27-SS_45.1-0 ; 
       ss27-SS_46.1-0 ; 
       ss27-SS_8.1-0 ; 
       ss27-strobe_set1.1-0 ; 
       ss27-turwepemt1.1-0 ; 
       ss27-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-design.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 9 110 ; 
       8 4 110 ; 
       11 8 110 ; 
       26 8 110 ; 
       34 36 110 ; 
       35 34 110 ; 
       36 37 110 ; 
       38 62 110 ; 
       39 62 110 ; 
       40 62 110 ; 
       2 3 110 ; 
       0 2 110 ; 
       42 35 110 ; 
       43 38 110 ; 
       44 38 110 ; 
       45 38 110 ; 
       46 38 110 ; 
       47 38 110 ; 
       48 38 110 ; 
       49 39 110 ; 
       50 39 110 ; 
       51 39 110 ; 
       52 39 110 ; 
       53 39 110 ; 
       54 39 110 ; 
       55 40 110 ; 
       56 40 110 ; 
       57 40 110 ; 
       58 40 110 ; 
       59 40 110 ; 
       60 40 110 ; 
       61 41 110 ; 
       62 34 110 ; 
       63 41 110 ; 
       64 34 110 ; 
       1 0 110 ; 
       12 1 110 ; 
       32 0 110 ; 
       7 32 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       5 32 110 ; 
       19 5 110 ; 
       20 5 110 ; 
       21 5 110 ; 
       22 5 110 ; 
       23 5 110 ; 
       24 5 110 ; 
       6 32 110 ; 
       25 6 110 ; 
       27 6 110 ; 
       28 6 110 ; 
       29 6 110 ; 
       30 6 110 ; 
       31 6 110 ; 
       33 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 12.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 15 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 72.5 0 0 SRT 1.168 1.168 1.338528 0 0 0 0 1.047835 2.936134 MPRFLG 0 ; 
       10 SCHEM 75 0 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 6.25 0 0 SRT 0.86 0.86 0.86 3.576279e-007 0 0 0.0007858945 -1.378695 -1.563608 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       3 SCHEM 101.25 0 0 SRT 0.59 0.59 0.59 3.496911e-007 -8.742278e-008 1.262824e-023 -1.328412 -3.227051 -3.32405 MPRFLG 0 ; 
       34 SCHEM 30 -12 0 MPRFLG 0 ; 
       35 SCHEM 1.25 -14 0 MPRFLG 0 ; 
       36 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       37 SCHEM 35 -8 0 SRT 0.59 0.59 0.59 -3.496911e-007 3.141593 -1.262824e-023 1.328412 -3.227051 -3.32405 MPRFLG 0 ; 
       38 SCHEM 28.75 -16 0 MPRFLG 0 ; 
       39 SCHEM 13.75 -16 0 MPRFLG 0 ; 
       40 SCHEM 43.75 -16 0 MPRFLG 0 ; 
       41 SCHEM 5 -22 0 SRT 1 1 1 -3.496911e-007 0 0 0 2.087002e-007 0.5968131 MPRFLG 0 ; 
       2 SCHEM 98.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 96.25 -4 0 MPRFLG 0 ; 
       42 SCHEM 0 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 35 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 22.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 25 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 27.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 30 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 32.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 20 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 7.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 10 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 12.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 15 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 17.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 50 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 37.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 40 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 42.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 45 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 47.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 2.5 -24 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 28.75 -14 0 MPRFLG 0 ; 
       63 SCHEM 0 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 66.25 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 95 -6 0 MPRFLG 0 ; 
       7 SCHEM 95 -8 0 MPRFLG 0 ; 
       13 SCHEM 101.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 88.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 91.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 93.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 96.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 98.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       5 SCHEM 80 -8 0 MPRFLG 0 ; 
       19 SCHEM 86.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 73.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 76.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 78.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 81.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 83.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       6 SCHEM 110 -8 0 MPRFLG 0 ; 
       25 SCHEM 116.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 103.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 106.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 108.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 111.25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 113.75 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 71.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
