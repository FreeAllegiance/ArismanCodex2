SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.11-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       text-light1.11-0 ROOT ; 
       text-light2.11-0 ROOT ; 
       text-light3.11-0 ROOT ; 
       text-light4.11-0 ROOT ; 
       text-light5.11-0 ROOT ; 
       text-light6.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 65     
       design-east_bay_7.7-0 ; 
       design-east_bay_9.1-0 ; 
       design-east_bay_antenna.1-0 ; 
       design-east_bay_antenna2.1-0 ; 
       design-east_bay_strut_2.1-0 ; 
       design-east_bay_strut_4.1-0 ; 
       design-east_bay_strut2.5-0 ; 
       design-east_bay_strut4.1-0 ; 
       design-hullroot_1.1-0 ; 
       design-landing_lights10.1-0 ; 
       design-landing_lights11.1-0 ; 
       design-landing_lights3.1-0 ; 
       design-landing_lights4.1-0 ; 
       design-landing_lights5.1-0 ; 
       design-landing_lights9.1-0 ; 
       design-south_block.1-0 ; 
       design-south_hull_2.3-0 ; 
       design-sphere1.4-0 ROOT ; 
       design-sphere2.1-0 ; 
       design-SS_01.1-0 ; 
       design-SS_10.1-0 ; 
       design-SS_29.1-0 ; 
       design-SS_30.1-0 ; 
       design-SS_31.1-0 ; 
       design-SS_32.1-0 ; 
       design-SS_33.1-0 ; 
       design-SS_34.1-0 ; 
       design-SS_35.1-0 ; 
       design-SS_36.1-0 ; 
       design-SS_37.1-0 ; 
       design-SS_38.1-0 ; 
       design-SS_39.1-0 ; 
       design-SS_40.1-0 ; 
       design-SS_41.1-0 ; 
       design-SS_42.1-0 ; 
       design-SS_43.1-0 ; 
       design-SS_44.1-0 ; 
       design-SS_45.1-0 ; 
       design-SS_46.1-0 ; 
       design-SS_66.1-0 ; 
       design-SS_67.1-0 ; 
       design-SS_68.1-0 ; 
       design-SS_69.1-0 ; 
       design-SS_70.1-0 ; 
       design-SS_71.1-0 ; 
       design-SS_72.1-0 ; 
       design-SS_73.1-0 ; 
       design-SS_74.1-0 ; 
       design-SS_75.1-0 ; 
       design-SS_76.1-0 ; 
       design-SS_77.1-0 ; 
       design-SS_78.1-0 ; 
       design-SS_79.1-0 ; 
       design-SS_8.1-0 ; 
       design-SS_8_1.1-0 ; 
       design-SS_80.1-0 ; 
       design-SS_81.1-0 ; 
       design-SS_82.1-0 ; 
       design-SS_83.1-0 ; 
       design-SS_84.1-0 ; 
       design-strobe_set1.1-0 ; 
       design-strobe_set3.1-0 ; 
       design-turwepemt1.1-0 ; 
       design-turwepemt3.1-0 ; 
       design-turwepemt5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-design.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       18 17 110 ; 
       8 17 110 ; 
       15 8 110 ; 
       19 15 110 ; 
       53 15 110 ; 
       7 17 110 ; 
       0 4 110 ; 
       2 0 110 ; 
       4 6 110 ; 
       6 17 110 ; 
       11 60 110 ; 
       12 60 110 ; 
       13 60 110 ; 
       16 17 110 ; 
       5 7 110 ; 
       1 5 110 ; 
       20 2 110 ; 
       21 11 110 ; 
       22 11 110 ; 
       23 11 110 ; 
       24 11 110 ; 
       25 11 110 ; 
       26 11 110 ; 
       27 12 110 ; 
       28 12 110 ; 
       29 12 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       32 12 110 ; 
       33 13 110 ; 
       34 13 110 ; 
       35 13 110 ; 
       36 13 110 ; 
       37 13 110 ; 
       38 13 110 ; 
       54 16 110 ; 
       60 0 110 ; 
       62 16 110 ; 
       63 0 110 ; 
       3 1 110 ; 
       39 3 110 ; 
       61 1 110 ; 
       14 61 110 ; 
       40 14 110 ; 
       41 14 110 ; 
       42 14 110 ; 
       43 14 110 ; 
       44 14 110 ; 
       45 14 110 ; 
       9 61 110 ; 
       46 9 110 ; 
       47 9 110 ; 
       48 9 110 ; 
       49 9 110 ; 
       50 9 110 ; 
       51 9 110 ; 
       10 61 110 ; 
       52 10 110 ; 
       55 10 110 ; 
       56 10 110 ; 
       57 10 110 ; 
       58 10 110 ; 
       59 10 110 ; 
       64 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 175 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 177.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 180 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 182.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 185 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 187.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       17 SCHEM 87.5 0 0 SRT 1.168 1.168 1.338528 0 0 0 0 1.047835 2.936134 MPRFLG 0 ; 
       18 SCHEM 97.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 15 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 17.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       7 SCHEM 135 -2 0 MPRFLG 0 ; 
       0 SCHEM 55 -6 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 60 -2 0 MPRFLG 0 ; 
       11 SCHEM 53.75 -10 0 MPRFLG 0 ; 
       12 SCHEM 38.75 -10 0 MPRFLG 0 ; 
       13 SCHEM 68.75 -10 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 132.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 130 -6 0 MPRFLG 0 ; 
       20 SCHEM 25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 60 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 47.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 50 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 55 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 45 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 32.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 35 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 37.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 40 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 75 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 62.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 65 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 67.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 70 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 72.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       62 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 101.25 -8 0 MPRFLG 0 ; 
       39 SCHEM 100 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 128.75 -8 0 MPRFLG 0 ; 
       14 SCHEM 128.75 -10 0 MPRFLG 0 ; 
       40 SCHEM 135 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 122.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 125 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 127.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 130 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 132.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       9 SCHEM 113.75 -10 0 MPRFLG 0 ; 
       46 SCHEM 120 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 107.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 110 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 112.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 115 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 117.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 143.75 -10 0 MPRFLG 0 ; 
       52 SCHEM 150 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 137.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 140 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 142.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 145 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 147.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
