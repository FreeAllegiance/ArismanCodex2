SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.35-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       fix_mat-light1.1-0 ROOT ; 
       fix_mat-light2.1-0 ROOT ; 
       fix_mat-light3.1-0 ROOT ; 
       fix_mat-light4.1-0 ROOT ; 
       fix_mat-light5.1-0 ROOT ; 
       fix_mat-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 67     
       fix_mat-cube1.1-0 ; 
       fix_mat-east_bay_7.7-0 ; 
       fix_mat-east_bay_9.1-0 ; 
       fix_mat-east_bay_antenna.1-0 ; 
       fix_mat-east_bay_antenna2.1-0 ; 
       fix_mat-east_bay_strut_2.1-0 ; 
       fix_mat-east_bay_strut_4.1-0 ; 
       fix_mat-east_bay_strut2.5-0 ; 
       fix_mat-east_bay_strut4.1-0 ; 
       fix_mat-hullroot_1.1-0 ; 
       fix_mat-landing_lights10.1-0 ; 
       fix_mat-landing_lights11.1-0 ; 
       fix_mat-landing_lights3.1-0 ; 
       fix_mat-landing_lights4.1-0 ; 
       fix_mat-landing_lights5.1-0 ; 
       fix_mat-landing_lights9.1-0 ; 
       fix_mat-south_block.1-0 ; 
       fix_mat-south_hull_2.3-0 ; 
       fix_mat-sphere1.1-0 ; 
       fix_mat-sphere1_1.1-0 ROOT ; 
       fix_mat-SS_01.1-0 ; 
       fix_mat-SS_10.1-0 ; 
       fix_mat-SS_29.1-0 ; 
       fix_mat-SS_30.1-0 ; 
       fix_mat-SS_31.1-0 ; 
       fix_mat-SS_32.1-0 ; 
       fix_mat-SS_33.1-0 ; 
       fix_mat-SS_34.1-0 ; 
       fix_mat-SS_35.1-0 ; 
       fix_mat-SS_36.1-0 ; 
       fix_mat-SS_37.1-0 ; 
       fix_mat-SS_38.1-0 ; 
       fix_mat-SS_39.1-0 ; 
       fix_mat-SS_40.1-0 ; 
       fix_mat-SS_41.1-0 ; 
       fix_mat-SS_42.1-0 ; 
       fix_mat-SS_43.1-0 ; 
       fix_mat-SS_44.1-0 ; 
       fix_mat-SS_45.1-0 ; 
       fix_mat-SS_46.1-0 ; 
       fix_mat-SS_66.1-0 ; 
       fix_mat-SS_67.1-0 ; 
       fix_mat-SS_68.1-0 ; 
       fix_mat-SS_69.1-0 ; 
       fix_mat-SS_70.1-0 ; 
       fix_mat-SS_71.1-0 ; 
       fix_mat-SS_72.1-0 ; 
       fix_mat-SS_73.1-0 ; 
       fix_mat-SS_74.1-0 ; 
       fix_mat-SS_75.1-0 ; 
       fix_mat-SS_76.1-0 ; 
       fix_mat-SS_77.1-0 ; 
       fix_mat-SS_78.1-0 ; 
       fix_mat-SS_79.1-0 ; 
       fix_mat-SS_8.1-0 ; 
       fix_mat-SS_8_1.1-0 ; 
       fix_mat-SS_80.1-0 ; 
       fix_mat-SS_81.1-0 ; 
       fix_mat-SS_82.1-0 ; 
       fix_mat-SS_83.1-0 ; 
       fix_mat-SS_84.1-0 ; 
       fix_mat-strobe_set1.1-0 ; 
       fix_mat-strobe_set3.1-0 ; 
       fix_mat-turwepemt1.1-0 ; 
       fix_mat-turwepemt2.1-0 ; 
       fix_mat-turwepemt3.1-0 ; 
       fix_mat-turwepemt4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-fix_mat.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 19 110 ; 
       1 5 110 ; 
       2 6 110 ; 
       3 1 110 ; 
       4 2 110 ; 
       5 7 110 ; 
       6 8 110 ; 
       7 19 110 ; 
       8 19 110 ; 
       9 19 110 ; 
       10 62 110 ; 
       11 62 110 ; 
       12 61 110 ; 
       13 61 110 ; 
       14 61 110 ; 
       15 62 110 ; 
       16 9 110 ; 
       17 19 110 ; 
       18 0 110 ; 
       20 16 110 ; 
       21 3 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 12 110 ; 
       25 12 110 ; 
       26 12 110 ; 
       27 12 110 ; 
       28 13 110 ; 
       29 13 110 ; 
       30 13 110 ; 
       31 13 110 ; 
       32 13 110 ; 
       33 13 110 ; 
       34 14 110 ; 
       35 14 110 ; 
       36 14 110 ; 
       37 14 110 ; 
       38 14 110 ; 
       39 14 110 ; 
       40 4 110 ; 
       41 15 110 ; 
       42 15 110 ; 
       43 15 110 ; 
       44 15 110 ; 
       45 15 110 ; 
       46 15 110 ; 
       47 10 110 ; 
       48 10 110 ; 
       49 10 110 ; 
       50 10 110 ; 
       51 10 110 ; 
       52 10 110 ; 
       53 11 110 ; 
       54 16 110 ; 
       55 17 110 ; 
       56 11 110 ; 
       57 11 110 ; 
       58 11 110 ; 
       59 11 110 ; 
       60 11 110 ; 
       61 1 110 ; 
       62 2 110 ; 
       63 17 110 ; 
       64 17 110 ; 
       65 1 110 ; 
       66 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 187.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 190 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 192.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 195 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 197.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 200 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 176.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 130 -6 0 MPRFLG 0 ; 
       3 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 103.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 60 -4 0 MPRFLG 0 ; 
       6 SCHEM 132.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 135 -2 0 MPRFLG 0 ; 
       9 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 113.75 -10 0 MPRFLG 0 ; 
       11 SCHEM 143.75 -10 0 MPRFLG 0 ; 
       12 SCHEM 56.25 -10 0 MPRFLG 0 ; 
       13 SCHEM 41.25 -10 0 MPRFLG 0 ; 
       14 SCHEM 71.25 -10 0 MPRFLG 0 ; 
       15 SCHEM 128.75 -10 0 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 MPRFLG 0 ; 
       17 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       18 SCHEM 175 -4 0 MPRFLG 0 ; 
       19 SCHEM 93.75 0 0 SRT 1.168 1.168 1.338528 -1.570796 0 0 0 0.6120043 2.936134 MPRFLG 0 ; 
       20 SCHEM 17.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 30 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 62.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 50 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 55 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 60 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 47.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 35 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 37.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 40 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 45 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 77.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 65 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 67.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 70 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 72.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 75 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 102.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 135 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 122.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 125 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 127.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 130 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 132.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 120 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 107.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 110 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 112.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 115 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 117.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 150 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 20 -6 0 WIRECOL 3 7 DISPLAY 1 2 MPRFLG 0 ; 
       55 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 137.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 140 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 142.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 145 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 147.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 56.25 -8 0 MPRFLG 0 ; 
       62 SCHEM 128.75 -8 0 MPRFLG 0 ; 
       63 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
