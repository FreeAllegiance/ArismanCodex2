SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.68-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       frames-light1.8-0 ROOT ; 
       frames-light2.8-0 ROOT ; 
       frames-light3.8-0 ROOT ; 
       frames-light4.8-0 ROOT ; 
       frames-light5.8-0 ROOT ; 
       frames-light6.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       frames-cube1.1-0 ; 
       frames-east_bay_7.7-0 ; 
       frames-east_bay_antenna.1-0 ; 
       frames-east_bay_strut_2.1-0 ; 
       frames-east_bay_strut2.5-0 ; 
       frames-garage1A.1-0 ; 
       frames-garage1B.1-0 ; 
       frames-garage1C.1-0 ; 
       frames-garage1D.1-0 ; 
       frames-garage1E.1-0 ; 
       frames-hullroot_1.1-0 ; 
       frames-launch.1-0 ; 
       frames-south_block.1-0 ; 
       frames-south_hull_2.3-0 ; 
       frames-sphere1.1-0 ; 
       frames-sphere1_1.3-0 ROOT ; 
       frames-SS_01.1-0 ; 
       frames-SS_10.1-0 ; 
       frames-SS_8.1-0 ; 
       frames-SS_8_1.1-0 ; 
       frames-strobe_set1.1-0 ; 
       frames-turwepemt1.1-0 ; 
       frames-turwepemt2.1-0 ; 
       frames-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-frames.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 15 110 ; 
       1 3 110 ; 
       2 1 110 ; 
       3 4 110 ; 
       4 15 110 ; 
       5 15 110 ; 
       6 15 110 ; 
       7 15 110 ; 
       8 15 110 ; 
       9 15 110 ; 
       10 15 110 ; 
       11 15 110 ; 
       12 10 110 ; 
       13 15 110 ; 
       14 0 110 ; 
       16 12 110 ; 
       17 2 110 ; 
       18 12 110 ; 
       19 13 110 ; 
       20 1 110 ; 
       21 13 110 ; 
       22 13 110 ; 
       23 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 142.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 145 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 147.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 150 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 152.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 155 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 125 -2 0 MPRFLG 0 ; 
       1 SCHEM 120 -6 0 MPRFLG 0 ; 
       2 SCHEM 117.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 120 -4 0 MPRFLG 0 ; 
       4 SCHEM 120 -2 0 MPRFLG 0 ; 
       5 SCHEM 127.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 130 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 132.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 135 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 137.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 113.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 140 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 113.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 107.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 125 -4 0 MPRFLG 0 ; 
       15 SCHEM 122.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 112.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 117.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 115 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 105 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 122.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
