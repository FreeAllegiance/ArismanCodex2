SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.88-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 12     
       fix_antenna-light1_1.5-0 ROOT ; 
       fix_antenna-light1_1_1.1-0 ROOT ; 
       fix_antenna-light2_1.5-0 ROOT ; 
       fix_antenna-light2_1_1.1-0 ROOT ; 
       fix_antenna-light3_1.5-0 ROOT ; 
       fix_antenna-light3_1_1.1-0 ROOT ; 
       fix_antenna-light4_1.5-0 ROOT ; 
       fix_antenna-light4_1_1.1-0 ROOT ; 
       fix_antenna-light5_1.5-0 ROOT ; 
       fix_antenna-light5_1_1.1-0 ROOT ; 
       fix_antenna-light6_1.5-0 ROOT ; 
       fix_antenna-light6_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       fix_antenna-COLORED_BAY.1-0 ; 
       fix_antenna-cube1.1-0 ; 
       fix_antenna-east_bay_antenna.1-0 ; 
       fix_antenna-east_bay_strut_2.1-0 ; 
       fix_antenna-east_bay_strut2_1.5-0 ; 
       fix_antenna-garage1A.1-0 ; 
       fix_antenna-garage1B.1-0 ; 
       fix_antenna-garage1C.1-0 ; 
       fix_antenna-garage1D.1-0 ; 
       fix_antenna-garage1E.1-0 ; 
       fix_antenna-hullroot_1.1-0 ; 
       fix_antenna-launch1.1-0 ; 
       fix_antenna-null4.1-0 ; 
       fix_antenna-null5.1-0 ; 
       fix_antenna-south_block.1-0 ; 
       fix_antenna-south_hull_2.3-0 ; 
       fix_antenna-sphere1.1-0 ; 
       fix_antenna-sphere1_1.2-0 ROOT ; 
       fix_antenna-SS_10.1-0 ; 
       fix_antenna-SS_66.1-0 ; 
       fix_antenna-SS_67.1-0 ; 
       fix_antenna-SS_78.1-0 ; 
       fix_antenna-SS_79.1-0 ; 
       fix_antenna-SS_8_1.1-0 ; 
       fix_antenna-SS_8_2.1-0 ; 
       fix_antenna-SS_80.1-0 ; 
       fix_antenna-SS_81.1-0 ; 
       fix_antenna-SS_82.1-0 ; 
       fix_antenna-SS_83.1-0 ; 
       fix_antenna-SS_84.1-0 ; 
       fix_antenna-SS_85.1-0 ; 
       fix_antenna-SS_86.1-0 ; 
       fix_antenna-SS_87.1-0 ; 
       fix_antenna-strobe_set1.1-0 ; 
       fix_antenna-turwepemt1.1-0 ; 
       fix_antenna-turwepemt2.1-0 ; 
       fix_antenna-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss90/PICTURES/ss27inside ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-freeze_scaling.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 17 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 17 110 ; 
       5 17 110 ; 
       6 17 110 ; 
       7 17 110 ; 
       8 17 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 10 110 ; 
       15 17 110 ; 
       16 1 110 ; 
       18 2 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 15 110 ; 
       24 15 110 ; 
       25 12 110 ; 
       26 12 110 ; 
       27 12 110 ; 
       28 13 110 ; 
       29 13 110 ; 
       30 13 110 ; 
       31 13 110 ; 
       32 13 110 ; 
       33 0 110 ; 
       34 15 110 ; 
       35 15 110 ; 
       36 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 10 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 15 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -10 0 MPRFLG 0 ; 
       1 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -12 0 MPRFLG 0 ; 
       3 SCHEM 30 -8 0 MPRFLG 0 ; 
       4 SCHEM 30 -6 0 MPRFLG 0 ; 
       5 SCHEM 50 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 52.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 55 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 60 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 62.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 40 -12 0 MPRFLG 0 ; 
       14 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       15 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 31.25 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 15 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 10 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 32.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 22.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 0 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 25 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 30 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 45 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 35 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 37.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 40 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 20 -12 0 MPRFLG 0 ; 
       34 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
