SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.80-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       move_nulls_out-light1_1.3-0 ROOT ; 
       move_nulls_out-light2_1.3-0 ROOT ; 
       move_nulls_out-light3_1.3-0 ROOT ; 
       move_nulls_out-light4_1.3-0 ROOT ; 
       move_nulls_out-light5_1.3-0 ROOT ; 
       move_nulls_out-light6_1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       move_nulls_out-COLORED_BAY.1-0 ; 
       move_nulls_out-cube1.1-0 ; 
       move_nulls_out-east_bay_antenna.1-0 ; 
       move_nulls_out-east_bay_strut_2.1-0 ; 
       move_nulls_out-east_bay_strut2_1.5-0 ; 
       move_nulls_out-garage1A.1-0 ; 
       move_nulls_out-garage1B.1-0 ; 
       move_nulls_out-garage1C.1-0 ; 
       move_nulls_out-garage1D.1-0 ; 
       move_nulls_out-garage1E.1-0 ; 
       move_nulls_out-hullroot_1.1-0 ; 
       move_nulls_out-launch1.1-0 ; 
       move_nulls_out-null4.1-0 ; 
       move_nulls_out-null5.1-0 ; 
       move_nulls_out-south_block.1-0 ; 
       move_nulls_out-south_hull_2.3-0 ; 
       move_nulls_out-sphere1.1-0 ; 
       move_nulls_out-sphere1_1.3-0 ROOT ; 
       move_nulls_out-SS_10.1-0 ; 
       move_nulls_out-SS_66.1-0 ; 
       move_nulls_out-SS_67.1-0 ; 
       move_nulls_out-SS_78.1-0 ; 
       move_nulls_out-SS_79.1-0 ; 
       move_nulls_out-SS_8_1.1-0 ; 
       move_nulls_out-SS_8_2.1-0 ; 
       move_nulls_out-SS_80.1-0 ; 
       move_nulls_out-SS_81.1-0 ; 
       move_nulls_out-SS_82.1-0 ; 
       move_nulls_out-SS_83.1-0 ; 
       move_nulls_out-SS_84.1-0 ; 
       move_nulls_out-SS_85.1-0 ; 
       move_nulls_out-SS_86.1-0 ; 
       move_nulls_out-SS_87.1-0 ; 
       move_nulls_out-strobe_set1.1-0 ; 
       move_nulls_out-turwepemt1.1-0 ; 
       move_nulls_out-turwepemt2.1-0 ; 
       move_nulls_out-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-move_nulls_out.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       24 15 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       12 0 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       25 12 110 ; 
       26 12 110 ; 
       27 12 110 ; 
       13 0 110 ; 
       28 13 110 ; 
       29 13 110 ; 
       30 13 110 ; 
       31 13 110 ; 
       32 13 110 ; 
       0 3 110 ; 
       1 17 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 17 110 ; 
       5 17 110 ; 
       6 17 110 ; 
       7 17 110 ; 
       8 17 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       14 10 110 ; 
       15 17 110 ; 
       16 1 110 ; 
       18 2 110 ; 
       23 15 110 ; 
       33 0 110 ; 
       34 15 110 ; 
       35 15 110 ; 
       36 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -21.23687 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -21.23687 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -21.23687 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -21.23687 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -21.23687 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -21.23687 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       24 SCHEM 5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 20 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 37.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 40 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 42.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 45 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 55 -12 0 MPRFLG 0 ; 
       28 SCHEM 60 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 50 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 52.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 55 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 57.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 50 -10 0 MPRFLG 0 ; 
       1 SCHEM 88.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 28.75 -12 0 MPRFLG 0 ; 
       3 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 55 -6 0 MPRFLG 0 ; 
       5 SCHEM 95 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 97.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 100 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 102.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 105 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 107.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 20 -8 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 56.25 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 27.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 0 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 35 -12 0 MPRFLG 0 ; 
       34 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
