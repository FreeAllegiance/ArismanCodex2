SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.46-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rotate-light1.11-0 ROOT ; 
       rotate-light2.11-0 ROOT ; 
       rotate-light3.11-0 ROOT ; 
       rotate-light4.11-0 ROOT ; 
       rotate-light5.11-0 ROOT ; 
       rotate-light6.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       rotate-cube1.1-0 ; 
       rotate-east_bay_7.7-0 ; 
       rotate-east_bay_antenna.1-0 ; 
       rotate-east_bay_strut_2.1-0 ; 
       rotate-east_bay_strut2.5-0 ; 
       rotate-hullroot_1.1-0 ; 
       rotate-landing_lights3.1-0 ; 
       rotate-landing_lights5.1-0 ; 
       rotate-south_block.1-0 ; 
       rotate-south_hull_2.3-0 ; 
       rotate-sphere1.1-0 ; 
       rotate-sphere1_1.11-0 ROOT ; 
       rotate-SS_01.1-0 ; 
       rotate-SS_10.1-0 ; 
       rotate-SS_29.1-0 ; 
       rotate-SS_30.1-0 ; 
       rotate-SS_31.1-0 ; 
       rotate-SS_32.1-0 ; 
       rotate-SS_33.1-0 ; 
       rotate-SS_34.1-0 ; 
       rotate-SS_41.1-0 ; 
       rotate-SS_42.1-0 ; 
       rotate-SS_43.1-0 ; 
       rotate-SS_44.1-0 ; 
       rotate-SS_45.1-0 ; 
       rotate-SS_46.1-0 ; 
       rotate-SS_8.1-0 ; 
       rotate-SS_8_1.1-0 ; 
       rotate-strobe_set1.1-0 ; 
       rotate-turwepemt1.1-0 ; 
       rotate-turwepemt2.1-0 ; 
       rotate-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-rotate.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       3 4 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       8 5 110 ; 
       9 11 110 ; 
       10 0 110 ; 
       12 8 110 ; 
       26 8 110 ; 
       27 9 110 ; 
       29 9 110 ; 
       30 9 110 ; 
       1 3 110 ; 
       2 1 110 ; 
       6 28 110 ; 
       7 28 110 ; 
       13 2 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 6 110 ; 
       20 7 110 ; 
       21 7 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
       25 7 110 ; 
       28 1 110 ; 
       31 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 86.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 50 -8 0 MPRFLG 0 ; 
       4 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       10 SCHEM 85 -8 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 15 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 0 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -12 0 MPRFLG 0 ; 
       6 SCHEM 38.75 -14 0 MPRFLG 0 ; 
       7 SCHEM 53.75 -14 0 MPRFLG 0 ; 
       13 SCHEM 25 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 45 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 35 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 40 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 42.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 60 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 50 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 52.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 55 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 57.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 46.25 -12 0 MPRFLG 0 ; 
       31 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
