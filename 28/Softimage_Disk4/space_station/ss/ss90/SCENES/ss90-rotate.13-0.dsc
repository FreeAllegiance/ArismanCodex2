SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.48-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rotate-light1.13-0 ROOT ; 
       rotate-light2.13-0 ROOT ; 
       rotate-light3.13-0 ROOT ; 
       rotate-light4.13-0 ROOT ; 
       rotate-light5.13-0 ROOT ; 
       rotate-light6.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       rotate-cube1.1-0 ; 
       rotate-east_bay_7.7-0 ; 
       rotate-east_bay_antenna.1-0 ; 
       rotate-east_bay_strut_2.1-0 ; 
       rotate-east_bay_strut2.5-0 ; 
       rotate-hullroot_1.1-0 ; 
       rotate-south_block.1-0 ; 
       rotate-south_hull_2.3-0 ; 
       rotate-sphere1.1-0 ; 
       rotate-sphere1_1.12-0 ROOT ; 
       rotate-SS_01.1-0 ; 
       rotate-SS_10.1-0 ; 
       rotate-SS_8.1-0 ; 
       rotate-SS_8_1.1-0 ; 
       rotate-strobe_set1.1-0 ; 
       rotate-turwepemt1.1-0 ; 
       rotate-turwepemt2.1-0 ; 
       rotate-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-rotate.13-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 3 110 ; 
       2 1 110 ; 
       3 4 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 5 110 ; 
       7 9 110 ; 
       8 0 110 ; 
       10 6 110 ; 
       11 2 110 ; 
       12 6 110 ; 
       13 7 110 ; 
       14 1 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 10 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 15 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 20 -8 0 MPRFLG 0 ; 
       15 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
