SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.7-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       text-light1.7-0 ROOT ; 
       text-light2.7-0 ROOT ; 
       text-light3.7-0 ROOT ; 
       text-light4.7-0 ROOT ; 
       text-light5.7-0 ROOT ; 
       text-light6.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       ss27-east_bay_7.7-0 ; 
       ss27-east_bay_antenna.1-0 ; 
       ss27-east_bay_strut_2.1-0 ; 
       ss27-east_bay_strut2.5-0 ROOT ; 
       ss27-landing_lights3.1-0 ; 
       ss27-landing_lights4.1-0 ; 
       ss27-landing_lights5.1-0 ; 
       ss27-south_hull_2.3-0 ROOT ; 
       ss27-SS_10.1-0 ; 
       ss27-SS_29.1-0 ; 
       ss27-SS_30.1-0 ; 
       ss27-SS_31.1-0 ; 
       ss27-SS_32.1-0 ; 
       ss27-SS_33.1-0 ; 
       ss27-SS_34.1-0 ; 
       ss27-SS_35.1-0 ; 
       ss27-SS_36.1-0 ; 
       ss27-SS_37.1-0 ; 
       ss27-SS_38.1-0 ; 
       ss27-SS_39.1-0 ; 
       ss27-SS_40.1-0 ; 
       ss27-SS_41.1-0 ; 
       ss27-SS_42.1-0 ; 
       ss27-SS_43.1-0 ; 
       ss27-SS_44.1-0 ; 
       ss27-SS_45.1-0 ; 
       ss27-SS_46.1-0 ; 
       ss27-SS_8.1-0 ; 
       ss27-strobe_set1.1-0 ; 
       ss27-turwepemt1.1-0 ; 
       ss27-turwepemt3.1-0 ; 
       start-cube1.1-0 ROOT ; 
       start-hullroot_1.4-0 ROOT ; 
       start-south_block.1-0 ; 
       start-SS_01.1-0 ; 
       start-SS_8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-start.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       33 32 110 ; 
       34 33 110 ; 
       35 33 110 ; 
       0 2 110 ; 
       1 0 110 ; 
       2 3 110 ; 
       4 28 110 ; 
       5 28 110 ; 
       6 28 110 ; 
       8 1 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 4 110 ; 
       15 5 110 ; 
       16 5 110 ; 
       17 5 110 ; 
       18 5 110 ; 
       19 5 110 ; 
       20 5 110 ; 
       21 6 110 ; 
       22 6 110 ; 
       23 6 110 ; 
       24 6 110 ; 
       25 6 110 ; 
       26 6 110 ; 
       27 7 110 ; 
       28 0 110 ; 
       29 7 110 ; 
       30 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 12.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 15 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       31 SCHEM 72.5 0 0 SRT 1 1 1 0 0 0 0.0001484901 3.11874 -0.2969055 MPRFLG 0 ; 
       32 SCHEM 6.25 0 0 SRT 1 1 1 3.576279e-007 0 0 0.001466274 -0.2853184 -2.924025 MPRFLG 0 ; 
       33 SCHEM 5 -2 0 MPRFLG 0 ; 
       34 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 30 -12 0 MPRFLG 0 ; 
       1 SCHEM 1.25 -14 0 MPRFLG 0 ; 
       2 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 35 -8 0 DISPLAY 1 2 SRT 0.59 0.59 0.59 -3.496911e-007 3.141593 -1.262824e-023 1.328412 -3.227051 -3.32405 MPRFLG 0 ; 
       4 SCHEM 28.75 -16 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -16 0 MPRFLG 0 ; 
       6 SCHEM 43.75 -16 0 MPRFLG 0 ; 
       7 SCHEM 5 -22 0 SRT 1 1 1 -3.496911e-007 0 0 0 2.087002e-007 0.5968131 MPRFLG 0 ; 
       8 SCHEM 0 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       9 SCHEM 35 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 25 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 30 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 20 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 10 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 15 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 17.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 50 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 37.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 40 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 42.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 45 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 47.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 2.5 -24 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 28.75 -14 0 MPRFLG 0 ; 
       29 SCHEM 0 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
