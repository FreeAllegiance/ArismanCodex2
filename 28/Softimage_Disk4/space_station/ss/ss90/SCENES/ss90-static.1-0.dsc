SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.90-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       fix_antenna-COLORED_BAY.1-0 ; 
       fix_antenna-cube1.1-0 ; 
       fix_antenna-east_bay_strut_2.1-0 ; 
       fix_antenna-east_bay_strut2_1.5-0 ; 
       fix_antenna-hullroot_1.1-0 ; 
       fix_antenna-null4.1-0 ; 
       fix_antenna-null5.1-0 ; 
       fix_antenna-south_block.1-0 ; 
       fix_antenna-south_hull_2.3-0 ; 
       fix_antenna-sphere1.1-0 ; 
       fix_antenna-sphere1_1.3-0 ROOT ; 
       fix_antenna-strobe_set1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss90/PICTURES/ss27inside ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-Static.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 10 110 ; 
       2 3 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 4 110 ; 
       8 10 110 ; 
       9 1 110 ; 
       11 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -6 0 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 MPRFLG 0 ; 
       10 SCHEM 8.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
