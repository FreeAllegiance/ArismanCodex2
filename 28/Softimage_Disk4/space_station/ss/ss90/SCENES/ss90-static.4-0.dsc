SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.73-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       Static-light1.1-0 ROOT ; 
       Static-light2.1-0 ROOT ; 
       Static-light3.1-0 ROOT ; 
       Static-light4.1-0 ROOT ; 
       Static-light5.1-0 ROOT ; 
       Static-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       Static-COLORED_BAY.1-0 ; 
       Static-cube1.1-0 ; 
       Static-east_bay_7.2-0 ROOT ; 
       Static-east_bay_strut_2.1-0 ; 
       Static-east_bay_strut2.5-0 ; 
       Static-hullroot_1.1-0 ; 
       Static-south_block.1-0 ; 
       Static-south_hull_2.3-0 ; 
       Static-sphere1.1-0 ; 
       Static-sphere1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss27inside ; 
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-static.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 9 110 ; 
       3 4 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 5 110 ; 
       7 9 110 ; 
       8 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -6 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 0.9980001 0.9980001 0.998 1.570796 5.6426e-007 8.742278e-008 -10.48147 4.14711 -2.269423 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 8.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
