SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.19-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       text-light1.19-0 ROOT ; 
       text-light2.19-0 ROOT ; 
       text-light3.19-0 ROOT ; 
       text-light4.19-0 ROOT ; 
       text-light5.19-0 ROOT ; 
       text-light6.19-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 66     
       text-cube1.1-0 ; 
       text-east_bay_7.7-0 ; 
       text-east_bay_9.1-0 ; 
       text-east_bay_antenna.1-0 ; 
       text-east_bay_antenna2.1-0 ; 
       text-east_bay_strut_2.1-0 ; 
       text-east_bay_strut_4.1-0 ; 
       text-east_bay_strut2.5-0 ; 
       text-east_bay_strut4.1-0 ; 
       text-hullroot_1.1-0 ; 
       text-landing_lights10.1-0 ; 
       text-landing_lights11.1-0 ; 
       text-landing_lights3.1-0 ; 
       text-landing_lights4.1-0 ; 
       text-landing_lights5.1-0 ; 
       text-landing_lights9.1-0 ; 
       text-south_block.1-0 ; 
       text-south_hull_2.3-0 ; 
       text-sphere1.1-0 ; 
       text-sphere1_1.3-0 ROOT ; 
       text-SS_01.1-0 ; 
       text-SS_10.1-0 ; 
       text-SS_29.1-0 ; 
       text-SS_30.1-0 ; 
       text-SS_31.1-0 ; 
       text-SS_32.1-0 ; 
       text-SS_33.1-0 ; 
       text-SS_34.1-0 ; 
       text-SS_35.1-0 ; 
       text-SS_36.1-0 ; 
       text-SS_37.1-0 ; 
       text-SS_38.1-0 ; 
       text-SS_39.1-0 ; 
       text-SS_40.1-0 ; 
       text-SS_41.1-0 ; 
       text-SS_42.1-0 ; 
       text-SS_43.1-0 ; 
       text-SS_44.1-0 ; 
       text-SS_45.1-0 ; 
       text-SS_46.1-0 ; 
       text-SS_66.1-0 ; 
       text-SS_67.1-0 ; 
       text-SS_68.1-0 ; 
       text-SS_69.1-0 ; 
       text-SS_70.1-0 ; 
       text-SS_71.1-0 ; 
       text-SS_72.1-0 ; 
       text-SS_73.1-0 ; 
       text-SS_74.1-0 ; 
       text-SS_75.1-0 ; 
       text-SS_76.1-0 ; 
       text-SS_77.1-0 ; 
       text-SS_78.1-0 ; 
       text-SS_79.1-0 ; 
       text-SS_8.1-0 ; 
       text-SS_8_1.1-0 ; 
       text-SS_80.1-0 ; 
       text-SS_81.1-0 ; 
       text-SS_82.1-0 ; 
       text-SS_83.1-0 ; 
       text-SS_84.1-0 ; 
       text-strobe_set1.1-0 ; 
       text-strobe_set3.1-0 ; 
       text-turwepemt1.1-0 ; 
       text-turwepemt3.1-0 ; 
       text-turwepemt5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss90/PICTURES/ss90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss90-text.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 19 110 ; 
       18 0 110 ; 
       1 5 110 ; 
       2 6 110 ; 
       3 1 110 ; 
       4 2 110 ; 
       5 7 110 ; 
       6 8 110 ; 
       7 19 110 ; 
       8 19 110 ; 
       9 19 110 ; 
       10 62 110 ; 
       11 62 110 ; 
       12 61 110 ; 
       13 61 110 ; 
       14 61 110 ; 
       15 62 110 ; 
       16 9 110 ; 
       17 19 110 ; 
       20 16 110 ; 
       21 3 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 12 110 ; 
       25 12 110 ; 
       26 12 110 ; 
       27 12 110 ; 
       28 13 110 ; 
       29 13 110 ; 
       30 13 110 ; 
       31 13 110 ; 
       32 13 110 ; 
       33 13 110 ; 
       34 14 110 ; 
       35 14 110 ; 
       36 14 110 ; 
       37 14 110 ; 
       38 14 110 ; 
       39 14 110 ; 
       40 4 110 ; 
       41 15 110 ; 
       42 15 110 ; 
       43 15 110 ; 
       44 15 110 ; 
       45 15 110 ; 
       46 15 110 ; 
       47 10 110 ; 
       48 10 110 ; 
       49 10 110 ; 
       50 10 110 ; 
       51 10 110 ; 
       52 10 110 ; 
       53 11 110 ; 
       54 16 110 ; 
       55 17 110 ; 
       56 11 110 ; 
       57 11 110 ; 
       58 11 110 ; 
       59 11 110 ; 
       60 11 110 ; 
       61 1 110 ; 
       62 2 110 ; 
       63 17 110 ; 
       64 1 110 ; 
       65 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 182.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 185 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 187.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 190 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 192.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 195 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 172.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 171.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 55 -6 0 MPRFLG 0 ; 
       2 SCHEM 127.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 98.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 130 -4 0 MPRFLG 0 ; 
       7 SCHEM 60 -2 0 MPRFLG 0 ; 
       8 SCHEM 132.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       10 SCHEM 111.25 -10 0 MPRFLG 0 ; 
       11 SCHEM 141.25 -10 0 MPRFLG 0 ; 
       12 SCHEM 53.75 -10 0 MPRFLG 0 ; 
       13 SCHEM 38.75 -10 0 MPRFLG 0 ; 
       14 SCHEM 68.75 -10 0 MPRFLG 0 ; 
       15 SCHEM 126.25 -10 0 MPRFLG 0 ; 
       16 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       19 SCHEM 91.25 0 0 SRT 1.168 1.168 1.338528 0 0 0 0 0.6120043 2.936134 MPRFLG 0 ; 
       20 SCHEM 15 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 60 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 50 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 52.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 55 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 45 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 32.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 35 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 40 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 42.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 75 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 62.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 65 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 67.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 70 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 72.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 97.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 132.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 120 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 122.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 125 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 127.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 130 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 117.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 105 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 107.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 110 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 112.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 115 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 147.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 17.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 135 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 137.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 140 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 142.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 145 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       62 SCHEM 126.25 -8 0 MPRFLG 0 ; 
       63 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
