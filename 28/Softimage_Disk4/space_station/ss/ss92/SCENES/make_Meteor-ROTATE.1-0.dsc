SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ROTATE-cam_int1.1-0 ROOT ; 
       ROTATE-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       ROTATE-ceiling_and_floor1.1-0 ; 
       ROTATE-entrance1.1-0 ; 
       ROTATE-entrance2.1-0 ; 
       ROTATE-mat4.1-0 ; 
       ROTATE-mat5.1-0 ; 
       ROTATE-mat6.1-0 ; 
       ROTATE-ring1.1-0 ; 
       ROTATE-walls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       1-BAY1.1-0 ; 
       1-BAY2.1-0 ; 
       1-garage1A.1-0 ; 
       1-garage1B.1-0 ; 
       1-garage1C.1-0 ; 
       1-garage1D.1-0 ; 
       1-garage1E.1-0 ; 
       1-garage2A.1-0 ; 
       1-garage2B.1-0 ; 
       1-garage2C.1-0 ; 
       1-garage2E.1-0 ; 
       1-garage3D.1-0 ; 
       1-poly_bean.30-0 ROOT ; 
       1-radar_base.1-0 ; 
       1-radar_Dish.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss92/PICTURES/ss92 ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/ss92a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       make_Meteor-ROTATE.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ROTATE-inside1.1-0 ; 
       ROTATE-t2d10.1-0 ; 
       ROTATE-t2d11.1-0 ; 
       ROTATE-t2d12.1-0 ; 
       ROTATE-t2d6.1-0 ; 
       ROTATE-t2d7.1-0 ; 
       ROTATE-t2d8.1-0 ; 
       ROTATE-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 12 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 1 110 ; 
       13 12 110 ; 
       14 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       12 3 300 ; 
       12 1 300 ; 
       12 2 300 ; 
       12 6 300 ; 
       12 7 300 ; 
       12 0 300 ; 
       13 4 300 ; 
       14 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 6 401 ; 
       2 5 401 ; 
       3 4 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 7 401 ; 
       7 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 20 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 25 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 30 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 23.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
