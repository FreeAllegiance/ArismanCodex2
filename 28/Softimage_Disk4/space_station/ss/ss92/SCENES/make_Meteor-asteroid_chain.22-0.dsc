SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       asteroid_chain-cam_int1.22-0 ROOT ; 
       asteroid_chain-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       asteroid_chain-inf_light1.18-0 ROOT ; 
       asteroid_chain-inf_light2.18-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       asteroid_chain-entrance1.1-0 ; 
       asteroid_chain-entrance2.1-0 ; 
       asteroid_chain-mat1.3-0 ; 
       asteroid_chain-mat2.2-0 ; 
       asteroid_chain-mat3.2-0 ; 
       asteroid_chain-mat4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       1-poly_bean.10-0 ROOT ; 
       asteroid_chain-bumper.6-0 ROOT ; 
       asteroid_chain-inner_tube_guide.1-0 ROOT ; 
       asteroid_chain-Trimmed_Meteor.3-0 ROOT ; 
       asteroid_chain1-bumper.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       E:/pete_data2/space_station/ss/ss92/PICTURES/VenusMap ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/bump ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/bump_Reveal ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/crater_type1 ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/crater_type2 ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/crater_type3 ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/rendermap ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/ss92a ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/venusbump ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       make_Meteor-asteroid_chain.22-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       asteroid_chain-bump_reveal1.3-0 ; 
       asteroid_chain-bump_reveal2.2-0 ; 
       asteroid_chain-bump1.6-0 ; 
       asteroid_chain-bump2.2-0 ; 
       asteroid_chain-rendermap1.9-0 ; 
       asteroid_chain-rendermap2.6-0 ; 
       asteroid_chain-rendermap3.2-0 ; 
       asteroid_chain-t2d1.5-0 ; 
       asteroid_chain-t2d2.4-0 ; 
       asteroid_chain-t2d3.4-0 ; 
       asteroid_chain-t2d4.4-0 ; 
       asteroid_chain-t2d5.4-0 ; 
       asteroid_chain-t2d6.3-0 ; 
       asteroid_chain-venusbump1.5-0 ; 
       asteroid_chain-venusbump2.2-0 ; 
       asteroid_chain-venusmap1.4-0 ; 
       asteroid_chain-venusmap2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       asteroid_chain-rock1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       1 2 300 ; 
       4 3 300 ; 
       3 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 7 400 ; 
       1 8 400 ; 
       1 9 400 ; 
       1 10 400 ; 
       1 11 400 ; 
       4 2 400 ; 
       4 13 400 ; 
       4 15 400 ; 
       4 0 400 ; 
       3 3 400 ; 
       3 14 400 ; 
       3 16 400 ; 
       3 1 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       1 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 57.5 0 0 DISPLAY 0 0 SRT 1 1.596372 1 0 0 1.570796 0.172062 0 0 MPRFLG 0 ; 
       0 SCHEM 47.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.001161084 0 0 MPRFLG 0 ; 
       1 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 DISPLAY 1 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 35 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 40 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
