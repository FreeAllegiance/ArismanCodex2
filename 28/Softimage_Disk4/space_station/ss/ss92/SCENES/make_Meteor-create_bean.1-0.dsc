SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       create_bean-cam_int1.1-0 ROOT ; 
       create_bean-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       create_bean-null1.1-0 ROOT ; 
       create_bean-nurbs1.1-0 ROOT ; 
       create_bean-nurbs10.1-0 ; 
       create_bean-nurbs11.1-0 ; 
       create_bean-nurbs12.1-0 ; 
       create_bean-nurbs2.1-0 ; 
       create_bean-nurbs3.1-0 ; 
       create_bean-nurbs4.1-0 ; 
       create_bean-nurbs5.1-0 ; 
       create_bean-nurbs6.1-0 ; 
       create_bean-nurbs7.1-0 ; 
       create_bean-nurbs8.1-0 ; 
       create_bean-nurbs9.1-0 ; 
       create_bean-skin1.1-0 ROOT ; 
       create_bean-skin2.1-0 ROOT ; 
       create_bean-sphere1.1-0 ROOT ; 
       create_bean-sphere5.1-0 ROOT ; 
       create_bean-sphere6.1-0 ROOT ; 
       create_bean-sphere7.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       make_Meteor-create_bean.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       15 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1.192 0.578 0.9277317 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 35.39047 4.002289 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 42.89047 4.002289 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 22.47332 -5.035688 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 37.89047 4.002289 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 40.39047 4.002289 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
