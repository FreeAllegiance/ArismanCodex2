SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly_bean-cam_int1.16-0 ROOT ; 
       poly_bean-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       poly_bean-entrance1.3-0 ; 
       poly_bean-entrance2.3-0 ; 
       poly_bean-inside1.1-0 ; 
       poly_bean-mat4.3-0 ; 
       poly_bean-ring1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       1-poly_bean.21-0 ROOT ; 
       poly_bean-inner_tube_guide.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss92/PICTURES/ss92 ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/ss92a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       make_Meteor-poly_bean.16-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       poly_bean-inside1.3-0 ; 
       poly_bean-t2d6.5-0 ; 
       poly_bean-t2d7.4-0 ; 
       poly_bean-t2d8.4-0 ; 
       poly_bean-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 4 300 ; 
       0 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 2 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 -0.001161084 0 0 MPRFLG 0 ; 
       1 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1.596372 1 0 0 1.570796 0.172062 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
