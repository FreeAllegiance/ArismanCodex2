SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       splines-cam_int1.1-0 ROOT ; 
       splines-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       splines-null1.1-0 ROOT ; 
       splines-nurbs1.1-0 ROOT ; 
       splines-nurbs10.1-0 ; 
       splines-nurbs11.1-0 ; 
       splines-nurbs12.1-0 ; 
       splines-nurbs2.1-0 ; 
       splines-nurbs3.1-0 ; 
       splines-nurbs4.1-0 ; 
       splines-nurbs5.1-0 ; 
       splines-nurbs6.1-0 ; 
       splines-nurbs7.1-0 ; 
       splines-nurbs8.1-0 ; 
       splines-nurbs9.1-0 ; 
       splines-sphere1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       make_Meteor-splines.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1.192 0.578 0.9277317 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 MPRFLG 0 ; 
       0 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 20 -2 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 25 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
