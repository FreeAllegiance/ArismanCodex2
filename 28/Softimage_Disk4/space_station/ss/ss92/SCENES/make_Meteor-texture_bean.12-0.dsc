SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_bean-cam_int1.12-0 ROOT ; 
       texture_bean-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       texture_bean-ceiling_and_floor1.2-0 ; 
       texture_bean-entrance1.3-0 ; 
       texture_bean-entrance2.3-0 ; 
       texture_bean-mat4.3-0 ; 
       texture_bean-mat5.1-0 ; 
       texture_bean-mat6.1-0 ; 
       texture_bean-ring1.3-0 ; 
       texture_bean-walls.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       1-poly_bean.28-0 ROOT ; 
       1-radar_base.1-0 ; 
       1-radar_Dish.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss92/PICTURES/ss92 ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/ss92a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       make_Meteor-texture_bean.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       texture_bean-inside1.5-0 ; 
       texture_bean-t2d10.4-0 ; 
       texture_bean-t2d11.2-0 ; 
       texture_bean-t2d12.2-0 ; 
       texture_bean-t2d6.5-0 ; 
       texture_bean-t2d7.5-0 ; 
       texture_bean-t2d8.5-0 ; 
       texture_bean-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 0 300 ; 
       1 4 300 ; 
       2 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 6 401 ; 
       2 5 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       3 4 401 ; 
       6 7 401 ; 
       7 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
