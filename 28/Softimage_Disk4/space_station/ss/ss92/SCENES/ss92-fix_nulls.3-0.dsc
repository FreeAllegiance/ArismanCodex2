SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fix_nulls-cam_int1.10-0 ROOT ; 
       fix_nulls-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       fix_nulls-ceiling_and_floor1.1-0 ; 
       fix_nulls-entrance1.1-0 ; 
       fix_nulls-entrance2.1-0 ; 
       fix_nulls-mat4.1-0 ; 
       fix_nulls-mat5.1-0 ; 
       fix_nulls-mat6.1-0 ; 
       fix_nulls-ring1.1-0 ; 
       fix_nulls-walls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       1-BAY1.1-0 ; 
       1-garage1A.1-0 ; 
       1-garage1B.1-0 ; 
       1-garage1C.1-0 ; 
       1-garage1D.1-0 ; 
       1-garage1E.1-0 ; 
       1-launch1.1-0 ; 
       1-poly_bean.38-0 ROOT ; 
       1-radar_base.1-0 ; 
       1-radar_Dish.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss92/PICTURES/ss92 ; 
       E:/pete_data2/space_station/ss/ss92/PICTURES/ss92a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss92-fix_nulls.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       fix_nulls-inside1.4-0 ; 
       fix_nulls-t2d10.4-0 ; 
       fix_nulls-t2d11.3-0 ; 
       fix_nulls-t2d12.3-0 ; 
       fix_nulls-t2d6.4-0 ; 
       fix_nulls-t2d7.4-0 ; 
       fix_nulls-t2d8.4-0 ; 
       fix_nulls-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 7 110 ; 
       8 7 110 ; 
       9 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 3 300 ; 
       7 1 300 ; 
       7 2 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       7 0 300 ; 
       8 4 300 ; 
       9 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 6 401 ; 
       2 5 401 ; 
       3 4 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 7 401 ; 
       7 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
