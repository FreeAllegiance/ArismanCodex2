SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       move_nulls_out-cam_int1.1-0 ROOT ; 
       move_nulls_out-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       move_nulls_out-ceiling_and_floor1.1-0 ; 
       move_nulls_out-entrance1.1-0 ; 
       move_nulls_out-entrance2.1-0 ; 
       move_nulls_out-mat313.1-0 ; 
       move_nulls_out-mat317.1-0 ; 
       move_nulls_out-mat321.1-0 ; 
       move_nulls_out-mat4.1-0 ; 
       move_nulls_out-mat5.1-0 ; 
       move_nulls_out-mat6.1-0 ; 
       move_nulls_out-ring1.1-0 ; 
       move_nulls_out-walls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       1-BAY1.1-0 ; 
       1-garage1A.1-0 ; 
       1-garage1B.1-0 ; 
       1-garage1C.1-0 ; 
       1-garage1D.1-0 ; 
       1-garage1E.1-0 ; 
       1-launch1.1-0 ; 
       1-null1.1-0 ; 
       1-null2.1-0 ; 
       1-poly_bean.40-0 ROOT ; 
       1-radar_base.1-0 ; 
       1-radar_Dish.1-0 ; 
       1-ss01.1-0 ; 
       1-ss2.1-0 ; 
       1-ss3.1-0 ; 
       1-ss4.1-0 ; 
       1-ss5.1-0 ; 
       1-ss6.1-0 ; 
       1-ss7.1-0 ; 
       1-ss8.1-0 ; 
       1-ss9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss92/PICTURES/ss92 ; 
       E:/Pete_Data2/space_station/ss/ss92/PICTURES/ss92a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss92-move_nulls_out.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       move_nulls_out-inside1.1-0 ; 
       move_nulls_out-t2d10.1-0 ; 
       move_nulls_out-t2d11.1-0 ; 
       move_nulls_out-t2d12.1-0 ; 
       move_nulls_out-t2d6.1-0 ; 
       move_nulls_out-t2d7.1-0 ; 
       move_nulls_out-t2d8.1-0 ; 
       move_nulls_out-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 9 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 9 110 ; 
       13 8 110 ; 
       14 8 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       7 9 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       19 7 110 ; 
       20 7 110 ; 
       8 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 6 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 9 300 ; 
       9 10 300 ; 
       9 0 300 ; 
       10 7 300 ; 
       11 8 300 ; 
       12 5 300 ; 
       13 4 300 ; 
       14 4 300 ; 
       15 4 300 ; 
       16 4 300 ; 
       17 3 300 ; 
       18 3 300 ; 
       19 3 300 ; 
       20 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 6 401 ; 
       2 5 401 ; 
       6 4 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 7 401 ; 
       10 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 22.5 -2 0 WIRECOL 3 7 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 35 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 37.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 40 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       7 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       17 SCHEM 25 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 30 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 32.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       8 SCHEM 38.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 60 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
