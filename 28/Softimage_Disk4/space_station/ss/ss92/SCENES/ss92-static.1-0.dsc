SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       static-cam_int1.5-0 ROOT ; 
       static-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       static-ceiling_and_floor1.3-0 ; 
       static-entrance1.3-0 ; 
       static-entrance2.3-0 ; 
       static-mat4.3-0 ; 
       static-ring1.3-0 ; 
       static-walls.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       ss92-poly_bean.38-0 ; 
       ss92-root.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss92/PICTURES/ss92 ; 
       E:/Pete_Data2/space_station/ss/ss92/PICTURES/ss92a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss92-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       static-inside1.3-0 ; 
       static-t2d10.3-0 ; 
       static-t2d6.3-0 ; 
       static-t2d7.3-0 ; 
       static-t2d8.3-0 ; 
       static-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 4 401 ; 
       2 3 401 ; 
       3 2 401 ; 
       4 5 401 ; 
       5 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 8.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
