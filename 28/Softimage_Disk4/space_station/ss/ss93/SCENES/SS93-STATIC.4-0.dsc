SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.73-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_nulls-light1.37-0 ROOT ; 
       add_nulls-light2.37-0 ROOT ; 
       add_nulls-light3.37-0 ROOT ; 
       add_nulls-light4.37-0 ROOT ; 
       add_nulls-light5.37-0 ROOT ; 
       add_nulls-light6.37-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 70     
       add_nulls-bays1.2-0 ; 
       add_nulls-mat1.2-0 ; 
       add_nulls-mat100.2-0 ; 
       add_nulls-mat101.2-0 ; 
       add_nulls-mat102.2-0 ; 
       add_nulls-mat103.2-0 ; 
       add_nulls-mat104.2-0 ; 
       add_nulls-mat105.2-0 ; 
       add_nulls-mat106.2-0 ; 
       add_nulls-mat107.2-0 ; 
       add_nulls-mat108.2-0 ; 
       add_nulls-mat109.2-0 ; 
       add_nulls-mat12.2-0 ; 
       add_nulls-mat35.2-0 ; 
       add_nulls-mat37.2-0 ; 
       add_nulls-mat40.2-0 ; 
       add_nulls-mat41.2-0 ; 
       add_nulls-mat42.2-0 ; 
       add_nulls-mat43.2-0 ; 
       add_nulls-mat44.2-0 ; 
       add_nulls-mat45.2-0 ; 
       add_nulls-mat46.2-0 ; 
       add_nulls-mat47.2-0 ; 
       add_nulls-mat48.2-0 ; 
       add_nulls-mat49.2-0 ; 
       add_nulls-mat50.2-0 ; 
       add_nulls-mat51.2-0 ; 
       add_nulls-mat52.2-0 ; 
       add_nulls-mat53.2-0 ; 
       add_nulls-mat54.2-0 ; 
       add_nulls-mat55.2-0 ; 
       add_nulls-mat56.2-0 ; 
       add_nulls-mat57.2-0 ; 
       add_nulls-mat58.2-0 ; 
       add_nulls-mat59.2-0 ; 
       add_nulls-mat60.2-0 ; 
       add_nulls-mat61.2-0 ; 
       add_nulls-mat62.2-0 ; 
       add_nulls-mat63.2-0 ; 
       add_nulls-mat64.2-0 ; 
       add_nulls-mat65.2-0 ; 
       add_nulls-mat66.2-0 ; 
       add_nulls-mat67.2-0 ; 
       add_nulls-mat68.2-0 ; 
       add_nulls-mat69.2-0 ; 
       add_nulls-mat70.2-0 ; 
       add_nulls-mat71.2-0 ; 
       add_nulls-mat72.2-0 ; 
       add_nulls-mat73.2-0 ; 
       add_nulls-mat74.2-0 ; 
       add_nulls-mat80.2-0 ; 
       add_nulls-mat82.2-0 ; 
       add_nulls-mat84.2-0 ; 
       add_nulls-mat89.2-0 ; 
       add_nulls-mat90.2-0 ; 
       add_nulls-mat91.2-0 ; 
       add_nulls-mat92.2-0 ; 
       add_nulls-mat93.2-0 ; 
       add_nulls-mat94.2-0 ; 
       add_nulls-mat95.2-0 ; 
       add_nulls-mat96.2-0 ; 
       add_nulls-mat97.2-0 ; 
       add_nulls-mat98.2-0 ; 
       add_nulls-mat99.2-0 ; 
       poly-mat3.5-0 ; 
       STATIC-mat187.3-0 ; 
       STATIC-mat188.3-0 ; 
       STATIC-mat189.3-0 ; 
       STATIC-mat190.3-0 ; 
       STATIC-mat191.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       ss93-bfuselg1.1-0 ; 
       ss93-bfuselg2.1-0 ; 
       ss93-craterer.4-0 ; 
       ss93-cyl1.1-0 ; 
       ss93-doccon1.1-0 ; 
       ss93-doccon2.1-0 ; 
       ss93-doccon3.1-0 ; 
       ss93-doccon4.1-0 ; 
       ss93-fuselg.5-0 ; 
       ss93-LAUNCH_TUBE.2-0 ; 
       ss93-LAUNCH_TUBE_1.3-0 ; 
       ss93-mfuselg0.1-0 ; 
       ss93-mfuselg1.1-0 ; 
       ss93-mfuselg2.1-0 ; 
       ss93-mfuselg3.1-0 ; 
       ss93-mfuselg4.1-0 ; 
       ss93-ss93.42-0 ROOT ; 
       ss93-tdoccon.1-0 ; 
       ss93-tfuselg1.1-0 ; 
       ss93-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss93/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss93/PICTURES/ss93 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss93/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       SS93-STATIC.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 69     
       add_nulls-qs.4-0 ; 
       add_nulls-t2d100.4-0 ; 
       add_nulls-t2d101.4-0 ; 
       add_nulls-t2d102.4-0 ; 
       add_nulls-t2d103.4-0 ; 
       add_nulls-t2d104.4-0 ; 
       add_nulls-t2d105.4-0 ; 
       add_nulls-t2d106.4-0 ; 
       add_nulls-t2d107.5-0 ; 
       add_nulls-t2d30.4-0 ; 
       add_nulls-t2d32.4-0 ; 
       add_nulls-t2d35.4-0 ; 
       add_nulls-t2d38.4-0 ; 
       add_nulls-t2d39.4-0 ; 
       add_nulls-t2d40.4-0 ; 
       add_nulls-t2d41.4-0 ; 
       add_nulls-t2d42.4-0 ; 
       add_nulls-t2d43.4-0 ; 
       add_nulls-t2d44.4-0 ; 
       add_nulls-t2d45.4-0 ; 
       add_nulls-t2d46.4-0 ; 
       add_nulls-t2d47.4-0 ; 
       add_nulls-t2d48.4-0 ; 
       add_nulls-t2d49.4-0 ; 
       add_nulls-t2d50.4-0 ; 
       add_nulls-t2d51.4-0 ; 
       add_nulls-t2d52.4-0 ; 
       add_nulls-t2d53.4-0 ; 
       add_nulls-t2d54.4-0 ; 
       add_nulls-t2d55.4-0 ; 
       add_nulls-t2d56.4-0 ; 
       add_nulls-t2d57.4-0 ; 
       add_nulls-t2d58.4-0 ; 
       add_nulls-t2d59.4-0 ; 
       add_nulls-t2d60.4-0 ; 
       add_nulls-t2d61.4-0 ; 
       add_nulls-t2d63.4-0 ; 
       add_nulls-t2d64.4-0 ; 
       add_nulls-t2d65.4-0 ; 
       add_nulls-t2d66.4-0 ; 
       add_nulls-t2d67.4-0 ; 
       add_nulls-t2d68.4-0 ; 
       add_nulls-t2d69.4-0 ; 
       add_nulls-t2d71.4-0 ; 
       add_nulls-t2d75.4-0 ; 
       add_nulls-t2d80.4-0 ; 
       add_nulls-t2d81.4-0 ; 
       add_nulls-t2d84.4-0 ; 
       add_nulls-t2d86.4-0 ; 
       add_nulls-t2d87.4-0 ; 
       add_nulls-t2d88.4-0 ; 
       add_nulls-t2d89.4-0 ; 
       add_nulls-t2d90.4-0 ; 
       add_nulls-t2d91.4-0 ; 
       add_nulls-t2d92.4-0 ; 
       add_nulls-t2d93.4-0 ; 
       add_nulls-t2d94.4-0 ; 
       add_nulls-t2d95.4-0 ; 
       add_nulls-t2d96.4-0 ; 
       add_nulls-t2d97.4-0 ; 
       add_nulls-t2d98.4-0 ; 
       add_nulls-t2d99.4-0 ; 
       add_nulls-_t2d62.4-0 ; 
       poly-rendermap1.14-0 ; 
       STATIC-t2d114.3-0 ; 
       STATIC-t2d115.3-0 ; 
       STATIC-t2d116.3-0 ; 
       STATIC-t2d117.3-0 ; 
       STATIC-t2d118.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 0 110 ; 
       2 16 110 ; 
       3 2 110 ; 
       4 12 110 ; 
       5 13 110 ; 
       6 14 110 ; 
       7 15 110 ; 
       8 16 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 8 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       17 19 110 ; 
       18 8 110 ; 
       19 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       0 38 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       0 41 300 ; 
       0 42 300 ; 
       0 43 300 ; 
       1 1 300 ; 
       1 44 300 ; 
       1 45 300 ; 
       1 46 300 ; 
       1 47 300 ; 
       2 64 300 ; 
       2 0 300 ; 
       3 69 300 ; 
       8 1 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       8 14 300 ; 
       8 48 300 ; 
       8 49 300 ; 
       8 50 300 ; 
       8 51 300 ; 
       8 52 300 ; 
       8 53 300 ; 
       9 67 300 ; 
       9 68 300 ; 
       10 65 300 ; 
       10 66 300 ; 
       12 1 300 ; 
       12 57 300 ; 
       12 61 300 ; 
       12 3 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       13 1 300 ; 
       13 54 300 ; 
       13 58 300 ; 
       13 62 300 ; 
       13 4 300 ; 
       13 5 300 ; 
       14 1 300 ; 
       14 55 300 ; 
       14 59 300 ; 
       14 63 300 ; 
       14 6 300 ; 
       14 7 300 ; 
       15 1 300 ; 
       15 56 300 ; 
       15 60 300 ; 
       15 2 300 ; 
       15 8 300 ; 
       15 9 300 ; 
       16 1 300 ; 
       18 1 300 ; 
       18 25 300 ; 
       18 32 300 ; 
       18 33 300 ; 
       18 34 300 ; 
       18 35 300 ; 
       19 1 300 ; 
       19 15 300 ; 
       19 16 300 ; 
       19 17 300 ; 
       19 18 300 ; 
       19 19 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       19 22 300 ; 
       19 23 300 ; 
       19 24 300 ; 
       19 26 300 ; 
       19 27 300 ; 
       19 28 300 ; 
       19 29 300 ; 
       19 30 300 ; 
       19 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 37 401 ; 
       2 58 401 ; 
       3 59 401 ; 
       4 60 401 ; 
       5 61 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 11 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 16 401 ; 
       16 18 401 ; 
       17 20 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 17 401 ; 
       23 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 62 401 ; 
       40 36 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       46 42 401 ; 
       47 0 401 ; 
       48 45 401 ; 
       49 46 401 ; 
       50 44 401 ; 
       51 7 401 ; 
       52 43 401 ; 
       53 47 401 ; 
       54 48 401 ; 
       55 49 401 ; 
       56 50 401 ; 
       57 51 401 ; 
       58 52 401 ; 
       59 53 401 ; 
       60 54 401 ; 
       61 55 401 ; 
       62 56 401 ; 
       63 57 401 ; 
       65 64 401 ; 
       66 65 401 ; 
       67 66 401 ; 
       68 67 401 ; 
       69 68 401 ; 
       64 63 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -8 0 MPRFLG 0 ; 
       1 SCHEM 10 -10 0 MPRFLG 0 ; 
       2 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 0 -12 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       6 SCHEM 5 -12 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       8 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 15 -10 0 MPRFLG 0 ; 
       11 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 0 -10 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 5 -10 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 8.75 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
