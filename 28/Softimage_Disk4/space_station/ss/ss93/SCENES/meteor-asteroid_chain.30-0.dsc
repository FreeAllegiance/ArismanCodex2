SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       asteroid_chain-cam_int1.30-0 ROOT ; 
       asteroid_chain-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       asteroid_chain-inf_light1.23-0 ROOT ; 
       asteroid_chain-inf_light2.23-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       asteroid_chain-mat1.7-0 ; 
       asteroid_chain-mat2.3-0 ; 
       asteroid_chain-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       asteroid_chain-craterer.18-0 ROOT ; 
       asteroid_chain3-craterer.4-0 ROOT ; 
       asteroid_chain4-craterer.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       E:/pete_data2/space_station/ss/ss93/PICTURES/bottom_of_craters ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/crater-rendermap ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/crater1 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/crater2 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/multi-craters ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/rendermap ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       meteor-asteroid_chain.30-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       asteroid_chain-bottom_of_craters1.8-0 ; 
       asteroid_chain-crater-rendermap1.3-0 ; 
       asteroid_chain-crater1a2.3-0 ; 
       asteroid_chain-crater2a2.3-0 ; 
       asteroid_chain-crater3.3-0 ; 
       asteroid_chain-crater4.3-0 ; 
       asteroid_chain-craters.8-0 ; 
       asteroid_chain-rendermap.6-0 ; 
       asteroid_chain-rendermap1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       asteroid_chain-rock03.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
       2 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 6 400 ; 
       0 0 400 ; 
       1 4 400 ; 
       1 2 400 ; 
       1 5 400 ; 
       1 3 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 1 401 ; 
       2 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
