SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       asteroid_chain-cam_int1.35-0 ROOT ; 
       asteroid_chain-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 11     
       asteroid_chain-inf_light1.28-0 ROOT ; 
       asteroid_chain-inf_light2.28-0 ROOT ; 
       asteroid_chain_add_light-light1.1-0 ROOT ; 
       asteroid_chain_add_light-spot1.1-0 ; 
       asteroid_chain_add_light-spot1_int.1-0 ROOT ; 
       asteroid_chain_add_light-spot2.1-0 ; 
       asteroid_chain_add_light-spot2_int.1-0 ROOT ; 
       asteroid_chain_add_light-spot3.1-0 ; 
       asteroid_chain_add_light-spot3_int.1-0 ROOT ; 
       asteroid_chain_add_light-spot4.1-0 ; 
       asteroid_chain_add_light-spot4_int.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 67     
       asteroid_chain-mat1.8-0 ; 
       asteroid_chain-mat2.3-0 ; 
       asteroid_chain-mat3.2-0 ; 
       asteroid_chain_add_light-mat1.1-0 ; 
       asteroid_chain_add_light-mat100.1-0 ; 
       asteroid_chain_add_light-mat101.1-0 ; 
       asteroid_chain_add_light-mat102.1-0 ; 
       asteroid_chain_add_light-mat103.1-0 ; 
       asteroid_chain_add_light-mat104.1-0 ; 
       asteroid_chain_add_light-mat105.1-0 ; 
       asteroid_chain_add_light-mat106.1-0 ; 
       asteroid_chain_add_light-mat107.1-0 ; 
       asteroid_chain_add_light-mat108.1-0 ; 
       asteroid_chain_add_light-mat109.1-0 ; 
       asteroid_chain_add_light-mat12.1-0 ; 
       asteroid_chain_add_light-mat35.1-0 ; 
       asteroid_chain_add_light-mat37.1-0 ; 
       asteroid_chain_add_light-mat40.1-0 ; 
       asteroid_chain_add_light-mat41.1-0 ; 
       asteroid_chain_add_light-mat42.1-0 ; 
       asteroid_chain_add_light-mat43.1-0 ; 
       asteroid_chain_add_light-mat44.1-0 ; 
       asteroid_chain_add_light-mat45.1-0 ; 
       asteroid_chain_add_light-mat46.1-0 ; 
       asteroid_chain_add_light-mat47.1-0 ; 
       asteroid_chain_add_light-mat48.1-0 ; 
       asteroid_chain_add_light-mat49.1-0 ; 
       asteroid_chain_add_light-mat50.1-0 ; 
       asteroid_chain_add_light-mat51.1-0 ; 
       asteroid_chain_add_light-mat52.1-0 ; 
       asteroid_chain_add_light-mat53.1-0 ; 
       asteroid_chain_add_light-mat54.1-0 ; 
       asteroid_chain_add_light-mat55.1-0 ; 
       asteroid_chain_add_light-mat56.1-0 ; 
       asteroid_chain_add_light-mat57.1-0 ; 
       asteroid_chain_add_light-mat58.1-0 ; 
       asteroid_chain_add_light-mat59.1-0 ; 
       asteroid_chain_add_light-mat60.1-0 ; 
       asteroid_chain_add_light-mat61.1-0 ; 
       asteroid_chain_add_light-mat62.1-0 ; 
       asteroid_chain_add_light-mat63.1-0 ; 
       asteroid_chain_add_light-mat64.1-0 ; 
       asteroid_chain_add_light-mat65.1-0 ; 
       asteroid_chain_add_light-mat66.1-0 ; 
       asteroid_chain_add_light-mat67.1-0 ; 
       asteroid_chain_add_light-mat68.1-0 ; 
       asteroid_chain_add_light-mat69.1-0 ; 
       asteroid_chain_add_light-mat70.1-0 ; 
       asteroid_chain_add_light-mat71.1-0 ; 
       asteroid_chain_add_light-mat72.1-0 ; 
       asteroid_chain_add_light-mat73.1-0 ; 
       asteroid_chain_add_light-mat74.1-0 ; 
       asteroid_chain_add_light-mat80.1-0 ; 
       asteroid_chain_add_light-mat82.1-0 ; 
       asteroid_chain_add_light-mat84.1-0 ; 
       asteroid_chain_add_light-mat89.1-0 ; 
       asteroid_chain_add_light-mat90.1-0 ; 
       asteroid_chain_add_light-mat91.1-0 ; 
       asteroid_chain_add_light-mat92.1-0 ; 
       asteroid_chain_add_light-mat93.1-0 ; 
       asteroid_chain_add_light-mat94.1-0 ; 
       asteroid_chain_add_light-mat95.1-0 ; 
       asteroid_chain_add_light-mat96.1-0 ; 
       asteroid_chain_add_light-mat97.1-0 ; 
       asteroid_chain_add_light-mat98.1-0 ; 
       asteroid_chain_add_light-mat99.1-0 ; 
       poly-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       asteroid_chain-craterer.23-0 ROOT ; 
       asteroid_chain_add_light-circle1.1-0 ROOT ; 
       asteroid_chain_add_light-circle2.1-0 ROOT ; 
       asteroid_chain3-craterer.6-0 ROOT ; 
       asteroid_chain4-craterer.5-0 ROOT ; 
       ss93-bfuselg1.1-0 ; 
       ss93-bfuselg2.1-0 ; 
       ss93-craterer.4-0 ; 
       ss93-doccon1.1-0 ; 
       ss93-doccon2.1-0 ; 
       ss93-doccon3.1-0 ; 
       ss93-doccon4.1-0 ; 
       ss93-fuselg.5-0 ; 
       ss93-mfuselg0.1-0 ; 
       ss93-mfuselg1.1-0 ; 
       ss93-mfuselg2.1-0 ; 
       ss93-mfuselg3.1-0 ; 
       ss93-mfuselg4.1-0 ; 
       ss93-ss93.3-0 ROOT ; 
       ss93-tdoccon.1-0 ; 
       ss93-tfuselg1.1-0 ; 
       ss93-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 10     
       E:/pete_data2/space_station/ss/ss93/PICTURES/VenusMap ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/bottom_of_craters ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/crater-rendermap ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/crater1 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/crater2 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/multi-craters ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/rendermap ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93a ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/venusbump ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       meteor-asteroid_chain_add_light.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 74     
       asteroid_chain-bottom_of_craters1.12-0 ; 
       asteroid_chain-crater-rendermap1.4-0 ; 
       asteroid_chain-crater1a2.4-0 ; 
       asteroid_chain-crater2a2.4-0 ; 
       asteroid_chain-crater3.4-0 ; 
       asteroid_chain-crater4.4-0 ; 
       asteroid_chain-craters.12-0 ; 
       asteroid_chain-rendermap.10-0 ; 
       asteroid_chain-rendermap1.2-0 ; 
       asteroid_chain-surface1.4-0 ; 
       asteroid_chain_add_light-qs.1-0 ; 
       asteroid_chain_add_light-t2d100.1-0 ; 
       asteroid_chain_add_light-t2d101.1-0 ; 
       asteroid_chain_add_light-t2d102.1-0 ; 
       asteroid_chain_add_light-t2d103.1-0 ; 
       asteroid_chain_add_light-t2d104.1-0 ; 
       asteroid_chain_add_light-t2d105.1-0 ; 
       asteroid_chain_add_light-t2d106.1-0 ; 
       asteroid_chain_add_light-t2d107.1-0 ; 
       asteroid_chain_add_light-t2d30.1-0 ; 
       asteroid_chain_add_light-t2d32.1-0 ; 
       asteroid_chain_add_light-t2d35.1-0 ; 
       asteroid_chain_add_light-t2d38.1-0 ; 
       asteroid_chain_add_light-t2d39.1-0 ; 
       asteroid_chain_add_light-t2d40.1-0 ; 
       asteroid_chain_add_light-t2d41.1-0 ; 
       asteroid_chain_add_light-t2d42.1-0 ; 
       asteroid_chain_add_light-t2d43.1-0 ; 
       asteroid_chain_add_light-t2d44.1-0 ; 
       asteroid_chain_add_light-t2d45.1-0 ; 
       asteroid_chain_add_light-t2d46.1-0 ; 
       asteroid_chain_add_light-t2d47.1-0 ; 
       asteroid_chain_add_light-t2d48.1-0 ; 
       asteroid_chain_add_light-t2d49.1-0 ; 
       asteroid_chain_add_light-t2d50.1-0 ; 
       asteroid_chain_add_light-t2d51.1-0 ; 
       asteroid_chain_add_light-t2d52.1-0 ; 
       asteroid_chain_add_light-t2d53.1-0 ; 
       asteroid_chain_add_light-t2d54.1-0 ; 
       asteroid_chain_add_light-t2d55.1-0 ; 
       asteroid_chain_add_light-t2d56.1-0 ; 
       asteroid_chain_add_light-t2d57.1-0 ; 
       asteroid_chain_add_light-t2d58.1-0 ; 
       asteroid_chain_add_light-t2d59.1-0 ; 
       asteroid_chain_add_light-t2d60.1-0 ; 
       asteroid_chain_add_light-t2d61.1-0 ; 
       asteroid_chain_add_light-t2d63.1-0 ; 
       asteroid_chain_add_light-t2d64.1-0 ; 
       asteroid_chain_add_light-t2d65.1-0 ; 
       asteroid_chain_add_light-t2d66.1-0 ; 
       asteroid_chain_add_light-t2d67.1-0 ; 
       asteroid_chain_add_light-t2d68.1-0 ; 
       asteroid_chain_add_light-t2d69.1-0 ; 
       asteroid_chain_add_light-t2d71.1-0 ; 
       asteroid_chain_add_light-t2d75.1-0 ; 
       asteroid_chain_add_light-t2d80.1-0 ; 
       asteroid_chain_add_light-t2d81.1-0 ; 
       asteroid_chain_add_light-t2d84.1-0 ; 
       asteroid_chain_add_light-t2d86.1-0 ; 
       asteroid_chain_add_light-t2d87.1-0 ; 
       asteroid_chain_add_light-t2d88.1-0 ; 
       asteroid_chain_add_light-t2d89.1-0 ; 
       asteroid_chain_add_light-t2d90.1-0 ; 
       asteroid_chain_add_light-t2d91.1-0 ; 
       asteroid_chain_add_light-t2d92.1-0 ; 
       asteroid_chain_add_light-t2d93.1-0 ; 
       asteroid_chain_add_light-t2d94.1-0 ; 
       asteroid_chain_add_light-t2d95.1-0 ; 
       asteroid_chain_add_light-t2d96.1-0 ; 
       asteroid_chain_add_light-t2d97.1-0 ; 
       asteroid_chain_add_light-t2d98.1-0 ; 
       asteroid_chain_add_light-t2d99.1-0 ; 
       asteroid_chain_add_light-_t2d62.1-0 ; 
       poly-rendermap1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       asteroid_chain-rock03.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 12 110 ; 
       6 5 110 ; 
       8 14 110 ; 
       9 15 110 ; 
       10 16 110 ; 
       11 17 110 ; 
       12 18 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       19 21 110 ; 
       20 12 110 ; 
       21 20 110 ; 
       7 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 3 300 ; 
       5 38 300 ; 
       5 39 300 ; 
       5 40 300 ; 
       5 41 300 ; 
       5 42 300 ; 
       5 43 300 ; 
       5 44 300 ; 
       5 45 300 ; 
       6 3 300 ; 
       6 46 300 ; 
       6 47 300 ; 
       6 48 300 ; 
       6 49 300 ; 
       12 3 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 16 300 ; 
       12 50 300 ; 
       12 51 300 ; 
       12 52 300 ; 
       12 53 300 ; 
       12 54 300 ; 
       12 55 300 ; 
       14 3 300 ; 
       14 59 300 ; 
       14 63 300 ; 
       14 5 300 ; 
       14 12 300 ; 
       14 13 300 ; 
       15 3 300 ; 
       15 56 300 ; 
       15 60 300 ; 
       15 64 300 ; 
       15 6 300 ; 
       15 7 300 ; 
       16 3 300 ; 
       16 57 300 ; 
       16 61 300 ; 
       16 65 300 ; 
       16 8 300 ; 
       16 9 300 ; 
       17 3 300 ; 
       17 58 300 ; 
       17 62 300 ; 
       17 4 300 ; 
       17 10 300 ; 
       17 11 300 ; 
       18 3 300 ; 
       20 3 300 ; 
       20 27 300 ; 
       20 34 300 ; 
       20 35 300 ; 
       20 36 300 ; 
       20 37 300 ; 
       21 3 300 ; 
       21 17 300 ; 
       21 18 300 ; 
       21 19 300 ; 
       21 20 300 ; 
       21 21 300 ; 
       21 22 300 ; 
       21 23 300 ; 
       21 24 300 ; 
       21 25 300 ; 
       21 26 300 ; 
       21 28 300 ; 
       21 29 300 ; 
       21 30 300 ; 
       21 31 300 ; 
       21 32 300 ; 
       21 33 300 ; 
       7 66 300 ; 
       0 0 300 ; 
       3 1 300 ; 
       4 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 6 400 ; 
       0 0 400 ; 
       0 9 400 ; 
       0 18 400 ; 
       3 4 400 ; 
       3 2 400 ; 
       3 5 400 ; 
       3 3 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       3 4 2110 ; 
       5 6 2110 ; 
       7 8 2110 ; 
       9 10 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 47 401 ; 
       4 68 401 ; 
       5 69 401 ; 
       6 70 401 ; 
       7 71 401 ; 
       8 11 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 15 401 ; 
       13 16 401 ; 
       14 21 401 ; 
       15 19 401 ; 
       16 20 401 ; 
       17 26 401 ; 
       18 28 401 ; 
       19 30 401 ; 
       20 22 401 ; 
       21 23 401 ; 
       22 24 401 ; 
       23 25 401 ; 
       24 27 401 ; 
       25 29 401 ; 
       26 31 401 ; 
       27 32 401 ; 
       28 33 401 ; 
       29 34 401 ; 
       30 35 401 ; 
       31 36 401 ; 
       32 37 401 ; 
       33 38 401 ; 
       34 39 401 ; 
       35 40 401 ; 
       36 41 401 ; 
       37 42 401 ; 
       38 43 401 ; 
       39 44 401 ; 
       40 45 401 ; 
       41 72 401 ; 
       42 46 401 ; 
       44 48 401 ; 
       45 49 401 ; 
       46 50 401 ; 
       47 51 401 ; 
       48 52 401 ; 
       49 10 401 ; 
       50 55 401 ; 
       51 56 401 ; 
       52 54 401 ; 
       53 17 401 ; 
       54 53 401 ; 
       55 57 401 ; 
       56 58 401 ; 
       57 59 401 ; 
       58 60 401 ; 
       59 61 401 ; 
       60 62 401 ; 
       61 63 401 ; 
       62 64 401 ; 
       63 65 401 ; 
       64 66 401 ; 
       65 67 401 ; 
       66 73 401 ; 
       0 7 401 ; 
       1 1 401 ; 
       2 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       3 SCHEM 37.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 40 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 45 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 110 -4 1 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 98.75 -6 1 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 35 -8 1 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 50 -8 1 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 65 -8 1 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 80 -8 1 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 118.75 -2 1 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 63.75 -4 1 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 41.25 -6 1 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 56.25 -6 1 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 71.25 -6 1 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 86.25 -6 1 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 35 0 1 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 127.5 -8 1 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 153.75 -4 1 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 147.5 -6 1 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 205 -2 1 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 20 0 0 DISPLAY 1 2 SRT 3.495959 3.495959 3.495959 0 0 0 0 -2.98516 0 MPRFLG 0 ; 
       3 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 3.508199 3.508199 3.508199 0 0 0 0 -2.98516 0 MPRFLG 0 ; 
       4 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 8.589459 0 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 SRT 1.706 1.706 1.706 0 0 0 0 4.972823 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 115 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 87.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 60 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 75 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 77.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 90 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 92.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 45 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 190 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 182.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 185 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 145 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 130 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 132.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 135 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 137.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 140 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 142.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 147.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 150 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 152.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 180 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 155 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 157.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 160 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 162.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 165 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 167.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 170 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 172.5 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 175 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 177.5 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 107.5 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 110 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 112.5 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 105 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 117.5 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 120 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 122.5 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 125 -6 1 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 95 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 97.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 100 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 102.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 187.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 192.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 195 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 197.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 200 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 202.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 52.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 67.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 82.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 37.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 55 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 70 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 85 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 40 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 57.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 72.5 -8 1 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 205 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 22.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       11 SCHEM 75 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 77.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 90 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 45 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 47.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 197.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 182.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 185 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 190 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 135 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 137.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 140 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 142.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 145 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 147.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 130 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 150 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 132.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 152.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 180 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 155 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 157.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 160 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 162.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 165 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 167.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 170 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 172.5 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 175 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 177.5 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 107.5 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 110 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 112.5 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       72 SCHEM 105 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 117.5 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 115 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 122.5 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 125 -8 1 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 95 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 97.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 100 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 102.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 200 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 195 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 187.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 192.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 202.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 52.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 67.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 82.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 37.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 55 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 70 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 85 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 40 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 57.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 72.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 87.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       69 SCHEM 42.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       70 SCHEM 60 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       71 SCHEM 62.5 -10 1 WIRECOL 10 7 MPRFLG 0 ; 
       73 SCHEM 205 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 17.49999 -2 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -3.999997 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 55 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 20 -2 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
