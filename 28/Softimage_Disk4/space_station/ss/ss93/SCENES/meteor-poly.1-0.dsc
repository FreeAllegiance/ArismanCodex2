SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.1-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       poly-inf_light1.1-0 ROOT ; 
       poly-inf_light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       poly-mat1.1-0 ; 
       poly-mat2.1-0 ; 
       poly-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       asteroid_chain3-craterer.4-0 ROOT ; 
       asteroid_chain4-craterer.3-0 ROOT ; 
       poly-craterer.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 8     
       E:/pete_data2/space_station/ss/ss93/PICTURES/VenusMap ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/bottom_of_craters ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/crater-rendermap ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/crater1 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/crater2 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/multi-craters ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/rendermap ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       meteor-poly.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       poly-bottom_of_craters1.1-0 ; 
       poly-crater-rendermap1.1-0 ; 
       poly-crater1a2.1-0 ; 
       poly-crater2a2.1-0 ; 
       poly-crater3.1-0 ; 
       poly-crater4.1-0 ; 
       poly-craters.1-0 ; 
       poly-rendermap.1-0 ; 
       poly-rendermap1.1-0 ; 
       poly-surface1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       poly-rock03.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       0 1 300 ; 
       1 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 6 400 ; 
       2 0 400 ; 
       2 9 400 ; 
       0 4 400 ; 
       0 2 400 ; 
       0 5 400 ; 
       0 3 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       2 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 1 401 ; 
       2 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
