SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       splines-cam_int1.2-0 ROOT ; 
       splines-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       splines-circle1.1-0 ; 
       splines-circle32.1-0 ; 
       splines-circle33.1-0 ; 
       splines-circle34.1-0 ; 
       splines-circle35.1-0 ; 
       splines-circle36.1-0 ; 
       splines-circle37.1-0 ; 
       splines-circle38.1-0 ; 
       splines-circle39.1-0 ; 
       splines-circle40.1-0 ; 
       splines-circle41.1-0 ; 
       splines-null4.1-0 ROOT ; 
       splines-nurbs1.1-0 ROOT ; 
       splines-nurbs2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       meteor-splines.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       6 11 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 10 -2 0 MPRFLG 0 ; 
       11 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 30 -2 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 5 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
