SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.23-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_garages-light1.2-0 ROOT ; 
       add_garages-light2.2-0 ROOT ; 
       add_garages-light3.2-0 ROOT ; 
       add_garages-light4.2-0 ROOT ; 
       add_garages-light5.2-0 ROOT ; 
       add_garages-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 65     
       add_garages-bays1.1-0 ; 
       add_garages-mat1.1-0 ; 
       add_garages-mat100.1-0 ; 
       add_garages-mat101.1-0 ; 
       add_garages-mat102.1-0 ; 
       add_garages-mat103.1-0 ; 
       add_garages-mat104.1-0 ; 
       add_garages-mat105.1-0 ; 
       add_garages-mat106.1-0 ; 
       add_garages-mat107.1-0 ; 
       add_garages-mat108.1-0 ; 
       add_garages-mat109.1-0 ; 
       add_garages-mat12.1-0 ; 
       add_garages-mat35.1-0 ; 
       add_garages-mat37.1-0 ; 
       add_garages-mat40.1-0 ; 
       add_garages-mat41.1-0 ; 
       add_garages-mat42.1-0 ; 
       add_garages-mat43.1-0 ; 
       add_garages-mat44.1-0 ; 
       add_garages-mat45.1-0 ; 
       add_garages-mat46.1-0 ; 
       add_garages-mat47.1-0 ; 
       add_garages-mat48.1-0 ; 
       add_garages-mat49.1-0 ; 
       add_garages-mat50.1-0 ; 
       add_garages-mat51.1-0 ; 
       add_garages-mat52.1-0 ; 
       add_garages-mat53.1-0 ; 
       add_garages-mat54.1-0 ; 
       add_garages-mat55.1-0 ; 
       add_garages-mat56.1-0 ; 
       add_garages-mat57.1-0 ; 
       add_garages-mat58.1-0 ; 
       add_garages-mat59.1-0 ; 
       add_garages-mat60.1-0 ; 
       add_garages-mat61.1-0 ; 
       add_garages-mat62.1-0 ; 
       add_garages-mat63.1-0 ; 
       add_garages-mat64.1-0 ; 
       add_garages-mat65.1-0 ; 
       add_garages-mat66.1-0 ; 
       add_garages-mat67.1-0 ; 
       add_garages-mat68.1-0 ; 
       add_garages-mat69.1-0 ; 
       add_garages-mat70.1-0 ; 
       add_garages-mat71.1-0 ; 
       add_garages-mat72.1-0 ; 
       add_garages-mat73.1-0 ; 
       add_garages-mat74.1-0 ; 
       add_garages-mat80.1-0 ; 
       add_garages-mat82.1-0 ; 
       add_garages-mat84.1-0 ; 
       add_garages-mat89.1-0 ; 
       add_garages-mat90.1-0 ; 
       add_garages-mat91.1-0 ; 
       add_garages-mat92.1-0 ; 
       add_garages-mat93.1-0 ; 
       add_garages-mat94.1-0 ; 
       add_garages-mat95.1-0 ; 
       add_garages-mat96.1-0 ; 
       add_garages-mat97.1-0 ; 
       add_garages-mat98.1-0 ; 
       add_garages-mat99.1-0 ; 
       poly-mat3.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       add_garages-cube2.3-0 ROOT ; 
       add_garages-cube3.2-0 ROOT ; 
       ss93-bfuselg1.1-0 ; 
       ss93-bfuselg2.1-0 ; 
       ss93-craterer.4-0 ; 
       ss93-doccon1.1-0 ; 
       ss93-doccon2.1-0 ; 
       ss93-doccon3.1-0 ; 
       ss93-doccon4.1-0 ; 
       ss93-fuselg.5-0 ; 
       ss93-mfuselg0.1-0 ; 
       ss93-mfuselg1.1-0 ; 
       ss93-mfuselg2.1-0 ; 
       ss93-mfuselg3.1-0 ; 
       ss93-mfuselg4.1-0 ; 
       ss93-ss93.15-0 ROOT ; 
       ss93-tdoccon.1-0 ; 
       ss93-tfuselg1.1-0 ; 
       ss93-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss19a-add_garages.15-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 64     
       add_garages-qs.3-0 ; 
       add_garages-t2d100.3-0 ; 
       add_garages-t2d101.3-0 ; 
       add_garages-t2d102.3-0 ; 
       add_garages-t2d103.3-0 ; 
       add_garages-t2d104.3-0 ; 
       add_garages-t2d105.3-0 ; 
       add_garages-t2d106.3-0 ; 
       add_garages-t2d107.4-0 ; 
       add_garages-t2d30.3-0 ; 
       add_garages-t2d32.3-0 ; 
       add_garages-t2d35.3-0 ; 
       add_garages-t2d38.3-0 ; 
       add_garages-t2d39.3-0 ; 
       add_garages-t2d40.3-0 ; 
       add_garages-t2d41.3-0 ; 
       add_garages-t2d42.3-0 ; 
       add_garages-t2d43.3-0 ; 
       add_garages-t2d44.3-0 ; 
       add_garages-t2d45.3-0 ; 
       add_garages-t2d46.3-0 ; 
       add_garages-t2d47.3-0 ; 
       add_garages-t2d48.3-0 ; 
       add_garages-t2d49.3-0 ; 
       add_garages-t2d50.3-0 ; 
       add_garages-t2d51.3-0 ; 
       add_garages-t2d52.3-0 ; 
       add_garages-t2d53.3-0 ; 
       add_garages-t2d54.3-0 ; 
       add_garages-t2d55.3-0 ; 
       add_garages-t2d56.3-0 ; 
       add_garages-t2d57.3-0 ; 
       add_garages-t2d58.3-0 ; 
       add_garages-t2d59.3-0 ; 
       add_garages-t2d60.3-0 ; 
       add_garages-t2d61.3-0 ; 
       add_garages-t2d63.3-0 ; 
       add_garages-t2d64.3-0 ; 
       add_garages-t2d65.3-0 ; 
       add_garages-t2d66.3-0 ; 
       add_garages-t2d67.3-0 ; 
       add_garages-t2d68.3-0 ; 
       add_garages-t2d69.3-0 ; 
       add_garages-t2d71.3-0 ; 
       add_garages-t2d75.3-0 ; 
       add_garages-t2d80.3-0 ; 
       add_garages-t2d81.3-0 ; 
       add_garages-t2d84.3-0 ; 
       add_garages-t2d86.3-0 ; 
       add_garages-t2d87.3-0 ; 
       add_garages-t2d88.3-0 ; 
       add_garages-t2d89.3-0 ; 
       add_garages-t2d90.3-0 ; 
       add_garages-t2d91.3-0 ; 
       add_garages-t2d92.3-0 ; 
       add_garages-t2d93.3-0 ; 
       add_garages-t2d94.3-0 ; 
       add_garages-t2d95.3-0 ; 
       add_garages-t2d96.3-0 ; 
       add_garages-t2d97.3-0 ; 
       add_garages-t2d98.3-0 ; 
       add_garages-t2d99.3-0 ; 
       add_garages-_t2d62.3-0 ; 
       poly-rendermap1.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 9 110 ; 
       3 2 110 ; 
       4 15 110 ; 
       5 11 110 ; 
       6 12 110 ; 
       7 13 110 ; 
       8 14 110 ; 
       9 15 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       16 18 110 ; 
       17 9 110 ; 
       18 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 1 300 ; 
       2 36 300 ; 
       2 37 300 ; 
       2 38 300 ; 
       2 39 300 ; 
       2 40 300 ; 
       2 41 300 ; 
       2 42 300 ; 
       2 43 300 ; 
       3 1 300 ; 
       3 44 300 ; 
       3 45 300 ; 
       3 46 300 ; 
       3 47 300 ; 
       4 64 300 ; 
       4 0 300 ; 
       9 1 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 14 300 ; 
       9 48 300 ; 
       9 49 300 ; 
       9 50 300 ; 
       9 51 300 ; 
       9 52 300 ; 
       9 53 300 ; 
       11 1 300 ; 
       11 57 300 ; 
       11 61 300 ; 
       11 3 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       12 1 300 ; 
       12 54 300 ; 
       12 58 300 ; 
       12 62 300 ; 
       12 4 300 ; 
       12 5 300 ; 
       13 1 300 ; 
       13 55 300 ; 
       13 59 300 ; 
       13 63 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       14 1 300 ; 
       14 56 300 ; 
       14 60 300 ; 
       14 2 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       15 1 300 ; 
       17 1 300 ; 
       17 25 300 ; 
       17 32 300 ; 
       17 33 300 ; 
       17 34 300 ; 
       17 35 300 ; 
       18 1 300 ; 
       18 15 300 ; 
       18 16 300 ; 
       18 17 300 ; 
       18 18 300 ; 
       18 19 300 ; 
       18 20 300 ; 
       18 21 300 ; 
       18 22 300 ; 
       18 23 300 ; 
       18 24 300 ; 
       18 26 300 ; 
       18 27 300 ; 
       18 28 300 ; 
       18 29 300 ; 
       18 30 300 ; 
       18 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       64 63 401 ; 
       1 37 401 ; 
       2 58 401 ; 
       3 59 401 ; 
       4 60 401 ; 
       5 61 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 11 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 16 401 ; 
       16 18 401 ; 
       17 20 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 17 401 ; 
       23 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 62 401 ; 
       40 36 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       46 42 401 ; 
       47 0 401 ; 
       48 45 401 ; 
       49 46 401 ; 
       50 44 401 ; 
       51 7 401 ; 
       52 43 401 ; 
       53 47 401 ; 
       54 48 401 ; 
       55 49 401 ; 
       56 50 401 ; 
       57 51 401 ; 
       58 52 401 ; 
       59 53 401 ; 
       60 54 401 ; 
       61 55 401 ; 
       62 56 401 ; 
       63 57 401 ; 
       0 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 186.2225 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 188.7225 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 191.2225 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 193.7225 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 196.2225 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 198.7225 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 173.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 86.25 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       10 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       13 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       15 SCHEM 88.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 95 -8 0 MPRFLG 0 ; 
       17 SCHEM 121.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 115 -6 0 MPRFLG 0 ; 
       0 SCHEM 183.7225 0 0 USR DISPLAY 0 0 SRT 0.4336026 0.2513102 0.2652653 0 0.4 0 6.154351 1.093738 9.19841 MPRFLG 0 ; 
       1 SCHEM 180 0 0 DISPLAY 0 0 SRT 0.4477726 0.259523 0.273934 0 0 0 -8.229176 0.578807 12.0434 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       64 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 157.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 150 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 152.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 155 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 160 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 162.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 165 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 167.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       63 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 150 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 152.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 157.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 147.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 130 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 135 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 140 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 142.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 167.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 162.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 155 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 160 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
