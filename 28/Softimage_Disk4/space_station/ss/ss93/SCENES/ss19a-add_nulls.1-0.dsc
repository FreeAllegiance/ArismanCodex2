SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.27-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_nulls-light1.1-0 ROOT ; 
       add_nulls-light2.1-0 ROOT ; 
       add_nulls-light3.1-0 ROOT ; 
       add_nulls-light4.1-0 ROOT ; 
       add_nulls-light5.1-0 ROOT ; 
       add_nulls-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 65     
       add_nulls-bays1.1-0 ; 
       add_nulls-mat1.1-0 ; 
       add_nulls-mat100.1-0 ; 
       add_nulls-mat101.1-0 ; 
       add_nulls-mat102.1-0 ; 
       add_nulls-mat103.1-0 ; 
       add_nulls-mat104.1-0 ; 
       add_nulls-mat105.1-0 ; 
       add_nulls-mat106.1-0 ; 
       add_nulls-mat107.1-0 ; 
       add_nulls-mat108.1-0 ; 
       add_nulls-mat109.1-0 ; 
       add_nulls-mat12.1-0 ; 
       add_nulls-mat35.1-0 ; 
       add_nulls-mat37.1-0 ; 
       add_nulls-mat40.1-0 ; 
       add_nulls-mat41.1-0 ; 
       add_nulls-mat42.1-0 ; 
       add_nulls-mat43.1-0 ; 
       add_nulls-mat44.1-0 ; 
       add_nulls-mat45.1-0 ; 
       add_nulls-mat46.1-0 ; 
       add_nulls-mat47.1-0 ; 
       add_nulls-mat48.1-0 ; 
       add_nulls-mat49.1-0 ; 
       add_nulls-mat50.1-0 ; 
       add_nulls-mat51.1-0 ; 
       add_nulls-mat52.1-0 ; 
       add_nulls-mat53.1-0 ; 
       add_nulls-mat54.1-0 ; 
       add_nulls-mat55.1-0 ; 
       add_nulls-mat56.1-0 ; 
       add_nulls-mat57.1-0 ; 
       add_nulls-mat58.1-0 ; 
       add_nulls-mat59.1-0 ; 
       add_nulls-mat60.1-0 ; 
       add_nulls-mat61.1-0 ; 
       add_nulls-mat62.1-0 ; 
       add_nulls-mat63.1-0 ; 
       add_nulls-mat64.1-0 ; 
       add_nulls-mat65.1-0 ; 
       add_nulls-mat66.1-0 ; 
       add_nulls-mat67.1-0 ; 
       add_nulls-mat68.1-0 ; 
       add_nulls-mat69.1-0 ; 
       add_nulls-mat70.1-0 ; 
       add_nulls-mat71.1-0 ; 
       add_nulls-mat72.1-0 ; 
       add_nulls-mat73.1-0 ; 
       add_nulls-mat74.1-0 ; 
       add_nulls-mat80.1-0 ; 
       add_nulls-mat82.1-0 ; 
       add_nulls-mat84.1-0 ; 
       add_nulls-mat89.1-0 ; 
       add_nulls-mat90.1-0 ; 
       add_nulls-mat91.1-0 ; 
       add_nulls-mat92.1-0 ; 
       add_nulls-mat93.1-0 ; 
       add_nulls-mat94.1-0 ; 
       add_nulls-mat95.1-0 ; 
       add_nulls-mat96.1-0 ; 
       add_nulls-mat97.1-0 ; 
       add_nulls-mat98.1-0 ; 
       add_nulls-mat99.1-0 ; 
       poly-mat3.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       add_nulls-bay1.1-0 ROOT ; 
       add_nulls-cube2.1-0 ROOT ; 
       add_nulls-cube3.1-0 ROOT ; 
       add_nulls-garage1A.1-0 ; 
       add_nulls-garage1B.1-0 ; 
       add_nulls-garage1C.1-0 ; 
       add_nulls-garage1D.1-0 ; 
       add_nulls-garage1E.1-0 ; 
       ss93-bfuselg1.1-0 ; 
       ss93-bfuselg2.1-0 ; 
       ss93-craterer.4-0 ; 
       ss93-doccon1.1-0 ; 
       ss93-doccon2.1-0 ; 
       ss93-doccon3.1-0 ; 
       ss93-doccon4.1-0 ; 
       ss93-fuselg.5-0 ; 
       ss93-mfuselg0.1-0 ; 
       ss93-mfuselg1.1-0 ; 
       ss93-mfuselg2.1-0 ; 
       ss93-mfuselg3.1-0 ; 
       ss93-mfuselg4.1-0 ; 
       ss93-ss93.15-0 ROOT ; 
       ss93-tdoccon.1-0 ; 
       ss93-tfuselg1.1-0 ; 
       ss93-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss19a-add_nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 64     
       add_nulls-qs.1-0 ; 
       add_nulls-t2d100.1-0 ; 
       add_nulls-t2d101.1-0 ; 
       add_nulls-t2d102.1-0 ; 
       add_nulls-t2d103.1-0 ; 
       add_nulls-t2d104.1-0 ; 
       add_nulls-t2d105.1-0 ; 
       add_nulls-t2d106.1-0 ; 
       add_nulls-t2d107.1-0 ; 
       add_nulls-t2d30.1-0 ; 
       add_nulls-t2d32.1-0 ; 
       add_nulls-t2d35.1-0 ; 
       add_nulls-t2d38.1-0 ; 
       add_nulls-t2d39.1-0 ; 
       add_nulls-t2d40.1-0 ; 
       add_nulls-t2d41.1-0 ; 
       add_nulls-t2d42.1-0 ; 
       add_nulls-t2d43.1-0 ; 
       add_nulls-t2d44.1-0 ; 
       add_nulls-t2d45.1-0 ; 
       add_nulls-t2d46.1-0 ; 
       add_nulls-t2d47.1-0 ; 
       add_nulls-t2d48.1-0 ; 
       add_nulls-t2d49.1-0 ; 
       add_nulls-t2d50.1-0 ; 
       add_nulls-t2d51.1-0 ; 
       add_nulls-t2d52.1-0 ; 
       add_nulls-t2d53.1-0 ; 
       add_nulls-t2d54.1-0 ; 
       add_nulls-t2d55.1-0 ; 
       add_nulls-t2d56.1-0 ; 
       add_nulls-t2d57.1-0 ; 
       add_nulls-t2d58.1-0 ; 
       add_nulls-t2d59.1-0 ; 
       add_nulls-t2d60.1-0 ; 
       add_nulls-t2d61.1-0 ; 
       add_nulls-t2d63.1-0 ; 
       add_nulls-t2d64.1-0 ; 
       add_nulls-t2d65.1-0 ; 
       add_nulls-t2d66.1-0 ; 
       add_nulls-t2d67.1-0 ; 
       add_nulls-t2d68.1-0 ; 
       add_nulls-t2d69.1-0 ; 
       add_nulls-t2d71.1-0 ; 
       add_nulls-t2d75.1-0 ; 
       add_nulls-t2d80.1-0 ; 
       add_nulls-t2d81.1-0 ; 
       add_nulls-t2d84.1-0 ; 
       add_nulls-t2d86.1-0 ; 
       add_nulls-t2d87.1-0 ; 
       add_nulls-t2d88.1-0 ; 
       add_nulls-t2d89.1-0 ; 
       add_nulls-t2d90.1-0 ; 
       add_nulls-t2d91.1-0 ; 
       add_nulls-t2d92.1-0 ; 
       add_nulls-t2d93.1-0 ; 
       add_nulls-t2d94.1-0 ; 
       add_nulls-t2d95.1-0 ; 
       add_nulls-t2d96.1-0 ; 
       add_nulls-t2d97.1-0 ; 
       add_nulls-t2d98.1-0 ; 
       add_nulls-t2d99.1-0 ; 
       add_nulls-_t2d62.1-0 ; 
       poly-rendermap1.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 15 110 ; 
       9 8 110 ; 
       10 21 110 ; 
       11 17 110 ; 
       12 18 110 ; 
       13 19 110 ; 
       14 20 110 ; 
       15 21 110 ; 
       16 15 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 16 110 ; 
       22 24 110 ; 
       23 15 110 ; 
       24 23 110 ; 
       7 0 110 ; 
       6 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       3 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 1 300 ; 
       8 36 300 ; 
       8 37 300 ; 
       8 38 300 ; 
       8 39 300 ; 
       8 40 300 ; 
       8 41 300 ; 
       8 42 300 ; 
       8 43 300 ; 
       9 1 300 ; 
       9 44 300 ; 
       9 45 300 ; 
       9 46 300 ; 
       9 47 300 ; 
       10 64 300 ; 
       10 0 300 ; 
       15 1 300 ; 
       15 12 300 ; 
       15 13 300 ; 
       15 14 300 ; 
       15 48 300 ; 
       15 49 300 ; 
       15 50 300 ; 
       15 51 300 ; 
       15 52 300 ; 
       15 53 300 ; 
       17 1 300 ; 
       17 57 300 ; 
       17 61 300 ; 
       17 3 300 ; 
       17 10 300 ; 
       17 11 300 ; 
       18 1 300 ; 
       18 54 300 ; 
       18 58 300 ; 
       18 62 300 ; 
       18 4 300 ; 
       18 5 300 ; 
       19 1 300 ; 
       19 55 300 ; 
       19 59 300 ; 
       19 63 300 ; 
       19 6 300 ; 
       19 7 300 ; 
       20 1 300 ; 
       20 56 300 ; 
       20 60 300 ; 
       20 2 300 ; 
       20 8 300 ; 
       20 9 300 ; 
       21 1 300 ; 
       23 1 300 ; 
       23 25 300 ; 
       23 32 300 ; 
       23 33 300 ; 
       23 34 300 ; 
       23 35 300 ; 
       24 1 300 ; 
       24 15 300 ; 
       24 16 300 ; 
       24 17 300 ; 
       24 18 300 ; 
       24 19 300 ; 
       24 20 300 ; 
       24 21 300 ; 
       24 22 300 ; 
       24 23 300 ; 
       24 24 300 ; 
       24 26 300 ; 
       24 27 300 ; 
       24 28 300 ; 
       24 29 300 ; 
       24 30 300 ; 
       24 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       64 63 401 ; 
       1 37 401 ; 
       2 58 401 ; 
       3 59 401 ; 
       4 60 401 ; 
       5 61 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 11 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 16 401 ; 
       16 18 401 ; 
       17 20 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 17 401 ; 
       23 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 62 401 ; 
       40 36 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       46 42 401 ; 
       47 0 401 ; 
       48 45 401 ; 
       49 46 401 ; 
       50 44 401 ; 
       51 7 401 ; 
       52 43 401 ; 
       53 47 401 ; 
       54 48 401 ; 
       55 49 401 ; 
       56 50 401 ; 
       57 51 401 ; 
       58 52 401 ; 
       59 53 401 ; 
       60 54 401 ; 
       61 55 401 ; 
       62 56 401 ; 
       63 57 401 ; 
       0 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 75 -8 0 MPRFLG 0 ; 
       9 SCHEM 63.75 -10 0 MPRFLG 0 ; 
       10 SCHEM 171.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 0 -12 0 MPRFLG 0 ; 
       12 SCHEM 15 -12 0 MPRFLG 0 ; 
       13 SCHEM 30 -12 0 MPRFLG 0 ; 
       14 SCHEM 45 -12 0 MPRFLG 0 ; 
       15 SCHEM 83.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       17 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       18 SCHEM 21.25 -10 0 MPRFLG 0 ; 
       19 SCHEM 36.25 -10 0 MPRFLG 0 ; 
       20 SCHEM 51.25 -10 0 MPRFLG 0 ; 
       21 SCHEM 86.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 92.5 -12 0 MPRFLG 0 ; 
       23 SCHEM 118.75 -8 0 MPRFLG 0 ; 
       24 SCHEM 112.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 10 -18 0 MPRFLG 0 ; 
       1 SCHEM 15 -16 0 DISPLAY 0 0 SRT 0.4336026 0.2513102 0.2652653 0 0.4 0 6.154351 1.093738 9.19841 MPRFLG 0 ; 
       2 SCHEM 12.5 -16 0 DISPLAY 0 0 SRT 0.4477726 0.259523 0.273934 0 0 0 -8.229176 0.578807 12.0434 MPRFLG 0 ; 
       6 SCHEM 7.5 -18 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -18 0 MPRFLG 0 ; 
       5 SCHEM 5 -18 0 MPRFLG 0 ; 
       0 SCHEM 5 -16 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 0 -18 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       64 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 110 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 102.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 107.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 112.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 115 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 120 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 125 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 130 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       63 SCHEM 170 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 67.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 40 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 162.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 155 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 100 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 105 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 107.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 110 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 112.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 115 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 97.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 117.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 145 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 120 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 122.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 125 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 127.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 130 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 132.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 135 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 137.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 142.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 165 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 160 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 157.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 167.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 172.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
