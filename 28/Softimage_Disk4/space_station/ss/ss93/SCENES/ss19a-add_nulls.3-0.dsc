SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.29-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_nulls-light1.3-0 ROOT ; 
       add_nulls-light2.3-0 ROOT ; 
       add_nulls-light3.3-0 ROOT ; 
       add_nulls-light4.3-0 ROOT ; 
       add_nulls-light5.3-0 ROOT ; 
       add_nulls-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 65     
       add_nulls-bays1.1-0 ; 
       add_nulls-mat1.1-0 ; 
       add_nulls-mat100.1-0 ; 
       add_nulls-mat101.1-0 ; 
       add_nulls-mat102.1-0 ; 
       add_nulls-mat103.1-0 ; 
       add_nulls-mat104.1-0 ; 
       add_nulls-mat105.1-0 ; 
       add_nulls-mat106.1-0 ; 
       add_nulls-mat107.1-0 ; 
       add_nulls-mat108.1-0 ; 
       add_nulls-mat109.1-0 ; 
       add_nulls-mat12.1-0 ; 
       add_nulls-mat35.1-0 ; 
       add_nulls-mat37.1-0 ; 
       add_nulls-mat40.1-0 ; 
       add_nulls-mat41.1-0 ; 
       add_nulls-mat42.1-0 ; 
       add_nulls-mat43.1-0 ; 
       add_nulls-mat44.1-0 ; 
       add_nulls-mat45.1-0 ; 
       add_nulls-mat46.1-0 ; 
       add_nulls-mat47.1-0 ; 
       add_nulls-mat48.1-0 ; 
       add_nulls-mat49.1-0 ; 
       add_nulls-mat50.1-0 ; 
       add_nulls-mat51.1-0 ; 
       add_nulls-mat52.1-0 ; 
       add_nulls-mat53.1-0 ; 
       add_nulls-mat54.1-0 ; 
       add_nulls-mat55.1-0 ; 
       add_nulls-mat56.1-0 ; 
       add_nulls-mat57.1-0 ; 
       add_nulls-mat58.1-0 ; 
       add_nulls-mat59.1-0 ; 
       add_nulls-mat60.1-0 ; 
       add_nulls-mat61.1-0 ; 
       add_nulls-mat62.1-0 ; 
       add_nulls-mat63.1-0 ; 
       add_nulls-mat64.1-0 ; 
       add_nulls-mat65.1-0 ; 
       add_nulls-mat66.1-0 ; 
       add_nulls-mat67.1-0 ; 
       add_nulls-mat68.1-0 ; 
       add_nulls-mat69.1-0 ; 
       add_nulls-mat70.1-0 ; 
       add_nulls-mat71.1-0 ; 
       add_nulls-mat72.1-0 ; 
       add_nulls-mat73.1-0 ; 
       add_nulls-mat74.1-0 ; 
       add_nulls-mat80.1-0 ; 
       add_nulls-mat82.1-0 ; 
       add_nulls-mat84.1-0 ; 
       add_nulls-mat89.1-0 ; 
       add_nulls-mat90.1-0 ; 
       add_nulls-mat91.1-0 ; 
       add_nulls-mat92.1-0 ; 
       add_nulls-mat93.1-0 ; 
       add_nulls-mat94.1-0 ; 
       add_nulls-mat95.1-0 ; 
       add_nulls-mat96.1-0 ; 
       add_nulls-mat97.1-0 ; 
       add_nulls-mat98.1-0 ; 
       add_nulls-mat99.1-0 ; 
       poly-mat3.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       ss93-bay1.1-0 ; 
       ss93-bay1_1.1-0 ; 
       ss93-bfuselg1.1-0 ; 
       ss93-bfuselg2.1-0 ; 
       ss93-craterer.4-0 ; 
       ss93-doccon1.1-0 ; 
       ss93-doccon2.1-0 ; 
       ss93-doccon3.1-0 ; 
       ss93-doccon4.1-0 ; 
       ss93-fuselg.5-0 ; 
       ss93-garage1A.1-0 ; 
       ss93-garage1B.1-0 ; 
       ss93-garage1C.1-0 ; 
       ss93-garage1D.1-0 ; 
       ss93-garage1E.1-0 ; 
       ss93-garage2A.1-0 ; 
       ss93-garage2B.1-0 ; 
       ss93-garage2C.1-0 ; 
       ss93-garage2D.1-0 ; 
       ss93-garage2E.1-0 ; 
       ss93-mfuselg0.1-0 ; 
       ss93-mfuselg1.1-0 ; 
       ss93-mfuselg2.1-0 ; 
       ss93-mfuselg3.1-0 ; 
       ss93-mfuselg4.1-0 ; 
       ss93-ss93.16-0 ROOT ; 
       ss93-tdoccon.1-0 ; 
       ss93-tfuselg1.1-0 ; 
       ss93-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss19a-add_nulls.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 64     
       add_nulls-qs.1-0 ; 
       add_nulls-t2d100.1-0 ; 
       add_nulls-t2d101.1-0 ; 
       add_nulls-t2d102.1-0 ; 
       add_nulls-t2d103.1-0 ; 
       add_nulls-t2d104.1-0 ; 
       add_nulls-t2d105.1-0 ; 
       add_nulls-t2d106.1-0 ; 
       add_nulls-t2d107.1-0 ; 
       add_nulls-t2d30.1-0 ; 
       add_nulls-t2d32.1-0 ; 
       add_nulls-t2d35.1-0 ; 
       add_nulls-t2d38.1-0 ; 
       add_nulls-t2d39.1-0 ; 
       add_nulls-t2d40.1-0 ; 
       add_nulls-t2d41.1-0 ; 
       add_nulls-t2d42.1-0 ; 
       add_nulls-t2d43.1-0 ; 
       add_nulls-t2d44.1-0 ; 
       add_nulls-t2d45.1-0 ; 
       add_nulls-t2d46.1-0 ; 
       add_nulls-t2d47.1-0 ; 
       add_nulls-t2d48.1-0 ; 
       add_nulls-t2d49.1-0 ; 
       add_nulls-t2d50.1-0 ; 
       add_nulls-t2d51.1-0 ; 
       add_nulls-t2d52.1-0 ; 
       add_nulls-t2d53.1-0 ; 
       add_nulls-t2d54.1-0 ; 
       add_nulls-t2d55.1-0 ; 
       add_nulls-t2d56.1-0 ; 
       add_nulls-t2d57.1-0 ; 
       add_nulls-t2d58.1-0 ; 
       add_nulls-t2d59.1-0 ; 
       add_nulls-t2d60.1-0 ; 
       add_nulls-t2d61.1-0 ; 
       add_nulls-t2d63.1-0 ; 
       add_nulls-t2d64.1-0 ; 
       add_nulls-t2d65.1-0 ; 
       add_nulls-t2d66.1-0 ; 
       add_nulls-t2d67.1-0 ; 
       add_nulls-t2d68.1-0 ; 
       add_nulls-t2d69.1-0 ; 
       add_nulls-t2d71.1-0 ; 
       add_nulls-t2d75.1-0 ; 
       add_nulls-t2d80.1-0 ; 
       add_nulls-t2d81.1-0 ; 
       add_nulls-t2d84.1-0 ; 
       add_nulls-t2d86.1-0 ; 
       add_nulls-t2d87.1-0 ; 
       add_nulls-t2d88.1-0 ; 
       add_nulls-t2d89.1-0 ; 
       add_nulls-t2d90.1-0 ; 
       add_nulls-t2d91.1-0 ; 
       add_nulls-t2d92.1-0 ; 
       add_nulls-t2d93.1-0 ; 
       add_nulls-t2d94.1-0 ; 
       add_nulls-t2d95.1-0 ; 
       add_nulls-t2d96.1-0 ; 
       add_nulls-t2d97.1-0 ; 
       add_nulls-t2d98.1-0 ; 
       add_nulls-t2d99.1-0 ; 
       add_nulls-_t2d62.1-0 ; 
       poly-rendermap1.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 9 110 ; 
       3 2 110 ; 
       4 25 110 ; 
       5 21 110 ; 
       6 22 110 ; 
       7 23 110 ; 
       8 24 110 ; 
       9 25 110 ; 
       20 9 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       26 28 110 ; 
       27 9 110 ; 
       28 27 110 ; 
       14 0 110 ; 
       13 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       0 25 110 ; 
       10 0 110 ; 
       1 25 110 ; 
       19 1 110 ; 
       18 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       15 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 1 300 ; 
       2 36 300 ; 
       2 37 300 ; 
       2 38 300 ; 
       2 39 300 ; 
       2 40 300 ; 
       2 41 300 ; 
       2 42 300 ; 
       2 43 300 ; 
       3 1 300 ; 
       3 44 300 ; 
       3 45 300 ; 
       3 46 300 ; 
       3 47 300 ; 
       4 64 300 ; 
       4 0 300 ; 
       9 1 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 14 300 ; 
       9 48 300 ; 
       9 49 300 ; 
       9 50 300 ; 
       9 51 300 ; 
       9 52 300 ; 
       9 53 300 ; 
       21 1 300 ; 
       21 57 300 ; 
       21 61 300 ; 
       21 3 300 ; 
       21 10 300 ; 
       21 11 300 ; 
       22 1 300 ; 
       22 54 300 ; 
       22 58 300 ; 
       22 62 300 ; 
       22 4 300 ; 
       22 5 300 ; 
       23 1 300 ; 
       23 55 300 ; 
       23 59 300 ; 
       23 63 300 ; 
       23 6 300 ; 
       23 7 300 ; 
       24 1 300 ; 
       24 56 300 ; 
       24 60 300 ; 
       24 2 300 ; 
       24 8 300 ; 
       24 9 300 ; 
       25 1 300 ; 
       27 1 300 ; 
       27 25 300 ; 
       27 32 300 ; 
       27 33 300 ; 
       27 34 300 ; 
       27 35 300 ; 
       28 1 300 ; 
       28 15 300 ; 
       28 16 300 ; 
       28 17 300 ; 
       28 18 300 ; 
       28 19 300 ; 
       28 20 300 ; 
       28 21 300 ; 
       28 22 300 ; 
       28 23 300 ; 
       28 24 300 ; 
       28 26 300 ; 
       28 27 300 ; 
       28 28 300 ; 
       28 29 300 ; 
       28 30 300 ; 
       28 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       64 63 401 ; 
       1 37 401 ; 
       2 58 401 ; 
       3 59 401 ; 
       4 60 401 ; 
       5 61 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 11 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 16 401 ; 
       16 18 401 ; 
       17 20 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 17 401 ; 
       23 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 62 401 ; 
       40 36 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       46 42 401 ; 
       47 0 401 ; 
       48 45 401 ; 
       49 46 401 ; 
       50 44 401 ; 
       51 7 401 ; 
       52 43 401 ; 
       53 47 401 ; 
       54 48 401 ; 
       55 49 401 ; 
       56 50 401 ; 
       57 51 401 ; 
       58 52 401 ; 
       59 53 401 ; 
       60 54 401 ; 
       61 55 401 ; 
       62 56 401 ; 
       63 57 401 ; 
       0 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 202.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 205 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 207.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 210 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 212.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 215 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 173.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       20 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       22 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       23 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       24 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       25 SCHEM 101.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 95 -8 0 MPRFLG 0 ; 
       27 SCHEM 121.25 -4 0 MPRFLG 0 ; 
       28 SCHEM 115 -6 0 MPRFLG 0 ; 
       14 SCHEM 187.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 185 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 180 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 182.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 182.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 177.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 195 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 200 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 197.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 192.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 195 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 190 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       64 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 157.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 150 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 152.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 155 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 160 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 162.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 165 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 167.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       63 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 150 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 152.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 157.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 147.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 130 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 135 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 140 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 142.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 167.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 162.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 155 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 160 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
