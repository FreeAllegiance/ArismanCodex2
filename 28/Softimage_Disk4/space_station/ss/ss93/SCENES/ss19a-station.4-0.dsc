SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.7-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       station-spot1.1-0 ; 
       station-spot1_int1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 64     
       poly-mat3.2-0 ; 
       station-mat1.1-0 ; 
       station-mat100.1-0 ; 
       station-mat101.1-0 ; 
       station-mat102.1-0 ; 
       station-mat103.1-0 ; 
       station-mat104.1-0 ; 
       station-mat105.1-0 ; 
       station-mat106.1-0 ; 
       station-mat107.1-0 ; 
       station-mat108.1-0 ; 
       station-mat109.1-0 ; 
       station-mat12.1-0 ; 
       station-mat35.1-0 ; 
       station-mat37.1-0 ; 
       station-mat40.1-0 ; 
       station-mat41.1-0 ; 
       station-mat42.1-0 ; 
       station-mat43.1-0 ; 
       station-mat44.1-0 ; 
       station-mat45.1-0 ; 
       station-mat46.1-0 ; 
       station-mat47.1-0 ; 
       station-mat48.1-0 ; 
       station-mat49.1-0 ; 
       station-mat50.1-0 ; 
       station-mat51.1-0 ; 
       station-mat52.1-0 ; 
       station-mat53.1-0 ; 
       station-mat54.1-0 ; 
       station-mat55.1-0 ; 
       station-mat56.1-0 ; 
       station-mat57.1-0 ; 
       station-mat58.1-0 ; 
       station-mat59.1-0 ; 
       station-mat60.1-0 ; 
       station-mat61.1-0 ; 
       station-mat62.1-0 ; 
       station-mat63.1-0 ; 
       station-mat64.1-0 ; 
       station-mat65.1-0 ; 
       station-mat66.1-0 ; 
       station-mat67.1-0 ; 
       station-mat68.1-0 ; 
       station-mat69.1-0 ; 
       station-mat70.1-0 ; 
       station-mat71.1-0 ; 
       station-mat72.1-0 ; 
       station-mat73.1-0 ; 
       station-mat74.1-0 ; 
       station-mat80.1-0 ; 
       station-mat82.1-0 ; 
       station-mat84.1-0 ; 
       station-mat89.1-0 ; 
       station-mat90.1-0 ; 
       station-mat91.1-0 ; 
       station-mat92.1-0 ; 
       station-mat93.1-0 ; 
       station-mat94.1-0 ; 
       station-mat95.1-0 ; 
       station-mat96.1-0 ; 
       station-mat97.1-0 ; 
       station-mat98.1-0 ; 
       station-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       ss93-bfuselg1.1-0 ; 
       ss93-bfuselg2.1-0 ; 
       ss93-craterer.4-0 ; 
       ss93-doccon1.1-0 ; 
       ss93-doccon2.1-0 ; 
       ss93-doccon3.1-0 ; 
       ss93-doccon4.1-0 ; 
       ss93-fuselg.5-0 ; 
       ss93-mfuselg0.1-0 ; 
       ss93-mfuselg1.1-0 ; 
       ss93-mfuselg2.1-0 ; 
       ss93-mfuselg3.1-0 ; 
       ss93-mfuselg4.1-0 ; 
       ss93-ss93.5-0 ROOT ; 
       ss93-tdoccon.1-0 ; 
       ss93-tfuselg1.1-0 ; 
       ss93-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93 ; 
       E:/pete_data2/space_station/ss/ss93/PICTURES/ss93a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss19a-station.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 63     
       poly-rendermap1.4-0 ; 
       station-qs.1-0 ; 
       station-t2d100.1-0 ; 
       station-t2d101.1-0 ; 
       station-t2d102.1-0 ; 
       station-t2d103.1-0 ; 
       station-t2d104.1-0 ; 
       station-t2d105.1-0 ; 
       station-t2d106.1-0 ; 
       station-t2d30.1-0 ; 
       station-t2d32.1-0 ; 
       station-t2d35.1-0 ; 
       station-t2d38.1-0 ; 
       station-t2d39.1-0 ; 
       station-t2d40.1-0 ; 
       station-t2d41.1-0 ; 
       station-t2d42.1-0 ; 
       station-t2d43.1-0 ; 
       station-t2d44.1-0 ; 
       station-t2d45.1-0 ; 
       station-t2d46.1-0 ; 
       station-t2d47.1-0 ; 
       station-t2d48.1-0 ; 
       station-t2d49.1-0 ; 
       station-t2d50.1-0 ; 
       station-t2d51.1-0 ; 
       station-t2d52.1-0 ; 
       station-t2d53.1-0 ; 
       station-t2d54.1-0 ; 
       station-t2d55.1-0 ; 
       station-t2d56.1-0 ; 
       station-t2d57.1-0 ; 
       station-t2d58.1-0 ; 
       station-t2d59.1-0 ; 
       station-t2d60.1-0 ; 
       station-t2d61.1-0 ; 
       station-t2d63.1-0 ; 
       station-t2d64.1-0 ; 
       station-t2d65.1-0 ; 
       station-t2d66.1-0 ; 
       station-t2d67.1-0 ; 
       station-t2d68.1-0 ; 
       station-t2d69.1-0 ; 
       station-t2d71.1-0 ; 
       station-t2d75.1-0 ; 
       station-t2d80.1-0 ; 
       station-t2d81.1-0 ; 
       station-t2d84.1-0 ; 
       station-t2d86.1-0 ; 
       station-t2d87.1-0 ; 
       station-t2d88.1-0 ; 
       station-t2d89.1-0 ; 
       station-t2d90.1-0 ; 
       station-t2d91.1-0 ; 
       station-t2d92.1-0 ; 
       station-t2d93.1-0 ; 
       station-t2d94.1-0 ; 
       station-t2d95.1-0 ; 
       station-t2d96.1-0 ; 
       station-t2d97.1-0 ; 
       station-t2d98.1-0 ; 
       station-t2d99.1-0 ; 
       station-_t2d62.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 0 110 ; 
       2 13 110 ; 
       3 9 110 ; 
       4 10 110 ; 
       5 11 110 ; 
       6 12 110 ; 
       7 13 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       14 16 110 ; 
       15 7 110 ; 
       16 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       0 38 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       0 41 300 ; 
       0 42 300 ; 
       0 43 300 ; 
       1 1 300 ; 
       1 44 300 ; 
       1 45 300 ; 
       1 46 300 ; 
       1 47 300 ; 
       2 0 300 ; 
       7 1 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       7 48 300 ; 
       7 49 300 ; 
       7 50 300 ; 
       7 51 300 ; 
       7 52 300 ; 
       7 53 300 ; 
       9 1 300 ; 
       9 57 300 ; 
       9 61 300 ; 
       9 3 300 ; 
       9 10 300 ; 
       9 11 300 ; 
       10 1 300 ; 
       10 54 300 ; 
       10 58 300 ; 
       10 62 300 ; 
       10 4 300 ; 
       10 5 300 ; 
       11 1 300 ; 
       11 55 300 ; 
       11 59 300 ; 
       11 63 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       12 1 300 ; 
       12 56 300 ; 
       12 60 300 ; 
       12 2 300 ; 
       12 8 300 ; 
       12 9 300 ; 
       13 1 300 ; 
       15 1 300 ; 
       15 25 300 ; 
       15 32 300 ; 
       15 33 300 ; 
       15 34 300 ; 
       15 35 300 ; 
       16 1 300 ; 
       16 15 300 ; 
       16 16 300 ; 
       16 17 300 ; 
       16 18 300 ; 
       16 19 300 ; 
       16 20 300 ; 
       16 21 300 ; 
       16 22 300 ; 
       16 23 300 ; 
       16 24 300 ; 
       16 26 300 ; 
       16 27 300 ; 
       16 28 300 ; 
       16 29 300 ; 
       16 30 300 ; 
       16 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 37 401 ; 
       2 58 401 ; 
       3 59 401 ; 
       4 60 401 ; 
       5 61 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 11 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 16 401 ; 
       16 18 401 ; 
       17 20 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 17 401 ; 
       23 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 62 401 ; 
       40 36 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       46 42 401 ; 
       47 1 401 ; 
       48 45 401 ; 
       49 46 401 ; 
       50 44 401 ; 
       51 8 401 ; 
       52 43 401 ; 
       53 47 401 ; 
       54 48 401 ; 
       55 49 401 ; 
       56 50 401 ; 
       57 51 401 ; 
       58 52 401 ; 
       59 53 401 ; 
       60 54 401 ; 
       61 55 401 ; 
       62 56 401 ; 
       63 57 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 75 -8 0 MPRFLG 0 ; 
       1 SCHEM 63.75 -10 0 MPRFLG 0 ; 
       2 SCHEM 170 -6 0 MPRFLG 0 ; 
       3 SCHEM 0 -12 0 MPRFLG 0 ; 
       4 SCHEM 15 -12 0 MPRFLG 0 ; 
       5 SCHEM 30 -12 0 MPRFLG 0 ; 
       6 SCHEM 45 -12 0 MPRFLG 0 ; 
       7 SCHEM 83.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       10 SCHEM 21.25 -10 0 MPRFLG 0 ; 
       11 SCHEM 36.25 -10 0 MPRFLG 0 ; 
       12 SCHEM 51.25 -10 0 MPRFLG 0 ; 
       13 SCHEM 85 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 92.5 -12 0 MPRFLG 0 ; 
       15 SCHEM 118.75 -8 0 MPRFLG 0 ; 
       16 SCHEM 112.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 110 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 102.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 107.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 112.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 115 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 120 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 125 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 130 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 170 -10 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 67.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 40 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 162.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 155 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 100 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 105 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 107.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 110 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 112.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 115 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 97.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 117.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 145 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 120 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 122.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 125 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 127.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 130 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 132.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 135 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 137.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 142.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 165 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 160 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 157.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 167.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
