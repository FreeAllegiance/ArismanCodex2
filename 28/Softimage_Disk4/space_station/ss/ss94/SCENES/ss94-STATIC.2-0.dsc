SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.23-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.23-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.23-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.22-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       dowager_sPtL-inf_light5_1_2.22-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.22-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       STATIC-mat28.2-0 ; 
       STATIC-mat29.2-0 ; 
       STATIC-mat3.2-0 ; 
       STATIC-mat30.2-0 ; 
       STATIC-mat31.2-0 ; 
       STATIC-mat32.2-0 ; 
       STATIC-mat33.2-0 ; 
       STATIC-mat34.2-0 ; 
       STATIC-mat35.2-0 ; 
       STATIC-mat36.2-0 ; 
       STATIC-mat41.2-0 ; 
       STATIC-mat42.2-0 ; 
       STATIC-mat43.2-0 ; 
       STATIC-mat44.2-0 ; 
       STATIC-mat45.2-0 ; 
       STATIC-mat46.2-0 ; 
       STATIC-mat47.2-0 ; 
       STATIC-mat48.2-0 ; 
       STATIC-mat49.2-0 ; 
       STATIC-mat5.2-0 ; 
       STATIC-mat50.2-0 ; 
       STATIC-mat51.2-0 ; 
       STATIC-mat52.2-0 ; 
       STATIC-mat53.2-0 ; 
       STATIC-mat54.2-0 ; 
       STATIC-mat55.2-0 ; 
       STATIC-mat56.2-0 ; 
       STATIC-mat57.2-0 ; 
       STATIC-mat58.2-0 ; 
       STATIC-mat59.2-0 ; 
       STATIC-mat6.2-0 ; 
       STATIC-mat60.2-0 ; 
       STATIC-mat61.2-0 ; 
       STATIC-mat62.2-0 ; 
       STATIC-mat7.2-0 ; 
       STATIC-mat8.2-0 ; 
       STATIC-mat9.2-0 ; 
       STATIC-shaft.2-0 ; 
       STATIC-shaft1.2-0 ; 
       STATIC-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       STATIC-corrdr2_3.1-0 ; 
       STATIC-corrdr2_4.1-0 ; 
       STATIC-corrdr2_5.1-0 ; 
       STATIC-corrdr2_6.1-0 ; 
       STATIC-cube4.1-0 ; 
       STATIC-cube5_1.2-0 ; 
       STATIC-cube5_4.1-0 ; 
       STATIC-cube5_6.1-0 ; 
       STATIC-cube5_7.1-0 ; 
       STATIC-cube6.1-0 ; 
       STATIC-cube8.1-0 ; 
       STATIC-cube9.1-0 ; 
       STATIC-fuselg6.2-0 ROOT ; 
       STATIC-ss01.1-0 ; 
       STATIC-ss2.1-0 ; 
       STATIC-ss3.1-0 ; 
       STATIC-ss4.1-0 ; 
       STATIC-ss5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss94/PICTURES/ss94 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss94-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       STATIC-t2d24.2-0 ; 
       STATIC-t2d25.2-0 ; 
       STATIC-t2d26.2-0 ; 
       STATIC-t2d27.2-0 ; 
       STATIC-t2d28.2-0 ; 
       STATIC-t2d29.2-0 ; 
       STATIC-t2d30.2-0 ; 
       STATIC-t2d31.2-0 ; 
       STATIC-t2d32.2-0 ; 
       STATIC-t2d33.2-0 ; 
       STATIC-t2d34.2-0 ; 
       STATIC-t2d39.2-0 ; 
       STATIC-t2d4.2-0 ; 
       STATIC-t2d40.2-0 ; 
       STATIC-t2d41.2-0 ; 
       STATIC-t2d42.2-0 ; 
       STATIC-t2d43.2-0 ; 
       STATIC-t2d44.2-0 ; 
       STATIC-t2d45.2-0 ; 
       STATIC-t2d46.2-0 ; 
       STATIC-t2d47.2-0 ; 
       STATIC-t2d48.2-0 ; 
       STATIC-t2d49.2-0 ; 
       STATIC-t2d5.2-0 ; 
       STATIC-t2d50.2-0 ; 
       STATIC-t2d51.2-0 ; 
       STATIC-t2d52.2-0 ; 
       STATIC-t2d6.2-0 ; 
       STATIC-t2d7.2-0 ; 
       STATIC-t2d8.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       14 12 110 ; 
       0 12 110 ; 
       1 12 110 ; 
       15 12 110 ; 
       2 12 110 ; 
       3 12 110 ; 
       4 5 110 ; 
       5 12 110 ; 
       6 12 110 ; 
       7 12 110 ; 
       8 12 110 ; 
       9 6 110 ; 
       10 7 110 ; 
       11 8 110 ; 
       13 12 110 ; 
       16 12 110 ; 
       17 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       14 29 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       0 36 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       15 31 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       4 0 300 ; 
       4 1 300 ; 
       4 3 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       7 10 300 ; 
       8 24 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       9 9 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       11 25 300 ; 
       11 26 300 ; 
       11 27 300 ; 
       12 2 300 ; 
       12 37 300 ; 
       12 19 300 ; 
       12 30 300 ; 
       12 39 300 ; 
       12 38 300 ; 
       12 4 300 ; 
       13 28 300 ; 
       16 32 300 ; 
       17 33 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       3 2 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 13 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       18 18 401 ; 
       19 23 401 ; 
       20 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       30 27 401 ; 
       35 28 401 ; 
       36 29 401 ; 
       37 12 401 ; 
       38 4 401 ; 
       39 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       14 SCHEM 10 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 2.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 17.5 0 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 30 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 32.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
