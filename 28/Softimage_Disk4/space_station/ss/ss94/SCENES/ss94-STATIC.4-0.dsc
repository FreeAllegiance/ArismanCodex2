SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.25-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.24-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       dowager_sPtL-inf_light5_1_2.24-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.24-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       STATIC-mat28.2-0 ; 
       STATIC-mat29.2-0 ; 
       STATIC-mat3.2-0 ; 
       STATIC-mat30.2-0 ; 
       STATIC-mat31.2-0 ; 
       STATIC-mat32.2-0 ; 
       STATIC-mat33.2-0 ; 
       STATIC-mat34.2-0 ; 
       STATIC-mat35.2-0 ; 
       STATIC-mat36.2-0 ; 
       STATIC-mat41.2-0 ; 
       STATIC-mat42.2-0 ; 
       STATIC-mat43.2-0 ; 
       STATIC-mat44.2-0 ; 
       STATIC-mat45.2-0 ; 
       STATIC-mat46.2-0 ; 
       STATIC-mat47.2-0 ; 
       STATIC-mat48.2-0 ; 
       STATIC-mat49.2-0 ; 
       STATIC-mat5.2-0 ; 
       STATIC-mat50.2-0 ; 
       STATIC-mat51.2-0 ; 
       STATIC-mat52.2-0 ; 
       STATIC-mat53.2-0 ; 
       STATIC-mat54.2-0 ; 
       STATIC-mat55.2-0 ; 
       STATIC-mat56.2-0 ; 
       STATIC-mat57.2-0 ; 
       STATIC-mat6.2-0 ; 
       STATIC-mat7.2-0 ; 
       STATIC-mat8.2-0 ; 
       STATIC-mat9.2-0 ; 
       STATIC-shaft.2-0 ; 
       STATIC-shaft1.2-0 ; 
       STATIC-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       STATIC-corrdr2_3.1-0 ; 
       STATIC-corrdr2_4.1-0 ; 
       STATIC-corrdr2_5.1-0 ; 
       STATIC-corrdr2_6.1-0 ; 
       STATIC-cube4.1-0 ; 
       STATIC-cube5_1.2-0 ; 
       STATIC-cube5_4.1-0 ; 
       STATIC-cube5_6.1-0 ; 
       STATIC-cube5_7.1-0 ; 
       STATIC-cube6.1-0 ; 
       STATIC-cube8.1-0 ; 
       STATIC-cube9.1-0 ; 
       STATIC-fuselg6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss94/PICTURES/ss94 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss94-STATIC.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       STATIC-t2d24.2-0 ; 
       STATIC-t2d25.2-0 ; 
       STATIC-t2d26.2-0 ; 
       STATIC-t2d27.2-0 ; 
       STATIC-t2d28.2-0 ; 
       STATIC-t2d29.2-0 ; 
       STATIC-t2d30.2-0 ; 
       STATIC-t2d31.2-0 ; 
       STATIC-t2d32.2-0 ; 
       STATIC-t2d33.2-0 ; 
       STATIC-t2d34.2-0 ; 
       STATIC-t2d39.2-0 ; 
       STATIC-t2d4.2-0 ; 
       STATIC-t2d40.2-0 ; 
       STATIC-t2d41.2-0 ; 
       STATIC-t2d42.2-0 ; 
       STATIC-t2d43.2-0 ; 
       STATIC-t2d44.2-0 ; 
       STATIC-t2d45.2-0 ; 
       STATIC-t2d46.2-0 ; 
       STATIC-t2d47.2-0 ; 
       STATIC-t2d48.2-0 ; 
       STATIC-t2d49.2-0 ; 
       STATIC-t2d5.2-0 ; 
       STATIC-t2d50.2-0 ; 
       STATIC-t2d51.2-0 ; 
       STATIC-t2d52.2-0 ; 
       STATIC-t2d6.2-0 ; 
       STATIC-t2d7.2-0 ; 
       STATIC-t2d8.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 12 110 ; 
       2 12 110 ; 
       3 12 110 ; 
       4 5 110 ; 
       5 12 110 ; 
       6 12 110 ; 
       7 12 110 ; 
       8 12 110 ; 
       9 6 110 ; 
       10 7 110 ; 
       11 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 29 300 ; 
       0 30 300 ; 
       0 31 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       4 0 300 ; 
       4 1 300 ; 
       4 3 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       7 10 300 ; 
       8 24 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       9 9 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       11 25 300 ; 
       11 26 300 ; 
       11 27 300 ; 
       12 2 300 ; 
       12 32 300 ; 
       12 19 300 ; 
       12 28 300 ; 
       12 34 300 ; 
       12 33 300 ; 
       12 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       3 2 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 13 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       18 18 401 ; 
       19 23 401 ; 
       20 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 12 401 ; 
       33 4 401 ; 
       34 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 MPRFLG 0 ; 
       12 SCHEM 11.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
