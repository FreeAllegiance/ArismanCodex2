SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.26-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.26-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.26-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.25-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       dowager_sPtL-inf_light5_1_2.25-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       bound-mat28.1-0 ; 
       bound-mat29.1-0 ; 
       bound-mat3.1-0 ; 
       bound-mat30.1-0 ; 
       bound-mat31.1-0 ; 
       bound-mat32.1-0 ; 
       bound-mat33.1-0 ; 
       bound-mat34.1-0 ; 
       bound-mat35.1-0 ; 
       bound-mat36.1-0 ; 
       bound-mat41.1-0 ; 
       bound-mat42.1-0 ; 
       bound-mat43.1-0 ; 
       bound-mat44.1-0 ; 
       bound-mat45.1-0 ; 
       bound-mat46.1-0 ; 
       bound-mat47.1-0 ; 
       bound-mat48.1-0 ; 
       bound-mat49.1-0 ; 
       bound-mat5.1-0 ; 
       bound-mat50.1-0 ; 
       bound-mat51.1-0 ; 
       bound-mat52.1-0 ; 
       bound-mat53.1-0 ; 
       bound-mat54.1-0 ; 
       bound-mat55.1-0 ; 
       bound-mat56.1-0 ; 
       bound-mat57.1-0 ; 
       bound-mat6.1-0 ; 
       bound-mat7.1-0 ; 
       bound-mat8.1-0 ; 
       bound-mat9.1-0 ; 
       bound-shaft.1-0 ; 
       bound-shaft1.1-0 ; 
       bound-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       bound-corrdr2_3.1-0 ; 
       bound-corrdr2_4.1-0 ; 
       bound-corrdr2_5.1-0 ; 
       bound-corrdr2_6.1-0 ; 
       bound-cube4.1-0 ; 
       bound-cube5_1.2-0 ; 
       bound-cube5_4.1-0 ; 
       bound-cube5_6.1-0 ; 
       bound-cube5_7.1-0 ; 
       bound-cube6.1-0 ; 
       bound-cube8.1-0 ; 
       bound-cube9.1-0 ; 
       bound-fuselg6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss94/PICTURES/ss94 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss94-bound.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       bound-t2d24.1-0 ; 
       bound-t2d25.1-0 ; 
       bound-t2d26.1-0 ; 
       bound-t2d27.1-0 ; 
       bound-t2d28.1-0 ; 
       bound-t2d29.1-0 ; 
       bound-t2d30.1-0 ; 
       bound-t2d31.1-0 ; 
       bound-t2d32.1-0 ; 
       bound-t2d33.1-0 ; 
       bound-t2d34.1-0 ; 
       bound-t2d39.1-0 ; 
       bound-t2d4.1-0 ; 
       bound-t2d40.1-0 ; 
       bound-t2d41.1-0 ; 
       bound-t2d42.1-0 ; 
       bound-t2d43.1-0 ; 
       bound-t2d44.1-0 ; 
       bound-t2d45.1-0 ; 
       bound-t2d46.1-0 ; 
       bound-t2d47.1-0 ; 
       bound-t2d48.1-0 ; 
       bound-t2d49.1-0 ; 
       bound-t2d5.1-0 ; 
       bound-t2d50.1-0 ; 
       bound-t2d51.1-0 ; 
       bound-t2d52.1-0 ; 
       bound-t2d6.1-0 ; 
       bound-t2d7.1-0 ; 
       bound-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 12 110 ; 
       2 12 110 ; 
       3 12 110 ; 
       4 5 110 ; 
       5 12 110 ; 
       6 12 110 ; 
       7 12 110 ; 
       8 12 110 ; 
       9 6 110 ; 
       10 7 110 ; 
       11 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 29 300 ; 
       0 30 300 ; 
       0 31 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       4 0 300 ; 
       4 1 300 ; 
       4 3 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       7 10 300 ; 
       8 24 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       9 9 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       11 25 300 ; 
       11 26 300 ; 
       11 27 300 ; 
       12 2 300 ; 
       12 32 300 ; 
       12 19 300 ; 
       12 28 300 ; 
       12 34 300 ; 
       12 33 300 ; 
       12 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       3 2 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 13 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       18 18 401 ; 
       19 23 401 ; 
       20 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 12 401 ; 
       33 4 401 ; 
       34 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 MPRFLG 0 ; 
       12 SCHEM 11.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
