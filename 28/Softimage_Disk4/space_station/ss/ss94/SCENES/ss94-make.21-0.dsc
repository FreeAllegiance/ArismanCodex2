SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.18-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       make-mat30.2-0 ; 
       make-mat31.1-0 ; 
       make-mat33.2-0 ; 
       make-mat35.1-0 ; 
       make-mat36.1-0 ; 
       make-mat37.1-0 ; 
       rix_post_sPTL-mat12.3-0 ; 
       rix_post_sPTL-mat13.3-0 ; 
       rix_post_sPTL-mat28.3-0 ; 
       rix_post_sPTL-mat29.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       make-base.17-0 ROOT ; 
       make-fuselg6.1-0 ; 
       make-tetra5.1-0 ; 
       make-tetra7.1-0 ROOT ; 
       make-tetra8.1-0 ; 
       make-tetra9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss94/PICTURES/ss94 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss94-make.21-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       make-t2d29.3-0 ; 
       make-t2d30.2-0 ; 
       make-t2d32.4-0 ; 
       make-t2d34.1-0 ; 
       make-t2d35.1-0 ; 
       make-t2d36.1-0 ; 
       rix_post_sPTL-t2d10.6-0 ; 
       rix_post_sPTL-t2d11.6-0 ; 
       rix_post_sPTL-t2d27.6-0 ; 
       rix_post_sPTL-t2d28.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 1 110 ; 
       5 1 110 ; 
       1 0 110 ; 
       2 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 4 300 ; 
       5 5 300 ; 
       3 3 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       2 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 1 401 ; 
       4 4 401 ; 
       2 2 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 32 0 0 MPRFLG 0 ; 
       5 SCHEM 39.5 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1.19 1.19 1.19 0 1.967999 0 -0.1948112 0.1379913 0 MPRFLG 0 ; 
       0 SCHEM 18.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 19.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 27 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 39.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
