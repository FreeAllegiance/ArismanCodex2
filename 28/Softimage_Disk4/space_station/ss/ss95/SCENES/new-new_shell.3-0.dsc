SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.34-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_post_sPTL-inf_light1_1.26-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.26-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.26-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       new_shell-mat159.1-0 ; 
       new_shell-mat160.1-0 ; 
       new_shell-mat161.1-0 ; 
       new_shell-mat162.1-0 ; 
       new_shell-mat163.1-0 ; 
       new_shell-mat164.1-0 ; 
       new_shell-mat2.1-0 ; 
       rip-default1.3-0 ; 
       rip-default2.3-0 ; 
       rip-default3.3-0 ; 
       rip-default4.3-0 ; 
       rip-mat149.2-0 ; 
       rip-mat150.2-0 ; 
       rip-mat158.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       poly2-cube2.1-0 ; 
       poly2-cube6.1-0 ; 
       poly2-cube7.1-0 ; 
       poly2-cube8.1-0 ; 
       poly2-fuselg7.1-0 ; 
       poly2-grid8.3-0 ROOT ; 
       poly2-icosa1.1-0 ; 
       poly2-icosa5.1-0 ; 
       poly2-icosa6.1-0 ; 
       poly2-icosa7.1-0 ; 
       poly2-tetra1.1-0 ; 
       poly2-tetra5.1-0 ; 
       poly2-tetra6.1-0 ; 
       poly2-tetra7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss95/PICTURES/ss95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new-new_shell.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       new_shell-t2d2.1-0 ; 
       new_shell-t2d45.1-0 ; 
       new_shell-t2d46.2-0 ; 
       new_shell-t2d47.1-0 ; 
       new_shell-t2d48.2-0 ; 
       new_shell-t2d49.1-0 ; 
       new_shell-t2d50.2-0 ; 
       rip-t2d31.5-0 ; 
       rip-t2d32.4-0 ; 
       rip-t2d40.2-0 ; 
       rip-t2d41.2-0 ; 
       rip-t2d42.2-0 ; 
       rip-t2d43.1-0 ; 
       rip-t2d44.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       4 5 110 ; 
       6 0 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       1 4 110 ; 
       7 1 110 ; 
       2 4 110 ; 
       8 2 110 ; 
       3 4 110 ; 
       9 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 6 300 ; 
       0 11 300 ; 
       4 13 300 ; 
       6 12 300 ; 
       10 7 300 ; 
       11 8 300 ; 
       12 9 300 ; 
       13 10 300 ; 
       1 0 300 ; 
       7 1 300 ; 
       2 2 300 ; 
       8 3 300 ; 
       3 4 300 ; 
       9 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 10 401 ; 
       8 11 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       0 1 401 ; 
       13 9 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 11.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 11.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 21.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
