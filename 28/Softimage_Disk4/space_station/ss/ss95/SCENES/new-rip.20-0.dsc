SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.17-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_post_sPTL-inf_light1_1.22-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.22-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.22-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       rip-default1.3-0 ; 
       rip-default2.3-0 ; 
       rip-default3.3-0 ; 
       rip-default4.3-0 ; 
       rip-mat149.2-0 ; 
       rip-mat150.2-0 ; 
       rip-mat157.1-0 ; 
       rip-mat158.1-0 ; 
       rip-mat159.1-0 ; 
       rip-mat160.1-0 ; 
       rip-mat161.1-0 ; 
       rip-mat162.1-0 ; 
       rip-mat163.1-0 ; 
       rip-mat164.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       tube2-cube2.1-0 ; 
       tube2-cube3.1-0 ; 
       tube2-cube4.1-0 ; 
       tube2-cube5.1-0 ; 
       tube2-fuselg7.1-0 ; 
       tube2-icosa1.1-0 ; 
       tube2-icosa2.1-0 ; 
       tube2-icosa3.1-0 ; 
       tube2-icosa4.1-0 ; 
       tube2-tetra1.1-0 ; 
       tube2-tetra5.1-0 ; 
       tube2-tetra6.1-0 ; 
       tube2-tetra7.1-0 ; 
       tube2-tube_1.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss95/PICTURES/ss95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new-rip.20-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       rip-t2d31.4-0 ; 
       rip-t2d32.3-0 ; 
       rip-t2d39.3-0 ; 
       rip-t2d40.2-0 ; 
       rip-t2d41.2-0 ; 
       rip-t2d42.2-0 ; 
       rip-t2d43.1-0 ; 
       rip-t2d44.1-0 ; 
       rip-t2d45.1-0 ; 
       rip-t2d46.1-0 ; 
       rip-t2d47.1-0 ; 
       rip-t2d48.1-0 ; 
       rip-t2d49.1-0 ; 
       rip-t2d50.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 13 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 2 110 ; 
       8 3 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 8 300 ; 
       2 10 300 ; 
       3 12 300 ; 
       4 7 300 ; 
       5 5 300 ; 
       6 9 300 ; 
       7 11 300 ; 
       8 13 300 ; 
       9 0 300 ; 
       10 1 300 ; 
       11 2 300 ; 
       12 3 300 ; 
       13 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 5 401 ; 
       2 6 401 ; 
       3 7 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 15 -4 0 MPRFLG 0 ; 
       13 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
