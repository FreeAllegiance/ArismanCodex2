SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.52-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_post_sPTL-inf_light1_1.44-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.44-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.44-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       rip-default1.3-0 ; 
       rip-default2.3-0 ; 
       rip-default3.3-0 ; 
       rip-default4.3-0 ; 
       rip-mat149.2-0 ; 
       rip-mat150.2-0 ; 
       rip-mat158.2-0 ; 
       static-mat159.1-0 ; 
       static-mat160.1-0 ; 
       static-mat161.1-0 ; 
       static-mat162.1-0 ; 
       static-mat163.1-0 ; 
       static-mat164.1-0 ; 
       static-mat165.1-0 ; 
       static-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       poly2-cube2.1-0 ; 
       poly2-cube6.1-0 ; 
       poly2-cube7.1-0 ; 
       poly2-cube8.1-0 ; 
       poly2-fuselg7.1-0 ; 
       poly2-icosa1.1-0 ; 
       poly2-icosa5.1-0 ; 
       poly2-icosa6.1-0 ; 
       poly2-icosa7.1-0 ; 
       poly2-root.6-0 ROOT ; 
       poly2-tetra1.1-0 ; 
       poly2-tetra5.1-0 ; 
       poly2-tetra6.1-0 ; 
       poly2-tetra7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss95/PICTURES/ss95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       rip-t2d31.6-0 ; 
       rip-t2d32.5-0 ; 
       rip-t2d40.4-0 ; 
       rip-t2d41.3-0 ; 
       rip-t2d42.3-0 ; 
       rip-t2d43.2-0 ; 
       rip-t2d44.2-0 ; 
       static-t2d2.1-0 ; 
       static-t2d45.1-0 ; 
       static-t2d46.1-0 ; 
       static-t2d47.1-0 ; 
       static-t2d48.1-0 ; 
       static-t2d49.1-0 ; 
       static-t2d50.1-0 ; 
       static-t2d51.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 9 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 2 110 ; 
       8 3 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 7 300 ; 
       2 9 300 ; 
       3 11 300 ; 
       4 6 300 ; 
       4 13 300 ; 
       9 14 300 ; 
       5 5 300 ; 
       6 8 300 ; 
       7 10 300 ; 
       8 12 300 ; 
       10 0 300 ; 
       11 1 300 ; 
       12 2 300 ; 
       13 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       9 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 12 401 ; 
       12 13 401 ; 
       13 14 401 ; 
       0 3 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       9 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 10 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 37.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
