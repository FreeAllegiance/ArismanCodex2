SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap05-cap05.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.1-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 12     
       rix_carrier_s-inf_light1_1.1-0 ROOT ; 
       rix_carrier_s-inf_light1_1_1.1-0 ROOT ; 
       rix_carrier_s-inf_light2_1.1-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1.1-0 ROOT ; 
       rix_carrier_s-inf_light3_1.1-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1.1-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.1-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1.1-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.1-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1.1-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.1-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       qq-mat1.2-0 ; 
       qq-mat148.1-0 ; 
       qq-mat149.1-0 ; 
       qq-mat150.1-0 ; 
       qq-mat151.1-0 ; 
       qq-mat2.2-0 ; 
       rix_post_sPTL-mat1.1-0 ; 
       rix_post_sPTL-mat10.1-0 ; 
       rix_post_sPTL-mat11.1-0 ; 
       rix_post_sPTL-mat12.1-0 ; 
       rix_post_sPTL-mat13.1-0 ; 
       rix_post_sPTL-mat14.1-0 ; 
       rix_post_sPTL-mat15.1-0 ; 
       rix_post_sPTL-mat16.1-0 ; 
       rix_post_sPTL-mat2.1-0 ; 
       rix_post_sPTL-mat21.1-0 ; 
       rix_post_sPTL-mat22.1-0 ; 
       rix_post_sPTL-mat23.1-0 ; 
       rix_post_sPTL-mat24.1-0 ; 
       rix_post_sPTL-mat25.1-0 ; 
       rix_post_sPTL-mat26.1-0 ; 
       rix_post_sPTL-mat27.1-0 ; 
       rix_post_sPTL-mat28.1-0 ; 
       rix_post_sPTL-mat29.1-0 ; 
       rix_post_sPTL-mat3.1-0 ; 
       rix_post_sPTL-mat4.1-0 ; 
       rix_post_sPTL-mat5.1-0 ; 
       rix_post_sPTL-mat7.1-0 ; 
       rix_post_sPTL-mat8.1-0 ; 
       rix_post_sPTL-mat9.1-0 ; 
       Static-bay1.1-0 ; 
       Static-mat144.1-0 ; 
       Static-mat145.1-0 ; 
       Static-mat146.1-0 ; 
       Static-mat147.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       cap05-adoccon.1-0 ; 
       cap05-cap05.1-0 ROOT ; 
       cap05-contwr.1-0 ; 
       cap05-fuselg0.1-0 ; 
       cap05-fuselg1.1-0 ; 
       cap05-fuselg2.1-0 ; 
       cap05-fuselg3.1-0 ; 
       cap05-fuselg4.1-0 ; 
       cap05-fuselg5.1-0 ; 
       cap05-fuselg6.1-0 ; 
       cap05-ldoccon.1-0 ; 
       cap05-lndpad1.1-0 ; 
       cap05-lndpad2.1-0 ; 
       cap05-lndpad3.1-0 ; 
       cap05-lndpad4.1-0 ; 
       cap05-lturatt.1-0 ; 
       cap05-platfm0.1-0 ; 
       cap05-platfm1.1-0 ; 
       cap05-platfm2.1-0 ; 
       cap05-platfm3.1-0 ; 
       cap05-platfm4.1-0 ; 
       cap05-rdoccon.1-0 ; 
       cap05-SS1.1-0 ; 
       cap05-SS2.1-0 ; 
       cap05-SS3.1-0 ; 
       cap05-SS4.1-0 ; 
       qq-fuselg7.1-0 ROOT ; 
       qq-spiral16.2-0 ROOT ; 
       tube-tube.10-0 ROOT ; 
       tube1-tube.2-0 ROOT ; 
       tube2-tube.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/More_Stations/Rix/ss/ss95/PICTURES/cap05 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss95/PICTURES/skin ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss95/PICTURES/ss95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       qq-qq.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       qq-t2d1.2-0 ; 
       qq-t2d2.2-0 ; 
       qq-t2d30.1-0 ; 
       qq-t2d31.1-0 ; 
       qq-t2d32.1-0 ; 
       qq-t2d33.1-0 ; 
       rix_post_sPTL-t2d1.1-0 ; 
       rix_post_sPTL-t2d10.1-0 ; 
       rix_post_sPTL-t2d11.1-0 ; 
       rix_post_sPTL-t2d12.1-0 ; 
       rix_post_sPTL-t2d14.1-0 ; 
       rix_post_sPTL-t2d19.1-0 ; 
       rix_post_sPTL-t2d2.1-0 ; 
       rix_post_sPTL-t2d20.1-0 ; 
       rix_post_sPTL-t2d21.1-0 ; 
       rix_post_sPTL-t2d22.1-0 ; 
       rix_post_sPTL-t2d23.1-0 ; 
       rix_post_sPTL-t2d24.1-0 ; 
       rix_post_sPTL-t2d25.1-0 ; 
       rix_post_sPTL-t2d26.1-0 ; 
       rix_post_sPTL-t2d27.1-0 ; 
       rix_post_sPTL-t2d28.1-0 ; 
       rix_post_sPTL-t2d3.1-0 ; 
       rix_post_sPTL-t2d4.1-0 ; 
       rix_post_sPTL-t2d5.1-0 ; 
       rix_post_sPTL-t2d6.1-0 ; 
       rix_post_sPTL-t2d7.1-0 ; 
       rix_post_sPTL-t2d8.1-0 ; 
       rix_post_sPTL-t2d9.1-0 ; 
       Static-t2d29.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       28 27 220 RELDATA SCLE 2.920241 6.230206 2.920241 ROLL 0 TRNS 0 -9.015214e-006 -1.192093e-007 EndOfRELDATA ; 
       28 27 220 2 ; 
       0 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 1 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 1 110 ; 
       10 4 110 ; 
       11 17 110 ; 
       12 18 110 ; 
       12 23 111 ; 
       12 23 114 ; 
       13 19 110 ; 
       14 20 110 ; 
       15 19 110 ; 
       16 1 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 16 110 ; 
       21 4 110 ; 
       22 17 110 ; 
       23 18 110 ; 
       24 19 110 ; 
       25 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       29 0 300 ; 
       30 5 300 ; 
       1 6 300 ; 
       2 27 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 30 300 ; 
       5 26 300 ; 
       6 14 300 ; 
       7 25 300 ; 
       8 24 300 ; 
       9 9 300 ; 
       9 10 300 ; 
       9 22 300 ; 
       9 23 300 ; 
       17 28 300 ; 
       18 7 300 ; 
       19 8 300 ; 
       20 29 300 ; 
       22 32 300 ; 
       23 31 300 ; 
       24 34 300 ; 
       25 33 300 ; 
       26 1 300 ; 
       26 2 300 ; 
       26 3 300 ; 
       26 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       29 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 1 401 ; 
       7 27 401 ; 
       8 28 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 14 401 ; 
       13 10 401 ; 
       14 6 401 ; 
       15 11 401 ; 
       16 13 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 12 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 29 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       28 SCHEM 55 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 52.5 0 0 DISPLAY 0 0 SRT 1 1 0.5679999 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 50 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 47.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 20 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 23.75 0 0 DISPLAY 0 0 SRT 0.9999999 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 45 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 42.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 35 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 41.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 36.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 30 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 23.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 40 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 37.5 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 27.5 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 22.5 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 87.5 0 0 SRT 0.06201597 0.06201598 0.06201598 -1.570796 3.141592 0 1.140293e-005 -1.098562e-005 -1.22799 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 46.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 86.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 86.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 86.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 86.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 86.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 86.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 86.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 86.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 46.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
