SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.61-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_post_sPTL-inf_light1_1.53-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.53-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.53-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       rip-default1.3-0 ; 
       rip-default2.3-0 ; 
       rip-default3.3-0 ; 
       rip-default4.3-0 ; 
       rip-mat149.2-0 ; 
       rip-mat150.2-0 ; 
       rip-mat158.3-0 ; 
       STATIC-mat159.1-0 ; 
       STATIC-mat160.1-0 ; 
       STATIC-mat161.1-0 ; 
       STATIC-mat162.1-0 ; 
       STATIC-mat163.1-0 ; 
       STATIC-mat164.1-0 ; 
       STATIC-mat165.1-0 ; 
       STATIC-mat175.1-0 ; 
       STATIC-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       poly2-cube2.1-0 ; 
       poly2-cube6.1-0 ; 
       poly2-cube7.1-0 ; 
       poly2-cube8.1-0 ; 
       poly2-fuselg7.1-0 ; 
       poly2-icosa1.1-0 ; 
       poly2-icosa5.1-0 ; 
       poly2-icosa6.1-0 ; 
       poly2-icosa7.1-0 ; 
       poly2-null1.1-0 ; 
       poly2-root.13-0 ROOT ; 
       poly2-tetra1.1-0 ; 
       poly2-tetra5.1-0 ; 
       poly2-tetra6.1-0 ; 
       poly2-tetra7.1-0 ; 
       poly2-torus5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss95/PICTURES/ss95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss95-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       rip-t2d31.7-0 ; 
       rip-t2d32.6-0 ; 
       rip-t2d40.7-0 ; 
       rip-t2d41.4-0 ; 
       rip-t2d42.4-0 ; 
       rip-t2d43.3-0 ; 
       rip-t2d44.3-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d45.1-0 ; 
       STATIC-t2d46.1-0 ; 
       STATIC-t2d47.1-0 ; 
       STATIC-t2d48.1-0 ; 
       STATIC-t2d49.1-0 ; 
       STATIC-t2d50.1-0 ; 
       STATIC-t2d51.1-0 ; 
       STATIC-t2d56.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 10 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 2 110 ; 
       8 3 110 ; 
       9 4 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 7 300 ; 
       2 9 300 ; 
       3 11 300 ; 
       4 6 300 ; 
       4 13 300 ; 
       5 5 300 ; 
       6 8 300 ; 
       7 10 300 ; 
       8 12 300 ; 
       10 15 300 ; 
       11 0 300 ; 
       12 1 300 ; 
       13 2 300 ; 
       14 3 300 ; 
       15 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 12 401 ; 
       12 13 401 ; 
       13 14 401 ; 
       14 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 30 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 20 -6 0 MPRFLG 0 ; 
       15 SCHEM 22.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
