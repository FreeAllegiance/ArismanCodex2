SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.69-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_post_sPTL-inf_light1_1.58-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.58-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.58-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       add_launch-mat159.2-0 ; 
       add_launch-mat160.2-0 ; 
       add_launch-mat161.2-0 ; 
       add_launch-mat162.2-0 ; 
       add_launch-mat163.2-0 ; 
       add_launch-mat164.2-0 ; 
       add_launch-mat165.2-0 ; 
       add_launch-mat171.2-0 ; 
       add_launch-mat172.2-0 ; 
       add_launch-mat173.2-0 ; 
       add_launch-mat174.2-0 ; 
       add_launch-mat175.2-0 ; 
       add_launch-mat2.2-0 ; 
       rip-default1.4-0 ; 
       rip-default2.4-0 ; 
       rip-default3.4-0 ; 
       rip-default4.4-0 ; 
       rip-mat149.3-0 ; 
       rip-mat150.3-0 ; 
       rip-mat158.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       poly2-cube2.1-0 ; 
       poly2-cube6.1-0 ; 
       poly2-cube7.1-0 ; 
       poly2-cube8.1-0 ; 
       poly2-cyl1.1-0 ; 
       poly2-cyl5.1-0 ; 
       poly2-cyl6.1-0 ; 
       poly2-cyl7.1-0 ; 
       poly2-fuselg7.1-0 ; 
       poly2-icosa1.1-0 ; 
       poly2-icosa5.1-0 ; 
       poly2-icosa6.1-0 ; 
       poly2-icosa7.1-0 ; 
       poly2-null1.1-0 ; 
       poly2-root.16-0 ROOT ; 
       poly2-tetra1.1-0 ; 
       poly2-tetra5.1-0 ; 
       poly2-tetra6.1-0 ; 
       poly2-tetra7.1-0 ; 
       poly2-torus5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss95/PICTURES/ss95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss95-STATIC.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       add_launch-t2d2.2-0 ; 
       add_launch-t2d45.2-0 ; 
       add_launch-t2d46.2-0 ; 
       add_launch-t2d47.2-0 ; 
       add_launch-t2d48.2-0 ; 
       add_launch-t2d49.2-0 ; 
       add_launch-t2d50.2-0 ; 
       add_launch-t2d51.2-0 ; 
       add_launch-t2d52.2-0 ; 
       add_launch-t2d53.2-0 ; 
       add_launch-t2d54.2-0 ; 
       add_launch-t2d55.2-0 ; 
       add_launch-t2d56.2-0 ; 
       rip-t2d31.8-0 ; 
       rip-t2d32.7-0 ; 
       rip-t2d40.8-0 ; 
       rip-t2d41.5-0 ; 
       rip-t2d42.5-0 ; 
       rip-t2d43.4-0 ; 
       rip-t2d44.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 8 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 14 110 ; 
       5 14 110 ; 
       6 14 110 ; 
       7 14 110 ; 
       8 14 110 ; 
       9 0 110 ; 
       10 1 110 ; 
       11 2 110 ; 
       12 3 110 ; 
       13 8 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 13 110 ; 
       19 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 17 300 ; 
       1 0 300 ; 
       2 2 300 ; 
       3 4 300 ; 
       4 7 300 ; 
       5 8 300 ; 
       6 9 300 ; 
       7 10 300 ; 
       8 19 300 ; 
       8 6 300 ; 
       9 18 300 ; 
       10 1 300 ; 
       11 3 300 ; 
       12 5 300 ; 
       14 12 300 ; 
       15 13 300 ; 
       16 14 300 ; 
       17 15 300 ; 
       18 16 300 ; 
       19 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       14 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 12 401 ; 
       13 16 401 ; 
       14 17 401 ; 
       15 18 401 ; 
       16 19 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 35 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 37.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 40 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 30 -2 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 17.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 20 -6 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
