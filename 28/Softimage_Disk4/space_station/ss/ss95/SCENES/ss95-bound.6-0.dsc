SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.74-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_post_sPTL-inf_light1_1.61-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.61-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.61-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       bounding_model-bound1.1-0 ; 
       bounding_model-bound2.1-0 ; 
       bounding_model-bounding_model.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss95-bound.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 7.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 3.75 0 0 DISPLAY 3 2 SRT 0.5920001 0.5920001 0.5920001 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
