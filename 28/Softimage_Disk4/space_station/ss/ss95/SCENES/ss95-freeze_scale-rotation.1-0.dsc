SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.68-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_post_sPTL-inf_light1_1.57-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.57-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.57-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       add_launch-mat159.2-0 ; 
       add_launch-mat160.2-0 ; 
       add_launch-mat161.2-0 ; 
       add_launch-mat162.2-0 ; 
       add_launch-mat163.2-0 ; 
       add_launch-mat164.2-0 ; 
       add_launch-mat165.2-0 ; 
       add_launch-mat166.2-0 ; 
       add_launch-mat167.2-0 ; 
       add_launch-mat168.2-0 ; 
       add_launch-mat169.2-0 ; 
       add_launch-mat170.2-0 ; 
       add_launch-mat171.2-0 ; 
       add_launch-mat172.2-0 ; 
       add_launch-mat173.2-0 ; 
       add_launch-mat174.2-0 ; 
       add_launch-mat175.2-0 ; 
       add_launch-mat2.2-0 ; 
       rip-default1.4-0 ; 
       rip-default2.4-0 ; 
       rip-default3.4-0 ; 
       rip-default4.4-0 ; 
       rip-mat149.3-0 ; 
       rip-mat150.3-0 ; 
       rip-mat158.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       poly2-cube2.1-0 ; 
       poly2-cube6.1-0 ; 
       poly2-cube7.1-0 ; 
       poly2-cube8.1-0 ; 
       poly2-cyl1.1-0 ; 
       poly2-cyl5.1-0 ; 
       poly2-cyl6.1-0 ; 
       poly2-cyl7.1-0 ; 
       poly2-fuselg7.1-0 ; 
       poly2-icosa1.1-0 ; 
       poly2-icosa5.1-0 ; 
       poly2-icosa6.1-0 ; 
       poly2-icosa7.1-0 ; 
       poly2-launch1.1-0 ; 
       poly2-null1.1-0 ; 
       poly2-root.15-0 ROOT ; 
       poly2-ss01.1-0 ; 
       poly2-ss02.1-0 ; 
       poly2-ss03.1-0 ; 
       poly2-ss04.1-0 ; 
       poly2-ss5.1-0 ; 
       poly2-tetra1.1-0 ; 
       poly2-tetra5.1-0 ; 
       poly2-tetra6.1-0 ; 
       poly2-tetra7.1-0 ; 
       poly2-torus5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss95/PICTURES/ss95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss95-freeze_scale-rotation.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       add_launch-t2d2.2-0 ; 
       add_launch-t2d45.2-0 ; 
       add_launch-t2d46.2-0 ; 
       add_launch-t2d47.2-0 ; 
       add_launch-t2d48.2-0 ; 
       add_launch-t2d49.2-0 ; 
       add_launch-t2d50.2-0 ; 
       add_launch-t2d51.2-0 ; 
       add_launch-t2d52.2-0 ; 
       add_launch-t2d53.2-0 ; 
       add_launch-t2d54.2-0 ; 
       add_launch-t2d55.2-0 ; 
       add_launch-t2d56.2-0 ; 
       rip-t2d31.8-0 ; 
       rip-t2d32.7-0 ; 
       rip-t2d40.8-0 ; 
       rip-t2d41.5-0 ; 
       rip-t2d42.5-0 ; 
       rip-t2d43.4-0 ; 
       rip-t2d44.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 8 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 15 110 ; 
       5 15 110 ; 
       6 15 110 ; 
       7 15 110 ; 
       8 15 110 ; 
       9 0 110 ; 
       10 1 110 ; 
       11 2 110 ; 
       12 3 110 ; 
       13 15 110 ; 
       14 8 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 15 110 ; 
       20 15 110 ; 
       21 14 110 ; 
       22 14 110 ; 
       23 14 110 ; 
       24 14 110 ; 
       25 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 22 300 ; 
       1 0 300 ; 
       2 2 300 ; 
       3 4 300 ; 
       4 12 300 ; 
       5 13 300 ; 
       6 14 300 ; 
       7 15 300 ; 
       8 24 300 ; 
       8 6 300 ; 
       9 23 300 ; 
       10 1 300 ; 
       11 3 300 ; 
       12 5 300 ; 
       15 17 300 ; 
       16 7 300 ; 
       17 8 300 ; 
       18 9 300 ; 
       19 10 300 ; 
       20 11 300 ; 
       21 18 300 ; 
       22 19 300 ; 
       23 20 300 ; 
       24 21 300 ; 
       25 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       15 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 13 401 ; 
       23 14 401 ; 
       24 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 50 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 52.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 55 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 40 -2 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 45 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 47.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 15 -6 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 20 -6 0 MPRFLG 0 ; 
       25 SCHEM 22.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 49 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
