SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.57-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_post_sPTL-inf_light1_1.49-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.49-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.49-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       rip-default1.3-0 ; 
       rip-default2.3-0 ; 
       rip-default3.3-0 ; 
       rip-default4.3-0 ; 
       rip-mat149.2-0 ; 
       rip-mat150.2-0 ; 
       rip-mat158.3-0 ; 
       rotate-mat159.1-0 ; 
       rotate-mat160.1-0 ; 
       rotate-mat161.1-0 ; 
       rotate-mat162.1-0 ; 
       rotate-mat163.1-0 ; 
       rotate-mat164.1-0 ; 
       rotate-mat165.2-0 ; 
       rotate-mat166.1-0 ; 
       rotate-mat167.1-0 ; 
       rotate-mat168.1-0 ; 
       rotate-mat169.1-0 ; 
       rotate-mat170.1-0 ; 
       rotate-mat171.1-0 ; 
       rotate-mat172.1-0 ; 
       rotate-mat173.1-0 ; 
       rotate-mat174.1-0 ; 
       rotate-mat175.1-0 ; 
       rotate-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       poly2-cube2.1-0 ; 
       poly2-cube6.1-0 ; 
       poly2-cube7.1-0 ; 
       poly2-cube8.1-0 ; 
       poly2-cyl1.1-0 ; 
       poly2-cyl5.1-0 ; 
       poly2-cyl6.1-0 ; 
       poly2-cyl7.1-0 ; 
       poly2-fuselg7.1-0 ; 
       poly2-icosa1.1-0 ; 
       poly2-icosa5.1-0 ; 
       poly2-icosa6.1-0 ; 
       poly2-icosa7.1-0 ; 
       poly2-null1.1-0 ; 
       poly2-root.11-0 ROOT ; 
       poly2-ss01.1-0 ; 
       poly2-ss02.1-0 ; 
       poly2-ss03.1-0 ; 
       poly2-ss04.1-0 ; 
       poly2-ss5.1-0 ; 
       poly2-tetra1.1-0 ; 
       poly2-tetra5.1-0 ; 
       poly2-tetra6.1-0 ; 
       poly2-tetra7.1-0 ; 
       poly2-torus5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss95/PICTURES/ss95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss95-rotate.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       rip-t2d31.7-0 ; 
       rip-t2d32.6-0 ; 
       rip-t2d40.7-0 ; 
       rip-t2d41.4-0 ; 
       rip-t2d42.4-0 ; 
       rip-t2d43.3-0 ; 
       rip-t2d44.3-0 ; 
       rotate-t2d2.3-0 ; 
       rotate-t2d45.3-0 ; 
       rotate-t2d46.3-0 ; 
       rotate-t2d47.3-0 ; 
       rotate-t2d48.3-0 ; 
       rotate-t2d49.3-0 ; 
       rotate-t2d50.3-0 ; 
       rotate-t2d51.5-0 ; 
       rotate-t2d52.1-0 ; 
       rotate-t2d53.1-0 ; 
       rotate-t2d54.1-0 ; 
       rotate-t2d55.1-0 ; 
       rotate-t2d56.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 8 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 14 110 ; 
       5 14 110 ; 
       6 14 110 ; 
       7 14 110 ; 
       8 14 110 ; 
       9 0 110 ; 
       10 1 110 ; 
       11 2 110 ; 
       12 3 110 ; 
       13 8 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 13 110 ; 
       21 13 110 ; 
       22 13 110 ; 
       23 13 110 ; 
       24 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 7 300 ; 
       2 9 300 ; 
       3 11 300 ; 
       4 19 300 ; 
       5 20 300 ; 
       6 21 300 ; 
       7 22 300 ; 
       8 6 300 ; 
       8 13 300 ; 
       9 5 300 ; 
       10 8 300 ; 
       11 10 300 ; 
       12 12 300 ; 
       14 24 300 ; 
       15 14 300 ; 
       16 15 300 ; 
       17 16 300 ; 
       18 17 300 ; 
       19 18 300 ; 
       20 0 300 ; 
       21 1 300 ; 
       22 2 300 ; 
       23 3 300 ; 
       24 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       14 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 12 401 ; 
       12 13 401 ; 
       13 14 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 47.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 50 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 52.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 40 -2 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 45 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 23.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 15 -6 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 20 -6 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 57.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
