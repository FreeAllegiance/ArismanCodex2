SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.1-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       poly-grid7.1-0 ROOT ; 
       poly-grid8.1-0 ROOT ; 
       poly-spiral22.1-0 ROOT ; 
       rip-spiral16.6-0 ROOT ; 
       tube3-tube_1.4-0 ROOT ; 
       www1-grid8.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       test-poly.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 0.5679999 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
