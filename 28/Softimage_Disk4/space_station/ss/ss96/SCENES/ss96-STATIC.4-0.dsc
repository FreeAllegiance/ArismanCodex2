SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.145-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       done_text-mat151.4-0 ; 
       done_text-mat162.4-0 ; 
       done_text-mat164.4-0 ; 
       done_text-mat165.4-0 ; 
       done_text-mat166.4-0 ; 
       done_text-mat167.4-0 ; 
       dowager_sPtL-mat1.5-0 ; 
       dowager_sPtL-mat14.7-0 ; 
       dowager_sPtL-mat2.5-0 ; 
       dowager_sPtL-mat3.5-0 ; 
       dowager_sPtL-mat4.7-0 ; 
       dowager_sPtL-mat60.4-0 ; 
       dowager_sPtL-mat62.4-0 ; 
       dowager_sPtL-mat86.9-0 ; 
       dowager_sPtL-mat90.9-0 ; 
       dowager_sPtL-mat91.9-0 ; 
       strobes-bottom1.2-0 ; 
       strobes-mat168.2-0 ; 
       strobes-mat186.2-0 ; 
       strobes-mat187.2-0 ; 
       strobes-mat188.2-0 ; 
       strobes-mat189.2-0 ; 
       strobes-mat190.2-0 ; 
       strobes-mat191.2-0 ; 
       strobes-mat192.2-0 ; 
       strobes-mat193.2-0 ; 
       strobes-mat194.2-0 ; 
       strobes-mat195.2-0 ; 
       strobes-mat196.2-0 ; 
       strobes-mat197.2-0 ; 
       strobes-mat198.2-0 ; 
       strobes-mat199.2-0 ; 
       strobes-mat200.2-0 ; 
       strobes-mat201.2-0 ; 
       strobes-mat202.2-0 ; 
       strobes-mat203.2-0 ; 
       strobes-mat204.2-0 ; 
       strobes-mat205.2-0 ; 
       strobes-mat206.2-0 ; 
       strobes-mat207.2-0 ; 
       strobes-side1.2-0 ; 
       strobes-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       strobes-contwr1.1-0 ; 
       strobes-corrdr2.1-0 ; 
       strobes-corrdr4.1-0 ; 
       strobes-cube1.2-0 ; 
       strobes-fuselg1.1-0 ; 
       strobes-fuselg10.1-0 ; 
       strobes-fuselg15.1-0 ; 
       strobes-fuselg16.1-0 ; 
       strobes-fuselg17.1-0 ; 
       strobes-fuselg18.1-0 ; 
       strobes-fuselg3.1-0 ; 
       strobes-fuselg7.1-0 ; 
       strobes-null1.1-0 ; 
       strobes-platfrm1.1-0 ; 
       strobes-root_1.1-0 ; 
       strobes-root_5.1-0 ; 
       strobes-root_6.1-0 ; 
       strobes-ss01_2.34-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss96/PICTURES/rixbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-STATIC.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       done_text-t2d15.15-0 ; 
       done_text-t2d17.15-0 ; 
       done_text-t2d18.15-0 ; 
       done_text-t2d19.15-0 ; 
       done_text-t2d20.15-0 ; 
       done_text-t2d3.13-0 ; 
       dowager_sPtL-t2d1.13-0 ; 
       dowager_sPtL-t2d11.15-0 ; 
       dowager_sPtL-t2d2.13-0 ; 
       dowager_sPtL-t2d46.12-0 ; 
       dowager_sPtL-t2d59_1.15-0 ; 
       dowager_sPtL-t2d63_1.15-0 ; 
       strobes-t2d100.8-0 ; 
       strobes-t2d101.9-0 ; 
       strobes-t2d59_2.8-0 ; 
       strobes-t2d63_2.8-0 ; 
       strobes-t2d64.9-0 ; 
       strobes-t2d65.9-0 ; 
       strobes-t2d66.9-0 ; 
       strobes-t2d67.9-0 ; 
       strobes-t2d68.8-0 ; 
       strobes-t2d83.8-0 ; 
       strobes-t2d84.8-0 ; 
       strobes-t2d85.8-0 ; 
       strobes-t2d86.8-0 ; 
       strobes-t2d87.8-0 ; 
       strobes-t2d88.8-0 ; 
       strobes-t2d89.8-0 ; 
       strobes-t2d90.8-0 ; 
       strobes-t2d91.8-0 ; 
       strobes-t2d92.8-0 ; 
       strobes-t2d93.8-0 ; 
       strobes-t2d94.8-0 ; 
       strobes-t2d95.8-0 ; 
       strobes-t2d96.8-0 ; 
       strobes-t2d97.8-0 ; 
       strobes-t2d98.8-0 ; 
       strobes-t2d99.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       3 12 110 ; 
       4 17 110 ; 
       5 14 110 ; 
       6 15 110 ; 
       7 15 110 ; 
       8 16 110 ; 
       9 16 110 ; 
       10 13 110 ; 
       11 14 110 ; 
       12 13 110 ; 
       13 17 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       1 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       4 6 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       5 17 300 ; 
       6 30 300 ; 
       7 31 300 ; 
       8 37 300 ; 
       9 38 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       11 0 300 ; 
       13 10 300 ; 
       13 41 300 ; 
       13 40 300 ; 
       13 16 300 ; 
       13 39 300 ; 
       14 1 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 27 300 ; 
       15 28 300 ; 
       15 29 300 ; 
       16 32 300 ; 
       16 33 300 ; 
       16 34 300 ; 
       16 35 300 ; 
       16 36 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 20 400 ; 
       6 30 400 ; 
       7 31 400 ; 
       8 37 400 ; 
       9 12 400 ; 
       11 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 7 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 16 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 19 401 ; 
       18 21 401 ; 
       19 22 401 ; 
       20 23 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       39 13 401 ; 
       40 18 401 ; 
       41 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 25 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 MPRFLG 0 ; 
       5 SCHEM 20 -10 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 15 -10 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       16 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       17 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 55.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 55.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
