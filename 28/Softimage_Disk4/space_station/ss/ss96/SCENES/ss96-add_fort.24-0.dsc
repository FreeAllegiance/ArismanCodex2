SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.24-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       dowager_sPtL-inf_light5_1_2.24-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.24-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.24-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       add_fort-mat105.1-0 ; 
       add_fort-mat106.3-0 ; 
       add_fort-mat107.2-0 ; 
       add_fort-mat108.2-0 ; 
       add_fort-mat109.2-0 ; 
       add_fort-mat110.1-0 ; 
       add_fort-mat111.1-0 ; 
       add_fort-mat112.1-0 ; 
       add_fort-mat113.1-0 ; 
       add_fort-mat114.1-0 ; 
       add_fort-mat115.1-0 ; 
       add_fort-mat116.1-0 ; 
       add_fort-mat117.1-0 ; 
       add_fort-mat118.1-0 ; 
       add_fort-mat119.1-0 ; 
       add_fort-mat120.1-0 ; 
       add_fort-mat121.1-0 ; 
       add_fort-mat122.1-0 ; 
       add_fort-mat3.2-0 ; 
       add_fort-mat9.2-0 ; 
       dowager_sPtL-mat86.2-0 ; 
       dowager_sPtL-mat90.2-0 ; 
       dowager_sPtL-mat91.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       Nurbs3-contwr4.1-0 ; 
       Nurbs3-corrdr0.1-0 ; 
       Nurbs3-corrdr2.1-0 ; 
       Nurbs3-corrdr3.1-0 ; 
       Nurbs3-corrdr4.1-0 ; 
       Nurbs3-corrdr5.1-0 ; 
       Nurbs3-cyl1.1-0 ; 
       Nurbs3-cyl2.1-0 ; 
       Nurbs3-cyl3.1-0 ; 
       Nurbs3-cyl4.1-0 ; 
       Nurbs3-null2.1-0 ; 
       Nurbs3-Nurbs_Skin.37-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-add_fort.24-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 19     
       add_fort-t2d2.4-0 ; 
       add_fort-t2d72.2-0 ; 
       add_fort-t2d73.4-0 ; 
       add_fort-t2d74.4-0 ; 
       add_fort-t2d75.6-0 ; 
       add_fort-t2d76.5-0 ; 
       add_fort-t2d77.1-0 ; 
       add_fort-t2d78.1-0 ; 
       add_fort-t2d79.1-0 ; 
       add_fort-t2d8.4-0 ; 
       add_fort-t2d80.1-0 ; 
       add_fort-t2d81.1-0 ; 
       add_fort-t2d82.1-0 ; 
       add_fort-t2d83.2-0 ; 
       add_fort-t2d84.2-0 ; 
       add_fort-t2d85.3-0 ; 
       add_fort-t2d86.3-0 ; 
       dowager_sPtL-t2d59.5-0 ; 
       dowager_sPtL-t2d63.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       0 10 110 ; 
       1 11 110 ; 
       2 1 110 ; 
       10 11 110 ; 
       6 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 3 300 ; 
       7 17 300 ; 
       8 2 300 ; 
       8 15 300 ; 
       9 1 300 ; 
       9 14 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       0 0 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       6 4 300 ; 
       6 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       18 0 401 ; 
       19 9 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 25 -8 0 MPRFLG 0 ; 
       0 SCHEM 30 -8 0 MPRFLG 0 ; 
       1 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 30 -6 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 48.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
