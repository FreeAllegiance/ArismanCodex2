SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.102-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       dowager_sPtL-inf_light5_1_2_4.2-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.95-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.95-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 53     
       done_text-mat151.3-0 ; 
       done_text-mat162.3-0 ; 
       done_text-mat164.3-0 ; 
       done_text-mat165.3-0 ; 
       done_text-mat166.3-0 ; 
       done_text-mat167.3-0 ; 
       dowager_sPtL-mat1.3-0 ; 
       dowager_sPtL-mat14.5-0 ; 
       dowager_sPtL-mat2.3-0 ; 
       dowager_sPtL-mat3.3-0 ; 
       dowager_sPtL-mat4.6-0 ; 
       dowager_sPtL-mat60.3-0 ; 
       dowager_sPtL-mat62.3-0 ; 
       dowager_sPtL-mat86.8-0 ; 
       dowager_sPtL-mat90.8-0 ; 
       dowager_sPtL-mat91.8-0 ; 
       strobes-bottom1.1-0 ; 
       strobes-mat168.1-0 ; 
       strobes-mat186.1-0 ; 
       strobes-mat187.1-0 ; 
       strobes-mat188.1-0 ; 
       strobes-mat189.1-0 ; 
       strobes-mat190.1-0 ; 
       strobes-mat191.1-0 ; 
       strobes-mat192.1-0 ; 
       strobes-mat193.1-0 ; 
       strobes-mat194.1-0 ; 
       strobes-mat195.1-0 ; 
       strobes-mat196.1-0 ; 
       strobes-mat197.1-0 ; 
       strobes-mat198.1-0 ; 
       strobes-mat199.1-0 ; 
       strobes-mat200.1-0 ; 
       strobes-mat201.1-0 ; 
       strobes-mat202.1-0 ; 
       strobes-mat203.1-0 ; 
       strobes-mat204.1-0 ; 
       strobes-mat205.1-0 ; 
       strobes-mat206.1-0 ; 
       strobes-mat207.1-0 ; 
       strobes-mat208.1-0 ; 
       strobes-mat209.1-0 ; 
       strobes-mat210.1-0 ; 
       strobes-mat211.1-0 ; 
       strobes-mat212.1-0 ; 
       strobes-mat213.1-0 ; 
       strobes-mat214.1-0 ; 
       strobes-mat215.1-0 ; 
       strobes-mat216.1-0 ; 
       strobes-mat217.1-0 ; 
       strobes-mat218.1-0 ; 
       strobes-side1.1-0 ; 
       strobes-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       strobes-bay1.1-0 ; 
       strobes-bay2.1-0 ; 
       strobes-contwr1.1-0 ; 
       strobes-corrdr2.1-0 ; 
       strobes-corrdr4.1-0 ; 
       strobes-cube1.2-0 ; 
       strobes-fuselg1.1-0 ; 
       strobes-fuselg10.1-0 ; 
       strobes-fuselg15.1-0 ; 
       strobes-fuselg16.1-0 ; 
       strobes-fuselg17.1-0 ; 
       strobes-fuselg18.1-0 ; 
       strobes-fuselg3.1-0 ; 
       strobes-fuselg7.1-0 ; 
       strobes-garage1A.1-0 ; 
       strobes-garage1A1.1-0 ; 
       strobes-garage1B.1-0 ; 
       strobes-garage1B1.1-0 ; 
       strobes-garage1C.1-0 ; 
       strobes-garage1C1.1-0 ; 
       strobes-garage1D.1-0 ; 
       strobes-garage1D1.1-0 ; 
       strobes-garage1E.1-0 ; 
       strobes-garage1E1.1-0 ; 
       strobes-null1.1-0 ; 
       strobes-platfrm1.1-0 ; 
       strobes-root_1.1-0 ; 
       strobes-root_5.1-0 ; 
       strobes-root_6.1-0 ; 
       strobes-ss01.1-0 ; 
       strobes-ss01_2.3-0 ROOT ; 
       strobes-ss10.1-0 ; 
       strobes-ss11.1-0 ; 
       strobes-ss2.1-0 ; 
       strobes-ss3.1-0 ; 
       strobes-ss4.1-0 ; 
       strobes-ss5.1-0 ; 
       strobes-ss6.1-0 ; 
       strobes-ss7.1-0 ; 
       strobes-ss8.1-0 ; 
       strobes-ss9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/rixbay ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-add_new_garages.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       done_text-t2d15.8-0 ; 
       done_text-t2d17.8-0 ; 
       done_text-t2d18.8-0 ; 
       done_text-t2d19.8-0 ; 
       done_text-t2d20.8-0 ; 
       done_text-t2d3.6-0 ; 
       dowager_sPtL-t2d1.6-0 ; 
       dowager_sPtL-t2d11.8-0 ; 
       dowager_sPtL-t2d2.6-0 ; 
       dowager_sPtL-t2d46.5-0 ; 
       dowager_sPtL-t2d59_1.8-0 ; 
       dowager_sPtL-t2d63_1.8-0 ; 
       strobes-t2d100.1-0 ; 
       strobes-t2d101.2-0 ; 
       strobes-t2d59_2.1-0 ; 
       strobes-t2d63_2.1-0 ; 
       strobes-t2d64.2-0 ; 
       strobes-t2d65.2-0 ; 
       strobes-t2d66.2-0 ; 
       strobes-t2d67.2-0 ; 
       strobes-t2d68.1-0 ; 
       strobes-t2d83.1-0 ; 
       strobes-t2d84.1-0 ; 
       strobes-t2d85.1-0 ; 
       strobes-t2d86.1-0 ; 
       strobes-t2d87.1-0 ; 
       strobes-t2d88.1-0 ; 
       strobes-t2d89.1-0 ; 
       strobes-t2d90.1-0 ; 
       strobes-t2d91.1-0 ; 
       strobes-t2d92.1-0 ; 
       strobes-t2d93.1-0 ; 
       strobes-t2d94.1-0 ; 
       strobes-t2d95.1-0 ; 
       strobes-t2d96.1-0 ; 
       strobes-t2d97.1-0 ; 
       strobes-t2d98.1-0 ; 
       strobes-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 30 110 ; 
       1 30 110 ; 
       2 6 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       5 24 110 ; 
       6 30 110 ; 
       7 26 110 ; 
       8 27 110 ; 
       9 27 110 ; 
       10 28 110 ; 
       11 28 110 ; 
       12 25 110 ; 
       13 26 110 ; 
       14 0 110 ; 
       15 1 110 ; 
       16 0 110 ; 
       17 1 110 ; 
       18 0 110 ; 
       19 1 110 ; 
       20 0 110 ; 
       21 1 110 ; 
       22 0 110 ; 
       23 1 110 ; 
       24 25 110 ; 
       25 30 110 ; 
       26 5 110 ; 
       27 5 110 ; 
       28 5 110 ; 
       29 30 110 ; 
       31 30 110 ; 
       32 30 110 ; 
       33 30 110 ; 
       34 30 110 ; 
       35 30 110 ; 
       36 30 110 ; 
       37 30 110 ; 
       38 30 110 ; 
       39 30 110 ; 
       40 30 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 7 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       6 6 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       7 17 300 ; 
       8 30 300 ; 
       9 31 300 ; 
       10 37 300 ; 
       11 38 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       13 0 300 ; 
       25 10 300 ; 
       25 52 300 ; 
       25 51 300 ; 
       25 16 300 ; 
       25 39 300 ; 
       26 1 300 ; 
       26 2 300 ; 
       26 3 300 ; 
       26 4 300 ; 
       26 5 300 ; 
       27 25 300 ; 
       27 26 300 ; 
       27 27 300 ; 
       27 28 300 ; 
       27 29 300 ; 
       28 32 300 ; 
       28 33 300 ; 
       28 34 300 ; 
       28 35 300 ; 
       28 36 300 ; 
       29 40 300 ; 
       31 49 300 ; 
       32 50 300 ; 
       33 41 300 ; 
       34 42 300 ; 
       35 43 300 ; 
       36 44 300 ; 
       37 45 300 ; 
       38 46 300 ; 
       39 47 300 ; 
       40 48 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       7 20 400 ; 
       8 30 400 ; 
       9 31 400 ; 
       10 37 400 ; 
       11 12 400 ; 
       13 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 7 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 16 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 19 401 ; 
       18 21 401 ; 
       19 22 401 ; 
       20 23 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       39 13 401 ; 
       51 18 401 ; 
       52 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14.62329 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14.62329 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14.62329 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 35 -6 0 MPRFLG 0 ; 
       1 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 25 -8 0 MPRFLG 0 ; 
       3 SCHEM 0 -10.62329 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -10.62329 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -8.623295 0 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -12.62329 0 MPRFLG 0 ; 
       8 SCHEM 5 -12.62329 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -12.62329 0 MPRFLG 0 ; 
       10 SCHEM 10 -12.62329 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -12.62329 0 MPRFLG 0 ; 
       12 SCHEM 20 -6.623295 0 MPRFLG 0 ; 
       13 SCHEM 15 -12.62329 0 MPRFLG 0 ; 
       14 SCHEM 30 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 45 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 35 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 50 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 40 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 52.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 8.75 -6.623295 0 MPRFLG 0 ; 
       25 SCHEM 10 -4.623295 0 USR MPRFLG 0 ; 
       26 SCHEM 16.25 -10.62329 0 MPRFLG 0 ; 
       27 SCHEM 6.25 -10.62329 0 MPRFLG 0 ; 
       28 SCHEM 11.25 -10.62329 0 MPRFLG 0 ; 
       29 SCHEM 57.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 52.01615 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       31 SCHEM 70 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 72.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 55 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 60 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 62.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 65 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 67.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 22.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 75 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -14.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 46.01615 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -8.623295 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8.623295 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM -1 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM -1 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM -1 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6.623295 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -14.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -10.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -10.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM -1 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -14.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -14.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -12.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -14.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -14.62329 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -6.623295 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 21.5 -6.623295 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 21.5 -6.623295 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -10.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -1 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM -1 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8.623295 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 46.01615 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8.623295 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8.623295 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -8.623295 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -12.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -12.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM -1 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 14 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -14.62329 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
