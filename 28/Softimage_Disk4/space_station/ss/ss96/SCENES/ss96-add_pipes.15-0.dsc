SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.42-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       dowager_sPtL-inf_light5_1_2.42-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.42-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.42-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       add_pipes-mat105.1-0 ; 
       add_pipes-mat106.1-0 ; 
       add_pipes-mat107.1-0 ; 
       add_pipes-mat108.1-0 ; 
       add_pipes-mat109.1-0 ; 
       add_pipes-mat110.1-0 ; 
       add_pipes-mat111.1-0 ; 
       add_pipes-mat112.1-0 ; 
       add_pipes-mat113.1-0 ; 
       add_pipes-mat114.1-0 ; 
       add_pipes-mat115.1-0 ; 
       add_pipes-mat116.1-0 ; 
       add_pipes-mat117.1-0 ; 
       add_pipes-mat118.1-0 ; 
       add_pipes-mat119.1-0 ; 
       add_pipes-mat120.1-0 ; 
       add_pipes-mat121.1-0 ; 
       add_pipes-mat122.1-0 ; 
       add_pipes-mat123.1-0 ; 
       add_pipes-mat124.1-0 ; 
       add_pipes-mat125.1-0 ; 
       add_pipes-mat126.1-0 ; 
       add_pipes-mat3.1-0 ; 
       add_pipes-mat9.1-0 ; 
       dowager_sPtL-mat86.2-0 ; 
       dowager_sPtL-mat90.2-0 ; 
       dowager_sPtL-mat91.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       Nurbs3-contwr4.1-0 ; 
       Nurbs3-corrdr0.1-0 ; 
       Nurbs3-corrdr2.1-0 ; 
       Nurbs3-corrdr3.1-0 ; 
       Nurbs3-corrdr4.1-0 ; 
       Nurbs3-corrdr5.1-0 ; 
       Nurbs3-cyl1.1-0 ; 
       Nurbs3-cyl2.1-0 ; 
       Nurbs3-cyl3.1-0 ; 
       Nurbs3-cyl4.1-0 ; 
       Nurbs3-null2.1-0 ; 
       Nurbs3-Nurbs_Skin.42-0 ROOT ; 
       Nurbs3-pipe.5-0 ; 
       Nurbs3-pipe_1.7-0 ; 
       Nurbs3-pipe_2.6-0 ; 
       Nurbs3-pipe_3.6-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-add_pipes.15-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       add_pipes-t2d2.1-0 ; 
       add_pipes-t2d72.1-0 ; 
       add_pipes-t2d73.1-0 ; 
       add_pipes-t2d74.1-0 ; 
       add_pipes-t2d75.1-0 ; 
       add_pipes-t2d76.1-0 ; 
       add_pipes-t2d77.1-0 ; 
       add_pipes-t2d78.1-0 ; 
       add_pipes-t2d79.1-0 ; 
       add_pipes-t2d8.1-0 ; 
       add_pipes-t2d80.1-0 ; 
       add_pipes-t2d81.1-0 ; 
       add_pipes-t2d82.1-0 ; 
       add_pipes-t2d83.1-0 ; 
       add_pipes-t2d84.1-0 ; 
       add_pipes-t2d85.1-0 ; 
       add_pipes-t2d86.1-0 ; 
       add_pipes-t2d87.3-0 ; 
       add_pipes-t2d88.3-0 ; 
       add_pipes-t2d89.3-0 ; 
       add_pipes-t2d90.2-0 ; 
       dowager_sPtL-t2d59.5-0 ; 
       dowager_sPtL-t2d63.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       13 11 110 ; 
       0 10 110 ; 
       1 11 110 ; 
       15 11 110 ; 
       12 11 110 ; 
       14 11 110 ; 
       2 1 110 ; 
       10 11 110 ; 
       6 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 3 300 ; 
       7 17 300 ; 
       8 2 300 ; 
       8 15 300 ; 
       9 1 300 ; 
       9 14 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       13 19 300 ; 
       0 0 300 ; 
       15 21 300 ; 
       12 18 300 ; 
       14 20 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       11 22 300 ; 
       11 23 300 ; 
       6 4 300 ; 
       6 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       22 0 401 ; 
       23 9 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 19 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       20 18 401 ; 
       21 20 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 10 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 15 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 20 -8 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 35 -8 0 MPRFLG 0 ; 
       13 SCHEM -0.9895396 -6 0 USR MPRFLG 0 ; 
       0 SCHEM 40 -8 0 MPRFLG 0 ; 
       1 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       15 SCHEM -5.061164 -6 0 USR MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 3.614839 -6 0 USR MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 40 -6 0 MPRFLG 0 ; 
       11 SCHEM 32.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 58.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM -0.9895396 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 3.614839 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM -5.061164 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 3.614839 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM -0.9895396 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM -5.061164 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
