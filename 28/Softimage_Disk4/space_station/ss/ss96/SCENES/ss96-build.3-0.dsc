SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       build-cam_int1.3-0 ROOT ; 
       build-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       build-circle1.1-0 ROOT ; 
       build-spline1.1-0 ROOT ; 
       build1-circle2.1-0 ROOT ; 
       build11-circle2.1-0 ROOT ; 
       build13-circle2.1-0 ROOT ; 
       build14-circle2.1-0 ROOT ; 
       build15-circle2.1-0 ROOT ; 
       build3-circle2.1-0 ROOT ; 
       build5-circle2.1-0 ROOT ; 
       build6-circle2.1-0 ROOT ; 
       build7-circle2.1-0 ROOT ; 
       build9-circle2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-build.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 7.5 0 0 SRT 1 1 1 0 1.570796 0 -1 0 0 MPRFLG 0 ; 
       7 SCHEM 10 0 0 SRT 1 1 1 0 1.570796 0 -3 0 0 MPRFLG 0 ; 
       8 SCHEM 12.5 0 0 SRT 1 1 1 0 1.570796 0 -5 0 0 MPRFLG 0 ; 
       9 SCHEM 15 0 0 SRT 1 1 1 0 1.570796 0 -6 0 0 MPRFLG 0 ; 
       10 SCHEM 17.5 0 0 SRT 1 1 1 0 1.570796 0 1 0 0 MPRFLG 0 ; 
       11 SCHEM 20 0 0 SRT 1 1 1 0 1.570796 0 3 0 0 MPRFLG 0 ; 
       3 SCHEM 22.5 0 0 SRT 1 1 1 0 1.570796 0 5 0 0 MPRFLG 0 ; 
       4 SCHEM 25 0 0 SRT 1 1 1 0 1.570796 0 7 0 0 MPRFLG 0 ; 
       5 SCHEM 27.5 0 0 SRT 1 1 1 0 1.570796 0 7.262639 0 0 MPRFLG 0 ; 
       6 SCHEM 30 0 0 SRT 1 1 1 0 1.570796 0 -6.157302 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
