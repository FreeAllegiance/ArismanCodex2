SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       chain-cam_int1.1-0 ROOT ; 
       chain-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 18     
       chain-inf_light1.1-0 ROOT ; 
       chain-inf_light3.1-0 ROOT ; 
       chain-spot1.1-0 ; 
       chain-spot1_int.1-0 ROOT ; 
       chain-spot2.1-0 ; 
       chain-spot2_int1.1-0 ROOT ; 
       chain-spot3.1-0 ; 
       chain-spot3_int1.1-0 ROOT ; 
       chain-spot4.1-0 ; 
       chain-spot4_int1.1-0 ROOT ; 
       chain-spot5.1-0 ; 
       chain-spot5_int1.1-0 ROOT ; 
       chain-spot6.1-0 ; 
       chain-spot6_int1.1-0 ROOT ; 
       chain-spot7.1-0 ; 
       chain-spot7_int1.1-0 ROOT ; 
       chain-spot8.1-0 ; 
       chain-spot8_int1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       chain-mat1.1-0 ; 
       chain-mat2.1-0 ; 
       chain-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       chain-circle2.1-0 ; 
       chain-circle2_1.1-0 ; 
       chain-circle2_2.1-0 ; 
       chain-circle2_3.1-0 ; 
       chain-circle2_4.1-0 ; 
       chain-circle2_5.1-0 ; 
       chain-circle2_6.1-0 ; 
       chain-circle2_7.1-0 ; 
       chain-circle2_8.1-0 ; 
       chain-circle2_9.1-0 ; 
       chain-null1.1-0 ROOT ; 
       chain-YouMap1.1-0 ROOT ; 
       Nurbs-Nurbs_Skin.21-0 ROOT ; 
       Nurbs1-Nurbs_Skin.9-0 ROOT ; 
       Nurbs3-Nurbs_Skin.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 6     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/baseskin ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/crewlights ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/rendermap ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/rendermap_light ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/sidelights ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-chain.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       chain-crewlights1.1-0 ; 
       chain-lights1.1-0 ; 
       chain-lights2.1-0 ; 
       chain-Rendermap1.1-0 ; 
       chain-skin.1-0 ; 
       chain-t2d1.1-0 ; 
       chain-t2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       14 12 20000 RELDATA 2 1 11 1 2 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       14 12 20000 2 RELDATA 2 2 11 1 1 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       14 11 20000 4 RELDATA 2 4 11 1 5 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       14 12 20000 RELDATA 1 1 11 2 2 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       14 12 20000 2 RELDATA 1 2 11 2 1 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       14 11 20000 4 RELDATA 1 4 11 2 5 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       0 10 110 ; 
       1 10 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       9 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       12 0 300 ; 
       13 1 300 ; 
       14 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       12 4 400 ; 
       12 1 400 ; 
       12 2 400 ; 
       12 0 400 ; 
       14 6 20000 3 RELDATA 2 3 11 1 0 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       14 6 20000 3 RELDATA 1 3 11 2 0 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
       4 5 2110 ; 
       6 7 2110 ; 
       8 9 2110 ; 
       10 11 2110 ; 
       12 13 2110 ; 
       14 15 2110 ; 
       16 17 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 5 401 ; 
       2 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 50 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 55 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 60 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 65 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       12 SCHEM 32.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 -0.009574085 MPRFLG 0 ; 
       13 SCHEM 40 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 0 0 -0.009574085 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 13.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 70 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 37.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 35 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
