SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.97-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       dowager_sPtL-inf_light5_1_2.90-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.90-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.90-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       done_text-mat151.3-0 ; 
       done_text-mat162.3-0 ; 
       done_text-mat164.3-0 ; 
       done_text-mat165.3-0 ; 
       done_text-mat166.3-0 ; 
       done_text-mat167.3-0 ; 
       dowager_sPtL-mat1.3-0 ; 
       dowager_sPtL-mat14.5-0 ; 
       dowager_sPtL-mat2.3-0 ; 
       dowager_sPtL-mat3.3-0 ; 
       dowager_sPtL-mat4.6-0 ; 
       dowager_sPtL-mat60.3-0 ; 
       dowager_sPtL-mat62.3-0 ; 
       dowager_sPtL-mat86.8-0 ; 
       dowager_sPtL-mat90.8-0 ; 
       dowager_sPtL-mat91.8-0 ; 
       frames-bottom1.1-0 ; 
       frames-mat168.1-0 ; 
       frames-mat186.1-0 ; 
       frames-mat187.1-0 ; 
       frames-mat188.1-0 ; 
       frames-mat189.1-0 ; 
       frames-mat190.1-0 ; 
       frames-mat191.1-0 ; 
       frames-mat192.1-0 ; 
       frames-mat193.1-0 ; 
       frames-mat194.1-0 ; 
       frames-mat195.1-0 ; 
       frames-mat196.1-0 ; 
       frames-mat197.1-0 ; 
       frames-mat198.1-0 ; 
       frames-mat199.1-0 ; 
       frames-mat200.1-0 ; 
       frames-mat201.1-0 ; 
       frames-mat202.1-0 ; 
       frames-mat203.1-0 ; 
       frames-mat204.1-0 ; 
       frames-mat205.1-0 ; 
       frames-mat206.1-0 ; 
       frames-mat207.1-0 ; 
       frames-side1.1-0 ; 
       frames-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       frames-bay1.1-0 ; 
       frames-contwr1.1-0 ; 
       frames-corrdr2.1-0 ; 
       frames-corrdr4.1-0 ; 
       frames-cube1.2-0 ; 
       frames-fuselg1.1-0 ; 
       frames-fuselg10.1-0 ; 
       frames-fuselg15.1-0 ; 
       frames-fuselg16.1-0 ; 
       frames-fuselg17.1-0 ; 
       frames-fuselg18.1-0 ; 
       frames-fuselg3.1-0 ; 
       frames-fuselg7.1-0 ; 
       frames-garage1A.1-0 ; 
       frames-garage1B.1-0 ; 
       frames-garage1C.1-0 ; 
       frames-garage1D.1-0 ; 
       frames-garage1E.1-0 ; 
       frames-null1.1-0 ; 
       frames-platfrm1.1-0 ; 
       frames-root_1.1-0 ; 
       frames-root_5.1-0 ; 
       frames-root_6.1-0 ; 
       frames-ss01_2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/rixbay ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-frames.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       done_text-t2d15.8-0 ; 
       done_text-t2d17.8-0 ; 
       done_text-t2d18.8-0 ; 
       done_text-t2d19.8-0 ; 
       done_text-t2d20.8-0 ; 
       done_text-t2d3.6-0 ; 
       dowager_sPtL-t2d1.6-0 ; 
       dowager_sPtL-t2d11.8-0 ; 
       dowager_sPtL-t2d2.6-0 ; 
       dowager_sPtL-t2d46.5-0 ; 
       dowager_sPtL-t2d59_1.8-0 ; 
       dowager_sPtL-t2d63_1.8-0 ; 
       frames-t2d100.1-0 ; 
       frames-t2d101.1-0 ; 
       frames-t2d59_2.1-0 ; 
       frames-t2d63_2.1-0 ; 
       frames-t2d64.1-0 ; 
       frames-t2d65.1-0 ; 
       frames-t2d66.1-0 ; 
       frames-t2d67.1-0 ; 
       frames-t2d68.1-0 ; 
       frames-t2d83.1-0 ; 
       frames-t2d84.1-0 ; 
       frames-t2d85.1-0 ; 
       frames-t2d86.1-0 ; 
       frames-t2d87.1-0 ; 
       frames-t2d88.1-0 ; 
       frames-t2d89.1-0 ; 
       frames-t2d90.1-0 ; 
       frames-t2d91.1-0 ; 
       frames-t2d92.1-0 ; 
       frames-t2d93.1-0 ; 
       frames-t2d94.1-0 ; 
       frames-t2d95.1-0 ; 
       frames-t2d96.1-0 ; 
       frames-t2d97.1-0 ; 
       frames-t2d98.1-0 ; 
       frames-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 18 110 ; 
       5 23 110 ; 
       6 20 110 ; 
       7 21 110 ; 
       8 21 110 ; 
       9 22 110 ; 
       10 22 110 ; 
       11 19 110 ; 
       12 20 110 ; 
       18 19 110 ; 
       19 23 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 4 110 ; 
       0 23 110 ; 
       14 0 110 ; 
       13 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 7 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       5 6 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       6 17 300 ; 
       7 30 300 ; 
       8 31 300 ; 
       9 37 300 ; 
       10 38 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       12 0 300 ; 
       19 10 300 ; 
       19 41 300 ; 
       19 40 300 ; 
       19 16 300 ; 
       19 39 300 ; 
       20 1 300 ; 
       20 2 300 ; 
       20 3 300 ; 
       20 4 300 ; 
       20 5 300 ; 
       21 25 300 ; 
       21 26 300 ; 
       21 27 300 ; 
       21 28 300 ; 
       21 29 300 ; 
       22 32 300 ; 
       22 33 300 ; 
       22 34 300 ; 
       22 35 300 ; 
       22 36 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 20 400 ; 
       7 30 400 ; 
       8 31 400 ; 
       9 37 400 ; 
       10 12 400 ; 
       12 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 7 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 16 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 19 401 ; 
       18 21 401 ; 
       19 22 401 ; 
       20 23 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       39 13 401 ; 
       40 18 401 ; 
       41 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 40 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 42.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 45 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 13.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 15 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 20 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 13.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 15 -2 0 MPRFLG 0 ; 
       20 SCHEM 21.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 11.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 16.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 32.44069 -2 0 USR DISPLAY 3 2 MPRFLG 0 ; 
       14 SCHEM 29.94069 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 27.44069 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 32.44069 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 34.94069 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 37.44069 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 24 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 24 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 24 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 4 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 29 -4 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 24 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 24 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 26.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 21.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 9 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 11.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 19 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 19 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 19 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 19 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 14 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 16.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 26.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 26.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 26.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 24 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 24 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 24 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 24 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 19 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 24 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 16.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 26.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 6.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 6.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 29 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 26.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 26.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 26.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 21.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 24 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 4 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 6.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 9 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 19 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 19 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 19 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 19 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 19 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
