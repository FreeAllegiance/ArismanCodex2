SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.92-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       dowager_sPtL-inf_light5_1_2.85-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.85-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.85-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       done_text-mat151.3-0 ; 
       done_text-mat162.3-0 ; 
       done_text-mat164.3-0 ; 
       done_text-mat165.3-0 ; 
       done_text-mat166.3-0 ; 
       done_text-mat167.3-0 ; 
       dowager_sPtL-mat1.3-0 ; 
       dowager_sPtL-mat14.5-0 ; 
       dowager_sPtL-mat2.3-0 ; 
       dowager_sPtL-mat3.3-0 ; 
       dowager_sPtL-mat4.6-0 ; 
       dowager_sPtL-mat60.3-0 ; 
       dowager_sPtL-mat62.3-0 ; 
       dowager_sPtL-mat86.8-0 ; 
       dowager_sPtL-mat90.8-0 ; 
       dowager_sPtL-mat91.8-0 ; 
       garage-bottom1.1-0 ; 
       garage-mat168.1-0 ; 
       garage-mat186.1-0 ; 
       garage-mat187.1-0 ; 
       garage-mat188.1-0 ; 
       garage-mat189.1-0 ; 
       garage-mat190.1-0 ; 
       garage-mat191.1-0 ; 
       garage-mat192.1-0 ; 
       garage-mat193.1-0 ; 
       garage-mat194.1-0 ; 
       garage-mat195.1-0 ; 
       garage-mat196.1-0 ; 
       garage-mat197.1-0 ; 
       garage-mat198.1-0 ; 
       garage-mat199.1-0 ; 
       garage-mat200.1-0 ; 
       garage-mat201.1-0 ; 
       garage-mat202.1-0 ; 
       garage-mat203.1-0 ; 
       garage-mat204.1-0 ; 
       garage-mat205.1-0 ; 
       garage-mat206.1-0 ; 
       garage-mat207.1-0 ; 
       garage-side1.1-0 ; 
       garage-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       ss01-contwr1.1-0 ; 
       ss01-corrdr2.1-0 ; 
       ss01-corrdr4.1-0 ; 
       ss01-cube1.2-0 ; 
       ss01-fuselg1.1-0 ; 
       ss01-fuselg10.1-0 ; 
       ss01-fuselg15.1-0 ; 
       ss01-fuselg16.1-0 ; 
       ss01-fuselg17.1-0 ; 
       ss01-fuselg18.1-0 ; 
       ss01-fuselg3.1-0 ; 
       ss01-fuselg7.1-0 ; 
       ss01-null1.1-0 ; 
       ss01-platfrm1.1-0 ; 
       ss01-root_1.1-0 ; 
       ss01-root_5.1-0 ; 
       ss01-root_6.1-0 ; 
       ss01-ss01_2.33-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/cwbay ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-garage.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       done_text-t2d15.8-0 ; 
       done_text-t2d17.8-0 ; 
       done_text-t2d18.8-0 ; 
       done_text-t2d19.8-0 ; 
       done_text-t2d20.8-0 ; 
       done_text-t2d3.6-0 ; 
       dowager_sPtL-t2d1.6-0 ; 
       dowager_sPtL-t2d11.8-0 ; 
       dowager_sPtL-t2d2.6-0 ; 
       dowager_sPtL-t2d46.5-0 ; 
       dowager_sPtL-t2d59_1.8-0 ; 
       dowager_sPtL-t2d63_1.8-0 ; 
       garage-t2d100.1-0 ; 
       garage-t2d101.1-0 ; 
       garage-t2d59_2.1-0 ; 
       garage-t2d63_2.1-0 ; 
       garage-t2d64.1-0 ; 
       garage-t2d65.1-0 ; 
       garage-t2d66.1-0 ; 
       garage-t2d67.1-0 ; 
       garage-t2d68.1-0 ; 
       garage-t2d83.1-0 ; 
       garage-t2d84.1-0 ; 
       garage-t2d85.1-0 ; 
       garage-t2d86.1-0 ; 
       garage-t2d87.1-0 ; 
       garage-t2d88.1-0 ; 
       garage-t2d89.1-0 ; 
       garage-t2d90.1-0 ; 
       garage-t2d91.1-0 ; 
       garage-t2d92.1-0 ; 
       garage-t2d93.1-0 ; 
       garage-t2d94.1-0 ; 
       garage-t2d95.1-0 ; 
       garage-t2d96.1-0 ; 
       garage-t2d97.1-0 ; 
       garage-t2d98.1-0 ; 
       garage-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       3 12 110 ; 
       4 17 110 ; 
       5 14 110 ; 
       6 15 110 ; 
       7 15 110 ; 
       8 16 110 ; 
       9 16 110 ; 
       10 13 110 ; 
       11 14 110 ; 
       12 13 110 ; 
       13 17 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       1 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       4 6 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       5 17 300 ; 
       6 30 300 ; 
       7 31 300 ; 
       8 37 300 ; 
       9 38 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       11 0 300 ; 
       13 10 300 ; 
       13 41 300 ; 
       13 40 300 ; 
       13 16 300 ; 
       13 39 300 ; 
       14 1 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 27 300 ; 
       15 28 300 ; 
       15 29 300 ; 
       16 32 300 ; 
       16 33 300 ; 
       16 34 300 ; 
       16 35 300 ; 
       16 36 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 20 400 ; 
       6 30 400 ; 
       7 31 400 ; 
       8 37 400 ; 
       9 12 400 ; 
       11 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 7 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 16 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 19 401 ; 
       18 21 401 ; 
       19 22 401 ; 
       20 23 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       40 18 401 ; 
       41 17 401 ; 
       39 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -8 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 10 -10 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 15 -10 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 MPRFLG 0 ; 
       11 SCHEM 20 -10 0 MPRFLG 0 ; 
       12 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 15 -2 0 MPRFLG 0 ; 
       14 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       15 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       16 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       17 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 35 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 37.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
