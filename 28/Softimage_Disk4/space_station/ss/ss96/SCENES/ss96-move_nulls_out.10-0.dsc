SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.125-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 63     
       done_text-mat151.4-0 ; 
       done_text-mat162.4-0 ; 
       done_text-mat164.4-0 ; 
       done_text-mat165.4-0 ; 
       done_text-mat166.4-0 ; 
       done_text-mat167.4-0 ; 
       dowager_sPtL-mat1.5-0 ; 
       dowager_sPtL-mat14.7-0 ; 
       dowager_sPtL-mat2.5-0 ; 
       dowager_sPtL-mat3.5-0 ; 
       dowager_sPtL-mat4.7-0 ; 
       dowager_sPtL-mat60.4-0 ; 
       dowager_sPtL-mat62.4-0 ; 
       dowager_sPtL-mat86.9-0 ; 
       dowager_sPtL-mat90.9-0 ; 
       dowager_sPtL-mat91.9-0 ; 
       move_nulls_out-mat337.2-0 ; 
       move_nulls_out-mat338.2-0 ; 
       move_nulls_out-mat339.2-0 ; 
       move_nulls_out-mat340.2-0 ; 
       move_nulls_out-mat341.2-0 ; 
       move_nulls_out-mat342.2-0 ; 
       move_nulls_out-mat343.2-0 ; 
       move_nulls_out-mat344.2-0 ; 
       move_nulls_out-mat345.2-0 ; 
       move_nulls_out-mat346.2-0 ; 
       strobes-bottom1.2-0 ; 
       strobes-mat168.2-0 ; 
       strobes-mat186.2-0 ; 
       strobes-mat187.2-0 ; 
       strobes-mat188.2-0 ; 
       strobes-mat189.2-0 ; 
       strobes-mat190.2-0 ; 
       strobes-mat191.2-0 ; 
       strobes-mat192.2-0 ; 
       strobes-mat193.2-0 ; 
       strobes-mat194.2-0 ; 
       strobes-mat195.2-0 ; 
       strobes-mat196.2-0 ; 
       strobes-mat197.2-0 ; 
       strobes-mat198.2-0 ; 
       strobes-mat199.2-0 ; 
       strobes-mat200.2-0 ; 
       strobes-mat201.2-0 ; 
       strobes-mat202.2-0 ; 
       strobes-mat203.2-0 ; 
       strobes-mat204.2-0 ; 
       strobes-mat205.2-0 ; 
       strobes-mat206.2-0 ; 
       strobes-mat207.2-0 ; 
       strobes-mat208.2-0 ; 
       strobes-mat209.2-0 ; 
       strobes-mat210.2-0 ; 
       strobes-mat211.2-0 ; 
       strobes-mat212.2-0 ; 
       strobes-mat213.2-0 ; 
       strobes-mat214.2-0 ; 
       strobes-mat215.2-0 ; 
       strobes-mat216.2-0 ; 
       strobes-mat217.2-0 ; 
       strobes-mat218.2-0 ; 
       strobes-side1.2-0 ; 
       strobes-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 67     
       strobes-bay1.1-0 ; 
       strobes-bay2.1-0 ; 
       strobes-bay3.1-0 ; 
       strobes-bay4.1-0 ; 
       strobes-contwr1.1-0 ; 
       strobes-corrdr2.1-0 ; 
       strobes-corrdr4.1-0 ; 
       strobes-cube1.2-0 ; 
       strobes-fuselg1.1-0 ; 
       strobes-fuselg10.1-0 ; 
       strobes-fuselg15.1-0 ; 
       strobes-fuselg16.1-0 ; 
       strobes-fuselg17.1-0 ; 
       strobes-fuselg18.1-0 ; 
       strobes-fuselg3.1-0 ; 
       strobes-fuselg7.1-0 ; 
       strobes-garage1A.1-0 ; 
       strobes-garage1B.1-0 ; 
       strobes-garage1C.1-0 ; 
       strobes-garage1D.1-0 ; 
       strobes-garage1E.1-0 ; 
       strobes-garage2A.1-0 ; 
       strobes-garage2B.1-0 ; 
       strobes-garage2C.1-0 ; 
       strobes-garage2D.1-0 ; 
       strobes-garage2E.1-0 ; 
       strobes-garage3A.1-0 ; 
       strobes-garage3B.1-0 ; 
       strobes-garage3C.1-0 ; 
       strobes-garage3D.1-0 ; 
       strobes-garage3E.1-0 ; 
       strobes-garage4A.1-0 ; 
       strobes-garage4B.1-0 ; 
       strobes-garage4C.1-0 ; 
       strobes-garage4D.1-0 ; 
       strobes-garage4E.1-0 ; 
       strobes-launch1.1-0 ; 
       strobes-launch2.1-0 ; 
       strobes-null1.1-0 ; 
       strobes-null2.1-0 ; 
       strobes-null3.1-0 ; 
       strobes-platfrm1.1-0 ; 
       strobes-root_1.1-0 ; 
       strobes-root_5.1-0 ; 
       strobes-root_6.1-0 ; 
       strobes-ss01.1-0 ; 
       strobes-ss01_2.19-0 ROOT ; 
       strobes-ss10.1-0 ; 
       strobes-ss11.1-0 ; 
       strobes-ss12.1-0 ; 
       strobes-ss13.1-0 ; 
       strobes-ss14.1-0 ; 
       strobes-ss15.1-0 ; 
       strobes-ss16.1-0 ; 
       strobes-ss17.1-0 ; 
       strobes-ss18.1-0 ; 
       strobes-ss19.1-0 ; 
       strobes-ss2.1-0 ; 
       strobes-ss20.1-0 ; 
       strobes-ss21.1-0 ; 
       strobes-ss3.1-0 ; 
       strobes-ss4.1-0 ; 
       strobes-ss5.1-0 ; 
       strobes-ss6.1-0 ; 
       strobes-ss7.1-0 ; 
       strobes-ss8.1-0 ; 
       strobes-ss9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss96/PICTURES/rixbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-move_nulls_out.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       done_text-t2d15.10-0 ; 
       done_text-t2d17.10-0 ; 
       done_text-t2d18.10-0 ; 
       done_text-t2d19.10-0 ; 
       done_text-t2d20.10-0 ; 
       done_text-t2d3.8-0 ; 
       dowager_sPtL-t2d1.8-0 ; 
       dowager_sPtL-t2d11.10-0 ; 
       dowager_sPtL-t2d2.8-0 ; 
       dowager_sPtL-t2d46.7-0 ; 
       dowager_sPtL-t2d59_1.10-0 ; 
       dowager_sPtL-t2d63_1.10-0 ; 
       strobes-t2d100.3-0 ; 
       strobes-t2d101.4-0 ; 
       strobes-t2d59_2.3-0 ; 
       strobes-t2d63_2.3-0 ; 
       strobes-t2d64.4-0 ; 
       strobes-t2d65.4-0 ; 
       strobes-t2d66.4-0 ; 
       strobes-t2d67.4-0 ; 
       strobes-t2d68.3-0 ; 
       strobes-t2d83.3-0 ; 
       strobes-t2d84.3-0 ; 
       strobes-t2d85.3-0 ; 
       strobes-t2d86.3-0 ; 
       strobes-t2d87.3-0 ; 
       strobes-t2d88.3-0 ; 
       strobes-t2d89.3-0 ; 
       strobes-t2d90.3-0 ; 
       strobes-t2d91.3-0 ; 
       strobes-t2d92.3-0 ; 
       strobes-t2d93.3-0 ; 
       strobes-t2d94.3-0 ; 
       strobes-t2d95.3-0 ; 
       strobes-t2d96.3-0 ; 
       strobes-t2d97.3-0 ; 
       strobes-t2d98.3-0 ; 
       strobes-t2d99.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 46 110 ; 
       1 46 110 ; 
       2 46 110 ; 
       3 46 110 ; 
       4 8 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       7 38 110 ; 
       8 46 110 ; 
       9 42 110 ; 
       10 43 110 ; 
       11 43 110 ; 
       12 44 110 ; 
       13 44 110 ; 
       14 41 110 ; 
       15 42 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 3 110 ; 
       32 3 110 ; 
       33 3 110 ; 
       34 3 110 ; 
       35 3 110 ; 
       36 46 110 ; 
       37 46 110 ; 
       38 41 110 ; 
       39 46 110 ; 
       40 46 110 ; 
       41 46 110 ; 
       42 7 110 ; 
       43 7 110 ; 
       44 7 110 ; 
       45 46 110 ; 
       47 46 110 ; 
       48 46 110 ; 
       49 39 110 ; 
       50 39 110 ; 
       51 39 110 ; 
       52 39 110 ; 
       53 39 110 ; 
       54 40 110 ; 
       55 40 110 ; 
       56 40 110 ; 
       57 46 110 ; 
       58 40 110 ; 
       59 40 110 ; 
       60 46 110 ; 
       61 46 110 ; 
       62 46 110 ; 
       63 46 110 ; 
       64 46 110 ; 
       65 46 110 ; 
       66 46 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 7 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       7 28 300 ; 
       7 29 300 ; 
       8 6 300 ; 
       8 8 300 ; 
       8 9 300 ; 
       9 27 300 ; 
       10 40 300 ; 
       11 41 300 ; 
       12 47 300 ; 
       13 48 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       15 0 300 ; 
       41 10 300 ; 
       41 62 300 ; 
       41 61 300 ; 
       41 26 300 ; 
       41 49 300 ; 
       42 1 300 ; 
       42 2 300 ; 
       42 3 300 ; 
       42 4 300 ; 
       42 5 300 ; 
       43 35 300 ; 
       43 36 300 ; 
       43 37 300 ; 
       43 38 300 ; 
       43 39 300 ; 
       44 42 300 ; 
       44 43 300 ; 
       44 44 300 ; 
       44 45 300 ; 
       44 46 300 ; 
       45 50 300 ; 
       47 59 300 ; 
       48 60 300 ; 
       49 16 300 ; 
       50 17 300 ; 
       51 18 300 ; 
       52 19 300 ; 
       53 20 300 ; 
       54 21 300 ; 
       55 22 300 ; 
       56 23 300 ; 
       57 51 300 ; 
       58 24 300 ; 
       59 25 300 ; 
       60 52 300 ; 
       61 53 300 ; 
       62 54 300 ; 
       63 55 300 ; 
       64 56 300 ; 
       65 57 300 ; 
       66 58 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       9 20 400 ; 
       10 30 400 ; 
       11 31 400 ; 
       12 37 400 ; 
       13 12 400 ; 
       15 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 7 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 16 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       26 19 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       32 14 401 ; 
       33 15 401 ; 
       34 24 401 ; 
       35 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 28 401 ; 
       39 29 401 ; 
       42 32 401 ; 
       43 33 401 ; 
       44 34 401 ; 
       45 35 401 ; 
       46 36 401 ; 
       49 13 401 ; 
       61 18 401 ; 
       62 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 50 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 120.3351 -1.916924 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 132 0 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 11.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 20 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 7.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 10 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 12.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 15 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 17.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 32.5 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 35 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 37.5 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 40 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 42.5 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 45 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 47.5 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 50 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 52.5 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 55 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 115.3351 -3.916924 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 120.3351 -3.916924 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 117.8351 -3.916924 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 125.3351 -3.916924 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 122.8351 -3.916924 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 127 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 132 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 129.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 137 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 134.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 80 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 82.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 11.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 90 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 102.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 18.75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 8.75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 13.75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 60 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 55 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       47 SCHEM 72.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 75 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 85 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 87.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 90 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 92.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 95 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 107.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 105 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 102.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 57.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 100 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 97.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 62.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       61 SCHEM 65 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       62 SCHEM 67.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       63 SCHEM 70 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       64 SCHEM 30 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       65 SCHEM 25 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       66 SCHEM 77.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 21.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 21.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 21.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 29 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 26.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 29 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 29 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 49 -4 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 21.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 21.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 1.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 1.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 86.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 89 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 91.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 106.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 104 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 101.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 99 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 96.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 19 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 1.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 6.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 9 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 16.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 16.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 16.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 16.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 16.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 11.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 14 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 24 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 59 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 56.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 64 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 66.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 69 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 29 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 24 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 76.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 71.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 74 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 24 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 24 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 21.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 21.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 21.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 21.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 16.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 1.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 24 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 4 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 4 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 24 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 24 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 24 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 19 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 1.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 9 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 16.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 16.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 16.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 16.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 16.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
