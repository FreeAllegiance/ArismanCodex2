SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.55-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       dowager_sPtL-inf_light5_1_2.50-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.50-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.50-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       add_frames-front_of_garages1.2-0 ; 
       add_frames-side_panels1.2-0 ; 
       done_text-mat151.2-0 ; 
       done_text-mat153.2-0 ; 
       done_text-mat162.2-0 ; 
       done_text-mat164.2-0 ; 
       done_text-mat165.2-0 ; 
       done_text-mat166.2-0 ; 
       done_text-mat167.2-0 ; 
       done_text-mat168.2-0 ; 
       done_text-mat169.2-0 ; 
       done_text-mat170.2-0 ; 
       done_text-mat171.2-0 ; 
       done_text-mat172.2-0 ; 
       done_text-mat173.2-0 ; 
       done_text-mat174.2-0 ; 
       dowager_sPtL-mat1.2-0 ; 
       dowager_sPtL-mat14.3-0 ; 
       dowager_sPtL-mat2.2-0 ; 
       dowager_sPtL-mat3.2-0 ; 
       dowager_sPtL-mat4.2-0 ; 
       dowager_sPtL-mat51.2-0 ; 
       dowager_sPtL-mat52.2-0 ; 
       dowager_sPtL-mat60.2-0 ; 
       dowager_sPtL-mat62.2-0 ; 
       dowager_sPtL-mat7.2-0 ; 
       dowager_sPtL-mat86.5-0 ; 
       dowager_sPtL-mat90.5-0 ; 
       dowager_sPtL-mat91.5-0 ; 
       new-mat175.1-0 ; 
       new-mat176.1-0 ; 
       new-mat177.1-0 ; 
       new-mat178.1-0 ; 
       new-mat179.1-0 ; 
       new-mat180.1-0 ; 
       new-mat181.1-0 ; 
       new-mat182.1-0 ; 
       new-mat183.1-0 ; 
       new-mat184.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       new-cube1.1-0 ROOT ; 
       ss01-contwr1.1-0 ; 
       ss01-corrdr2.1-0 ; 
       ss01-corrdr2_1.1-0 ; 
       ss01-fuselg1.1-0 ; 
       ss01-fuselg3.1-0 ; 
       ss01-fuselg7.1-0 ; 
       ss01-fuselg7_1.1-0 ; 
       ss01-fuselg7_1_1.1-0 ; 
       ss01-fuselg9.1-0 ; 
       ss01-fuselg9_1.1-0 ; 
       ss01-fuselg9_1_1.1-0 ; 
       ss01-null1.1-0 ; 
       ss01-platfrm1.1-0 ; 
       ss01-root_1.1-0 ; 
       ss01-root_1_1.1-0 ; 
       ss01-root_1_1_1.1-0 ; 
       ss01-ss01_2.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss01 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss98 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-new.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       add_frames-t2d65.2-0 ; 
       add_frames-t2d66.2-0 ; 
       done_text-t2d15.2-0 ; 
       done_text-t2d17.2-0 ; 
       done_text-t2d18.2-0 ; 
       done_text-t2d19.2-0 ; 
       done_text-t2d20.2-0 ; 
       done_text-t2d21.2-0 ; 
       done_text-t2d22.2-0 ; 
       done_text-t2d23.2-0 ; 
       done_text-t2d24.2-0 ; 
       done_text-t2d25.2-0 ; 
       done_text-t2d26.2-0 ; 
       done_text-t2d27.2-0 ; 
       done_text-t2d3.2-0 ; 
       done_text-t2d5.2-0 ; 
       dowager_sPtL-t2d1.2-0 ; 
       dowager_sPtL-t2d11.3-0 ; 
       dowager_sPtL-t2d2.2-0 ; 
       dowager_sPtL-t2d40.2-0 ; 
       dowager_sPtL-t2d41.2-0 ; 
       dowager_sPtL-t2d46.2-0 ; 
       dowager_sPtL-t2d5.2-0 ; 
       dowager_sPtL-t2d59_1.2-0 ; 
       dowager_sPtL-t2d63_1.2-0 ; 
       new-t2d59_2.1-0 ; 
       new-t2d63_2.1-0 ; 
       new-t2d83.2-0 ; 
       new-t2d84.2-0 ; 
       new-t2d85.2-0 ; 
       new-t2d86.2-0 ; 
       new-t2d87.2-0 ; 
       new-t2d88.2-0 ; 
       new-t2d89.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       15 12 110 ; 
       3 12 110 ; 
       2 12 110 ; 
       12 13 110 ; 
       10 15 110 ; 
       8 15 110 ; 
       1 4 110 ; 
       6 14 110 ; 
       7 16 110 ; 
       9 14 110 ; 
       11 16 110 ; 
       14 12 110 ; 
       16 12 110 ; 
       4 17 110 ; 
       5 13 110 ; 
       13 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       15 29 300 ; 
       15 30 300 ; 
       15 31 300 ; 
       15 32 300 ; 
       15 33 300 ; 
       3 36 300 ; 
       3 37 300 ; 
       3 38 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       10 34 300 ; 
       8 35 300 ; 
       1 17 300 ; 
       6 2 300 ; 
       7 15 300 ; 
       9 3 300 ; 
       11 14 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       14 6 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       16 9 300 ; 
       16 10 300 ; 
       16 11 300 ; 
       16 12 300 ; 
       16 13 300 ; 
       4 16 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       13 20 300 ; 
       13 25 300 ; 
       13 21 300 ; 
       13 22 300 ; 
       13 0 300 ; 
       13 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 32 400 ; 
       8 33 400 ; 
       6 14 400 ; 
       7 13 400 ; 
       9 15 400 ; 
       11 12 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       29 27 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       17 17 401 ; 
       37 25 401 ; 
       38 26 401 ; 
       18 16 401 ; 
       19 18 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       27 23 401 ; 
       28 24 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       15 SCHEM 6.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 20 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 13.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 35 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 15 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 10 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 16.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 11.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 13.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 -1.570796 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       29 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 154.75 -4 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 156.25 -4 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 1.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 4 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 4 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 157.75 -4 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 151.75 -4 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 153.25 -4 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 19 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 19 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 19 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 6.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 24 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 24 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 150.25 -4 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 19 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 14 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 153.75 -6 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 155.25 -6 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 1.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 150.75 -6 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 152.25 -6 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 4 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 24 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 149.25 -6 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 19 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 19 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 19 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 14 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 14 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 16.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 14 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
