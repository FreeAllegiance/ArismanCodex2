SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.18-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       poly-mat3.3-0 ; 
       poly-mat4.1-0 ; 
       poly-mat6.1-0 ; 
       poly-mat7.1-0 ; 
       poly-mat8.1-0 ; 
       poly-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       Nurbs3-cube5.3-0 ; 
       Nurbs3-cube6.2-0 ; 
       Nurbs3-cube7.2-0 ; 
       Nurbs3-cube8.2-0 ; 
       Nurbs3-Nurbs_Skin.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-poly.18-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       poly-t2d2.4-0 ; 
       poly-t2d3.2-0 ; 
       poly-t2d5.2-0 ; 
       poly-t2d6.2-0 ; 
       poly-t2d7.2-0 ; 
       poly-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 0 300 ; 
       4 5 300 ; 
       0 1 300 ; 
       1 2 300 ; 
       2 3 300 ; 
       3 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       1 2 400 ; 
       2 3 400 ; 
       3 4 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       5 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 -0.009574085 MPRFLG 0 ; 
       0 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
