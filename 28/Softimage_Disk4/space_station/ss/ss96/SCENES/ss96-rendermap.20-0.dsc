SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rendermap-cam_int1.20-0 ROOT ; 
       rendermap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 18     
       rendermap-inf_light1.20-0 ROOT ; 
       rendermap-inf_light3.20-0 ROOT ; 
       rendermap-spot1.1-0 ; 
       rendermap-spot1_int.16-0 ROOT ; 
       rendermap-spot2.1-0 ; 
       rendermap-spot2_int1.16-0 ROOT ; 
       rendermap-spot3.1-0 ; 
       rendermap-spot3_int1.16-0 ROOT ; 
       rendermap-spot4.1-0 ; 
       rendermap-spot4_int1.16-0 ROOT ; 
       rendermap-spot5.1-0 ; 
       rendermap-spot5_int1.16-0 ROOT ; 
       rendermap-spot6.1-0 ; 
       rendermap-spot6_int1.16-0 ROOT ; 
       rendermap-spot7.1-0 ; 
       rendermap-spot7_int1.16-0 ROOT ; 
       rendermap-spot8.1-0 ; 
       rendermap-spot8_int1.16-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       rendermap-mat1.6-0 ; 
       rendermap-mat2.3-0 ; 
       rendermap-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       Nurbs-Nurbs_Skin.18-0 ROOT ; 
       Nurbs1-Nurbs_Skin.8-0 ROOT ; 
       Nurbs3-Nurbs_Skin.3-0 ROOT ; 
       rendermap-circle2.1-0 ; 
       rendermap-circle2_1.1-0 ; 
       rendermap-circle2_2.1-0 ; 
       rendermap-circle2_3.1-0 ; 
       rendermap-circle2_4.1-0 ; 
       rendermap-circle2_5.1-0 ; 
       rendermap-circle2_6.1-0 ; 
       rendermap-circle2_7.1-0 ; 
       rendermap-circle2_8.1-0 ; 
       rendermap-circle2_9.1-0 ; 
       rendermap-null1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/baseskin ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/crewlights ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/rendermap ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/sidelights ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-rendermap.20-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       rendermap-crewlights1.5-0 ; 
       rendermap-lights1.7-0 ; 
       rendermap-lights2.7-0 ; 
       rendermap-Rendermap1.10-0 ; 
       rendermap-skin.10-0 ; 
       rendermap-t2d1.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 13 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       10 13 110 ; 
       11 13 110 ; 
       12 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
       2 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 4 400 ; 
       0 1 400 ; 
       0 2 400 ; 
       0 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
       4 5 2110 ; 
       6 7 2110 ; 
       8 9 2110 ; 
       10 11 2110 ; 
       12 13 2110 ; 
       14 15 2110 ; 
       16 17 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 50 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 55 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 60 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 65 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 -0.009574085 MPRFLG 0 ; 
       1 SCHEM 40 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 13.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 42.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 -0.009574085 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 32.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 35 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 37.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
