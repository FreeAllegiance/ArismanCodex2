SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.136-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 63     
       done_text-mat151.4-0 ; 
       done_text-mat162.4-0 ; 
       done_text-mat164.4-0 ; 
       done_text-mat165.4-0 ; 
       done_text-mat166.4-0 ; 
       done_text-mat167.4-0 ; 
       dowager_sPtL-mat1.5-0 ; 
       dowager_sPtL-mat14.7-0 ; 
       dowager_sPtL-mat2.5-0 ; 
       dowager_sPtL-mat3.5-0 ; 
       dowager_sPtL-mat4.7-0 ; 
       dowager_sPtL-mat60.4-0 ; 
       dowager_sPtL-mat62.4-0 ; 
       dowager_sPtL-mat86.9-0 ; 
       dowager_sPtL-mat90.9-0 ; 
       dowager_sPtL-mat91.9-0 ; 
       rotate_cap-mat337.1-0 ; 
       rotate_cap-mat338.1-0 ; 
       rotate_cap-mat339.1-0 ; 
       rotate_cap-mat340.1-0 ; 
       rotate_cap-mat341.1-0 ; 
       rotate_cap-mat342.1-0 ; 
       rotate_cap-mat343.1-0 ; 
       rotate_cap-mat344.1-0 ; 
       rotate_cap-mat345.1-0 ; 
       rotate_cap-mat346.1-0 ; 
       strobes-bottom1.2-0 ; 
       strobes-mat168.2-0 ; 
       strobes-mat186.2-0 ; 
       strobes-mat187.2-0 ; 
       strobes-mat188.2-0 ; 
       strobes-mat189.2-0 ; 
       strobes-mat190.2-0 ; 
       strobes-mat191.2-0 ; 
       strobes-mat192.2-0 ; 
       strobes-mat193.2-0 ; 
       strobes-mat194.2-0 ; 
       strobes-mat195.2-0 ; 
       strobes-mat196.2-0 ; 
       strobes-mat197.2-0 ; 
       strobes-mat198.2-0 ; 
       strobes-mat199.2-0 ; 
       strobes-mat200.2-0 ; 
       strobes-mat201.2-0 ; 
       strobes-mat202.2-0 ; 
       strobes-mat203.2-0 ; 
       strobes-mat204.2-0 ; 
       strobes-mat205.2-0 ; 
       strobes-mat206.2-0 ; 
       strobes-mat207.2-0 ; 
       strobes-mat208.2-0 ; 
       strobes-mat209.2-0 ; 
       strobes-mat210.2-0 ; 
       strobes-mat211.2-0 ; 
       strobes-mat212.2-0 ; 
       strobes-mat213.2-0 ; 
       strobes-mat214.2-0 ; 
       strobes-mat215.2-0 ; 
       strobes-mat216.2-0 ; 
       strobes-mat217.2-0 ; 
       strobes-mat218.2-0 ; 
       strobes-side1.2-0 ; 
       strobes-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 57     
       strobes-capgarage1A.1-0 ; 
       strobes-capgarage1B.1-0 ; 
       strobes-capgarage1C.1-0 ; 
       strobes-capgarage1D.1-0 ; 
       strobes-capgarage1E.1-0 ; 
       strobes-contwr1.1-0 ; 
       strobes-corrdr2.1-0 ; 
       strobes-corrdr4.1-0 ; 
       strobes-cube1.2-0 ; 
       strobes-fuselg1.1-0 ; 
       strobes-fuselg10.1-0 ; 
       strobes-fuselg15.1-0 ; 
       strobes-fuselg16.1-0 ; 
       strobes-fuselg17.1-0 ; 
       strobes-fuselg18.1-0 ; 
       strobes-fuselg3.1-0 ; 
       strobes-fuselg7.1-0 ; 
       strobes-garage3A.1-0 ; 
       strobes-garage3B.1-0 ; 
       strobes-garage3C.1-0 ; 
       strobes-garage3D.1-0 ; 
       strobes-garage3E.1-0 ; 
       strobes-garage4A.1-0 ; 
       strobes-garage4B.1-0 ; 
       strobes-garage4C.1-0 ; 
       strobes-garage4D.1-0 ; 
       strobes-garage4E.1-0 ; 
       strobes-launch1.1-0 ; 
       strobes-null1.1-0 ; 
       strobes-null2.1-0 ; 
       strobes-null3.1-0 ; 
       strobes-platfrm1.1-0 ; 
       strobes-root_1.1-0 ; 
       strobes-root_5.1-0 ; 
       strobes-root_6.1-0 ; 
       strobes-ss01.1-0 ; 
       strobes-ss01_2.26-0 ROOT ; 
       strobes-ss10.1-0 ; 
       strobes-ss11.1-0 ; 
       strobes-ss12.1-0 ; 
       strobes-ss13.1-0 ; 
       strobes-ss14.1-0 ; 
       strobes-ss15.1-0 ; 
       strobes-ss16.1-0 ; 
       strobes-ss17.1-0 ; 
       strobes-ss18.1-0 ; 
       strobes-ss19.1-0 ; 
       strobes-ss2.1-0 ; 
       strobes-ss20.1-0 ; 
       strobes-ss21.1-0 ; 
       strobes-ss3.1-0 ; 
       strobes-ss4.1-0 ; 
       strobes-ss5.1-0 ; 
       strobes-ss6.1-0 ; 
       strobes-ss7.1-0 ; 
       strobes-ss8.1-0 ; 
       strobes-ss9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss96/PICTURES/rixbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-rotate-cap.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       done_text-t2d15.13-0 ; 
       done_text-t2d17.13-0 ; 
       done_text-t2d18.13-0 ; 
       done_text-t2d19.13-0 ; 
       done_text-t2d20.13-0 ; 
       done_text-t2d3.11-0 ; 
       dowager_sPtL-t2d1.11-0 ; 
       dowager_sPtL-t2d11.13-0 ; 
       dowager_sPtL-t2d2.11-0 ; 
       dowager_sPtL-t2d46.10-0 ; 
       dowager_sPtL-t2d59_1.13-0 ; 
       dowager_sPtL-t2d63_1.13-0 ; 
       strobes-t2d100.6-0 ; 
       strobes-t2d101.7-0 ; 
       strobes-t2d59_2.6-0 ; 
       strobes-t2d63_2.6-0 ; 
       strobes-t2d64.7-0 ; 
       strobes-t2d65.7-0 ; 
       strobes-t2d66.7-0 ; 
       strobes-t2d67.7-0 ; 
       strobes-t2d68.6-0 ; 
       strobes-t2d83.6-0 ; 
       strobes-t2d84.6-0 ; 
       strobes-t2d85.6-0 ; 
       strobes-t2d86.6-0 ; 
       strobes-t2d87.6-0 ; 
       strobes-t2d88.6-0 ; 
       strobes-t2d89.6-0 ; 
       strobes-t2d90.6-0 ; 
       strobes-t2d91.6-0 ; 
       strobes-t2d92.6-0 ; 
       strobes-t2d93.6-0 ; 
       strobes-t2d94.6-0 ; 
       strobes-t2d95.6-0 ; 
       strobes-t2d96.6-0 ; 
       strobes-t2d97.6-0 ; 
       strobes-t2d98.6-0 ; 
       strobes-t2d99.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 36 110 ; 
       1 36 110 ; 
       2 36 110 ; 
       3 36 110 ; 
       4 36 110 ; 
       5 9 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 28 110 ; 
       9 36 110 ; 
       10 32 110 ; 
       11 33 110 ; 
       12 33 110 ; 
       13 34 110 ; 
       14 34 110 ; 
       15 31 110 ; 
       16 32 110 ; 
       17 36 110 ; 
       18 36 110 ; 
       19 36 110 ; 
       20 36 110 ; 
       21 36 110 ; 
       22 36 110 ; 
       23 36 110 ; 
       24 36 110 ; 
       25 36 110 ; 
       26 36 110 ; 
       27 36 110 ; 
       28 31 110 ; 
       29 36 110 ; 
       30 36 110 ; 
       31 36 110 ; 
       32 8 110 ; 
       33 8 110 ; 
       34 8 110 ; 
       35 36 110 ; 
       37 36 110 ; 
       38 36 110 ; 
       39 29 110 ; 
       40 29 110 ; 
       41 29 110 ; 
       42 29 110 ; 
       43 29 110 ; 
       44 30 110 ; 
       45 30 110 ; 
       46 30 110 ; 
       47 36 110 ; 
       48 30 110 ; 
       49 30 110 ; 
       50 36 110 ; 
       51 36 110 ; 
       52 36 110 ; 
       53 36 110 ; 
       54 36 110 ; 
       55 36 110 ; 
       56 36 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 7 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 30 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       9 6 300 ; 
       9 8 300 ; 
       9 9 300 ; 
       10 27 300 ; 
       11 40 300 ; 
       12 41 300 ; 
       13 47 300 ; 
       14 48 300 ; 
       15 11 300 ; 
       15 12 300 ; 
       16 0 300 ; 
       31 10 300 ; 
       31 62 300 ; 
       31 61 300 ; 
       31 26 300 ; 
       31 49 300 ; 
       32 1 300 ; 
       32 2 300 ; 
       32 3 300 ; 
       32 4 300 ; 
       32 5 300 ; 
       33 35 300 ; 
       33 36 300 ; 
       33 37 300 ; 
       33 38 300 ; 
       33 39 300 ; 
       34 42 300 ; 
       34 43 300 ; 
       34 44 300 ; 
       34 45 300 ; 
       34 46 300 ; 
       35 50 300 ; 
       37 59 300 ; 
       38 60 300 ; 
       39 16 300 ; 
       40 17 300 ; 
       41 18 300 ; 
       42 19 300 ; 
       43 20 300 ; 
       44 21 300 ; 
       45 22 300 ; 
       46 23 300 ; 
       47 51 300 ; 
       48 24 300 ; 
       49 25 300 ; 
       50 52 300 ; 
       51 53 300 ; 
       52 54 300 ; 
       53 55 300 ; 
       54 56 300 ; 
       55 57 300 ; 
       56 58 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 20 400 ; 
       11 30 400 ; 
       12 31 400 ; 
       13 37 400 ; 
       14 12 400 ; 
       16 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 7 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 16 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       26 19 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       32 14 401 ; 
       33 15 401 ; 
       34 24 401 ; 
       35 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 28 401 ; 
       39 29 401 ; 
       42 32 401 ; 
       43 33 401 ; 
       44 34 401 ; 
       45 35 401 ; 
       46 36 401 ; 
       49 13 401 ; 
       61 18 401 ; 
       62 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 65 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 40 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 42.5 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 48.75 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 65 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 57.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 45 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 47.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 50 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 52.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 60 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 55 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 15 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 17.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 20 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 22.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 25 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 27.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 30 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 32.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 35 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 37.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 0 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 48.75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 97.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 110 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 50 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 56.25 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 46.25 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 51.25 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 72.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 57.5 -4 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       37 SCHEM 85 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 87.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 92.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 95 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 97.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 100 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 102.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 115 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 112.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 110 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 70 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 107.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 105 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 75 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 77.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 80 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 82.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 67.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 62.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 90 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 59 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 59 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.55192 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 96.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 99 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 101.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 114 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 111.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 109 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 106.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 104 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 56.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 44 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 46.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 49 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 51.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 89 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 86.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 54 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 64 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 39 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 39 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 12.55192 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 59 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 59 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 39 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 49 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 49 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 54 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 54 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 54 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 54 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 54 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 49 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
