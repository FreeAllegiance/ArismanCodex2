SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.142-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       done_text-mat151.4-0 ; 
       done_text-mat162.4-0 ; 
       done_text-mat164.4-0 ; 
       done_text-mat165.4-0 ; 
       done_text-mat166.4-0 ; 
       done_text-mat167.4-0 ; 
       dowager_sPtL-mat1.5-0 ; 
       dowager_sPtL-mat14.7-0 ; 
       dowager_sPtL-mat2.5-0 ; 
       dowager_sPtL-mat3.5-0 ; 
       dowager_sPtL-mat4.7-0 ; 
       dowager_sPtL-mat60.4-0 ; 
       dowager_sPtL-mat62.4-0 ; 
       dowager_sPtL-mat86.9-0 ; 
       dowager_sPtL-mat90.9-0 ; 
       dowager_sPtL-mat91.9-0 ; 
       rotate_cap-mat337.2-0 ; 
       rotate_cap-mat342.2-0 ; 
       rotate_cap-mat347.1-0 ; 
       rotate_cap-mat353.1-0 ; 
       strobes-bottom1.2-0 ; 
       strobes-mat168.2-0 ; 
       strobes-mat186.2-0 ; 
       strobes-mat187.2-0 ; 
       strobes-mat188.2-0 ; 
       strobes-mat189.2-0 ; 
       strobes-mat190.2-0 ; 
       strobes-mat191.2-0 ; 
       strobes-mat192.2-0 ; 
       strobes-mat193.2-0 ; 
       strobes-mat194.2-0 ; 
       strobes-mat195.2-0 ; 
       strobes-mat196.2-0 ; 
       strobes-mat197.2-0 ; 
       strobes-mat198.2-0 ; 
       strobes-mat199.2-0 ; 
       strobes-mat200.2-0 ; 
       strobes-mat201.2-0 ; 
       strobes-mat202.2-0 ; 
       strobes-mat203.2-0 ; 
       strobes-mat204.2-0 ; 
       strobes-mat205.2-0 ; 
       strobes-mat206.2-0 ; 
       strobes-mat207.2-0 ; 
       strobes-mat208.3-0 ; 
       strobes-side1.2-0 ; 
       strobes-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 78     
       strobes-capgarage1A.1-0 ; 
       strobes-capgarage1B.1-0 ; 
       strobes-capgarage1C.1-0 ; 
       strobes-capgarage1D.1-0 ; 
       strobes-capgarage1E.1-0 ; 
       strobes-contwr1.1-0 ; 
       strobes-corrdr2.1-0 ; 
       strobes-corrdr4.1-0 ; 
       strobes-cube1.2-0 ; 
       strobes-fuselg1.1-0 ; 
       strobes-fuselg10.1-0 ; 
       strobes-fuselg15.1-0 ; 
       strobes-fuselg16.1-0 ; 
       strobes-fuselg17.1-0 ; 
       strobes-fuselg18.1-0 ; 
       strobes-fuselg3.1-0 ; 
       strobes-fuselg7.1-0 ; 
       strobes-garage2A.1-0 ; 
       strobes-garage2B.1-0 ; 
       strobes-garage2C.1-0 ; 
       strobes-garage2D.1-0 ; 
       strobes-garage2E.1-0 ; 
       strobes-garage3A.1-0 ; 
       strobes-garage3B.1-0 ; 
       strobes-garage3C.1-0 ; 
       strobes-garage3D.1-0 ; 
       strobes-garage3E.1-0 ; 
       strobes-launch1.1-0 ; 
       strobes-null1.1-0 ; 
       strobes-platfrm1.1-0 ; 
       strobes-root_1.1-0 ; 
       strobes-root_5.1-0 ; 
       strobes-root_6.1-0 ; 
       strobes-ss01.1-0 ; 
       strobes-ss01_2.31-0 ROOT ; 
       strobes-ss10.1-0 ; 
       strobes-ss11.1-0 ; 
       strobes-ss12.1-0 ; 
       strobes-ss13.1-0 ; 
       strobes-ss14.1-0 ; 
       strobes-ss15.1-0 ; 
       strobes-ss17.1-0 ; 
       strobes-ss18.1-0 ; 
       strobes-ss19.1-0 ; 
       strobes-ss2.1-0 ; 
       strobes-ss20.1-0 ; 
       strobes-ss21.1-0 ; 
       strobes-ss22.1-0 ; 
       strobes-ss23.1-0 ; 
       strobes-ss24.1-0 ; 
       strobes-ss25.1-0 ; 
       strobes-ss26.1-0 ; 
       strobes-ss27.1-0 ; 
       strobes-ss28.1-0 ; 
       strobes-ss29.1-0 ; 
       strobes-ss30.1-0 ; 
       strobes-ss31.1-0 ; 
       strobes-ss32.1-0 ; 
       strobes-ss33.1-0 ; 
       strobes-ss34.1-0 ; 
       strobes-ss35.1-0 ; 
       strobes-ss36.1-0 ; 
       strobes-ss37.1-0 ; 
       strobes-ss38.1-0 ; 
       strobes-ss39.1-0 ; 
       strobes-ss40.1-0 ; 
       strobes-ss41.1-0 ; 
       strobes-ss42.1-0 ; 
       strobes-ss43.1-0 ; 
       strobes-ss44.1-0 ; 
       strobes-ss45.1-0 ; 
       strobes-ss46.1-0 ; 
       strobes-ss47.1-0 ; 
       strobes-ss5.1-0 ; 
       strobes-ss6.1-0 ; 
       strobes-ss7.1-0 ; 
       strobes-ss8.1-0 ; 
       strobes-ss9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss96/PICTURES/rixbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-scale.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       done_text-t2d15.14-0 ; 
       done_text-t2d17.14-0 ; 
       done_text-t2d18.14-0 ; 
       done_text-t2d19.14-0 ; 
       done_text-t2d20.14-0 ; 
       done_text-t2d3.12-0 ; 
       dowager_sPtL-t2d1.12-0 ; 
       dowager_sPtL-t2d11.14-0 ; 
       dowager_sPtL-t2d2.12-0 ; 
       dowager_sPtL-t2d46.11-0 ; 
       dowager_sPtL-t2d59_1.14-0 ; 
       dowager_sPtL-t2d63_1.14-0 ; 
       strobes-t2d100.7-0 ; 
       strobes-t2d101.8-0 ; 
       strobes-t2d59_2.7-0 ; 
       strobes-t2d63_2.7-0 ; 
       strobes-t2d64.8-0 ; 
       strobes-t2d65.8-0 ; 
       strobes-t2d66.8-0 ; 
       strobes-t2d67.8-0 ; 
       strobes-t2d68.7-0 ; 
       strobes-t2d83.7-0 ; 
       strobes-t2d84.7-0 ; 
       strobes-t2d85.7-0 ; 
       strobes-t2d86.7-0 ; 
       strobes-t2d87.7-0 ; 
       strobes-t2d88.7-0 ; 
       strobes-t2d89.7-0 ; 
       strobes-t2d90.7-0 ; 
       strobes-t2d91.7-0 ; 
       strobes-t2d92.7-0 ; 
       strobes-t2d93.7-0 ; 
       strobes-t2d94.7-0 ; 
       strobes-t2d95.7-0 ; 
       strobes-t2d96.7-0 ; 
       strobes-t2d97.7-0 ; 
       strobes-t2d98.7-0 ; 
       strobes-t2d99.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 34 110 ; 
       1 34 110 ; 
       2 34 110 ; 
       3 34 110 ; 
       4 34 110 ; 
       5 9 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 28 110 ; 
       9 34 110 ; 
       10 30 110 ; 
       11 31 110 ; 
       12 31 110 ; 
       13 32 110 ; 
       14 32 110 ; 
       15 29 110 ; 
       16 30 110 ; 
       17 34 110 ; 
       18 34 110 ; 
       19 34 110 ; 
       20 34 110 ; 
       21 34 110 ; 
       22 34 110 ; 
       23 34 110 ; 
       24 34 110 ; 
       25 34 110 ; 
       26 34 110 ; 
       27 34 110 ; 
       28 29 110 ; 
       29 34 110 ; 
       30 8 110 ; 
       31 8 110 ; 
       32 8 110 ; 
       33 34 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       37 34 110 ; 
       38 34 110 ; 
       39 34 110 ; 
       40 34 110 ; 
       41 34 110 ; 
       42 34 110 ; 
       43 34 110 ; 
       44 34 110 ; 
       45 34 110 ; 
       46 34 110 ; 
       47 34 110 ; 
       48 34 110 ; 
       49 34 110 ; 
       50 34 110 ; 
       51 34 110 ; 
       52 34 110 ; 
       53 34 110 ; 
       54 34 110 ; 
       55 34 110 ; 
       56 34 110 ; 
       57 34 110 ; 
       58 34 110 ; 
       59 34 110 ; 
       60 34 110 ; 
       61 34 110 ; 
       62 34 110 ; 
       63 34 110 ; 
       64 34 110 ; 
       65 34 110 ; 
       66 34 110 ; 
       67 34 110 ; 
       68 34 110 ; 
       69 34 110 ; 
       70 34 110 ; 
       71 34 110 ; 
       72 34 110 ; 
       73 34 110 ; 
       74 34 110 ; 
       75 34 110 ; 
       76 34 110 ; 
       77 34 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 7 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 24 300 ; 
       7 25 300 ; 
       7 26 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       8 22 300 ; 
       8 23 300 ; 
       9 6 300 ; 
       9 8 300 ; 
       9 9 300 ; 
       10 21 300 ; 
       11 34 300 ; 
       12 35 300 ; 
       13 41 300 ; 
       14 42 300 ; 
       15 11 300 ; 
       15 12 300 ; 
       16 0 300 ; 
       29 10 300 ; 
       29 46 300 ; 
       29 45 300 ; 
       29 20 300 ; 
       29 43 300 ; 
       30 1 300 ; 
       30 2 300 ; 
       30 3 300 ; 
       30 4 300 ; 
       30 5 300 ; 
       31 29 300 ; 
       31 30 300 ; 
       31 31 300 ; 
       31 32 300 ; 
       31 33 300 ; 
       32 36 300 ; 
       32 37 300 ; 
       32 38 300 ; 
       32 39 300 ; 
       32 40 300 ; 
       33 44 300 ; 
       35 44 300 ; 
       36 44 300 ; 
       37 16 300 ; 
       38 16 300 ; 
       39 16 300 ; 
       40 16 300 ; 
       41 17 300 ; 
       42 17 300 ; 
       43 17 300 ; 
       44 18 300 ; 
       45 17 300 ; 
       46 17 300 ; 
       47 18 300 ; 
       48 18 300 ; 
       49 18 300 ; 
       50 18 300 ; 
       51 18 300 ; 
       52 18 300 ; 
       53 18 300 ; 
       54 18 300 ; 
       55 18 300 ; 
       56 18 300 ; 
       57 18 300 ; 
       58 18 300 ; 
       59 19 300 ; 
       60 19 300 ; 
       61 19 300 ; 
       62 19 300 ; 
       63 19 300 ; 
       64 19 300 ; 
       65 19 300 ; 
       66 19 300 ; 
       67 19 300 ; 
       68 19 300 ; 
       69 19 300 ; 
       70 19 300 ; 
       71 19 300 ; 
       72 19 300 ; 
       73 44 300 ; 
       74 18 300 ; 
       75 44 300 ; 
       76 44 300 ; 
       77 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 20 400 ; 
       11 30 400 ; 
       12 31 400 ; 
       13 37 400 ; 
       14 12 400 ; 
       16 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 7 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 16 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       20 19 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       26 14 401 ; 
       27 15 401 ; 
       28 24 401 ; 
       29 25 401 ; 
       30 26 401 ; 
       31 27 401 ; 
       32 28 401 ; 
       33 29 401 ; 
       36 32 401 ; 
       37 33 401 ; 
       38 34 401 ; 
       39 35 401 ; 
       40 36 401 ; 
       43 13 401 ; 
       45 18 401 ; 
       46 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 45 -8 0 MPRFLG 0 ; 
       8 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 65 -2 0 MPRFLG 0 ; 
       10 SCHEM 60 -10 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 50 -10 0 MPRFLG 0 ; 
       13 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 55 -10 0 MPRFLG 0 ; 
       15 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 35 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 40 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 2.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       29 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       30 SCHEM 58.75 -8 0 MPRFLG 0 ; 
       31 SCHEM 48.75 -8 0 MPRFLG 0 ; 
       32 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       33 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 88.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       35 SCHEM 77.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 85 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 87.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 90 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 92.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 105 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 102.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       43 SCHEM 100 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 125 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 97.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 95 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 110 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 112.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 115 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 117.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 120 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 122.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 127.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 130 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 132.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 135 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 137.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 140 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 142.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 160 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 145 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 147.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 150 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 152.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 155 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 157.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 162.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 165 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 167.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       70 SCHEM 170 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       71 SCHEM 172.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       72 SCHEM 175 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       73 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 107.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 95.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 84 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 104 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 109 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 141.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 95.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
