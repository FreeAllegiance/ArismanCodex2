SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       skin-cam_int1.9-0 ROOT ; 
       skin-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       skin-inf_light1.1-0 ROOT ; 
       skin-inf_light3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       skin-mat1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       Nurbs-Nurbs_Skin.4-0 ROOT ; 
       Nurbs1-Nurbs_Skin.1-0 ROOT ; 
       skin-circle2.1-0 ; 
       skin-circle2_1.1-0 ; 
       skin-circle2_2.1-0 ; 
       skin-circle2_3.1-0 ; 
       skin-circle2_4.1-0 ; 
       skin-circle2_5.1-0 ; 
       skin-circle2_6.1-0 ; 
       skin-circle2_7.1-0 ; 
       skin-circle2_8.1-0 ; 
       skin-circle2_9.1-0 ; 
       skin-null1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/baseskin ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-skin.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       skin-rendermap1.1-0 ; 
       skin-skin.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 12 110 ; 
       8 12 110 ; 
       9 12 110 ; 
       10 12 110 ; 
       7 12 110 ; 
       5 12 110 ; 
       4 12 110 ; 
       3 12 110 ; 
       6 12 110 ; 
       11 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       0 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 35 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 30 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 13.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 32.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
