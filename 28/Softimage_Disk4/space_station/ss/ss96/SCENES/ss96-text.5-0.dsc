SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.63-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       dowager_sPtL-inf_light5_1_2.58-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.58-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.58-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       done_text-mat151.3-0 ; 
       done_text-mat153.3-0 ; 
       done_text-mat162.3-0 ; 
       done_text-mat164.3-0 ; 
       done_text-mat165.3-0 ; 
       done_text-mat166.3-0 ; 
       done_text-mat167.3-0 ; 
       dowager_sPtL-mat1.3-0 ; 
       dowager_sPtL-mat14.5-0 ; 
       dowager_sPtL-mat2.3-0 ; 
       dowager_sPtL-mat3.3-0 ; 
       dowager_sPtL-mat4.4-0 ; 
       dowager_sPtL-mat60.3-0 ; 
       dowager_sPtL-mat62.3-0 ; 
       dowager_sPtL-mat86.7-0 ; 
       dowager_sPtL-mat90.7-0 ; 
       dowager_sPtL-mat91.7-0 ; 
       text-bottom1.1-0 ; 
       text-mat168.1-0 ; 
       text-mat169.1-0 ; 
       text-mat170.1-0 ; 
       text-mat171.1-0 ; 
       text-mat172.1-0 ; 
       text-mat173.1-0 ; 
       text-mat174.1-0 ; 
       text-mat175.2-0 ; 
       text-mat176.2-0 ; 
       text-mat177.2-0 ; 
       text-mat178.2-0 ; 
       text-mat179.2-0 ; 
       text-mat180.2-0 ; 
       text-mat181.2-0 ; 
       text-mat182.2-0 ; 
       text-mat183.2-0 ; 
       text-mat184.2-0 ; 
       text-side1.1-0 ; 
       text-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       ss01-contwr1.1-0 ; 
       ss01-corrdr2.1-0 ; 
       ss01-corrdr3.1-0 ; 
       ss01-cube1.2-0 ; 
       ss01-fuselg1.1-0 ; 
       ss01-fuselg10.1-0 ; 
       ss01-fuselg11.1-0 ; 
       ss01-fuselg14.1-0 ; 
       ss01-fuselg15.1-0 ; 
       ss01-fuselg3.1-0 ; 
       ss01-fuselg7.1-0 ; 
       ss01-fuselg9.1-0 ; 
       ss01-null1.1-0 ; 
       ss01-platfrm1.1-0 ; 
       ss01-root_1.1-0 ; 
       ss01-root_2.1-0 ; 
       ss01-root_4.1-0 ; 
       ss01-ss01_2.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss96/PICTURES/ss96 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss96-text.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       done_text-t2d15.5-0 ; 
       done_text-t2d17.5-0 ; 
       done_text-t2d18.5-0 ; 
       done_text-t2d19.5-0 ; 
       done_text-t2d20.5-0 ; 
       done_text-t2d3.5-0 ; 
       done_text-t2d5.5-0 ; 
       dowager_sPtL-t2d1.5-0 ; 
       dowager_sPtL-t2d11.7-0 ; 
       dowager_sPtL-t2d2.5-0 ; 
       dowager_sPtL-t2d46.5-0 ; 
       dowager_sPtL-t2d59_1.5-0 ; 
       dowager_sPtL-t2d63_1.5-0 ; 
       text-t2d100.1-0 ; 
       text-t2d101.1-0 ; 
       text-t2d102.1-0 ; 
       text-t2d103.1-0 ; 
       text-t2d104.1-0 ; 
       text-t2d105.1-0 ; 
       text-t2d106.1-0 ; 
       text-t2d59_2.2-0 ; 
       text-t2d63_2.2-0 ; 
       text-t2d90.1-0 ; 
       text-t2d91.1-0 ; 
       text-t2d92.1-0 ; 
       text-t2d93.1-0 ; 
       text-t2d94.1-0 ; 
       text-t2d95.1-0 ; 
       text-t2d96.1-0 ; 
       text-t2d97.1-0 ; 
       text-t2d98.1-0 ; 
       text-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 12 110 ; 
       2 12 110 ; 
       3 12 110 ; 
       4 17 110 ; 
       9 13 110 ; 
       10 14 110 ; 
       15 13 110 ; 
       5 15 110 ; 
       11 14 110 ; 
       6 15 110 ; 
       12 13 110 ; 
       13 17 110 ; 
       14 12 110 ; 
       16 12 110 ; 
       7 16 110 ; 
       8 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       4 7 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 0 300 ; 
       15 21 300 ; 
       15 22 300 ; 
       15 23 300 ; 
       15 24 300 ; 
       15 25 300 ; 
       5 26 300 ; 
       11 1 300 ; 
       6 27 300 ; 
       13 11 300 ; 
       13 36 300 ; 
       13 35 300 ; 
       13 17 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       14 6 300 ; 
       16 28 300 ; 
       16 29 300 ; 
       16 30 300 ; 
       16 31 300 ; 
       16 32 300 ; 
       7 33 300 ; 
       8 34 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 5 400 ; 
       5 30 400 ; 
       11 6 400 ; 
       6 31 400 ; 
       7 18 400 ; 
       8 19 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       36 22 401 ; 
       35 23 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       19 20 401 ; 
       20 21 401 ; 
       21 25 401 ; 
       22 26 401 ; 
       23 27 401 ; 
       24 28 401 ; 
       8 8 401 ; 
       9 7 401 ; 
       10 9 401 ; 
       17 24 401 ; 
       25 29 401 ; 
       13 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       28 13 401 ; 
       29 14 401 ; 
       30 15 401 ; 
       31 16 401 ; 
       32 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 107.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 76.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 90 -4 0 MPRFLG 0 ; 
       5 SCHEM 81.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 86.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 61.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 56.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       8 SCHEM 58.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       36 SCHEM 77.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 80.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 75.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 82.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       22 SCHEM 77.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 80.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 82.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
