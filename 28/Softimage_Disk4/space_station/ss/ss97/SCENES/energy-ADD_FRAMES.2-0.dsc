SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.42-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       ADD_FRAMES-inf_light1.2-0 ROOT ; 
       ADD_FRAMES-inf_light2.2-0 ROOT ; 
       ADD_FRAMES-inf_light3.2-0 ROOT ; 
       ADD_FRAMES-inf_light4.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       ADD_FRAMES-mat148.1-0 ; 
       ADD_FRAMES-mat149.1-0 ; 
       ADD_FRAMES-mat150.1-0 ; 
       ADD_FRAMES-mat151.1-0 ; 
       ADD_FRAMES-mat152.1-0 ; 
       ADD_FRAMES-mat153.1-0 ; 
       ADD_FRAMES-mat154.1-0 ; 
       ADD_FRAMES-mat155.1-0 ; 
       ADD_FRAMES-mat156.1-0 ; 
       ADD_FRAMES-mat157.1-0 ; 
       ADD_FRAMES-mat158.1-0 ; 
       ADD_FRAMES-mat159.1-0 ; 
       ADD_FRAMES-mat160.1-0 ; 
       ADD_FRAMES-mat161.1-0 ; 
       ADD_FRAMES-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       ADD_FRAMES-bgrnd51.5-0 ; 
       ADD_FRAMES-bmerge1.1-0 ROOT ; 
       ADD_FRAMES-fuselg0.1-0 ; 
       ADD_FRAMES-fuselg2.1-0 ; 
       ADD_FRAMES-fuselg6.1-0 ; 
       ADD_FRAMES-fuselg7.1-0 ; 
       ADD_FRAMES-fuselg8.1-0 ; 
       ADD_FRAMES-fuselg9.1-0 ; 
       ADD_FRAMES-garage1A.1-0 ; 
       ADD_FRAMES-garage1B.1-0 ; 
       ADD_FRAMES-garage1C.1-0 ; 
       ADD_FRAMES-garage1D.1-0 ; 
       ADD_FRAMES-garage1E.1-0 ; 
       ADD_FRAMES-launch1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy-ADD_FRAMES.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       ADD_FRAMES-rendermap1.1-0 ; 
       ADD_FRAMES-t2d1.1-0 ; 
       ADD_FRAMES-t2d10.1-0 ; 
       ADD_FRAMES-t2d11.1-0 ; 
       ADD_FRAMES-t2d12.1-0 ; 
       ADD_FRAMES-t2d13.1-0 ; 
       ADD_FRAMES-t2d14.1-0 ; 
       ADD_FRAMES-t2d2.1-0 ; 
       ADD_FRAMES-t2d3.1-0 ; 
       ADD_FRAMES-t2d4.1-0 ; 
       ADD_FRAMES-t2d5.1-0 ; 
       ADD_FRAMES-t2d6.1-0 ; 
       ADD_FRAMES-t2d7.1-0 ; 
       ADD_FRAMES-t2d8.1-0 ; 
       ADD_FRAMES-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 1 110 ; 
       13 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       1 0 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       3 2 300 ; 
       4 1 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       7 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 7 400 ; 
       4 1 400 ; 
       5 8 400 ; 
       6 9 400 ; 
       7 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 2 401 ; 
       14 0 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 33.75 0 0 SRT 1 1 1 0 0 0 0 -5.503489 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 35 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 40 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 5 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 55 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
