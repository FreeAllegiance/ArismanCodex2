SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.45-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       ROTATE-inf_light1.1-0 ROOT ; 
       ROTATE-inf_light2.1-0 ROOT ; 
       ROTATE-inf_light3.1-0 ROOT ; 
       ROTATE-inf_light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       ROTATE-mat148.1-0 ; 
       ROTATE-mat149.1-0 ; 
       ROTATE-mat150.1-0 ; 
       ROTATE-mat151.1-0 ; 
       ROTATE-mat152.1-0 ; 
       ROTATE-mat153.1-0 ; 
       ROTATE-mat154.1-0 ; 
       ROTATE-mat155.1-0 ; 
       ROTATE-mat156.1-0 ; 
       ROTATE-mat157.1-0 ; 
       ROTATE-mat158.1-0 ; 
       ROTATE-mat159.1-0 ; 
       ROTATE-mat160.1-0 ; 
       ROTATE-mat161.1-0 ; 
       ROTATE-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       ss97-bgrnd51.5-0 ; 
       ss97-fuselg0.1-0 ; 
       ss97-fuselg2.1-0 ; 
       ss97-fuselg6.1-0 ; 
       ss97-fuselg7.1-0 ; 
       ss97-fuselg8.1-0 ; 
       ss97-fuselg9.1-0 ; 
       ss97-garage1A.1-0 ; 
       ss97-garage1B.1-0 ; 
       ss97-garage1C.1-0 ; 
       ss97-garage1D.1-0 ; 
       ss97-garage1E.1-0 ; 
       ss97-launch1.1-0 ; 
       ss97-root.3-0 ROOT ; 
       ss97-ss01.1-0 ; 
       ss97-ss02.1-0 ; 
       ss97-ss03.1-0 ; 
       ss97-ss04.1-0 ; 
       ss97-ss05.1-0 ; 
       ss97-ss06.1-0 ; 
       ss97-ss07.1-0 ; 
       ss97-ss08.1-0 ; 
       ss97-ss09.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy-ROTATE.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       ROTATE-rendermap1.1-0 ; 
       ROTATE-t2d1.1-0 ; 
       ROTATE-t2d10.1-0 ; 
       ROTATE-t2d11.1-0 ; 
       ROTATE-t2d12.1-0 ; 
       ROTATE-t2d13.1-0 ; 
       ROTATE-t2d14.1-0 ; 
       ROTATE-t2d2.1-0 ; 
       ROTATE-t2d3.1-0 ; 
       ROTATE-t2d4.1-0 ; 
       ROTATE-t2d5.1-0 ; 
       ROTATE-t2d6.1-0 ; 
       ROTATE-t2d7.1-0 ; 
       ROTATE-t2d8.1-0 ; 
       ROTATE-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       10 13 110 ; 
       11 13 110 ; 
       1 13 110 ; 
       2 1 110 ; 
       3 13 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 13 110 ; 
       17 13 110 ; 
       12 13 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       22 13 110 ; 
       18 13 110 ; 
       19 13 110 ; 
       20 13 110 ; 
       21 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       13 0 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       13 9 300 ; 
       13 10 300 ; 
       13 11 300 ; 
       13 12 300 ; 
       13 13 300 ; 
       2 2 300 ; 
       3 1 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       6 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 7 400 ; 
       3 1 400 ; 
       4 8 400 ; 
       5 9 400 ; 
       6 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 2 401 ; 
       14 0 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 55 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 57.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 60 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 62.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 27.5 0 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 -5.503489 0 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 22.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 25 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 27.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 11.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 40 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 32.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 35 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 37.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 52.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 42.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 45 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 47.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 50 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
