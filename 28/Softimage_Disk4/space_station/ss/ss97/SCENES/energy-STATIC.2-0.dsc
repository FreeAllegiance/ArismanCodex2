SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.47-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       STATIC-inf_light1.2-0 ROOT ; 
       STATIC-inf_light2.2-0 ROOT ; 
       STATIC-inf_light3.2-0 ROOT ; 
       STATIC-inf_light4.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       STATIC-mat148.1-0 ; 
       STATIC-mat149.1-0 ; 
       STATIC-mat150.1-0 ; 
       STATIC-mat151.1-0 ; 
       STATIC-mat152.1-0 ; 
       STATIC-mat153.1-0 ; 
       STATIC-mat154.1-0 ; 
       STATIC-mat155.1-0 ; 
       STATIC-mat156.1-0 ; 
       STATIC-mat157.1-0 ; 
       STATIC-mat158.1-0 ; 
       STATIC-mat159.1-0 ; 
       STATIC-mat160.1-0 ; 
       STATIC-mat161.1-0 ; 
       STATIC-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       ss97-bgrnd51.5-0 ; 
       ss97-fuselg0.1-0 ; 
       ss97-fuselg2.1-0 ; 
       ss97-fuselg6.1-0 ; 
       ss97-fuselg7.1-0 ; 
       ss97-fuselg8.1-0 ; 
       ss97-fuselg9.1-0 ; 
       ss97-launch1.1-0 ; 
       ss97-root.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       STATIC-rendermap1.1-0 ; 
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d4.1-0 ; 
       STATIC-t2d5.1-0 ; 
       STATIC-t2d6.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 8 110 ; 
       2 1 110 ; 
       3 8 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       2 2 300 ; 
       3 1 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       6 5 300 ; 
       8 0 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       8 9 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       8 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 7 400 ; 
       3 1 400 ; 
       4 8 400 ; 
       5 9 400 ; 
       6 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 11.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 10 0 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 -5.503489 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
