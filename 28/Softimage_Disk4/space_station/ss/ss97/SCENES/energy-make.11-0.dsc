SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.11-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       make-inf_light1.7-0 ROOT ; 
       make-inf_light2.7-0 ROOT ; 
       make-inf_light3.7-0 ROOT ; 
       make-inf_light4.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       make-mat148.4-0 ; 
       make-mat149.2-0 ; 
       make-mat150.2-0 ; 
       make-mat151.1-0 ; 
       make-mat152.1-0 ; 
       make-mat153.1-0 ; 
       make-mat154.1-0 ; 
       make-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       make-bgrnd51.5-0 ; 
       make-bmerge1.9-0 ROOT ; 
       make-fuselg0.1-0 ; 
       make-fuselg2.1-0 ; 
       make-fuselg6.1-0 ; 
       make-fuselg7.1-0 ; 
       make-fuselg8.1-0 ; 
       make-fuselg9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy-make.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       make-rendermap1.1-0 ; 
       make-t2d1.1-0 ; 
       make-t2d2.2-0 ; 
       make-t2d3.1-0 ; 
       make-t2d4.1-0 ; 
       make-t2d5.1-0 ; 
       make-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 3 300 ; 
       6 4 300 ; 
       7 5 300 ; 
       3 2 300 ; 
       4 1 300 ; 
       1 0 300 ; 
       1 6 300 ; 
       0 7 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 3 400 ; 
       6 4 400 ; 
       7 5 400 ; 
       3 2 400 ; 
       4 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       6 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 -5.503489 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 40 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
