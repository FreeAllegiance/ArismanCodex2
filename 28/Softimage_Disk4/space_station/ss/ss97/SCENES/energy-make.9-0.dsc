SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.9-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       make-inf_light1.5-0 ROOT ; 
       make-inf_light2.5-0 ROOT ; 
       make-inf_light3.5-0 ROOT ; 
       make-inf_light4.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       make-mat148.2-0 ; 
       make-mat149.1-0 ; 
       make-mat150.1-0 ; 
       make-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       make-bgrnd51.5-0 ; 
       make-bmerge1.7-0 ROOT ; 
       make-fuselg0.1-0 ; 
       make-fuselg2.1-0 ; 
       make-fuselg6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy-make.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       make-rendermap1.1-0 ; 
       make-t2d1.1-0 ; 
       make-t2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 2 300 ; 
       4 1 300 ; 
       1 0 300 ; 
       0 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 2 400 ; 
       4 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 -5.503489 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
