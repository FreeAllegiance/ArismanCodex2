SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.38-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       texture-inf_light1.26-0 ROOT ; 
       texture-inf_light2.26-0 ROOT ; 
       texture-inf_light3.26-0 ROOT ; 
       texture-inf_light4.26-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       texture-mat148.8-0 ; 
       texture-mat149.1-0 ; 
       texture-mat150.1-0 ; 
       texture-mat151.1-0 ; 
       texture-mat152.1-0 ; 
       texture-mat153.1-0 ; 
       texture-mat154.8-0 ; 
       texture-mat155.7-0 ; 
       texture-mat156.6-0 ; 
       texture-mat157.5-0 ; 
       texture-mat158.4-0 ; 
       texture-mat159.3-0 ; 
       texture-mat160.2-0 ; 
       texture-mat161.1-0 ; 
       texture-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       texture-bgrnd51.5-0 ; 
       texture-bmerge1.20-0 ROOT ; 
       texture-cube1.2-0 ROOT ; 
       texture-cube2.2-0 ROOT ; 
       texture-cube3.2-0 ROOT ; 
       texture-cube4.2-0 ROOT ; 
       texture-fuselg0.1-0 ; 
       texture-fuselg2.1-0 ; 
       texture-fuselg6.1-0 ; 
       texture-fuselg7.1-0 ; 
       texture-fuselg8.1-0 ; 
       texture-fuselg9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy-texture.26-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       texture-rendermap1.1-0 ; 
       texture-t2d1.1-0 ; 
       texture-t2d10.7-0 ; 
       texture-t2d11.6-0 ; 
       texture-t2d12.5-0 ; 
       texture-t2d13.4-0 ; 
       texture-t2d14.2-0 ; 
       texture-t2d2.1-0 ; 
       texture-t2d3.1-0 ; 
       texture-t2d4.1-0 ; 
       texture-t2d5.1-0 ; 
       texture-t2d6.14-0 ; 
       texture-t2d7.13-0 ; 
       texture-t2d8.12-0 ; 
       texture-t2d9.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       6 1 110 ; 
       7 6 110 ; 
       8 1 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       1 0 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       7 2 300 ; 
       8 1 300 ; 
       9 3 300 ; 
       10 4 300 ; 
       11 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       7 7 400 ; 
       8 1 400 ; 
       9 8 400 ; 
       10 9 400 ; 
       11 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 2 401 ; 
       14 0 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 -5.503489 0 MPRFLG 0 ; 
       2 SCHEM 57.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 50 0 0 DISPLAY 0 0 SRT 1 1 1 0 1.570796 0 -1.136568 0 0 MPRFLG 0 ; 
       4 SCHEM 52.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 55 0 0 DISPLAY 0 0 SRT 1 1 1 0 4.712389 0 1.037736 0 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 11.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 67.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 70 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 72.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
