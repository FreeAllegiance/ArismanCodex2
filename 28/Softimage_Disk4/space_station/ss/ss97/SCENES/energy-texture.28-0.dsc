SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.40-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       texture-inf_light1.28-0 ROOT ; 
       texture-inf_light2.28-0 ROOT ; 
       texture-inf_light3.28-0 ROOT ; 
       texture-inf_light4.28-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       texture-mat148.8-0 ; 
       texture-mat149.1-0 ; 
       texture-mat150.1-0 ; 
       texture-mat151.1-0 ; 
       texture-mat152.1-0 ; 
       texture-mat153.1-0 ; 
       texture-mat154.8-0 ; 
       texture-mat155.7-0 ; 
       texture-mat156.6-0 ; 
       texture-mat157.5-0 ; 
       texture-mat158.4-0 ; 
       texture-mat159.3-0 ; 
       texture-mat160.2-0 ; 
       texture-mat161.1-0 ; 
       texture-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       texture-bgrnd51.5-0 ; 
       texture-bmerge1.21-0 ROOT ; 
       texture-fuselg0.1-0 ; 
       texture-fuselg2.1-0 ; 
       texture-fuselg6.1-0 ; 
       texture-fuselg7.1-0 ; 
       texture-fuselg8.1-0 ; 
       texture-fuselg9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy-texture.28-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       texture-rendermap1.1-0 ; 
       texture-t2d1.1-0 ; 
       texture-t2d10.8-0 ; 
       texture-t2d11.7-0 ; 
       texture-t2d12.6-0 ; 
       texture-t2d13.5-0 ; 
       texture-t2d14.3-0 ; 
       texture-t2d2.1-0 ; 
       texture-t2d3.1-0 ; 
       texture-t2d4.1-0 ; 
       texture-t2d5.1-0 ; 
       texture-t2d6.15-0 ; 
       texture-t2d7.14-0 ; 
       texture-t2d8.13-0 ; 
       texture-t2d9.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       1 0 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       3 2 300 ; 
       4 1 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       7 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 7 400 ; 
       4 1 400 ; 
       5 8 400 ; 
       6 9 400 ; 
       7 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 2 401 ; 
       14 0 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -6 0 MPRFLG 0 ; 
       1 SCHEM 23.75 -4 0 SRT 1 1 1 0 0 0 0 -5.503489 0 MPRFLG 0 ; 
       2 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       7 SCHEM 8.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
