SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.75-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       ROTATE-inf_light1.20-0 ROOT ; 
       ROTATE-inf_light2.20-0 ROOT ; 
       ROTATE-inf_light3.20-0 ROOT ; 
       ROTATE-inf_light4.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       ROTATE-mat148.2-0 ; 
       ROTATE-mat149.2-0 ; 
       ROTATE-mat150.2-0 ; 
       ROTATE-mat151.2-0 ; 
       ROTATE-mat152.2-0 ; 
       ROTATE-mat153.2-0 ; 
       ROTATE-mat154.2-0 ; 
       ROTATE-mat155.2-0 ; 
       ROTATE-mat156.2-0 ; 
       ROTATE-mat157.2-0 ; 
       ROTATE-mat158.2-0 ; 
       ROTATE-mat159.2-0 ; 
       ROTATE-mat160.2-0 ; 
       ROTATE-mat161.2-0 ; 
       ROTATE-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       ss97-bay1.1-0 ; 
       ss97-bgrnd51.5-0 ; 
       ss97-fuselg0.1-0 ; 
       ss97-fuselg2.1-0 ; 
       ss97-fuselg6.1-0 ; 
       ss97-fuselg7.1-0 ; 
       ss97-fuselg8.1-0 ; 
       ss97-fuselg9.1-0 ; 
       ss97-garage1A.1-0 ; 
       ss97-garage1B.1-0 ; 
       ss97-garage1C.1-0 ; 
       ss97-garage1D.1-0 ; 
       ss97-garage1E.1-0 ; 
       ss97-launch1.1-0 ; 
       ss97-root.16-0 ROOT ; 
       ss97-ss01.1-0 ; 
       ss97-ss02.1-0 ; 
       ss97-ss03.1-0 ; 
       ss97-ss04.1-0 ; 
       ss97-ss05.1-0 ; 
       ss97-ss06.1-0 ; 
       ss97-ss07.1-0 ; 
       ss97-ss08.1-0 ; 
       ss97-ss09.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/rixbay ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy_ss97-ROTATE.19-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       ROTATE-rendermap1.2-0 ; 
       ROTATE-t2d1.2-0 ; 
       ROTATE-t2d10.5-0 ; 
       ROTATE-t2d11.5-0 ; 
       ROTATE-t2d12.5-0 ; 
       ROTATE-t2d13.5-0 ; 
       ROTATE-t2d14.5-0 ; 
       ROTATE-t2d2.2-0 ; 
       ROTATE-t2d3.2-0 ; 
       ROTATE-t2d4.2-0 ; 
       ROTATE-t2d5.2-0 ; 
       ROTATE-t2d6.5-0 ; 
       ROTATE-t2d7.5-0 ; 
       ROTATE-t2d8.5-0 ; 
       ROTATE-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 14 110 ; 
       2 14 110 ; 
       3 2 110 ; 
       4 14 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 14 110 ; 
       22 14 110 ; 
       23 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 14 300 ; 
       3 2 300 ; 
       4 1 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       7 5 300 ; 
       14 0 300 ; 
       14 6 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       14 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 7 400 ; 
       4 1 400 ; 
       5 8 400 ; 
       6 9 400 ; 
       7 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 55 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 57.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 60 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 0 -6 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 10 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -4 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 27.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
