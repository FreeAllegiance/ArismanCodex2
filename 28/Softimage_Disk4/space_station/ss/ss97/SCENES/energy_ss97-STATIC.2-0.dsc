SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.69-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       STATIC-inf_light1.6-0 ROOT ; 
       STATIC-inf_light2.6-0 ROOT ; 
       STATIC-inf_light3.6-0 ROOT ; 
       STATIC-inf_light4.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       STATIC-mat148.2-0 ; 
       STATIC-mat149.2-0 ; 
       STATIC-mat150.2-0 ; 
       STATIC-mat151.2-0 ; 
       STATIC-mat152.2-0 ; 
       STATIC-mat153.2-0 ; 
       STATIC-mat154.2-0 ; 
       STATIC-mat155.2-0 ; 
       STATIC-mat156.2-0 ; 
       STATIC-mat157.2-0 ; 
       STATIC-mat158.2-0 ; 
       STATIC-mat159.2-0 ; 
       STATIC-mat160.2-0 ; 
       STATIC-mat161.2-0 ; 
       STATIC-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       ss97-bgrnd51.5-0 ; 
       ss97-fuselg0.1-0 ; 
       ss97-fuselg2.1-0 ; 
       ss97-fuselg6.1-0 ; 
       ss97-fuselg7.1-0 ; 
       ss97-fuselg8.1-0 ; 
       ss97-fuselg9.1-0 ; 
       ss97-root.15-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/rixbay ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy_ss97-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       STATIC-rendermap1.2-0 ; 
       STATIC-t2d1.2-0 ; 
       STATIC-t2d10.2-0 ; 
       STATIC-t2d11.2-0 ; 
       STATIC-t2d12.2-0 ; 
       STATIC-t2d13.2-0 ; 
       STATIC-t2d14.2-0 ; 
       STATIC-t2d2.2-0 ; 
       STATIC-t2d3.2-0 ; 
       STATIC-t2d4.2-0 ; 
       STATIC-t2d5.2-0 ; 
       STATIC-t2d6.2-0 ; 
       STATIC-t2d7.2-0 ; 
       STATIC-t2d8.2-0 ; 
       STATIC-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       2 1 110 ; 
       3 7 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       2 2 300 ; 
       3 1 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       6 5 300 ; 
       7 0 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 7 400 ; 
       3 1 400 ; 
       4 8 400 ; 
       5 9 400 ; 
       6 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 8.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
