SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.79-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       move_nulls_out-inf_light1.1-0 ROOT ; 
       move_nulls_out-inf_light2.1-0 ROOT ; 
       move_nulls_out-inf_light3.1-0 ROOT ; 
       move_nulls_out-inf_light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       move_nulls_out-mat148.1-0 ; 
       move_nulls_out-mat149.1-0 ; 
       move_nulls_out-mat150.1-0 ; 
       move_nulls_out-mat151.1-0 ; 
       move_nulls_out-mat152.1-0 ; 
       move_nulls_out-mat153.1-0 ; 
       move_nulls_out-mat154.1-0 ; 
       move_nulls_out-mat155.1-0 ; 
       move_nulls_out-mat156.1-0 ; 
       move_nulls_out-mat157.1-0 ; 
       move_nulls_out-mat158.1-0 ; 
       move_nulls_out-mat159.1-0 ; 
       move_nulls_out-mat160.1-0 ; 
       move_nulls_out-mat161.1-0 ; 
       move_nulls_out-mat3.1-0 ; 
       move_nulls_out-mat337.1-0 ; 
       move_nulls_out-mat338.1-0 ; 
       move_nulls_out-mat339.1-0 ; 
       move_nulls_out-mat340.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       ss97-bay1.1-0 ; 
       ss97-bgrnd51.5-0 ; 
       ss97-fuselg0.1-0 ; 
       ss97-fuselg2.1-0 ; 
       ss97-fuselg6.1-0 ; 
       ss97-fuselg7.1-0 ; 
       ss97-fuselg8.1-0 ; 
       ss97-fuselg9.1-0 ; 
       ss97-garage1A.1-0 ; 
       ss97-garage1B.1-0 ; 
       ss97-garage1C.1-0 ; 
       ss97-garage1D.1-0 ; 
       ss97-garage1E.1-0 ; 
       ss97-launch1.1-0 ; 
       ss97-null1.1-0 ; 
       ss97-root.18-0 ROOT ; 
       ss97-ss01.1-0 ; 
       ss97-ss02.1-0 ; 
       ss97-ss03.1-0 ; 
       ss97-ss04.1-0 ; 
       ss97-ss05.1-0 ; 
       ss97-ss06.1-0 ; 
       ss97-ss07.1-0 ; 
       ss97-ss08.1-0 ; 
       ss97-ss09.1-0 ; 
       ss97-ss10.1-0 ; 
       ss97-ss11.1-0 ; 
       ss97-ss12.1-0 ; 
       ss97-ss13.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/bgrnd51 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/rixbay ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss97/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy_ss97-move_nulls_out.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       move_nulls_out-rendermap1.1-0 ; 
       move_nulls_out-t2d1.1-0 ; 
       move_nulls_out-t2d10.1-0 ; 
       move_nulls_out-t2d11.1-0 ; 
       move_nulls_out-t2d12.1-0 ; 
       move_nulls_out-t2d13.1-0 ; 
       move_nulls_out-t2d14.1-0 ; 
       move_nulls_out-t2d2.1-0 ; 
       move_nulls_out-t2d3.1-0 ; 
       move_nulls_out-t2d4.1-0 ; 
       move_nulls_out-t2d5.1-0 ; 
       move_nulls_out-t2d6.1-0 ; 
       move_nulls_out-t2d7.1-0 ; 
       move_nulls_out-t2d8.1-0 ; 
       move_nulls_out-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       25 14 110 ; 
       26 14 110 ; 
       27 14 110 ; 
       28 14 110 ; 
       14 15 110 ; 
       0 15 110 ; 
       1 15 110 ; 
       2 15 110 ; 
       3 2 110 ; 
       4 15 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 15 110 ; 
       20 15 110 ; 
       21 15 110 ; 
       22 15 110 ; 
       23 15 110 ; 
       24 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       25 15 300 ; 
       26 16 300 ; 
       27 17 300 ; 
       28 18 300 ; 
       1 14 300 ; 
       3 2 300 ; 
       4 1 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       7 5 300 ; 
       15 0 300 ; 
       15 6 300 ; 
       15 7 300 ; 
       15 8 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       15 11 300 ; 
       15 12 300 ; 
       15 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 7 400 ; 
       4 1 400 ; 
       5 8 400 ; 
       6 9 400 ; 
       7 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 65 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 67.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 70 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 72.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       25 SCHEM 55 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 60 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 62.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 10 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 32.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
