SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.3-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       dowager_sPtL-mat80.2-0 ; 
       dowager_sPtL-mat81.2-0 ; 
       dowager_sPtL-mat82.2-0 ; 
       dowager_sPtL-mat83.2-0 ; 
       dowager_sPtL-mat84.2-0 ; 
       dowager_sPtL-mat85.2-0 ; 
       dowager_sPtL-mat92.2-0 ; 
       dowager_sPtL-mat93.2-0 ; 
       dowager_sPtL-mat94.2-0 ; 
       dowager_sPtL-mat95.2-0 ; 
       dowager_sPtL-mat96.2-0 ; 
       dowager_sPtL-mat97.2-0 ; 
       make-mat151.3-0 ; 
       make-mat153.3-0 ; 
       make-mat162.3-0 ; 
       make-mat164.3-0 ; 
       make-mat165.3-0 ; 
       make-mat166.3-0 ; 
       make-mat167.3-0 ; 
       make-mat168.3-0 ; 
       make-mat169.3-0 ; 
       make-mat170.3-0 ; 
       make-mat171.3-0 ; 
       make-mat172.3-0 ; 
       make-mat173.3-0 ; 
       make-mat174.3-0 ; 
       make-mat175.2-0 ; 
       make-mat176.2-0 ; 
       make-mat177.2-0 ; 
       make-mat178.2-0 ; 
       make-mat179.2-0 ; 
       make-mat180.2-0 ; 
       make-mat181.2-0 ; 
       make-mat182.2-0 ; 
       make-mat183.2-0 ; 
       make-mat184.2-0 ; 
       make-mat185.2-0 ; 
       make-mat186.2-0 ; 
       make-mat187.2-0 ; 
       make-mat188.2-0 ; 
       make-mat189.2-0 ; 
       make-mat190.2-0 ; 
       make-mat201.1-0 ; 
       make-mat202.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       make-bcorrdr.1-0 ; 
       make-corrdr1.1-0 ; 
       make-corrdr1_1.1-0 ; 
       make-corrdr4.1-0 ; 
       make-corrdr4_1.1-0 ; 
       make-fuselg7.1-0 ; 
       make-fuselg7_1.1-0 ; 
       make-fuselg7_2.1-0 ; 
       make-fuselg9.1-0 ; 
       make-fuselg9_1.1-0 ; 
       make-null1.2-0 ; 
       make-root_1.2-0 ; 
       make-root_1_1.2-0 ; 
       make-sphere1.6-0 ROOT ; 
       make-torus1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/More_Stations/Rix/ss/ss98/PICTURES/ss01 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss98/PICTURES/ss95 ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss98/PICTURES/ss97 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ordinance-make.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       dowager_sPtL-t2d57.3-0 ; 
       dowager_sPtL-t2d61.3-0 ; 
       dowager_sPtL-t2d62.3-0 ; 
       dowager_sPtL-t2d64.3-0 ; 
       make-t2d15.5-0 ; 
       make-t2d17.5-0 ; 
       make-t2d18.5-0 ; 
       make-t2d19.5-0 ; 
       make-t2d20.5-0 ; 
       make-t2d21.4-0 ; 
       make-t2d22.4-0 ; 
       make-t2d23.4-0 ; 
       make-t2d24.4-0 ; 
       make-t2d25.4-0 ; 
       make-t2d26.4-0 ; 
       make-t2d27.4-0 ; 
       make-t2d3.5-0 ; 
       make-t2d5.5-0 ; 
       make-t2d70.3-0 ; 
       make-t2d71.3-0 ; 
       make-t2d72.3-0 ; 
       make-t2d73.3-0 ; 
       make-t2d74.2-0 ; 
       make-t2d75.2-0 ; 
       make-t2d76.2-0 ; 
       make-t2d91.1-0 ; 
       make-t2d92.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       12 14 110 ; 
       6 12 110 ; 
       9 12 110 ; 
       0 14 110 ; 
       14 13 110 ; 
       1 14 110 ; 
       3 14 110 ; 
       10 14 110 ; 
       2 10 110 ; 
       4 10 110 ; 
       5 11 110 ; 
       8 11 110 ; 
       11 14 110 ; 
       7 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       12 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       6 25 300 ; 
       9 24 300 ; 
       0 38 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       0 41 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       4 34 300 ; 
       4 35 300 ; 
       4 36 300 ; 
       4 37 300 ; 
       5 12 300 ; 
       8 13 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       7 42 300 ; 
       7 43 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 15 400 ; 
       9 14 400 ; 
       5 16 400 ; 
       8 17 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       39 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       14 4 401 ; 
       19 9 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       18 8 401 ; 
       20 10 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       42 25 401 ; 
       43 26 401 ; 
       4 0 401 ; 
       5 3 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       36 20 401 ; 
       37 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       12 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       0 SCHEM 111.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       13 SCHEM 61.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 91.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 83.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 61.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 65 -4 0 MPRFLG 0 ; 
       7 SCHEM 118.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       24 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       14 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
