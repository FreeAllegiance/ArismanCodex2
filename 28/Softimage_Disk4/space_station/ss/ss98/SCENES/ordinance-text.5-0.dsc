SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       www-cam_int1.10-0 ROOT ; 
       www-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 36     
       text-mat151.1-0 ; 
       text-mat153.1-0 ; 
       text-mat162.1-0 ; 
       text-mat164.1-0 ; 
       text-mat165.1-0 ; 
       text-mat166.1-0 ; 
       text-mat167.1-0 ; 
       text-mat168.1-0 ; 
       text-mat169.1-0 ; 
       text-mat170.1-0 ; 
       text-mat171.1-0 ; 
       text-mat172.1-0 ; 
       text-mat173.1-0 ; 
       text-mat174.1-0 ; 
       text-mat187.2-0 ; 
       text-mat188.2-0 ; 
       text-mat189.2-0 ; 
       text-mat201.1-0 ; 
       text-mat202.1-0 ; 
       text-mat203.1-0 ; 
       text-mat206.1-0 ; 
       text-mat207.1-0 ; 
       text-mat208.2-0 ; 
       text-mat209.1-0 ; 
       text-mat210.1-0 ; 
       text-mat211.1-0 ; 
       text-mat212.1-0 ; 
       text-mat213.1-0 ; 
       text-mat214.1-0 ; 
       text-mat215.1-0 ; 
       text-mat216.1-0 ; 
       text-mat217.1-0 ; 
       text-mat218.1-0 ; 
       text-mat219.1-0 ; 
       text-mat220.1-0 ; 
       text-mat221.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       text-bcorrdr.1-0 ; 
       text-bcorrdr1.1-0 ; 
       text-bcorrdr2.1-0 ; 
       text-bcorrdr3.1-0 ; 
       text-bcorrdr4.1-0 ; 
       text-fuselg7.1-0 ; 
       text-fuselg7_1.1-0 ; 
       text-fuselg7_2.1-0 ; 
       text-fuselg7_4.1-0 ; 
       text-fuselg9.1-0 ; 
       text-fuselg9_1.1-0 ; 
       text-root_1.2-0 ; 
       text-root_1_1.2-0 ; 
       text-sphere1.5-0 ROOT ; 
       text-torus1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/More_Stations/Rix/ss/ss98/PICTURES/ss98 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ordinance-text.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       text-t2d100.1-0 ; 
       text-t2d101.2-0 ; 
       text-t2d102.2-0 ; 
       text-t2d103.2-0 ; 
       text-t2d104.2-0 ; 
       text-t2d105.2-0 ; 
       text-t2d106.2-0 ; 
       text-t2d107.2-0 ; 
       text-t2d108.2-0 ; 
       text-t2d109.2-0 ; 
       text-t2d110.2-0 ; 
       text-t2d111.2-0 ; 
       text-t2d112.2-0 ; 
       text-t2d15.2-0 ; 
       text-t2d17.2-0 ; 
       text-t2d18.2-0 ; 
       text-t2d19.2-0 ; 
       text-t2d20.2-0 ; 
       text-t2d21.2-0 ; 
       text-t2d22.2-0 ; 
       text-t2d23.2-0 ; 
       text-t2d24.2-0 ; 
       text-t2d25.2-0 ; 
       text-t2d26.2-0 ; 
       text-t2d27.2-0 ; 
       text-t2d3.2-0 ; 
       text-t2d5.2-0 ; 
       text-t2d74.2-0 ; 
       text-t2d75.2-0 ; 
       text-t2d91.1-0 ; 
       text-t2d92.1-0 ; 
       text-t2d93.1-0 ; 
       text-t2d96.1-0 ; 
       text-t2d97.1-0 ; 
       text-t2d98.2-0 ; 
       text-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       12 14 110 ; 
       6 12 110 ; 
       10 12 110 ; 
       0 14 110 ; 
       14 13 110 ; 
       1 13 110 ; 
       8 13 110 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 11 110 ; 
       9 11 110 ; 
       11 14 110 ; 
       7 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       12 7 300 ; 
       12 8 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       6 13 300 ; 
       10 12 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       14 22 300 ; 
       14 23 300 ; 
       13 19 300 ; 
       1 24 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       4 33 300 ; 
       4 34 300 ; 
       4 35 300 ; 
       5 0 300 ; 
       9 1 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       7 17 300 ; 
       7 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 24 400 ; 
       10 23 400 ; 
       5 25 400 ; 
       9 26 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       14 0 401 ; 
       15 27 401 ; 
       16 28 401 ; 
       24 1 401 ; 
       19 31 401 ; 
       22 34 401 ; 
       23 35 401 ; 
       20 32 401 ; 
       21 33 401 ; 
       25 2 401 ; 
       26 3 401 ; 
       2 13 401 ; 
       7 18 401 ; 
       3 14 401 ; 
       4 15 401 ; 
       5 16 401 ; 
       6 17 401 ; 
       8 19 401 ; 
       9 20 401 ; 
       10 21 401 ; 
       11 22 401 ; 
       27 4 401 ; 
       28 5 401 ; 
       29 6 401 ; 
       30 7 401 ; 
       31 8 401 ; 
       32 9 401 ; 
       33 10 401 ; 
       17 29 401 ; 
       18 30 401 ; 
       34 11 401 ; 
       35 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       12 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       0 SCHEM 50 -4 0 MPRFLG 0 ; 
       14 SCHEM 30 -2 0 MPRFLG 0 ; 
       13 SCHEM 51.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 72.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 66.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 80 -2 0 MPRFLG 0 ; 
       3 SCHEM 87.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 95 -2 0 MPRFLG 0 ; 
       5 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 35 -4 0 MPRFLG 0 ; 
       7 SCHEM 61.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       12 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       23 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 100 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 97.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
