SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.21-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       dowager_sPtL-inf_light5_1_2.18-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.18-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       done_text-mat151.3-0 ; 
       done_text-mat153.3-0 ; 
       done_text-mat162.3-0 ; 
       done_text-mat164.3-0 ; 
       done_text-mat165.3-0 ; 
       done_text-mat166.3-0 ; 
       done_text-mat167.3-0 ; 
       done_text-mat173.3-0 ; 
       done_text-mat174.3-0 ; 
       done_text-mat201.3-0 ; 
       done_text-mat202.3-0 ; 
       done_text-mat203.4-0 ; 
       done_text-mat208.3-0 ; 
       done_text-mat209.3-0 ; 
       done_text-mat210.3-0 ; 
       done_text-mat211.3-0 ; 
       done_text-mat212.3-0 ; 
       done_text-mat213.3-0 ; 
       done_text-mat214.3-0 ; 
       done_text-mat215.3-0 ; 
       done_text-mat216.3-0 ; 
       done_text-mat217.3-0 ; 
       done_text-mat218.3-0 ; 
       done_text-mat219.3-0 ; 
       done_text-mat220.3-0 ; 
       done_text-mat221.3-0 ; 
       done_text-mat222.3-0 ; 
       done_text-mat223.3-0 ; 
       done_text-mat224.3-0 ; 
       done_text-mat225.3-0 ; 
       done_text-mat226.3-0 ; 
       done_text-mat227.3-0 ; 
       done_text-mat228.3-0 ; 
       done_text-mat229.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       done_text-all_hangars.1-0 ; 
       done_text-bcorrdr1.1-0 ; 
       done_text-bcorrdr2.1-0 ; 
       done_text-bcorrdr3.1-0 ; 
       done_text-bcorrdr4.1-0 ; 
       done_text-fuselg7.1-0 ; 
       done_text-fuselg7_1.1-0 ; 
       done_text-fuselg7_2.1-0 ; 
       done_text-fuselg7_5.1-0 ; 
       done_text-fuselg9.1-0 ; 
       done_text-fuselg9_1.1-0 ; 
       done_text-null1.1-0 ; 
       done_text-root_1.2-0 ; 
       done_text-root_2.1-0 ; 
       done_text-sphere1.24-0 ROOT ; 
       done_text-torus1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss98/PICTURES/rixbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss98/PICTURES/ss98 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss98_ordinance-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       done_text-t2d101.3-0 ; 
       done_text-t2d102.3-0 ; 
       done_text-t2d103.3-0 ; 
       done_text-t2d104.3-0 ; 
       done_text-t2d105.3-0 ; 
       done_text-t2d106.3-0 ; 
       done_text-t2d107.3-0 ; 
       done_text-t2d108.3-0 ; 
       done_text-t2d109.3-0 ; 
       done_text-t2d110.3-0 ; 
       done_text-t2d111.3-0 ; 
       done_text-t2d112.3-0 ; 
       done_text-t2d113.3-0 ; 
       done_text-t2d114.3-0 ; 
       done_text-t2d115.4-0 ; 
       done_text-t2d116.4-0 ; 
       done_text-t2d117.4-0 ; 
       done_text-t2d118.4-0 ; 
       done_text-t2d119.4-0 ; 
       done_text-t2d120.10-0 ; 
       done_text-t2d15.5-0 ; 
       done_text-t2d17.5-0 ; 
       done_text-t2d18.5-0 ; 
       done_text-t2d19.5-0 ; 
       done_text-t2d20.5-0 ; 
       done_text-t2d26.3-0 ; 
       done_text-t2d27.3-0 ; 
       done_text-t2d3.3-0 ; 
       done_text-t2d5.3-0 ; 
       done_text-t2d91.4-0 ; 
       done_text-t2d92.4-0 ; 
       done_text-t2d93.12-0 ; 
       done_text-t2d98.4-0 ; 
       done_text-t2d99.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 14 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 14 110 ; 
       5 12 110 ; 
       6 13 110 ; 
       7 14 110 ; 
       8 15 110 ; 
       9 12 110 ; 
       10 13 110 ; 
       11 14 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       15 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 14 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       5 0 300 ; 
       6 8 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       8 26 300 ; 
       8 27 300 ; 
       9 1 300 ; 
       10 7 300 ; 
       12 2 300 ; 
       12 3 300 ; 
       12 4 300 ; 
       12 5 300 ; 
       12 6 300 ; 
       13 28 300 ; 
       13 29 300 ; 
       13 30 300 ; 
       13 31 300 ; 
       13 32 300 ; 
       14 11 300 ; 
       14 33 300 ; 
       15 12 300 ; 
       15 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 27 400 ; 
       6 26 400 ; 
       9 28 400 ; 
       10 25 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 20 401 ; 
       3 21 401 ; 
       4 22 401 ; 
       5 23 401 ; 
       6 24 401 ; 
       9 29 401 ; 
       10 30 401 ; 
       11 31 401 ; 
       12 32 401 ; 
       13 33 401 ; 
       14 0 401 ; 
       15 1 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 11 401 ; 
       26 12 401 ; 
       27 13 401 ; 
       28 14 401 ; 
       29 15 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -2 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 20 -2 0 MPRFLG 0 ; 
       12 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 16.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
