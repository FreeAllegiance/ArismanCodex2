SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.10-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       dowager_sPtL-inf_light5_1_2.10-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       done_text-mat151.2-0 ; 
       done_text-mat153.2-0 ; 
       done_text-mat162.2-0 ; 
       done_text-mat164.2-0 ; 
       done_text-mat165.2-0 ; 
       done_text-mat166.2-0 ; 
       done_text-mat167.2-0 ; 
       done_text-mat173.2-0 ; 
       done_text-mat174.2-0 ; 
       done_text-mat201.2-0 ; 
       done_text-mat202.2-0 ; 
       done_text-mat203.3-0 ; 
       done_text-mat208.2-0 ; 
       done_text-mat209.2-0 ; 
       done_text-mat210.2-0 ; 
       done_text-mat211.2-0 ; 
       done_text-mat212.2-0 ; 
       done_text-mat213.2-0 ; 
       done_text-mat214.2-0 ; 
       done_text-mat215.2-0 ; 
       done_text-mat216.2-0 ; 
       done_text-mat217.2-0 ; 
       done_text-mat218.2-0 ; 
       done_text-mat219.2-0 ; 
       done_text-mat220.2-0 ; 
       done_text-mat221.2-0 ; 
       done_text-mat222.2-0 ; 
       done_text-mat223.2-0 ; 
       done_text-mat224.2-0 ; 
       done_text-mat225.2-0 ; 
       done_text-mat226.2-0 ; 
       done_text-mat227.2-0 ; 
       done_text-mat228.2-0 ; 
       done_text-mat229.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       done_text-bcorrdr1.1-0 ; 
       done_text-bcorrdr2.1-0 ; 
       done_text-bcorrdr3.1-0 ; 
       done_text-bcorrdr4.1-0 ; 
       done_text-fuselg7.1-0 ; 
       done_text-fuselg7_1.1-0 ; 
       done_text-fuselg7_2.1-0 ; 
       done_text-fuselg7_5.1-0 ; 
       done_text-fuselg9.1-0 ; 
       done_text-fuselg9_1.1-0 ; 
       done_text-root_1.2-0 ; 
       done_text-root_2.1-0 ; 
       done_text-sphere1.19-0 ROOT ; 
       done_text-torus1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/More_Stations/Rix/ss/ss98/PICTURES/rixbay ; 
       F:/Pete_Data3/More_Stations/Rix/ss/ss98/PICTURES/ss98 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss98_ordinance-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       done_text-t2d101.2-0 ; 
       done_text-t2d102.2-0 ; 
       done_text-t2d103.2-0 ; 
       done_text-t2d104.2-0 ; 
       done_text-t2d105.2-0 ; 
       done_text-t2d106.2-0 ; 
       done_text-t2d107.2-0 ; 
       done_text-t2d108.2-0 ; 
       done_text-t2d109.2-0 ; 
       done_text-t2d110.2-0 ; 
       done_text-t2d111.2-0 ; 
       done_text-t2d112.2-0 ; 
       done_text-t2d113.2-0 ; 
       done_text-t2d114.2-0 ; 
       done_text-t2d115.3-0 ; 
       done_text-t2d116.3-0 ; 
       done_text-t2d117.3-0 ; 
       done_text-t2d118.3-0 ; 
       done_text-t2d119.3-0 ; 
       done_text-t2d120.9-0 ; 
       done_text-t2d15.4-0 ; 
       done_text-t2d17.4-0 ; 
       done_text-t2d18.4-0 ; 
       done_text-t2d19.4-0 ; 
       done_text-t2d20.4-0 ; 
       done_text-t2d26.2-0 ; 
       done_text-t2d27.2-0 ; 
       done_text-t2d3.2-0 ; 
       done_text-t2d5.2-0 ; 
       done_text-t2d91.3-0 ; 
       done_text-t2d92.3-0 ; 
       done_text-t2d93.11-0 ; 
       done_text-t2d98.3-0 ; 
       done_text-t2d99.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 12 110 ; 
       2 12 110 ; 
       3 12 110 ; 
       4 10 110 ; 
       5 11 110 ; 
       6 12 110 ; 
       7 13 110 ; 
       8 10 110 ; 
       9 11 110 ; 
       10 13 110 ; 
       11 13 110 ; 
       13 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       4 0 300 ; 
       5 8 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       7 26 300 ; 
       7 27 300 ; 
       8 1 300 ; 
       9 7 300 ; 
       10 2 300 ; 
       10 3 300 ; 
       10 4 300 ; 
       10 5 300 ; 
       10 6 300 ; 
       11 28 300 ; 
       11 29 300 ; 
       11 30 300 ; 
       11 31 300 ; 
       11 32 300 ; 
       12 11 300 ; 
       12 33 300 ; 
       13 12 300 ; 
       13 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 27 400 ; 
       5 26 400 ; 
       8 28 400 ; 
       9 25 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 20 401 ; 
       3 21 401 ; 
       4 22 401 ; 
       5 23 401 ; 
       6 24 401 ; 
       9 29 401 ; 
       10 30 401 ; 
       11 31 401 ; 
       12 32 401 ; 
       13 33 401 ; 
       14 0 401 ; 
       15 1 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 11 401 ; 
       26 12 401 ; 
       27 13 401 ; 
       28 14 401 ; 
       29 15 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 13.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
