SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.29-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       bound-mat230.1-0 ; 
       bound-mat231.1-0 ; 
       bound-mat232.1-0 ; 
       bound-mat233.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       bounding_model-bound1.1-0 ; 
       bounding_model-bound10.2-0 ; 
       bounding_model-bound11.2-0 ; 
       bounding_model-bound12.1-0 ; 
       bounding_model-bound13.1-0 ; 
       bounding_model-bound14.1-0 ; 
       bounding_model-bound15.1-0 ; 
       bounding_model-bound16.1-0 ; 
       bounding_model-bound2.1-0 ; 
       bounding_model-bound3.1-0 ; 
       bounding_model-bound4.1-0 ; 
       bounding_model-bound5.1-0 ; 
       bounding_model-bound6.1-0 ; 
       bounding_model-bound7.2-0 ; 
       bounding_model-bound8.1-0 ; 
       bounding_model-bound9.2-0 ; 
       bounding_model-bounding_model_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss98/PICTURES/ss98 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss98_ordinance-bound.13-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       bound-t2d121.2-0 ; 
       bound-t2d122.2-0 ; 
       bound-t2d123.2-0 ; 
       bound-t2d124.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 16 110 ; 
       2 16 110 ; 
       8 16 110 ; 
       9 16 110 ; 
       10 16 110 ; 
       11 16 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
       3 16 110 ; 
       4 16 110 ; 
       5 16 110 ; 
       6 16 110 ; 
       7 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 2 300 ; 
       2 3 300 ; 
       12 0 300 ; 
       12 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 10 -2 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 20 -2 0 MPRFLG 0 ; 
       15 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 35 -2 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 40 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
