SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss99-Poly_stomachShp.16-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.56-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1_1.28-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1.28-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1.28-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1.28-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1.28-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1.28-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       Animation-mat10.1-0 ; 
       Animation-mat11.1-0 ; 
       Animation-mat12.1-0 ; 
       Animation-mat13.1-0 ; 
       Animation-mat14.1-0 ; 
       Animation-mat15.1-0 ; 
       Animation-mat17.3-0 ; 
       Animation-mat19.1-0 ; 
       Animation-mat20.1-0 ; 
       Animation-mat21.3-0 ; 
       Animation-mat22.2-0 ; 
       Animation-mat23.1-0 ; 
       Animation-mat3.1-0 ; 
       Animation-mat4.1-0 ; 
       Animation-mat5.1-0 ; 
       Animation-mat6.1-0 ; 
       Animation-mat7.1-0 ; 
       Animation-mat8.1-0 ; 
       Animation-mat9.1-0 ; 
       rix_post_sPTL-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       ss99-bmerge1.2-0 ; 
       ss99-bmerge2.1-0 ; 
       ss99-cube1.1-0 ; 
       ss99-fuselg4.1-0 ; 
       ss99-fuselg4_1.1-0 ; 
       ss99-fuselg5.1-0 ; 
       ss99-fuselg6.1-0 ; 
       ss99-null1.40-0 ROOT ; 
       ss99-null2.1-0 ; 
       ss99-Poly_stomach.6-0 ; 
       ss99-sphere1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/ss99 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-Animation.16-0 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES NBELEM 1     
       ss99-Poly_stomachShp.16-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       Animation-t2d10.1-0 ; 
       Animation-t2d11.1-0 ; 
       Animation-t2d12.1-0 ; 
       Animation-t2d13.1-0 ; 
       Animation-t2d14.1-0 ; 
       Animation-t2d15.1-0 ; 
       Animation-t2d16.1-0 ; 
       Animation-t2d17.1-0 ; 
       Animation-t2d18.2-0 ; 
       Animation-t2d19.9-0 ; 
       Animation-t2d20.8-0 ; 
       Animation-t2d21.7-0 ; 
       Animation-t2d22.4-0 ; 
       Animation-t2d4.1-0 ; 
       Animation-t2d5.1-0 ; 
       Animation-t2d6.1-0 ; 
       Animation-t2d7.1-0 ; 
       Animation-t2d8.1-0 ; 
       Animation-t2d9.1-0 ; 
       rix_post_sPTL-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 9 110 ; 
       2 1 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER SHAPES 
       9 0 200 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       1 8 300 ; 
       2 6 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       3 19 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       6 5 300 ; 
       9 12 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       10 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 11 401 ; 
       7 8 401 ; 
       8 7 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 12 401 ; 
       12 15 401 ; 
       13 3 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES CHAPTER ANIMATION 
       0 0 15010 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 5 -10 0 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 30 -10 0 MPRFLG 0 ; 
       5 SCHEM 25 -10 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 23.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 MPRFLG 0 ; 
       10 SCHEM 0 -12 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 0 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 0 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 2.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 52.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 11.5 -12 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 25 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
