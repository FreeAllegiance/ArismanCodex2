SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       test_conver_to_poly-cam_int1.7-0 ROOT ; 
       test_conver_to_poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       Texture_Blob-light1.3-0 ROOT ; 
       Texture_Blob-light2.3-0 ROOT ; 
       Texture_Blob-light3.3-0 ROOT ; 
       Texture_Blob-light4.3-0 ROOT ; 
       Texture_Blob-light5.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       Texture_Blob-mat1.2-0 ; 
       Texture_Blob-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       Texture_Blob-sphere11.2-0 ROOT ; 
       Texture_Blob1-sphere11.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/Stomach ; 
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-Texture_Blob.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       Texture_Blob-Rendermap1.1-0 ; 
       Texture_Blob-Rendermap2.1-0 ; 
       Texture_Blob-Stomach.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 10 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 3.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
