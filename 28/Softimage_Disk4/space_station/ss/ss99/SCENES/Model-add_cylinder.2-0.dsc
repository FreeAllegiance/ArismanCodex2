SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.1-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1.1-0 ROOT ; 
       rix_carrier_s-inf_light2_1.1-0 ROOT ; 
       rix_carrier_s-inf_light3_1.1-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.1-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.1-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       add_cylinder-bay1.1-0 ; 
       add_cylinder-mat148.1-0 ; 
       add_cylinder-mat149.1-0 ; 
       add_cylinder-mat150.1-0 ; 
       add_cylinder-mat151.1-0 ; 
       add_cylinder-mat152.1-0 ; 
       add_cylinder-mat153.1-0 ; 
       add_cylinder-mat154.1-0 ; 
       add_cylinder-mat155.1-0 ; 
       add_cylinder-mat156.1-0 ; 
       add_cylinder-mat157.1-0 ; 
       add_cylinder-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       add_cylinder-cyl2.2-0 ROOT ; 
       cap05-fuselg1.1-0 ROOT ; 
       Poly_Stomach-Poly_stomach.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/cap05 ; 
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/guts ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-add_cylinder.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       add_cylinder-t2d1.2-0 ; 
       add_cylinder-t2d29.1-0 ; 
       add_cylinder-t2d30.1-0 ; 
       add_cylinder-t2d31.1-0 ; 
       add_cylinder-t2d32.1-0 ; 
       add_cylinder-t2d33.1-0 ; 
       add_cylinder-t2d34.1-0 ; 
       add_cylinder-t2d35.1-0 ; 
       add_cylinder-t2d36.1-0 ; 
       add_cylinder-t2d37.1-0 ; 
       add_cylinder-t2d38.1-0 ; 
       add_cylinder-t2d39.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       2 11 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       0 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 10 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 7.5 0 0 SRT 3.611626 3.611626 3.611626 0 0 0 0 -25.92466 0 MPRFLG 0 ; 
       0 SCHEM 2.5 0 0 SRT 10.52089 20.09703 10.52089 0 0 0 0 -87.94566 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 12.87533 5.639522e-007 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       11 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
