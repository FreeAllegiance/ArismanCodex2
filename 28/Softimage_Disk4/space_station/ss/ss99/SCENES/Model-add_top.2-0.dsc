SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.4-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1.4-0 ROOT ; 
       rix_carrier_s-inf_light2_1.4-0 ROOT ; 
       rix_carrier_s-inf_light3_1.4-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.4-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.4-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       add_top-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       add_top-bmerge1.1-0 ROOT ; 
       add_top-cyl2.1-0 ROOT ; 
       add_top-spline1.1-0 ROOT ; 
       cap05-fuselg1.3-0 ROOT ; 
       cap6-fuselg1.1-0 ROOT ; 
       cap7-fuselg1.1-0 ROOT ; 
       cap8-fuselg1.1-0 ROOT ; 
       Poly_Stomach-Poly_stomach.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/guts ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-add_top.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       add_top-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       7 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       7 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 10 -4 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 0 -6 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 3.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -25.92466 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 10.52089 20.09703 10.52089 0 0 0 0 -87.94566 0 MPRFLG 0 ; 
       4 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 -1.570796 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 0 -4 0 DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 15.27968 -18.03264 MPRFLG 0 ; 
       0 SCHEM 7.5 -4 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 22.5497 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
