SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       test_conver_to_poly-cam_int1.4-0 ROOT ; 
       test_conver_to_poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       blob-sphere11.1-0 ROOT ; 
       test_conver_to_poly-null1.1-0 ROOT ; 
       test_conver_to_poly-Pipe.2-0 ; 
       test_conver_to_poly-Pipe_1.4-0 ; 
       test_conver_to_poly-Pipe_2.3-0 ; 
       test_conver_to_poly-Pipe3.1-0 ; 
       test_conver_to_poly-Pipe3_1.1-0 ; 
       test_conver_to_poly-Pipe3_2.3-0 ; 
       test_conver_to_poly-sphere2.3-0 ; 
       test_conver_to_poly-sphere3.1-0 ; 
       test_conver_to_poly-sphere3_1.1-0 ; 
       test_conver_to_poly-sphere3_2.1-0 ; 
       test_wrap1-bmerge1.1-0 ROOT ; 
       test_wrap1-sphere10.1-0 ROOT ; 
       test_wrap1-sphere9.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-blob.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 7 110 ; 
       10 5 110 ; 
       11 6 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 22.5 5.288874 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 9.59724 4.259573 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.4444282 0.1521862 0.4222994 MPRFLG 0 ; 
       14 SCHEM 6.152272 5.288874 0 USR DISPLAY 0 0 SRT 1.712 1.712 1.712 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 11.54097 3.04863 0 USR DISPLAY 0 0 SRT 0.9176317 0.9176317 0.9176317 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
