SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       def8-cam_int1.1-0 ROOT ; 
       def8-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       def8-135.2-0 ; 
       def8-45.2-0 ; 
       def8-null1.1-0 ROOT ; 
       def8-null2.1-0 ROOT ; 
       def8-nurbs1.2-0 ; 
       def8-nurbs11.2-0 ; 
       def8-nurbs12.2-0 ; 
       def8-nurbs13.2-0 ; 
       def8-nurbs14.2-0 ; 
       def8-nurbs15.2-0 ; 
       def8-nurbs16.2-0 ; 
       def8-nurbs2.2-0 ; 
       def8-nurbs3.2-0 ; 
       def8-nurbs4.2-0 ; 
       def8-nurbs5.2-0 ; 
       def8-nurbs6.2-0 ; 
       def8-nurbs7.2-0 ; 
       def8-Pipe.1-0 ROOT ; 
       def8-sphere1.1-0 ROOT ; 
       def8-X.2-0 ; 
       def8-Z.2-0 ; 
       lines_in_a_ball1-Pipe.7-0 ROOT ; 
       lines_in_a_ball10-Pipe.4-0 ROOT ; 
       lines_in_a_ball11-Pipe.4-0 ROOT ; 
       lines_in_a_ball12-Pipe.4-0 ROOT ; 
       lines_in_a_ball13-Pipe.3-0 ROOT ; 
       lines_in_a_ball2-Pipe.6-0 ROOT ; 
       lines_in_a_ball3-Pipe.5-0 ROOT ; 
       lines_in_a_ball4-Pipe.4-0 ROOT ; 
       lines_in_a_ball5-Pipe.3-0 ROOT ; 
       lines_in_a_ball6-Pipe.4-0 ROOT ; 
       lines_in_a_ball7-Pipe.3-0 ROOT ; 
       lines_in_a_ball8-Pipe.3-0 ROOT ; 
       lines_in_a_ball9-Pipe.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-def8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 4 220 RELDATA SCLE 1 0.2300566 1 ROLL 0 TRNS 0 1.886094 0 EndOfRELDATA ; 
       17 4 220 2 ; 
       19 3 110 ; 
       20 3 110 ; 
       21 11 220 RELDATA SCLE 1 0.4811881 1 ROLL 0 TRNS 0 3.944969 0 EndOfRELDATA ; 
       21 11 220 2 ; 
       22 8 220 RELDATA SCLE 1 0.5678109 1 ROLL 0 TRNS 0 4.655136 0 EndOfRELDATA ; 
       22 8 220 2 ; 
       23 6 220 RELDATA SCLE 1 0.339076 1 ROLL 0 TRNS 0 2.779878 0 EndOfRELDATA ; 
       23 6 220 2 ; 
       24 7 220 RELDATA SCLE 1 0.3344646 1 ROLL 0 TRNS 0 2.742072 0 EndOfRELDATA ; 
       24 7 220 2 ; 
       26 12 220 RELDATA SCLE 1 1.583674 1 ROLL 0 TRNS 0 12.98358 0 EndOfRELDATA ; 
       26 12 220 2 ; 
       27 13 220 RELDATA SCLE 1 0.1718886 1 ROLL 0 TRNS 0 1.409211 0 EndOfRELDATA ; 
       27 13 220 2 ; 
       30 10 220 RELDATA SCLE 1 0.2952074 1 ROLL 0 TRNS 0 2.420226 0 EndOfRELDATA ; 
       30 10 220 2 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 45 -2 0 MPRFLG 0 ; 
       8 SCHEM 40 -2 0 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 MPRFLG 0 ; 
       10 SCHEM 30 -2 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 20 -2 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 25 -2 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 14.94469 -3.103402 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 17.68299 -3.346807 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 40.50426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       23 SCHEM 43.00426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 45.53152 -3.06715 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 48.03152 0 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 20.50426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 23.00426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 25.50426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 28.00426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 30.50426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       31 SCHEM 33.00426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       32 SCHEM 35.50426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       33 SCHEM 38.00426 -3.274893 0 USR DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
