SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       done3-cam_int1.2-0 ROOT ; 
       done3-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       done3-135.2-0 ; 
       done3-45.2-0 ; 
       done3-null1.1-0 ROOT ; 
       done3-null2.1-0 ROOT ; 
       done3-nurbs1.2-0 ; 
       done3-nurbs11.2-0 ; 
       done3-nurbs12.2-0 ; 
       done3-nurbs13.2-0 ; 
       done3-nurbs14.2-0 ; 
       done3-nurbs15.2-0 ; 
       done3-nurbs16.2-0 ; 
       done3-nurbs2.2-0 ; 
       done3-nurbs3.2-0 ; 
       done3-nurbs4.2-0 ; 
       done3-nurbs5.2-0 ; 
       done3-nurbs6.2-0 ; 
       done3-nurbs7.2-0 ; 
       done3-Pipe.1-0 ROOT ; 
       done3-sphere1.1-0 ROOT ; 
       done3-X.2-0 ; 
       done3-Z.2-0 ; 
       lines_in_a_ball1-Pipe.14-0 ROOT ; 
       lines_in_a_ball10-Pipe.10-0 ROOT ; 
       lines_in_a_ball11-Pipe.10-0 ROOT ; 
       lines_in_a_ball12-Pipe.11-0 ROOT ; 
       lines_in_a_ball13-Pipe.6-0 ROOT ; 
       lines_in_a_ball2-Pipe.13-0 ROOT ; 
       lines_in_a_ball3-Pipe.11-0 ROOT ; 
       lines_in_a_ball4-Pipe.11-0 ROOT ; 
       lines_in_a_ball5-Pipe.9-0 ROOT ; 
       lines_in_a_ball6-Pipe.11-0 ROOT ; 
       lines_in_a_ball7-Pipe.8-0 ROOT ; 
       lines_in_a_ball8-Pipe.7-0 ROOT ; 
       lines_in_a_ball9-Pipe.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-done3.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       19 3 110 ; 
       20 3 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 30 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 45 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 40 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 30 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 14.94469 -3.103402 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 17.68299 -3.346807 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 40.50426 -3.274893 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       23 SCHEM 43.00426 -3.274893 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 45.53152 -3.06715 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 48.03152 0 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 20.50426 -3.274893 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 23.00426 -3.274893 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 25.50426 -3.274893 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 28.00426 -3.274893 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 30.50426 -3.274893 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       31 SCHEM 33.00426 -3.274893 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       32 SCHEM 35.50426 -3.274893 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       33 SCHEM 38.00426 -3.274893 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
