SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.9-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1.9-0 ROOT ; 
       rix_carrier_s-inf_light2_1.9-0 ROOT ; 
       rix_carrier_s-inf_light3_1.9-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.9-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.9-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       edit_heart-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       edit_heart-cube1.1-0 ROOT ; 
       edit_heart-cyl3.1-0 ROOT ; 
       ss99-bmerge1.2-0 ; 
       ss99-cyl2.1-0 ; 
       ss99-null1.1-0 ROOT ; 
       ss99-Poly_stomach.6-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/guts ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-edit_heart.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       edit_heart-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 2 110 ; 
       3 5 110 ; 
       2 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 10 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 15 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 17.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 0 -8 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       0 SCHEM 20 0 0 SRT 1.884 1.884 1.884 0 0 0 0 -107.6487 0 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 SRT 6.600194 12.60771 6.600194 0 0 0 0 -145.632 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
