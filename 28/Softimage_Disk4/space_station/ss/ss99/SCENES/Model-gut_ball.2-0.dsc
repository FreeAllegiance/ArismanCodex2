SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       gut_ball-cam_int1.2-0 ROOT ; 
       gut_ball-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       gut_ball-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       gut_ball-135.2-0 ; 
       gut_ball-45.2-0 ; 
       gut_ball-null1.1-0 ROOT ; 
       gut_ball-null2.1-0 ROOT ; 
       gut_ball-null3.1-0 ; 
       gut_ball-nurbs1.2-0 ; 
       gut_ball-nurbs11.2-0 ; 
       gut_ball-nurbs12.2-0 ; 
       gut_ball-nurbs13.2-0 ; 
       gut_ball-nurbs14.2-0 ; 
       gut_ball-nurbs15.2-0 ; 
       gut_ball-nurbs16.2-0 ; 
       gut_ball-nurbs2.2-0 ; 
       gut_ball-nurbs3.2-0 ; 
       gut_ball-nurbs4.2-0 ; 
       gut_ball-nurbs5.2-0 ; 
       gut_ball-nurbs6.2-0 ; 
       gut_ball-nurbs7.2-0 ; 
       gut_ball-Pipe.1-0 ; 
       gut_ball-Pipe_1.1-0 ; 
       gut_ball-Pipe_10.13-0 ; 
       gut_ball-Pipe_11.13-0 ; 
       gut_ball-Pipe_12.16-0 ; 
       gut_ball-Pipe_13.13-0 ; 
       gut_ball-Pipe_14.14-0 ; 
       gut_ball-Pipe_15.17-0 ; 
       gut_ball-Pipe_16.17-0 ; 
       gut_ball-Pipe_17.18-0 ; 
       gut_ball-Pipe_18.19-0 ; 
       gut_ball-Pipe_19.1-0 ; 
       gut_ball-Pipe_2.1-0 ; 
       gut_ball-Pipe_3.1-0 ; 
       gut_ball-Pipe_4.1-0 ; 
       gut_ball-Pipe_5.1-0 ; 
       gut_ball-Pipe_6.1-0 ; 
       gut_ball-Pipe_7.17-0 ; 
       gut_ball-Pipe_8.15-0 ; 
       gut_ball-Pipe_9.16-0 ; 
       gut_ball-sphere1.2-0 ROOT ; 
       gut_ball-X.2-0 ; 
       gut_ball-Z.2-0 ; 
       lines_in_a_ball13-Pipe.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-gut_ball.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 4 110 ; 
       39 3 110 ; 
       40 3 110 ; 
       28 4 110 ; 
       37 4 110 ; 
       36 4 110 ; 
       35 4 110 ; 
       27 4 110 ; 
       26 4 110 ; 
       25 4 110 ; 
       24 4 110 ; 
       22 4 110 ; 
       23 4 110 ; 
       21 4 110 ; 
       20 4 110 ; 
       34 4 110 ; 
       33 4 110 ; 
       32 4 110 ; 
       31 4 110 ; 
       30 4 110 ; 
       19 4 110 ; 
       29 4 110 ; 
       4 38 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       38 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 6.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 35 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 40 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 37.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 30 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       38 SCHEM 72.5 0 0 SRT 0.922 0.922 0.922 0 0 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 50 -4 0 MPRFLG 0 ; 
       37 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       36 SCHEM 75 -4 0 MPRFLG 0 ; 
       35 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       41 SCHEM 45 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 55 -4 0 MPRFLG 0 ; 
       25 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 60 -4 0 MPRFLG 0 ; 
       22 SCHEM 65 -4 0 MPRFLG 0 ; 
       23 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 70 -4 0 MPRFLG 0 ; 
       34 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 85 -4 0 MPRFLG 0 ; 
       32 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 90 -4 0 MPRFLG 0 ; 
       30 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 80 -4 0 MPRFLG 0 ; 
       29 SCHEM 95 -4 0 MPRFLG 0 ; 
       4 SCHEM 71.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
