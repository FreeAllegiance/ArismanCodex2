SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       gut_model-cam_int1.9-0 ROOT ; 
       gut_model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       gut_model-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       gut_model-sphere2.3-0 ROOT ; 
       gut_model-spline1.3-0 ROOT ; 
       gut_model-spline2.3-0 ROOT ; 
       make_stomach-Pipe.2-0 ROOT ; 
       make_stomach-Pipe3.3-0 ROOT ; 
       make_stomach-sphere3.1-0 ; 
       make_stomach1-Pipe.3-0 ROOT ; 
       make_stomach2-Pipe.4-0 ROOT ; 
       make_stomach3-Pipe.2-0 ROOT ; 
       make_stomach4-Pipe3.1-0 ROOT ; 
       make_stomach4-sphere3.1-0 ; 
       make_stomach5-Pipe3.1-0 ROOT ; 
       make_stomach5-sphere3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-gut_model.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       gut_model-3DMarble_Golden1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       gut_model-cloud1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 4 110 ; 
       10 9 110 ; 
       12 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 0 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 MPRFLG 0 ; 
       1 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 5.264774 MPRFLG 0 ; 
       2 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 20 0 0 SRT 1 1 1 0 -16.868 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 22.5 0 0 SRT 1 1 1 0 -14.906 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 25 0 0 SRT 1 1 1 0 1.850001 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 25 -2 0 MPRFLG 0 ; 
       11 SCHEM 27.5 0 0 SRT 1 1 1 0 3.900001 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
