SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       lines_in_a_ball-cam_int1.1-0 ROOT ; 
       lines_in_a_ball-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       lines_in_a_ball-135.2-0 ; 
       lines_in_a_ball-45.2-0 ; 
       lines_in_a_ball-null1.1-0 ROOT ; 
       lines_in_a_ball-null2.1-0 ROOT ; 
       lines_in_a_ball-nurbs1.2-0 ; 
       lines_in_a_ball-nurbs11.2-0 ; 
       lines_in_a_ball-nurbs12.2-0 ; 
       lines_in_a_ball-nurbs13.2-0 ; 
       lines_in_a_ball-nurbs14.2-0 ; 
       lines_in_a_ball-nurbs15.2-0 ; 
       lines_in_a_ball-nurbs16.2-0 ; 
       lines_in_a_ball-nurbs2.2-0 ; 
       lines_in_a_ball-nurbs3.2-0 ; 
       lines_in_a_ball-nurbs4.2-0 ; 
       lines_in_a_ball-nurbs5.2-0 ; 
       lines_in_a_ball-nurbs6.2-0 ; 
       lines_in_a_ball-nurbs7.2-0 ; 
       lines_in_a_ball-Pipe.1-0 ROOT ; 
       lines_in_a_ball-sphere1.1-0 ROOT ; 
       lines_in_a_ball-X.2-0 ; 
       lines_in_a_ball-Z.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-lines_in_a_ball.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       19 3 110 ; 
       20 3 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 MPRFLG 0 ; 
       2 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 45 -2 0 MPRFLG 0 ; 
       8 SCHEM 40 -2 0 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 MPRFLG 0 ; 
       10 SCHEM 30 -2 0 MPRFLG 0 ; 
       3 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 20 -2 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 25 -2 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 47.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
