SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       make_stomach-cam_int1.1-0 ROOT ; 
       make_stomach-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       make_stomach-Pipe.1-0 ROOT ; 
       make_stomach-Pipe1.1-0 ROOT ; 
       make_stomach-Pipe3.1-0 ROOT ; 
       make_stomach-spline3.1-0 ROOT ; 
       make_stomach-spline4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-make_stomach.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 220 RELDATA SCLE 1 3.218315 1 ROLL 0 TRNS 0 27.15782 0 EndOfRELDATA ; 
       1 3 220 2 ; 
       2 4 220 RELDATA SCLE 1 3.253473 1 ROLL 0 TRNS 0 26.72921 0 EndOfRELDATA ; 
       2 4 220 2 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0.006774316 0.1067456 -2.020191 MPRFLG 0 ; 
       0 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 10 0 0 SRT 1 1 1 0 0 0 -0.6269886 0.06713538 -2.020191 MPRFLG 0 ; 
       2 SCHEM 12.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
