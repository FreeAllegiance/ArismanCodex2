SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       make_stomach2-cam_int1.1-0 ROOT ; 
       make_stomach2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       make_stomach2-135.1-0 ROOT ; 
       make_stomach2-45.1-0 ROOT ; 
       make_stomach2-nurbs1.1-0 ROOT ; 
       make_stomach2-nurbs10.1-0 ROOT ; 
       make_stomach2-nurbs11.1-0 ROOT ; 
       make_stomach2-nurbs12.1-0 ROOT ; 
       make_stomach2-nurbs13.1-0 ROOT ; 
       make_stomach2-nurbs14.1-0 ROOT ; 
       make_stomach2-nurbs15.1-0 ROOT ; 
       make_stomach2-nurbs16.1-0 ROOT ; 
       make_stomach2-nurbs17.1-0 ROOT ; 
       make_stomach2-nurbs2.1-0 ROOT ; 
       make_stomach2-nurbs3.1-0 ROOT ; 
       make_stomach2-nurbs4.1-0 ROOT ; 
       make_stomach2-nurbs5.1-0 ROOT ; 
       make_stomach2-nurbs6.1-0 ROOT ; 
       make_stomach2-nurbs7.1-0 ROOT ; 
       make_stomach2-nurbs8.1-0 ROOT ; 
       make_stomach2-nurbs9.1-0 ROOT ; 
       make_stomach2-Pipe.1-0 ROOT ; 
       make_stomach2-sphere1.1-0 ROOT ; 
       make_stomach2-X.1-0 ROOT ; 
       make_stomach2-Z.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-make_stomach2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 15 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 40 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 45 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 47.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 50 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 52.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 55 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 22.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 27.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 32.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 35 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 57.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       20 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
