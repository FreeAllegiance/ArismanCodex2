SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       make_stomach2-cam_int1.2-0 ROOT ; 
       make_stomach2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       make_stomach2-135.2-0 ROOT ; 
       make_stomach2-45.2-0 ROOT ; 
       make_stomach2-nurbs1.2-0 ROOT ; 
       make_stomach2-nurbs11.2-0 ROOT ; 
       make_stomach2-nurbs12.2-0 ROOT ; 
       make_stomach2-nurbs13.2-0 ROOT ; 
       make_stomach2-nurbs14.2-0 ROOT ; 
       make_stomach2-nurbs15.2-0 ROOT ; 
       make_stomach2-nurbs16.2-0 ROOT ; 
       make_stomach2-nurbs2.2-0 ROOT ; 
       make_stomach2-nurbs3.2-0 ROOT ; 
       make_stomach2-nurbs4.2-0 ROOT ; 
       make_stomach2-nurbs5.2-0 ROOT ; 
       make_stomach2-nurbs6.2-0 ROOT ; 
       make_stomach2-nurbs7.2-0 ROOT ; 
       make_stomach2-Pipe.2-0 ROOT ; 
       make_stomach2-sphere1.2-0 ROOT ; 
       make_stomach2-X.2-0 ROOT ; 
       make_stomach2-Z.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-make_stomach2.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 15 2.383202 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 32.5 2.948707 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 35 2.908314 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 37.5 3.069887 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 40 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 45 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 17.5 2.221629 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 20 2.181235 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 22.5 2.423595 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 25 2.423595 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 27.5 2.302415 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 30 3.069887 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 47.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
