SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.40-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1_1.12-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1.12-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1.12-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1.12-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1.12-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1.12-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       remap-mat10.5-0 ; 
       remap-mat11.3-0 ; 
       remap-mat12.2-0 ; 
       remap-mat13.1-0 ; 
       remap-mat14.1-0 ; 
       remap-mat15.1-0 ; 
       remap-mat17.4-0 ; 
       remap-mat19.1-0 ; 
       remap-mat20.1-0 ; 
       remap-mat21.1-0 ; 
       remap-mat3.4-0 ; 
       remap-mat4.9-0 ; 
       remap-mat5.3-0 ; 
       remap-mat6.3-0 ; 
       remap-mat7.8-0 ; 
       remap-mat8.7-0 ; 
       remap-mat9.6-0 ; 
       rix_post_sPTL-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       ss99-bmerge1.2-0 ; 
       ss99-bmerge2.1-0 ; 
       ss99-cube1.1-0 ; 
       ss99-fuselg4.1-0 ; 
       ss99-fuselg4_1.1-0 ; 
       ss99-fuselg5.1-0 ; 
       ss99-fuselg6.1-0 ; 
       ss99-null1.25-0 ROOT ; 
       ss99-null2.1-0 ; 
       ss99-Poly_stomach.6-0 ; 
       ss99-sphere1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/ss99 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-remap.27-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       remap-t2d10.6-0 ; 
       remap-t2d11.5-0 ; 
       remap-t2d12.4-0 ; 
       remap-t2d13.2-0 ; 
       remap-t2d14.1-0 ; 
       remap-t2d15.1-0 ; 
       remap-t2d16.1-0 ; 
       remap-t2d17.4-0 ; 
       remap-t2d18.1-0 ; 
       remap-t2d19.1-0 ; 
       remap-t2d4.2-0 ; 
       remap-t2d5.2-0 ; 
       remap-t2d6.2-0 ; 
       remap-t2d7.10-0 ; 
       remap-t2d8.9-0 ; 
       remap-t2d9.7-0 ; 
       rix_post_sPTL-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       2 1 110 ; 
       9 0 110 ; 
       10 1 110 ; 
       1 9 110 ; 
       8 0 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       3 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       2 6 300 ; 
       2 9 300 ; 
       9 10 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 7 300 ; 
       1 8 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       6 5 300 ; 
       3 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       10 12 401 ; 
       11 3 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       9 9 401 ; 
       7 8 401 ; 
       8 7 401 ; 
       17 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       7 SCHEM 23.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       10 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       10 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
