SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       test-cam_int1.7-0 ROOT ; 
       test-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       test-null1.2-0 ROOT ; 
       test-Pipe.2-0 ; 
       test-Pipe_1.4-0 ; 
       test-Pipe_2.3-0 ; 
       test-Pipe3.1-0 ; 
       test-Pipe3_1.1-0 ; 
       test-Pipe3_2.3-0 ; 
       test-sphere2.3-0 ; 
       test-sphere3.1-0 ; 
       test-sphere3_1.1-0 ; 
       test-sphere3_2.1-0 ; 
       test-sphere6.2-0 ROOT ; 
       test-sphere7.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Model-test.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 6 110 ; 
       9 4 110 ; 
       10 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 MPRFLG 0 ; 
       11 SCHEM 2.5 0 0 SRT 2.15 2.15 2.15 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 22.5 0 0 SRT 0.4773 0.4773 0.4773 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
