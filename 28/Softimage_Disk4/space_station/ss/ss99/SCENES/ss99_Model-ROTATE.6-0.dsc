SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss99-Poly_stomachShp.33-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.76-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1_1.45-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1.45-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1.45-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1.45-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1.45-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1.45-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       rix_post_sPTL-mat4.1-0 ; 
       ROTATE-mat10.1-0 ; 
       ROTATE-mat11.1-0 ; 
       ROTATE-mat12.1-0 ; 
       ROTATE-mat13.1-0 ; 
       ROTATE-mat14.1-0 ; 
       ROTATE-mat15.1-0 ; 
       ROTATE-mat17.1-0 ; 
       ROTATE-mat19.1-0 ; 
       ROTATE-mat20.1-0 ; 
       ROTATE-mat21.1-0 ; 
       ROTATE-mat22.1-0 ; 
       ROTATE-mat23.1-0 ; 
       ROTATE-mat3.1-0 ; 
       ROTATE-mat4.1-0 ; 
       ROTATE-mat5.1-0 ; 
       ROTATE-mat6.1-0 ; 
       ROTATE-mat7.1-0 ; 
       ROTATE-mat8.1-0 ; 
       ROTATE-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       ss99-bay.1-0 ; 
       ss99-bmerge1.2-0 ; 
       ss99-bmerge2.1-0 ; 
       ss99-cube1.1-0 ; 
       ss99-fuselg4.1-0 ; 
       ss99-fuselg4_1.1-0 ; 
       ss99-fuselg5.1-0 ; 
       ss99-fuselg6.1-0 ; 
       ss99-garage1A.1-0 ; 
       ss99-garage1B.1-0 ; 
       ss99-garage1C.1-0 ; 
       ss99-garage1D.1-0 ; 
       ss99-garage1E.1-0 ; 
       ss99-launch1.1-0 ; 
       ss99-null1.55-0 ROOT ; 
       ss99-null2.1-0 ; 
       ss99-Poly_stomach.6-0 ; 
       ss99-sphere1.1-0 ; 
       ss99-ss01.1-0 ; 
       ss99-ss01_1.1-0 ; 
       ss99-ss01_2.1-0 ; 
       ss99-ss01_3.1-0 ; 
       ss99-ss01_4.1-0 ; 
       ss99-ss01_5.1-0 ; 
       ss99-ss01_6.1-0 ; 
       ss99-ss01_7.1-0 ; 
       ss99-ss01_8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/rixbay ; 
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/ss99 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss99_Model-ROTATE.6-0 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES NBELEM 1     
       ss99-Poly_stomachShp.33-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       rix_post_sPTL-t2d3.1-0 ; 
       ROTATE-t2d10.1-0 ; 
       ROTATE-t2d11.1-0 ; 
       ROTATE-t2d12.1-0 ; 
       ROTATE-t2d13.1-0 ; 
       ROTATE-t2d14.1-0 ; 
       ROTATE-t2d15.1-0 ; 
       ROTATE-t2d16.1-0 ; 
       ROTATE-t2d17.1-0 ; 
       ROTATE-t2d18.1-0 ; 
       ROTATE-t2d19.1-0 ; 
       ROTATE-t2d20.1-0 ; 
       ROTATE-t2d21.1-0 ; 
       ROTATE-t2d22.1-0 ; 
       ROTATE-t2d4.1-0 ; 
       ROTATE-t2d5.1-0 ; 
       ROTATE-t2d6.1-0 ; 
       ROTATE-t2d7.1-0 ; 
       ROTATE-t2d8.1-0 ; 
       ROTATE-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 14 110 ; 
       2 16 110 ; 
       3 2 110 ; 
       4 15 110 ; 
       5 15 110 ; 
       6 15 110 ; 
       7 15 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 2 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 14 110 ; 
       22 14 110 ; 
       23 14 110 ; 
       24 14 110 ; 
       25 14 110 ; 
       26 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER SHAPES 
       16 0 200 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 14 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       2 9 300 ; 
       3 7 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       4 0 300 ; 
       5 4 300 ; 
       6 5 300 ; 
       7 6 300 ; 
       16 13 300 ; 
       16 15 300 ; 
       16 16 300 ; 
       17 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 12 401 ; 
       8 9 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 13 401 ; 
       13 16 401 ; 
       14 4 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES CHAPTER ANIMATION 
       0 0 15010 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -8 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 15 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -4 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 22.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 30 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 28.75 0 0 SRT 1 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       16 SCHEM 5 -4 0 MPRFLG 0 ; 
       17 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 6.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 25 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
