SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss99-Poly_stomachShp.27-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.67-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1_1.39-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1.39-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1.39-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1.39-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1.39-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1.39-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       rix_post_sPTL-mat4.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat14.1-0 ; 
       STATIC-mat15.1-0 ; 
       STATIC-mat17.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat20.1-0 ; 
       STATIC-mat21.1-0 ; 
       STATIC-mat22.1-0 ; 
       STATIC-mat23.1-0 ; 
       STATIC-mat3.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat5.1-0 ; 
       STATIC-mat6.1-0 ; 
       STATIC-mat7.1-0 ; 
       STATIC-mat8.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       ss99-bmerge1.2-0 ; 
       ss99-bmerge2.1-0 ; 
       ss99-cube1.1-0 ; 
       ss99-fuselg4.1-0 ; 
       ss99-fuselg4_1.1-0 ; 
       ss99-fuselg5.1-0 ; 
       ss99-fuselg6.1-0 ; 
       ss99-null1.50-0 ROOT ; 
       ss99-null2.1-0 ; 
       ss99-Poly_stomach.6-0 ; 
       ss99-sphere1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/rixbay ; 
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/ss99 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss99_Model-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES NBELEM 1     
       ss99-Poly_stomachShp.27-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       rix_post_sPTL-t2d3.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d18.1-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d21.1-0 ; 
       STATIC-t2d22.1-0 ; 
       STATIC-t2d4.1-0 ; 
       STATIC-t2d5.1-0 ; 
       STATIC-t2d6.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 9 110 ; 
       2 1 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER SHAPES 
       9 0 200 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       1 9 300 ; 
       2 7 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       3 0 300 ; 
       4 4 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       9 13 300 ; 
       9 15 300 ; 
       9 16 300 ; 
       10 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 12 401 ; 
       8 9 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 13 401 ; 
       13 16 401 ; 
       14 4 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       0 0 401 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES CHAPTER ANIMATION 
       0 0 15010 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -2 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 10 0 0 SRT 1 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 6.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 25 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
