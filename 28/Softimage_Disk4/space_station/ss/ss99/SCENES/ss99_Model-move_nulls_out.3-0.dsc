SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss99-Poly_stomachShp.36-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.79-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1_1_1.3-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1_1.3-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1_1.3-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1_1.3-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1_1.3-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1_1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       move_nulls_out-mat10.1-0 ; 
       move_nulls_out-mat11.1-0 ; 
       move_nulls_out-mat12.1-0 ; 
       move_nulls_out-mat13.1-0 ; 
       move_nulls_out-mat14.1-0 ; 
       move_nulls_out-mat15.1-0 ; 
       move_nulls_out-mat17.1-0 ; 
       move_nulls_out-mat19.1-0 ; 
       move_nulls_out-mat20.1-0 ; 
       move_nulls_out-mat21.1-0 ; 
       move_nulls_out-mat22.1-0 ; 
       move_nulls_out-mat23.1-0 ; 
       move_nulls_out-mat3.1-0 ; 
       move_nulls_out-mat337.1-0 ; 
       move_nulls_out-mat338.1-0 ; 
       move_nulls_out-mat339.1-0 ; 
       move_nulls_out-mat340.1-0 ; 
       move_nulls_out-mat4.1-0 ; 
       move_nulls_out-mat5.1-0 ; 
       move_nulls_out-mat6.1-0 ; 
       move_nulls_out-mat7.1-0 ; 
       move_nulls_out-mat8.1-0 ; 
       move_nulls_out-mat9.1-0 ; 
       rix_post_sPTL-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       ss99-bay.1-0 ; 
       ss99-bmerge1.2-0 ; 
       ss99-bmerge2.1-0 ; 
       ss99-cube1.1-0 ; 
       ss99-fuselg4.1-0 ; 
       ss99-fuselg4_1.1-0 ; 
       ss99-fuselg5.1-0 ; 
       ss99-fuselg6.1-0 ; 
       ss99-garage1A.1-0 ; 
       ss99-garage1B.1-0 ; 
       ss99-garage1C.1-0 ; 
       ss99-garage1D.1-0 ; 
       ss99-garage1E.1-0 ; 
       ss99-launch1.1-0 ; 
       ss99-null1.58-0 ROOT ; 
       ss99-null2.1-0 ; 
       ss99-null3.1-0 ; 
       ss99-Poly_stomach.6-0 ; 
       ss99-sphere1.1-0 ; 
       ss99-ss01.1-0 ; 
       ss99-ss01_1.1-0 ; 
       ss99-ss01_10.1-0 ; 
       ss99-ss01_11.1-0 ; 
       ss99-ss01_12.1-0 ; 
       ss99-ss01_2.1-0 ; 
       ss99-ss01_3.1-0 ; 
       ss99-ss01_4.1-0 ; 
       ss99-ss01_5.1-0 ; 
       ss99-ss01_6.1-0 ; 
       ss99-ss01_7.1-0 ; 
       ss99-ss01_8.1-0 ; 
       ss99-ss01_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/rixbay ; 
       E:/Pete_Data2/space_station/ss/ss99/PICTURES/ss99 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss99_Model-move_nulls_out.3-0 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES NBELEM 1     
       ss99-Poly_stomachShp.36-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       move_nulls_out-t2d10.1-0 ; 
       move_nulls_out-t2d11.1-0 ; 
       move_nulls_out-t2d12.1-0 ; 
       move_nulls_out-t2d13.1-0 ; 
       move_nulls_out-t2d14.1-0 ; 
       move_nulls_out-t2d15.1-0 ; 
       move_nulls_out-t2d16.1-0 ; 
       move_nulls_out-t2d17.1-0 ; 
       move_nulls_out-t2d18.1-0 ; 
       move_nulls_out-t2d19.1-0 ; 
       move_nulls_out-t2d20.1-0 ; 
       move_nulls_out-t2d21.1-0 ; 
       move_nulls_out-t2d22.1-0 ; 
       move_nulls_out-t2d4.1-0 ; 
       move_nulls_out-t2d5.1-0 ; 
       move_nulls_out-t2d6.1-0 ; 
       move_nulls_out-t2d7.1-0 ; 
       move_nulls_out-t2d8.1-0 ; 
       move_nulls_out-t2d9.1-0 ; 
       rix_post_sPTL-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 14 110 ; 
       2 17 110 ; 
       3 2 110 ; 
       4 15 110 ; 
       5 15 110 ; 
       6 15 110 ; 
       7 15 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       15 1 110 ; 
       16 14 110 ; 
       17 1 110 ; 
       18 2 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 16 110 ; 
       22 16 110 ; 
       23 16 110 ; 
       24 14 110 ; 
       25 14 110 ; 
       26 14 110 ; 
       27 14 110 ; 
       28 14 110 ; 
       29 14 110 ; 
       30 14 110 ; 
       31 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER SHAPES 
       17 0 200 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 17 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       2 8 300 ; 
       3 6 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       4 23 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       7 5 300 ; 
       17 12 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       18 7 300 ; 
       21 15 300 ; 
       22 14 300 ; 
       23 13 300 ; 
       31 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 11 401 ; 
       7 8 401 ; 
       8 7 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 12 401 ; 
       12 15 401 ; 
       17 3 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 19 401 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES CHAPTER ANIMATION 
       0 0 15010 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 15 -8 0 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 25 -6 0 MPRFLG 0 ; 
       8 SCHEM 30 -4 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 35 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 40 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 33.75 0 0 SRT 1 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       16 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 15 -4 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 10 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 2.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 67.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 70 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 72.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 75 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 16.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 25 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
