SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.12-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       ADD_launch-mat1.1-0 ; 
       ADD_launch-mat10.1-0 ; 
       ADD_launch-mat11.1-0 ; 
       ADD_launch-mat12.1-0 ; 
       ADD_launch-mat13.1-0 ; 
       ADD_launch-mat14.1-0 ; 
       ADD_launch-mat15.1-0 ; 
       ADD_launch-mat16.1-0 ; 
       ADD_launch-mat17.1-0 ; 
       ADD_launch-mat18.1-0 ; 
       ADD_launch-mat19.1-0 ; 
       ADD_launch-mat2.1-0 ; 
       ADD_launch-mat20.1-0 ; 
       ADD_launch-mat21.1-0 ; 
       ADD_launch-mat22.1-0 ; 
       ADD_launch-mat23.1-0 ; 
       ADD_launch-mat24.1-0 ; 
       ADD_launch-mat25.1-0 ; 
       ADD_launch-mat26.1-0 ; 
       ADD_launch-mat27.1-0 ; 
       ADD_launch-mat3.1-0 ; 
       ADD_launch-mat4.1-0 ; 
       ADD_launch-mat5.1-0 ; 
       ADD_launch-mat6.1-0 ; 
       ADD_launch-mat7.1-0 ; 
       ADD_launch-mat8.1-0 ; 
       ADD_launch-mat9.1-0 ; 
       ADD_launch-port_red-left.1-0.1-0 ; 
       ADD_launch-port_red-left.1-18.1-0 ; 
       ADD_launch-port_red-left.1-19.1-0 ; 
       ADD_launch-port_red-left.1-20.1-0 ; 
       ADD_launch-port_red-left.1-21.1-0 ; 
       ADD_launch-port_red-left.1-22.1-0 ; 
       ADD_launch-port_red-left.1-23.1-0 ; 
       ADD_launch-port_red-left.1-24.1-0 ; 
       ADD_launch-port_red-left.1-25.1-0 ; 
       ADD_launch-port_red-left.1-26.1-0 ; 
       ADD_launch-port_red-left.1-27.1-0 ; 
       ADD_launch-port_red-left.1-28.1-0 ; 
       ADD_launch-port_red-left.1-29.1-0 ; 
       ADD_launch-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       utl06-antenn1.1-0 ; 
       utl06-antenn2.1-0 ; 
       utl06-beacon.1-0 ; 
       utl06-fuselg.2-0 ; 
       utl06-luanch1.1-0 ; 
       utl06-shield1a.1-0 ; 
       utl06-shield1b.1-0 ; 
       utl06-shield2a.1-0 ; 
       utl06-shield2b.1-0 ; 
       utl06-shield3a.1-0 ; 
       utl06-shield3b.1-0 ; 
       utl06-shield4a.1-0 ; 
       utl06-shield4b.1-0 ; 
       utl06-SS0a.2-0 ; 
       utl06-SS0b.1-0 ; 
       utl06-SS0c.1-0 ; 
       utl06-SS0d.1-0 ; 
       utl06-SSa1.1-0 ; 
       utl06-SSa1_1.2-0 ; 
       utl06-SSa2.1-0 ; 
       utl06-SSa5.1-0 ; 
       utl06-SSa6.1-0 ; 
       utl06-SSb1.1-0 ; 
       utl06-SSb2.1-0 ; 
       utl06-SSb3.1-0 ; 
       utl06-SSc1.1-0 ; 
       utl06-SSc2.1-0 ; 
       utl06-SSc3.1-0 ; 
       utl06-SSd1.1-0 ; 
       utl06-SSd2.1-0 ; 
       utl06-SSd3.1-0 ; 
       utl06-tractr0.1-0 ; 
       utl06-tractr1.1-0 ; 
       utl06-tractr1a.1-0 ; 
       utl06-tractr1b.1-0 ; 
       utl06-tractr2.1-0 ; 
       utl06-tractr2a.1-0 ; 
       utl06-tractr2b.1-0 ; 
       utl06-tractr3.1-0 ; 
       utl06-tractr3a.1-0 ; 
       utl06-tractr3b.1-0 ; 
       utl06-tractr4.1-0 ; 
       utl06-tractr4a.1-0 ; 
       utl06-tractr4b.1-0 ; 
       utl06-utl06.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl06/PICTURES/utl6 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl06-ADD_launch.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       ADD_launch-t2d1.2-0 ; 
       ADD_launch-t2d10.2-0 ; 
       ADD_launch-t2d11.2-0 ; 
       ADD_launch-t2d12.2-0 ; 
       ADD_launch-t2d13.2-0 ; 
       ADD_launch-t2d14.2-0 ; 
       ADD_launch-t2d15.2-0 ; 
       ADD_launch-t2d16.2-0 ; 
       ADD_launch-t2d17.2-0 ; 
       ADD_launch-t2d18.2-0 ; 
       ADD_launch-t2d19.2-0 ; 
       ADD_launch-t2d2.2-0 ; 
       ADD_launch-t2d20.2-0 ; 
       ADD_launch-t2d21.2-0 ; 
       ADD_launch-t2d22.2-0 ; 
       ADD_launch-t2d23.2-0 ; 
       ADD_launch-t2d24.2-0 ; 
       ADD_launch-t2d25.2-0 ; 
       ADD_launch-t2d26.2-0 ; 
       ADD_launch-t2d3.2-0 ; 
       ADD_launch-t2d4.2-0 ; 
       ADD_launch-t2d5.2-0 ; 
       ADD_launch-t2d6.2-0 ; 
       ADD_launch-t2d7.2-0 ; 
       ADD_launch-t2d8.2-0 ; 
       ADD_launch-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       3 44 110 ; 
       4 44 110 ; 
       5 33 110 ; 
       6 33 110 ; 
       7 36 110 ; 
       8 36 110 ; 
       9 39 110 ; 
       10 39 110 ; 
       11 42 110 ; 
       12 42 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 13 110 ; 
       18 1 110 ; 
       19 13 110 ; 
       20 13 110 ; 
       21 0 110 ; 
       22 14 110 ; 
       23 14 110 ; 
       24 14 110 ; 
       25 15 110 ; 
       26 15 110 ; 
       27 15 110 ; 
       28 16 110 ; 
       29 16 110 ; 
       30 16 110 ; 
       31 44 110 ; 
       32 31 110 ; 
       33 32 110 ; 
       34 32 110 ; 
       35 31 110 ; 
       36 35 110 ; 
       37 35 110 ; 
       38 31 110 ; 
       39 38 110 ; 
       40 38 110 ; 
       41 31 110 ; 
       42 41 110 ; 
       43 41 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 20 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       1 22 300 ; 
       2 21 300 ; 
       3 0 300 ; 
       3 11 300 ; 
       5 12 300 ; 
       6 13 300 ; 
       7 17 300 ; 
       8 16 300 ; 
       9 8 300 ; 
       10 7 300 ; 
       11 3 300 ; 
       12 4 300 ; 
       17 28 300 ; 
       18 27 300 ; 
       19 29 300 ; 
       20 30 300 ; 
       21 40 300 ; 
       22 31 300 ; 
       23 32 300 ; 
       24 33 300 ; 
       25 34 300 ; 
       26 35 300 ; 
       27 36 300 ; 
       28 37 300 ; 
       29 38 300 ; 
       30 39 300 ; 
       32 23 300 ; 
       33 10 300 ; 
       34 9 300 ; 
       35 24 300 ; 
       36 14 300 ; 
       37 15 300 ; 
       38 26 300 ; 
       39 6 300 ; 
       40 5 300 ; 
       41 25 300 ; 
       42 1 300 ; 
       43 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 25 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 0 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 11 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 55 -4 0 MPRFLG 0 ; 
       1 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 MPRFLG 0 ; 
       3 SCHEM 50 -2 0 MPRFLG 0 ; 
       4 SCHEM 70 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 30 -8 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 MPRFLG 0 ; 
       9 SCHEM 15 -8 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 20 -8 0 MPRFLG 0 ; 
       13 SCHEM 65 -4 0 MPRFLG 0 ; 
       14 SCHEM 35 -4 0 MPRFLG 0 ; 
       15 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 50 -4 0 MPRFLG 0 ; 
       17 SCHEM 67.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 57.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 62.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 65 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 55 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 37.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 35 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 45 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 40 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 42.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 52.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 47.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 50 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       32 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       34 SCHEM 25 -6 0 MPRFLG 0 ; 
       35 SCHEM 5 -4 0 MPRFLG 0 ; 
       36 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       37 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       40 SCHEM 10 -6 0 MPRFLG 0 ; 
       41 SCHEM 20 -4 0 MPRFLG 0 ; 
       42 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       43 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       44 SCHEM 36.25 0 0 SRT 0.9999999 0.9999999 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
