SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.1-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       sm_gate-inf_light1_1.1-0 ROOT ; 
       sm_gate-inf_light2_1.1-0 ROOT ; 
       sm_gate-light1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       sm_warp_F-mat1.1-0 ; 
       sm_warp_F-mat10.1-0 ; 
       sm_warp_F-mat11.1-0 ; 
       sm_warp_F-mat12.1-0 ; 
       sm_warp_F-mat13.1-0 ; 
       sm_warp_F-mat14.1-0 ; 
       sm_warp_F-mat15.1-0 ; 
       sm_warp_F-mat16.1-0 ; 
       sm_warp_F-mat17.1-0 ; 
       sm_warp_F-mat18.1-0 ; 
       sm_warp_F-mat19.1-0 ; 
       sm_warp_F-mat2.1-0 ; 
       sm_warp_F-mat20.1-0 ; 
       sm_warp_F-mat21.1-0 ; 
       sm_warp_F-mat22.1-0 ; 
       sm_warp_F-mat23.1-0 ; 
       sm_warp_F-mat24.1-0 ; 
       sm_warp_F-mat25.1-0 ; 
       sm_warp_F-mat26.1-0 ; 
       sm_warp_F-mat27.1-0 ; 
       sm_warp_F-mat3.1-0 ; 
       sm_warp_F-mat4.1-0 ; 
       sm_warp_F-mat5.1-0 ; 
       sm_warp_F-mat6.1-0 ; 
       sm_warp_F-mat7.1-0 ; 
       sm_warp_F-mat8.1-0 ; 
       sm_warp_F-mat9.1-0 ; 
       sm_warp_F-port_red-left.1-0.1-0 ; 
       sm_warp_F-port_red-left.1-18.1-0 ; 
       sm_warp_F-port_red-left.1-19.1-0 ; 
       sm_warp_F-port_red-left.1-20.1-0 ; 
       sm_warp_F-port_red-left.1-21.1-0 ; 
       sm_warp_F-port_red-left.1-22.1-0 ; 
       sm_warp_F-port_red-left.1-23.1-0 ; 
       sm_warp_F-port_red-left.1-24.1-0 ; 
       sm_warp_F-port_red-left.1-25.1-0 ; 
       sm_warp_F-port_red-left.1-26.1-0 ; 
       sm_warp_F-port_red-left.1-27.1-0 ; 
       sm_warp_F-port_red-left.1-28.1-0 ; 
       sm_warp_F-port_red-left.1-29.1-0 ; 
       sm_warp_F-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       utl06-antenn1.1-0 ; 
       utl06-antenn2.1-0 ; 
       utl06-beacon.1-0 ; 
       utl06-fuselg.2-0 ; 
       utl06-shield1a.1-0 ; 
       utl06-shield1b.1-0 ; 
       utl06-shield2a.1-0 ; 
       utl06-shield2b.1-0 ; 
       utl06-shield3a.1-0 ; 
       utl06-shield3b.1-0 ; 
       utl06-shield4a.1-0 ; 
       utl06-shield4b.1-0 ; 
       utl06-SS0a.2-0 ; 
       utl06-SS0b.1-0 ; 
       utl06-SS0c.1-0 ; 
       utl06-SS0d.1-0 ; 
       utl06-SSa1.1-0 ; 
       utl06-SSa1_1.2-0 ; 
       utl06-SSa2.1-0 ; 
       utl06-SSa5.1-0 ; 
       utl06-SSa6.1-0 ; 
       utl06-SSb1.1-0 ; 
       utl06-SSb2.1-0 ; 
       utl06-SSb3.1-0 ; 
       utl06-SSc1.1-0 ; 
       utl06-SSc2.1-0 ; 
       utl06-SSc3.1-0 ; 
       utl06-SSd1.1-0 ; 
       utl06-SSd2.1-0 ; 
       utl06-SSd3.1-0 ; 
       utl06-tractr0.1-0 ; 
       utl06-tractr1.1-0 ; 
       utl06-tractr1a.1-0 ; 
       utl06-tractr1b.1-0 ; 
       utl06-tractr2.1-0 ; 
       utl06-tractr2a.1-0 ; 
       utl06-tractr2b.1-0 ; 
       utl06-tractr3.1-0 ; 
       utl06-tractr3a.1-0 ; 
       utl06-tractr3b.1-0 ; 
       utl06-tractr4.1-0 ; 
       utl06-tractr4a.1-0 ; 
       utl06-tractr4b.1-0 ; 
       utl06-utl06.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/utl/utl06/PICTURES/utl6 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl06-sm_warp_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       sm_warp_F-t2d1.1-0 ; 
       sm_warp_F-t2d10.1-0 ; 
       sm_warp_F-t2d11.1-0 ; 
       sm_warp_F-t2d12.1-0 ; 
       sm_warp_F-t2d13.1-0 ; 
       sm_warp_F-t2d14.1-0 ; 
       sm_warp_F-t2d15.1-0 ; 
       sm_warp_F-t2d16.1-0 ; 
       sm_warp_F-t2d17.1-0 ; 
       sm_warp_F-t2d18.1-0 ; 
       sm_warp_F-t2d19.1-0 ; 
       sm_warp_F-t2d2.1-0 ; 
       sm_warp_F-t2d20.1-0 ; 
       sm_warp_F-t2d21.1-0 ; 
       sm_warp_F-t2d22.1-0 ; 
       sm_warp_F-t2d23.1-0 ; 
       sm_warp_F-t2d24.1-0 ; 
       sm_warp_F-t2d25.1-0 ; 
       sm_warp_F-t2d26.1-0 ; 
       sm_warp_F-t2d3.1-0 ; 
       sm_warp_F-t2d4.1-0 ; 
       sm_warp_F-t2d5.1-0 ; 
       sm_warp_F-t2d6.1-0 ; 
       sm_warp_F-t2d7.1-0 ; 
       sm_warp_F-t2d8.1-0 ; 
       sm_warp_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       12 3 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 12 110 ; 
       17 1 110 ; 
       18 12 110 ; 
       19 12 110 ; 
       20 0 110 ; 
       21 13 110 ; 
       22 13 110 ; 
       23 13 110 ; 
       24 14 110 ; 
       25 14 110 ; 
       26 14 110 ; 
       27 15 110 ; 
       28 15 110 ; 
       29 15 110 ; 
       0 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       3 43 110 ; 
       4 32 110 ; 
       5 32 110 ; 
       6 35 110 ; 
       7 35 110 ; 
       8 38 110 ; 
       9 38 110 ; 
       10 41 110 ; 
       11 41 110 ; 
       30 43 110 ; 
       31 30 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 30 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       37 30 110 ; 
       38 37 110 ; 
       39 37 110 ; 
       40 30 110 ; 
       41 40 110 ; 
       42 40 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       16 28 300 ; 
       17 27 300 ; 
       18 29 300 ; 
       19 30 300 ; 
       20 40 300 ; 
       21 31 300 ; 
       22 32 300 ; 
       23 33 300 ; 
       24 34 300 ; 
       25 35 300 ; 
       26 36 300 ; 
       27 37 300 ; 
       28 38 300 ; 
       29 39 300 ; 
       0 20 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       1 22 300 ; 
       2 21 300 ; 
       3 0 300 ; 
       3 11 300 ; 
       4 12 300 ; 
       5 13 300 ; 
       6 17 300 ; 
       7 16 300 ; 
       8 8 300 ; 
       9 7 300 ; 
       10 3 300 ; 
       11 4 300 ; 
       31 23 300 ; 
       32 10 300 ; 
       33 9 300 ; 
       34 24 300 ; 
       35 14 300 ; 
       36 15 300 ; 
       37 26 300 ; 
       38 6 300 ; 
       39 5 300 ; 
       40 25 300 ; 
       41 1 300 ; 
       42 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 25 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 0 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 11 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -56.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -58.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -60.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       12 SCHEM 7 -22.5 0 MPRFLG 0 ; 
       13 SCHEM 7 -16.5 0 MPRFLG 0 ; 
       14 SCHEM 7 -10.5 0 MPRFLG 0 ; 
       15 SCHEM 7 -4.5 0 USR MPRFLG 0 ; 
       16 SCHEM 10.5 -20.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 10.5 -28.5 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 10.5 -22.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 10.5 -24.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 10.5 -30.5 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 10.5 -14.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 10.5 -16.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 10.5 -18.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 10.5 -8.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 10.5 -10.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 10.5 -12.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 10.5 -2.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 10.5 -4.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 10.5 -6.5 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 7 -30.5 0 MPRFLG 0 ; 
       1 SCHEM 7 -28.5 0 MPRFLG 0 ; 
       2 SCHEM 7 -26.5 0 MPRFLG 0 ; 
       3 SCHEM 3.5 -16.5 0 MPRFLG 0 ; 
       4 SCHEM 14 -50.5 0 MPRFLG 0 ; 
       5 SCHEM 14 -52.5 0 MPRFLG 0 ; 
       6 SCHEM 14 -44.5 0 MPRFLG 0 ; 
       7 SCHEM 14 -46.5 0 MPRFLG 0 ; 
       8 SCHEM 14 -32.5 0 MPRFLG 0 ; 
       9 SCHEM 14 -34.5 0 MPRFLG 0 ; 
       10 SCHEM 14 -38.5 0 MPRFLG 0 ; 
       11 SCHEM 14 -40.5 0 MPRFLG 0 ; 
       30 SCHEM 3.5 -43.5 0 MPRFLG 0 ; 
       31 SCHEM 7 -52.5 0 MPRFLG 0 ; 
       32 SCHEM 10.5 -51.5 0 MPRFLG 0 ; 
       33 SCHEM 10.5 -54.5 0 MPRFLG 0 ; 
       34 SCHEM 7 -46.5 0 MPRFLG 0 ; 
       35 SCHEM 10.5 -45.5 0 MPRFLG 0 ; 
       36 SCHEM 10.5 -48.5 0 MPRFLG 0 ; 
       37 SCHEM 7 -34.5 0 MPRFLG 0 ; 
       38 SCHEM 10.5 -33.5 0 MPRFLG 0 ; 
       39 SCHEM 10.5 -36.5 0 MPRFLG 0 ; 
       40 SCHEM 7 -40.5 0 MPRFLG 0 ; 
       41 SCHEM 10.5 -39.5 0 MPRFLG 0 ; 
       42 SCHEM 10.5 -42.5 0 MPRFLG 0 ; 
       43 SCHEM 0 -28.5 0 SRT 1 1 1 1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -36.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -42.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -38.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -40.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -36.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -30.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -34.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -32.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -54.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -48.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -50.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -52.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -42.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -48.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -46.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -44.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 10.5 -28.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 10.5 -28.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 10.5 -28.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 10.5 -26.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 10.5 -26.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 10.5 -48.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10.5 -42.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10.5 -36.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 10.5 -30.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -28.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -22.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -24.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -14.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -18.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -8.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -10.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -12.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -6.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 14 -30.75 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -42.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21 -38.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21 -40.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -36.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -30.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21 -34.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21 -32.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -54.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -48.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21 -50.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -28.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21 -52.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -42.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -48.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21 -46.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21 -44.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -28.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -28.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -26.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -26.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -48.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -42.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -36.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -30.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 17.5 -36.75 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
