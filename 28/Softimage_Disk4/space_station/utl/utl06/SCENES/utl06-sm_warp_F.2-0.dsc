SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.2-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       sm_gate-inf_light1_1.2-0 ROOT ; 
       sm_gate-inf_light2_1.2-0 ROOT ; 
       sm_gate-light1_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       sm_warp_F-mat1.1-0 ; 
       sm_warp_F-mat10.1-0 ; 
       sm_warp_F-mat11.1-0 ; 
       sm_warp_F-mat12.1-0 ; 
       sm_warp_F-mat13.1-0 ; 
       sm_warp_F-mat14.1-0 ; 
       sm_warp_F-mat15.1-0 ; 
       sm_warp_F-mat16.1-0 ; 
       sm_warp_F-mat17.1-0 ; 
       sm_warp_F-mat18.1-0 ; 
       sm_warp_F-mat19.1-0 ; 
       sm_warp_F-mat2.1-0 ; 
       sm_warp_F-mat20.1-0 ; 
       sm_warp_F-mat21.1-0 ; 
       sm_warp_F-mat22.1-0 ; 
       sm_warp_F-mat23.1-0 ; 
       sm_warp_F-mat24.1-0 ; 
       sm_warp_F-mat25.1-0 ; 
       sm_warp_F-mat26.1-0 ; 
       sm_warp_F-mat27.1-0 ; 
       sm_warp_F-mat3.1-0 ; 
       sm_warp_F-mat4.1-0 ; 
       sm_warp_F-mat5.1-0 ; 
       sm_warp_F-mat6.1-0 ; 
       sm_warp_F-mat7.1-0 ; 
       sm_warp_F-mat8.1-0 ; 
       sm_warp_F-mat9.1-0 ; 
       sm_warp_F-port_red-left.1-0.1-0 ; 
       sm_warp_F-port_red-left.1-18.1-0 ; 
       sm_warp_F-port_red-left.1-19.1-0 ; 
       sm_warp_F-port_red-left.1-20.1-0 ; 
       sm_warp_F-port_red-left.1-21.1-0 ; 
       sm_warp_F-port_red-left.1-22.1-0 ; 
       sm_warp_F-port_red-left.1-23.1-0 ; 
       sm_warp_F-port_red-left.1-24.1-0 ; 
       sm_warp_F-port_red-left.1-25.1-0 ; 
       sm_warp_F-port_red-left.1-26.1-0 ; 
       sm_warp_F-port_red-left.1-27.1-0 ; 
       sm_warp_F-port_red-left.1-28.1-0 ; 
       sm_warp_F-port_red-left.1-29.1-0 ; 
       sm_warp_F-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       utl06-antenn1.1-0 ; 
       utl06-antenn2.1-0 ; 
       utl06-beacon.1-0 ; 
       utl06-fuselg.2-0 ; 
       utl06-shield1a.1-0 ; 
       utl06-shield1b.1-0 ; 
       utl06-shield2a.1-0 ; 
       utl06-shield2b.1-0 ; 
       utl06-shield3a.1-0 ; 
       utl06-shield3b.1-0 ; 
       utl06-shield4a.1-0 ; 
       utl06-shield4b.1-0 ; 
       utl06-SS0a.2-0 ; 
       utl06-SS0b.1-0 ; 
       utl06-SS0c.1-0 ; 
       utl06-SS0d.1-0 ; 
       utl06-SSa1.1-0 ; 
       utl06-SSa1_1.2-0 ; 
       utl06-SSa2.1-0 ; 
       utl06-SSa5.1-0 ; 
       utl06-SSa6.1-0 ; 
       utl06-SSb1.1-0 ; 
       utl06-SSb2.1-0 ; 
       utl06-SSb3.1-0 ; 
       utl06-SSc1.1-0 ; 
       utl06-SSc2.1-0 ; 
       utl06-SSc3.1-0 ; 
       utl06-SSd1.1-0 ; 
       utl06-SSd2.1-0 ; 
       utl06-SSd3.1-0 ; 
       utl06-tractr0.1-0 ; 
       utl06-tractr1.1-0 ; 
       utl06-tractr1a.1-0 ; 
       utl06-tractr1b.1-0 ; 
       utl06-tractr2.1-0 ; 
       utl06-tractr2a.1-0 ; 
       utl06-tractr2b.1-0 ; 
       utl06-tractr3.1-0 ; 
       utl06-tractr3a.1-0 ; 
       utl06-tractr3b.1-0 ; 
       utl06-tractr4.1-0 ; 
       utl06-tractr4a.1-0 ; 
       utl06-tractr4b.1-0 ; 
       utl06-utl06.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/utl/utl06/PICTURES/utl6 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl06-sm_warp_F.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       sm_warp_F-t2d1.1-0 ; 
       sm_warp_F-t2d10.1-0 ; 
       sm_warp_F-t2d11.1-0 ; 
       sm_warp_F-t2d12.1-0 ; 
       sm_warp_F-t2d13.1-0 ; 
       sm_warp_F-t2d14.1-0 ; 
       sm_warp_F-t2d15.1-0 ; 
       sm_warp_F-t2d16.1-0 ; 
       sm_warp_F-t2d17.1-0 ; 
       sm_warp_F-t2d18.1-0 ; 
       sm_warp_F-t2d19.1-0 ; 
       sm_warp_F-t2d2.1-0 ; 
       sm_warp_F-t2d20.1-0 ; 
       sm_warp_F-t2d21.1-0 ; 
       sm_warp_F-t2d22.1-0 ; 
       sm_warp_F-t2d23.1-0 ; 
       sm_warp_F-t2d24.1-0 ; 
       sm_warp_F-t2d25.1-0 ; 
       sm_warp_F-t2d26.1-0 ; 
       sm_warp_F-t2d3.1-0 ; 
       sm_warp_F-t2d4.1-0 ; 
       sm_warp_F-t2d5.1-0 ; 
       sm_warp_F-t2d6.1-0 ; 
       sm_warp_F-t2d7.1-0 ; 
       sm_warp_F-t2d8.1-0 ; 
       sm_warp_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       12 3 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 12 110 ; 
       17 1 110 ; 
       18 12 110 ; 
       19 12 110 ; 
       20 0 110 ; 
       21 13 110 ; 
       22 13 110 ; 
       23 13 110 ; 
       24 14 110 ; 
       25 14 110 ; 
       26 14 110 ; 
       27 15 110 ; 
       28 15 110 ; 
       29 15 110 ; 
       0 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       3 43 110 ; 
       4 32 110 ; 
       5 32 110 ; 
       6 35 110 ; 
       7 35 110 ; 
       8 38 110 ; 
       9 38 110 ; 
       10 41 110 ; 
       11 41 110 ; 
       30 43 110 ; 
       31 30 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 30 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       37 30 110 ; 
       38 37 110 ; 
       39 37 110 ; 
       40 30 110 ; 
       41 40 110 ; 
       42 40 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       16 28 300 ; 
       17 27 300 ; 
       18 29 300 ; 
       19 30 300 ; 
       20 40 300 ; 
       21 31 300 ; 
       22 32 300 ; 
       23 33 300 ; 
       24 34 300 ; 
       25 35 300 ; 
       26 36 300 ; 
       27 37 300 ; 
       28 38 300 ; 
       29 39 300 ; 
       0 20 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       1 22 300 ; 
       2 21 300 ; 
       3 0 300 ; 
       3 11 300 ; 
       4 12 300 ; 
       5 13 300 ; 
       6 17 300 ; 
       7 16 300 ; 
       8 8 300 ; 
       9 7 300 ; 
       10 3 300 ; 
       11 4 300 ; 
       31 23 300 ; 
       32 10 300 ; 
       33 9 300 ; 
       34 24 300 ; 
       35 14 300 ; 
       36 15 300 ; 
       37 26 300 ; 
       38 6 300 ; 
       39 5 300 ; 
       40 25 300 ; 
       41 1 300 ; 
       42 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 25 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 0 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 11 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       12 SCHEM 65 -4 0 MPRFLG 0 ; 
       13 SCHEM 35 -4 0 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 50 -4 0 MPRFLG 0 ; 
       16 SCHEM 67.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 65 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 55 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 37.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 35 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 45 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 40 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 42.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 52.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 47.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 50 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 55 -4 0 MPRFLG 0 ; 
       1 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 MPRFLG 0 ; 
       3 SCHEM 50 -2 0 MPRFLG 0 ; 
       4 SCHEM 30 -8 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 5 -8 0 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 MPRFLG 0 ; 
       30 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       31 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       33 SCHEM 25 -6 0 MPRFLG 0 ; 
       34 SCHEM 5 -4 0 MPRFLG 0 ; 
       35 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       36 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       38 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       39 SCHEM 10 -6 0 MPRFLG 0 ; 
       40 SCHEM 20 -4 0 MPRFLG 0 ; 
       41 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       42 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       43 SCHEM 35 0 0 SRT 1 1 1 1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
