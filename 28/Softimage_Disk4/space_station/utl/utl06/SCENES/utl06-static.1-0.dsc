SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.3-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       sm_gate-inf_light1_1.3-0 ROOT ; 
       sm_gate-inf_light2_1.3-0 ROOT ; 
       sm_gate-light1_1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       static-mat1.1-0 ; 
       static-mat10.1-0 ; 
       static-mat11.1-0 ; 
       static-mat12.1-0 ; 
       static-mat13.1-0 ; 
       static-mat14.1-0 ; 
       static-mat15.1-0 ; 
       static-mat16.1-0 ; 
       static-mat17.1-0 ; 
       static-mat18.1-0 ; 
       static-mat19.1-0 ; 
       static-mat2.1-0 ; 
       static-mat20.1-0 ; 
       static-mat21.1-0 ; 
       static-mat22.1-0 ; 
       static-mat23.1-0 ; 
       static-mat24.1-0 ; 
       static-mat25.1-0 ; 
       static-mat26.1-0 ; 
       static-mat27.1-0 ; 
       static-mat3.1-0 ; 
       static-mat4.1-0 ; 
       static-mat6.1-0 ; 
       static-mat7.1-0 ; 
       static-mat8.1-0 ; 
       static-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       utl06-antenn1.1-0 ; 
       utl06-beacon.1-0 ; 
       utl06-fuselg.2-0 ; 
       utl06-shield1a.1-0 ; 
       utl06-shield1b.1-0 ; 
       utl06-shield2a.1-0 ; 
       utl06-shield2b.1-0 ; 
       utl06-shield3a.1-0 ; 
       utl06-shield3b.1-0 ; 
       utl06-shield4a.1-0 ; 
       utl06-shield4b.1-0 ; 
       utl06-tractr0.1-0 ; 
       utl06-tractr1.1-0 ; 
       utl06-tractr1a.1-0 ; 
       utl06-tractr1b.1-0 ; 
       utl06-tractr2.1-0 ; 
       utl06-tractr2a.1-0 ; 
       utl06-tractr2b.1-0 ; 
       utl06-tractr3.1-0 ; 
       utl06-tractr3a.1-0 ; 
       utl06-tractr3b.1-0 ; 
       utl06-tractr4.1-0 ; 
       utl06-tractr4a.1-0 ; 
       utl06-tractr4b.1-0 ; 
       utl06-utl06.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/utl/utl06/PICTURES/utl6 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl06-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       static-t2d1.1-0 ; 
       static-t2d10.1-0 ; 
       static-t2d11.1-0 ; 
       static-t2d12.1-0 ; 
       static-t2d13.1-0 ; 
       static-t2d14.1-0 ; 
       static-t2d15.1-0 ; 
       static-t2d16.1-0 ; 
       static-t2d17.1-0 ; 
       static-t2d18.1-0 ; 
       static-t2d19.1-0 ; 
       static-t2d2.1-0 ; 
       static-t2d20.1-0 ; 
       static-t2d21.1-0 ; 
       static-t2d22.1-0 ; 
       static-t2d23.1-0 ; 
       static-t2d24.1-0 ; 
       static-t2d25.1-0 ; 
       static-t2d26.1-0 ; 
       static-t2d3.1-0 ; 
       static-t2d5.1-0 ; 
       static-t2d6.1-0 ; 
       static-t2d7.1-0 ; 
       static-t2d8.1-0 ; 
       static-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 24 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 16 110 ; 
       6 16 110 ; 
       7 19 110 ; 
       8 19 110 ; 
       9 22 110 ; 
       10 22 110 ; 
       11 24 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 11 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 11 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 11 110 ; 
       22 21 110 ; 
       23 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 20 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       1 21 300 ; 
       2 0 300 ; 
       2 11 300 ; 
       3 12 300 ; 
       4 13 300 ; 
       5 17 300 ; 
       6 16 300 ; 
       7 8 300 ; 
       8 7 300 ; 
       9 3 300 ; 
       10 4 300 ; 
       12 22 300 ; 
       13 10 300 ; 
       14 9 300 ; 
       15 23 300 ; 
       16 14 300 ; 
       17 15 300 ; 
       18 25 300 ; 
       19 6 300 ; 
       20 5 300 ; 
       21 24 300 ; 
       22 1 300 ; 
       23 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 24 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 0 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 11 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 35 -4 0 MPRFLG 0 ; 
       2 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 30 -8 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 MPRFLG 0 ; 
       7 SCHEM 15 -8 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 MPRFLG 0 ; 
       15 SCHEM 5 -4 0 MPRFLG 0 ; 
       16 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       17 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 10 -6 0 MPRFLG 0 ; 
       21 SCHEM 20 -4 0 MPRFLG 0 ; 
       22 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 18.75 0 0 SRT 1 1 1 1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 12 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
