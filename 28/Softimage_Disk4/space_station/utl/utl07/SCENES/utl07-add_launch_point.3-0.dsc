SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.19-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 69     
       add_launch_point-port_red-left.1-0.1-0 ; 
       add_launch_point-port_red-left.1-10.1-0 ; 
       add_launch_point-port_red-left.1-11.1-0 ; 
       add_launch_point-port_red-left.1-12.1-0 ; 
       add_launch_point-port_red-left.1-13.1-0 ; 
       add_launch_point-port_red-left.1-14.1-0 ; 
       add_launch_point-port_red-left.1-15.1-0 ; 
       add_launch_point-port_red-left.1-16.1-0 ; 
       add_launch_point-port_red-left.1-17.1-0 ; 
       add_launch_point-port_red-left.1-18.1-0 ; 
       add_launch_point-port_red-left.1-19.1-0 ; 
       add_launch_point-port_red-left.1-20.1-0 ; 
       add_launch_point-port_red-left.1-21.1-0 ; 
       add_launch_point-port_red-left.1-22.1-0 ; 
       add_launch_point-port_red-left.1-23.1-0 ; 
       add_launch_point-port_red-left.1-24.1-0 ; 
       add_launch_point-port_red-left.1-25.1-0 ; 
       add_launch_point-port_red-left.1-4.1-0 ; 
       add_launch_point-port_red-left.1-5.1-0 ; 
       add_launch_point-port_red-left.1-7.1-0 ; 
       add_launch_point-port_red-left.1-8.1-0 ; 
       add_launch_point-port_red-left.1-9.1-0 ; 
       add_launch_point-starbord_green-right.1-0.1-0 ; 
       lg_gate_sAPTL-mat11.1-0 ; 
       lg_gate_sAPTL-mat12.1-0 ; 
       lg_gate_sAPTL-mat13.1-0 ; 
       lg_gate_sAPTL-mat14.1-0 ; 
       lg_gate_sAPTL-mat15.1-0 ; 
       lg_gate_sAPTL-mat16.1-0 ; 
       lg_gate_sAPTL-mat18.1-0 ; 
       lg_gate_sAPTL-mat19.1-0 ; 
       lg_gate_sAPTL-mat20.2-0 ; 
       lg_gate_sAPTL-mat21.1-0 ; 
       lg_gate_sAPTL-mat22.1-0 ; 
       lg_gate_sAPTL-mat23.1-0 ; 
       lg_gate_sAPTL-mat25.1-0 ; 
       lg_gate_sAPTL-mat26.1-0 ; 
       lg_gate_sAPTL-mat27.1-0 ; 
       lg_gate_sAPTL-mat28.1-0 ; 
       lg_gate_sAPTL-mat29.1-0 ; 
       lg_gate_sAPTL-mat30.1-0 ; 
       lg_gate_sAPTL-mat31.1-0 ; 
       lg_gate_sAPTL-mat32.1-0 ; 
       lg_gate_sAPTL-mat33.1-0 ; 
       lg_gate_sAPTL-mat34.1-0 ; 
       lg_gate_sAPTL-mat35.1-0 ; 
       lg_gate_sAPTL-mat36.1-0 ; 
       lg_gate_sAPTL-mat37.1-0 ; 
       lg_gate_sAPTL-mat38.1-0 ; 
       lg_gate_sAPTL-mat39.1-0 ; 
       lg_gate_sAPTL-mat40.1-0 ; 
       lg_gate_sAPTL-mat41.1-0 ; 
       lg_gate_sAPTL-mat42.2-0 ; 
       lg_gate_sAPTL-mat43.2-0 ; 
       lg_gate_sAPTL-mat44.2-0 ; 
       lg_gate_sAPTL-mat45.1-0 ; 
       lg_gate_sAPTL-mat46.1-0 ; 
       lg_gate_sAPTL-mat47.1-0 ; 
       lg_gate_sAPTL-mat48.1-0 ; 
       lg_gate_sAPTL-mat49.1-0 ; 
       lg_gate_sAPTL-mat5_2.1-0 ; 
       lg_gate_sAPTL-mat50.1-0 ; 
       lg_gate_sAPTL-mat51.1-0 ; 
       lg_gate_sAPTL-mat6.1-0 ; 
       lg_gate_sAPTL-mat7.1-0 ; 
       lg_gate_sAPTL-y1.1-0 ; 
       Utl07-mat2.1-0 ; 
       Utl07-mat3.1-0 ; 
       Utl07-mat4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 49     
       utl07-antenn.1-0 ; 
       utl07-antenn0.1-0 ; 
       utl07-bantenn.1-0 ; 
       utl07-barmour.1-0 ; 
       utl07-beacon.5-0 ; 
       utl07-fuselg.1-0 ; 
       utl07-fuselg0.5-0 ; 
       utl07-launch1.1-0 ; 
       utl07-more_strobes.1-0 ; 
       utl07-root.13-0 ROOT ; 
       utl07-SS_1.1-0 ; 
       utl07-SS_2.1-0 ; 
       utl07-SS_3.1-0 ; 
       utl07-SS_a2.1-0 ; 
       utl07-SS_a3.1-0 ; 
       utl07-SS_a4.1-0 ; 
       utl07-SS_a5.1-0 ; 
       utl07-SS_b1.1-0 ; 
       utl07-SS_b2.1-0 ; 
       utl07-SS_b3.1-0 ; 
       utl07-SS_b4.1-0 ; 
       utl07-SS_b5.1-0 ; 
       utl07-SS_c1.1-0 ; 
       utl07-SS_c2.1-0 ; 
       utl07-SS_c3.1-0 ; 
       utl07-SS_c4.1-0 ; 
       utl07-SS_c5.1-0 ; 
       utl07-SS_d1.1-0 ; 
       utl07-SS_d2.1-0 ; 
       utl07-SS_d3.1-0 ; 
       utl07-SS_d4.1-0 ; 
       utl07-SS_d5.1-0 ; 
       utl07-SSa1.1-0 ; 
       utl07-still_more_strobes.1-0 ; 
       utl07-strobes.1-0 ; 
       utl07-tarmour.1-0 ; 
       utl07-tractr1.1-0 ; 
       utl07-tractr1a.1-0 ; 
       utl07-tractr1b.1-0 ; 
       utl07-tractr2.1-0 ; 
       utl07-tractr2a.1-0 ; 
       utl07-tractr2b.1-0 ; 
       utl07-tractr3.1-0 ; 
       utl07-tractr3a.1-0 ; 
       utl07-tractr3b.1-0 ; 
       utl07-tractr4.1-0 ; 
       utl07-tractr4a.1-0 ; 
       utl07-tractr4b.1-0 ; 
       utl07-yet_more_Strobes.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Softmachine_Backup/Disk_1_3_4/Disk3/space_station/utl/utl07/PICTURES/utl07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl07-add_launch_point.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       lg_gate_sAPTL-t2d1.4-0 ; 
       lg_gate_sAPTL-t2d11.4-0 ; 
       lg_gate_sAPTL-t2d12.4-0 ; 
       lg_gate_sAPTL-t2d13.4-0 ; 
       lg_gate_sAPTL-t2d14.4-0 ; 
       lg_gate_sAPTL-t2d15.4-0 ; 
       lg_gate_sAPTL-t2d17.4-0 ; 
       lg_gate_sAPTL-t2d18.4-0 ; 
       lg_gate_sAPTL-t2d19.4-0 ; 
       lg_gate_sAPTL-t2d20.4-0 ; 
       lg_gate_sAPTL-t2d21.4-0 ; 
       lg_gate_sAPTL-t2d22.4-0 ; 
       lg_gate_sAPTL-t2d23.4-0 ; 
       lg_gate_sAPTL-t2d24.4-0 ; 
       lg_gate_sAPTL-t2d25.4-0 ; 
       lg_gate_sAPTL-t2d26.4-0 ; 
       lg_gate_sAPTL-t2d27.4-0 ; 
       lg_gate_sAPTL-t2d28.4-0 ; 
       lg_gate_sAPTL-t2d29.4-0 ; 
       lg_gate_sAPTL-t2d30.4-0 ; 
       lg_gate_sAPTL-t2d31.4-0 ; 
       lg_gate_sAPTL-t2d32.4-0 ; 
       lg_gate_sAPTL-t2d33.4-0 ; 
       lg_gate_sAPTL-t2d34.4-0 ; 
       lg_gate_sAPTL-t2d35.4-0 ; 
       lg_gate_sAPTL-t2d36.4-0 ; 
       lg_gate_sAPTL-t2d37.4-0 ; 
       lg_gate_sAPTL-t2d38.4-0 ; 
       lg_gate_sAPTL-t2d39.4-0 ; 
       lg_gate_sAPTL-t2d40.4-0 ; 
       lg_gate_sAPTL-t2d41.4-0 ; 
       lg_gate_sAPTL-t2d42.4-0 ; 
       lg_gate_sAPTL-t2d43.4-0 ; 
       lg_gate_sAPTL-t2d6.4-0 ; 
       lg_gate_sAPTL-t2d7.4-0 ; 
       lg_gate_sAPTL-t2d9.4-0 ; 
       lg_gate_sAPTL-z.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 6 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 6 110 ; 
       6 9 110 ; 
       8 5 110 ; 
       10 35 110 ; 
       11 0 110 ; 
       12 2 110 ; 
       13 33 110 ; 
       14 33 110 ; 
       15 33 110 ; 
       16 33 110 ; 
       17 34 110 ; 
       18 34 110 ; 
       19 34 110 ; 
       20 34 110 ; 
       21 34 110 ; 
       22 8 110 ; 
       23 8 110 ; 
       24 8 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 48 110 ; 
       28 48 110 ; 
       29 48 110 ; 
       30 48 110 ; 
       31 48 110 ; 
       32 33 110 ; 
       33 5 110 ; 
       34 5 110 ; 
       35 1 110 ; 
       36 9 110 ; 
       37 36 110 ; 
       38 36 110 ; 
       39 9 110 ; 
       40 39 110 ; 
       41 39 110 ; 
       42 9 110 ; 
       43 42 110 ; 
       44 42 110 ; 
       45 9 110 ; 
       46 45 110 ; 
       47 45 110 ; 
       48 5 110 ; 
       7 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 67 300 ; 
       0 23 300 ; 
       0 24 300 ; 
       0 25 300 ; 
       0 29 300 ; 
       0 30 300 ; 
       0 57 300 ; 
       2 60 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       2 47 300 ; 
       2 65 300 ; 
       2 56 300 ; 
       3 68 300 ; 
       3 31 300 ; 
       3 52 300 ; 
       3 53 300 ; 
       3 54 300 ; 
       4 48 300 ; 
       4 49 300 ; 
       4 51 300 ; 
       5 63 300 ; 
       5 64 300 ; 
       10 17 300 ; 
       11 22 300 ; 
       12 0 300 ; 
       13 8 300 ; 
       14 9 300 ; 
       15 10 300 ; 
       16 11 300 ; 
       17 12 300 ; 
       18 13 300 ; 
       19 14 300 ; 
       20 15 300 ; 
       21 16 300 ; 
       22 18 300 ; 
       23 19 300 ; 
       24 20 300 ; 
       25 21 300 ; 
       26 1 300 ; 
       27 2 300 ; 
       28 3 300 ; 
       29 4 300 ; 
       30 5 300 ; 
       31 6 300 ; 
       32 7 300 ; 
       35 66 300 ; 
       35 26 300 ; 
       35 27 300 ; 
       35 28 300 ; 
       35 50 300 ; 
       35 55 300 ; 
       36 35 300 ; 
       36 62 300 ; 
       37 37 300 ; 
       38 36 300 ; 
       39 44 300 ; 
       39 61 300 ; 
       40 46 300 ; 
       41 45 300 ; 
       42 41 300 ; 
       42 59 300 ; 
       43 43 300 ; 
       44 42 300 ; 
       45 38 300 ; 
       45 58 300 ; 
       46 40 300 ; 
       47 39 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       24 33 401 ; 
       25 34 401 ; 
       27 36 401 ; 
       28 35 401 ; 
       29 1 401 ; 
       30 2 401 ; 
       31 3 401 ; 
       33 4 401 ; 
       34 5 401 ; 
       35 6 401 ; 
       36 7 401 ; 
       37 8 401 ; 
       38 9 401 ; 
       39 10 401 ; 
       40 11 401 ; 
       41 12 401 ; 
       42 13 401 ; 
       43 14 401 ; 
       44 15 401 ; 
       45 16 401 ; 
       46 17 401 ; 
       47 18 401 ; 
       49 20 401 ; 
       50 21 401 ; 
       51 22 401 ; 
       52 23 401 ; 
       53 24 401 ; 
       54 25 401 ; 
       55 26 401 ; 
       56 27 401 ; 
       57 28 401 ; 
       58 29 401 ; 
       59 30 401 ; 
       61 31 401 ; 
       62 32 401 ; 
       64 0 401 ; 
       65 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 75 -6 0 MPRFLG 0 ; 
       4 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 40 -6 0 MPRFLG 0 ; 
       9 SCHEM 43.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 80 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 82.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       12 SCHEM 72.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 60 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 65 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 25 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 45 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 37.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 40 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 42.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 47.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 50 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 52.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 55 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 70 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 65 -6 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 80 -6 0 MPRFLG 0 ; 
       36 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       37 SCHEM 5 -4 0 MPRFLG 0 ; 
       38 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       40 SCHEM 10 -4 0 MPRFLG 0 ; 
       41 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       42 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       43 SCHEM 15 -4 0 MPRFLG 0 ; 
       44 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       45 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       46 SCHEM 20 -4 0 MPRFLG 0 ; 
       47 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       48 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 85 -2 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       23 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 71.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 69 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 81.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 81.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 81.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 81.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 81.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
