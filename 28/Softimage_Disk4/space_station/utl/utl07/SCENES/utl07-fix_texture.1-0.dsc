SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl07-utl07.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.1-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 8     
       Utl07-spot1.1-0 ; 
       Utl07-spot1_3.1-0 ; 
       Utl07-spot1_3_int.1-0 ROOT ; 
       Utl07-spot1_int.1-0 ROOT ; 
       Utl07-spot2.1-0 ; 
       Utl07-spot2_3.1-0 ; 
       Utl07-spot2_3_int.1-0 ROOT ; 
       Utl07-spot2_int.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 70     
       fix_texture-port_red-left.1-0.1-0 ; 
       fix_texture-port_red-left.1-10.1-0 ; 
       fix_texture-port_red-left.1-11.1-0 ; 
       fix_texture-port_red-left.1-12.1-0 ; 
       fix_texture-port_red-left.1-13.1-0 ; 
       fix_texture-port_red-left.1-14.1-0 ; 
       fix_texture-port_red-left.1-15.1-0 ; 
       fix_texture-port_red-left.1-16.1-0 ; 
       fix_texture-port_red-left.1-17.1-0 ; 
       fix_texture-port_red-left.1-18.1-0 ; 
       fix_texture-port_red-left.1-19.1-0 ; 
       fix_texture-port_red-left.1-20.1-0 ; 
       fix_texture-port_red-left.1-21.1-0 ; 
       fix_texture-port_red-left.1-22.1-0 ; 
       fix_texture-port_red-left.1-23.1-0 ; 
       fix_texture-port_red-left.1-24.1-0 ; 
       fix_texture-port_red-left.1-25.1-0 ; 
       fix_texture-port_red-left.1-4.1-0 ; 
       fix_texture-port_red-left.1-5.1-0 ; 
       fix_texture-port_red-left.1-7.1-0 ; 
       fix_texture-port_red-left.1-8.1-0 ; 
       fix_texture-port_red-left.1-9.1-0 ; 
       fix_texture-starbord_green-right.1-0.1-0 ; 
       lg_gate_sAPTL-mat11.1-0 ; 
       lg_gate_sAPTL-mat12.1-0 ; 
       lg_gate_sAPTL-mat13.1-0 ; 
       lg_gate_sAPTL-mat14.1-0 ; 
       lg_gate_sAPTL-mat15.1-0 ; 
       lg_gate_sAPTL-mat16.1-0 ; 
       lg_gate_sAPTL-mat17.1-0 ; 
       lg_gate_sAPTL-mat18.1-0 ; 
       lg_gate_sAPTL-mat19.1-0 ; 
       lg_gate_sAPTL-mat20.1-0 ; 
       lg_gate_sAPTL-mat21.1-0 ; 
       lg_gate_sAPTL-mat22.1-0 ; 
       lg_gate_sAPTL-mat23.1-0 ; 
       lg_gate_sAPTL-mat25.1-0 ; 
       lg_gate_sAPTL-mat26.1-0 ; 
       lg_gate_sAPTL-mat27.1-0 ; 
       lg_gate_sAPTL-mat28.1-0 ; 
       lg_gate_sAPTL-mat29.1-0 ; 
       lg_gate_sAPTL-mat30.1-0 ; 
       lg_gate_sAPTL-mat31.1-0 ; 
       lg_gate_sAPTL-mat32.1-0 ; 
       lg_gate_sAPTL-mat33.1-0 ; 
       lg_gate_sAPTL-mat34.1-0 ; 
       lg_gate_sAPTL-mat35.1-0 ; 
       lg_gate_sAPTL-mat36.1-0 ; 
       lg_gate_sAPTL-mat37.1-0 ; 
       lg_gate_sAPTL-mat38.1-0 ; 
       lg_gate_sAPTL-mat39.1-0 ; 
       lg_gate_sAPTL-mat40.1-0 ; 
       lg_gate_sAPTL-mat41.1-0 ; 
       lg_gate_sAPTL-mat42.1-0 ; 
       lg_gate_sAPTL-mat43.1-0 ; 
       lg_gate_sAPTL-mat44.1-0 ; 
       lg_gate_sAPTL-mat45.1-0 ; 
       lg_gate_sAPTL-mat46.1-0 ; 
       lg_gate_sAPTL-mat47.1-0 ; 
       lg_gate_sAPTL-mat48.1-0 ; 
       lg_gate_sAPTL-mat49.1-0 ; 
       lg_gate_sAPTL-mat5_2.1-0 ; 
       lg_gate_sAPTL-mat50.1-0 ; 
       lg_gate_sAPTL-mat51.1-0 ; 
       lg_gate_sAPTL-mat6.1-0 ; 
       lg_gate_sAPTL-mat7.1-0 ; 
       lg_gate_sAPTL-y1.1-0 ; 
       Utl07-mat2.1-0 ; 
       Utl07-mat3.1-0 ; 
       Utl07-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       utl07-antenn.1-0 ; 
       utl07-antenn0.1-0 ; 
       utl07-bantenn.1-0 ; 
       utl07-barmour.1-0 ; 
       utl07-beacon.5-0 ; 
       utl07-fuselg.1-0 ; 
       utl07-fuselg0.5-0 ; 
       utl07-SS0a.1-0 ; 
       utl07-SS0b.1-0 ; 
       utl07-SS0c.1-0 ; 
       utl07-SS0d.1-0 ; 
       utl07-SS1.1-0 ; 
       utl07-SS2.1-0 ; 
       utl07-SS3.1-0 ; 
       utl07-SSa1.1-0 ; 
       utl07-SSa2.1-0 ; 
       utl07-SSa3.1-0 ; 
       utl07-SSa4.1-0 ; 
       utl07-SSa5.1-0 ; 
       utl07-SSb1.1-0 ; 
       utl07-SSb2.1-0 ; 
       utl07-SSb3.1-0 ; 
       utl07-SSb4.1-0 ; 
       utl07-SSb5.1-0 ; 
       utl07-SSc1.1-0 ; 
       utl07-SSc2.1-0 ; 
       utl07-SSc3.1-0 ; 
       utl07-SSc4.1-0 ; 
       utl07-SSc5.1-0 ; 
       utl07-SSd1.1-0 ; 
       utl07-SSd2.1-0 ; 
       utl07-SSd3.1-0 ; 
       utl07-SSd4.1-0 ; 
       utl07-SSd5.1-0 ; 
       utl07-tarmour.1-0 ; 
       utl07-tractr1.1-0 ; 
       utl07-tractr1a.1-0 ; 
       utl07-tractr1b.1-0 ; 
       utl07-tractr2.1-0 ; 
       utl07-tractr2a.1-0 ; 
       utl07-tractr2b.1-0 ; 
       utl07-tractr3.1-0 ; 
       utl07-tractr3a.1-0 ; 
       utl07-tractr3b.1-0 ; 
       utl07-tractr4.1-0 ; 
       utl07-tractr4a.1-0 ; 
       utl07-tractr4b.1-0 ; 
       utl07-utl07.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/utl/utl07/PICTURES/utl07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl07-fix_texture.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       lg_gate_sAPTL-t2d1.1-0 ; 
       lg_gate_sAPTL-t2d10.1-0 ; 
       lg_gate_sAPTL-t2d11.1-0 ; 
       lg_gate_sAPTL-t2d12.1-0 ; 
       lg_gate_sAPTL-t2d13.1-0 ; 
       lg_gate_sAPTL-t2d14.1-0 ; 
       lg_gate_sAPTL-t2d15.1-0 ; 
       lg_gate_sAPTL-t2d17.1-0 ; 
       lg_gate_sAPTL-t2d18.1-0 ; 
       lg_gate_sAPTL-t2d19.1-0 ; 
       lg_gate_sAPTL-t2d20.1-0 ; 
       lg_gate_sAPTL-t2d21.1-0 ; 
       lg_gate_sAPTL-t2d22.1-0 ; 
       lg_gate_sAPTL-t2d23.1-0 ; 
       lg_gate_sAPTL-t2d24.1-0 ; 
       lg_gate_sAPTL-t2d25.1-0 ; 
       lg_gate_sAPTL-t2d26.1-0 ; 
       lg_gate_sAPTL-t2d27.1-0 ; 
       lg_gate_sAPTL-t2d28.1-0 ; 
       lg_gate_sAPTL-t2d29.1-0 ; 
       lg_gate_sAPTL-t2d30.1-0 ; 
       lg_gate_sAPTL-t2d31.1-0 ; 
       lg_gate_sAPTL-t2d32.1-0 ; 
       lg_gate_sAPTL-t2d33.1-0 ; 
       lg_gate_sAPTL-t2d34.1-0 ; 
       lg_gate_sAPTL-t2d35.1-0 ; 
       lg_gate_sAPTL-t2d36.1-0 ; 
       lg_gate_sAPTL-t2d37.1-0 ; 
       lg_gate_sAPTL-t2d38.1-0 ; 
       lg_gate_sAPTL-t2d39.1-0 ; 
       lg_gate_sAPTL-t2d40.1-0 ; 
       lg_gate_sAPTL-t2d41.1-0 ; 
       lg_gate_sAPTL-t2d42.1-0 ; 
       lg_gate_sAPTL-t2d43.1-0 ; 
       lg_gate_sAPTL-t2d6.1-0 ; 
       lg_gate_sAPTL-t2d7.1-0 ; 
       lg_gate_sAPTL-t2d9.1-0 ; 
       lg_gate_sAPTL-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 6 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 6 110 ; 
       6 47 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 34 110 ; 
       12 0 110 ; 
       13 2 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 8 110 ; 
       23 8 110 ; 
       24 9 110 ; 
       25 9 110 ; 
       26 9 110 ; 
       27 9 110 ; 
       28 9 110 ; 
       29 10 110 ; 
       30 10 110 ; 
       31 10 110 ; 
       32 10 110 ; 
       33 10 110 ; 
       34 1 110 ; 
       35 47 110 ; 
       36 35 110 ; 
       37 35 110 ; 
       38 47 110 ; 
       39 38 110 ; 
       40 38 110 ; 
       41 47 110 ; 
       42 41 110 ; 
       43 41 110 ; 
       44 47 110 ; 
       45 44 110 ; 
       46 44 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 68 300 ; 
       0 23 300 ; 
       0 24 300 ; 
       0 25 300 ; 
       0 30 300 ; 
       0 31 300 ; 
       0 58 300 ; 
       2 61 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 48 300 ; 
       2 66 300 ; 
       2 57 300 ; 
       3 69 300 ; 
       3 29 300 ; 
       3 32 300 ; 
       3 53 300 ; 
       3 54 300 ; 
       3 55 300 ; 
       4 49 300 ; 
       4 50 300 ; 
       4 52 300 ; 
       5 64 300 ; 
       5 65 300 ; 
       11 17 300 ; 
       12 22 300 ; 
       13 0 300 ; 
       14 7 300 ; 
       15 8 300 ; 
       16 9 300 ; 
       17 10 300 ; 
       18 11 300 ; 
       19 12 300 ; 
       20 13 300 ; 
       21 14 300 ; 
       22 15 300 ; 
       23 16 300 ; 
       24 18 300 ; 
       25 19 300 ; 
       26 20 300 ; 
       27 21 300 ; 
       28 1 300 ; 
       29 2 300 ; 
       30 3 300 ; 
       31 4 300 ; 
       32 5 300 ; 
       33 6 300 ; 
       34 67 300 ; 
       34 26 300 ; 
       34 27 300 ; 
       34 28 300 ; 
       34 51 300 ; 
       34 56 300 ; 
       35 36 300 ; 
       35 63 300 ; 
       36 38 300 ; 
       37 37 300 ; 
       38 45 300 ; 
       38 62 300 ; 
       39 47 300 ; 
       40 46 300 ; 
       41 42 300 ; 
       41 60 300 ; 
       42 44 300 ; 
       43 43 300 ; 
       44 39 300 ; 
       44 59 300 ; 
       45 41 300 ; 
       46 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       47 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 3 2110 ; 
       1 2 2110 ; 
       4 7 2110 ; 
       5 6 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       24 34 401 ; 
       25 35 401 ; 
       27 37 401 ; 
       28 36 401 ; 
       29 1 401 ; 
       30 2 401 ; 
       31 3 401 ; 
       32 4 401 ; 
       34 5 401 ; 
       35 6 401 ; 
       36 7 401 ; 
       37 8 401 ; 
       38 9 401 ; 
       39 10 401 ; 
       40 11 401 ; 
       41 12 401 ; 
       42 13 401 ; 
       43 14 401 ; 
       44 15 401 ; 
       45 16 401 ; 
       46 17 401 ; 
       47 18 401 ; 
       48 19 401 ; 
       50 21 401 ; 
       51 22 401 ; 
       52 23 401 ; 
       53 24 401 ; 
       54 25 401 ; 
       55 26 401 ; 
       56 27 401 ; 
       57 28 401 ; 
       58 29 401 ; 
       59 30 401 ; 
       60 31 401 ; 
       62 32 401 ; 
       63 33 401 ; 
       65 0 401 ; 
       66 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 163.75 -10 0 MPRFLG 0 ; 
       1 SCHEM 133.75 -8 0 MPRFLG 0 ; 
       2 SCHEM 103.75 -10 0 MPRFLG 0 ; 
       3 SCHEM 121.25 -10 0 MPRFLG 0 ; 
       4 SCHEM 132.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 66.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 106.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 82.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 45 -10 0 MPRFLG 0 ; 
       9 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 70 -10 0 MPRFLG 0 ; 
       11 SCHEM 137.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 155 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       13 SCHEM 95 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 87.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 77.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 80 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 82.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 85 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 50 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 40 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 42.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 45 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 52.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 55 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 60 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 75 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 65 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 67.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 70 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 72.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 145 -10 0 MPRFLG 0 ; 
       35 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       36 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 0 -8 0 MPRFLG 0 ; 
       38 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       39 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 10 -8 0 MPRFLG 0 ; 
       41 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       42 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       43 SCHEM 20 -8 0 MPRFLG 0 ; 
       44 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       45 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       46 SCHEM 30 -8 0 MPRFLG 0 ; 
       47 SCHEM 86.25 -4 0 SRT 1 1 1 0 8.742289e-008 1.061083e-013 -9.313226e-010 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       23 SCHEM 157.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 160 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 162.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 140 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 142.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 145 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 115 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 165 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 167.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 102.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 135 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 130 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 147.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 120 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 125 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 150 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 110 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 170 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 112.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 107.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 95 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 60 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 65 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 72.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 87.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 80 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 82.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 85 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 50 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 47.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 137.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 55 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 155 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 152.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 172.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 115 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 165 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 167.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 117.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 100 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 102.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 105 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 107.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 130 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 147.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 132.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 120 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 122.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 125 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 150 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 110 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 170 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 160 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 162.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 142.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 145 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 174 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 17 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
