SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.15-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 69     
       lg_gate_sAPTL-mat11.1-0 ; 
       lg_gate_sAPTL-mat12.1-0 ; 
       lg_gate_sAPTL-mat13.1-0 ; 
       lg_gate_sAPTL-mat14.1-0 ; 
       lg_gate_sAPTL-mat15.1-0 ; 
       lg_gate_sAPTL-mat16.1-0 ; 
       lg_gate_sAPTL-mat18.1-0 ; 
       lg_gate_sAPTL-mat19.1-0 ; 
       lg_gate_sAPTL-mat20.2-0 ; 
       lg_gate_sAPTL-mat21.1-0 ; 
       lg_gate_sAPTL-mat22.1-0 ; 
       lg_gate_sAPTL-mat23.1-0 ; 
       lg_gate_sAPTL-mat25.1-0 ; 
       lg_gate_sAPTL-mat26.1-0 ; 
       lg_gate_sAPTL-mat27.1-0 ; 
       lg_gate_sAPTL-mat28.1-0 ; 
       lg_gate_sAPTL-mat29.1-0 ; 
       lg_gate_sAPTL-mat30.1-0 ; 
       lg_gate_sAPTL-mat31.1-0 ; 
       lg_gate_sAPTL-mat32.1-0 ; 
       lg_gate_sAPTL-mat33.1-0 ; 
       lg_gate_sAPTL-mat34.1-0 ; 
       lg_gate_sAPTL-mat35.1-0 ; 
       lg_gate_sAPTL-mat36.1-0 ; 
       lg_gate_sAPTL-mat37.1-0 ; 
       lg_gate_sAPTL-mat38.1-0 ; 
       lg_gate_sAPTL-mat39.1-0 ; 
       lg_gate_sAPTL-mat40.1-0 ; 
       lg_gate_sAPTL-mat41.1-0 ; 
       lg_gate_sAPTL-mat42.2-0 ; 
       lg_gate_sAPTL-mat43.2-0 ; 
       lg_gate_sAPTL-mat44.2-0 ; 
       lg_gate_sAPTL-mat45.1-0 ; 
       lg_gate_sAPTL-mat46.1-0 ; 
       lg_gate_sAPTL-mat47.1-0 ; 
       lg_gate_sAPTL-mat48.1-0 ; 
       lg_gate_sAPTL-mat49.1-0 ; 
       lg_gate_sAPTL-mat5_2.1-0 ; 
       lg_gate_sAPTL-mat50.1-0 ; 
       lg_gate_sAPTL-mat51.1-0 ; 
       lg_gate_sAPTL-mat6.1-0 ; 
       lg_gate_sAPTL-mat7.1-0 ; 
       lg_gate_sAPTL-y1.1-0 ; 
       rotate-port_red-left.1-0.1-0 ; 
       rotate-port_red-left.1-10.1-0 ; 
       rotate-port_red-left.1-11.1-0 ; 
       rotate-port_red-left.1-12.1-0 ; 
       rotate-port_red-left.1-13.1-0 ; 
       rotate-port_red-left.1-14.1-0 ; 
       rotate-port_red-left.1-15.1-0 ; 
       rotate-port_red-left.1-16.1-0 ; 
       rotate-port_red-left.1-17.1-0 ; 
       rotate-port_red-left.1-18.1-0 ; 
       rotate-port_red-left.1-19.1-0 ; 
       rotate-port_red-left.1-20.1-0 ; 
       rotate-port_red-left.1-21.1-0 ; 
       rotate-port_red-left.1-22.1-0 ; 
       rotate-port_red-left.1-23.1-0 ; 
       rotate-port_red-left.1-24.1-0 ; 
       rotate-port_red-left.1-25.1-0 ; 
       rotate-port_red-left.1-4.1-0 ; 
       rotate-port_red-left.1-5.1-0 ; 
       rotate-port_red-left.1-7.1-0 ; 
       rotate-port_red-left.1-8.1-0 ; 
       rotate-port_red-left.1-9.1-0 ; 
       rotate-starbord_green-right.1-0.1-0 ; 
       Utl07-mat2.1-0 ; 
       Utl07-mat3.1-0 ; 
       Utl07-mat4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       utl07-antenn.1-0 ; 
       utl07-antenn0.1-0 ; 
       utl07-bantenn.1-0 ; 
       utl07-barmour.1-0 ; 
       utl07-beacon.5-0 ; 
       utl07-fuselg.1-0 ; 
       utl07-fuselg0.5-0 ; 
       utl07-more_strobes.1-0 ; 
       utl07-root.10-0 ROOT ; 
       utl07-SS_1.1-0 ; 
       utl07-SS_2.1-0 ; 
       utl07-SS_3.1-0 ; 
       utl07-SS_a2.1-0 ; 
       utl07-SS_a3.1-0 ; 
       utl07-SS_a4.1-0 ; 
       utl07-SS_a5.1-0 ; 
       utl07-SS_b1.1-0 ; 
       utl07-SS_b2.1-0 ; 
       utl07-SS_b3.1-0 ; 
       utl07-SS_b4.1-0 ; 
       utl07-SS_b5.1-0 ; 
       utl07-SS_c1.1-0 ; 
       utl07-SS_c2.1-0 ; 
       utl07-SS_c3.1-0 ; 
       utl07-SS_c4.1-0 ; 
       utl07-SS_c5.1-0 ; 
       utl07-SS_d1.1-0 ; 
       utl07-SS_d2.1-0 ; 
       utl07-SS_d3.1-0 ; 
       utl07-SS_d4.1-0 ; 
       utl07-SS_d5.1-0 ; 
       utl07-SSa1.1-0 ; 
       utl07-still_more_strobes.1-0 ; 
       utl07-strobes.1-0 ; 
       utl07-tarmour.1-0 ; 
       utl07-tractr1.1-0 ; 
       utl07-tractr1a.1-0 ; 
       utl07-tractr1b.1-0 ; 
       utl07-tractr2.1-0 ; 
       utl07-tractr2a.1-0 ; 
       utl07-tractr2b.1-0 ; 
       utl07-tractr3.1-0 ; 
       utl07-tractr3a.1-0 ; 
       utl07-tractr3b.1-0 ; 
       utl07-tractr4.1-0 ; 
       utl07-tractr4a.1-0 ; 
       utl07-tractr4b.1-0 ; 
       utl07-yet_more_Strobes.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/pete_data2/space_station/utl/utl07/PICTURES/utl07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl07-rotate.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       lg_gate_sAPTL-t2d1.3-0 ; 
       lg_gate_sAPTL-t2d11.3-0 ; 
       lg_gate_sAPTL-t2d12.3-0 ; 
       lg_gate_sAPTL-t2d13.3-0 ; 
       lg_gate_sAPTL-t2d14.3-0 ; 
       lg_gate_sAPTL-t2d15.3-0 ; 
       lg_gate_sAPTL-t2d17.3-0 ; 
       lg_gate_sAPTL-t2d18.3-0 ; 
       lg_gate_sAPTL-t2d19.3-0 ; 
       lg_gate_sAPTL-t2d20.3-0 ; 
       lg_gate_sAPTL-t2d21.3-0 ; 
       lg_gate_sAPTL-t2d22.3-0 ; 
       lg_gate_sAPTL-t2d23.3-0 ; 
       lg_gate_sAPTL-t2d24.3-0 ; 
       lg_gate_sAPTL-t2d25.3-0 ; 
       lg_gate_sAPTL-t2d26.3-0 ; 
       lg_gate_sAPTL-t2d27.3-0 ; 
       lg_gate_sAPTL-t2d28.3-0 ; 
       lg_gate_sAPTL-t2d29.3-0 ; 
       lg_gate_sAPTL-t2d30.3-0 ; 
       lg_gate_sAPTL-t2d31.3-0 ; 
       lg_gate_sAPTL-t2d32.3-0 ; 
       lg_gate_sAPTL-t2d33.3-0 ; 
       lg_gate_sAPTL-t2d34.3-0 ; 
       lg_gate_sAPTL-t2d35.3-0 ; 
       lg_gate_sAPTL-t2d36.3-0 ; 
       lg_gate_sAPTL-t2d37.3-0 ; 
       lg_gate_sAPTL-t2d38.3-0 ; 
       lg_gate_sAPTL-t2d39.3-0 ; 
       lg_gate_sAPTL-t2d40.3-0 ; 
       lg_gate_sAPTL-t2d41.3-0 ; 
       lg_gate_sAPTL-t2d42.3-0 ; 
       lg_gate_sAPTL-t2d43.3-0 ; 
       lg_gate_sAPTL-t2d6.3-0 ; 
       lg_gate_sAPTL-t2d7.3-0 ; 
       lg_gate_sAPTL-t2d9.3-0 ; 
       lg_gate_sAPTL-z.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 6 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 6 110 ; 
       6 8 110 ; 
       7 5 110 ; 
       9 34 110 ; 
       10 0 110 ; 
       11 2 110 ; 
       12 32 110 ; 
       13 32 110 ; 
       14 32 110 ; 
       15 32 110 ; 
       16 33 110 ; 
       17 33 110 ; 
       18 33 110 ; 
       19 33 110 ; 
       20 33 110 ; 
       21 7 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
       25 7 110 ; 
       26 47 110 ; 
       27 47 110 ; 
       28 47 110 ; 
       29 47 110 ; 
       30 47 110 ; 
       31 32 110 ; 
       32 5 110 ; 
       33 5 110 ; 
       34 1 110 ; 
       35 8 110 ; 
       36 35 110 ; 
       37 35 110 ; 
       38 8 110 ; 
       39 38 110 ; 
       40 38 110 ; 
       41 8 110 ; 
       42 41 110 ; 
       43 41 110 ; 
       44 8 110 ; 
       45 44 110 ; 
       46 44 110 ; 
       47 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 67 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 34 300 ; 
       2 37 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 24 300 ; 
       2 42 300 ; 
       2 33 300 ; 
       3 68 300 ; 
       3 8 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       4 28 300 ; 
       5 40 300 ; 
       5 41 300 ; 
       9 60 300 ; 
       10 65 300 ; 
       11 43 300 ; 
       12 51 300 ; 
       13 52 300 ; 
       14 53 300 ; 
       15 54 300 ; 
       16 55 300 ; 
       17 56 300 ; 
       18 57 300 ; 
       19 58 300 ; 
       20 59 300 ; 
       21 61 300 ; 
       22 62 300 ; 
       23 63 300 ; 
       24 64 300 ; 
       25 44 300 ; 
       26 45 300 ; 
       27 46 300 ; 
       28 47 300 ; 
       29 48 300 ; 
       30 49 300 ; 
       31 50 300 ; 
       34 66 300 ; 
       34 3 300 ; 
       34 4 300 ; 
       34 5 300 ; 
       34 27 300 ; 
       34 32 300 ; 
       35 12 300 ; 
       35 39 300 ; 
       36 14 300 ; 
       37 13 300 ; 
       38 21 300 ; 
       38 38 300 ; 
       39 23 300 ; 
       40 22 300 ; 
       41 18 300 ; 
       41 36 300 ; 
       42 20 300 ; 
       43 19 300 ; 
       44 15 300 ; 
       44 35 300 ; 
       45 17 300 ; 
       46 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 33 401 ; 
       2 34 401 ; 
       4 36 401 ; 
       5 35 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 26 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 30 401 ; 
       38 31 401 ; 
       39 32 401 ; 
       41 0 401 ; 
       42 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 75 -6 0 MPRFLG 0 ; 
       4 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 MPRFLG 0 ; 
       8 SCHEM 42.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 80 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 82.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 60 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 65 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 25 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 30 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 45 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 35 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 40 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 42.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 50 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 52.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 55 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 70 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 65 -6 0 MPRFLG 0 ; 
       33 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 80 -6 0 MPRFLG 0 ; 
       35 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       36 SCHEM 5 -4 0 MPRFLG 0 ; 
       37 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       38 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       39 SCHEM 10 -4 0 MPRFLG 0 ; 
       40 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       41 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       42 SCHEM 15 -4 0 MPRFLG 0 ; 
       43 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       44 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       45 SCHEM 20 -4 0 MPRFLG 0 ; 
       46 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       47 SCHEM 52.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 71.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 69 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 81.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 81.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 81.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 81.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 81.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
