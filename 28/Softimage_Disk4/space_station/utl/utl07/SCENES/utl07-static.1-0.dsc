SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.24-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       lg_gate_sAPTL-mat11.1-0 ; 
       lg_gate_sAPTL-mat12.1-0 ; 
       lg_gate_sAPTL-mat13.1-0 ; 
       lg_gate_sAPTL-mat14.1-0 ; 
       lg_gate_sAPTL-mat15.1-0 ; 
       lg_gate_sAPTL-mat16.1-0 ; 
       lg_gate_sAPTL-mat18.1-0 ; 
       lg_gate_sAPTL-mat19.1-0 ; 
       lg_gate_sAPTL-mat20.2-0 ; 
       lg_gate_sAPTL-mat25.1-0 ; 
       lg_gate_sAPTL-mat26.1-0 ; 
       lg_gate_sAPTL-mat27.1-0 ; 
       lg_gate_sAPTL-mat28.1-0 ; 
       lg_gate_sAPTL-mat29.1-0 ; 
       lg_gate_sAPTL-mat30.1-0 ; 
       lg_gate_sAPTL-mat31.1-0 ; 
       lg_gate_sAPTL-mat32.1-0 ; 
       lg_gate_sAPTL-mat33.1-0 ; 
       lg_gate_sAPTL-mat34.1-0 ; 
       lg_gate_sAPTL-mat35.1-0 ; 
       lg_gate_sAPTL-mat36.1-0 ; 
       lg_gate_sAPTL-mat38.1-0 ; 
       lg_gate_sAPTL-mat39.1-0 ; 
       lg_gate_sAPTL-mat40.1-0 ; 
       lg_gate_sAPTL-mat41.1-0 ; 
       lg_gate_sAPTL-mat42.2-0 ; 
       lg_gate_sAPTL-mat43.2-0 ; 
       lg_gate_sAPTL-mat44.2-0 ; 
       lg_gate_sAPTL-mat45.1-0 ; 
       lg_gate_sAPTL-mat47.1-0 ; 
       lg_gate_sAPTL-mat48.1-0 ; 
       lg_gate_sAPTL-mat49.1-0 ; 
       lg_gate_sAPTL-mat50.1-0 ; 
       lg_gate_sAPTL-mat51.1-0 ; 
       lg_gate_sAPTL-mat6.1-0 ; 
       lg_gate_sAPTL-mat7.1-0 ; 
       Utl07-mat2.1-0 ; 
       Utl07-mat3.1-0 ; 
       Utl07-mat4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       utl07-antenn.1-0 ; 
       utl07-antenn0.1-0 ; 
       utl07-barmour.1-0 ; 
       utl07-beacon.5-0 ; 
       utl07-fuselg.1-0 ; 
       utl07-fuselg0.5-0 ; 
       utl07-more_strobes.1-0 ; 
       utl07-root.16-0 ROOT ; 
       utl07-still_more_strobes.1-0 ; 
       utl07-strobes.1-0 ; 
       utl07-tarmour.1-0 ; 
       utl07-tractr1.1-0 ; 
       utl07-tractr1a.1-0 ; 
       utl07-tractr1b.1-0 ; 
       utl07-tractr2.1-0 ; 
       utl07-tractr2a.1-0 ; 
       utl07-tractr2b.1-0 ; 
       utl07-tractr3.1-0 ; 
       utl07-tractr3a.1-0 ; 
       utl07-tractr3b.1-0 ; 
       utl07-tractr4.1-0 ; 
       utl07-tractr4a.1-0 ; 
       utl07-tractr4b.1-0 ; 
       utl07-yet_more_Strobes.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl07/PICTURES/utl07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl07-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       lg_gate_sAPTL-t2d1.5-0 ; 
       lg_gate_sAPTL-t2d11.5-0 ; 
       lg_gate_sAPTL-t2d12.5-0 ; 
       lg_gate_sAPTL-t2d13.5-0 ; 
       lg_gate_sAPTL-t2d17.5-0 ; 
       lg_gate_sAPTL-t2d18.5-0 ; 
       lg_gate_sAPTL-t2d19.5-0 ; 
       lg_gate_sAPTL-t2d20.5-0 ; 
       lg_gate_sAPTL-t2d21.5-0 ; 
       lg_gate_sAPTL-t2d22.5-0 ; 
       lg_gate_sAPTL-t2d23.5-0 ; 
       lg_gate_sAPTL-t2d24.5-0 ; 
       lg_gate_sAPTL-t2d25.5-0 ; 
       lg_gate_sAPTL-t2d26.5-0 ; 
       lg_gate_sAPTL-t2d27.5-0 ; 
       lg_gate_sAPTL-t2d28.5-0 ; 
       lg_gate_sAPTL-t2d31.5-0 ; 
       lg_gate_sAPTL-t2d32.5-0 ; 
       lg_gate_sAPTL-t2d33.5-0 ; 
       lg_gate_sAPTL-t2d34.5-0 ; 
       lg_gate_sAPTL-t2d35.5-0 ; 
       lg_gate_sAPTL-t2d36.5-0 ; 
       lg_gate_sAPTL-t2d37.5-0 ; 
       lg_gate_sAPTL-t2d39.5-0 ; 
       lg_gate_sAPTL-t2d40.5-0 ; 
       lg_gate_sAPTL-t2d41.5-0 ; 
       lg_gate_sAPTL-t2d42.5-0 ; 
       lg_gate_sAPTL-t2d43.5-0 ; 
       lg_gate_sAPTL-t2d6.5-0 ; 
       lg_gate_sAPTL-t2d7.5-0 ; 
       lg_gate_sAPTL-t2d9.5-0 ; 
       lg_gate_sAPTL-z.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 5 110 ; 
       5 7 110 ; 
       6 4 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 1 110 ; 
       11 7 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 7 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 7 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 7 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 37 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 29 300 ; 
       2 38 300 ; 
       2 8 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 24 300 ; 
       4 34 300 ; 
       4 35 300 ; 
       10 36 300 ; 
       10 3 300 ; 
       10 4 300 ; 
       10 5 300 ; 
       10 23 300 ; 
       10 28 300 ; 
       11 9 300 ; 
       11 33 300 ; 
       12 11 300 ; 
       13 10 300 ; 
       14 18 300 ; 
       14 32 300 ; 
       15 20 300 ; 
       16 19 300 ; 
       17 15 300 ; 
       17 31 300 ; 
       18 17 300 ; 
       19 16 300 ; 
       20 12 300 ; 
       20 30 300 ; 
       21 14 300 ; 
       22 13 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 28 401 ; 
       2 29 401 ; 
       4 31 401 ; 
       5 30 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       15 10 401 ; 
       16 11 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 26 401 ; 
       33 27 401 ; 
       35 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 35 -6 0 MPRFLG 0 ; 
       3 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 MPRFLG 0 ; 
       7 SCHEM 22.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 30 -6 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 40 -6 0 MPRFLG 0 ; 
       11 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 10 -4 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       18 SCHEM 15 -4 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       21 SCHEM 20 -4 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 27.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
