SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       youmap-cam_int1.61-0 ROOT ; 
       youmap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       add_frames-default13.6-0 ; 
       add_frames-mat2.6-0 ; 
       add_frames-mat23.6-0 ; 
       add_frames-mat24.6-0 ; 
       add_frames-mat25.6-0 ; 
       add_frames-mat26.6-0 ; 
       add_frames-mat27.6-0 ; 
       add_frames-mat28.6-0 ; 
       add_frames-mat29.6-0 ; 
       add_frames-mat30.6-0 ; 
       add_frames-mat31.6-0 ; 
       add_frames-mat33.6-0 ; 
       add_frames-mat34.6-0 ; 
       add_frames-mat37.6-0 ; 
       add_frames-mat38.6-0 ; 
       add_frames-mat39.6-0 ; 
       add_frames-mat40.6-0 ; 
       add_frames-mat41.6-0 ; 
       add_frames-mat42.6-0 ; 
       add_frames-mat43.6-0 ; 
       add_frames-mat44.6-0 ; 
       add_frames-mat45.6-0 ; 
       add_frames-mat46.6-0 ; 
       add_frames-mat47.6-0 ; 
       add_frames-mat48.6-0 ; 
       add_frames-mat49.6-0 ; 
       add_frames-mat50.6-0 ; 
       add_frames-mat51.6-0 ; 
       add_frames-mat52.6-0 ; 
       add_frames-mat53.6-0 ; 
       add_frames-mat54.6-0 ; 
       add_frames-mat55.6-0 ; 
       add_frames-mat56.6-0 ; 
       add_frames-mat57.6-0 ; 
       add_frames-mat58.6-0 ; 
       add_frames-mat59.6-0 ; 
       add_frames-mat60.6-0 ; 
       add_frames-mat61.6-0 ; 
       add_frames-mat62.6-0 ; 
       add_frames-mat63.6-0 ; 
       add_frames-mat64.6-0 ; 
       add_frames-mat66.6-0 ; 
       add_frames-mat67.6-0 ; 
       add_frames-nose_white-center.1-0.6-0 ; 
       add_frames-nose_white-center.1-0_1.6-0 ; 
       add_frames-nose_white-center.1-0_2.6-0 ; 
       add_frames-nose_white-center.1-0_3.6-0 ; 
       add_frames-nose_white-center.1-0_4.6-0 ; 
       add_frames-nose_white-center.1-2.6-0 ; 
       add_frames-nose_white-center.1-3.6-0 ; 
       add_frames-nose_white-center.1-4.6-0 ; 
       add_frames-nose_white-center.1-5.6-0 ; 
       add_frames-nose_white-center.1-6.6-0 ; 
       add_frames-nose_white-center.1-7.6-0 ; 
       add_frames-nose_white-center.1-8.6-0 ; 
       add_frames-nose_white-center.1-9.6-0 ; 
       ROTATE-mat68.3-0 ; 
       ROTATE-mat69.3-0 ; 
       ROTATE-mat70.3-0 ; 
       ROTATE-mat71.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       ROTATE-ahubcon.1-0 ; 
       ROTATE-armour.1-0 ; 
       ROTATE-bay1.1-0 ; 
       ROTATE-bay2.1-0 ; 
       ROTATE-doccon.1-0 ; 
       ROTATE-engine.2-0 ; 
       ROTATE-fhubcon.3-0 ; 
       ROTATE-fuselg1.1-0 ; 
       ROTATE-fuselg2.1-0 ; 
       ROTATE-garage1A.1-0 ; 
       ROTATE-garage1A1.1-0 ; 
       ROTATE-garage1B.1-0 ; 
       ROTATE-garage1B1.1-0 ; 
       ROTATE-garage1C.1-0 ; 
       ROTATE-garage1C1.1-0 ; 
       ROTATE-garage1D.1-0 ; 
       ROTATE-garage1D1.1-0 ; 
       ROTATE-garageE.1-0 ; 
       ROTATE-garageE1.1-0 ; 
       ROTATE-LAUNCH_TUBE.1-0 ; 
       ROTATE-LAUNCH_TUBE_1.1-0 ; 
       ROTATE-launch1.1-0 ; 
       ROTATE-launch2.1-0 ; 
       ROTATE-ldoccon.1-0 ; 
       ROTATE-lights1.1-0 ; 
       ROTATE-lights2.2-0 ; 
       ROTATE-rdoccon.1-0 ; 
       ROTATE-skin2.2-0 ; 
       ROTATE-SSa0.1-0 ; 
       ROTATE-SSb0.1-0 ; 
       ROTATE-SSb1.1-0 ; 
       ROTATE-SSb3.1-0 ; 
       ROTATE-SSb4.1-0 ; 
       ROTATE-SSb5.1-0 ; 
       ROTATE-SSb6.1-0 ; 
       ROTATE-SSm1.1-0 ; 
       ROTATE-SSm2.1-0 ; 
       ROTATE-SSm3.1-0 ; 
       ROTATE-SSm4.1-0 ; 
       ROTATE-SSr0.1-0 ; 
       ROTATE-SSr1.1-0 ; 
       ROTATE-SSr2.1-0 ; 
       ROTATE-SSr3.1-0 ; 
       ROTATE-SSr4.1-0 ; 
       ROTATE-tractr1.1-0 ; 
       ROTATE-tractr2.1-0 ; 
       ROTATE-tractr3.1-0 ; 
       ROTATE-utl20.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-ROTATE.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       add_frames-rendermap2.12-0 ; 
       add_frames-t2d23.7-0 ; 
       add_frames-t2d24.7-0 ; 
       add_frames-t2d25.7-0 ; 
       add_frames-t2d26.7-0 ; 
       add_frames-t2d27.7-0 ; 
       add_frames-t2d28.7-0 ; 
       add_frames-t2d29.7-0 ; 
       add_frames-t2d31.7-0 ; 
       add_frames-t2d32.7-0 ; 
       add_frames-t2d35.7-0 ; 
       add_frames-t2d36.7-0 ; 
       add_frames-t2d37.7-0 ; 
       add_frames-t2d38.7-0 ; 
       add_frames-t2d39.7-0 ; 
       add_frames-t2d40.7-0 ; 
       add_frames-t2d41.7-0 ; 
       add_frames-t2d42.7-0 ; 
       add_frames-t2d43.7-0 ; 
       add_frames-t2d44.7-0 ; 
       add_frames-t2d45.7-0 ; 
       add_frames-t2d46.7-0 ; 
       add_frames-t2d47.7-0 ; 
       add_frames-t2d48.7-0 ; 
       add_frames-t2d49.7-0 ; 
       add_frames-t2d50.7-0 ; 
       add_frames-t2d51.8-0 ; 
       add_frames-t2d52.8-0 ; 
       add_frames-t2d53.8-0 ; 
       add_frames-t2d54.8-0 ; 
       add_frames-t2d55.7-0 ; 
       add_frames-t2d56.8-0 ; 
       add_frames-t2d57.8-0 ; 
       add_frames-t2d58.7-0 ; 
       add_frames-t2d59.12-0 ; 
       ROTATE-t2d60.3-0 ; 
       ROTATE-t2d61.3-0 ; 
       ROTATE-t2d62.3-0 ; 
       ROTATE-t2d63.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       2 47 110 ; 
       3 47 110 ; 
       4 1 110 ; 
       5 8 110 ; 
       6 7 110 ; 
       7 47 110 ; 
       8 7 110 ; 
       9 2 110 ; 
       10 3 110 ; 
       11 2 110 ; 
       12 3 110 ; 
       13 2 110 ; 
       14 3 110 ; 
       15 2 110 ; 
       16 3 110 ; 
       17 2 110 ; 
       18 3 110 ; 
       19 47 110 ; 
       20 47 110 ; 
       21 47 110 ; 
       22 47 110 ; 
       23 6 110 ; 
       24 7 110 ; 
       25 24 110 ; 
       26 6 110 ; 
       27 47 110 ; 
       28 8 110 ; 
       29 5 110 ; 
       30 29 110 ; 
       31 29 110 ; 
       32 29 110 ; 
       33 39 110 ; 
       34 39 110 ; 
       35 28 110 ; 
       36 28 110 ; 
       37 28 110 ; 
       38 28 110 ; 
       39 0 110 ; 
       40 39 110 ; 
       41 39 110 ; 
       42 39 110 ; 
       43 39 110 ; 
       44 7 110 ; 
       45 44 110 ; 
       46 45 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 27 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 38 300 ; 
       5 1 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       7 0 300 ; 
       7 34 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       19 56 300 ; 
       19 57 300 ; 
       20 58 300 ; 
       20 59 300 ; 
       24 6 300 ; 
       24 7 300 ; 
       24 8 300 ; 
       24 9 300 ; 
       25 5 300 ; 
       27 41 300 ; 
       27 42 300 ; 
       30 43 300 ; 
       31 48 300 ; 
       32 49 300 ; 
       33 54 300 ; 
       34 55 300 ; 
       35 44 300 ; 
       36 45 300 ; 
       37 46 300 ; 
       38 47 300 ; 
       40 50 300 ; 
       41 51 300 ; 
       42 52 300 ; 
       43 53 300 ; 
       44 26 300 ; 
       45 23 300 ; 
       45 24 300 ; 
       45 25 300 ; 
       46 19 300 ; 
       46 20 300 ; 
       46 21 300 ; 
       46 22 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 33 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 0 401 ; 
       42 34 401 ; 
       56 35 401 ; 
       57 36 401 ; 
       58 37 401 ; 
       59 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 45 -4 0 MPRFLG 0 ; 
       2 SCHEM 55 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 67.5 -2 0 WIRECOL 9 7 DISPLAY 3 2 MPRFLG 0 ; 
       4 SCHEM 45 -6 0 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 MPRFLG 0 ; 
       6 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 MPRFLG 0 ; 
       9 SCHEM 50 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 62.5 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 52.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 65 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 55 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 67.5 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 57.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 70 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 60 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -4 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 75 -2 0 MPRFLG 0 ; 
       20 SCHEM 77.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 80 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 40 -4 0 MPRFLG 0 ; 
       25 SCHEM 40 -6 0 MPRFLG 0 ; 
       26 SCHEM 35 -6 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       28 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       29 SCHEM 20 -8 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 20 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 23.75 -10 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 15 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 2.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 32.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       36 SCHEM 25 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       37 SCHEM 27.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       38 SCHEM 30 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       39 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       40 SCHEM 5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 7.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       45 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       46 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       47 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 22.75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
