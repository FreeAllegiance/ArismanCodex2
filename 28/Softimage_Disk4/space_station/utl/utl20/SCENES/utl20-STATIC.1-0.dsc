SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       youmap-cam_int1.72-0 ROOT ; 
       youmap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       add_frames-default13.6-0 ; 
       add_frames-mat2.6-0 ; 
       add_frames-mat23.6-0 ; 
       add_frames-mat24.6-0 ; 
       add_frames-mat25.6-0 ; 
       add_frames-mat27.6-0 ; 
       add_frames-mat28.6-0 ; 
       add_frames-mat29.6-0 ; 
       add_frames-mat30.6-0 ; 
       add_frames-mat31.6-0 ; 
       add_frames-mat33.6-0 ; 
       add_frames-mat34.6-0 ; 
       add_frames-mat37.6-0 ; 
       add_frames-mat38.6-0 ; 
       add_frames-mat39.6-0 ; 
       add_frames-mat40.6-0 ; 
       add_frames-mat41.6-0 ; 
       add_frames-mat42.6-0 ; 
       add_frames-mat43.6-0 ; 
       add_frames-mat44.6-0 ; 
       add_frames-mat45.6-0 ; 
       add_frames-mat46.6-0 ; 
       add_frames-mat47.6-0 ; 
       add_frames-mat48.6-0 ; 
       add_frames-mat49.6-0 ; 
       add_frames-mat50.6-0 ; 
       add_frames-mat51.6-0 ; 
       add_frames-mat52.6-0 ; 
       add_frames-mat53.6-0 ; 
       add_frames-mat54.6-0 ; 
       add_frames-mat55.6-0 ; 
       add_frames-mat56.6-0 ; 
       add_frames-mat57.6-0 ; 
       add_frames-mat58.6-0 ; 
       add_frames-mat59.6-0 ; 
       add_frames-mat60.6-0 ; 
       add_frames-mat61.6-0 ; 
       add_frames-mat62.6-0 ; 
       add_frames-mat63.6-0 ; 
       add_frames-mat64.6-0 ; 
       add_frames-mat66.6-0 ; 
       add_frames-mat67.6-0 ; 
       STATIC-mat68.1-0 ; 
       STATIC-mat69.1-0 ; 
       STATIC-mat70.1-0 ; 
       STATIC-mat71.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       STATIC-ahubcon.1-0 ; 
       STATIC-armour.1-0 ; 
       STATIC-doccon.1-0 ; 
       STATIC-engine.2-0 ; 
       STATIC-fhubcon.3-0 ; 
       STATIC-fuselg1.1-0 ; 
       STATIC-fuselg2.1-0 ; 
       STATIC-LAUNCH_TUBE.1-0 ; 
       STATIC-LAUNCH_TUBE_1.1-0 ; 
       STATIC-lights1.1-0 ; 
       STATIC-skin2.2-0 ; 
       STATIC-tractr1.1-0 ; 
       STATIC-tractr2.1-0 ; 
       STATIC-tractr3.1-0 ; 
       STATIC-utl20.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl20/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl20/PICTURES/utl20 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl20/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       add_frames-rendermap2.12-0 ; 
       add_frames-t2d23.7-0 ; 
       add_frames-t2d24.7-0 ; 
       add_frames-t2d26.7-0 ; 
       add_frames-t2d27.7-0 ; 
       add_frames-t2d28.7-0 ; 
       add_frames-t2d29.7-0 ; 
       add_frames-t2d31.7-0 ; 
       add_frames-t2d32.7-0 ; 
       add_frames-t2d35.7-0 ; 
       add_frames-t2d36.7-0 ; 
       add_frames-t2d37.7-0 ; 
       add_frames-t2d38.7-0 ; 
       add_frames-t2d39.7-0 ; 
       add_frames-t2d40.7-0 ; 
       add_frames-t2d41.7-0 ; 
       add_frames-t2d42.7-0 ; 
       add_frames-t2d43.7-0 ; 
       add_frames-t2d44.7-0 ; 
       add_frames-t2d45.7-0 ; 
       add_frames-t2d46.7-0 ; 
       add_frames-t2d47.7-0 ; 
       add_frames-t2d48.7-0 ; 
       add_frames-t2d49.7-0 ; 
       add_frames-t2d50.7-0 ; 
       add_frames-t2d51.8-0 ; 
       add_frames-t2d52.8-0 ; 
       add_frames-t2d53.8-0 ; 
       add_frames-t2d54.8-0 ; 
       add_frames-t2d55.7-0 ; 
       add_frames-t2d56.8-0 ; 
       add_frames-t2d57.8-0 ; 
       add_frames-t2d58.7-0 ; 
       add_frames-t2d59.12-0 ; 
       STATIC-t2d60.1-0 ; 
       STATIC-t2d61.1-0 ; 
       STATIC-t2d62.1-0 ; 
       STATIC-t2d63.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 6 110 ; 
       4 5 110 ; 
       5 14 110 ; 
       6 5 110 ; 
       7 14 110 ; 
       8 14 110 ; 
       9 5 110 ; 
       10 14 110 ; 
       11 5 110 ; 
       12 11 110 ; 
       13 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 13 300 ; 
       0 14 300 ; 
       0 26 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 37 300 ; 
       3 1 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       5 0 300 ; 
       5 33 300 ; 
       5 34 300 ; 
       5 35 300 ; 
       5 36 300 ; 
       5 38 300 ; 
       5 39 300 ; 
       6 27 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       6 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       7 42 300 ; 
       7 43 300 ; 
       8 44 300 ; 
       8 45 300 ; 
       9 5 300 ; 
       9 6 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       10 40 300 ; 
       10 41 300 ; 
       11 25 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 32 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       16 11 401 ; 
       17 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 0 401 ; 
       41 33 401 ; 
       42 34 401 ; 
       43 35 401 ; 
       44 36 401 ; 
       45 37 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 28.3958 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
