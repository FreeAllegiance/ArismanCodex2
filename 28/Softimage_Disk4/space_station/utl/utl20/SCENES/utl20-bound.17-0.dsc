SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       youmap-cam_int1.90-0 ROOT ; 
       youmap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       bound-mat72.1-0 ; 
       bound-mat73.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bounding_model.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl20/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl20/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-bound.17-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       bound-rendermap3.1-0 ; 
       bound-t2d64.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 110 ; 
       2 3 110 ; 
       0 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
