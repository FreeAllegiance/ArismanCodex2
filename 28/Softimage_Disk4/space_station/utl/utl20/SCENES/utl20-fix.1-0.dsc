SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fix-utl20.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       youmap-cam_int1.37-0 ROOT ; 
       youmap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 61     
       add_frames-default13.5-0 ; 
       add_frames-mat2.5-0 ; 
       add_frames-mat23.5-0 ; 
       add_frames-mat24.5-0 ; 
       add_frames-mat25.5-0 ; 
       add_frames-mat26.5-0 ; 
       add_frames-mat27.5-0 ; 
       add_frames-mat28.5-0 ; 
       add_frames-mat29.5-0 ; 
       add_frames-mat30.5-0 ; 
       add_frames-mat31.5-0 ; 
       add_frames-mat33.5-0 ; 
       add_frames-mat34.5-0 ; 
       add_frames-mat37.5-0 ; 
       add_frames-mat38.5-0 ; 
       add_frames-mat39.5-0 ; 
       add_frames-mat40.5-0 ; 
       add_frames-mat41.5-0 ; 
       add_frames-mat42.5-0 ; 
       add_frames-mat43.5-0 ; 
       add_frames-mat44.5-0 ; 
       add_frames-mat45.5-0 ; 
       add_frames-mat46.5-0 ; 
       add_frames-mat47.5-0 ; 
       add_frames-mat48.5-0 ; 
       add_frames-mat49.5-0 ; 
       add_frames-mat50.5-0 ; 
       add_frames-mat51.5-0 ; 
       add_frames-mat52.5-0 ; 
       add_frames-mat53.5-0 ; 
       add_frames-mat54.5-0 ; 
       add_frames-mat55.5-0 ; 
       add_frames-mat56.5-0 ; 
       add_frames-mat57.5-0 ; 
       add_frames-mat58.5-0 ; 
       add_frames-mat59.5-0 ; 
       add_frames-mat60.5-0 ; 
       add_frames-mat61.5-0 ; 
       add_frames-mat62.5-0 ; 
       add_frames-mat63.5-0 ; 
       add_frames-mat64.5-0 ; 
       add_frames-mat66.5-0 ; 
       add_frames-mat67.5-0 ; 
       add_frames-nose_white-center.1-0.5-0 ; 
       add_frames-nose_white-center.1-0_1.5-0 ; 
       add_frames-nose_white-center.1-0_2.5-0 ; 
       add_frames-nose_white-center.1-0_3.5-0 ; 
       add_frames-nose_white-center.1-0_4.5-0 ; 
       add_frames-nose_white-center.1-1.5-0 ; 
       add_frames-nose_white-center.1-2.5-0 ; 
       add_frames-nose_white-center.1-3.5-0 ; 
       add_frames-nose_white-center.1-4.5-0 ; 
       add_frames-nose_white-center.1-5.5-0 ; 
       add_frames-nose_white-center.1-6.5-0 ; 
       add_frames-nose_white-center.1-7.5-0 ; 
       add_frames-nose_white-center.1-8.5-0 ; 
       add_frames-nose_white-center.1-9.5-0 ; 
       fix-mat68.1-0 ; 
       fix-mat69.1-0 ; 
       fix-mat70.1-0 ; 
       fix-mat71.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 49     
       fix-ahubcon.1-0 ; 
       fix-armour.1-0 ; 
       fix-bay1.1-0 ; 
       fix-bay2.1-0 ; 
       fix-doccon.1-0 ; 
       fix-engine.2-0 ; 
       fix-fhubcon.3-0 ; 
       fix-fuselg1.1-0 ; 
       fix-fuselg2.1-0 ; 
       fix-garage1A.1-0 ; 
       fix-garage1A1.1-0 ; 
       fix-garage1B.1-0 ; 
       fix-garage1B1.1-0 ; 
       fix-garage1C.1-0 ; 
       fix-garage1C1.1-0 ; 
       fix-garage1D.1-0 ; 
       fix-garage1D1.1-0 ; 
       fix-garageE.1-0 ; 
       fix-garageE1.1-0 ; 
       fix-LAUNCH_TUBE.1-0 ; 
       fix-LAUNCH_TUBE_1.1-0 ; 
       fix-launch1.1-0 ; 
       fix-launch2.1-0 ; 
       fix-ldoccon.1-0 ; 
       fix-lights1.1-0 ; 
       fix-lights2.2-0 ; 
       fix-rdoccon.1-0 ; 
       fix-skin2.2-0 ; 
       fix-SSa0.1-0 ; 
       fix-SSb0.1-0 ; 
       fix-SSb1.1-0 ; 
       fix-SSb2.1-0 ; 
       fix-SSb3.1-0 ; 
       fix-SSb4.1-0 ; 
       fix-SSb5.1-0 ; 
       fix-SSb6.1-0 ; 
       fix-SSm1.1-0 ; 
       fix-SSm2.1-0 ; 
       fix-SSm3.1-0 ; 
       fix-SSm4.1-0 ; 
       fix-SSr0.1-0 ; 
       fix-SSr1.1-0 ; 
       fix-SSr2.1-0 ; 
       fix-SSr3.1-0 ; 
       fix-SSr4.1-0 ; 
       fix-tractr1.1-0 ; 
       fix-tractr2.1-0 ; 
       fix-tractr3.1-0 ; 
       fix-utl20.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-fix.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       add_frames-rendermap2.11-0 ; 
       add_frames-t2d23.6-0 ; 
       add_frames-t2d24.6-0 ; 
       add_frames-t2d25.6-0 ; 
       add_frames-t2d26.6-0 ; 
       add_frames-t2d27.6-0 ; 
       add_frames-t2d28.6-0 ; 
       add_frames-t2d29.6-0 ; 
       add_frames-t2d31.6-0 ; 
       add_frames-t2d32.6-0 ; 
       add_frames-t2d35.6-0 ; 
       add_frames-t2d36.6-0 ; 
       add_frames-t2d37.6-0 ; 
       add_frames-t2d38.6-0 ; 
       add_frames-t2d39.6-0 ; 
       add_frames-t2d40.6-0 ; 
       add_frames-t2d41.6-0 ; 
       add_frames-t2d42.6-0 ; 
       add_frames-t2d43.6-0 ; 
       add_frames-t2d44.6-0 ; 
       add_frames-t2d45.6-0 ; 
       add_frames-t2d46.6-0 ; 
       add_frames-t2d47.6-0 ; 
       add_frames-t2d48.6-0 ; 
       add_frames-t2d49.6-0 ; 
       add_frames-t2d50.6-0 ; 
       add_frames-t2d51.7-0 ; 
       add_frames-t2d52.7-0 ; 
       add_frames-t2d53.7-0 ; 
       add_frames-t2d54.7-0 ; 
       add_frames-t2d55.6-0 ; 
       add_frames-t2d56.7-0 ; 
       add_frames-t2d57.7-0 ; 
       add_frames-t2d58.6-0 ; 
       add_frames-t2d59.11-0 ; 
       fix-t2d60.1-0 ; 
       fix-t2d61.1-0 ; 
       fix-t2d62.1-0 ; 
       fix-t2d63.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       2 48 110 ; 
       3 48 110 ; 
       4 1 110 ; 
       5 8 110 ; 
       6 7 110 ; 
       7 48 110 ; 
       8 7 110 ; 
       9 2 110 ; 
       10 3 110 ; 
       11 2 110 ; 
       12 3 110 ; 
       13 2 110 ; 
       14 3 110 ; 
       15 2 110 ; 
       16 3 110 ; 
       17 2 110 ; 
       18 3 110 ; 
       19 48 110 ; 
       20 48 110 ; 
       21 48 110 ; 
       22 48 110 ; 
       23 6 110 ; 
       24 7 110 ; 
       25 24 110 ; 
       26 6 110 ; 
       27 48 110 ; 
       28 8 110 ; 
       29 5 110 ; 
       30 29 110 ; 
       31 29 110 ; 
       32 29 110 ; 
       33 29 110 ; 
       34 40 110 ; 
       35 40 110 ; 
       36 28 110 ; 
       37 28 110 ; 
       38 28 110 ; 
       39 28 110 ; 
       40 0 110 ; 
       41 40 110 ; 
       42 40 110 ; 
       43 40 110 ; 
       44 40 110 ; 
       45 7 110 ; 
       46 45 110 ; 
       47 46 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 27 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 38 300 ; 
       5 1 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       7 0 300 ; 
       7 34 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       19 57 300 ; 
       19 58 300 ; 
       20 59 300 ; 
       20 60 300 ; 
       24 6 300 ; 
       24 7 300 ; 
       24 8 300 ; 
       24 9 300 ; 
       25 5 300 ; 
       27 41 300 ; 
       27 42 300 ; 
       30 43 300 ; 
       31 48 300 ; 
       32 49 300 ; 
       33 50 300 ; 
       34 55 300 ; 
       35 56 300 ; 
       36 44 300 ; 
       37 45 300 ; 
       38 46 300 ; 
       39 47 300 ; 
       41 51 300 ; 
       42 52 300 ; 
       43 53 300 ; 
       44 54 300 ; 
       45 26 300 ; 
       46 23 300 ; 
       46 24 300 ; 
       46 25 300 ; 
       47 19 300 ; 
       47 20 300 ; 
       47 21 300 ; 
       47 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       48 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 33 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 0 401 ; 
       42 34 401 ; 
       57 35 401 ; 
       58 36 401 ; 
       59 37 401 ; 
       60 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 57.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 70 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 25 -4 0 MPRFLG 0 ; 
       8 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 52.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 65 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 55 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 70 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 60 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 72.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 62.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 75 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 80 -4 0 MPRFLG 0 ; 
       21 SCHEM 82.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 85 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 40 -8 0 MPRFLG 0 ; 
       24 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 50 -4 0 MPRFLG 0 ; 
       28 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       29 SCHEM 21.25 -10 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 22.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 20 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 25 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 15 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 2.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 35 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       37 SCHEM 27.5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       38 SCHEM 30 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       39 SCHEM 32.5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       40 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       41 SCHEM 5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 7.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 10 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 12.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 45 -6 0 MPRFLG 0 ; 
       46 SCHEM 45 -8 0 MPRFLG 0 ; 
       47 SCHEM 45 -10 0 MPRFLG 0 ; 
       48 SCHEM 43.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30.25 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 79 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 79 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 44 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 30.25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 79 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 79 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 87.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
