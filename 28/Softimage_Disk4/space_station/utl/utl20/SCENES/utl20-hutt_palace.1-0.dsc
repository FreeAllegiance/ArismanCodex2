SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl20-utl20.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       hutt_palace-cam_int1.1-0 ROOT ; 
       hutt_palace-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       hutt_palace-inf_light1.1-0 ROOT ; 
       hutt_palace-inf_light2.1-0 ROOT ; 
       hutt_palace-light1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 75     
       hutt_palace-default13.1-0 ; 
       hutt_palace-mat10.1-0 ; 
       hutt_palace-mat11.1-0 ; 
       hutt_palace-mat12.1-0 ; 
       hutt_palace-mat13.1-0 ; 
       hutt_palace-mat14.1-0 ; 
       hutt_palace-mat15.1-0 ; 
       hutt_palace-mat16.1-0 ; 
       hutt_palace-mat17.1-0 ; 
       hutt_palace-mat18.1-0 ; 
       hutt_palace-mat19.1-0 ; 
       hutt_palace-mat2.1-0 ; 
       hutt_palace-mat20.1-0 ; 
       hutt_palace-mat21.1-0 ; 
       hutt_palace-mat22.1-0 ; 
       hutt_palace-mat23.1-0 ; 
       hutt_palace-mat24.1-0 ; 
       hutt_palace-mat25.1-0 ; 
       hutt_palace-mat26.1-0 ; 
       hutt_palace-mat27.1-0 ; 
       hutt_palace-mat28.1-0 ; 
       hutt_palace-mat29.1-0 ; 
       hutt_palace-mat3.1-0 ; 
       hutt_palace-mat30.1-0 ; 
       hutt_palace-mat31.1-0 ; 
       hutt_palace-mat33.1-0 ; 
       hutt_palace-mat34.1-0 ; 
       hutt_palace-mat37.1-0 ; 
       hutt_palace-mat38.1-0 ; 
       hutt_palace-mat39.1-0 ; 
       hutt_palace-mat4.1-0 ; 
       hutt_palace-mat40.1-0 ; 
       hutt_palace-mat41.1-0 ; 
       hutt_palace-mat42.1-0 ; 
       hutt_palace-mat43.1-0 ; 
       hutt_palace-mat44.1-0 ; 
       hutt_palace-mat45.1-0 ; 
       hutt_palace-mat46.1-0 ; 
       hutt_palace-mat47.1-0 ; 
       hutt_palace-mat48.1-0 ; 
       hutt_palace-mat49.1-0 ; 
       hutt_palace-mat5.1-0 ; 
       hutt_palace-mat50.1-0 ; 
       hutt_palace-mat51.1-0 ; 
       hutt_palace-mat52.1-0 ; 
       hutt_palace-mat53.1-0 ; 
       hutt_palace-mat54.1-0 ; 
       hutt_palace-mat55.1-0 ; 
       hutt_palace-mat56.1-0 ; 
       hutt_palace-mat57.1-0 ; 
       hutt_palace-mat58.1-0 ; 
       hutt_palace-mat59.1-0 ; 
       hutt_palace-mat6.1-0 ; 
       hutt_palace-mat60.1-0 ; 
       hutt_palace-mat61.1-0 ; 
       hutt_palace-mat62.1-0 ; 
       hutt_palace-mat63.1-0 ; 
       hutt_palace-mat64.1-0 ; 
       hutt_palace-mat7.1-0 ; 
       hutt_palace-mat8.1-0 ; 
       hutt_palace-mat9.1-0 ; 
       hutt_palace-nose_white-center.1-0.1-0 ; 
       hutt_palace-nose_white-center.1-0_1.1-0 ; 
       hutt_palace-nose_white-center.1-0_2.1-0 ; 
       hutt_palace-nose_white-center.1-0_3.1-0 ; 
       hutt_palace-nose_white-center.1-0_4.1-0 ; 
       hutt_palace-nose_white-center.1-1.1-0 ; 
       hutt_palace-nose_white-center.1-2.1-0 ; 
       hutt_palace-nose_white-center.1-3.1-0 ; 
       hutt_palace-nose_white-center.1-4.1-0 ; 
       hutt_palace-nose_white-center.1-5.1-0 ; 
       hutt_palace-nose_white-center.1-6.1-0 ; 
       hutt_palace-nose_white-center.1-7.1-0 ; 
       hutt_palace-nose_white-center.1-8.1-0 ; 
       hutt_palace-nose_white-center.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       utl20-ahubcon.1-0 ; 
       utl20-armour.1-0 ; 
       utl20-doccon.1-0 ; 
       utl20-engine.2-0 ; 
       utl20-fhubcon.3-0 ; 
       utl20-fuselg1.1-0 ; 
       utl20-fuselg2.1-0 ; 
       utl20-ldoccon.1-0 ; 
       utl20-lights1.1-0 ; 
       utl20-lights2.2-0 ; 
       utl20-rdoccon.1-0 ; 
       utl20-SSa0.1-0 ; 
       utl20-SSb0.1-0 ; 
       utl20-SSb1.1-0 ; 
       utl20-SSb2.1-0 ; 
       utl20-SSb3.1-0 ; 
       utl20-SSb4.1-0 ; 
       utl20-SSb5.1-0 ; 
       utl20-SSb6.1-0 ; 
       utl20-SSm1.1-0 ; 
       utl20-SSm2.1-0 ; 
       utl20-SSm3.1-0 ; 
       utl20-SSm4.1-0 ; 
       utl20-SSr0.1-0 ; 
       utl20-SSr1.1-0 ; 
       utl20-SSr2.1-0 ; 
       utl20-SSr3.1-0 ; 
       utl20-SSr4.1-0 ; 
       utl20-tractr1.1-0 ; 
       utl20-tractr2.1-0 ; 
       utl20-tractr3.1-0 ; 
       utl20-utl20.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-hutt_palace.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       hutt_palace-t2d10.1-0 ; 
       hutt_palace-t2d11.1-0 ; 
       hutt_palace-t2d12.1-0 ; 
       hutt_palace-t2d13.1-0 ; 
       hutt_palace-t2d14.1-0 ; 
       hutt_palace-t2d15.1-0 ; 
       hutt_palace-t2d16.1-0 ; 
       hutt_palace-t2d17.1-0 ; 
       hutt_palace-t2d18.1-0 ; 
       hutt_palace-t2d19.1-0 ; 
       hutt_palace-t2d20.1-0 ; 
       hutt_palace-t2d21.1-0 ; 
       hutt_palace-t2d22.1-0 ; 
       hutt_palace-t2d23.1-0 ; 
       hutt_palace-t2d24.1-0 ; 
       hutt_palace-t2d25.1-0 ; 
       hutt_palace-t2d26.1-0 ; 
       hutt_palace-t2d27.1-0 ; 
       hutt_palace-t2d28.1-0 ; 
       hutt_palace-t2d29.1-0 ; 
       hutt_palace-t2d3.1-0 ; 
       hutt_palace-t2d31.1-0 ; 
       hutt_palace-t2d32.1-0 ; 
       hutt_palace-t2d35.1-0 ; 
       hutt_palace-t2d36.1-0 ; 
       hutt_palace-t2d37.1-0 ; 
       hutt_palace-t2d38.1-0 ; 
       hutt_palace-t2d39.1-0 ; 
       hutt_palace-t2d4.1-0 ; 
       hutt_palace-t2d40.1-0 ; 
       hutt_palace-t2d41.1-0 ; 
       hutt_palace-t2d42.1-0 ; 
       hutt_palace-t2d43.1-0 ; 
       hutt_palace-t2d44.1-0 ; 
       hutt_palace-t2d45.1-0 ; 
       hutt_palace-t2d46.1-0 ; 
       hutt_palace-t2d47.1-0 ; 
       hutt_palace-t2d48.1-0 ; 
       hutt_palace-t2d49.1-0 ; 
       hutt_palace-t2d5.1-0 ; 
       hutt_palace-t2d50.1-0 ; 
       hutt_palace-t2d51.1-0 ; 
       hutt_palace-t2d52.1-0 ; 
       hutt_palace-t2d53.1-0 ; 
       hutt_palace-t2d54.1-0 ; 
       hutt_palace-t2d55.1-0 ; 
       hutt_palace-t2d56.1-0 ; 
       hutt_palace-t2d57.1-0 ; 
       hutt_palace-t2d6.1-0 ; 
       hutt_palace-t2d7.1-0 ; 
       hutt_palace-t2d8.1-0 ; 
       hutt_palace-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 6 110 ; 
       12 3 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 12 110 ; 
       16 12 110 ; 
       17 23 110 ; 
       18 23 110 ; 
       19 11 110 ; 
       20 11 110 ; 
       21 11 110 ; 
       22 11 110 ; 
       23 0 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       0 5 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 6 110 ; 
       4 5 110 ; 
       5 31 110 ; 
       6 5 110 ; 
       7 4 110 ; 
       8 5 110 ; 
       9 8 110 ; 
       10 4 110 ; 
       28 5 110 ; 
       29 28 110 ; 
       30 29 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 61 300 ; 
       14 66 300 ; 
       15 67 300 ; 
       16 68 300 ; 
       17 73 300 ; 
       18 74 300 ; 
       19 62 300 ; 
       20 63 300 ; 
       21 64 300 ; 
       22 65 300 ; 
       24 69 300 ; 
       25 70 300 ; 
       26 71 300 ; 
       27 72 300 ; 
       0 27 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       0 43 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 24 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 55 300 ; 
       3 11 300 ; 
       3 22 300 ; 
       3 30 300 ; 
       3 41 300 ; 
       3 52 300 ; 
       3 58 300 ; 
       3 59 300 ; 
       3 60 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       5 0 300 ; 
       5 50 300 ; 
       5 51 300 ; 
       5 53 300 ; 
       5 54 300 ; 
       5 56 300 ; 
       5 57 300 ; 
       6 44 300 ; 
       6 45 300 ; 
       6 46 300 ; 
       6 47 300 ; 
       6 48 300 ; 
       6 49 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 23 300 ; 
       9 18 300 ; 
       28 42 300 ; 
       29 38 300 ; 
       29 39 300 ; 
       29 40 300 ; 
       30 34 300 ; 
       30 35 300 ; 
       30 36 300 ; 
       30 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       31 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 20 401 ; 
       23 18 401 ; 
       24 19 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 28 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       35 27 401 ; 
       36 29 401 ; 
       37 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 39 401 ; 
       42 33 401 ; 
       43 34 401 ; 
       45 35 401 ; 
       46 36 401 ; 
       47 37 401 ; 
       48 38 401 ; 
       49 40 401 ; 
       50 41 401 ; 
       51 42 401 ; 
       52 48 401 ; 
       53 43 401 ; 
       54 44 401 ; 
       55 45 401 ; 
       56 46 401 ; 
       57 47 401 ; 
       58 49 401 ; 
       59 50 401 ; 
       60 51 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 20 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 15 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 35 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       23 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       24 SCHEM 5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 MPRFLG 0 ; 
       6 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 45 -4 0 MPRFLG 0 ; 
       29 SCHEM 45 -6 0 MPRFLG 0 ; 
       30 SCHEM 45 -8 0 MPRFLG 0 ; 
       31 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
