SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       youmap-cam_int1.70-0 ROOT ; 
       youmap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 65     
       add_frames-default13.6-0 ; 
       add_frames-mat2.6-0 ; 
       add_frames-mat23.6-0 ; 
       add_frames-mat24.6-0 ; 
       add_frames-mat25.6-0 ; 
       add_frames-mat26.6-0 ; 
       add_frames-mat27.6-0 ; 
       add_frames-mat28.6-0 ; 
       add_frames-mat29.6-0 ; 
       add_frames-mat30.6-0 ; 
       add_frames-mat31.6-0 ; 
       add_frames-mat33.6-0 ; 
       add_frames-mat34.6-0 ; 
       add_frames-mat37.6-0 ; 
       add_frames-mat38.6-0 ; 
       add_frames-mat39.6-0 ; 
       add_frames-mat40.6-0 ; 
       add_frames-mat41.6-0 ; 
       add_frames-mat42.6-0 ; 
       add_frames-mat43.6-0 ; 
       add_frames-mat44.6-0 ; 
       add_frames-mat45.6-0 ; 
       add_frames-mat46.6-0 ; 
       add_frames-mat47.6-0 ; 
       add_frames-mat48.6-0 ; 
       add_frames-mat49.6-0 ; 
       add_frames-mat50.6-0 ; 
       add_frames-mat51.6-0 ; 
       add_frames-mat52.6-0 ; 
       add_frames-mat53.6-0 ; 
       add_frames-mat54.6-0 ; 
       add_frames-mat55.6-0 ; 
       add_frames-mat56.6-0 ; 
       add_frames-mat57.6-0 ; 
       add_frames-mat58.6-0 ; 
       add_frames-mat59.6-0 ; 
       add_frames-mat60.6-0 ; 
       add_frames-mat61.6-0 ; 
       add_frames-mat62.6-0 ; 
       add_frames-mat63.6-0 ; 
       add_frames-mat64.6-0 ; 
       add_frames-mat66.6-0 ; 
       add_frames-mat67.6-0 ; 
       add_frames-nose_white-center.1-0.6-0 ; 
       add_frames-nose_white-center.1-0_1.6-0 ; 
       add_frames-nose_white-center.1-0_2.6-0 ; 
       add_frames-nose_white-center.1-0_3.6-0 ; 
       add_frames-nose_white-center.1-0_4.6-0 ; 
       add_frames-nose_white-center.1-2.6-0 ; 
       add_frames-nose_white-center.1-3.6-0 ; 
       add_frames-nose_white-center.1-4.6-0 ; 
       add_frames-nose_white-center.1-5.6-0 ; 
       add_frames-nose_white-center.1-6.6-0 ; 
       add_frames-nose_white-center.1-7.6-0 ; 
       add_frames-nose_white-center.1-8.6-0 ; 
       add_frames-nose_white-center.1-9.6-0 ; 
       move_nulls_out-mat68.1-0 ; 
       move_nulls_out-mat69.1-0 ; 
       move_nulls_out-mat70.1-0 ; 
       move_nulls_out-mat71.1-0 ; 
       move_nulls_out-mat72.1-0 ; 
       move_nulls_out-mat73.1-0 ; 
       move_nulls_out-mat74.1-0 ; 
       move_nulls_out-mat75.1-0 ; 
       move_nulls_out-nose_white-center.1-10.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       move_nulls_out-ahubcon.1-0 ; 
       move_nulls_out-armour.1-0 ; 
       move_nulls_out-bay1.1-0 ; 
       move_nulls_out-bay2.1-0 ; 
       move_nulls_out-doccon.1-0 ; 
       move_nulls_out-engine.2-0 ; 
       move_nulls_out-fhubcon.3-0 ; 
       move_nulls_out-fuselg1.1-0 ; 
       move_nulls_out-fuselg2.1-0 ; 
       move_nulls_out-garage1A.1-0 ; 
       move_nulls_out-garage1B.1-0 ; 
       move_nulls_out-garage1C.1-0 ; 
       move_nulls_out-garage1D.1-0 ; 
       move_nulls_out-garage1E.1-0 ; 
       move_nulls_out-garage2A.1-0 ; 
       move_nulls_out-garage2B.1-0 ; 
       move_nulls_out-garage2C.1-0 ; 
       move_nulls_out-garage2D.1-0 ; 
       move_nulls_out-garage2E.1-0 ; 
       move_nulls_out-LAUNCH_TUBE.1-0 ; 
       move_nulls_out-LAUNCH_TUBE_1.1-0 ; 
       move_nulls_out-launch1.1-0 ; 
       move_nulls_out-launch2.1-0 ; 
       move_nulls_out-ldoccon.1-0 ; 
       move_nulls_out-lights1.1-0 ; 
       move_nulls_out-lights2.2-0 ; 
       move_nulls_out-null1.1-0 ; 
       move_nulls_out-null2.1-0 ; 
       move_nulls_out-rdoccon.1-0 ; 
       move_nulls_out-skin2.2-0 ; 
       move_nulls_out-skin3.1-0 ROOT ; 
       move_nulls_out-skin4.1-0 ROOT ; 
       move_nulls_out-SSa0.1-0 ; 
       move_nulls_out-SSb0.1-0 ; 
       move_nulls_out-SSb1.1-0 ; 
       move_nulls_out-SSb3.1-0 ; 
       move_nulls_out-SSb4.1-0 ; 
       move_nulls_out-SSb5.1-0 ; 
       move_nulls_out-SSb6.1-0 ; 
       move_nulls_out-SSm1.1-0 ; 
       move_nulls_out-SSm2.1-0 ; 
       move_nulls_out-SSm3.1-0 ; 
       move_nulls_out-SSm4.1-0 ; 
       move_nulls_out-SSr0.1-0 ; 
       move_nulls_out-SSr1.1-0 ; 
       move_nulls_out-SSr10.1-0 ; 
       move_nulls_out-SSr11.1-0 ; 
       move_nulls_out-SSr12.1-0 ; 
       move_nulls_out-SSr2.1-0 ; 
       move_nulls_out-SSr3.1-0 ; 
       move_nulls_out-SSr4.1-0 ; 
       move_nulls_out-SSr5.1-0 ; 
       move_nulls_out-SSr6.1-0 ; 
       move_nulls_out-SSr7.1-0 ; 
       move_nulls_out-SSr8.1-0 ; 
       move_nulls_out-SSr9.1-0 ; 
       move_nulls_out-tractr1.1-0 ; 
       move_nulls_out-tractr2.1-0 ; 
       move_nulls_out-tractr3.1-0 ; 
       move_nulls_out-utl20.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-move_nulls_out.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       add_frames-rendermap2.12-0 ; 
       add_frames-t2d23.7-0 ; 
       add_frames-t2d24.7-0 ; 
       add_frames-t2d25.7-0 ; 
       add_frames-t2d26.7-0 ; 
       add_frames-t2d27.7-0 ; 
       add_frames-t2d28.7-0 ; 
       add_frames-t2d29.7-0 ; 
       add_frames-t2d31.7-0 ; 
       add_frames-t2d32.7-0 ; 
       add_frames-t2d35.7-0 ; 
       add_frames-t2d36.7-0 ; 
       add_frames-t2d37.7-0 ; 
       add_frames-t2d38.7-0 ; 
       add_frames-t2d39.7-0 ; 
       add_frames-t2d40.7-0 ; 
       add_frames-t2d41.7-0 ; 
       add_frames-t2d42.7-0 ; 
       add_frames-t2d43.7-0 ; 
       add_frames-t2d44.7-0 ; 
       add_frames-t2d45.7-0 ; 
       add_frames-t2d46.7-0 ; 
       add_frames-t2d47.7-0 ; 
       add_frames-t2d48.7-0 ; 
       add_frames-t2d49.7-0 ; 
       add_frames-t2d50.7-0 ; 
       add_frames-t2d51.8-0 ; 
       add_frames-t2d52.8-0 ; 
       add_frames-t2d53.8-0 ; 
       add_frames-t2d54.8-0 ; 
       add_frames-t2d55.7-0 ; 
       add_frames-t2d56.8-0 ; 
       add_frames-t2d57.8-0 ; 
       add_frames-t2d58.7-0 ; 
       add_frames-t2d59.12-0 ; 
       move_nulls_out-rendermap3.1-0 ; 
       move_nulls_out-rendermap4.1-0 ; 
       move_nulls_out-t2d60.1-0 ; 
       move_nulls_out-t2d61.1-0 ; 
       move_nulls_out-t2d62.1-0 ; 
       move_nulls_out-t2d63.1-0 ; 
       move_nulls_out-t2d64.1-0 ; 
       move_nulls_out-t2d65.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       51 26 110 ; 
       26 59 110 ; 
       52 26 110 ; 
       53 26 110 ; 
       54 26 110 ; 
       27 59 110 ; 
       55 27 110 ; 
       45 27 110 ; 
       46 27 110 ; 
       47 27 110 ; 
       0 7 110 ; 
       1 7 110 ; 
       2 59 110 ; 
       3 59 110 ; 
       4 1 110 ; 
       5 8 110 ; 
       6 7 110 ; 
       7 59 110 ; 
       8 7 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 3 110 ; 
       18 3 110 ; 
       19 59 110 ; 
       20 59 110 ; 
       21 59 110 ; 
       22 59 110 ; 
       23 6 110 ; 
       24 7 110 ; 
       25 24 110 ; 
       28 6 110 ; 
       29 59 110 ; 
       32 8 110 ; 
       33 5 110 ; 
       34 33 110 ; 
       35 33 110 ; 
       36 33 110 ; 
       37 43 110 ; 
       38 43 110 ; 
       39 32 110 ; 
       40 32 110 ; 
       41 32 110 ; 
       42 32 110 ; 
       43 0 110 ; 
       44 43 110 ; 
       48 43 110 ; 
       49 43 110 ; 
       50 43 110 ; 
       56 7 110 ; 
       57 56 110 ; 
       58 57 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       51 64 300 ; 
       52 64 300 ; 
       53 64 300 ; 
       54 64 300 ; 
       55 64 300 ; 
       45 64 300 ; 
       46 64 300 ; 
       47 64 300 ; 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 27 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 38 300 ; 
       5 1 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       7 0 300 ; 
       7 34 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       19 56 300 ; 
       19 57 300 ; 
       20 58 300 ; 
       20 59 300 ; 
       24 6 300 ; 
       24 7 300 ; 
       24 8 300 ; 
       24 9 300 ; 
       25 5 300 ; 
       29 41 300 ; 
       29 42 300 ; 
       30 60 300 ; 
       30 61 300 ; 
       31 62 300 ; 
       31 63 300 ; 
       34 43 300 ; 
       35 48 300 ; 
       36 49 300 ; 
       37 54 300 ; 
       38 55 300 ; 
       39 44 300 ; 
       40 45 300 ; 
       41 46 300 ; 
       42 47 300 ; 
       44 50 300 ; 
       48 51 300 ; 
       49 52 300 ; 
       50 53 300 ; 
       56 26 300 ; 
       57 23 300 ; 
       57 24 300 ; 
       57 25 300 ; 
       58 19 300 ; 
       58 20 300 ; 
       58 21 300 ; 
       58 22 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 33 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 0 401 ; 
       42 34 401 ; 
       56 37 401 ; 
       57 38 401 ; 
       58 39 401 ; 
       59 40 401 ; 
       60 35 401 ; 
       61 41 401 ; 
       62 36 401 ; 
       63 42 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       51 SCHEM 85 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 88.75 -2 0 MPRFLG 0 ; 
       52 SCHEM 87.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 90 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 92.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 98.75 -2 0 MPRFLG 0 ; 
       55 SCHEM 95 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 97.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 100 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 102.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 45 -4 0 MPRFLG 0 ; 
       2 SCHEM 55 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 67.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 45 -6 0 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 MPRFLG 0 ; 
       6 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 MPRFLG 0 ; 
       9 SCHEM 50 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 52.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 55 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 60 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 65 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 70 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 75 -2 0 MPRFLG 0 ; 
       20 SCHEM 77.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 80 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 40 -4 0 MPRFLG 0 ; 
       25 SCHEM 40 -6 0 MPRFLG 0 ; 
       28 SCHEM 35 -6 0 MPRFLG 0 ; 
       29 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       30 SCHEM 105 0 0 DISPLAY 0 0 SRT 1.3062 1.3062 1.3062 3.308722e-022 -1.57853e-024 0 -13.20436 0.9613662 -21.00753 MPRFLG 0 ; 
       31 SCHEM 107.5 0 0 DISPLAY 0 0 SRT 1.3062 1.3062 1.3062 3.308722e-022 3.141593 0 19.0836 0.9613662 -21.00753 MPRFLG 0 ; 
       32 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       33 SCHEM 20 -8 0 MPRFLG 0 ; 
       34 SCHEM 17.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 20 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 23.75 -10 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 15 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 2.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 32.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       40 SCHEM 25 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       41 SCHEM 27.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       42 SCHEM 30 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       43 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       44 SCHEM 5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 7.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       57 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       58 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       59 SCHEM 52.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 22.75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 104 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 104 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 106.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 106.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 84 -6 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 104 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 106.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 104 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 106.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
