SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl20-utl20.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rendermap-cam_int1.1-0 ROOT ; 
       rendermap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 76     
       rendermap-default13.1-0 ; 
       rendermap-mat10.1-0 ; 
       rendermap-mat11.1-0 ; 
       rendermap-mat12.1-0 ; 
       rendermap-mat13.1-0 ; 
       rendermap-mat14.1-0 ; 
       rendermap-mat15.1-0 ; 
       rendermap-mat16.1-0 ; 
       rendermap-mat17.1-0 ; 
       rendermap-mat18.1-0 ; 
       rendermap-mat19.1-0 ; 
       rendermap-mat2.1-0 ; 
       rendermap-mat20.1-0 ; 
       rendermap-mat21.1-0 ; 
       rendermap-mat22.1-0 ; 
       rendermap-mat23.1-0 ; 
       rendermap-mat24.1-0 ; 
       rendermap-mat25.1-0 ; 
       rendermap-mat26.1-0 ; 
       rendermap-mat27.1-0 ; 
       rendermap-mat28.1-0 ; 
       rendermap-mat29.1-0 ; 
       rendermap-mat3.1-0 ; 
       rendermap-mat30.1-0 ; 
       rendermap-mat31.1-0 ; 
       rendermap-mat33.1-0 ; 
       rendermap-mat34.1-0 ; 
       rendermap-mat37.1-0 ; 
       rendermap-mat38.1-0 ; 
       rendermap-mat39.1-0 ; 
       rendermap-mat4.1-0 ; 
       rendermap-mat40.1-0 ; 
       rendermap-mat41.1-0 ; 
       rendermap-mat42.1-0 ; 
       rendermap-mat43.1-0 ; 
       rendermap-mat44.1-0 ; 
       rendermap-mat45.1-0 ; 
       rendermap-mat46.1-0 ; 
       rendermap-mat47.1-0 ; 
       rendermap-mat48.1-0 ; 
       rendermap-mat49.1-0 ; 
       rendermap-mat5.1-0 ; 
       rendermap-mat50.1-0 ; 
       rendermap-mat51.1-0 ; 
       rendermap-mat52.1-0 ; 
       rendermap-mat53.1-0 ; 
       rendermap-mat54.1-0 ; 
       rendermap-mat55.1-0 ; 
       rendermap-mat56.1-0 ; 
       rendermap-mat57.1-0 ; 
       rendermap-mat58.1-0 ; 
       rendermap-mat59.1-0 ; 
       rendermap-mat6.1-0 ; 
       rendermap-mat60.1-0 ; 
       rendermap-mat61.1-0 ; 
       rendermap-mat62.1-0 ; 
       rendermap-mat63.1-0 ; 
       rendermap-mat64.1-0 ; 
       rendermap-mat65.1-0 ; 
       rendermap-mat7.1-0 ; 
       rendermap-mat8.1-0 ; 
       rendermap-mat9.1-0 ; 
       rendermap-nose_white-center.1-0.1-0 ; 
       rendermap-nose_white-center.1-0_1.1-0 ; 
       rendermap-nose_white-center.1-0_2.1-0 ; 
       rendermap-nose_white-center.1-0_3.1-0 ; 
       rendermap-nose_white-center.1-0_4.1-0 ; 
       rendermap-nose_white-center.1-1.1-0 ; 
       rendermap-nose_white-center.1-2.1-0 ; 
       rendermap-nose_white-center.1-3.1-0 ; 
       rendermap-nose_white-center.1-4.1-0 ; 
       rendermap-nose_white-center.1-5.1-0 ; 
       rendermap-nose_white-center.1-6.1-0 ; 
       rendermap-nose_white-center.1-7.1-0 ; 
       rendermap-nose_white-center.1-8.1-0 ; 
       rendermap-nose_white-center.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       rendermap-circle1.1-0 ; 
       rendermap-circle10.1-0 ; 
       rendermap-circle13.1-0 ; 
       rendermap-circle15.1-0 ; 
       rendermap-circle16.1-0 ; 
       rendermap-circle17.1-0 ; 
       rendermap-circle3.1-0 ; 
       rendermap-circle4.1-0 ; 
       rendermap-circle6.1-0 ; 
       rendermap-circle8.1-0 ; 
       rendermap-null1.1-0 ROOT ; 
       rendermap-skin2.1-0 ROOT ; 
       rendermap-spline1.1-0 ROOT ; 
       rendermap-spline2.1-0 ROOT ; 
       utl20-ahubcon.1-0 ; 
       utl20-armour.1-0 ; 
       utl20-doccon.1-0 ; 
       utl20-engine.2-0 ; 
       utl20-fhubcon.3-0 ; 
       utl20-fuselg1.1-0 ; 
       utl20-fuselg2.1-0 ; 
       utl20-ldoccon.1-0 ; 
       utl20-lights1.1-0 ; 
       utl20-lights2.2-0 ; 
       utl20-rdoccon.1-0 ; 
       utl20-SSa0.1-0 ; 
       utl20-SSb0.1-0 ; 
       utl20-SSb1.1-0 ; 
       utl20-SSb2.1-0 ; 
       utl20-SSb3.1-0 ; 
       utl20-SSb4.1-0 ; 
       utl20-SSb5.1-0 ; 
       utl20-SSb6.1-0 ; 
       utl20-SSm1.1-0 ; 
       utl20-SSm2.1-0 ; 
       utl20-SSm3.1-0 ; 
       utl20-SSm4.1-0 ; 
       utl20-SSr0.1-0 ; 
       utl20-SSr1.1-0 ; 
       utl20-SSr2.1-0 ; 
       utl20-SSr3.1-0 ; 
       utl20-SSr4.1-0 ; 
       utl20-tractr1.1-0 ; 
       utl20-tractr2.1-0 ; 
       utl20-tractr3.1-0 ; 
       utl20-utl20.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/MoonMap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/cap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/rendermap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-rendermap.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       rendermap-3DRock_Lava1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 55     
       rendermap-cap.1-0 ; 
       rendermap-main.1-0 ; 
       rendermap-rendermap1.1-0 ; 
       rendermap-t2d10.1-0 ; 
       rendermap-t2d11.1-0 ; 
       rendermap-t2d12.1-0 ; 
       rendermap-t2d13.1-0 ; 
       rendermap-t2d14.1-0 ; 
       rendermap-t2d15.1-0 ; 
       rendermap-t2d16.1-0 ; 
       rendermap-t2d17.1-0 ; 
       rendermap-t2d18.1-0 ; 
       rendermap-t2d19.1-0 ; 
       rendermap-t2d20.1-0 ; 
       rendermap-t2d21.1-0 ; 
       rendermap-t2d22.1-0 ; 
       rendermap-t2d23.1-0 ; 
       rendermap-t2d24.1-0 ; 
       rendermap-t2d25.1-0 ; 
       rendermap-t2d26.1-0 ; 
       rendermap-t2d27.1-0 ; 
       rendermap-t2d28.1-0 ; 
       rendermap-t2d29.1-0 ; 
       rendermap-t2d3.1-0 ; 
       rendermap-t2d31.1-0 ; 
       rendermap-t2d32.1-0 ; 
       rendermap-t2d35.1-0 ; 
       rendermap-t2d36.1-0 ; 
       rendermap-t2d37.1-0 ; 
       rendermap-t2d38.1-0 ; 
       rendermap-t2d39.1-0 ; 
       rendermap-t2d4.1-0 ; 
       rendermap-t2d40.1-0 ; 
       rendermap-t2d41.1-0 ; 
       rendermap-t2d42.1-0 ; 
       rendermap-t2d43.1-0 ; 
       rendermap-t2d44.1-0 ; 
       rendermap-t2d45.1-0 ; 
       rendermap-t2d46.1-0 ; 
       rendermap-t2d47.1-0 ; 
       rendermap-t2d48.1-0 ; 
       rendermap-t2d49.1-0 ; 
       rendermap-t2d5.1-0 ; 
       rendermap-t2d50.1-0 ; 
       rendermap-t2d51.1-0 ; 
       rendermap-t2d52.1-0 ; 
       rendermap-t2d53.1-0 ; 
       rendermap-t2d54.1-0 ; 
       rendermap-t2d55.1-0 ; 
       rendermap-t2d56.1-0 ; 
       rendermap-t2d57.1-0 ; 
       rendermap-t2d6.1-0 ; 
       rendermap-t2d7.1-0 ; 
       rendermap-t2d8.1-0 ; 
       rendermap-t2d9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       rendermap-t3d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 10 110 ; 
       1 10 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       14 19 110 ; 
       15 19 110 ; 
       16 15 110 ; 
       17 20 110 ; 
       18 19 110 ; 
       19 45 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 19 110 ; 
       23 22 110 ; 
       24 18 110 ; 
       25 20 110 ; 
       26 17 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       30 26 110 ; 
       31 37 110 ; 
       32 37 110 ; 
       33 25 110 ; 
       34 25 110 ; 
       35 25 110 ; 
       36 25 110 ; 
       37 14 110 ; 
       38 37 110 ; 
       39 37 110 ; 
       40 37 110 ; 
       41 37 110 ; 
       42 19 110 ; 
       43 42 110 ; 
       44 43 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       11 58 300 ; 
       14 27 300 ; 
       14 28 300 ; 
       14 29 300 ; 
       14 43 300 ; 
       15 15 300 ; 
       15 16 300 ; 
       15 17 300 ; 
       15 24 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 55 300 ; 
       17 11 300 ; 
       17 22 300 ; 
       17 30 300 ; 
       17 41 300 ; 
       17 52 300 ; 
       17 59 300 ; 
       17 60 300 ; 
       17 61 300 ; 
       17 1 300 ; 
       17 2 300 ; 
       17 3 300 ; 
       17 4 300 ; 
       17 5 300 ; 
       17 6 300 ; 
       17 7 300 ; 
       17 8 300 ; 
       17 9 300 ; 
       17 10 300 ; 
       17 12 300 ; 
       17 13 300 ; 
       17 14 300 ; 
       18 31 300 ; 
       18 32 300 ; 
       18 33 300 ; 
       19 0 300 ; 
       19 50 300 ; 
       19 51 300 ; 
       19 53 300 ; 
       19 54 300 ; 
       19 56 300 ; 
       19 57 300 ; 
       20 44 300 ; 
       20 45 300 ; 
       20 46 300 ; 
       20 47 300 ; 
       20 48 300 ; 
       20 49 300 ; 
       22 19 300 ; 
       22 20 300 ; 
       22 21 300 ; 
       22 23 300 ; 
       23 18 300 ; 
       27 62 300 ; 
       28 67 300 ; 
       29 68 300 ; 
       30 69 300 ; 
       31 74 300 ; 
       32 75 300 ; 
       33 63 300 ; 
       34 64 300 ; 
       35 65 300 ; 
       36 66 300 ; 
       38 70 300 ; 
       39 71 300 ; 
       40 72 300 ; 
       41 73 300 ; 
       42 42 300 ; 
       43 38 300 ; 
       43 39 300 ; 
       43 40 300 ; 
       44 34 300 ; 
       44 35 300 ; 
       44 36 300 ; 
       44 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       11 1 400 ; 
       11 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       11 0 500 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       45 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 3 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 7 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       8 10 401 ; 
       9 11 401 ; 
       10 12 401 ; 
       12 13 401 ; 
       13 14 401 ; 
       14 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 23 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       28 26 401 ; 
       29 27 401 ; 
       30 31 401 ; 
       32 28 401 ; 
       33 29 401 ; 
       35 30 401 ; 
       36 32 401 ; 
       37 33 401 ; 
       39 34 401 ; 
       40 35 401 ; 
       41 42 401 ; 
       42 36 401 ; 
       43 37 401 ; 
       45 38 401 ; 
       46 39 401 ; 
       47 40 401 ; 
       48 41 401 ; 
       49 43 401 ; 
       50 44 401 ; 
       51 45 401 ; 
       52 51 401 ; 
       53 46 401 ; 
       54 47 401 ; 
       55 48 401 ; 
       56 49 401 ; 
       57 50 401 ; 
       58 2 401 ; 
       59 52 401 ; 
       60 53 401 ; 
       61 54 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 11 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 55 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 67.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 72.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 77.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 80 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 57.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 60 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 62.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 65 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 67.5 0 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 82.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 1.266336 -19.27997 -5.318614 MPRFLG 0 ; 
       12 SCHEM 50 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 52.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -23.4193 0 MPRFLG 0 ; 
       14 SCHEM 8.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 47.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 21.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 38.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 26.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 40 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 42.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 37.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 31.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 21.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 17.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 22.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 20 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 25 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 15 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 2.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 35 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 30 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 32.5 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 8.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 7.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 10 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 12.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 45 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 45 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 45 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 44 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 44 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 44 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 41.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 41.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 41.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 46.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 46.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 46.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 46.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 85 0 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 16.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 34 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 26.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 29 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 31.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 21.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 19 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 24 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 6.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 9 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 1.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 41.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 44 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 44 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 41.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 41.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 44 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 44 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 44 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 46.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 46.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 46.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 90 0 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 85 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 87.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 87.5 0 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
