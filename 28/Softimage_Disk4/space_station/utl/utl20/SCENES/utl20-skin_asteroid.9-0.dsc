SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl20-utl20.11-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       skin_asteroid-cam_int1.9-0 ROOT ; 
       skin_asteroid-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 76     
       skin_asteroid-default13.1-0 ; 
       skin_asteroid-mat10.1-0 ; 
       skin_asteroid-mat11.1-0 ; 
       skin_asteroid-mat12.1-0 ; 
       skin_asteroid-mat13.1-0 ; 
       skin_asteroid-mat14.1-0 ; 
       skin_asteroid-mat15.1-0 ; 
       skin_asteroid-mat16.1-0 ; 
       skin_asteroid-mat17.1-0 ; 
       skin_asteroid-mat18.1-0 ; 
       skin_asteroid-mat19.1-0 ; 
       skin_asteroid-mat2.1-0 ; 
       skin_asteroid-mat20.1-0 ; 
       skin_asteroid-mat21.1-0 ; 
       skin_asteroid-mat22.1-0 ; 
       skin_asteroid-mat23.1-0 ; 
       skin_asteroid-mat24.1-0 ; 
       skin_asteroid-mat25.1-0 ; 
       skin_asteroid-mat26.1-0 ; 
       skin_asteroid-mat27.1-0 ; 
       skin_asteroid-mat28.1-0 ; 
       skin_asteroid-mat29.1-0 ; 
       skin_asteroid-mat3.1-0 ; 
       skin_asteroid-mat30.1-0 ; 
       skin_asteroid-mat31.1-0 ; 
       skin_asteroid-mat33.1-0 ; 
       skin_asteroid-mat34.1-0 ; 
       skin_asteroid-mat37.1-0 ; 
       skin_asteroid-mat38.1-0 ; 
       skin_asteroid-mat39.1-0 ; 
       skin_asteroid-mat4.1-0 ; 
       skin_asteroid-mat40.1-0 ; 
       skin_asteroid-mat41.1-0 ; 
       skin_asteroid-mat42.1-0 ; 
       skin_asteroid-mat43.1-0 ; 
       skin_asteroid-mat44.1-0 ; 
       skin_asteroid-mat45.1-0 ; 
       skin_asteroid-mat46.1-0 ; 
       skin_asteroid-mat47.1-0 ; 
       skin_asteroid-mat48.1-0 ; 
       skin_asteroid-mat49.1-0 ; 
       skin_asteroid-mat5.1-0 ; 
       skin_asteroid-mat50.1-0 ; 
       skin_asteroid-mat51.1-0 ; 
       skin_asteroid-mat52.1-0 ; 
       skin_asteroid-mat53.1-0 ; 
       skin_asteroid-mat54.1-0 ; 
       skin_asteroid-mat55.1-0 ; 
       skin_asteroid-mat56.1-0 ; 
       skin_asteroid-mat57.1-0 ; 
       skin_asteroid-mat58.1-0 ; 
       skin_asteroid-mat59.1-0 ; 
       skin_asteroid-mat6.1-0 ; 
       skin_asteroid-mat60.1-0 ; 
       skin_asteroid-mat61.1-0 ; 
       skin_asteroid-mat62.1-0 ; 
       skin_asteroid-mat63.1-0 ; 
       skin_asteroid-mat64.1-0 ; 
       skin_asteroid-mat65.4-0 ; 
       skin_asteroid-mat7.1-0 ; 
       skin_asteroid-mat8.1-0 ; 
       skin_asteroid-mat9.1-0 ; 
       skin_asteroid-nose_white-center.1-0.1-0 ; 
       skin_asteroid-nose_white-center.1-0_1.1-0 ; 
       skin_asteroid-nose_white-center.1-0_2.1-0 ; 
       skin_asteroid-nose_white-center.1-0_3.1-0 ; 
       skin_asteroid-nose_white-center.1-0_4.1-0 ; 
       skin_asteroid-nose_white-center.1-1.1-0 ; 
       skin_asteroid-nose_white-center.1-2.1-0 ; 
       skin_asteroid-nose_white-center.1-3.1-0 ; 
       skin_asteroid-nose_white-center.1-4.1-0 ; 
       skin_asteroid-nose_white-center.1-5.1-0 ; 
       skin_asteroid-nose_white-center.1-6.1-0 ; 
       skin_asteroid-nose_white-center.1-7.1-0 ; 
       skin_asteroid-nose_white-center.1-8.1-0 ; 
       skin_asteroid-nose_white-center.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       skin_asteroid-circle1.1-0 ; 
       skin_asteroid-circle10.1-0 ; 
       skin_asteroid-circle13.1-0 ; 
       skin_asteroid-circle15.1-0 ; 
       skin_asteroid-circle16.1-0 ; 
       skin_asteroid-circle17.1-0 ; 
       skin_asteroid-circle3.1-0 ; 
       skin_asteroid-circle4.1-0 ; 
       skin_asteroid-circle6.1-0 ; 
       skin_asteroid-circle8.1-0 ; 
       skin_asteroid-null1.3-0 ROOT ; 
       skin_asteroid-skin2.6-0 ROOT ; 
       skin_asteroid-spline1.3-0 ROOT ; 
       skin_asteroid-spline2.2-0 ROOT ; 
       utl20-ahubcon.1-0 ; 
       utl20-armour.1-0 ; 
       utl20-doccon.1-0 ; 
       utl20-engine.2-0 ; 
       utl20-fhubcon.3-0 ; 
       utl20-fuselg1.1-0 ; 
       utl20-fuselg2.1-0 ; 
       utl20-ldoccon.1-0 ; 
       utl20-lights1.1-0 ; 
       utl20-lights2.2-0 ; 
       utl20-rdoccon.1-0 ; 
       utl20-SSa0.1-0 ; 
       utl20-SSb0.1-0 ; 
       utl20-SSb1.1-0 ; 
       utl20-SSb2.1-0 ; 
       utl20-SSb3.1-0 ; 
       utl20-SSb4.1-0 ; 
       utl20-SSb5.1-0 ; 
       utl20-SSb6.1-0 ; 
       utl20-SSm1.1-0 ; 
       utl20-SSm2.1-0 ; 
       utl20-SSm3.1-0 ; 
       utl20-SSm4.1-0 ; 
       utl20-SSr0.1-0 ; 
       utl20-SSr1.1-0 ; 
       utl20-SSr2.1-0 ; 
       utl20-SSr3.1-0 ; 
       utl20-SSr4.1-0 ; 
       utl20-tractr1.1-0 ; 
       utl20-tractr2.1-0 ; 
       utl20-tractr3.1-0 ; 
       utl20-utl20.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/MoonMap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/cap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-skin_asteroid.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       skin_asteroid-3DRock_Lava1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 54     
       skin_asteroid-cap.1-0 ; 
       skin_asteroid-main.1-0 ; 
       skin_asteroid-t2d10.1-0 ; 
       skin_asteroid-t2d11.1-0 ; 
       skin_asteroid-t2d12.1-0 ; 
       skin_asteroid-t2d13.1-0 ; 
       skin_asteroid-t2d14.1-0 ; 
       skin_asteroid-t2d15.1-0 ; 
       skin_asteroid-t2d16.1-0 ; 
       skin_asteroid-t2d17.1-0 ; 
       skin_asteroid-t2d18.1-0 ; 
       skin_asteroid-t2d19.1-0 ; 
       skin_asteroid-t2d20.1-0 ; 
       skin_asteroid-t2d21.1-0 ; 
       skin_asteroid-t2d22.1-0 ; 
       skin_asteroid-t2d23.1-0 ; 
       skin_asteroid-t2d24.1-0 ; 
       skin_asteroid-t2d25.1-0 ; 
       skin_asteroid-t2d26.1-0 ; 
       skin_asteroid-t2d27.1-0 ; 
       skin_asteroid-t2d28.1-0 ; 
       skin_asteroid-t2d29.1-0 ; 
       skin_asteroid-t2d3.1-0 ; 
       skin_asteroid-t2d31.1-0 ; 
       skin_asteroid-t2d32.1-0 ; 
       skin_asteroid-t2d35.1-0 ; 
       skin_asteroid-t2d36.1-0 ; 
       skin_asteroid-t2d37.1-0 ; 
       skin_asteroid-t2d38.1-0 ; 
       skin_asteroid-t2d39.1-0 ; 
       skin_asteroid-t2d4.1-0 ; 
       skin_asteroid-t2d40.1-0 ; 
       skin_asteroid-t2d41.1-0 ; 
       skin_asteroid-t2d42.1-0 ; 
       skin_asteroid-t2d43.1-0 ; 
       skin_asteroid-t2d44.1-0 ; 
       skin_asteroid-t2d45.1-0 ; 
       skin_asteroid-t2d46.1-0 ; 
       skin_asteroid-t2d47.1-0 ; 
       skin_asteroid-t2d48.1-0 ; 
       skin_asteroid-t2d49.1-0 ; 
       skin_asteroid-t2d5.1-0 ; 
       skin_asteroid-t2d50.1-0 ; 
       skin_asteroid-t2d51.1-0 ; 
       skin_asteroid-t2d52.1-0 ; 
       skin_asteroid-t2d53.1-0 ; 
       skin_asteroid-t2d54.1-0 ; 
       skin_asteroid-t2d55.1-0 ; 
       skin_asteroid-t2d56.1-0 ; 
       skin_asteroid-t2d57.1-0 ; 
       skin_asteroid-t2d6.1-0 ; 
       skin_asteroid-t2d7.1-0 ; 
       skin_asteroid-t2d8.1-0 ; 
       skin_asteroid-t2d9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       skin_asteroid-t3d1.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 10 110 ; 
       1 10 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       14 19 110 ; 
       15 19 110 ; 
       16 15 110 ; 
       17 20 110 ; 
       18 19 110 ; 
       19 45 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 19 110 ; 
       23 22 110 ; 
       24 18 110 ; 
       25 20 110 ; 
       26 17 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       30 26 110 ; 
       31 37 110 ; 
       32 37 110 ; 
       33 25 110 ; 
       34 25 110 ; 
       35 25 110 ; 
       36 25 110 ; 
       37 14 110 ; 
       38 37 110 ; 
       39 37 110 ; 
       40 37 110 ; 
       41 37 110 ; 
       42 19 110 ; 
       43 42 110 ; 
       44 43 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       11 58 300 ; 
       14 27 300 ; 
       14 28 300 ; 
       14 29 300 ; 
       14 43 300 ; 
       15 15 300 ; 
       15 16 300 ; 
       15 17 300 ; 
       15 24 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 55 300 ; 
       17 11 300 ; 
       17 22 300 ; 
       17 30 300 ; 
       17 41 300 ; 
       17 52 300 ; 
       17 59 300 ; 
       17 60 300 ; 
       17 61 300 ; 
       17 1 300 ; 
       17 2 300 ; 
       17 3 300 ; 
       17 4 300 ; 
       17 5 300 ; 
       17 6 300 ; 
       17 7 300 ; 
       17 8 300 ; 
       17 9 300 ; 
       17 10 300 ; 
       17 12 300 ; 
       17 13 300 ; 
       17 14 300 ; 
       18 31 300 ; 
       18 32 300 ; 
       18 33 300 ; 
       19 0 300 ; 
       19 50 300 ; 
       19 51 300 ; 
       19 53 300 ; 
       19 54 300 ; 
       19 56 300 ; 
       19 57 300 ; 
       20 44 300 ; 
       20 45 300 ; 
       20 46 300 ; 
       20 47 300 ; 
       20 48 300 ; 
       20 49 300 ; 
       22 19 300 ; 
       22 20 300 ; 
       22 21 300 ; 
       22 23 300 ; 
       23 18 300 ; 
       27 62 300 ; 
       28 67 300 ; 
       29 68 300 ; 
       30 69 300 ; 
       31 74 300 ; 
       32 75 300 ; 
       33 63 300 ; 
       34 64 300 ; 
       35 65 300 ; 
       36 66 300 ; 
       38 70 300 ; 
       39 71 300 ; 
       40 72 300 ; 
       41 73 300 ; 
       42 42 300 ; 
       43 38 300 ; 
       43 39 300 ; 
       43 40 300 ; 
       44 34 300 ; 
       44 35 300 ; 
       44 36 300 ; 
       44 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       11 1 400 ; 
       11 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       11 0 500 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       45 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 22 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 30 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       35 29 401 ; 
       36 31 401 ; 
       37 32 401 ; 
       39 33 401 ; 
       40 34 401 ; 
       41 41 401 ; 
       42 35 401 ; 
       43 36 401 ; 
       45 37 401 ; 
       46 38 401 ; 
       47 39 401 ; 
       48 40 401 ; 
       49 42 401 ; 
       50 43 401 ; 
       51 44 401 ; 
       52 50 401 ; 
       53 45 401 ; 
       54 46 401 ; 
       55 47 401 ; 
       56 48 401 ; 
       57 49 401 ; 
       59 51 401 ; 
       60 52 401 ; 
       61 53 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 11 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 55 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 67.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 72.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 77.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 80 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 57.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 60 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 62.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 65 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 67.5 0 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 82.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 1.266336 -19.27997 -5.318614 MPRFLG 0 ; 
       12 SCHEM 50 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 52.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -23.4193 0 MPRFLG 0 ; 
       14 SCHEM 8.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 47.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 21.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 38.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 26.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 40 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 42.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 37.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 31.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 21.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 17.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 22.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 20 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 25 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 15 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 2.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 35 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 30 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 32.5 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 8.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 7.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 10 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 12.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 45 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 45 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 45 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 44 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 44 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 44 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 41.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 41.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 41.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 46.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 46.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 46.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 46.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 49 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 85 0 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 16.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 34 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 26.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 29 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 31.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 21.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 19 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 24 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 6.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 9 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 1.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 41.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 44 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 44 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 41.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 41.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 44 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 44 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 44 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 46.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 46.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 46.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 49 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 90 0 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 85 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 87.5 0 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
