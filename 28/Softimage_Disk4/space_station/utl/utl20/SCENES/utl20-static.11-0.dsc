SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       youmap-cam_int1.50-0 ROOT ; 
       youmap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       add_frames-default13.5-0 ; 
       add_frames-mat2.5-0 ; 
       add_frames-mat23.5-0 ; 
       add_frames-mat24.5-0 ; 
       add_frames-mat25.5-0 ; 
       add_frames-mat26.5-0 ; 
       add_frames-mat27.5-0 ; 
       add_frames-mat28.5-0 ; 
       add_frames-mat29.5-0 ; 
       add_frames-mat30.5-0 ; 
       add_frames-mat31.5-0 ; 
       add_frames-mat33.5-0 ; 
       add_frames-mat34.5-0 ; 
       add_frames-mat37.5-0 ; 
       add_frames-mat38.5-0 ; 
       add_frames-mat39.5-0 ; 
       add_frames-mat40.5-0 ; 
       add_frames-mat41.5-0 ; 
       add_frames-mat42.5-0 ; 
       add_frames-mat43.5-0 ; 
       add_frames-mat44.5-0 ; 
       add_frames-mat45.5-0 ; 
       add_frames-mat46.5-0 ; 
       add_frames-mat47.5-0 ; 
       add_frames-mat48.5-0 ; 
       add_frames-mat49.5-0 ; 
       add_frames-mat50.5-0 ; 
       add_frames-mat51.5-0 ; 
       add_frames-mat52.5-0 ; 
       add_frames-mat53.5-0 ; 
       add_frames-mat54.5-0 ; 
       add_frames-mat55.5-0 ; 
       add_frames-mat56.5-0 ; 
       add_frames-mat57.5-0 ; 
       add_frames-mat58.5-0 ; 
       add_frames-mat59.5-0 ; 
       add_frames-mat60.5-0 ; 
       add_frames-mat61.5-0 ; 
       add_frames-mat62.5-0 ; 
       add_frames-mat63.5-0 ; 
       add_frames-mat64.5-0 ; 
       add_frames-mat66.5-0 ; 
       add_frames-mat67.5-0 ; 
       static-mat68.3-0 ; 
       static-mat69.3-0 ; 
       static-mat70.3-0 ; 
       static-mat71.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       static-ahubcon.1-0 ; 
       static-armour.1-0 ; 
       static-doccon.1-0 ; 
       static-engine.2-0 ; 
       static-fhubcon.3-0 ; 
       static-fuselg1.1-0 ; 
       static-fuselg2.1-0 ; 
       static-LAUNCH_TUBE.1-0 ; 
       static-LAUNCH_TUBE_1.1-0 ; 
       static-ldoccon.1-0 ; 
       static-lights1.1-0 ; 
       static-lights2.2-0 ; 
       static-rdoccon.1-0 ; 
       static-skin2.2-0 ; 
       static-tractr1.1-0 ; 
       static-tractr2.1-0 ; 
       static-tractr3.1-0 ; 
       static-utl20.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-static.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       add_frames-rendermap2.11-0 ; 
       add_frames-t2d23.6-0 ; 
       add_frames-t2d24.6-0 ; 
       add_frames-t2d25.6-0 ; 
       add_frames-t2d26.6-0 ; 
       add_frames-t2d27.6-0 ; 
       add_frames-t2d28.6-0 ; 
       add_frames-t2d29.6-0 ; 
       add_frames-t2d31.6-0 ; 
       add_frames-t2d32.6-0 ; 
       add_frames-t2d35.6-0 ; 
       add_frames-t2d36.6-0 ; 
       add_frames-t2d37.6-0 ; 
       add_frames-t2d38.6-0 ; 
       add_frames-t2d39.6-0 ; 
       add_frames-t2d40.6-0 ; 
       add_frames-t2d41.6-0 ; 
       add_frames-t2d42.6-0 ; 
       add_frames-t2d43.6-0 ; 
       add_frames-t2d44.6-0 ; 
       add_frames-t2d45.6-0 ; 
       add_frames-t2d46.6-0 ; 
       add_frames-t2d47.6-0 ; 
       add_frames-t2d48.6-0 ; 
       add_frames-t2d49.6-0 ; 
       add_frames-t2d50.6-0 ; 
       add_frames-t2d51.7-0 ; 
       add_frames-t2d52.7-0 ; 
       add_frames-t2d53.7-0 ; 
       add_frames-t2d54.7-0 ; 
       add_frames-t2d55.6-0 ; 
       add_frames-t2d56.7-0 ; 
       add_frames-t2d57.7-0 ; 
       add_frames-t2d58.6-0 ; 
       add_frames-t2d59.11-0 ; 
       static-t2d60.3-0 ; 
       static-t2d61.3-0 ; 
       static-t2d62.3-0 ; 
       static-t2d63.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 6 110 ; 
       4 5 110 ; 
       5 17 110 ; 
       6 5 110 ; 
       7 17 110 ; 
       8 17 110 ; 
       9 4 110 ; 
       10 5 110 ; 
       11 10 110 ; 
       12 4 110 ; 
       13 17 110 ; 
       14 5 110 ; 
       15 14 110 ; 
       16 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 27 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 38 300 ; 
       3 1 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       5 0 300 ; 
       5 34 300 ; 
       5 35 300 ; 
       5 36 300 ; 
       5 37 300 ; 
       5 39 300 ; 
       5 40 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       6 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       8 45 300 ; 
       8 46 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       10 9 300 ; 
       11 5 300 ; 
       13 41 300 ; 
       13 42 300 ; 
       14 26 300 ; 
       15 23 300 ; 
       15 24 300 ; 
       15 25 300 ; 
       16 19 300 ; 
       16 20 300 ; 
       16 21 300 ; 
       16 22 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 33 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 0 401 ; 
       42 34 401 ; 
       43 35 401 ; 
       44 36 401 ; 
       45 37 401 ; 
       46 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 25 -2 0 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 20 -2 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
       15 SCHEM 15 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -8 0 MPRFLG 0 ; 
       17 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
