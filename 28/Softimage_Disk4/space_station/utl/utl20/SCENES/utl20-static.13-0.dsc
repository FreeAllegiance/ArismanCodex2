SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       youmap-cam_int1.87-0 ROOT ; 
       youmap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       add_frames-default13.6-0 ; 
       add_frames-mat2.6-0 ; 
       add_frames-mat23.6-0 ; 
       add_frames-mat24.6-0 ; 
       add_frames-mat25.6-0 ; 
       add_frames-mat26.6-0 ; 
       add_frames-mat27.6-0 ; 
       add_frames-mat28.6-0 ; 
       add_frames-mat29.6-0 ; 
       add_frames-mat30.6-0 ; 
       add_frames-mat31.6-0 ; 
       add_frames-mat33.6-0 ; 
       add_frames-mat34.6-0 ; 
       add_frames-mat37.6-0 ; 
       add_frames-mat38.6-0 ; 
       add_frames-mat39.6-0 ; 
       add_frames-mat40.6-0 ; 
       add_frames-mat41.6-0 ; 
       add_frames-mat42.6-0 ; 
       add_frames-mat43.6-0 ; 
       add_frames-mat44.6-0 ; 
       add_frames-mat45.6-0 ; 
       add_frames-mat46.6-0 ; 
       add_frames-mat47.6-0 ; 
       add_frames-mat48.6-0 ; 
       add_frames-mat49.6-0 ; 
       add_frames-mat50.6-0 ; 
       add_frames-mat51.6-0 ; 
       add_frames-mat52.6-0 ; 
       add_frames-mat53.6-0 ; 
       add_frames-mat54.6-0 ; 
       add_frames-mat55.6-0 ; 
       add_frames-mat56.6-0 ; 
       add_frames-mat57.6-0 ; 
       add_frames-mat58.6-0 ; 
       add_frames-mat59.6-0 ; 
       add_frames-mat60.6-0 ; 
       add_frames-mat61.6-0 ; 
       add_frames-mat62.6-0 ; 
       add_frames-mat63.6-0 ; 
       add_frames-mat64.6-0 ; 
       add_frames-mat66.6-0 ; 
       add_frames-mat67.6-0 ; 
       static-mat68.5-0 ; 
       static-mat69.5-0 ; 
       static-mat70.5-0 ; 
       static-mat71.5-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       utl20-ahubcon.1-0 ; 
       utl20-armour.1-0 ; 
       utl20-doccon.1-0 ; 
       utl20-engine.2-0 ; 
       utl20-fhubcon.3-0 ; 
       utl20-fuselg1.1-0 ; 
       utl20-fuselg2.1-0 ; 
       utl20-ldoccon.1-0 ; 
       utl20-lights1.1-0 ; 
       utl20-lights2.2-0 ; 
       utl20-rdoccon.1-0 ; 
       utl20-skin2.2-0 ; 
       utl20-tractr1.1-0 ; 
       utl20-tractr2.1-0 ; 
       utl20-tractr3.1-0 ; 
       utl20-TUBE.1-0 ; 
       utl20-TUBE1.1-0 ; 
       utl20-utl20.41-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl20/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl20/PICTURES/utl20 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/utl/utl20/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-static.13-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       add_frames-rendermap2.12-0 ; 
       add_frames-t2d23.7-0 ; 
       add_frames-t2d24.7-0 ; 
       add_frames-t2d25.7-0 ; 
       add_frames-t2d26.7-0 ; 
       add_frames-t2d27.7-0 ; 
       add_frames-t2d28.7-0 ; 
       add_frames-t2d29.7-0 ; 
       add_frames-t2d31.7-0 ; 
       add_frames-t2d32.7-0 ; 
       add_frames-t2d35.7-0 ; 
       add_frames-t2d36.7-0 ; 
       add_frames-t2d37.7-0 ; 
       add_frames-t2d38.7-0 ; 
       add_frames-t2d39.7-0 ; 
       add_frames-t2d40.7-0 ; 
       add_frames-t2d41.7-0 ; 
       add_frames-t2d42.7-0 ; 
       add_frames-t2d43.7-0 ; 
       add_frames-t2d44.7-0 ; 
       add_frames-t2d45.7-0 ; 
       add_frames-t2d46.7-0 ; 
       add_frames-t2d47.7-0 ; 
       add_frames-t2d48.7-0 ; 
       add_frames-t2d49.7-0 ; 
       add_frames-t2d50.7-0 ; 
       add_frames-t2d51.8-0 ; 
       add_frames-t2d52.8-0 ; 
       add_frames-t2d53.8-0 ; 
       add_frames-t2d54.8-0 ; 
       add_frames-t2d55.7-0 ; 
       add_frames-t2d56.8-0 ; 
       add_frames-t2d57.8-0 ; 
       add_frames-t2d58.7-0 ; 
       add_frames-t2d59.12-0 ; 
       static-t2d60.5-0 ; 
       static-t2d61.5-0 ; 
       static-t2d62.5-0 ; 
       static-t2d63.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 6 110 ; 
       4 5 110 ; 
       5 17 110 ; 
       6 5 110 ; 
       7 4 110 ; 
       8 5 110 ; 
       9 8 110 ; 
       10 4 110 ; 
       11 17 110 ; 
       12 5 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       15 17 110 ; 
       16 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 27 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 38 300 ; 
       3 1 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       5 0 300 ; 
       5 34 300 ; 
       5 35 300 ; 
       5 36 300 ; 
       5 37 300 ; 
       5 39 300 ; 
       5 40 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       6 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       8 9 300 ; 
       9 5 300 ; 
       11 41 300 ; 
       11 42 300 ; 
       12 26 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       13 25 300 ; 
       14 19 300 ; 
       14 20 300 ; 
       14 21 300 ; 
       14 22 300 ; 
       15 45 300 ; 
       15 46 300 ; 
       16 43 300 ; 
       16 44 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 33 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 0 401 ; 
       42 34 401 ; 
       43 35 401 ; 
       44 36 401 ; 
       45 37 401 ; 
       46 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 20 -2 0 MPRFLG 0 ; 
       12 SCHEM 15 -4 0 MPRFLG 0 ; 
       13 SCHEM 15 -6 0 MPRFLG 0 ; 
       14 SCHEM 15 -8 0 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 13.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
