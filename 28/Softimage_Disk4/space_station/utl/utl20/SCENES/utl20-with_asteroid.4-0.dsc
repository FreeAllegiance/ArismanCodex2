SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl20-utl20.21-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       with_asteroid-cam_int1.4-0 ROOT ; 
       with_asteroid-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 56     
       with_asteroid-default13.1-0 ; 
       with_asteroid-mat2.2-0 ; 
       with_asteroid-mat23.1-0 ; 
       with_asteroid-mat24.1-0 ; 
       with_asteroid-mat25.1-0 ; 
       with_asteroid-mat26.1-0 ; 
       with_asteroid-mat27.1-0 ; 
       with_asteroid-mat28.1-0 ; 
       with_asteroid-mat29.1-0 ; 
       with_asteroid-mat30.1-0 ; 
       with_asteroid-mat31.1-0 ; 
       with_asteroid-mat33.1-0 ; 
       with_asteroid-mat34.1-0 ; 
       with_asteroid-mat37.1-0 ; 
       with_asteroid-mat38.1-0 ; 
       with_asteroid-mat39.1-0 ; 
       with_asteroid-mat40.1-0 ; 
       with_asteroid-mat41.1-0 ; 
       with_asteroid-mat42.1-0 ; 
       with_asteroid-mat43.1-0 ; 
       with_asteroid-mat44.1-0 ; 
       with_asteroid-mat45.1-0 ; 
       with_asteroid-mat46.1-0 ; 
       with_asteroid-mat47.1-0 ; 
       with_asteroid-mat48.1-0 ; 
       with_asteroid-mat49.1-0 ; 
       with_asteroid-mat50.1-0 ; 
       with_asteroid-mat51.1-0 ; 
       with_asteroid-mat52.1-0 ; 
       with_asteroid-mat53.1-0 ; 
       with_asteroid-mat54.1-0 ; 
       with_asteroid-mat55.1-0 ; 
       with_asteroid-mat56.1-0 ; 
       with_asteroid-mat57.1-0 ; 
       with_asteroid-mat58.1-0 ; 
       with_asteroid-mat59.1-0 ; 
       with_asteroid-mat60.1-0 ; 
       with_asteroid-mat61.1-0 ; 
       with_asteroid-mat62.1-0 ; 
       with_asteroid-mat63.1-0 ; 
       with_asteroid-mat64.1-0 ; 
       with_asteroid-mat66.1-0 ; 
       with_asteroid-nose_white-center.1-0.1-0 ; 
       with_asteroid-nose_white-center.1-0_1.1-0 ; 
       with_asteroid-nose_white-center.1-0_2.1-0 ; 
       with_asteroid-nose_white-center.1-0_3.1-0 ; 
       with_asteroid-nose_white-center.1-0_4.1-0 ; 
       with_asteroid-nose_white-center.1-1.1-0 ; 
       with_asteroid-nose_white-center.1-2.1-0 ; 
       with_asteroid-nose_white-center.1-3.1-0 ; 
       with_asteroid-nose_white-center.1-4.1-0 ; 
       with_asteroid-nose_white-center.1-5.1-0 ; 
       with_asteroid-nose_white-center.1-6.1-0 ; 
       with_asteroid-nose_white-center.1-7.1-0 ; 
       with_asteroid-nose_white-center.1-8.1-0 ; 
       with_asteroid-nose_white-center.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       utl20-ahubcon.1-0 ; 
       utl20-armour.1-0 ; 
       utl20-doccon.1-0 ; 
       utl20-engine.2-0 ; 
       utl20-fhubcon.3-0 ; 
       utl20-fuselg1.1-0 ; 
       utl20-fuselg2.1-0 ; 
       utl20-ldoccon.1-0 ; 
       utl20-lights1.1-0 ; 
       utl20-lights2.2-0 ; 
       utl20-rdoccon.1-0 ; 
       utl20-skin2.2-0 ; 
       utl20-SSa0.1-0 ; 
       utl20-SSb0.1-0 ; 
       utl20-SSb1.1-0 ; 
       utl20-SSb2.1-0 ; 
       utl20-SSb3.1-0 ; 
       utl20-SSb4.1-0 ; 
       utl20-SSb5.1-0 ; 
       utl20-SSb6.1-0 ; 
       utl20-SSm1.1-0 ; 
       utl20-SSm2.1-0 ; 
       utl20-SSm3.1-0 ; 
       utl20-SSm4.1-0 ; 
       utl20-SSr0.1-0 ; 
       utl20-SSr1.1-0 ; 
       utl20-SSr2.1-0 ; 
       utl20-SSr3.1-0 ; 
       utl20-SSr4.1-0 ; 
       utl20-tractr1.1-0 ; 
       utl20-tractr2.1-0 ; 
       utl20-tractr3.1-0 ; 
       utl20-utl20.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-with_asteroid.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       with_asteroid-rendermap2.1-0 ; 
       with_asteroid-t2d23.1-0 ; 
       with_asteroid-t2d24.1-0 ; 
       with_asteroid-t2d25.1-0 ; 
       with_asteroid-t2d26.1-0 ; 
       with_asteroid-t2d27.1-0 ; 
       with_asteroid-t2d28.1-0 ; 
       with_asteroid-t2d29.1-0 ; 
       with_asteroid-t2d31.1-0 ; 
       with_asteroid-t2d32.1-0 ; 
       with_asteroid-t2d35.1-0 ; 
       with_asteroid-t2d36.1-0 ; 
       with_asteroid-t2d37.1-0 ; 
       with_asteroid-t2d38.1-0 ; 
       with_asteroid-t2d39.1-0 ; 
       with_asteroid-t2d40.1-0 ; 
       with_asteroid-t2d41.1-0 ; 
       with_asteroid-t2d42.1-0 ; 
       with_asteroid-t2d43.1-0 ; 
       with_asteroid-t2d44.1-0 ; 
       with_asteroid-t2d45.1-0 ; 
       with_asteroid-t2d46.1-0 ; 
       with_asteroid-t2d47.1-0 ; 
       with_asteroid-t2d48.1-0 ; 
       with_asteroid-t2d49.1-0 ; 
       with_asteroid-t2d50.1-0 ; 
       with_asteroid-t2d51.1-0 ; 
       with_asteroid-t2d52.1-0 ; 
       with_asteroid-t2d53.1-0 ; 
       with_asteroid-t2d54.1-0 ; 
       with_asteroid-t2d55.1-0 ; 
       with_asteroid-t2d56.1-0 ; 
       with_asteroid-t2d57.1-0 ; 
       with_asteroid-t2d58.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 6 110 ; 
       4 5 110 ; 
       5 32 110 ; 
       6 5 110 ; 
       7 4 110 ; 
       8 5 110 ; 
       9 8 110 ; 
       10 4 110 ; 
       12 6 110 ; 
       13 3 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 0 110 ; 
       25 24 110 ; 
       26 24 110 ; 
       27 24 110 ; 
       28 24 110 ; 
       29 5 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       11 32 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 27 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 38 300 ; 
       3 1 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       5 0 300 ; 
       5 34 300 ; 
       5 35 300 ; 
       5 36 300 ; 
       5 37 300 ; 
       5 39 300 ; 
       5 40 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       6 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       8 9 300 ; 
       9 5 300 ; 
       14 42 300 ; 
       15 47 300 ; 
       16 48 300 ; 
       17 49 300 ; 
       18 54 300 ; 
       19 55 300 ; 
       20 43 300 ; 
       21 44 300 ; 
       22 45 300 ; 
       23 46 300 ; 
       25 50 300 ; 
       26 51 300 ; 
       27 52 300 ; 
       28 53 300 ; 
       29 26 300 ; 
       30 23 300 ; 
       30 24 300 ; 
       30 25 300 ; 
       31 19 300 ; 
       31 20 300 ; 
       31 21 300 ; 
       31 22 300 ; 
       11 41 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       32 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 33 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 168.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 160 -6 0 MPRFLG 0 ; 
       3 SCHEM 57.5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 120 -4 0 MPRFLG 0 ; 
       5 SCHEM 98.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 70 -4 0 MPRFLG 0 ; 
       7 SCHEM 117.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 132.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 127.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 115 -6 0 MPRFLG 0 ; 
       12 SCHEM 93.75 -6 0 MPRFLG 0 ; 
       13 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 30 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 35 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 97.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       21 SCHEM 90 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       22 SCHEM 92.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       23 SCHEM 95 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       24 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       25 SCHEM 5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 7.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 148.75 -4 0 MPRFLG 0 ; 
       30 SCHEM 147.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 143.75 -8 0 MPRFLG 0 ; 
       32 SCHEM 100 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 197.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 195 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 63.26396 -30.23969 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 177.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 170 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 172.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 147.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 182.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 185 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 187.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 175 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 190 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 192.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 197.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       33 SCHEM 200 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 162.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 165 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 132.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 135 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 167.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 170 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 172.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 120 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 122.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 142.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 145 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 182.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 185 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 187.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 175 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 190 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 192.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 197.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 199 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
