SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl20-utl20.15-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       youmap-cam_int1.1-0 ROOT ; 
       youmap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       youmap-inf_light1.1-0 ROOT ; 
       youmap-inf_light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 78     
       youmap-default13.1-0 ; 
       youmap-mat10.1-0 ; 
       youmap-mat11.1-0 ; 
       youmap-mat12.1-0 ; 
       youmap-mat13.1-0 ; 
       youmap-mat14.1-0 ; 
       youmap-mat15.1-0 ; 
       youmap-mat16.1-0 ; 
       youmap-mat17.1-0 ; 
       youmap-mat18.1-0 ; 
       youmap-mat19.1-0 ; 
       youmap-mat2.1-0 ; 
       youmap-mat20.1-0 ; 
       youmap-mat21.1-0 ; 
       youmap-mat22.1-0 ; 
       youmap-mat23.1-0 ; 
       youmap-mat24.1-0 ; 
       youmap-mat25.1-0 ; 
       youmap-mat26.1-0 ; 
       youmap-mat27.1-0 ; 
       youmap-mat28.1-0 ; 
       youmap-mat29.1-0 ; 
       youmap-mat3.1-0 ; 
       youmap-mat30.1-0 ; 
       youmap-mat31.1-0 ; 
       youmap-mat33.1-0 ; 
       youmap-mat34.1-0 ; 
       youmap-mat37.1-0 ; 
       youmap-mat38.1-0 ; 
       youmap-mat39.1-0 ; 
       youmap-mat4.1-0 ; 
       youmap-mat40.1-0 ; 
       youmap-mat41.1-0 ; 
       youmap-mat42.1-0 ; 
       youmap-mat43.1-0 ; 
       youmap-mat44.1-0 ; 
       youmap-mat45.1-0 ; 
       youmap-mat46.1-0 ; 
       youmap-mat47.1-0 ; 
       youmap-mat48.1-0 ; 
       youmap-mat49.1-0 ; 
       youmap-mat5.1-0 ; 
       youmap-mat50.1-0 ; 
       youmap-mat51.1-0 ; 
       youmap-mat52.1-0 ; 
       youmap-mat53.1-0 ; 
       youmap-mat54.1-0 ; 
       youmap-mat55.1-0 ; 
       youmap-mat56.1-0 ; 
       youmap-mat57.1-0 ; 
       youmap-mat58.1-0 ; 
       youmap-mat59.1-0 ; 
       youmap-mat6.1-0 ; 
       youmap-mat60.1-0 ; 
       youmap-mat61.1-0 ; 
       youmap-mat62.1-0 ; 
       youmap-mat63.1-0 ; 
       youmap-mat64.1-0 ; 
       youmap-mat65.1-0 ; 
       youmap-mat66.1-0 ; 
       youmap-mat67.1-0 ; 
       youmap-mat7.1-0 ; 
       youmap-mat8.1-0 ; 
       youmap-mat9.1-0 ; 
       youmap-nose_white-center.1-0.1-0 ; 
       youmap-nose_white-center.1-0_1.1-0 ; 
       youmap-nose_white-center.1-0_2.1-0 ; 
       youmap-nose_white-center.1-0_3.1-0 ; 
       youmap-nose_white-center.1-0_4.1-0 ; 
       youmap-nose_white-center.1-1.1-0 ; 
       youmap-nose_white-center.1-2.1-0 ; 
       youmap-nose_white-center.1-3.1-0 ; 
       youmap-nose_white-center.1-4.1-0 ; 
       youmap-nose_white-center.1-5.1-0 ; 
       youmap-nose_white-center.1-6.1-0 ; 
       youmap-nose_white-center.1-7.1-0 ; 
       youmap-nose_white-center.1-8.1-0 ; 
       youmap-nose_white-center.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 49     
       rendermap1-skin2.1-0 ROOT ; 
       rendermap2-skin2.1-0 ROOT ; 
       utl20-ahubcon.1-0 ; 
       utl20-armour.1-0 ; 
       utl20-doccon.1-0 ; 
       utl20-engine.2-0 ; 
       utl20-fhubcon.3-0 ; 
       utl20-fuselg1.1-0 ; 
       utl20-fuselg2.1-0 ; 
       utl20-ldoccon.1-0 ; 
       utl20-lights1.1-0 ; 
       utl20-lights2.2-0 ; 
       utl20-rdoccon.1-0 ; 
       utl20-SSa0.1-0 ; 
       utl20-SSb0.1-0 ; 
       utl20-SSb1.1-0 ; 
       utl20-SSb2.1-0 ; 
       utl20-SSb3.1-0 ; 
       utl20-SSb4.1-0 ; 
       utl20-SSb5.1-0 ; 
       utl20-SSb6.1-0 ; 
       utl20-SSm1.1-0 ; 
       utl20-SSm2.1-0 ; 
       utl20-SSm3.1-0 ; 
       utl20-SSm4.1-0 ; 
       utl20-SSr0.1-0 ; 
       utl20-SSr1.1-0 ; 
       utl20-SSr2.1-0 ; 
       utl20-SSr3.1-0 ; 
       utl20-SSr4.1-0 ; 
       utl20-tractr1.1-0 ; 
       utl20-tractr2.1-0 ; 
       utl20-tractr3.1-0 ; 
       utl20-utl20.5-0 ROOT ; 
       youmap-circle1.1-0 ; 
       youmap-circle10.1-0 ; 
       youmap-circle13.1-0 ; 
       youmap-circle15.1-0 ; 
       youmap-circle16.1-0 ; 
       youmap-circle17.1-0 ; 
       youmap-circle3.1-0 ; 
       youmap-circle4.1-0 ; 
       youmap-circle6.1-0 ; 
       youmap-circle8.1-0 ; 
       youmap-null1.1-0 ROOT ; 
       youmap-skin2.1-0 ROOT ; 
       youmap-spline1.1-0 ROOT ; 
       youmap-spline2.1-0 ROOT ; 
       youmap-YouMap1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/MoonMap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/cap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/rendermap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/rendermap_light ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-youmap.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       youmap-3DRock_Lava1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 58     
       youmap-cap.1-0 ; 
       youmap-main.1-0 ; 
       youmap-main1.1-0 ; 
       youmap-rendermap1.1-0 ; 
       youmap-rendermap2.1-0 ; 
       youmap-rendermap3.1-0 ; 
       youmap-t2d10.1-0 ; 
       youmap-t2d11.1-0 ; 
       youmap-t2d12.1-0 ; 
       youmap-t2d13.1-0 ; 
       youmap-t2d14.1-0 ; 
       youmap-t2d15.1-0 ; 
       youmap-t2d16.1-0 ; 
       youmap-t2d17.1-0 ; 
       youmap-t2d18.1-0 ; 
       youmap-t2d19.1-0 ; 
       youmap-t2d20.1-0 ; 
       youmap-t2d21.1-0 ; 
       youmap-t2d22.1-0 ; 
       youmap-t2d23.1-0 ; 
       youmap-t2d24.1-0 ; 
       youmap-t2d25.1-0 ; 
       youmap-t2d26.1-0 ; 
       youmap-t2d27.1-0 ; 
       youmap-t2d28.1-0 ; 
       youmap-t2d29.1-0 ; 
       youmap-t2d3.1-0 ; 
       youmap-t2d31.1-0 ; 
       youmap-t2d32.1-0 ; 
       youmap-t2d35.1-0 ; 
       youmap-t2d36.1-0 ; 
       youmap-t2d37.1-0 ; 
       youmap-t2d38.1-0 ; 
       youmap-t2d39.1-0 ; 
       youmap-t2d4.1-0 ; 
       youmap-t2d40.1-0 ; 
       youmap-t2d41.1-0 ; 
       youmap-t2d42.1-0 ; 
       youmap-t2d43.1-0 ; 
       youmap-t2d44.1-0 ; 
       youmap-t2d45.1-0 ; 
       youmap-t2d46.1-0 ; 
       youmap-t2d47.1-0 ; 
       youmap-t2d48.1-0 ; 
       youmap-t2d49.1-0 ; 
       youmap-t2d5.1-0 ; 
       youmap-t2d50.1-0 ; 
       youmap-t2d51.1-0 ; 
       youmap-t2d52.1-0 ; 
       youmap-t2d53.1-0 ; 
       youmap-t2d54.1-0 ; 
       youmap-t2d55.1-0 ; 
       youmap-t2d56.1-0 ; 
       youmap-t2d57.1-0 ; 
       youmap-t2d6.1-0 ; 
       youmap-t2d7.1-0 ; 
       youmap-t2d8.1-0 ; 
       youmap-t2d9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       youmap-t3d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       34 44 110 ; 
       35 44 110 ; 
       36 44 110 ; 
       37 44 110 ; 
       38 44 110 ; 
       39 44 110 ; 
       40 44 110 ; 
       41 44 110 ; 
       42 44 110 ; 
       43 44 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 3 110 ; 
       5 8 110 ; 
       6 7 110 ; 
       7 33 110 ; 
       8 7 110 ; 
       9 6 110 ; 
       10 7 110 ; 
       11 10 110 ; 
       12 6 110 ; 
       13 8 110 ; 
       14 5 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 25 110 ; 
       20 25 110 ; 
       21 13 110 ; 
       22 13 110 ; 
       23 13 110 ; 
       24 13 110 ; 
       25 2 110 ; 
       26 25 110 ; 
       27 25 110 ; 
       28 25 110 ; 
       29 25 110 ; 
       30 7 110 ; 
       31 30 110 ; 
       32 31 110 ; 
       0 1 20000 RELDATA 2 1 48 1 2 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       0 1 20000 2 RELDATA 2 2 48 1 1 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       0 48 20000 4 RELDATA 2 4 48 1 5 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       0 1 20000 RELDATA 1 1 48 2 2 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       0 1 20000 2 RELDATA 1 2 48 2 1 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       0 48 20000 4 RELDATA 1 4 48 2 5 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       45 58 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 43 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       3 17 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 55 300 ; 
       5 11 300 ; 
       5 22 300 ; 
       5 30 300 ; 
       5 41 300 ; 
       5 52 300 ; 
       5 61 300 ; 
       5 62 300 ; 
       5 63 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       7 0 300 ; 
       7 50 300 ; 
       7 51 300 ; 
       7 53 300 ; 
       7 54 300 ; 
       7 56 300 ; 
       7 57 300 ; 
       8 44 300 ; 
       8 45 300 ; 
       8 46 300 ; 
       8 47 300 ; 
       8 48 300 ; 
       8 49 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       10 23 300 ; 
       11 18 300 ; 
       15 64 300 ; 
       16 69 300 ; 
       17 70 300 ; 
       18 71 300 ; 
       19 76 300 ; 
       20 77 300 ; 
       21 65 300 ; 
       22 66 300 ; 
       23 67 300 ; 
       24 68 300 ; 
       26 72 300 ; 
       27 73 300 ; 
       28 74 300 ; 
       29 75 300 ; 
       30 42 300 ; 
       31 38 300 ; 
       31 39 300 ; 
       31 40 300 ; 
       32 34 300 ; 
       32 35 300 ; 
       32 36 300 ; 
       32 37 300 ; 
       0 59 300 ; 
       1 60 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       45 1 400 ; 
       45 0 400 ; 
       0 4 20000 3 RELDATA 2 3 48 1 0 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       0 4 20000 3 RELDATA 1 3 48 2 0 AOCC 0 ROCC 0 ENAME YouMap MNAME Matter EndOfRELDATA ; 
       1 2 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       45 0 500 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       33 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 6 401 ; 
       2 7 401 ; 
       3 8 401 ; 
       4 9 401 ; 
       5 10 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 15 401 ; 
       12 16 401 ; 
       13 17 401 ; 
       14 18 401 ; 
       16 19 401 ; 
       17 20 401 ; 
       18 21 401 ; 
       20 22 401 ; 
       21 23 401 ; 
       22 26 401 ; 
       23 24 401 ; 
       24 25 401 ; 
       25 27 401 ; 
       26 28 401 ; 
       28 29 401 ; 
       29 30 401 ; 
       30 34 401 ; 
       32 31 401 ; 
       33 32 401 ; 
       35 33 401 ; 
       36 35 401 ; 
       37 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 45 401 ; 
       42 39 401 ; 
       43 40 401 ; 
       45 41 401 ; 
       46 42 401 ; 
       47 43 401 ; 
       48 44 401 ; 
       49 46 401 ; 
       50 47 401 ; 
       51 48 401 ; 
       52 54 401 ; 
       53 49 401 ; 
       54 50 401 ; 
       55 51 401 ; 
       56 52 401 ; 
       57 53 401 ; 
       58 3 401 ; 
       61 55 401 ; 
       62 56 401 ; 
       63 57 401 ; 
       59 4 401 ; 
       60 5 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 45 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 237.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 240 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       34 SCHEM 202.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 215 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 217.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 220 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 222.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 225 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 205 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 207.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 210 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 212.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 213.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       45 SCHEM 231.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 1.266336 -19.27997 -5.318614 MPRFLG 0 ; 
       46 SCHEM 197.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       47 SCHEM 200 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -23.4193 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 168.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 160 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 120 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 98.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 70 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 117.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 132.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 127.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 115 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 93.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 31.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 30 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 35 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 15 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 97.5 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 90 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 92.5 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 95 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 8.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 7.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 10 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 12.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 148.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 147.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 143.75 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 98.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 242.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 1.266336 -19.27997 -5.318614 MPRFLG 0 ; 
       1 SCHEM 248.3463 0 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 1.266336 -19.27997 -5.318614 MPRFLG 0 ; 
       48 SCHEM 254.5963 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 195 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 57.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 60 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 37.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 65 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 67.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 70 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 72.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 75 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 77.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 80 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 62.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 82.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 85 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 87.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 177.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 162.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 165 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 127.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 137.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 130 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 132.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 40 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 135 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 167.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 170 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 172.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 25 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 17.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 20 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 42.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 125 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 120 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 122.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 147.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 140 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 142.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 145 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 155 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 150 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 152.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 45 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 157.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 22.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 112.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 100 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 102.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 105 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 107.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 110 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 180 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 182.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 47.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 185 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 187.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 175 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 190 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 192.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 230 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 50 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 52.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 55 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 27.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 97.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 90 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 92.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 95 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 32.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 30 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 35 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 7.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 10 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 12.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 15 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM 2.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 241.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 247.0963 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       6 SCHEM 57.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 60 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 65 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 67.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 70 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 72.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 75 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 77.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 80 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 82.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 85 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 87.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 162.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 165 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 127.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 130 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 132.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 135 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 167.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 40 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 170 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 172.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 17.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 20 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 120 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 122.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 140 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 42.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 142.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 145 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 150 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 152.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 157.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 22.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 100 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 102.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 105 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 107.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 45 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 110 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 180 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 182.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 185 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 187.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 175 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 190 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 192.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 235 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 47.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 50 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 52.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 55 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 227.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 230 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 252.0963 -2 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 241.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 247.0963 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 232.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 196.5 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
