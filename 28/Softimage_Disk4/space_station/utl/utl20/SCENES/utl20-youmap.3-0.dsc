SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl20-utl20.17-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       youmap-cam_int1.3-0 ROOT ; 
       youmap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       youmap-inf_light1.3-0 ROOT ; 
       youmap-inf_light2.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 77     
       youmap-default13.1-0 ; 
       youmap-mat10.1-0 ; 
       youmap-mat11.1-0 ; 
       youmap-mat12.1-0 ; 
       youmap-mat13.1-0 ; 
       youmap-mat14.1-0 ; 
       youmap-mat15.1-0 ; 
       youmap-mat16.1-0 ; 
       youmap-mat17.1-0 ; 
       youmap-mat18.1-0 ; 
       youmap-mat19.1-0 ; 
       youmap-mat2.1-0 ; 
       youmap-mat20.1-0 ; 
       youmap-mat21.1-0 ; 
       youmap-mat22.1-0 ; 
       youmap-mat23.1-0 ; 
       youmap-mat24.1-0 ; 
       youmap-mat25.1-0 ; 
       youmap-mat26.1-0 ; 
       youmap-mat27.1-0 ; 
       youmap-mat28.1-0 ; 
       youmap-mat29.1-0 ; 
       youmap-mat3.1-0 ; 
       youmap-mat30.1-0 ; 
       youmap-mat31.1-0 ; 
       youmap-mat33.1-0 ; 
       youmap-mat34.1-0 ; 
       youmap-mat37.1-0 ; 
       youmap-mat38.1-0 ; 
       youmap-mat39.1-0 ; 
       youmap-mat4.1-0 ; 
       youmap-mat40.1-0 ; 
       youmap-mat41.1-0 ; 
       youmap-mat42.1-0 ; 
       youmap-mat43.1-0 ; 
       youmap-mat44.1-0 ; 
       youmap-mat45.1-0 ; 
       youmap-mat46.1-0 ; 
       youmap-mat47.1-0 ; 
       youmap-mat48.1-0 ; 
       youmap-mat49.1-0 ; 
       youmap-mat5.1-0 ; 
       youmap-mat50.1-0 ; 
       youmap-mat51.1-0 ; 
       youmap-mat52.1-0 ; 
       youmap-mat53.1-0 ; 
       youmap-mat54.1-0 ; 
       youmap-mat55.1-0 ; 
       youmap-mat56.1-0 ; 
       youmap-mat57.1-0 ; 
       youmap-mat58.1-0 ; 
       youmap-mat59.1-0 ; 
       youmap-mat6.1-0 ; 
       youmap-mat60.1-0 ; 
       youmap-mat61.1-0 ; 
       youmap-mat62.1-0 ; 
       youmap-mat63.1-0 ; 
       youmap-mat64.1-0 ; 
       youmap-mat65.1-0 ; 
       youmap-mat66.2-0 ; 
       youmap-mat7.1-0 ; 
       youmap-mat8.1-0 ; 
       youmap-mat9.1-0 ; 
       youmap-nose_white-center.1-0.1-0 ; 
       youmap-nose_white-center.1-0_1.1-0 ; 
       youmap-nose_white-center.1-0_2.1-0 ; 
       youmap-nose_white-center.1-0_3.1-0 ; 
       youmap-nose_white-center.1-0_4.1-0 ; 
       youmap-nose_white-center.1-1.1-0 ; 
       youmap-nose_white-center.1-2.1-0 ; 
       youmap-nose_white-center.1-3.1-0 ; 
       youmap-nose_white-center.1-4.1-0 ; 
       youmap-nose_white-center.1-5.1-0 ; 
       youmap-nose_white-center.1-6.1-0 ; 
       youmap-nose_white-center.1-7.1-0 ; 
       youmap-nose_white-center.1-8.1-0 ; 
       youmap-nose_white-center.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 47     
       utl20-ahubcon.1-0 ; 
       utl20-armour.1-0 ; 
       utl20-doccon.1-0 ; 
       utl20-engine.2-0 ; 
       utl20-fhubcon.3-0 ; 
       utl20-fuselg1.1-0 ; 
       utl20-fuselg2.1-0 ; 
       utl20-ldoccon.1-0 ; 
       utl20-lights1.1-0 ; 
       utl20-lights2.2-0 ; 
       utl20-rdoccon.1-0 ; 
       utl20-skin2.2-0 ; 
       utl20-SSa0.1-0 ; 
       utl20-SSb0.1-0 ; 
       utl20-SSb1.1-0 ; 
       utl20-SSb2.1-0 ; 
       utl20-SSb3.1-0 ; 
       utl20-SSb4.1-0 ; 
       utl20-SSb5.1-0 ; 
       utl20-SSb6.1-0 ; 
       utl20-SSm1.1-0 ; 
       utl20-SSm2.1-0 ; 
       utl20-SSm3.1-0 ; 
       utl20-SSm4.1-0 ; 
       utl20-SSr0.1-0 ; 
       utl20-SSr1.1-0 ; 
       utl20-SSr2.1-0 ; 
       utl20-SSr3.1-0 ; 
       utl20-SSr4.1-0 ; 
       utl20-tractr1.1-0 ; 
       utl20-tractr2.1-0 ; 
       utl20-tractr3.1-0 ; 
       utl20-utl20.6-0 ROOT ; 
       youmap-circle1.1-0 ; 
       youmap-circle10.1-0 ; 
       youmap-circle13.1-0 ; 
       youmap-circle15.1-0 ; 
       youmap-circle16.1-0 ; 
       youmap-circle17.1-0 ; 
       youmap-circle3.1-0 ; 
       youmap-circle4.1-0 ; 
       youmap-circle6.1-0 ; 
       youmap-circle8.1-0 ; 
       youmap-null1.1-0 ROOT ; 
       youmap-skin2.1-0 ROOT ; 
       youmap-spline1.1-0 ROOT ; 
       youmap-spline2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/MoonMap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/cap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/rendermap ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20 ; 
       E:/Pete_Data2/space_station/utl/utl20/PICTURES/utl20a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl20-youmap.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       youmap-3DRock_Lava1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 56     
       youmap-cap.1-0 ; 
       youmap-main.1-0 ; 
       youmap-rendermap1.1-0 ; 
       youmap-rendermap2.3-0 ; 
       youmap-t2d10.1-0 ; 
       youmap-t2d11.1-0 ; 
       youmap-t2d12.1-0 ; 
       youmap-t2d13.1-0 ; 
       youmap-t2d14.1-0 ; 
       youmap-t2d15.1-0 ; 
       youmap-t2d16.1-0 ; 
       youmap-t2d17.1-0 ; 
       youmap-t2d18.1-0 ; 
       youmap-t2d19.1-0 ; 
       youmap-t2d20.1-0 ; 
       youmap-t2d21.1-0 ; 
       youmap-t2d22.1-0 ; 
       youmap-t2d23.1-0 ; 
       youmap-t2d24.1-0 ; 
       youmap-t2d25.1-0 ; 
       youmap-t2d26.1-0 ; 
       youmap-t2d27.1-0 ; 
       youmap-t2d28.1-0 ; 
       youmap-t2d29.1-0 ; 
       youmap-t2d3.1-0 ; 
       youmap-t2d31.1-0 ; 
       youmap-t2d32.1-0 ; 
       youmap-t2d35.1-0 ; 
       youmap-t2d36.1-0 ; 
       youmap-t2d37.1-0 ; 
       youmap-t2d38.1-0 ; 
       youmap-t2d39.1-0 ; 
       youmap-t2d4.1-0 ; 
       youmap-t2d40.1-0 ; 
       youmap-t2d41.1-0 ; 
       youmap-t2d42.1-0 ; 
       youmap-t2d43.1-0 ; 
       youmap-t2d44.1-0 ; 
       youmap-t2d45.1-0 ; 
       youmap-t2d46.1-0 ; 
       youmap-t2d47.1-0 ; 
       youmap-t2d48.1-0 ; 
       youmap-t2d49.1-0 ; 
       youmap-t2d5.1-0 ; 
       youmap-t2d50.1-0 ; 
       youmap-t2d51.1-0 ; 
       youmap-t2d52.1-0 ; 
       youmap-t2d53.1-0 ; 
       youmap-t2d54.1-0 ; 
       youmap-t2d55.1-0 ; 
       youmap-t2d56.1-0 ; 
       youmap-t2d57.1-0 ; 
       youmap-t2d6.1-0 ; 
       youmap-t2d7.1-0 ; 
       youmap-t2d8.1-0 ; 
       youmap-t2d9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       youmap-t3d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       33 43 110 ; 
       34 43 110 ; 
       35 43 110 ; 
       36 43 110 ; 
       37 43 110 ; 
       38 43 110 ; 
       39 43 110 ; 
       40 43 110 ; 
       41 43 110 ; 
       42 43 110 ; 
       0 5 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 6 110 ; 
       4 5 110 ; 
       5 32 110 ; 
       6 5 110 ; 
       7 4 110 ; 
       8 5 110 ; 
       9 8 110 ; 
       10 4 110 ; 
       12 6 110 ; 
       13 3 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 0 110 ; 
       25 24 110 ; 
       26 24 110 ; 
       27 24 110 ; 
       28 24 110 ; 
       29 5 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       11 32 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       44 58 300 ; 
       0 27 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       0 43 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 24 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 55 300 ; 
       3 11 300 ; 
       3 22 300 ; 
       3 30 300 ; 
       3 41 300 ; 
       3 52 300 ; 
       3 60 300 ; 
       3 61 300 ; 
       3 62 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       5 0 300 ; 
       5 50 300 ; 
       5 51 300 ; 
       5 53 300 ; 
       5 54 300 ; 
       5 56 300 ; 
       5 57 300 ; 
       6 44 300 ; 
       6 45 300 ; 
       6 46 300 ; 
       6 47 300 ; 
       6 48 300 ; 
       6 49 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 23 300 ; 
       9 18 300 ; 
       14 63 300 ; 
       15 68 300 ; 
       16 69 300 ; 
       17 70 300 ; 
       18 75 300 ; 
       19 76 300 ; 
       20 64 300 ; 
       21 65 300 ; 
       22 66 300 ; 
       23 67 300 ; 
       25 71 300 ; 
       26 72 300 ; 
       27 73 300 ; 
       28 74 300 ; 
       29 42 300 ; 
       30 38 300 ; 
       30 39 300 ; 
       30 40 300 ; 
       31 34 300 ; 
       31 35 300 ; 
       31 36 300 ; 
       31 37 300 ; 
       11 59 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       44 1 400 ; 
       44 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       44 0 500 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       32 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 7 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 10 401 ; 
       8 11 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       14 16 401 ; 
       16 17 401 ; 
       17 18 401 ; 
       18 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 24 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 32 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       35 31 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       39 35 401 ; 
       40 36 401 ; 
       41 43 401 ; 
       42 37 401 ; 
       43 38 401 ; 
       45 39 401 ; 
       46 40 401 ; 
       47 41 401 ; 
       48 42 401 ; 
       49 44 401 ; 
       50 45 401 ; 
       51 46 401 ; 
       52 52 401 ; 
       53 47 401 ; 
       54 48 401 ; 
       55 49 401 ; 
       56 50 401 ; 
       57 51 401 ; 
       58 2 401 ; 
       60 53 401 ; 
       61 54 401 ; 
       62 55 401 ; 
       59 3 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 44 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 240 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 242.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       33 SCHEM 205 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 217.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 220 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 222.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 225 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 227.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 207.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 210 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 212.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 215 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 216.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       44 SCHEM 233.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 1.266336 -19.27997 -5.318614 MPRFLG 0 ; 
       45 SCHEM 200 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       46 SCHEM 202.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -23.4193 0 MPRFLG 0 ; 
       0 SCHEM 13.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 168.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 160 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 57.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 120 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 98.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 70 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 117.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 132.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 127.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 115 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 93.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 31.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 30 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 35 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 2.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 97.5 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 90 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 92.5 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 95 -8 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 8.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 7.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 10 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 12.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 148.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 147.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 143.75 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 100 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 197.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 195 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 57.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 60 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 37.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 65 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 67.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 70 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 72.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 75 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 77.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 80 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 62.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 82.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 85 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 87.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 177.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 162.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 165 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 127.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 137.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 130 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 132.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 40 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 135 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 167.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 170 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 172.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 25 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 17.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 20 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 42.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 125 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 120 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 122.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 147.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 140 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 142.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 145 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 155 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 150 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 152.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 45 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 157.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 22.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 112.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 100 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 102.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 105 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 107.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 110 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 180 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 182.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 47.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 185 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 187.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 175 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 190 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 192.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 232.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 50 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 52.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 55 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 27.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 97.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 90 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 92.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 95 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 32.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 30 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 35 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 7.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 10 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 12.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 15 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 2.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 197.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       4 SCHEM 57.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 60 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 65 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 67.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 70 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 72.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 75 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 77.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 80 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 82.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 85 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 87.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 162.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 165 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 127.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 130 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 132.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 135 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 167.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 40 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 170 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 172.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 17.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 20 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 120 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 122.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 140 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 42.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 142.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 145 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 150 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 152.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 157.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 22.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 100 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 102.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 105 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 107.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 45 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 110 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 180 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 182.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 185 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 187.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 175 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 190 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 192.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 237.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 47.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 50 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 52.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 55 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 230 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 232.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 197.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 235 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 199 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
