SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       Letters-cam_int1.1-0 ROOT ; 
       Letters-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       Logo-face10.1-0 ; 
       Logo-face12.1-0 ; 
       Logo-face13.1-0 ; 
       Logo-face16.1-0 ; 
       Logo-face18.1-0 ; 
       Logo-face20.1-0 ; 
       Logo-face22.1-0 ; 
       Logo-face5.1-0 ; 
       Logo-face6.1-0 ; 
       Logo-face9.1-0 ; 
       Logo-Root.1-0 ROOT ; 
       Logo-Root_1.10-0 ; 
       Logo-Shield.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Polys-Letters.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       9 11 110 ; 
       4 11 110 ; 
       11 10 110 ; 
       5 11 110 ; 
       6 11 110 ; 
       1 11 110 ; 
       0 11 110 ; 
       12 10 110 ; 
       3 11 110 ; 
       2 11 110 ; 
       7 11 110 ; 
       8 11 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 MPRFLG 0 ; 
       0 SCHEM 10 -4 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
