SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       Letters-cam_int1.1-0 ROOT ; 
       Letters-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       Letters-Constant_Black1.1-0 ; 
       Letters-White_Text1.1-0 ; 
       Letters-White_Text2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       Logo-face10.1-0 ; 
       Logo-face12.1-0 ; 
       Logo-face13.1-0 ; 
       Logo-face16.1-0 ; 
       Logo-face18.1-0 ; 
       Logo-face20.1-0 ; 
       Logo-face22.1-0 ; 
       Logo-face23.1-0 ; 
       Logo-face24.1-0 ; 
       Logo-face25.1-0 ; 
       Logo-face26.1-0 ; 
       Logo-face27.1-0 ; 
       Logo-face28.1-0 ; 
       Logo-face29.1-0 ; 
       Logo-face30.1-0 ; 
       Logo-face31.1-0 ; 
       Logo-face32.1-0 ; 
       Logo-face5.1-0 ; 
       Logo-face6.1-0 ; 
       Logo-face9.1-0 ; 
       Logo-null1.1-0 ; 
       Logo-Red_Text.1-0 ; 
       Logo-Root.2-0 ROOT ; 
       Logo-Shield.1-0 ; 
       Logo-White_Text.10-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Polys-Letters.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       21 20 110 ; 
       7 21 110 ; 
       8 21 110 ; 
       9 21 110 ; 
       10 21 110 ; 
       11 21 110 ; 
       12 21 110 ; 
       13 21 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 21 110 ; 
       20 22 110 ; 
       19 24 110 ; 
       4 24 110 ; 
       24 20 110 ; 
       5 24 110 ; 
       6 24 110 ; 
       1 24 110 ; 
       0 24 110 ; 
       23 22 110 ; 
       3 24 110 ; 
       2 24 110 ; 
       17 24 110 ; 
       18 24 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       21 2 300 ; 
       24 1 300 ; 
       23 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       21 SCHEM 45 -4 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 40 -6 0 MPRFLG 0 ; 
       9 SCHEM 35 -6 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 45 -6 0 MPRFLG 0 ; 
       13 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 50 -6 0 MPRFLG 0 ; 
       15 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 55 -6 0 MPRFLG 0 ; 
       20 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 25 -6 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       0 SCHEM 10 -6 0 MPRFLG 0 ; 
       23 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
