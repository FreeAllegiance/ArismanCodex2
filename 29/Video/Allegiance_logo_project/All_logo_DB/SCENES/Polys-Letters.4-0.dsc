SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       Letters-cam_int1.1-0 ROOT ; 
       Letters-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       Letters-Constant_Black1.1-0 ; 
       Letters-White_Text1.1-0 ; 
       Letters-White_Text2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       Letters-face10.1-0 ; 
       Letters-face12.1-0 ; 
       Letters-face13.1-0 ; 
       Letters-face16.1-0 ; 
       Letters-face18.1-0 ; 
       Letters-face20.1-0 ; 
       Letters-face22.1-0 ; 
       Letters-face23.1-0 ; 
       Letters-face24.1-0 ; 
       Letters-face25.1-0 ; 
       Letters-face26.1-0 ; 
       Letters-face27.1-0 ; 
       Letters-face28.1-0 ; 
       Letters-face29.1-0 ; 
       Letters-face30.1-0 ; 
       Letters-face31.1-0 ; 
       Letters-face32.1-0 ; 
       Letters-face5.1-0 ; 
       Letters-face6.1-0 ; 
       Letters-face9.1-0 ; 
       Letters-null1.1-0 ; 
       Letters-null2.1-0 ROOT ; 
       Letters-Red_Text.1-0 ; 
       Letters-Root.2-0 ; 
       Letters-Shield.1-0 ; 
       Letters-White_Text.10-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Polys-Letters.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       22 20 110 ; 
       7 22 110 ; 
       8 22 110 ; 
       9 22 110 ; 
       10 22 110 ; 
       11 22 110 ; 
       12 22 110 ; 
       13 22 110 ; 
       14 22 110 ; 
       15 22 110 ; 
       16 22 110 ; 
       20 23 110 ; 
       19 25 110 ; 
       4 25 110 ; 
       25 20 110 ; 
       5 25 110 ; 
       6 25 110 ; 
       23 21 110 ; 
       1 25 110 ; 
       0 25 110 ; 
       24 23 110 ; 
       3 25 110 ; 
       2 25 110 ; 
       17 25 110 ; 
       18 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       22 2 300 ; 
       25 1 300 ; 
       24 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       22 SCHEM 45 -6 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 40 -8 0 MPRFLG 0 ; 
       9 SCHEM 35 -8 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 45 -8 0 MPRFLG 0 ; 
       13 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 50 -8 0 MPRFLG 0 ; 
       15 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 55 -8 0 MPRFLG 0 ; 
       20 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -6 0 USR MPRFLG 0 ; 
       5 SCHEM 25 -8 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 30 -2 0 MPRFLG 0 ; 
       1 SCHEM 15 -8 0 MPRFLG 0 ; 
       0 SCHEM 10 -8 0 MPRFLG 0 ; 
       24 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 20 -8 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
