SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       lettes-cam_int1.1-0 ROOT ; 
       lettes-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 35     
       from_Ai-Root.3-0 ROOT ; 
       from_Ai-Splines1.1-0 ; 
       from_Ai-Splines2.1-0 ; 
       from_Ai-spl_1.1-0 ; 
       from_Ai-spl_10.1-0 ; 
       from_Ai-spl_11.1-0 ; 
       from_Ai-spl_12.1-0 ; 
       from_Ai-spl_13.1-0 ; 
       from_Ai-spl_14.1-0 ; 
       from_Ai-spl_15.1-0 ; 
       from_Ai-spl_2.1-0 ; 
       from_Ai-spl_3.1-0 ; 
       from_Ai-spl_4.1-0 ; 
       from_Ai-spl_5.1-0 ; 
       from_Ai-spl_6.1-0 ; 
       from_Ai-spl_7.1-0 ; 
       from_Ai-spl_8.1-0 ; 
       from_Ai-spl_9.1-0 ; 
       from_Ai2-face1.1-0 ; 
       from_Ai2-midI.1-0 ; 
       from_Ai2-Root.6-0 ROOT ; 
       from_Ai2-Sheild.1-0 ; 
       from_Ai2-Splines2.1-0 ; 
       from_Ai2-spl_10.1-0 ; 
       from_Ai2-spl_11.1-0 ; 
       from_Ai2-spl_12.1-0 ; 
       from_Ai2-spl_13.1-0 ; 
       from_Ai2-spl_14.1-0 ; 
       from_Ai2-spl_15.1-0 ; 
       from_Ai2-spl_2.1-0 ; 
       from_Ai2-spl_3.1-0 ; 
       from_Ai2-spl_5.1-0 ; 
       from_Ai2-spl_7.1-0 ; 
       from_Ai2-spl_8.1-0 ; 
       from_Ai2-spl_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       set_up_ai-lettes.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 1 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 22 110 ; 
       19 20 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       26 22 110 ; 
       27 22 110 ; 
       28 22 110 ; 
       29 22 110 ; 
       30 22 110 ; 
       31 22 110 ; 
       32 22 110 ; 
       33 22 110 ; 
       34 22 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 30 -4 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 35 -4 0 MPRFLG 0 ; 
       9 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 MPRFLG 0 ; 
       17 SCHEM 5 -4 0 MPRFLG 0 ; 
       18 SCHEM 42.98244 -7.001032 0 USR MPRFLG 0 ; 
       19 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       20 SCHEM 57.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 40 -2 0 MPRFLG 0 ; 
       22 SCHEM 60 -2 0 MPRFLG 0 ; 
       23 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 65 -4 0 MPRFLG 0 ; 
       25 SCHEM 67.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       26 SCHEM 70 -4 0 MPRFLG 0 ; 
       27 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 45.88548 -6.664074 0 USR MPRFLG 0 ; 
       29 SCHEM 60 -4 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 50 -4 0 MPRFLG 0 ; 
       32 SCHEM 55 -4 0 MPRFLG 0 ; 
       33 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 45 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
