SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       polys-cam_int1.1-0 ROOT ; 
       polys-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       from_Ai-Root.4-0 ROOT ; 
       from_Ai-Splines1.1-0 ; 
       from_Ai-Splines2.1-0 ; 
       from_Ai-spl_1.1-0 ; 
       from_Ai-spl_10.1-0 ; 
       from_Ai-spl_11.1-0 ; 
       from_Ai-spl_12.1-0 ; 
       from_Ai-spl_13.1-0 ; 
       from_Ai-spl_14.1-0 ; 
       from_Ai-spl_15.1-0 ; 
       from_Ai-spl_2.1-0 ; 
       from_Ai-spl_3.1-0 ; 
       from_Ai-spl_4.1-0 ; 
       from_Ai-spl_5.1-0 ; 
       from_Ai-spl_6.1-0 ; 
       from_Ai-spl_7.1-0 ; 
       from_Ai-spl_8.1-0 ; 
       from_Ai-spl_9.1-0 ; 
       from_Ai2-face10.1-0 ; 
       from_Ai2-face12.1-0 ; 
       from_Ai2-face13.1-0 ; 
       from_Ai2-face14.1-0 ; 
       from_Ai2-face16.1-0 ; 
       from_Ai2-face18.1-0 ; 
       from_Ai2-face20.1-0 ; 
       from_Ai2-face22.1-0 ; 
       from_Ai2-face5.1-0 ; 
       from_Ai2-face6.1-0 ; 
       from_Ai2-face9.1-0 ; 
       from_Ai2-Root.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       set_up_ai-polys.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 1 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       28 29 110 ; 
       23 29 110 ; 
       24 29 110 ; 
       25 29 110 ; 
       19 29 110 ; 
       18 29 110 ; 
       21 29 110 ; 
       22 29 110 ; 
       20 29 110 ; 
       26 29 110 ; 
       27 29 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 21.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 30 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 35 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 37.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       23 SCHEM 65 -2 0 MPRFLG 0 ; 
       29 SCHEM 55 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 67.5 -2 0 MPRFLG 0 ; 
       25 SCHEM 70 -2 0 MPRFLG 0 ; 
       19 SCHEM 55 -2 0 MPRFLG 0 ; 
       18 SCHEM 50 -2 0 MPRFLG 0 ; 
       21 SCHEM 60 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       26 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       27 SCHEM 45 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
